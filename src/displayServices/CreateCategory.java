package displayServices;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import utils.WaitTool;
import utils.DriverUtils;

public class CreateCategory {
	
	private WebDriver		driver;
	
	private static String categoryName			    = "name";
	private static String descriptionFrame		    = "//*[@id='cke_1_contents']/iframe";
	private static String categoryDescription		= "/html/body";
	private static String saveButton				= "save-category";
	
	public CreateCategory(WebDriver driver) {
		this.driver = driver;
	}
	
	public void setCategoryName(String nameValue) {
		WebElement nameInput = WaitTool.waitForElement(driver, By.name(categoryName), WaitTool.DEFAULT_WAIT_4_ELEMENT);
		DriverUtils.clickElement(nameInput, driver);
		nameInput.clear();
		nameInput.sendKeys(nameValue);
	}
	
	public void setCategoryDescription(String descriptionValue ) {
		driver.switchTo().frame(driver.findElement(By.xpath(descriptionFrame)));
		driver.findElement(By.xpath(categoryDescription)).sendKeys(descriptionValue);
		driver.switchTo().defaultContent();
	}
	
	public void clickSaveButton() {
		DriverUtils.click(By.id(saveButton), driver);
		WaitTool.waitForJQueryProcessing(driver, 5);
	}
}
