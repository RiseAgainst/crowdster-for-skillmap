package displayServices;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import utils.DriverUtils;
import utils.WebTable;

public class Categories {

	private WebDriver		driver;
	private static String createCategory 	= "//*[@id='tab-body']/div[1]/h4/a";
	private static String categoriesTable	= "//*[@id='general-table']";
	private static String searchId			= "general-table_filter";
	
	public Categories(WebDriver driver) {
		this.driver = driver;
	}
	
	public CreateCategory clickCreateCategory() {
		DriverUtils.click(By.xpath(createCategory), driver);
		return new CreateCategory(driver);
	}
	
	public void search(String searchValue) {
		driver.findElement(By.id(searchId)).findElement(By.tagName("input")).sendKeys(searchValue);
	}
	
	public String getCategoryName() {
		return WebTable.getFirstElementFromColumn(categoriesTable, 2, driver);
	}
	
	public void checkIfCreatedCategoryExist(String expectedName) {
		Assert.assertTrue(getCategoryName().equals(expectedName), "Something went wrong with category creation");
	}
}
