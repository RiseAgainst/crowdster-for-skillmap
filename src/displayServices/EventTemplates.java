package displayServices;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

import eventPages.Event;
import utils.DriverUtils;
import utils.WindowHandler;

public class EventTemplates {
	private WindowHandler winHandler = new WindowHandler();
	
	private WebDriver		driver;
	private static String savebutton 		= "customForm";
	private static String category	 		= "categoryId";
	private static String name				= "//*[@id='name']";
	private static String templatesTable	= "//*[@id='general-table_wrapper']";
	private static String searchInput		= "//*[@id='general-table_filter']/label/input";
	
	public EventTemplates(WebDriver driver) {
		this.driver = driver;
	}
	
	public void selectCategory(String categoryValue) {
		new Select(driver.findElement(By.id(category))).selectByVisibleText(categoryValue);
		utils.WaitTool.waitForJQueryProcessing(driver, 5);
	}
	
	public void changeName(String nameValue) {
		driver.findElement(By.xpath(name)).clear();
		driver.findElement(By.xpath(name)).sendKeys(nameValue);
	}
	
	public void clickSave() {
		List<WebElement> buttons = driver.findElements(By.id(savebutton));
		DriverUtils.clickElement(buttons.get(1), driver);
		utils.WaitTool.waitForJQueryProcessing(driver, 5);
	}
	
	public void searchBy(String searchValue) {
		searchClear();
		driver.findElement(By.xpath(searchInput)).sendKeys(searchValue);
		utils.WaitTool.waitForJQueryProcessing(driver, 5);
	}
	

	public void searchClear() {
		driver.findElement(By.xpath(searchInput)).clear();
		driver.findElement(By.xpath(searchInput)).sendKeys(Keys.ENTER);
		utils.WaitTool.waitForJQueryProcessing(driver, 5);
	}
	
	public void clickEdit() {
		WebElement table 	= driver.findElement(By.xpath(templatesTable));
		List<WebElement>firstRow	= table.findElements(By.tagName("tr"));
		Actions action = new Actions(driver);
		action.moveToElement(firstRow.get(1)).perform();
		firstRow.get(1).findElement(By.linkText("Edit")).click();
		utils.WaitTool.waitForJQueryProcessing(driver, 5);
	}
	
	public Event clickView() {
		WebElement table 	= driver.findElement(By.xpath(templatesTable));
		List<WebElement>firstRow	= table.findElements(By.tagName("tr"));
		Actions action = new Actions(driver);
		action.moveToElement(firstRow.get(1)).perform();
		winHandler.getOldWindow(driver);
		firstRow.get(1).findElement(By.linkText("View")).click();
		winHandler.switchToNewWindow(driver);
		utils.WaitTool.waitForJQueryProcessing(driver, 5);
		return new Event(driver);
	}
	
	public void switchToMainWindow() {
		winHandler.switchToOldWindow(driver);
	}

}
