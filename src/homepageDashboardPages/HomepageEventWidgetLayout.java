package homepageDashboardPages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import utils.WaitTool;

public class HomepageEventWidgetLayout {
	private  WebDriver			driver;
	private static final int 	WAIT_4_ELEMENT					= WaitTool.DEFAULT_WAIT_4_ELEMENT;
	
	private static By widgetsLayoutTemplateNameLocator			= By.name("templateText");
	private static By homepageEventsWidgetElementCssLocator		= By.cssSelector("ul.site-list-tiles");
	private static By imageContainerElementCssLocator			= By.cssSelector("div.img-container");
	private static By nameSpanElementCssLocator					= By.cssSelector("span.name");
	private static By eventsThermoDataTableElementCssLocator	= By.cssSelector("table.sites-thermo-data");
	
	public HomepageEventWidgetLayout(WebDriver driver) {
		this.driver = driver;
	}
	
	public String getHomepageEventWidgetLayoutTemplate(){
		return WaitTool.waitForElement(driver, widgetsLayoutTemplateNameLocator, WAIT_4_ELEMENT).getText();
	}
	
	//Asserts
	
	public void checkEventsWidgetListElement(String hpEventWidgetTemplate){
		WebElement widgetsElement = WaitTool.waitForElement(driver, homepageEventsWidgetElementCssLocator, WAIT_4_ELEMENT);
		String widgetsElementClassAtr = widgetsElement.getAttribute("class");
		Assert.assertTrue(widgetsElement.isDisplayed(), "Events widget element is not displayed.");
		Assert.assertTrue(hpEventWidgetTemplate.contains(widgetsElementClassAtr), "Events list element wrong attribute class.");
	}
	
	public void checkImageContainer(String hpEventWidgetTemplate){
		WebElement imgContainerElement = WaitTool.waitForElement(driver, imageContainerElementCssLocator, WAIT_4_ELEMENT);
		String imgContainerElementClassAtr = imgContainerElement.getAttribute("class");
		Assert.assertTrue(imgContainerElement.isDisplayed(), "Image container element is not displayed.");
		Assert.assertTrue(hpEventWidgetTemplate.contains(imgContainerElementClassAtr), "Image container element wrong attribute class.");
	}
	
	public void checkNameSpanElement(String hpEventWidgetTemplate){
		WebElement nameSpanElement = WaitTool.waitForElement(driver, nameSpanElementCssLocator, WAIT_4_ELEMENT);
		String nameSpanElementClassAtr = nameSpanElement.getAttribute("class");
		Assert.assertTrue(nameSpanElement.isDisplayed(), "Name span element is not displayed.");
		Assert.assertTrue(hpEventWidgetTemplate.contains(nameSpanElementClassAtr), "Name span element wrong attribute class.");
	}
	
	public void checkEventsThermoDataTable(String hpEventWidgetTemplate){
		WebElement thermoDataTableElement = WaitTool.waitForElement(driver, eventsThermoDataTableElementCssLocator, WAIT_4_ELEMENT);
		String thermoDataTableElementClassAtr = thermoDataTableElement.getAttribute("class");
		Assert.assertTrue(thermoDataTableElement.isDisplayed(), "Thermo data table element is not displayed.");
		Assert.assertTrue(hpEventWidgetTemplate.contains(thermoDataTableElementClassAtr), "Thermo data table element wrong attribute class.");
	}
}
