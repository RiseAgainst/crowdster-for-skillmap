package homepageDashboardPages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import utils.DriverUtils;
import utils.WaitTool;

public class HomepageSubPageLayout {
	private  WebDriver			driver;
	private static final int 	WAIT_4_ELEMENT				= WaitTool.DEFAULT_WAIT_4_ELEMENT;
	
	private static By subPageLayoutTemplateNameLocator		= By.name("templateText");
	private static By mainDivElementIdLocator				= By.id("siteMain");
	private static By homepageEventsListCssLocator			= By.cssSelector("ul.site-list-tiles");
	private static By listElementTagNameLocator				= By.tagName("li");
	private static By headingDivElementCssLocator			= By.cssSelector("div.m-hp-main-heading");
	
	public HomepageSubPageLayout(WebDriver driver) {
		this.driver = driver;
	}
	
	public String getHomepageSubPageLayoutTemplate(){
		return WaitTool.waitForElement(driver, subPageLayoutTemplateNameLocator, WAIT_4_ELEMENT).getText();
	}
	
	public void clickFirstEvent(){
		WebElement homepageEventsElement = WaitTool.waitForElement(driver, homepageEventsListCssLocator, WAIT_4_ELEMENT);
		List<WebElement> listElements = homepageEventsElement.findElements(listElementTagNameLocator);
		DriverUtils.clickElement(listElements.get(0), driver);
	}
	
	//Asserts
	
	public void checkMainDivElement(String subPageLayoutTemplate){
		WebElement mainDivElement = WaitTool.waitForElement(driver, mainDivElementIdLocator, WAIT_4_ELEMENT);
		String mainDivElementIdAtr = mainDivElement.getAttribute("id");
		Assert.assertTrue(mainDivElement.isDisplayed(), "Main div element is not displayed.");
		Assert.assertTrue(subPageLayoutTemplate.contains(mainDivElementIdAtr), "Main div element wrong id attribute.");
	}
	
	public void checkHeadingDivElement(String subPageLayoutTemplate){
		WebElement headingDivElement = WaitTool.waitForElement(driver, headingDivElementCssLocator, WAIT_4_ELEMENT);
		String mainDivElementClassAtr = headingDivElement.getAttribute("class");
		Assert.assertTrue(headingDivElement.isDisplayed(), "Heading div element is not displayed.");
		Assert.assertTrue(subPageLayoutTemplate.contains(mainDivElementClassAtr), "Heading div element wrong class attribute.");
	}
}
