package homepageDashboardPages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import utils.WaitTool;

public class HomepageLayout {
	private  WebDriver			driver;
	private static final int 	WAIT_4_ELEMENT				= WaitTool.DEFAULT_WAIT_4_ELEMENT;
	
	private static By homepageLayoutNameLocator				= By.name("templateText");
	private static By homepageRotatorCssSelector			= By.cssSelector(".cff-super-hp-rotator-0");
	private static By homepageScriptElementTagLocator 		= By.tagName("script");
	private static By homepageMainDivElementIdLocator		= By.id("siteMain");
	private static By homepageHyperLinkElementLinkTextLocator 	= By.linkText("Get Started!");
	private static By homepageInputElementIdLocator			= By.id("searchInput-proj");
	private static By homepageFormElementTagNameLocator		= By.tagName("form");
	private static By homepageBodyElementIdLocator 			= By.tagName("body");
	
	public HomepageLayout(WebDriver driver) {
		this.driver = driver;

	}
	
	public String getHomepageLayout(){
		return WaitTool.waitForElement(driver, homepageLayoutNameLocator, WAIT_4_ELEMENT).getText();
	}
	
	//Asserts
	
	public void checkStyleBackgroundImg(String homepageLayoutText){
		WebElement rotatorElement = WaitTool.waitForElement(driver, homepageRotatorCssSelector, WAIT_4_ELEMENT);
		String backgroundImgValue = rotatorElement.getCssValue("background-image");
		backgroundImgValue = backgroundImgValue.substring(25, backgroundImgValue.length() - 2);
		Assert.assertTrue(homepageLayoutText.contains(backgroundImgValue), "Homepage layout wrong attribute background-image.");
	}
	
	public void checkScriptSource(String homepageLayoutText){
		List<WebElement> scriptElement = WaitTool.waitForListElementsPresent(driver, homepageScriptElementTagLocator, WAIT_4_ELEMENT);
		String scriptSource = scriptElement.get(0).getAttribute("src");
		scriptSource = scriptSource.replace("http://karma.com", "");
		Assert.assertTrue(homepageLayoutText.contains(scriptSource), "Homepage layout wrong attribute script.");
	}
	
	public void checkDivElementById(String homepageLayoutText){
		WebElement divMainElement = WaitTool.waitForElement(driver, homepageMainDivElementIdLocator, WAIT_4_ELEMENT);
		String divMainElementId = divMainElement.getAttribute("id");
		Assert.assertTrue(divMainElement.isDisplayed(), "Div element is not displayed.");
		Assert.assertTrue(homepageLayoutText.contains(divMainElementId), "Homepage main div wrong attribute id.");
	}
	
	public void checkHyperLinkElement(String homepageLayoutText){
		WebElement hyperLinkElement = WaitTool.waitForElement(driver, homepageHyperLinkElementLinkTextLocator, WAIT_4_ELEMENT);
		String hyperLinkElementClassAtr = hyperLinkElement.getAttribute("class");
		Assert.assertTrue(hyperLinkElement.isDisplayed(), "Hyper link element is not displayed.");
		Assert.assertTrue(homepageLayoutText.contains(hyperLinkElementClassAtr), "Homepage hyper link wrong attribute class.");
	}
	
	public void checkInputElement(String homepageLayoutText){
		WebElement inputElement = WaitTool.waitForElement(driver, homepageInputElementIdLocator, WAIT_4_ELEMENT);
		String inputElementNameAtr = inputElement.getAttribute("name");
		Assert.assertTrue(inputElement.isDisplayed(), "Input element is not displayed.");
		Assert.assertTrue(homepageLayoutText.contains(inputElementNameAtr), "Input element wrong attribute name.");
	}
	
	public void checkFormElement(String homepageLayoutText){
		WebElement formElement = WaitTool.waitForElement(driver, homepageFormElementTagNameLocator, WAIT_4_ELEMENT);
		String formElementClassAtr = formElement.getAttribute("class");
		Assert.assertTrue(formElement.isDisplayed(), "Form element is not displayed.");
		Assert.assertTrue(homepageLayoutText.contains(formElementClassAtr), "Form element wrong attribute class.");
	}
	
	public void checkBodyElement(String homepageLayoutText){
		WebElement bodyElement = WaitTool.waitForElement(driver, homepageBodyElementIdLocator, WAIT_4_ELEMENT);
		Assert.assertTrue(bodyElement.isDisplayed(), "Body element is not displayed.");
	}
}
