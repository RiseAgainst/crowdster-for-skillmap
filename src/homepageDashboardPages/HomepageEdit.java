package homepageDashboardPages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import eventDashboardPages.CreateEvent;

public class HomepageEdit {
	private  WebDriver		driver;
	private static String createEvent = "micro-lic-popup";
	
	public HomepageEdit(WebDriver driver) {
		this.driver = driver;
	}
	
	public CreateEvent clickCreateEvent() {
		driver.findElement(By.id(createEvent)).click();
		return new CreateEvent(driver);
	}
	
}
