package homepageDashboardPages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import utils.DriverUtils;
import utils.WaitTool;

public class HomepageMasthead {
	private  WebDriver		driver;
	private static final int 	WAIT_4_ELEMENT		= WaitTool.DEFAULT_WAIT_4_ELEMENT;
	private static By bodyElementTagNameLocator		= By.tagName("body");
	private static By saveBtnIdLocator				= By.id("createSuperSitemastheadBtn");
	private static By mastheadIdLocator				= By.id("masthead-image");
	
	private static String mastheadTextValue			= "TEST MASTHEAD. TEST MASTHEAD. TEST MASTHEAD.";
	
	public HomepageMasthead(WebDriver driver) {
		this.driver = driver;
	}
	
	public void clickSaveBtn(){
		DriverUtils.click(saveBtnIdLocator, driver);
		WaitTool.waitForJQueryProcessing(driver, WAIT_4_ELEMENT);
	}
	
	public void fillMastheadText(){
		WebElement bodyElement = WaitTool.waitForElement(driver, bodyElementTagNameLocator, WAIT_4_ELEMENT);
		bodyElement.sendKeys(mastheadTextValue);
	}
	
	//Asserts
	
	public void checkMastheadText(){
		String mastheadText = WaitTool.waitForElement(driver, mastheadIdLocator, WAIT_4_ELEMENT).getText();
		Assert.assertTrue(mastheadText.equals(mastheadTextValue), "Wrong masthead text.");
	}
	
	public void checkMastheadImage(){
		WebElement mastheadElement = WaitTool.waitForElement(driver, mastheadIdLocator, WAIT_4_ELEMENT);
		mastheadElement.findElement(By.tagName("img"));
		Assert.assertTrue(mastheadElement.isDisplayed(), "Masthead image is not displayed.");
	}
}
