package homepageDashboardPages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;

import utils.DriverUtils;
import utils.WaitTool;

public class HomepageNavigation {
	private  WebDriver			driver;
	private static final int 	WAIT_4_ELEMENT		= WaitTool.DEFAULT_WAIT_4_ELEMENT;
	
	private static String navigationLabelValue			= " TEST Navigation Label ";
	private static String titleValue					= "TEST title";
	private static String contentValue					= "TEST content. TEST content. TEST content. TEST content.";
	private static String subpageNavigationLabelValue 	= "TEST Subpage Navigation Label";
	private static String subpageTitleValue				= "TEST Subpage title";
	private static String subpageContentValue			= "TEST Subpage content. TEST Subpage content. TEST content.";
	private static String editNavigationLabelValue		= " TEST EDIT Navigation Label ";
	private static String editTitleValue				= "TEST EDIT title";
	private static String editContentValue				= "TEST  EDIT content. TEST EDIT content. TEST EDIT content.";
	private static String editSubpageNavigationLabelValue 	= "TEST EDIT Subpage Navigation Label";
	private static String editSubpageTitleValue			= "TEST EDIT Subpage title";
	private static String editSubpageContentValue		= "TEST EDIT Subpage content. TEST EDIT Subpage content.";
	                     
	private static By createBtnIdLocator				= By.id("hp_navi_create_btn");
	private static By navigationLabelNameLocator		= By.name("name");
	private static By titleNameLocator					= By.name("title");
	private static By iframeCssLocator					= By.cssSelector("iframe");
	private static By bodyElementTagLocator				= By.tagName("body");
	private static By saveBtnIdLocator					= By.id("createPageBtn");
	private static By navigationPageCssLocator			= By.cssSelector("div.site-rows");
	private static By navigationSubPageCssLocator		= By.cssSelector("div.site-subpages-row");
	private static By addSubpageBtnIdLocator			= By.id("hp_navi_add_subpage");
	private static By saveSubpageBtnIdLocator			= By.id("subePageBtn");
	private static By hpNavigationLabelLinkTextLocator	= By.linkText(navigationLabelValue);
	private static By hpNavigationLabelEditedLinkTextLocator	= By.linkText(editNavigationLabelValue);
	private static By hpTitleTagNameLocator				= By.tagName("h1");
	private static By hpContentCssLocator				= By.cssSelector("div.sitePageInner");
	private static By editNavigationLinkTextLocator		= By.linkText("Edit");
	private static By deleteNavigationLinkTextLocator	= By.linkText("Delete");
	private static By editSaveBtnIdLocator				= By.id("editPageBtn");
	private static By dontShowInNavigationCssLocator	= By.cssSelector("a.toggle");
	
	public HomepageNavigation(WebDriver driver) {
		this.driver = driver;
	}
	
	public void clickCreatePageBtn(){
		DriverUtils.click(createBtnIdLocator, driver);
	}
	
	public void clickSaveBtn(){
		DriverUtils.click(saveBtnIdLocator, driver);
	}
	
	public void clickEditSaveBtn(){
		DriverUtils.click(editSaveBtnIdLocator, driver);
	}
	
	public void clickAddSubpageBtn(){
		DriverUtils.click(addSubpageBtnIdLocator, driver);
	}
	
	public void clickSaveSubpageBtn(){
		DriverUtils.click(saveSubpageBtnIdLocator, driver);
	}
	
	public void clickNavigationPage(){
		WaitTool.waitForElement(driver, hpNavigationLabelLinkTextLocator, WAIT_4_ELEMENT).click();
	}
	
	public void clickNavigationPageEdited(){
		WaitTool.waitForElement(driver, hpNavigationLabelEditedLinkTextLocator, WAIT_4_ELEMENT).click();
	}
	
	public void clickEditNavigationPage(){
		List<WebElement> navigationPagesElements = WaitTool.waitForListElementsPresent(driver, navigationPageCssLocator, WAIT_4_ELEMENT);
		navigationPagesElements.get(0).findElement(editNavigationLinkTextLocator).click();
	}
	
	public void clickEditNavigationSubpage(){
		List<WebElement> navigationSubpagesElements = WaitTool.waitForListElementsPresent(driver, navigationSubPageCssLocator, WAIT_4_ELEMENT);
		navigationSubpagesElements.get(0).findElement(editNavigationLinkTextLocator).click();
	}
	
	public void clickDeleteNavigationPage(){
		List<WebElement> navigationPagesElements = WaitTool.waitForListElementsPresent(driver, navigationPageCssLocator, WAIT_4_ELEMENT);
		navigationPagesElements.get(0).findElement(deleteNavigationLinkTextLocator).click();
	}
	
	public void clickDeleteNavigationSubpage(){
		List<WebElement> navigationPagesElements = WaitTool.waitForListElementsPresent(driver, navigationSubPageCssLocator, WAIT_4_ELEMENT);
		navigationPagesElements.get(0).findElement(deleteNavigationLinkTextLocator).click();
	}
	
	public void clickDontShowInNavigation(){
		WaitTool.waitForElement(driver, dontShowInNavigationCssLocator, WAIT_4_ELEMENT).click();
	}
	
	public void clickNavigationSubpage(){
		WebElement hpNavigationLabelElement = WaitTool.waitForElement(driver, hpNavigationLabelLinkTextLocator, WAIT_4_ELEMENT);
		Actions action = new Actions(driver);
		action.moveToElement(hpNavigationLabelElement).perform();
		WaitTool.waitForElement(driver, By.linkText(subpageNavigationLabelValue), WAIT_4_ELEMENT).click();
	}
	
	public void clickNavigationSubpageEdited(){
		WebElement hpNavigationLabelElement = WaitTool.waitForElement(driver, hpNavigationLabelEditedLinkTextLocator, WAIT_4_ELEMENT);
		Actions action = new Actions(driver);
		action.moveToElement(hpNavigationLabelElement).perform();
		WaitTool.waitForElement(driver, By.linkText(editSubpageNavigationLabelValue), WAIT_4_ELEMENT).click();
	}
	
	public int getNavigationPagesCount(){
		List<WebElement> navigationPagesElements = WaitTool.waitForListElementsPresent(driver, navigationPageCssLocator, WAIT_4_ELEMENT);
		WaitTool.waitForJQueryProcessing(driver, 5);
		if(navigationPagesElements == null){
			return 0;
		}
		return navigationPagesElements.size();
	}
	
	public int getNavigationSubPagesCount(){
		List<WebElement> navigationSubPagesElements = WaitTool.waitForListElementsPresent(driver, navigationSubPageCssLocator, WAIT_4_ELEMENT);
		WaitTool.waitForJQueryProcessing(driver, 5);
		if(navigationSubPagesElements == null){
			return 0;
		}
		return navigationSubPagesElements.size();
	}
	
	public void createNavigationPage(){
		WaitTool.waitForElement(driver, navigationLabelNameLocator, WAIT_4_ELEMENT).sendKeys(navigationLabelValue.trim());
		WaitTool.waitForElement(driver, titleNameLocator, WAIT_4_ELEMENT).sendKeys(titleValue);
		this.switchToIframe();
		WaitTool.waitForElement(driver, bodyElementTagLocator, WAIT_4_ELEMENT).sendKeys(contentValue);
		this.switchToDefaultContent();
		this.clickSaveBtn();
	}
	
	public void createNavigationSubPage(){
		WaitTool.waitForElement(driver, navigationLabelNameLocator, WAIT_4_ELEMENT).sendKeys(subpageNavigationLabelValue);
		WaitTool.waitForElement(driver, titleNameLocator, WAIT_4_ELEMENT).sendKeys(subpageTitleValue);
		this.switchToIframe();
		WaitTool.waitForElement(driver, bodyElementTagLocator, WAIT_4_ELEMENT).sendKeys(subpageContentValue);
		this.switchToDefaultContent();
		this.clickSaveSubpageBtn();
	}
	
	public void editNavigationPage(){
		WebElement navigationLabelElement = WaitTool.waitForElement(driver, navigationLabelNameLocator, WAIT_4_ELEMENT);
		navigationLabelElement.clear();
		navigationLabelElement.sendKeys(editNavigationLabelValue.trim());
		WebElement titleElement = WaitTool.waitForElement(driver, titleNameLocator, WAIT_4_ELEMENT);
		titleElement.clear();
		titleElement.sendKeys(editTitleValue);
		this.switchToIframe();
		WebElement contentElement = WaitTool.waitForElement(driver, bodyElementTagLocator, WAIT_4_ELEMENT);
		contentElement.clear();
		contentElement.sendKeys(editContentValue);
		this.switchToDefaultContent();
		this.clickEditSaveBtn();
	}
	
	public void editNavigationSubpage(){
		WebElement navigationLabelElement = WaitTool.waitForElement(driver, navigationLabelNameLocator, WAIT_4_ELEMENT);
		navigationLabelElement.clear();
		navigationLabelElement.sendKeys(editSubpageNavigationLabelValue.trim());
		WebElement titleElement = WaitTool.waitForElement(driver, titleNameLocator, WAIT_4_ELEMENT);
		titleElement.clear();
		titleElement.sendKeys(editSubpageTitleValue);
		this.switchToIframe();
		WebElement contentElement = WaitTool.waitForElement(driver, bodyElementTagLocator, WAIT_4_ELEMENT);
		contentElement.clear();
		contentElement.sendKeys(editSubpageContentValue);
		this.switchToDefaultContent();
		this.clickEditSaveBtn();
	}
	
	private void switchToIframe(){
		WebElement iFrame= WaitTool.waitForElement(driver, iframeCssLocator, WAIT_4_ELEMENT);
		driver.switchTo().frame(iFrame);
	}
	
	private void switchToDefaultContent(){
		driver.switchTo().defaultContent();
	}
	
	private String getElementTextByLocator(By locator){
		return WaitTool.waitForElement(driver, locator, WAIT_4_ELEMENT).getText();
	}
	
	private boolean isPrivateSubpage(){
		WebElement hpNavigationLabelElement = WaitTool.waitForElement(driver, hpNavigationLabelEditedLinkTextLocator, WAIT_4_ELEMENT);
		Actions action = new Actions(driver);
		action.moveToElement(hpNavigationLabelElement).perform();
		WebElement homepageSubpage = WaitTool.waitForElement(driver, By.linkText(editSubpageNavigationLabelValue), WAIT_4_ELEMENT);
		if(homepageSubpage == null){
			return true;
		}
		return false;
	}
	
	private boolean isPrivatePage(){
		WebElement hpNavigationLabelElement = WaitTool.waitForElement(driver, hpNavigationLabelEditedLinkTextLocator, WAIT_4_ELEMENT);
		if(hpNavigationLabelElement == null){
			return true;
		}
		return false;
	}
	
	//Asserts
	
	public void checkNavigationPagesCountAfterCreate(int navigationPagesCountBefore, int navigationPagesCountAfter){
		Assert.assertTrue(navigationPagesCountAfter == navigationPagesCountBefore + 1, "Wrong navigation pages count.");
	}
	
	public void checkNavigationSubpagesCountAfterCreate(int navigationSubpagesCountBefore, int navigationSubpagesCountAfter){
		Assert.assertTrue(navigationSubpagesCountAfter == navigationSubpagesCountBefore + 1, "Wrong navigation subpages count.");
	}
	
	public void checkHomepageNavigationPage(){
		Assert.assertTrue(this.getElementTextByLocator(hpTitleTagNameLocator).equals(titleValue), "Wrong Navigation page title.");
		Assert.assertTrue(this.getElementTextByLocator(hpContentCssLocator).equals(contentValue), "Wrong Navigation page content.");
	}
	
	public void checkHomepageNavigationSubpage(){
		Assert.assertTrue(this.getElementTextByLocator(hpTitleTagNameLocator).equals(subpageTitleValue), "Wrong Navigation subpage title.");
		Assert.assertTrue(this.getElementTextByLocator(hpContentCssLocator).equals(subpageContentValue), "Wrong Navigation subpage content.");
	}
	
	public void checkHomepageNavigationPageEdited(){
		Assert.assertTrue(this.getElementTextByLocator(hpTitleTagNameLocator).equals(editTitleValue), "Wrong Navigation page title edited.");
		Assert.assertTrue(this.getElementTextByLocator(hpContentCssLocator).equals(editContentValue), "Wrong Navigation page content edited.");
	}
	
	public void checkHomepageNavigationSubpageEdited(){
		Assert.assertTrue(this.getElementTextByLocator(hpTitleTagNameLocator).equals(editSubpageTitleValue), "Wrong Navigation subpage title edited.");
		Assert.assertTrue(this.getElementTextByLocator(hpContentCssLocator).equals(editSubpageContentValue), "Wrong Navigation subpage content edited.");
	}
	
	public void checkNavigationPagesCountAfterDelete(int navigationPagesCountBefore, int navigationPagesCountAfter){
		Assert.assertTrue(navigationPagesCountAfter == navigationPagesCountBefore - 1, "Wrong navigation pages count.");
	}
	
	public void checkNavigationSubpagesCountAfterDelete(int navigationSubpagesCountBefore, int navigationSubpagesCountAfter){
		Assert.assertTrue(navigationSubpagesCountAfter == navigationSubpagesCountBefore - 1, "Wrong navigation subpages count.");
	}
	
	public void checkPrivateNavigationSubpage(){
		Assert.assertTrue(isPrivateSubpage(), "Subpage is shown.");
	}
	
	public void checkPrivateNavigationPage(){
		Assert.assertTrue(isPrivatePage(), "Page is shown.");
	}
}
