package otherSitesPages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class Twitter {

	private  WebDriver		driver;
	
	private static String loginInput		= "username_or_email";
	private static String passwordInput		= "password";
	private static String submit			= "//*[@id='update-form']/div[3]/fieldset[2]/input[2]";
	
	
	private static String passwordValue		= "crowdstertest";
	
	public Twitter(WebDriver driver) {
		this.driver = driver;
	}
	
	public void sendTwit() {
		driver.findElement(By.id(loginInput)).sendKeys(utils.Data.testEmail);
		driver.findElement(By.id(passwordInput)).sendKeys(passwordValue);
		driver.findElement(By.xpath(submit)).click();
	}
	
	
}
