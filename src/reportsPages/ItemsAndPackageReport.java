package reportsPages;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import utils.URLStatusChecker;
import utils.WebTable;



public class ItemsAndPackageReport {
	
	private WebDriver		driver;
	
	private static String donatioReportTable	= "//*[@id='itemReport']";
	private static String sortByItemName		= "//*[@id='itemReport']/thead/tr/th[1]";
	private static String sortByMsName			= "//*[@id='itemReport']/thead/tr/th[2]";
	private static String sortBySsName			= "//*[@id='itemReport']/thead/tr/th[3]";
	private static String sortByType			= "//*[@id='itemReport']/thead/tr/th[4]";	
	private static String sortByItemType		= "//*[@id='itemReport']/thead/tr/th[5]";	
	private static String sortByAvailableQuant	= "//*[@id='itemReport']/thead/tr/th[6]";
	private static String sortByQuantitySold	= "//*[@id='itemReport']/thead/tr/th[7]";

	
	
	public ItemsAndPackageReport(WebDriver driver) {
		this.driver = driver;
	}
	
	public void sortByItemName() {
		driver.findElement(By.xpath(sortByItemName)).click();
		utils.WaitTool.waitForJQueryProcessing(driver, 5);
	}
	
	public void sortByMsName() {
		driver.findElement(By.xpath(sortByMsName)).click();
		utils.WaitTool.waitForJQueryProcessing(driver, 5);
	}
	
	public void sortBySsName() {
		driver.findElement(By.xpath(sortBySsName)).click();
		utils.WaitTool.waitForJQueryProcessing(driver, 5);
	}
	
	public void sortByType() {
		driver.findElement(By.xpath(sortByType)).click();
		utils.WaitTool.waitForJQueryProcessing(driver, 5);
	}
	
	public void sortByItemType() {
		driver.findElement(By.xpath(sortByItemType)).click();
		utils.WaitTool.waitForJQueryProcessing(driver, 5);
	}
	
	public void sortByAvailableQuant() {
		driver.findElement(By.xpath(sortByAvailableQuant)).click();
		utils.WaitTool.waitForJQueryProcessing(driver, 5);
	}
	
	public void sortByQuantitySold() {
		driver.findElement(By.xpath(sortByQuantitySold)).click();
		utils.WaitTool.waitForJQueryProcessing(driver, 5);
	}
	
	public int getRegItemSoldQuantity() {
		String text = WebTable.getElement(donatioReportTable, 6, 7, driver).getText();
		text = text.replaceAll("\\s", "");
		return Integer.parseInt(text);
	}
	
	public void clickRegItemDrillDown() {
		WebTable.getElement(donatioReportTable, 6, 8, driver).findElement(By.linkText("drill down")).click();
		utils.WaitTool.waitForJQueryProcessing(driver, 5);
	}
	
	public int getQuantitySoldSum() {
		String[] quantitySoldFromTable = WebTable.getDataFromColumn(donatioReportTable, 1, driver);
		int[] results = new int[quantitySoldFromTable.length];
		int sum = 0;
		for (int i = 0; i < quantitySoldFromTable.length; i++) {
			results[i] = Integer.parseInt(quantitySoldFromTable[i]);
			sum = sum + results[i];
		}
		return sum;
	}
	
	public void checkSoldQuantity(int solQuantity, int drillDownSoldQuantity) {
		Assert.assertTrue(solQuantity == drillDownSoldQuantity, "Something wrong with sold quantity in item and package report");
	}
	
	public void checkDownloadLinks() throws Exception{
		List<WebElement> data =	WebTable.getElementsFromColumn(donatioReportTable, 8, driver);
		List<WebElement> dataLinks = new ArrayList<WebElement>();
		URLStatusChecker urlChecker = new URLStatusChecker();
		for(WebElement e : data) {
			dataLinks.add(e.findElement(By.linkText("download")));
		}
		for(WebElement e : dataLinks) {
			int result = urlChecker.getLinkStatusCode(e.getAttribute("href"));
			checkLinkStatus(result);
		}
	}
	
	public void checkDrillDownLinks() throws Exception{
		List<WebElement> data =	WebTable.getElementsFromColumn(donatioReportTable, 8, driver);
		List<WebElement> dataLinks = new ArrayList<WebElement>();
		URLStatusChecker urlChecker = new URLStatusChecker();
		for(WebElement e : data) {
			dataLinks.add(e.findElement(By.linkText("drill down")));
		}
		for(WebElement e : dataLinks) {
			int result = urlChecker.getLinkStatusCode(e.getAttribute("href"));
			System.out.println(result);
			checkLinkStatus(result);
		}
	}
	
	
	
	public void checkLinkStatus(int status) {
		Assert.assertTrue(status == 200 || status == 302, "HTTP Response should be 200 or 302");
	}

}
