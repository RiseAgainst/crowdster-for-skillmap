package reportsPages;

import org.openqa.selenium.By;

import org.openqa.selenium.WebDriver;


public class DonationReport {
	
	private WebDriver		driver;
	
	
	private static String sortByFirstName		= "//*[@id='itemReport']/thead/tr/th[1]";
	private static String sortByLasttName		= "//*[@id='itemReport']/thead/tr/th[2]";
	private static String sortByEmail			= "//*[@id='itemReport']/thead/tr/th[3]";
	private static String sortBySsName			= "//*[@id='itemReport']/thead/tr/th[4]";	
	private static String sortByMsName			= "//*[@id='itemReport']/thead/tr/th[5]";	
	private static String sortByDonationDate	= "//*[@id='itemReport']/thead/tr/th[6]";
	private static String sortByTotal			= "//*[@id='itemReport']/thead/tr/th[7]";
	private static String sortBySource			= "//*[@id='itemReport']/thead/tr/th[8]";
	private static String sortByType			= "//*[@id='itemReport']/thead/tr/th[9]";;
	
	


	
	public DonationReport(WebDriver driver) {
		this.driver = driver;
	}
	
	public void sortByFirstName() {
		driver.findElement(By.xpath(sortByFirstName)).click();
		utils.WaitTool.waitForJQueryProcessing(driver, 5);
	}
	
	public void sortByLastName() {
		driver.findElement(By.xpath(sortByLasttName)).click();
		utils.WaitTool.waitForJQueryProcessing(driver, 5);
	}
	
	public void sortByEmail() {
		driver.findElement(By.xpath(sortByEmail)).click();
		utils.WaitTool.waitForJQueryProcessing(driver, 5);
	}
	
	public void sortBySsName() {
		driver.findElement(By.xpath(sortBySsName)).click();
		utils.WaitTool.waitForJQueryProcessing(driver, 5);
	}
	
	public void sortByMsName() {
		driver.findElement(By.xpath(sortByMsName)).click();
		utils.WaitTool.waitForJQueryProcessing(driver, 5);
	}
	
	public void sortByDonationDate() {
		driver.findElement(By.xpath(sortByDonationDate)).click();
		utils.WaitTool.waitForJQueryProcessing(driver, 5);
	}
	
	public void sortBySource() {
		driver.findElement(By.xpath(sortBySource)).click();
		utils.WaitTool.waitForJQueryProcessing(driver, 5);
	}
	
	public void sortByType() {
		driver.findElement(By.xpath(sortByType)).click();
		utils.WaitTool.waitForJQueryProcessing(driver, 5);
	}
	
	public void sortByRaisedCount() {
		driver.findElement(By.xpath(sortByTotal)).click();
		utils.WaitTool.waitForJQueryProcessing(driver, 5);
	}
	

	

	

	
	



}
