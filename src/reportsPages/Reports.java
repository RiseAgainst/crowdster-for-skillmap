package reportsPages;

import java.util.Arrays;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.Reporter;

import utils.Misc;
import utils.URLStatusChecker;
import utils.WebTable;

public class Reports {
	
	private WebDriver		driver;
	
	private static String donatioReportTable	= "//*[@id='itemReport']";
	private static String donationReportId		= "donation-report-nav";
	private static String itemPackageReportId	= "item-and-package-nav";
	private static String transactionReportId	= "transaction-report-nav";
	private static String campaignPageReportId	= "campaign-page-report-nav";
	private static String socialSharingId		= "-report-nav";
	private static String searchInput			= "//*[@id='itemReport_filter']/label/input";
	private static String reportName			= "//*[@id='tab-body']/div[1]/h4/span";
	private static String reportDownloadXpath	= "//*[@id='tab-body']/div[1]/h4/span/a";
	private static String itemReportEntries		= "//*[@id='itemReport_length']/label/select";
	
	public Reports(WebDriver driver) {
		this.driver = driver;
	}
	
	public DonationReport clickDonationReport() {
		driver.findElement(By.id(donationReportId)).click();
		return new DonationReport(driver);
	}
	
	public ItemsAndPackageReport clickItemAndPackageReport() {
		driver.findElement(By.id(itemPackageReportId)).findElement(By.tagName("a")).click();
		utils.WaitTool.waitForJQueryProcessing(driver, 5);
		utils.WaitTool.waitForTextPresent(driver, By.xpath(reportName), "Item & Package Report", 10);
		return new ItemsAndPackageReport(driver);
	}
	
	public TransactionReport clickTransactionReport() {
		driver.findElement(By.id(transactionReportId)).findElement(By.tagName("a")).click();
		utils.WaitTool.waitForJQueryProcessing(driver, 5);
		return new TransactionReport(driver);
	}
	
	public CampaignPageReport clickCampaignPageReport() {
		driver.findElement(By.id(campaignPageReportId)).findElement(By.tagName("a")).click();
		utils.WaitTool.waitForJQueryProcessing(driver, 5);
		return new CampaignPageReport(driver);
	}
	
	public SocialSharing clickSocialSharing() {
		driver.findElement(By.id(socialSharingId)).findElement(By.tagName("a")).click();
		utils.WaitTool.waitForJQueryProcessing(driver, 5);
		return new SocialSharing(driver);
	}
	
	public void showEntries() {
		new Select(driver.findElement(By.xpath(itemReportEntries))).selectByVisibleText("100");
		utils.WaitTool.waitForJQueryProcessing(driver, 5);
	}
	
	
	
	public void searchClear() {
		driver.findElement(By.xpath(searchInput)).clear();
		driver.findElement(By.xpath(searchInput)).sendKeys(Keys.ENTER);
		utils.WaitTool.waitForJQueryProcessing(driver, 5);
	}
	
	
	public void searchBy(String searchValue) {
		searchClear();
		driver.findElement(By.xpath(searchInput)).sendKeys(searchValue);
		utils.WaitTool.waitForJQueryProcessing(driver, 5);
	}
	
	public void filterByDesktopDonation() {
		searchBy("desktop");
	}
	
	public int getDownloadLink2Status() throws Exception {
		String downloadLink = driver.findElement(By.xpath(reportDownloadXpath)).getAttribute("href");
		URLStatusChecker urlChecker = new URLStatusChecker();
		int result = urlChecker.getLinkStatusCode(downloadLink);
		return result;
	}
	
	
	public String[] getDonationDataFromReport() {
		return WebTable.getDataFromLastRow(donatioReportTable, driver);
	}
	
	public void checkSortingForIntegerValues(int column) {
		String[] data = WebTable.getDataFromColumn(donatioReportTable, column, driver);
		int[] results = new int[data.length];
		for (int i = 0; i < data.length; i++) {
			results[i] = Integer.parseInt(data[i].substring(data[i].indexOf("$") + 1, data[i].indexOf(".")));	
		}
		int[] initial = new int[data.length];
		System.arraycopy(results, 0, initial, 0, data.length);
		if (!Misc.intArraysEqual(initial, results)) {
			Reporter.log("Data are NOT sorted properly");
			Assert.fail("Data are NOT sorted properly");
		}
		else {
			Reporter.log("Data are sorted OK");
		}
	}
	
	public void checkSorting(int column) {
		String[] data = WebTable.getDataFromColumn(donatioReportTable, column, driver);
		String[] initial = new String[data.length];
		System.arraycopy(data, 0, initial, 0, data.length);
		Arrays.sort(data, String.CASE_INSENSITIVE_ORDER);
		System.out.println("Sorted: " + Arrays.asList(data));
		System.out.println("Actual: " + Arrays.asList(initial));
		if (!Misc.stringArraysEqual(initial, data)) {
			Reporter.log("Data are NOT sorted properly");
			Assert.fail("Data are NOT sorted properly");
		}
		else {
			Reporter.log("Data are sorted OK");
		}
	}
	
	public void checkDonationReport(String[] expectedData, String[] actualData) {
		if (!Misc.stringArraysEqual(actualData, expectedData)) {
			Reporter.log("Donation data are NOT  correct");
			Assert.fail("Donation data are NOT  correct");
		}
		else {
			Reporter.log("Donation data are correct");
		}
	}
	
	public void checkDataAfterSearch(int column, String keyword) {
		String[] result = WebTable.getDataFromColumn(donatioReportTable, column, driver);
		for (int i = 0; i < result.length; i++)
		{
			if (!result[i].contains(keyword))
			{
				Reporter.log("'" + result[i] + "' does not contain keyword: '" + keyword + "'");
				Assert.fail("'" + result[i] + "' does not contain keyword: '" + keyword + "'");
                System.out.println(result[i]);
			}
		}
	}
	
	public void checkLinkStatus(int status) {
		Assert.assertTrue(status == 200 || status == 302, "HTTP Response should be 200 or 302");
	}

}
