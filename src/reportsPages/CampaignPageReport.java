package reportsPages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import utils.WebTable;



public class CampaignPageReport {
	
	private WebDriver		driver;
	
	private static String donatioReportTable	= "//*[@id='itemReport']";
	private static String sortByFirstName		= "//*[@id='itemReport']/thead/tr/th[1]";
	private static String sortByLastName		= "//*[@id='itemReport']/thead/tr/th[2]";
	private static String sortByEmail			= "//*[@id='itemReport']/thead/tr/th[3]";
	private static String sortBySsName			= "//*[@id='itemReport']/thead/tr/th[4]";
	private static String sortBySsId			= "//*[@id='itemReport']/thead/tr/th[5]";
	private static String sortByPageType		= "//*[@id='itemReport']/thead/tr/th[6]";
	private static String sortByPageId			= "//*[@id='itemReport']/thead/tr/th[7]";
	private static String sortByRaised			= "//*[@id='itemReport']/thead/tr/th[8]";
	private static String sortByGoal			= "//*[@id='itemReport']/thead/tr/th[9]";
	
	
	public CampaignPageReport(WebDriver driver) {
		this.driver = driver;
	}
	
	public void sortByFirstName() {
		driver.findElement(By.xpath(sortByFirstName)).click();
		utils.WaitTool.waitForJQueryProcessing(driver, 5);
	}
	
	public void sortByLastName() {
		driver.findElement(By.xpath(sortByLastName)).click();
		utils.WaitTool.waitForJQueryProcessing(driver, 5);
	}
	
	public void sortByEmail() {
		driver.findElement(By.xpath(sortByEmail)).click();
		utils.WaitTool.waitForJQueryProcessing(driver, 5);
	}
	
	public void sortBySsName() {
		driver.findElement(By.xpath(sortBySsName)).click();
		utils.WaitTool.waitForJQueryProcessing(driver, 5);
	}
	
	public void sortBySsId() {
		driver.findElement(By.xpath(sortBySsId)).click();
		utils.WaitTool.waitForJQueryProcessing(driver, 5);
	}
	
	public void sortByPageType() {
		driver.findElement(By.xpath(sortByPageType)).click();
		utils.WaitTool.waitForJQueryProcessing(driver, 5);
	}
	
	public void sortByPageId() {
		driver.findElement(By.xpath(sortByPageId)).click();
		utils.WaitTool.waitForJQueryProcessing(driver, 5);
	}
	
	public void sortByRaised() {
		driver.findElement(By.xpath(sortByRaised)).click();
		utils.WaitTool.waitForJQueryProcessing(driver, 5);
	}
	
	public void sortByGoal() {
		driver.findElement(By.xpath(sortByGoal)).click();
		utils.WaitTool.waitForJQueryProcessing(driver, 5);
	}
	
	public int getMasterCampaignRaisedValue() {
		String text = WebTable.getFirstElementFromColumn(donatioReportTable, 8, driver);
		String result = text.substring(text.indexOf("$") + 1, text.indexOf("."));
		result = result.replaceAll(",", "");
		return Integer.parseInt(result);
	}
	
	public void filterByTeam() {
		Reports reports = new Reports(driver);
		reports.searchBy("1362146005796"); //Team Page Id 
	}
	
	public void filterPageId(String pageId) {
		Reports reports = new Reports(driver);
		reports.searchBy(pageId); //Team Page Id 
	}
	
	public void filterByIndividual() {
		Reports reports = new Reports(driver);
		reports.searchBy("individual"); 
	}
	
	public void checkRaisedValueAfterDonation(int beforeDonation, int afterDonation) {
		Assert.assertTrue(beforeDonation + Integer.parseInt(utils.Data.donationAmount) == afterDonation,"Something went wrong with donation");
	}
	
	public void checkRaisedValueAfterBuyingItems(int beforeDonation, int afterDonation, int itemsPrice) {
		Assert.assertTrue(beforeDonation + itemsPrice == afterDonation,"Something went wrong with donation");
	}
	

}
