package reportsPages;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import utils.WebTable;



public class SocialSharing {
	private static String donatioReportTable	= "//*[@id='socialShareReport']";
	private WebDriver		driver;
	
	
	public SocialSharing(WebDriver driver) {
		this.driver = driver;
	}
	
	public String getType() {
		return WebTable.getLastElementFromColumn(donatioReportTable, 5, driver);
	}
	
	public void checkTwitterRecord(String dataFromTable) {
		Assert.assertTrue(dataFromTable.equals("Twitter"));
	}
	
	public void checkEmailRecord(String dataFromTable) {
		Assert.assertTrue(dataFromTable.equals("Email"));
	}
}
