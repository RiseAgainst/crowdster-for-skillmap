package reportsPages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;



public class TransactionReport {
	
	private WebDriver		driver;
	
	private static String sortByTransactionId	= "//*[@id='itemReport']/thead/tr[2]/th[1]";
	private static String sortByMsName			= "//*[@id='itemReport']/thead/tr[2]/th[2]";
	private static String sortBySsName			= "//*[@id='itemReport']/thead/tr[2]/th[3]";
	private static String sortByFirstName		= "//*[@id='itemReport']/thead/tr[2]/th[4]";
	private static String sortByLastName		= "//*[@id='itemReport']/thead/tr[2]/th[5]";
	private static String sortByEmail			= "//*[@id='itemReport']/thead/tr[2]/th[6]";
	private static String sortByDate			= "//*[@id='itemReport']/thead/tr[2]/th[7]";
	private static String sortByValue			= "//*[@id='itemReport']/thead/tr[2]/th[8]";
	private static String sortByItemName		= "//*[@id='itemReport']/thead/tr[2]/th[10]";
	private static String sortBySolicitorId		= "//*[@id='itemReport']/thead/tr[2]/th[11]";
	private static String datePickerFrom		= "itemReport_range_from_4";
	private static String datePickerTo			= "itemReport_range_to_4";
	
	public TransactionReport(WebDriver driver) {
		this.driver = driver;
	}
	
	public void sortByTransactionId() {
		driver.findElement(By.xpath(sortByTransactionId)).click();
		utils.WaitTool.waitForJQueryProcessing(driver, 5);
	}
	
	public void sortByMsName() {
		driver.findElement(By.xpath(sortByMsName)).click();
		utils.WaitTool.waitForJQueryProcessing(driver, 5);
	}
	
	public void sortBySsName() {
		driver.findElement(By.xpath(sortBySsName)).click();
		utils.WaitTool.waitForJQueryProcessing(driver, 5);
	}
	
	public void sortByFirstName() {
		driver.findElement(By.xpath(sortByFirstName)).click();
		utils.WaitTool.waitForJQueryProcessing(driver, 5);
	}
	
	public void sortByLastName() {
		driver.findElement(By.xpath(sortByLastName)).click();
		utils.WaitTool.waitForJQueryProcessing(driver, 5);
	}
	
	public void sortByEmail() {
		driver.findElement(By.xpath(sortByEmail)).click();
		utils.WaitTool.waitForJQueryProcessing(driver, 5);
	}
	
	public void sortByDate() {
		driver.findElement(By.xpath(sortByDate)).click();
		utils.WaitTool.waitForJQueryProcessing(driver, 5);
	}
	
	public void sortByValue() {
		driver.findElement(By.xpath(sortByValue)).click();
		utils.WaitTool.waitForJQueryProcessing(driver, 5);
	}
	
	public void sortByItemName() {
		driver.findElement(By.xpath(sortByItemName)).click();
		utils.WaitTool.waitForJQueryProcessing(driver, 5);
	}
	
	public void sortBySolicitorId() {
		driver.findElement(By.xpath(sortBySolicitorId)).click();
		utils.WaitTool.waitForJQueryProcessing(driver, 5);
	}
	
	public void setDataRange(String date) {
		driver.findElement(By.id(datePickerFrom)).sendKeys(date + Keys.ENTER);
		driver.findElement(By.id(datePickerTo)).sendKeys(date + Keys.ENTER);
		utils.WaitTool.waitForJQueryProcessing(driver, 5);
		driver.findElement(By.xpath("//*[@id='itemReport']")).click();
		utils.WaitTool.waitForJQueryProcessing(driver, 5);
	}
	
	public void checkReportRecord(String[] dataFromReport, String itemName, String value) {
		Assert.assertTrue(dataFromReport[7].equals(value), "Wrong total value");
		Assert.assertTrue(dataFromReport[9].contains(itemName), "Wrong item name");
	}
	

}
