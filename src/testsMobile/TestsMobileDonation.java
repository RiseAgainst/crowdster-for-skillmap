package testsMobile;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import mobilePages.EventMobile;
import mobilePages.PaymentInfoMobile;
import organizationPages.OrganizationDashboard;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import eventDashboardPages.EventDashboard;
import eventPages.EventDonation;

import org.testng.annotations.BeforeClass;

import pages.Login;
import utils.Data;
import utils.DriverSetup;
import utils.WindowHandler;


public class TestsMobileDonation {
	
	private WebDriver	driver;
	private String		username	= Data.userLogin;
	private String		password	= Data.userPassword;
	WindowHandler handler = new WindowHandler();
	
	@BeforeClass
	public void beforeClass() {
		driver = DriverSetup.getDriver("mobile");
		DriverSetup.initialSetup(driver);
	}
	
	@Test
	public void loginPage() {
		Login loginPage = new Login(driver);
		ArrayList<String> locators = pages.Login.getLocators();
		loginPage.locatorsCheck(locators);
	}

	@Test(dependsOnMethods = { "loginPage" }, groups = { "Profile" })
	public void login() {
		Login loginPage = new Login(driver);
		OrganizationDashboard result = loginPage.loginAsUser(username, password);
		loginPage.checkIfLoginSuccessful(result.getTitleText());
	}
	
	@Test(dependsOnMethods = { "login" })
	public void donationByDonationAmountMobile() {	
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditFirstEvent();
		dashboard.switchToEvent();
		EventMobile ms = new EventMobile(driver);
		EventDonation msDonation = ms.clickDonationButton();	
		msDonation.clickDonationTab();
		msDonation.setDonationAmount(utils.Data.donationAmount);
		msDonation.clickQuickDonateMobile();
		PaymentInfoMobile payment = msDonation.clickMobileCompleteCheckout();
		payment.paymentDataFilling();
		payment.acceptTermsAndconditions();
		payment.clickContinue();
		payment.clickCompleteButton();
		payment.checkThankYouMessage();
		dashboard.switchToOldWindow();
	}
	
	
	
	@Test(dependsOnMethods = { "login" })
	public void donationDonationAmoutLowerBoundaryMobile() {
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditFirstEvent();
		dashboard.switchToEvent();
		EventMobile ms = new EventMobile(driver);
		EventDonation msDonation = ms.clickDonationButton();	
		msDonation.clickDonationTab();
		msDonation.setDonationAmount("1");
		msDonation.clickQuickDonateMobile();
		msDonation.clickMobileCompleteCheckout();
		msDonation.checkLowerBoundaryMessage();
		dashboard.switchToOldWindow();
		
	}
	
	@Test(dependsOnMethods = { "login" })
	public void donationDonationAmoutUpperBoundaryMobile() {
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditFirstEvent();
		dashboard.switchToEvent();
		EventMobile ms = new EventMobile(driver);
		EventDonation msDonation = ms.clickDonationButton();	
		msDonation.clickDonationTab();
		msDonation.setDonationAmount("1000000000");
		msDonation.clickQuickDonateMobile();
		msDonation.clickMobileCompleteCheckout();
		msDonation.checkUpperBoundaryMessage();
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" })
	public void donationBuyRegistrationItemMobile() {
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditFirstEvent();
		dashboard.switchToEvent();
		EventMobile ms = new EventMobile(driver);
		EventDonation msDonation = ms.clickRegistrationButton();
		msDonation.clickRegistrationTab();
		msDonation.setRegItemQuantity();
		msDonation.addToCartRegItem();
		PaymentInfoMobile payment = msDonation.clickMobileCompleteCheckout();
		payment.paymentDataFilling();
		payment.acceptTermsAndconditions();
		payment.clickContinue();
		payment.clickCompleteButton();
		payment.checkThankYouMessage();
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" })
	public void donationRegistrationItemLimitMobile() {
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditFirstEvent();
		dashboard.switchToEvent();
		EventMobile ms = new EventMobile(driver);
		EventDonation msDonation = ms.clickRegistrationButton();
		msDonation.clickRegistrationTab();
		msDonation.setRegLimitItemQuantity();
		dashboard.handelJavaScriptAlert();
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" })
	public void donationBuySponsorItemMobile() {	
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditFirstEvent();
		dashboard.switchToEvent();
		EventMobile ms = new EventMobile(driver);
		EventDonation msDonation = ms.clickSponsorButton();
		msDonation.clickSponsorTab();
		msDonation.setSponsorItemQuantity();
		msDonation.addToCartSponsorItem();
		PaymentInfoMobile payment = msDonation.clickMobileCompleteCheckout();
		payment.paymentDataFilling();
		payment.acceptTermsAndconditions();
		payment.clickContinue();
		payment.clickCompleteButton();
		payment.checkThankYouMessage();
		dashboard.switchToOldWindow();
	}
	
	@AfterMethod
	public void setScreenshot(ITestResult result) {
		if (!result.isSuccess()){
			String methodName = result.getName();
			String filename = null;
			try
			{
				SimpleDateFormat formater = new SimpleDateFormat("yyyy_MM_dd_hh_mm_ss");
				File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
				filename = formater.format(Calendar.getInstance().getTime()) + "_" + methodName + ".png";
				String separator = System.getProperty("file.separator");
				String path = System.getProperty("user.dir") + separator + "test-output" + separator + "screens" + separator;
				FileUtils.copyFile(scrFile, new File(path + filename));
				Reporter.setCurrentTestResult(result);
				Reporter.log("<img width='100%' src='" + "screens" + separator + filename + "'/>");
			}
			catch (IOException e1)
			{
				e1.printStackTrace();
			}
			if (driver.getWindowHandles().size() > 1) {
				handler.switchToOldWindow(driver);
			}
		}
	}

	@AfterClass
	public void afterClass() {
		try
		{
			driver.close();
			driver.quit();
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
	} 

}
