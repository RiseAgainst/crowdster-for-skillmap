package testsMobile;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import eventDashboardPages.EventDashboard;
import eventDashboardPages.MobileNavigation;
import organizationPages.OrganizationDashboard;
import pages.Login;
import utils.Data;
import utils.DriverSetup;
import utils.WindowHandler;

public class TestsMobileNavigation {
	private WebDriver		driver;
	private WindowHandler 	handler;
	private String			username	= Data.userLogin;
	private String			password	= Data.userPassword;
	
	@BeforeClass
	public void beforeClass() {
		driver = DriverSetup.getDriver("mobile");
		DriverSetup.initialSetup(driver);
		handler = new WindowHandler();
	}
	
	@Test
	public void loginPage() {
		Login loginPage = new Login(driver);
		ArrayList<String> locators = pages.Login.getLocators();
		loginPage.locatorsCheck(locators);
	}

	@Test(dependsOnMethods = { "loginPage" }, groups = { "Profile" })
	public void login() {
		Login loginPage = new Login(driver);
		OrganizationDashboard result = loginPage.loginAsUser(username, password);
		loginPage.checkIfLoginSuccessful(result.getTitleText());
	}
	
	@Test(dependsOnMethods = { "login" }, priority = 1)
	public void createMobileNavigationPage(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditFirstEvent();
		MobileNavigation mobileNavigation = dashboard.navigateToMobileNavigation();
		mobileNavigation.clickCreatePage();
		mobileNavigation.createNavigationPage();
		mobileNavigation.switchToMobileSiteEvent();
		mobileNavigation.clickMobileMenuNavigationPageElement();
		mobileNavigation.checkMobileNavigationPage();
		mobileNavigation.switchToMainWindow();
	}
	
	@Test(dependsOnMethods = { "login" }, priority = 2)
	public void editMobileNavigationPage(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditFirstEvent();
		MobileNavigation mobileNavigation = dashboard.navigateToMobileNavigation();
		mobileNavigation.clickEditPage();
		mobileNavigation.editNavigationPage();
		mobileNavigation.switchToMobileSiteEvent();
		mobileNavigation.clickMobileMenuNavigationPageEditElement();
		mobileNavigation.checkEditedMobileNavigationPage();
		mobileNavigation.switchToMainWindow();
	}
	
	@Test(dependsOnMethods = { "login" }, priority = 3)
	public void createMobileNavigationSubpage(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditFirstEvent();
		MobileNavigation mobileNavigation = dashboard.navigateToMobileNavigation();
		mobileNavigation.clickAddSubpage();
		mobileNavigation.createNavigationSubpage();
		mobileNavigation.switchToMobileSiteEvent();
		mobileNavigation.clickMobileMenuNavigationSubpageElement();
		mobileNavigation.checkMobileNavigationSubpage();
		mobileNavigation.switchToMainWindow();
	}
	
	@Test(dependsOnMethods = { "login" }, priority = 4)
	public void editMobileNavigationSubpage(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditFirstEvent();
		MobileNavigation mobileNavigation = dashboard.navigateToMobileNavigation();
		mobileNavigation.clickEditSubpage();
		mobileNavigation.editNavigationSubpage();
		mobileNavigation.switchToMobileSiteEvent();
		mobileNavigation.clickMobileMenuNavigationSubpageEditedElement();
		mobileNavigation.checkMobileNavigationSubpageEdited();
		mobileNavigation.switchToMainWindow();
	}
	
	@Test(dependsOnMethods = { "login" }, priority = 5)
	public void deleteMobileNavigationSubpage(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditFirstEvent();
		MobileNavigation mobileNavigation = dashboard.navigateToMobileNavigation();
		mobileNavigation.clickDeleteSubpage();
		mobileNavigation.waitSwitchAcceptJsAlert();
	}
	
	@Test(dependsOnMethods = { "login" }, priority = 6)
	public void deleteMobileNavigationPage(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditFirstEvent();
		MobileNavigation mobileNavigation = dashboard.navigateToMobileNavigation();
		int naviPagesCountBefore = mobileNavigation.getNaviPagesCount();
		mobileNavigation.deleteNavigationPage();
		mobileNavigation.clickNavigationDashboard();
		int naviPagesCountAfter = mobileNavigation.getNaviPagesCount();
		mobileNavigation.checkNavigationPagesCount(naviPagesCountBefore, naviPagesCountAfter);
	}
	
	@Test(dependsOnMethods = { "login" }, priority = 7)
	public void createPrivateMobileNavigationPage() {
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditFirstEvent();
		MobileNavigation mobileNavigation = dashboard.navigateToMobileNavigation();
		mobileNavigation.clickCreatePage();
		mobileNavigation.createPrivateNavigationPage();
		mobileNavigation.switchToMobileSiteEvent();
		Boolean isPrivate = mobileNavigation.isPrivateNavigationPage();
		mobileNavigation.checkForPrivateNavigationPage(isPrivate);
		mobileNavigation.switchToMainWindow();
	}
	
	@Test(dependsOnMethods = { "login" }, priority = 8)
	public void createPrivateMobileNavigationSubpage() {
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditFirstEvent();
		MobileNavigation mobileNavigation = dashboard.navigateToMobileNavigation();
		mobileNavigation.clickAddSubpage();
		mobileNavigation.createPrivateNavigationSubpage();
		mobileNavigation.switchToMobileSiteEvent();
		Boolean isPrivate = mobileNavigation.isPrivateNavigationSubpage();
		mobileNavigation.checkForPrivateNavigationSubpage(isPrivate);
		mobileNavigation.switchToMainWindow();
	}
	
	@AfterMethod
	public void setScreenshot(ITestResult result) {
		if (!result.isSuccess()){
			String methodName = result.getName();
			String filename = null;
			try
			{
				SimpleDateFormat formater = new SimpleDateFormat("yyyy_MM_dd_hh_mm_ss");
				File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
				filename = formater.format(Calendar.getInstance().getTime()) + "_" + methodName + ".png";
				String separator = System.getProperty("file.separator");
				String path = System.getProperty("user.dir") + separator + "test-output" + separator + "screens" + separator;
				FileUtils.copyFile(scrFile, new File(path + filename));
				Reporter.setCurrentTestResult(result);
				Reporter.log("<img width='100%' src='" + "screens" + separator + filename + "'/>");
			}
			catch (IOException e1)
			{
				e1.printStackTrace();
			}
			if (driver.getWindowHandles().size() > 1) {
				handler.switchToOldWindow(driver);
			}
		}
	}
	
	@AfterClass
	public void afterClass() {
		try
		{
			driver.close();
			driver.quit();
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
	} 
}
