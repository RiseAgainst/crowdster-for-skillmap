package testsMobile;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import eventDashboardPages.EventDashboard;
import eventDashboardPages.MobileAppearance;
import organizationPages.OrganizationDashboard;
import pages.Login;
import utils.Data;
import utils.DriverSetup;
import utils.WindowHandler;

public class TestsMobileAppearance {
	private WebDriver		driver;
	private WindowHandler 	handler;
	private String			username	= Data.userLogin;
	private String			password	= Data.userPassword;
	
	@BeforeClass
	public void beforeClass() {
		driver = DriverSetup.getDriver("mobile");
		DriverSetup.initialSetup(driver);
		handler = new WindowHandler();
	}
	
	@Test
	public void loginPage() {
		Login loginPage = new Login(driver);
		ArrayList<String> locators = pages.Login.getLocators();
		loginPage.locatorsCheck(locators);
	}

	@Test(dependsOnMethods = { "loginPage" })
	public void login() {
		Login loginPage = new Login(driver);
		OrganizationDashboard result = loginPage.loginAsUser(username, password);
		loginPage.checkIfLoginSuccessful(result.getTitleText());
	}
	
	@Test(dependsOnMethods = { "login" })
	public void setMobileSiteEventHeader(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditFirstEvent();
		MobileAppearance mobileAppearance = dashboard.navigateToMobileAppearance();
		mobileAppearance.setMobileHeader();
		String mobileSiteEventHeader = mobileAppearance.getMobileSiteEventHeader();
		mobileAppearance.checkMobileSiteEventHeader(mobileSiteEventHeader);
		mobileAppearance.switchToMainWindow();
	}
	
	@Test(dependsOnMethods = { "login" })
	public void setMobileSiteEventHomepageMainContent(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditFirstEvent();
		MobileAppearance mobileAppearance = dashboard.navigateToMobileAppearance();
		mobileAppearance.setMobileSiteEventHomepageMainContent();
		String mobileSiteEventHomepageMainContent = mobileAppearance.getMobileSiteEventHomepageMainContent();
		mobileAppearance.checkMobileSiteHomepageMainContent(mobileSiteEventHomepageMainContent);
		mobileAppearance.switchToMainWindow();
	}
	
	@Test(dependsOnMethods = { "login" })
	public void setMobileSiteEventMenuIconColor(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditFirstEvent();
		MobileAppearance mobileAppearance = dashboard.navigateToMobileAppearance();
		String minicolorsSwatchStyle = mobileAppearance.setMobileSiteEventMenuIconColor();
		String backgroundColor = mobileAppearance.getMobileSiteEventMenuIconColor();
		mobileAppearance.checkMobileMenuIconColor(minicolorsSwatchStyle, backgroundColor);
		mobileAppearance.switchToMainWindow();
	}
	
	@AfterMethod
	public void setScreenshot(ITestResult result) {
		if (!result.isSuccess()){
			String methodName = result.getName();
			String filename = null;
			try
			{
				SimpleDateFormat formater = new SimpleDateFormat("yyyy_MM_dd_hh_mm_ss");
				File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
				filename = formater.format(Calendar.getInstance().getTime()) + "_" + methodName + ".png";
				String separator = System.getProperty("file.separator");
				String path = System.getProperty("user.dir") + separator + "test-output" + separator + "screens" + separator;
				FileUtils.copyFile(scrFile, new File(path + filename));
				Reporter.setCurrentTestResult(result);
				Reporter.log("<img width='100%' src='" + "screens" + separator + filename + "'/>");
			}
			catch (IOException e1)
			{
				e1.printStackTrace();
			}
			if (driver.getWindowHandles().size() > 1) {
				handler.switchToOldWindow(driver);
			}
		}
	}
	
	@AfterClass
	public void afterClass() {
		try
		{
			driver.close();
			driver.quit();
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
	} 
}
