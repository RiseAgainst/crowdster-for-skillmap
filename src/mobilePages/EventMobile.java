package mobilePages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import utils.DriverUtils;
import utils.WaitTool;
import eventPages.EventDonation;
import eventPages.EventTeam;


public class EventMobile {
	private WebDriver			driver;
	
	private static String mobileThermometrText	= "number-div";
	private static String mobileDropDownMenu	= "menu-icon";
	private static String menuLocators			= "drop";
	private static String teamClass				= "profile-info-container";
	
	private static By registrationBtnIdLocator	= By.id("registration-btn-mobile");
	private static By donationBtnIdLocator		= By.id("donation-btn-mobile");
	private static By sponsorBtnIdLocator		= By.id("sponsor-btn-mobile");

	
	public EventMobile(WebDriver driver) {
		this.driver = driver;
	}
	
	

	public void clickDropDownMenu() {
	driver.findElement(By.id(mobileDropDownMenu)).click();	
	}
	
	public void clickHomeMobile() {
		clickDropDownMenu();
		List<WebElement> elements = driver.findElements(By.className(menuLocators));
		elements.get(0).findElement(By.tagName("a")).click();
	}
	
	public int getMobileThermometrValue() {
		String thermometrTxt = driver.findElement(By.id(mobileThermometrText)).getText();
		return Integer.parseInt(thermometrTxt.substring(thermometrTxt.indexOf("$") + 1));
	}
	
	
	public EventDonation clickDonationButton() {
		DriverUtils.click(donationBtnIdLocator, driver);
		WaitTool.waitForJQueryProcessing(driver, 5);
		return new EventDonation(driver);
	}
	
	public EventDonation clickRegistrationButton() {
		DriverUtils.click(registrationBtnIdLocator, driver);
		WaitTool.waitForJQueryProcessing(driver, 5);
		return new EventDonation(driver);
	}
	
	public EventDonation clickSponsorButton() {
		DriverUtils.click(sponsorBtnIdLocator, driver);
		WaitTool.waitForJQueryProcessing(driver, 5);
		return new EventDonation(driver);
	}
	
	public EventTeam goToTeamPage() {
		List<WebElement> buttons = driver.findElements(By.className(teamClass));
		System.out.println(buttons.get(4).getText());
		buttons.get(4).click();
		return new EventTeam(driver);
		
	}

	//Asserts
	public void checkDonateThermometr(int beforeDonation, int afterDonation) {
		Assert.assertTrue(beforeDonation + Integer.parseInt(utils.Data.donationAmount) == afterDonation,"Something went wrong with donation");
	}
	
	public void checkDonateThermometrAfterBuyingItems(int beforeDonation, int afterDonation, int itemsPrice) {
		Assert.assertTrue(beforeDonation + itemsPrice == afterDonation,"Something went wrong with donation");
	}
	
	
}
