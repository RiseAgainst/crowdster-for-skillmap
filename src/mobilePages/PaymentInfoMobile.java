package mobilePages;

import java.util.ArrayList;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import utils.Misc;
import utils.WaitTool;

public class PaymentInfoMobile {
	private WebDriver		driver;
	
	//Payment Data
	private static String firstNameValue 		= "WalterMobile";
	private static String lastNameValue  		= "White";
	private static String emailValue	 		= "ww@test.qwe";
	private static String cardNameValue			= "Visa";
	private static String cardNumberValue		= "4111111111111111";
	private static String securityCodeValue		= "123";
	private static String addressValue			= "Test street 20";
	private static String cityValue				= "City of heaven";
	private static String postalCodeValue		= "777";
	private static String thankYouMessageValue	= "Thank you";
	private  String paymentData[] = { firstNameValue, lastNameValue, emailValue, "FirstSupersite", "FirstEvent", "date", utils.Data.donationAmountForReports, "Online", "Site"};
	
	//Locators
	private static String firstNameId 			= "firstName";
	private static String lastNameId  			= "lastName";
	private static String emailId	  			= "email";
	private static String cardNameXpath			= "//*[@id='credit-card-form']/div[6]/p[1]/input";
	private static String creditCardNumberXpath	= "//*[@class='card-number stripe-sensitive required']";
	private static String securityCodeName		= "card-cvc";
	private static String addressName			= "address";
	private static String cityName				= "city";
	private static String postalCodeName		= "zip";
	private static String termsAndConditionsId	= "legalTerms";
	private static String continueButtonName	= "submit-button";
	private static String completeButtonName	= "Place Order";
	private static String totalValue			= "//*[@id='siteMain']/div/div[4]/b[1]";
	private static String itemName				= "//*[@class='table-rows']/table/tbody/tr/td[1]";
	//Thank you page
	private static String startTeamId			= "start-team-btn";
	private static String joinTeamId			= "join-team-btn";
	private static String createPageId			= "create-your-page";
	
	private static By eventMobileThankYouMsgIdLocator 	= By.id("main");
	
	public PaymentInfoMobile(WebDriver driver) {
		this.driver = driver;
	}
	
	public static ArrayList<String> getThankYouPageLocators() {
		ArrayList<String> locators = new ArrayList<String>();
		locators.add("id," + startTeamId + ", Start Team");
		locators.add("id," + joinTeamId + ", Join Team");
		locators.add("id," + createPageId + ", Create Page");
		return locators;
	}
	
	
	public void paymentDataFilling() {
		driver.findElement(By.id(firstNameId)).sendKeys(firstNameValue);
		driver.findElement(By.id(lastNameId)).sendKeys(lastNameValue);
		driver.findElement(By.id(emailId)).sendKeys(emailValue);
		driver.findElement(By.xpath(cardNameXpath)).sendKeys(cardNameValue);
		driver.findElement(By.xpath(creditCardNumberXpath)).sendKeys(cardNumberValue);
		driver.findElement(By.name(securityCodeName)).sendKeys(securityCodeValue);
		driver.findElement(By.name(addressName)).sendKeys(addressValue);
		driver.findElement(By.name(cityName)).sendKeys(cityValue);
		driver.findElement(By.name(postalCodeName)).sendKeys(postalCodeValue);
		driver.findElement(By.name(securityCodeName)).sendKeys(securityCodeValue);
	}
	
	public String getItemName() {
		return driver.findElement(By.xpath(itemName)).getText();
	}

	public void acceptTermsAndconditions() {
		driver.findElement(By.id(termsAndConditionsId)).click();
	}
	
	public void clickContinue() {
		driver.findElement(By.name(continueButtonName)).click();
	}
	
	public void clickCompleteButton() {
		driver.findElement(By.name(completeButtonName)).click();
	}
	
	public void thankYouPageLocatorsCheck() {
		Assert.assertTrue(Misc.checklocators(getThankYouPageLocators(), driver), "All locators should be present");
	}
	
	public void checkThankYouMessage() {
		WebElement thankYouMsgElement = WaitTool.waitForElement(driver, eventMobileThankYouMsgIdLocator, WaitTool.DEFAULT_WAIT_4_ELEMENT);
		Assert.assertTrue(thankYouMsgElement.getText().equals(thankYouMessageValue),"Wrong message !");
	}
	
	public String [] getPaymentData() {
		paymentData[5] = Misc.getCurrentDate("ReportFormat");
		return paymentData; 
	}
	
	public String getTotalValue() {
		String text = driver.findElement(By.xpath(totalValue)).getText();
		String name = text.substring(text.indexOf("$"), text.lastIndexOf("0"));
		return name;
	}
	

	
	
	
	
}
