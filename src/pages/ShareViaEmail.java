package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ShareViaEmail {
	private WebDriver		driver;
	
	private static String emailsInput = "//*[@id='singleFieldTags']/li/input";
	private static String messageArea = "/html";
	private static String sendButton = "customFormBtn";
	
	
	public ShareViaEmail(WebDriver driver) {
		this.driver = driver;
	}
	
	public void sendEmail() {
		driver.findElement(By.xpath(emailsInput)).sendKeys(utils.Data.testEmail);
		driver.findElement(By.xpath(messageArea)).sendKeys("test message");
		driver.findElement(By.id(sendButton)).click();
	}

}
