package pages;

import java.util.ArrayList;
import java.util.List;




import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import utils.DriverUtils;
import utils.Misc;

public class Registration {
	private WebDriver		driver;
	private static String registrationPageUrl = utils.Data.baseURL + "/create/account.do";
	private static String userNameXpath			= "/html/body/div[1]/div/div/div[1]/div[1]";
	
	private static String firstNameId			= "firstName";
	private static String lastNameId			= "lastName";
	private static String organizationId		= "orgName";
	private static String emailId				= "email";
	private static String passwordId			= "password";
	private static String passwordConfirmId		= "passwordAgain";
	private static String promocodeId			= "awards";
	private static String termsOfUseId			= "terms";
	private static String continueXpath			= "//*[@id='crowdster-form']/input[3]";
	private static String connectWithFb			= "/html/body/div[3]/p/a/img";
	//Data
	private static String firstNameValue		= "User";
	private static String lastNameValue			= "Test";
	private static String organizationValue		= "Test organization";
	private static String emailValue			= "test7@test.test";
	private static String passwordValue			= "zxcv$%";
	private static String promocodeValue		= "123";
	

	
	public Registration(WebDriver driver) {
		this.driver = driver;
		driver.navigate().to(registrationPageUrl);
	}
	
	public static ArrayList<String> getLocators() {
		ArrayList<String> locators = new ArrayList<String>();
		locators.add("id," + firstNameId + ", First Name");
		locators.add("id," + lastNameId + ", Last Name");
		locators.add("id," + organizationId + ", Organization Name");
		locators.add("id," + emailId + ", Email ");
		locators.add("id," + passwordId + ", Password");
		locators.add("id," + passwordConfirmId + ", Confirm Password");
		locators.add("id," + promocodeId + ", Promocode");
		locators.add("id," + termsOfUseId + ", Terms of use");
		locators.add("xpath," + continueXpath + ", Continue button");
		locators.add("xpath," + connectWithFb + ", Connect with FaceBook");
		return locators;
	}
	
	
	public void dataFilling() {
		driver.findElement(By.id(firstNameId)).sendKeys(firstNameValue);
		driver.findElement(By.id(lastNameId)).sendKeys(lastNameValue);
		driver.findElement(By.id(organizationId)).sendKeys(organizationValue);
		driver.findElement(By.id(emailId)).sendKeys(emailValue);
		driver.findElement(By.id(passwordId)).sendKeys(passwordValue);
		driver.findElement(By.id(passwordConfirmId)).sendKeys(passwordValue);
		driver.findElement(By.id(promocodeId)).sendKeys(promocodeValue);
		DriverUtils.click(By.id(termsOfUseId), driver);
		clickContinue();
	}
	
	public void clickContinue() {
		DriverUtils.click(By.xpath(continueXpath), driver);
	}
	
	public int getErrorMessages() {
		List<WebElement> errorList = driver.findElements(By.tagName("label"));
		return errorList.size();
	}

	public void checkErrorMessagesCount() {
		Assert.assertTrue(getErrorMessages() == 6, "Some fields didn't received error message"); // Required fields count = 6, so error message count shoul be = 6 too
	}
	
	public String getUserName() {
		return driver.findElement(By.xpath(userNameXpath)).getText();
	}
	
	public void checkLoginAfterRegistration() {
		Assert.assertTrue(getUserName().equals(firstNameValue + " " + lastNameValue), "Something went wrong with registration");
	}
	
	public void locatorsCheck() {
		Assert.assertTrue(Misc.checklocators(getLocators(), driver), "All locators should be present");
	}
	
	
}
