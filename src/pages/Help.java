package pages;

import java.util.ArrayList;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import utils.Misc;

public class Help {
	private WebDriver		driver;
	
	private static String helpWindowId 			= "display-list-content";
	private static String systemExplainedId		= "system-list";
	private static String solutionExplainedId	= "sol-list";
	private static String ssExplainedId			= "ss-list";
	private static String msExplainedId			= "ms-list";
	private static String displayExplainedid	= "display-list";
	private static String closeHelp				= "close-help-popup";
	
	
	public Help(WebDriver driver) {
		this.driver = driver;
	}
	
	public static ArrayList<String> getPageLocators() {
		ArrayList<String> locators = new ArrayList<String>();
		locators.add("id," + helpWindowId + ", Help window");
		locators.add("id," + systemExplainedId + ", System explained ");
		locators.add("id," + solutionExplainedId + ", Organization explained");
		locators.add("id," + ssExplainedId + ", Supersite explained");
		locators.add("id," + msExplainedId + ", Microsite explained");
		locators.add("id," + displayExplainedid + ", Display explained");
		return locators;
	}
	
	public void clickSystemExplained() {
		driver.findElement(By.id(systemExplainedId)).click();
		utils.WaitTool.waitForJQueryProcessing(driver, 5);
	}
	
	public void clickCloseHelp() {
		driver.findElement(By.className(closeHelp)).click();
		utils.WaitTool.waitForJQueryProcessing(driver, 5);
	}
	
	public void clickOrganizationExplained() {
		driver.findElement(By.id(solutionExplainedId)).click();
		utils.WaitTool.waitForJQueryProcessing(driver, 5);
	}
	
	public void clickSsExplained() {
		driver.findElement(By.id(ssExplainedId)).click();
		utils.WaitTool.waitForJQueryProcessing(driver, 5);
	}
	
	public void clickMsExplained() {
		driver.findElement(By.id(msExplainedId)).click();
		utils.WaitTool.waitForJQueryProcessing(driver, 5);
	}
	
	public void clickDisplayExplained() {
		driver.findElement(By.id(displayExplainedid)).click();
		utils.WaitTool.waitForJQueryProcessing(driver, 5);
	}
	
	public void locatorsCheck() {
		Assert.assertTrue(Misc.checklocators(getPageLocators(), driver), "All locators should be present");
	}

}
