package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;

import utils.DriverUtils;
import utils.FileUploader;
import utils.WaitTool;
import utils.WebTable;

public class AccountSettings {
	private WebDriver			driver;
	private FileUploader		fileUploader;
	private static final int 	WAIT_4_ELEMENT			= WaitTool.DEFAULT_WAIT_4_ELEMENT;
	
	private static String firstNameValue				= "ChangedName";
	private static String lastNameValue					= "ChangedLastName";
	private static String phoneValue					= "+1111223456";
	private static String passwordValue					= "123456";
	private static String passwordNewValue				= "654321";
	private static String newUserFirstName				= "Sarah";
	private static String newUserLastName				= "Kerrigan";
	private static String newUserEmailName				= "sarahkerrigan@test.com";
	private static String organizationImageFileName		= "grandmaster.jpg";
	private static String logoImageFileName				= "randomLogo.png";
	
	private static By userTableIdLocator				= By.id("usersTable");
	private static By manageUserLinkTextLocator			= By.linkText("Manage");
	private static By firstNameNameLocator				= By.name("firstName");
	private static By lastNameNameLocator				= By.name("lastName");
	private static By phoneNameLocator					= By.name("phone");
	private static By saveBtnNameLocator				= By.name("action");
	private static By passwordCurrentNameLocator		= By.name("passwordCurrent");
	private static By passwordNewNameLocator			= By.name("passwordNew");
	private static By passwordConfirmNameLocator		= By.name("passwordAgain");
	private static By createUserBtnIdLocator			= By.id("micro-lic-popup");
	private static By emailNameLocator					= By.name("email");
	private static By brandingLinkTextLocator			= By.linkText("Branding");
	private static By removeBtnNameLocator				= By.name("Remove");
	private static By uploadOrganizationImgBtnIdLocator = By.id("organization_file-addToLib");
	private static By uploadLogoImgBtnIdLocator 		= By.id("file-addToLib");
	private static By fileInputNameLocator				= By.name("file");
	private static By fileSubmitBtnIdLocator			= By.id("uploadmediaSubmit");
	private static By organizationImgIdLocator			= By.id("organization_img");
	private static By logoImgIdLocator					= By.id("logo_img");
	
	public AccountSettings(WebDriver driver) {
		this.driver = driver;
		this.fileUploader = new FileUploader(driver);
	}
	
	public String getPasswordNew(){
		return passwordNewValue;
	}
	
	public void clickSaveBtn(){
		DriverUtils.click(saveBtnNameLocator, driver);
		WaitTool.waitForJQueryProcessing(driver, 5);
	}
	
	public void clickCreateUserBtn(){
		DriverUtils.click(createUserBtnIdLocator, driver);
		WaitTool.waitForJQueryProcessing(driver, 5);
	}
	
	public void clickBranding(){
		WaitTool.waitForElement(driver, brandingLinkTextLocator, WAIT_4_ELEMENT).click();
		WaitTool.waitForJQueryProcessing(driver, 5);
	}

	public void goToAccountInformation(){
		WebElement userElement = WebTable.getFirstElementFromColumnAsWebElementByIdLocator(userTableIdLocator, 1, driver);
		Actions builder = new Actions(driver);
		builder.moveToElement(userElement).perform();		
		WaitTool.waitForElement(driver, manageUserLinkTextLocator, WAIT_4_ELEMENT).click();	
		WaitTool.waitForJQueryProcessing(driver, 5);
	}
	
	public void changeFirstLastNamePhone(){
		DriverUtils.setElementValue(firstNameNameLocator, firstNameValue, driver);
		DriverUtils.setElementValue(lastNameNameLocator, lastNameValue, driver);
		DriverUtils.setElementValue(phoneNameLocator, phoneValue, driver);
	}
	
	public void changePassword(){
		DriverUtils.setElementValue(passwordCurrentNameLocator, passwordValue, driver);
		DriverUtils.setElementValue(passwordNewNameLocator, passwordNewValue, driver);
		DriverUtils.setElementValue(passwordConfirmNameLocator, passwordNewValue, driver);
	}
	
	public void setOldPassword(){
		DriverUtils.setElementValue(passwordCurrentNameLocator, passwordNewValue, driver);
		DriverUtils.setElementValue(passwordNewNameLocator, passwordValue, driver);
		DriverUtils.setElementValue(passwordConfirmNameLocator, passwordValue, driver);
	}
	
	public void createNewUser(){
		DriverUtils.setElementValue(firstNameNameLocator, newUserFirstName, driver);
		DriverUtils.setElementValue(lastNameNameLocator, newUserLastName, driver);
		DriverUtils.setElementValue(emailNameLocator, newUserEmailName, driver);
		DriverUtils.setElementValue(passwordNewNameLocator, passwordValue, driver);
		DriverUtils.setElementValue(passwordConfirmNameLocator, passwordValue, driver);
		this.clickSaveBtn();
	}
	
	public int getUsersCout(){
		return DriverUtils.getTableElementsCount(userTableIdLocator, driver);
	}
	
	public void clickIfExistingRemoveButtons(){
		DriverUtils.clickIfExistingRemoveButtons(removeBtnNameLocator, driver);
	}
	
	public void setOrganizationImage(){
		DriverUtils.click(uploadOrganizationImgBtnIdLocator, driver);
		this.fileUploader.uploadFile(organizationImageFileName, fileInputNameLocator);
		DriverUtils.click(fileSubmitBtnIdLocator, driver);
	}
	
	public void setLogoImage(){
		DriverUtils.click(uploadLogoImgBtnIdLocator, driver);
		this.fileUploader.uploadFile(logoImageFileName, fileInputNameLocator);
		DriverUtils.click(fileSubmitBtnIdLocator, driver);
	}
	
	private boolean isDisplayedInput(By location){
		WebElement webElement = WaitTool.waitForElement(driver, location, WAIT_4_ELEMENT);
		if(webElement == null){
			return false;
		} else {
			if(webElement.isDisplayed()){
				return true;
			}
		}
		return false;
	}
	
	// Asserts
	
	public void checkFirstNameChanged(){
		String firstName = DriverUtils.getElementValueAttribute(firstNameNameLocator, driver);
		Assert.assertTrue(firstName.equals(firstNameValue), "Wrong changed first name.");
	}
	
	public void checkLastNameChanged(){
		String lastName = DriverUtils.getElementValueAttribute(lastNameNameLocator, driver);
		Assert.assertTrue(lastName.equals(lastNameValue), "Wrong changed last name.");
	}
	
	public void checkPhoneChanged(){
		String phone = DriverUtils.getElementValueAttribute(phoneNameLocator, driver);
		Assert.assertTrue(phone.equals(phoneValue), "Wrong changed phone.");
	}
	
	public void checkUsersCountAfterCreated(int usersCountBefore, int usersCountAfter){
		Assert.assertTrue(usersCountAfter == usersCountBefore + 1, "Wrong users count.");
	}
	
	public void checkUsersCountAfterCreateExistingUser(int usersCountBefore, int usersCountAfter){
		Assert.assertTrue(usersCountAfter == usersCountBefore, "Wrong users count.");
	}
	
	public void checkOrganizationImg(){
		String organizationImgSrc = WaitTool.waitForElement(driver, organizationImgIdLocator, WAIT_4_ELEMENT).getAttribute("src");
		organizationImageFileName = organizationImageFileName.substring(0, organizationImageFileName.indexOf("."));
		Assert.assertTrue(organizationImgSrc.contains(organizationImageFileName), "Wrong organization image.");
	}
	
	public void checkLogoImg(){
		String logoImgSrc = WaitTool.waitForElement(driver, logoImgIdLocator, WAIT_4_ELEMENT).getAttribute("src");
		logoImageFileName = logoImageFileName.substring(0, logoImageFileName.indexOf("."));
		Assert.assertTrue(logoImgSrc.contains(logoImageFileName), "Wrong logo image.");
	}
	
	public void checkAccountInformationInputs(){
		Assert.assertTrue(this.isDisplayedInput(firstNameNameLocator), "No first name input on current page.");
		Assert.assertTrue(this.isDisplayedInput(lastNameNameLocator), "No last name input on current page.");
		Assert.assertTrue(this.isDisplayedInput(phoneNameLocator), "No phone input on current page.");
	}
	
	public void checkBrandingNoOptions(){
		Assert.assertTrue(!this.isDisplayedInput(saveBtnNameLocator), "Save button is displayed.");
		Assert.assertTrue(!this.isDisplayedInput(uploadLogoImgBtnIdLocator), "Upload Logo image button is displayed.");
		Assert.assertTrue(!this.isDisplayedInput(uploadOrganizationImgBtnIdLocator), "Upload Organization Image button is displayed.");
	}
}
