package pages;


import organizationPages.ManageOrganizationDashboard;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import displayServices.Categories;
import eventDashboardPages.EventDashboard;
import eventDashboardPages.EventEdit;
import homepageDashboardPages.HomepageEdit;
import utils.DriverUtils;
import utils.WebTable;

public class Menu {
	private WebDriver		driver;
	private static String testOrganization				= "//*[@id='sol-id-1']";
	private static String firstSupersite				= "//*[@id='ss-id-1362146005140']";
	private static String firstEvent					= "FirstEvent"; 
	private static String menuTable						= "//*[@class='menu-popup-wrapper']/table";
	private static String testOrganizationName			= "TestOrganization";
	private static String oneMoreTestSsName				= "One more test";
	private static String micrositeDashboardLinkText 	= "Event Dashboard";
	

	
	public Menu(WebDriver driver) {
		this.driver = driver;
	}
	
	public void clickTestOrganization() {
		DriverUtils.click(By.xpath(testOrganization), driver);
	}
	
	public void clickFirstSuperSite() {
		DriverUtils.click(By.xpath(firstSupersite), driver);
	}
	
	public void clickOrganizationDashboard() {
		WebElement solution = WebTable.getFirstElementFromColumnAsWebElement(menuTable, 1, driver);
		solution.findElement(By.linkText("Organization Dashboard")).click();
	}
	
	public void clickEventDashboard() {
		WebElement solution = WebTable.getFirstElementFromColumnAsWebElement(menuTable, 1, driver);
		DriverUtils.clickChildElement(solution, By.linkText("Event Dashboard"), driver);
	}
	
	public void clickHomepageDashboard() {
		WebElement solution = WebTable.getFirstElementFromColumnAsWebElement(menuTable, 1, driver);
		solution.findElement(By.linkText("Homepage Dashboard")).click();
	}
	
	public Categories clickCategories() {
		WebElement solution = WebTable.getFirstElementFromColumnAsWebElement(menuTable, 1, driver);
		solution.findElement(By.linkText("Categories")).click();
		return new Categories(driver);
	}
	
	public EventDashboard clickEvent() {
		WebElement solution = WebTable.getFirstElementFromColumnAsWebElement(menuTable, 1, driver);
		solution.findElement(By.linkText(micrositeDashboardLinkText)).click();
		return new EventDashboard(driver);
	}
	
	public EventEdit selectEvent(String micrositeName) {
		clickTestOrganization();
		clickFirstSuperSite();
		WebElement site = WebTable.getFirstElementFromColumnAsWebElement(menuTable, 2, driver);
		site.findElement(By.linkText(micrositeName)).click();
		utils.WaitTool.waitForJQueryProcessing(driver, 5);
		return new EventEdit(driver);
	}
	
	public void selectSupersite(String supersiteName) {
		clickTestOrganization();
		WebElement site = WebTable.getFirstElementFromColumnAsWebElement(menuTable, 2, driver);
		site.findElement(By.linkText(supersiteName)).click();
		utils.WaitTool.waitForJQueryProcessing(driver, 5);	
	}
	
	public HomepageEdit clickOneMoreTestSs() {
		selectSupersite(oneMoreTestSsName);
		return new HomepageEdit(driver);
	}
	
	
	
	public ManageOrganizationDashboard manageTestOrganization() {
		WebElement site = WebTable.getFirstElementFromColumnAsWebElement(menuTable, 2, driver);
		site.findElement(By.linkText(testOrganizationName)).click();
		return new ManageOrganizationDashboard(driver);
	}
	
	
	public ManageOrganizationDashboard manageOrganization(String name) {
		WebElement site = WebTable.getFirstElementFromColumnAsWebElement(menuTable, 2, driver);
		site.findElement(By.linkText(name)).click();
		return new ManageOrganizationDashboard(driver);
	}
	
	public EventEdit clickEventFirstEvent() {
		return selectEvent(firstEvent);
	}
	
	
	
	
}
