package pages;

import java.util.ArrayList;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import organizationPages.OrganizationDashboard;
import utils.Misc;

public class Login {
	private WebDriver			driver;
	public static String		pageURL					= utils.Data.baseURL;
	private static String loginFieldId 		= "email";
	private static String passwordFieldId	= "passwordNew";
	private static String loginButton		= "//*[@id='crowdster-form']/input[3]";
	private static String forgotPassword	= "//*[@id='crowdster-form']/p[1]/a";
	
	public Login(WebDriver driver) {
		this.driver = driver;
		driver.navigate().to(pageURL);
	}
	
	public static ArrayList<String> getLocators() {
		ArrayList<String> locators = new ArrayList<String>();
		locators.add("id," + loginFieldId + ", Login Field");
		locators.add("id," + passwordFieldId + ", Password Field");
		locators.add("xpath," + loginButton + ", Login Button");
		locators.add("xpath," + forgotPassword + ", Forgot Password");
		return locators;
	}

	
	public OrganizationDashboard loginAsUser(String username, String password) {
		login(username, password);
		return new OrganizationDashboard(driver);
	}
	
	public void login(String username, String password) {
		loginClean();
		driver.findElement(By.id(loginFieldId)).sendKeys(username);
		driver.findElement(By.id(passwordFieldId)).sendKeys(password + Keys.ENTER);
	//	driver.findElement(By.xpath(loginButton)).click();
	}
	
	public void loginClean() {
		driver.findElement(By.id(loginFieldId)).clear();
		driver.findElement(By.id(passwordFieldId)).clear();
	}
	
	public void locatorsCheck(ArrayList<String> locators) {
		Assert.assertTrue(Misc.checklocators(locators, driver), "All locators should be present");
	}
	
	public void checkIfLoginSuccessful(String resultText) {
		Assert.assertTrue(resultText.contains("|  Organization Dashboard"), "Login is not successful");
	}
	
}
