package testsReports;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import eventDashboardPages.EventEdit;
import eventPages.Event;
import eventPages.EventDonation;
import eventPages.EventTeam;
import eventPages.PaymentInfo;
import organizationPages.ManageOrganizationDashboard;
import organizationPages.OrganizationDashboard;
import otherSitesPages.Twitter;
import pages.Login;
import pages.Menu;
import reportsPages.CampaignPageReport;
import reportsPages.ItemsAndPackageReport;
import reportsPages.Reports;
import reportsPages.SocialSharing;
import reportsPages.TransactionReport;
import utils.Data;
import utils.DriverSetup;
import utils.WindowHandler;

public class TestsEventReports {
	private WebDriver	driver;
	private String		username	= Data.userLogin;
	private String		password	= Data.userPassword;
	WindowHandler handler = new WindowHandler();
	
	
	@BeforeClass
	public void beforeClass() {
		driver = DriverSetup.getDriver("desktop");
		DriverSetup.initialSetup(driver);
	}
	
	@Test
	public void loginPage() {
		Login loginPage = new Login(driver);
		ArrayList<String> locators = pages.Login.getLocators();
		loginPage.locatorsCheck(locators);
	}

	@Test(dependsOnMethods = { "loginPage" }, groups = { "Profile" })
	public void login() {
		Login loginPage = new Login(driver);
		OrganizationDashboard result = loginPage.loginAsUser(username, password);
		loginPage.checkIfLoginSuccessful(result.getTitleText());
	}
	
	@Test(dependsOnMethods = { "login" })
	public void reportsDonationReport() {
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		Menu menu = dashboard.clickMenu();
		EventEdit msEdit = menu.clickEventFirstEvent();
		Event ms = msEdit.clickViewSite();		
		int beforeDonateValue = ms.getThermometrValue();
		EventDonation msDonation = ms.clickDonationButton();	
		msDonation.setDonationAmount(utils.Data.donationAmount);
		PaymentInfo payment = msDonation.clickCompleteCheckout();
		payment.paymentDataFilling();
		payment.acceptTermsAndconditions();
		payment.clickContinue();
		payment.clickCompleteButton();
		payment.checkThankYouMessage();
		payment.thankYouPageLocatorsCheck(); 
		ms.clickHomeButton();
		int afterDonateValue = ms.getThermometrValue();
		msEdit.switchToMainWindow();
		ms.checkDonateThermometr(beforeDonateValue, afterDonateValue);
		dashboard.clickMenu();
		ManageOrganizationDashboard manage = menu.manageTestOrganization();
		Reports reports = manage.clickReports();
		reports.clickDonationReport();
		reports.showEntries();
		reports.filterByDesktopDonation();
		reports.checkDonationReport(payment.getPaymentData(), reports.getDonationDataFromReport());
	}
	
	@Test(dependsOnMethods = { "login" })
	public void reportsItemsAndPackageCheckDrillDownSoldQuantity() {
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		Menu menu = dashboard.clickMenu();
		EventEdit msEdit = menu.clickEventFirstEvent();
		Event ms = msEdit.clickViewSite();		
		EventDonation msDonation = ms.clickRegistrationButton();
		msDonation.setRegItemQuantity();
		msDonation.addToCartRegItem();
		PaymentInfo payment = msDonation.clickCompleteCheckout();
		payment.paymentDataFilling();
		payment.acceptTermsAndconditions();
		payment.clickContinue();
		payment.clickCompleteButton();
		msEdit.switchToMainWindow();
		dashboard.clickMenu(); 
		ManageOrganizationDashboard manage = menu.manageTestOrganization();
		Reports reports = manage.clickReports();
		ItemsAndPackageReport itemRep = reports.clickItemAndPackageReport();
		int soldQuantity = itemRep.getRegItemSoldQuantity();
		itemRep.clickRegItemDrillDown();
		reports.showEntries();
		itemRep.checkSoldQuantity(soldQuantity, itemRep.getQuantitySoldSum());
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
	public void reportsTransactionReportCheckBuyRegItemRecord() {
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		Menu menu = dashboard.clickMenu();
		EventEdit msEdit = menu.clickEventFirstEvent();
		Event ms = msEdit.clickViewSite();		
		EventDonation msDonation = ms.clickRegistrationButton();
		msDonation.setRegItemQuantity();
		msDonation.addToCartRegItem();
		String itemName = msDonation.getRegItemName();
		PaymentInfo payment = msDonation.clickCompleteCheckout();
		payment.paymentDataFilling();
		payment.acceptTermsAndconditions();
		payment.clickContinue();
		String totalPaymentValue = payment.getTotalValue();
		payment.clickCompleteButton();
		ms.clickHomeButton();
		msEdit.switchToMainWindow();
		dashboard.clickMenu();
		ManageOrganizationDashboard manage = menu.manageTestOrganization();
		Reports reports = manage.clickReports();
		TransactionReport transaction = reports.clickTransactionReport();
		reports.showEntries();
		reports.filterByDesktopDonation();
		transaction.checkReportRecord(reports.getDonationDataFromReport(), itemName, totalPaymentValue);
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
	public void reportsTransactionReportCheckBuyDonationItemRecord() {
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		Menu menu = dashboard.clickMenu();
		EventEdit msEdit = menu.clickEventFirstEvent();
		Event ms = msEdit.clickViewSite();		
		EventDonation msDonation = ms.clickDonationButton();
		msDonation.setDonationItemQuantity();
		msDonation.addToCartDonationItem();
		String itemName = msDonation.getDonationItemName();
		PaymentInfo payment = msDonation.clickCompleteCheckout();
		payment.paymentDataFilling();
		payment.acceptTermsAndconditions();
		payment.clickContinue();
		String totalPaymentValue = payment.getTotalValue();
		payment.clickCompleteButton();
		ms.clickHomeButton();
		msEdit.switchToMainWindow();
		dashboard.clickMenu();
		ManageOrganizationDashboard manage = menu.manageTestOrganization();
		Reports reports = manage.clickReports();
		TransactionReport transaction = reports.clickTransactionReport();
		reports.showEntries();
		reports.filterByDesktopDonation();
		transaction.checkReportRecord(reports.getDonationDataFromReport(), itemName, totalPaymentValue);
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
	public void reportsTransactionReportCheckBuySponsorItemRecord() {
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		Menu menu = dashboard.clickMenu();
		EventEdit msEdit = menu.clickEventFirstEvent();
		Event ms = msEdit.clickViewSite();		
		EventDonation msDonation = ms.clickSponsorButton();
		msDonation.setSponsorItemQuantity();
		msDonation.addToCartSponsorItem();
		String itemName = msDonation.getSponsorItemName();
		PaymentInfo payment = msDonation.clickCompleteCheckout();
		payment.paymentDataFilling();
		payment.acceptTermsAndconditions();
		payment.clickContinue();
		String totalPaymentValue = payment.getTotalValue();
		payment.clickCompleteButton();
		ms.clickHomeButton();
		msEdit.switchToMainWindow();
		dashboard.clickMenu();
		ManageOrganizationDashboard manage = menu.manageTestOrganization();
		Reports reports = manage.clickReports();
		TransactionReport transaction = reports.clickTransactionReport();
		reports.showEntries();
		reports.filterByDesktopDonation();
		transaction.checkReportRecord(reports.getDonationDataFromReport(), itemName, totalPaymentValue);
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
	public void reportsTransactionReportCheckStandartDonationRecord() {
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		Menu menu = dashboard.clickMenu();
		EventEdit msEdit = menu.clickEventFirstEvent();
		Event ms = msEdit.clickViewSite();		
		EventDonation msDonation = ms.clickDonationButton();	
		msDonation.setDonationAmount(utils.Data.donationAmount);
		PaymentInfo payment = msDonation.clickCompleteCheckout();
		payment.paymentDataFilling();
		payment.acceptTermsAndconditions();
		payment.clickContinue();
		String totalPaymentValue = payment.getTotalValue();
		payment.clickCompleteButton();
		msEdit.switchToMainWindow();
		dashboard.clickMenu();
		ManageOrganizationDashboard manage = menu.manageTestOrganization();
		Reports reports = manage.clickReports();
		TransactionReport transaction = reports.clickTransactionReport();
		reports.showEntries();
		reports.filterByDesktopDonation();
		transaction.checkReportRecord(reports.getDonationDataFromReport(), "Standard Donation", totalPaymentValue);
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
	public void reportsCampaignPageReportCheckMasterCampaignRaisedAfterDonation() {
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		Menu menu = dashboard.clickMenu();
		ManageOrganizationDashboard manage = menu.manageTestOrganization();
		Reports reports = manage.clickReports();
		CampaignPageReport campaign = reports.clickCampaignPageReport();
		int beforeDonate  = campaign.getMasterCampaignRaisedValue();
		dashboard.clickMenu();
		EventEdit msEdit = menu.clickEventFirstEvent();
		Event ms = msEdit.clickViewSite();		
		EventDonation msDonation = ms.clickDonationButton();	
		msDonation.setDonationAmount(utils.Data.donationAmount);
		PaymentInfo payment = msDonation.clickCompleteCheckout();
		payment.paymentDataFilling();
		payment.acceptTermsAndconditions();
		payment.clickContinue();
		payment.clickCompleteButton();
		msEdit.switchToMainWindow();
		dashboard.clickMenu();
		menu.manageTestOrganization();
		manage.clickReports();
		reports.clickCampaignPageReport();
		reports.showEntries();
		int afterDonate = campaign.getMasterCampaignRaisedValue();
		campaign.checkRaisedValueAfterDonation(beforeDonate, afterDonate);
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
	public void reportsCampaignPageReportTeamRaisedAfterBuyRegItem() {
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		Menu menu = dashboard.clickMenu();
		ManageOrganizationDashboard manage = menu.manageTestOrganization();
		Reports reports = manage.clickReports();
		CampaignPageReport campaign = reports.clickCampaignPageReport();
		campaign.filterByTeam();
		int beforeDonate  = campaign.getMasterCampaignRaisedValue();
		dashboard.clickMenu();
		EventEdit msEdit = menu.clickEventFirstEvent();
		Event ms = msEdit.clickViewSite();
		ms.goToTeamPage();
		EventDonation msDonation = ms.clickRegistrationButton();
		int regPrice = msDonation.getRegItemPrice();
		msDonation.setRegItemQuantity();
		msDonation.addToCartRegItem();
		PaymentInfo payment = msDonation.clickCompleteCheckout();
		payment.paymentDataFilling();
		payment.acceptTermsAndconditions();
		payment.clickContinue();
		payment.clickCompleteButton();
		msEdit.switchToMainWindow();
		dashboard.clickMenu();
		menu.manageTestOrganization();
		manage.clickReports();
		reports.clickCampaignPageReport();
		campaign.filterByTeam();
		int afterDonate = campaign.getMasterCampaignRaisedValue();
		campaign.checkRaisedValueAfterBuyingItems(beforeDonate, afterDonate, regPrice);
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
	public void reportsCampaignPageReportIndividualRaisedAfterBuyRegItem() {
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		Menu menu = dashboard.clickMenu();
		ManageOrganizationDashboard manage = menu.manageTestOrganization();
		Reports reports = manage.clickReports();
		CampaignPageReport campaign = reports.clickCampaignPageReport();
		campaign.filterPageId("1362146005789");
		int beforeDonate  = campaign.getMasterCampaignRaisedValue();
		dashboard.clickMenu();
		EventEdit msEdit = menu.clickEventFirstEvent();
		Event ms = msEdit.clickViewSite();
		EventTeam msTeam = ms.goToTeamPage();
		msTeam.clickTeamTab();
		msTeam.goToTeamMemberPage();
		EventDonation msDonation = ms.clickRegistrationButton();
		int regPrice = msDonation.getRegItemPrice();
		msDonation.setRegItemQuantity();
		msDonation.addToCartRegItem();
		PaymentInfo payment = msDonation.clickCompleteCheckout();
		payment.paymentDataFilling();
		payment.acceptTermsAndconditions();
		payment.clickContinue();
		payment.clickCompleteButton();
		msEdit.switchToMainWindow();
		dashboard.clickMenu();
		menu.manageTestOrganization();
		manage.clickReports();
		reports.clickCampaignPageReport();
		campaign.filterPageId("1362146005789");
		int afterDonate = campaign.getMasterCampaignRaisedValue();
		campaign.checkRaisedValueAfterBuyingItems(beforeDonate, afterDonate, regPrice);
	}
	
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
	public void reportsCampaignPageReportIndividualRaisedAfterBuyRegItemWithPromoCode() {
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		Menu menu = dashboard.clickMenu();
		ManageOrganizationDashboard manage = menu.manageTestOrganization();
		Reports reports = manage.clickReports();
		CampaignPageReport campaign = reports.clickCampaignPageReport();
		//campaign.filterByIndividual();
		campaign.filterPageId("1362146005789");
		int beforeDonate  = campaign.getMasterCampaignRaisedValue();
		dashboard.clickMenu();
		EventEdit msEdit = menu.clickEventFirstEvent();
		Event ms = msEdit.clickViewSite();
		EventTeam msTeam = ms.goToTeamPage();
		msTeam.clickTeamTab();
		msTeam.goToTeamMemberPage();
		EventDonation msDonation = ms.clickRegistrationButton();
		int regPrice = msDonation.getRegItemPrice();
		msDonation.setRegItemQuantity();
		msDonation.addToCartRegItem();
		PaymentInfo payment = msDonation.clickCompleteCheckout();
		payment.paymentDataFilling();
		payment.setPromocode15();
		regPrice = regPrice - payment.getPromoCodeValue();	
		payment.acceptTermsAndconditions();
		payment.clickContinue();
		payment.clickCompleteButton();
		msEdit.switchToMainWindow();
		dashboard.clickMenu();
		menu.manageTestOrganization();
		manage.clickReports();
		reports.clickCampaignPageReport();	
		//campaign.filterByIndividual();
		campaign.filterPageId("1362146005789");
		int afterDonate = campaign.getMasterCampaignRaisedValue();
		campaign.checkRaisedValueAfterBuyingItems(beforeDonate, afterDonate, regPrice);
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
	public void reportsSocialSharingReportTwitter() {
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		Menu menu = dashboard.clickMenu();
		EventEdit msEdit = menu.clickEventFirstEvent();
		Twitter twitter = msEdit.clickPromoteOnTwitter();
		twitter.sendTwit();
		msEdit.switchToMainWindow();
		dashboard.clickMenu();
		ManageOrganizationDashboard manage = menu.manageTestOrganization();
		Reports reports = manage.clickReports();
		SocialSharing social = reports.clickSocialSharing();
		social.checkTwitterRecord(social.getType());
	}
	
	@AfterMethod
	public void setScreenshot(ITestResult result) {
		if (!result.isSuccess()){
			String methodName = result.getName();
			String filename = null;
			try
			{
				SimpleDateFormat formater = new SimpleDateFormat("yyyy_MM_dd_hh_mm_ss");
				File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
				filename = formater.format(Calendar.getInstance().getTime()) + "_" + methodName + ".png";
				String separator = System.getProperty("file.separator");
				String path = System.getProperty("user.dir") + separator + "test-output" + separator + "screens" + separator;
				FileUtils.copyFile(scrFile, new File(path + filename));
				Reporter.setCurrentTestResult(result);
				Reporter.log("<img width='100%' src='" + "screens" + separator + filename + "'/>");
			}
			catch (IOException e1)
			{
				e1.printStackTrace();
			}
			if (driver.getWindowHandles().size() > 1) {
				handler.switchToOldWindow(driver);
			}	
		}
	}

	@AfterClass
	public void afterClass() {
		try
		{
			driver.close();
			driver.quit();
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
	} 
}
