package testsReports;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import eventDashboardPages.EventEdit;
import organizationPages.ManageOrganizationDashboard;
import organizationPages.OrganizationDashboard;
import pages.Login;
import pages.Menu;
import pages.ShareViaEmail;
import reportsPages.CampaignPageReport;
import reportsPages.DonationReport;
import reportsPages.ItemsAndPackageReport;
import reportsPages.Reports;
import reportsPages.SocialSharing;
import reportsPages.TransactionReport;
import utils.Data;
import utils.DriverSetup;

public class TestsMainSiteReports {
	private WebDriver	driver;
	private String		username	= Data.userLogin;
	private String		password	= Data.userPassword;
	
	
	@BeforeClass
	public void beforeClass() {
		driver = DriverSetup.getDriver("desktop");
		DriverSetup.initialSetup(driver);
	}
	
	@Test
	public void loginPage() {
		Login loginPage = new Login(driver);
		ArrayList<String> locators = pages.Login.getLocators();
		loginPage.locatorsCheck(locators);
	}

	@Test(dependsOnMethods = { "loginPage" }, groups = { "Profile" })
	public void login() {
		Login loginPage = new Login(driver);
		OrganizationDashboard result = loginPage.loginAsUser(username, password);
		loginPage.checkIfLoginSuccessful(result.getTitleText());
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
	public void reportsDonationReportSortByRaisedCount() {
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		Menu menu = dashboard.clickMenu();
		ManageOrganizationDashboard manage = menu.manageTestOrganization();
		Reports reports = manage.clickReports();
		DonationReport donationRep = reports.clickDonationReport();
		donationRep.sortByRaisedCount();
		reports.checkSortingForIntegerValues(7);
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
	public void reportsDonationReportSortByFirstName() {
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		Menu menu = dashboard.clickMenu();
		ManageOrganizationDashboard manage = menu.manageTestOrganization();
		Reports reports = manage.clickReports();
		DonationReport donationRep = reports.clickDonationReport();
		donationRep.sortByFirstName();
		donationRep.sortByFirstName();
		reports.checkSorting(1);
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
	public void reportsDonationReportSortByLastName() {
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		Menu menu = dashboard.clickMenu();
		ManageOrganizationDashboard manage = menu.manageTestOrganization();
		Reports reports = manage.clickReports();
		DonationReport donationRep = reports.clickDonationReport();
		donationRep.sortByLastName();
		reports.checkSorting(2);
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
	public void reportsDonationReportSortByEmail() {
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		Menu menu = dashboard.clickMenu();
		ManageOrganizationDashboard manage = menu.manageTestOrganization();
		Reports reports = manage.clickReports();
		DonationReport donationRep = reports.clickDonationReport();
		donationRep.sortByEmail();
		reports.checkSorting(3);
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" }, enabled = false)
	public void reportsDonationReportSortBySsName() {
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		Menu menu = dashboard.clickMenu();
		ManageOrganizationDashboard manage = menu.manageTestOrganization();
		Reports reports = manage.clickReports();
		DonationReport donationRep = reports.clickDonationReport();
		donationRep.sortBySsName();
		reports.checkSorting(4);
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
	public void reportsDonationReportSortByMsName() {
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		Menu menu = dashboard.clickMenu();
		ManageOrganizationDashboard manage = menu.manageTestOrganization();
		Reports reports = manage.clickReports();
		DonationReport donationRep = reports.clickDonationReport();
		donationRep.sortByMsName();
		reports.checkSorting(5);
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
	public void reportsDonationReportSortByDonationDate() {
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		Menu menu = dashboard.clickMenu();
		ManageOrganizationDashboard manage = menu.manageTestOrganization();
		Reports reports = manage.clickReports();
		DonationReport donationRep = reports.clickDonationReport();
		donationRep.sortByDonationDate();
		reports.checkSorting(6);
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
	public void reportsDonationReportSortBySource() {
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		Menu menu = dashboard.clickMenu();
		ManageOrganizationDashboard manage = menu.manageTestOrganization();
		Reports reports = manage.clickReports();
		DonationReport donationRep = reports.clickDonationReport();
		donationRep.sortBySource();
		reports.checkSorting(8);
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
	public void reportsDonationReportSortByType() {
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		Menu menu = dashboard.clickMenu();
		ManageOrganizationDashboard manage = menu.manageTestOrganization();
		Reports reports = manage.clickReports();
		DonationReport donationRep = reports.clickDonationReport();
		donationRep.sortByType();
		reports.checkSorting(9);
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
	public void reportsDonationReportDownloadReport() throws Exception {
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		Menu menu = dashboard.clickMenu();
		ManageOrganizationDashboard manage = menu.manageTestOrganization();
		Reports reports = manage.clickReports();
		reports.clickDonationReport();
		reports.checkLinkStatus(reports.getDownloadLink2Status());
	} 
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
	public void reportsDonationReportSearchByType() {
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		Menu menu = dashboard.clickMenu();
		ManageOrganizationDashboard manage = menu.manageTestOrganization();
		Reports reports = manage.clickReports();
		reports.clickDonationReport();
		reports.searchBy("Individual");
		reports.checkDataAfterSearch(9, "Individual");
	}
	

	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
	public void reportsDonationReportSearchByName() {
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		Menu menu = dashboard.clickMenu();
		ManageOrganizationDashboard manage = menu.manageTestOrganization();
		Reports reports = manage.clickReports();
		reports.clickDonationReport();
		reports.searchBy("John");
		reports.checkDataAfterSearch(1, "John");
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
	public void reportsItemAndPackageSortByType() {
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		Menu menu = dashboard.clickMenu();
		ManageOrganizationDashboard manage = menu.manageTestOrganization();
		Reports reports = manage.clickReports();
		ItemsAndPackageReport itemRep = reports.clickItemAndPackageReport();
		itemRep.sortByType();
		reports.checkSorting(4);
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
	public void reportsItemAndPackageSortByItemType() {
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		Menu menu = dashboard.clickMenu();
		ManageOrganizationDashboard manage = menu.manageTestOrganization();
		Reports reports = manage.clickReports();
		ItemsAndPackageReport itemRep = reports.clickItemAndPackageReport();
		itemRep.sortByItemType();
		reports.checkSorting(5);
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
	public void reportsItemAndPackageSortByAvailableQuantity() {
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		Menu menu = dashboard.clickMenu();
		ManageOrganizationDashboard manage = menu.manageTestOrganization();
		Reports reports = manage.clickReports();
		ItemsAndPackageReport itemRep = reports.clickItemAndPackageReport();
		itemRep.sortByAvailableQuant();
		reports.checkSorting(6);
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
	public void reportsItemAndPackageSortBySoldQuantity() {
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		Menu menu = dashboard.clickMenu();
		ManageOrganizationDashboard manage = menu.manageTestOrganization();
		Reports reports = manage.clickReports();
		ItemsAndPackageReport itemRep = reports.clickItemAndPackageReport();
		itemRep.sortByQuantitySold();
		reports.checkSorting(7);
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
	public void reportsItemsAndPackageReportSearchByType() {
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		Menu menu = dashboard.clickMenu();
		ManageOrganizationDashboard manage = menu.manageTestOrganization();
		Reports reports = manage.clickReports();
		reports.clickItemAndPackageReport();
		reports.searchBy("Registration");
		reports.checkDataAfterSearch(4, "Registration");
	}
	

	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
	public void reportsItemsAndPackageReportSearchByMsName() {
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		Menu menu = dashboard.clickMenu();
		ManageOrganizationDashboard manage = menu.manageTestOrganization();
		Reports reports = manage.clickReports();
		reports.clickItemAndPackageReport();
		reports.searchBy("FirstEvent");
		reports.checkDataAfterSearch(2, "FirstEvent");
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
	public void reportsItemsAndPackageReportDownloadReport() throws Exception {
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		Menu menu = dashboard.clickMenu();
		ManageOrganizationDashboard manage = menu.manageTestOrganization();
		Reports reports = manage.clickReports();
		reports.clickItemAndPackageReport();
		reports.checkLinkStatus(reports.getDownloadLink2Status());
	} 
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
	public void reportsItemsAndPackageReportCheckDownloadLinks() throws Exception {
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		Menu menu = dashboard.clickMenu();
		ManageOrganizationDashboard manage = menu.manageTestOrganization();
		Reports reports = manage.clickReports();
		ItemsAndPackageReport itemRep = reports.clickItemAndPackageReport();
		itemRep.checkDownloadLinks();
	} 
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
	public void reportsItemsAndPackageReportCheckDrillDownLinks() throws Exception {
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		Menu menu = dashboard.clickMenu();
		ManageOrganizationDashboard manage = menu.manageTestOrganization();
		Reports reports = manage.clickReports();
		ItemsAndPackageReport itemRep = reports.clickItemAndPackageReport();
		itemRep.checkDrillDownLinks();
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
	public void reportsTransactionReportSortByTransactionId() {
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		Menu menu = dashboard.clickMenu();
		ManageOrganizationDashboard manage = menu.manageTestOrganization();
		Reports reports = manage.clickReports();
		TransactionReport transaction = reports.clickTransactionReport();
		transaction.sortByTransactionId();
		transaction.sortByTransactionId();
		reports.checkSorting(1);
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" }, enabled = false)
	public void reportsTransactionReportSortByMsName() {
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		Menu menu = dashboard.clickMenu();
		ManageOrganizationDashboard manage = menu.manageTestOrganization();
		Reports reports = manage.clickReports();
		TransactionReport transaction = reports.clickTransactionReport();
		transaction.sortByMsName();
		reports.checkSorting(2);
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" }, enabled = false)
	public void reportsTransactionReportSortBySsName() {
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		Menu menu = dashboard.clickMenu();
		ManageOrganizationDashboard manage = menu.manageTestOrganization();
		Reports reports = manage.clickReports();
		TransactionReport transaction = reports.clickTransactionReport();
		transaction.sortBySsName();
		reports.checkSorting(3);
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
	public void reportsTransactionReportSortByFirstName() {
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		Menu menu = dashboard.clickMenu();
		ManageOrganizationDashboard manage = menu.manageTestOrganization();
		Reports reports = manage.clickReports();
		TransactionReport transaction = reports.clickTransactionReport();
		transaction.sortByFirstName();
		reports.checkSorting(4);
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" }, enabled = false)
	public void reportsTransactionReportSortByLastName() {
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		Menu menu = dashboard.clickMenu();
		ManageOrganizationDashboard manage = menu.manageTestOrganization();
		Reports reports = manage.clickReports();
		TransactionReport transaction = reports.clickTransactionReport();
		transaction.sortByLastName();
		reports.checkSorting(5);
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" }, enabled = false)
	public void reportsTransactionReportSortByEmail() {
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		Menu menu = dashboard.clickMenu();
		ManageOrganizationDashboard manage = menu.manageTestOrganization();
		Reports reports = manage.clickReports();
		TransactionReport transaction = reports.clickTransactionReport();
		transaction.sortByEmail();
		reports.checkSorting(6);
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
	public void reportsTransactionReportSortByDate() {
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		Menu menu = dashboard.clickMenu();
		ManageOrganizationDashboard manage = menu.manageTestOrganization();
		Reports reports = manage.clickReports();
		TransactionReport transaction = reports.clickTransactionReport();
		transaction.sortByDate();
		reports.checkSorting(7);
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
	public void reportsTransactionReportSortByValue() {
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		Menu menu = dashboard.clickMenu();
		ManageOrganizationDashboard manage = menu.manageTestOrganization();
		Reports reports = manage.clickReports();
		TransactionReport transaction = reports.clickTransactionReport();
		transaction.sortByValue();
		reports.checkSortingForIntegerValues(8);
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" }, enabled = false)
	public void reportsTransactionReportSortByItemName() {
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		Menu menu = dashboard.clickMenu();
		ManageOrganizationDashboard manage = menu.manageTestOrganization();
		Reports reports = manage.clickReports();
		TransactionReport transaction = reports.clickTransactionReport();
		transaction.sortByItemName();
		reports.checkSorting(10);
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" }, enabled = false)
	public void reportsTransactionReportSortBySolicitorId() {
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		Menu menu = dashboard.clickMenu();
		ManageOrganizationDashboard manage = menu.manageTestOrganization();
		Reports reports = manage.clickReports();
		TransactionReport transaction = reports.clickTransactionReport();
		transaction.sortBySolicitorId();
		reports.checkSorting(11);
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" }, enabled = false)
	public void reportsTransactionnReportSearchByMsName() {
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		Menu menu = dashboard.clickMenu();
		ManageOrganizationDashboard manage = menu.manageTestOrganization();
		Reports reports = manage.clickReports();
		reports.clickTransactionReport();
		reports.searchBy("AnotherOneEvenet");
		reports.checkDataAfterSearch(2, "AnotherOneEvenet");
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" }, enabled = false)
	public void reportsTransactionReportSearchBySsName() {
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		Menu menu = dashboard.clickMenu();
		ManageOrganizationDashboard manage = menu.manageTestOrganization();
		Reports reports = manage.clickReports();
		reports.clickTransactionReport();
		reports.searchBy("SecondSupersite");
		reports.checkDataAfterSearch(3, "SecondSupersite");
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
	public void reportsTransactionReportSearchByFirstName() {
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		Menu menu = dashboard.clickMenu();
		ManageOrganizationDashboard manage = menu.manageTestOrganization();
		Reports reports = manage.clickReports();
		reports.clickTransactionReport();
		reports.searchBy("Ron");
		reports.checkDataAfterSearch(4, "Ron");
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
	public void reportsTransactionReportFilterByDate() {
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		Menu menu = dashboard.clickMenu();
		ManageOrganizationDashboard manage = menu.manageTestOrganization();
		Reports reports = manage.clickReports();
		TransactionReport transaction = reports.clickTransactionReport();
		transaction.setDataRange("09/01/2015");
		reports.checkDataAfterSearch(7, "09-01-2015");
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
	public void reportsTransactionReportDownloadReport() throws Exception {
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		Menu menu = dashboard.clickMenu();
		ManageOrganizationDashboard manage = menu.manageTestOrganization();
		Reports reports = manage.clickReports();
		reports.clickTransactionReport();
		reports.checkLinkStatus(reports.getDownloadLink2Status());
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
	public void reportsCampaignPageReportSortByFirstName() {
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		Menu menu = dashboard.clickMenu();
		ManageOrganizationDashboard manage = menu.manageTestOrganization();
		Reports reports = manage.clickReports();
		CampaignPageReport campaign = reports.clickCampaignPageReport();
		campaign.sortByFirstName();
		campaign.sortByFirstName();
		reports.checkSorting(1);
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
	public void reportsCampaignPageReportSortByLastName() {
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		Menu menu = dashboard.clickMenu();
		ManageOrganizationDashboard manage = menu.manageTestOrganization();
		Reports reports = manage.clickReports();
		CampaignPageReport campaign = reports.clickCampaignPageReport();
		campaign.sortByLastName();
		reports.checkSorting(2);
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
	public void reportsCampaignPageReportSortByEmail() {
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		Menu menu = dashboard.clickMenu();
		ManageOrganizationDashboard manage = menu.manageTestOrganization();
		Reports reports = manage.clickReports();
		CampaignPageReport campaign = reports.clickCampaignPageReport();
		campaign.sortByEmail();
		reports.checkSorting(3);
	}
	
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" }, enabled = false)
	public void reportsCampaignPageReportSortBySsName() {
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		Menu menu = dashboard.clickMenu();
		ManageOrganizationDashboard manage = menu.manageTestOrganization();
		Reports reports = manage.clickReports();
		CampaignPageReport campaign = reports.clickCampaignPageReport();
		campaign.sortBySsName();
		reports.checkSorting(4);
	}

	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
	public void reportsCampaignPageReportSortBySsId() {
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		Menu menu = dashboard.clickMenu();
		ManageOrganizationDashboard manage = menu.manageTestOrganization();
		Reports reports = manage.clickReports();
		CampaignPageReport campaign = reports.clickCampaignPageReport();
		campaign.sortBySsId();
		reports.checkSorting(5);
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
	public void reportsCampaignPageReportSortByPageType() {
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		Menu menu = dashboard.clickMenu();
		ManageOrganizationDashboard manage = menu.manageTestOrganization();
		Reports reports = manage.clickReports();
		CampaignPageReport campaign = reports.clickCampaignPageReport();
		campaign.sortByPageType();
		reports.checkSorting(6);
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
	public void reportsCampaignPageReportSortByPageId() {
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		Menu menu = dashboard.clickMenu();
		ManageOrganizationDashboard manage = menu.manageTestOrganization();
		Reports reports = manage.clickReports();
		CampaignPageReport campaign = reports.clickCampaignPageReport();
		campaign.sortByPageId();
		reports.checkSorting(7);
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" }, enabled = false)
	public void reportsCampaignPageReportSortByRaised() {
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		Menu menu = dashboard.clickMenu();
		ManageOrganizationDashboard manage = menu.manageTestOrganization();
		Reports reports = manage.clickReports();
		CampaignPageReport campaign = reports.clickCampaignPageReport();
		campaign.sortByRaised();
		reports.checkSortingForIntegerValues(8);
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
	public void reportsCampaignPageReportSortByGoal() {
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		Menu menu = dashboard.clickMenu();
		ManageOrganizationDashboard manage = menu.manageTestOrganization();
		Reports reports = manage.clickReports();
		CampaignPageReport campaign = reports.clickCampaignPageReport();
		campaign.sortByGoal();
		reports.checkSortingForIntegerValues(9);
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
	public void reportsCampaignPageReportSearchByPageType() {
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		Menu menu = dashboard.clickMenu();
		ManageOrganizationDashboard manage = menu.manageTestOrganization();
		Reports reports = manage.clickReports();
		reports.clickCampaignPageReport();
		reports.searchBy("Master");
		reports.checkDataAfterSearch(6, "Master");
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
	public void reportsCampaignPageReportSearchByFirstName() {
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		Menu menu = dashboard.clickMenu();
		ManageOrganizationDashboard manage = menu.manageTestOrganization();
		Reports reports = manage.clickReports();
		reports.clickCampaignPageReport();
		reports.searchBy("Ron");
		reports.checkDataAfterSearch(1, "Ron");
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
	public void reportsCampaignPageReportDownloadReport() throws Exception {
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		Menu menu = dashboard.clickMenu();
		ManageOrganizationDashboard manage = menu.manageTestOrganization();
		Reports reports = manage.clickReports();
		reports.clickCampaignPageReport();
		reports.checkLinkStatus(reports.getDownloadLink2Status());
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
	public void reportsSocialSharingReportDownloadReport() throws Exception {
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		Menu menu = dashboard.clickMenu();
		ManageOrganizationDashboard manage = menu.manageTestOrganization();
		Reports reports = manage.clickReports();
		reports.clickSocialSharing();
		reports.checkLinkStatus(reports.getDownloadLink2Status());
	}
	

	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
	public void reportsSocialSharingReportEmail() {
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		Menu menu = dashboard.clickMenu();
		EventEdit msEdit = menu.clickEventFirstEvent();
		ShareViaEmail email = msEdit.clickShareViaEmail();
		email.sendEmail();
		dashboard.clickMenu();
		ManageOrganizationDashboard manage = menu.manageTestOrganization();
		Reports reports = manage.clickReports();
		SocialSharing social = reports.clickSocialSharing();
		social.checkEmailRecord(social.getType());
	}
	
	@AfterMethod
	public void setScreenshot(ITestResult result) {
		
		if (!result.isSuccess()){
			String methodName = result.getName();
			String filename = null;
			try
			{
				SimpleDateFormat formater = new SimpleDateFormat("yyyy_MM_dd_hh_mm_ss");
				File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
				filename = formater.format(Calendar.getInstance().getTime()) + "_" + methodName + ".png";
				String separator = System.getProperty("file.separator");
				String path = System.getProperty("user.dir") + separator + "test-output" + separator + "screens" + separator;
				FileUtils.copyFile(scrFile, new File(path + filename));
				Reporter.setCurrentTestResult(result);
				Reporter.log("<img width='100%' src='" + "screens" + separator + filename + "'/>");
				
			}
			catch (IOException e1)
			{
				e1.printStackTrace();
			}
		}
	}

	@AfterClass
	public void afterClass() {
		try
		{
			driver.close();
			driver.quit();
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
	} 
}
