package organizationPages;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.Reporter;

import pages.AccountSettings;
import pages.Help;
import pages.Menu;
import utils.DriverUtils;
import utils.Misc;
import utils.WaitTool;
import utils.WebTable;



public class OrganizationDashboard {
	private WebDriver			driver;
	private static final int WAIT_4_ELEMENT		= WaitTool.DEFAULT_WAIT_4_ELEMENT;
	public static String	PAGE_URL			= utils.Data.baseURL + "/auth/member/digest/my/solutions.do";
	
	private static String titleXpath			= "/html/body/div[3]/div";
	private static String userNameXpath			= "/html/body/div[1]/div/div/div[1]/div[1]";
	private static String accountSettingsXpath	= "/html/body/div[1]/div/div/div[1]/div[2]/div[1]";
	private static String signOutXpath			= "/html/body/div[1]/div/div/div[1]/div[2]/div[2]";
	private static String solutionTable			= "//*[@id='sites-table']";
	private static String sortByName			= "//*[@id='sites-table']/thead/tr/th[1]";
	private static String sortByRaised			= "//*[@id='sites-table']/thead/tr/th[2]";
	private static String sortByMsCount			= "//*[@id='sites-table']/thead/tr/th[3]";
	private static String sortBySsCount			= "//*[@id='sites-table']/thead/tr/th[4]";
	private static String sortByCreateDate		= "//*[@id='sites-table']/thead/tr/th[5]";
	private static String searchInput			= "//*[@id='sites-table_filter']/label/input";
	private static String selectEntries			= "//*[@id='sites-table_length']/label/select";
	private static String createOrganization	= "//*[@id='micro-lic-popup']";
	private static String overviewNumber		= "numbers";
	private static String helpClass				= "open-help";
	private static String solutionCountXpath	= "//*[@id='siteWidth']/div[1]/div[1]/div[2]/h2";
	private static String supersiteCountXpath	= "//*[@id='siteWidth']/div[1]/div[2]/div[3]/h2/span";
	private static String mainMenuIdSelector	= "main_menu_icon";
	
	private static By manageLinkTextLocator 	= By.linkText("Manage");
	private static By siteTableIdLocator		= By.id("sites-table");
	
	
	public OrganizationDashboard(WebDriver driver) {
		this.driver = driver;
		if (!isAt())
		{
			Menu menu = clickMenu();
			menu.clickOrganizationDashboard();
		}
	}
	
	public boolean isAt() {
			return driver.getCurrentUrl().equals(PAGE_URL);
	}
	
	public static ArrayList<String> getLocators() {
		ArrayList<String> locators = new ArrayList<String>();
		locators.add("xpath," + solutionTable + ", Organization table");
		locators.add("xpath," + sortByName + ", Sort by name");
		locators.add("xpath," + sortByRaised + ", Sort by Raised");
		locators.add("xpath," + sortByMsCount + ", Sort by Events count");
		locators.add("xpath," + sortBySsCount + ", Sort by Suprsites count");
		locators.add("xpath," + sortByCreateDate + ", Sort by create date");
		locators.add("xpath," + searchInput + ", Search input");
		locators.add("xpath," + selectEntries + ", Select entries");
		locators.add("xpath," + createOrganization + ", Create solution button");
		return locators;
	}
	
	public Menu clickMenu() {
		if(DriverUtils.checkBrowser(driver).equals("chrome")){
			JavascriptExecutor jse = (JavascriptExecutor)driver;
			jse.executeScript("document.getElementById('" + mainMenuIdSelector + "').click();");
		}else {
			WaitTool.waitForElement(driver, By.id(mainMenuIdSelector), WAIT_4_ELEMENT).click();
		}
		WaitTool.waitForJQueryProcessing(driver, 5);
		return new Menu(driver);
	}
	
	
	public String getTitleText() {
		return driver.findElement(By.xpath(titleXpath)).getText();
	}
	
	public String getUserName() {
		return driver.findElement(By.xpath(userNameXpath)).getText();
	}
	
	public void clickOnDropdownMenu () {
		DriverUtils.click(By.xpath(userNameXpath), driver);
		utils.WaitTool.waitForJQueryProcessing(driver, 5);
	}
	
	public AccountSettings clickOnAccountSettings() {
		clickOnDropdownMenu();
		DriverUtils.click(By.xpath(accountSettingsXpath), driver);
		utils.WaitTool.waitForJQueryProcessing(driver, 5);
		return new AccountSettings(driver);
	}
	
	public void clickManageOnFirstElement(){
		WebElement eventElement = WebTable.getFirstElementFromColumnAsWebElementByIdLocator(siteTableIdLocator, 1, driver);
		Actions builder = new Actions(driver);
		builder.moveToElement(eventElement).perform();		
		WaitTool.waitForElement(driver, manageLinkTextLocator, WAIT_4_ELEMENT).click();
	}
	
	public void showEntries() {
		new Select(driver.findElement(By.xpath(selectEntries))).selectByVisibleText("25");
	}
	
	public void searchBy(String searchValue) {
		searchClear();
		driver.findElement(By.xpath(searchInput)).sendKeys(searchValue);
		utils.WaitTool.waitForJQueryProcessing(driver, 5);
	}
	
	public void searchOnUserTable(String searchValue){
		WebElement usersTableFilterElement = WaitTool.waitForElement(driver, By.id("usersTable_filter"), WAIT_4_ELEMENT);
		usersTableFilterElement.findElement(By.tagName("input")).sendKeys(searchValue);
		WaitTool.waitForJQueryProcessing(driver, WAIT_4_ELEMENT);
	}
	
	public CreateOrganization clickCreateOrganization() {
		DriverUtils.click(By.xpath(createOrganization), driver);
		return new CreateOrganization(driver);
	}
	

	public void searchClear() {
		driver.findElement(By.xpath(searchInput)).clear();
		driver.findElement(By.xpath(searchInput)).sendKeys(Keys.ENTER);
		utils.WaitTool.waitForJQueryProcessing(driver, 5);
	}
	
	
	public void signOut() {
		clickOnDropdownMenu();
		DriverUtils.click(By.xpath(signOutXpath), driver);
		WaitTool.waitForJQueryProcessing(driver, 5);
	}
	
	public void sortByName() {
		DriverUtils.click(By.xpath(sortByName), driver);
		utils.WaitTool.waitForJQueryProcessing(driver, 5);
	}
	
	public void sortByRaisedCount() {
		DriverUtils.click(By.xpath(sortByRaised), driver);
		utils.WaitTool.waitForJQueryProcessing(driver, 5);
	}
	
	public void sortByEventsCount() {
		DriverUtils.click(By.xpath(sortByMsCount), driver);
		utils.WaitTool.waitForJQueryProcessing(driver, 5);
	}
	
	public void sortBySupersitesCount() {
		DriverUtils.click(By.xpath(sortBySsCount), driver);
		utils.WaitTool.waitForJQueryProcessing(driver, 5);
	}
	
	public void sortByCreateDate() {
		DriverUtils.click(By.xpath(sortByCreateDate), driver);
		utils.WaitTool.waitForJQueryProcessing(driver, 5);
	}
	
	public int getTotalRaisedValue() {
		List<WebElement> overviewNumbers = driver.findElements(By.className(overviewNumber));
		String totalRaiseValue = overviewNumbers.get(0).getText();
		totalRaiseValue = totalRaiseValue.replaceAll(",", "");
		return Integer.parseInt((totalRaiseValue.substring(totalRaiseValue.indexOf("$") + 1)));
	}
	
	public int getSupersitesValue() {
		List<WebElement> overviewNumbers = driver.findElements(By.className(overviewNumber));
		String supersitesValue = overviewNumbers.get(1).getText();
		return Integer.parseInt(supersitesValue);
	}
	
	public int geEventsValue() {
		List<WebElement> overviewNumbers = driver.findElements(By.className(overviewNumber));
		String msValue = overviewNumbers.get(2).getText();
		return Integer.parseInt(msValue);
	}
	
	public int getPagesValue() {
		List<WebElement> overviewNumbers = driver.findElements(By.className(overviewNumber));
		String pagesValue = overviewNumbers.get(3).getText();
		return Integer.parseInt(pagesValue);
	}
	
	public int getOrganizationCountValue() {
		String solutionText = driver.findElement(By.xpath(solutionCountXpath)).getText();
		String solutionCount = solutionText.substring(solutionText.indexOf("(") + 1, solutionText.indexOf(")"));
		return Integer.parseInt(solutionCount);
	}
	
	public int getSupersiteCountValue() {
		String ssText = driver.findElement(By.xpath(supersiteCountXpath)).getText();
		String ssCount = ssText.substring(ssText.indexOf("(") + 1, ssText.indexOf(")"));
		return Integer.parseInt(ssCount);
	}
	
	public int getCountFromTableFirstColumn() {
		String[] solutionCountFromTable = WebTable.getDataFromColumn(solutionTable, 1, driver);
		return solutionCountFromTable.length;
	}
	
	public int getTotalRaisedValueFromTable() {
		String[] totalRaisedFromTable = WebTable.getDataFromColumn(solutionTable, 2, driver);
		for (int i = 0; i < totalRaisedFromTable.length; i++) {
			totalRaisedFromTable[i] = totalRaisedFromTable[i].replaceAll(",", "");
			
		}
		int[] results = new int[totalRaisedFromTable.length];
		int sum = 0;
		for (int i = 0; i < totalRaisedFromTable.length; i++) {
			results[i] = Integer.parseInt(totalRaisedFromTable[i].substring(totalRaisedFromTable[i].indexOf("$") + 1));
			sum = sum + results[i];
		}
		return sum;
	}
	
	public int getValueFromTable(int column) {
		String[] totalRaisedFromTable = WebTable.getDataFromColumn(solutionTable, column, driver);
		int[] results = new int[totalRaisedFromTable.length];
		int sum = 0;
		for (int i = 0; i < totalRaisedFromTable.length; i++) {
			results[i] = Integer.parseInt(totalRaisedFromTable[i]);
			sum = sum + results[i];
		}
		return sum;
	}
	
	public Help clickHelp() {
		DriverUtils.click(By.className(helpClass), driver);
		//driver.findElement(By.className(helpClass)).click();
		return new Help(driver);
	}
	
	public void checkOverviewNumbers(int valueFromOverviewNubmer, int valueFromTable) {
		Assert.assertTrue(valueFromOverviewNubmer == valueFromTable , "Something wrong with overview numbers");
	}
	
	public void checkDataAfterSearch(int column, String keyword) {
		String[] result = WebTable.getDataFromColumn(solutionTable, column, driver);
		for (int i = 0; i < result.length; i++)
		{
			if (!result[i].contains(keyword))
			{
				Reporter.log("'" + result[i] + "' does not contain keyword: '" + keyword + "'");
				Assert.fail("'" + result[i] + "' does not contain keyword: '" + keyword + "'");
                System.out.println(result[i]);
			}
		}
	}
	
	public void checkData(String solName) {
		String result = WebTable.getFirstElementFromColumn(solutionTable, 1, driver);
		Assert.assertTrue(solName.equals(result), "New solution doesn't contain expected name");
	}
	
	public void checkDataAfterError(String solName) {
		WebElement sitesTableFilterElement = WaitTool.waitForElement(driver, By.id("sites-table_filter"), WaitTool.DEFAULT_WAIT_4_ELEMENT);
		WebElement inputFilterElement = sitesTableFilterElement.findElement(By.cssSelector("input"));
		inputFilterElement.clear();
		inputFilterElement.sendKeys(utils.Data.organizationNameErr);
		String result = WebTable.getFirstElementFromColumn(solutionTable, 1, driver);
		Assert.assertTrue(solName.equals(result), "New solution doesn't contain expected name");
	}
	
	public void checkRenamedOrganization(String solName) {
		String[] result = WebTable.getDataFromColumn(solutionTable, 1, driver);
		Boolean checkResult = false;
		for(int i = 0; i < result.length; i++){
			if (result[i].equals(solName)) {
				checkResult = true;
			}
		}
		Assert.assertTrue(checkResult, "Renamed solution doesn't contains in solution table");
	}
	
	public void checkIntSortingForParsedValues(int column) {
		String[] data = WebTable.getDataFromColumn(solutionTable, column, driver);
		for (int i = 0; i < data.length; i++) {
			data[i] = data[i].replaceAll(",", "");
			
		}
		int[] results = new int[data.length];
		for (int i = 0; i < data.length; i++) {
			results[i] = Integer.parseInt(data[i].substring(data[i].indexOf("$") + 1));	
		}
		int[] initial = new int[results.length];
		System.arraycopy(results, 0, initial, 0, results.length);
		Arrays.sort(results);
		if (!Misc.intArraysEqual(initial, results)) {
			Reporter.log("Data are NOT sorted properly");
			Assert.fail("Data are NOT sorted properly");
		}
		else {
			Reporter.log("Data are sorted OK");
		}
	}
	
	
	
	public void checkSorting(int column) {
		String[] data = WebTable.getDataFromColumn(solutionTable, column, driver);
		String[] initial = new String[data.length];
		System.arraycopy(data, 0, initial, 0, data.length);
		Arrays.sort(data, String.CASE_INSENSITIVE_ORDER);
		if (!Misc.stringArraysEqual(initial, data)) {
			Reporter.log("Data are NOT sorted properly");
			Assert.fail("Data are NOT sorted properly");
		}
		else {
			Reporter.log("Data are sorted OK");
		}
	}
	
	public void checkNumbersSorting(int column) {
		int[] data = WebTable.getIntegerArrayDataFromColumn(solutionTable, column, driver);
		int[] initial = new int[data.length];
		System.arraycopy(data, 0, initial, 0, data.length);
		Arrays.sort(data);
		if (!Misc.intArraysEqual(initial, data)) {
			Reporter.log("Data are NOT sorted properly");
			Assert.fail("Data are NOT sorted properly");
		}
		else {
			Reporter.log("Data are sorted OK");
		}
	}
	
	public void checkIntSorting(int column) {
		String[] data = WebTable.getDataFromColumn(solutionTable, column, driver);
		int[] results = new int[data.length];
		for (int i = 0; i < data.length; i++) {
			results[i] = Integer.parseInt(data[i]);	
		}
		int[] initial = new int[data.length];
		System.arraycopy(results, 0, initial, 0, data.length);
		Arrays.sort(results);
		if (!Misc.intArraysEqual(initial, results)) {
			Reporter.log("Data are NOT sorted properly");
			Assert.fail("Data are NOT sorted properly");
		}
		else {
			Reporter.log("Data are sorted OK");
		}
	}
	
	public void locatorsCheck() {
		Assert.assertTrue(Misc.checklocators(getLocators(), driver), "All locators should be present");
	}

}
