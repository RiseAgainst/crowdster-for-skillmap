package organizationPages;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import utils.DriverUtils;

public class CreateOrganization {
	
	private WebDriver		driver;
	
	private static String errMsgValue				= "This field is required.";
	
	private static String solutionName				= "name";
	private static String createOrganizationButton	= "createSolutionBtn";
	private static String errMsg					= "//*[@id='createMicrositeForm']/table/tbody/tr[1]/td[2]/label";
	
	public CreateOrganization(WebDriver driver) {
		this.driver = driver;
	}
	
	public void setOrganizationName(String solName) {
		driver.findElement(By.id(solutionName)).sendKeys(solName);
	}
	
	public void clickCreateOrganization() {
		DriverUtils.click(By.id(createOrganizationButton), driver);
	}
	
	public String getErrorMessage() {
		return driver.findElement(By.xpath(errMsg)).getText();
	}
	
	public void checkErrorMessage() {
		Assert.assertTrue(getErrorMessage().equals(errMsgValue), "Wrong or missing error message");
	}
	
	
	

}
