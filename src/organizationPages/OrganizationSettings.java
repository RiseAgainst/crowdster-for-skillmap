package organizationPages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import utils.DriverUtils;
import utils.WaitTool;

public class OrganizationSettings {
	private WebDriver		driver;
	
	private static String successfullSaveMessage 	= "Your page was saved successfully.";
	private static String errMsgValue			= "This field is required.";
	
	private static String solutionNameInput	= "name";
	private static String submitButtonId	= "solutionSaveBtn";
	private static String saveMessageClass			= "message-popup";
	private static String errMsg				= "//*[@id='createMicrositeForm']/table/tbody/tr[1]/td[2]/label";
	
	
	
	public OrganizationSettings(WebDriver driver) {
		this.driver = driver;
	}
	
	public void clickSubmit() {
		DriverUtils.click(By.id(submitButtonId), driver);
		//driver.navigate().refresh();
		utils.WaitTool.waitForJQueryProcessing(driver, 5);
	}
	
	public void setOrganizationName(String name) {
		driver.findElement(By.name(solutionNameInput)).clear();
		driver.findElement(By.name(solutionNameInput)).sendKeys(name);
	}
	
	public String getErrorMessage() {
		return driver.findElement(By.xpath(errMsg)).getText();
	}
	
	public String getSaveMessage() {
		return driver.findElement(By.className(saveMessageClass)).getText();
	}
	
	//Asserts
	public void checkIfSaveWasSuccessfull() {
		Assert.assertTrue(getSaveMessage().equals(successfullSaveMessage), "Save goes wrong");
	}
	
	public void checkErrorMessage() {
		Assert.assertTrue(getErrorMessage().equals(errMsgValue), "Wrong or missing error message");
	}
	
	public void checkErrorMessageOnSettings(){
		WebElement errorElement = WaitTool.waitForElement(driver, By.className("error"), WaitTool.DEFAULT_WAIT_4_ELEMENT);
		Boolean isEnabled = errorElement.isEnabled();
		Assert.assertTrue(isEnabled, "Error message is missing.");
	}
	

}
