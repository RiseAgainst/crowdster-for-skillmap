package organizationPages;

import java.util.ArrayList;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import reportsPages.Reports;
import utils.DriverUtils;
import utils.Misc;

public class ManageOrganizationDashboard {
	
	private WebDriver		driver;
	
	private static String sortByMsCount			= "//*[@id='sites-table']/thead/tr/th[4]";
	private static String sortByPageCount	    = "//*[@id='sites-table']/thead/tr/th[3]";
	private static String solutionTable			= "//*[@id='sites-table']";
	private static String sortByName			= "//*[@id='sites-table']/thead/tr/th[1]";
	private static String sortByRaised			= "//*[@id='sites-table']/thead/tr/th[2]";
	private static String sortBySsCount			= "//*[@id='sites-table']/thead/tr/th[4]";
	private static String sortByCreateDate		= "//*[@id='sites-table']/thead/tr/th[5]";
	private static String searchInput			= "//*[@id='sites-table_filter']/label/input";
	private static String selectEntries			= "//*[@id='sites-table_length']/label/select";
	private static String createSupersite		= "//*[@id='siteWidth']/div[1]/div[2]/div[3]/h2/a";
	private static String solutionName			= "//*[@id='siteWidth']/div[1]/div[1]";
	private static String settingsButtonXpath	= "//*[@id='siteWidth']/div[1]/div[3]/div[2]/ul/li[2]/a";
	private static String reportsXpath			= "//*[@id='siteWidth']/div[1]/div[3]/div[2]/ul/li[3]/a";

	
	public ManageOrganizationDashboard(WebDriver driver) {
		this.driver = driver;
	}
	
	public static ArrayList<String> getLocators() {
		ArrayList<String> locators = new ArrayList<String>();
		locators.add("xpath," + solutionTable + ", Organization table");
		locators.add("xpath," + sortByName + ", Sort by name");
		locators.add("xpath," + sortByRaised + ", Sort by Raised");
		locators.add("xpath," + sortByMsCount + ", Sort by Events count");
		locators.add("xpath," + sortBySsCount + ", Sort by Suprsites count");
		locators.add("xpath," + sortByCreateDate + ", Sort by create date");
		locators.add("xpath," + searchInput + ", Search input");
		locators.add("xpath," + selectEntries + ", Select entries");
		locators.add("xpath," + createSupersite + ", Create supersite button");
		return locators;
	}
	
	public void sortByEventsCount() {
		DriverUtils.click(By.xpath(sortByMsCount), driver);
		utils.WaitTool.waitForJQueryProcessing(driver, 5);
	}
	
	public void sortByPageCount() {
		DriverUtils.click(By.xpath(sortByPageCount), driver);
		utils.WaitTool.waitForJQueryProcessing(driver, 5);
	}
	
	public OrganizationSettings clickSettings() {
		DriverUtils.click(By.xpath(settingsButtonXpath), driver);
		return new OrganizationSettings(driver);
	}
	
	public Reports clickReports() {
		DriverUtils.click(By.xpath(reportsXpath), driver);
		utils.WaitTool.waitForJQueryProcessing(driver, 5);
		return new Reports(driver);
	}
	
	
	public void locatorsCheck() {
		Assert.assertTrue(Misc.checklocators(getLocators(), driver), "All locators should be present");
	}
	
	public String getSolutioName() {
		return driver.findElement(By.xpath(solutionName)).getText();
	}
	
	public void checkName(String solName) {
		Assert.assertTrue(solName.equals(getSolutioName()), "Wrong solution name");
	}

}
