package tests;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import eventDashboardPages.EventDashboard;
import eventDashboardPages.ManageCampaignsConfig;
import organizationPages.OrganizationDashboard;
import pages.Login;
import utils.Data;
import utils.DriverSetup;
import utils.WindowHandler;

public class TestsManageCampaignsConfig {
	private WebDriver driver;
	private WindowHandler 	handler;
	private String username	= Data.userLogin;
	private String password	= Data.userPassword;
	
	@BeforeClass
	public void beforeClass() {
		driver = DriverSetup.getDriver("desktop");
		DriverSetup.initialSetup(driver);
		handler = new WindowHandler();
	}
	
	@Test
	public void loginPage() {
		Login loginPage = new Login(driver);
		ArrayList<String> locators = pages.Login.getLocators();
		loginPage.locatorsCheck(locators);
	}

	@Test(dependsOnMethods = { "loginPage" }, groups = { "Profile" })
	public void login() {
		Login loginPage = new Login(driver);
		OrganizationDashboard result = loginPage.loginAsUser(username, password);
		loginPage.checkIfLoginSuccessful(result.getTitleText());
	}
	
	@Test(dependsOnMethods = { "login" }, priority = -2)
	public void manageCampaignsConfiguration(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditFirstEvent();
		ManageCampaignsConfig manageCampConfig = dashboard.navigateToManageCampaignsConfiguration();
		dashboard.switchToFirstIframe();
		manageCampConfig.fillStatusUpdate();
		dashboard.switchToDefaultContent();
		dashboard.switchToSecondIframe();
		manageCampConfig.fillTheStory();
		dashboard.switchToDefaultContent();
		dashboard.switchToThirdIframe();
		manageCampConfig.fillDefaultTeamStory();
		dashboard.switchToDefaultContent();
		manageCampConfig.clickToggleButtons();
		manageCampConfig.setTeamImage();
		manageCampConfig.setTeamCampaignImage();
		manageCampConfig.setFinancialGoal();
		manageCampConfig.clickSaveBtn();
		manageCampConfig.checkSuccessMessage();
	}
	
	@Test(dependsOnMethods = { "login" }, priority = -1)
	public void checkStatusUpdateAndTheStory(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditFirstEvent();
		ManageCampaignsConfig manageCampConfig = dashboard.navigateToManageCampaignsConfiguration();
		dashboard.switchToEvent();
		manageCampConfig.clickJoinATeamBtn();
		manageCampConfig.setMemberInformation();
		manageCampConfig.clickViewMyPageBtn();
		manageCampConfig.checkStatusUpdate();
		manageCampConfig.checkTheStory();
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" })
	public void checkDefaultTeamStory(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditFirstEvent();
		ManageCampaignsConfig manageCampConfig = dashboard.navigateToManageCampaignsConfiguration();
		dashboard.switchToEvent();
		manageCampConfig.eventLogInIfNecessary();
		manageCampConfig.clickHome();
		manageCampConfig.clickStartATeamBtn();
		dashboard.switchToFirstIframe();
		manageCampConfig.checkCreateATeamDescritpion();
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" })
	public void checkTeamImage(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditFirstEvent();
		ManageCampaignsConfig manageCampConfig = dashboard.navigateToManageCampaignsConfiguration();
		dashboard.switchToEvent();
		manageCampConfig.eventLogInIfNecessary();
		manageCampConfig.clickHome();
		manageCampConfig.clickStartATeamBtn();
		manageCampConfig.checkTeamImage();
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" })
	public void checkTeamCampaignImage(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditFirstEvent();
		ManageCampaignsConfig manageCampConfig = dashboard.navigateToManageCampaignsConfiguration();
		dashboard.switchToEvent();
		manageCampConfig.eventLogInIfNecessary();
		manageCampConfig.clickHome();
		manageCampConfig.clickStartATeamBtn();
		manageCampConfig.checkTeamCampaignImage();
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" })
	public void checkFinancialGoal(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditFirstEvent();
		ManageCampaignsConfig manageCampConfig = dashboard.navigateToManageCampaignsConfiguration();
		String financialGoal = manageCampConfig.getFinancialGoal();
		dashboard.switchToEvent();
		manageCampConfig.eventLogInIfNecessary();
		manageCampConfig.clickHome();
		manageCampConfig.goToTestmemberPage();
		manageCampConfig.checkFinancialGoal(financialGoal);
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" })
	public void checkAllowedOfflineDonations(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditFirstEvent();
		ManageCampaignsConfig manageCampConfig = dashboard.navigateToManageCampaignsConfiguration();
		dashboard.switchToEvent();
		manageCampConfig.eventLogInIfNecessary();
		manageCampConfig.clickMHPOfflineDonations();
		int offlineDonationsCountBefore = manageCampConfig.getOfflineDonationsCount();
		manageCampConfig.clickAddOfflineDonations();
		manageCampConfig.fillOfflineDonation();
		manageCampConfig.clickAddDonorBtn();
		int offlineDonationsCountAfter = manageCampConfig.getOfflineDonationsCount();
		manageCampConfig.checkOfflineDonationsAfterCreate(offlineDonationsCountBefore, offlineDonationsCountAfter);
	}
	
	@AfterMethod
	public void setScreenshot(ITestResult result) {
		if (!result.isSuccess()){
			String methodName = result.getName();
			String filename = null;
			try
			{
				SimpleDateFormat formater = new SimpleDateFormat("yyyy_MM_dd_hh_mm_ss");
				File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
				filename = formater.format(Calendar.getInstance().getTime()) + "_" + methodName + ".png";
				String separator = System.getProperty("file.separator");
				String path = System.getProperty("user.dir") + separator + "test-output" + separator + "screens" + separator;
				FileUtils.copyFile(scrFile, new File(path + filename));
				Reporter.setCurrentTestResult(result);
				Reporter.log("<img width='100%' src='" + "screens" + separator + filename + "'/>");
			}
			catch (IOException e1)
			{
				e1.printStackTrace();
			}
			if (driver.getWindowHandles().size() > 1) {
				handler.switchToOldWindow(driver);
			}
		}
	}
	
	@AfterClass
	public void afterClass() {
		try
		{
			driver.close();
			driver.quit();
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
	} 
}
