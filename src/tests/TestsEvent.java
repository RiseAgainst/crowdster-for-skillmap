package tests;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import organizationPages.OrganizationDashboard;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;

import displayServices.Categories;
import displayServices.CreateCategory;
import displayServices.EventTemplates;
import eventDashboardPages.CreateEvent;
import eventDashboardPages.Items;
import eventDashboardPages.EventDashboard;
import eventDashboardPages.EventEdit;
import eventDashboardPages.HomepageDashboard;
import eventDashboardPages.NavigationPages;
import eventDashboardPages.WebsiteAppearance;
import eventDashboardPages.WebsiteNavigationPages;
import eventPages.Event;
import eventPages.EventDonation;
import eventPages.EventTeam;
import eventPages.NavigationPage;
import eventPages.PaymentInfo;
import pages.Login;
import pages.Menu;
import utils.Data;
import utils.DriverSetup;
import utils.Misc;
import utils.WindowHandler;


public class TestsEvent {
	
	private WebDriver	driver;
	private String		username	= Data.userLogin;
	private String		password	= Data.userPassword;
	private WindowHandler handler;
	
	@BeforeClass
	public void beforeClass() {
		driver = DriverSetup.getDriver("desktop");
		DriverSetup.initialSetup(driver);
		this.handler = new WindowHandler();
	}
	
	@Test
	public void loginPage() {
		Login loginPage = new Login(driver);
		ArrayList<String> locators = pages.Login.getLocators();
		loginPage.locatorsCheck(locators);
	}

	@Test(dependsOnMethods = { "loginPage" }, groups = { "Profile" })
	public void login() {
		Login loginPage = new Login(driver);
		OrganizationDashboard result = loginPage.loginAsUser(username, password);
		loginPage.checkIfLoginSuccessful(result.getTitleText());
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
	public void donationByDonationAmount() {	
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditFirstEvent();
		dashboard.switchToEvent();
		Event ms = new Event(driver);
		int beforeDonateValue = ms.getThermometrValue();
		EventDonation msDonation = ms.clickDonationButton();	
		msDonation.setDonationAmount(utils.Data.donationAmount);
		PaymentInfo payment = msDonation.clickCompleteCheckout();
		payment.paymentDataFilling();
		payment.acceptTermsAndconditions();
		payment.clickContinue();
		payment.clickCompleteButton();
		payment.checkThankYouMessage();
		payment.thankYouPageLocatorsCheck(); 
		ms.clickHomeButton();
		int afterDonateValue = ms.getThermometrValue();
		dashboard.switchToOldWindow();
		ms.checkDonateThermometr(beforeDonateValue, afterDonateValue);
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
	public void donationDonationAmoutLowerBoundary() {
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditFirstEvent();
		dashboard.switchToEvent();
		Event ms = new Event(driver);
		EventDonation msDonation = ms.clickDonationButton();	
		msDonation.setDonationAmount("1");
		msDonation.clickCompleteCheckout();
		msDonation.checkLowerBoundaryMessage();
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
	public void donationDonationAmoutUpperBoundary() {
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditFirstEvent();
		dashboard.switchToEvent();
		Event ms = new Event(driver);
		EventDonation msDonation = ms.clickDonationButton();	
		msDonation.setDonationAmount("1000000000");
		msDonation.clickCompleteCheckout();
		msDonation.checkUpperBoundaryMessage();
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
	public void donationBuyRegistrationItem() {	
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditFirstEvent();
		dashboard.switchToEvent();
		Event ms = new Event(driver);
		int beforeDonateValue = ms.getThermometrValue();
		EventDonation msDonation = ms.clickRegistrationButton();
		int regPrice = msDonation.getRegItemPrice();
		msDonation.setRegItemQuantity();
		msDonation.addToCartRegItem();
		PaymentInfo payment = msDonation.clickCompleteCheckout();
		payment.paymentDataFilling();
		payment.acceptTermsAndconditions();
		payment.clickContinue();
		payment.clickCompleteButton();
		payment.checkThankYouMessage();
		payment.thankYouPageLocatorsCheck();
		ms.clickHomeButton();
		int afterDonateValue = ms.getThermometrValue();
		dashboard.switchToOldWindow();
		ms.checkDonateThermometrAfterBuyingItems(beforeDonateValue, afterDonateValue, regPrice);
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
	public void donationBuySponsorItem() {	
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditFirstEvent();
		dashboard.switchToEvent();
		Event ms = new Event(driver);
		int beforeDonateValue = ms.getThermometrValue();
		EventDonation msDonation = ms.clickSponsorButton();
		int sponsorPrice = msDonation.getSponsorItemPrice();
		msDonation.setSponsorItemQuantity();
		msDonation.addToCartSponsorItem();
		PaymentInfo payment = msDonation.clickCompleteCheckout();
		payment.paymentDataFilling();
		payment.acceptTermsAndconditions();
		payment.clickContinue();
		payment.clickCompleteButton();
		payment.checkThankYouMessage();
		payment.thankYouPageLocatorsCheck();
		ms.clickHomeButton();
		int afterDonateValue = ms.getThermometrValue();
		dashboard.switchToOldWindow();
		ms.checkDonateThermometrAfterBuyingItems(beforeDonateValue, afterDonateValue, sponsorPrice);
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
	public void donationBuyDonationItem() {		
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditFirstEvent();
		dashboard.switchToEvent();
		Event ms = new Event(driver);
		int beforeDonateValue = ms.getThermometrValue();
		EventDonation msDonation = ms.clickDonationButton();
		int donationPrice = msDonation.getDonationItemPrice();
		msDonation.setDonationItemQuantity();
		msDonation.addToCartDonationItem();
		PaymentInfo payment = msDonation.clickCompleteCheckout();
		payment.paymentDataFilling();
		payment.acceptTermsAndconditions();
		payment.clickContinue();
		payment.clickCompleteButton();
		payment.checkThankYouMessage();
		payment.thankYouPageLocatorsCheck();
		ms.clickHomeButton();
		int afterDonateValue = ms.getThermometrValue();
		dashboard.switchToOldWindow();
		ms.checkDonateThermometrAfterBuyingItems(beforeDonateValue, afterDonateValue, donationPrice);
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
	public void donationDonateToTeam() {
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditFirstEvent();
		dashboard.switchToEvent();
		Event ms = new Event(driver);
		int beforeDonateValue = ms.getThermometrValue();
		EventTeam msTeam = ms.goToTeamPage();
		int beforeDonateTeamValue = msTeam.getTeamThermometrValue();
		ms.setTeamDonation();
		PaymentInfo payment = new PaymentInfo(driver);
		payment.paymentTeamDataFilling();
		payment.acceptTermsAndconditions();
		payment.clickContinue();
		payment.clickCompleteButton();
		payment.checkThankYouMessage();
		payment.thankYouPageLocatorsCheck(); 
		ms.clickHomeButton();
		int afterDonateValue = ms.getThermometrValue();
		ms.goToTeamPage();
		int afterDonateTeamValue = msTeam.getTeamThermometrValue();
		dashboard.switchToOldWindow();
		ms.checkDonateThermometr(beforeDonateValue, afterDonateValue);	
		ms.checkDonateThermometr(beforeDonateTeamValue, afterDonateTeamValue);
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
	public void donationDonateToTeamMember() {
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditFirstEvent();
		dashboard.switchToEvent();
		Event ms = new Event(driver);
		int beforeDonateValue = ms.getThermometrValue();
		EventTeam msTeam = ms.goToTeamPage();
		int beforeDonateTeamValue = msTeam.getTeamThermometrValue();
		msTeam.clickTeamTab();
		msTeam.goToTeamMemberPage();
		int beforeDonateTeamMemberValue	= msTeam.getTeamThermometrValue();
		ms.setTeamOwnerDonation();
		PaymentInfo payment = new PaymentInfo(driver);
		payment.paymentTeamDataFilling();
		payment.acceptTermsAndconditions();
		payment.clickContinue();
		payment.clickCompleteButton();
		payment.checkThankYouMessage();
		payment.thankYouPageLocatorsCheck(); 
		ms.clickHomeButton();
		int afterDonateValue = ms.getThermometrValue();
		ms.goToTeamPage();
		int afterDonateTeamValue = msTeam.getTeamThermometrValue();
		msTeam.clickTeamTab();
		msTeam.goToTeamMemberPage();
		int afterDonateTeamMemberValue = msTeam.getTeamThermometrValue();
		dashboard.switchToOldWindow();
		ms.checkDonateThermometr(beforeDonateValue, afterDonateValue);	
		ms.checkDonateThermometr(beforeDonateTeamValue, afterDonateTeamValue);
		ms.checkDonateThermometr(beforeDonateTeamMemberValue, afterDonateTeamMemberValue);
	}
	
	@Test(dependsOnMethods = { "login" })
	public void donationRegistrationItemLimit() {
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditFirstEvent();
		dashboard.switchToEvent();
		Event ms = new Event(driver);
		EventDonation msDonation = ms.clickRegistrationButton();
		msDonation.setRegLimitItemQuantity();
		msDonation.checkLimitMessage();
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" })
	public void donationBuyRegistrationItemWithPromoCode() {
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditFirstEvent();
		dashboard.switchToEvent();
		Event ms = new Event(driver);
		int beforeDonateValue = ms.getThermometrValue();
		EventDonation msDonation = ms.clickRegistrationButton();
		int regPrice = msDonation.getRegItemPrice();
		msDonation.setRegItemQuantity();
		msDonation.addToCartRegItem();
		PaymentInfo payment = msDonation.clickCompleteCheckout();
		payment.paymentDataFilling();
		payment.setPromocode15();
		regPrice = regPrice - payment.getPromoCodeValue();
		payment.acceptTermsAndconditions();
		payment.clickContinue();
		payment.clickCompleteButton();
		payment.checkThankYouMessage();
		payment.thankYouPageLocatorsCheck();
		ms.clickHomeButton();
		int afterDonateValue = ms.getThermometrValue();
		dashboard.switchToOldWindow();
		ms.checkDonateThermometrAfterBuyingItems(beforeDonateValue, afterDonateValue, regPrice);
	}
	
	@Test(dependsOnMethods = { "login" })
	public void donationBuyDonationItemWithPromoCode() {	
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditFirstEvent();
		dashboard.switchToEvent();
		Event ms = new Event(driver);
		int beforeDonateValue = ms.getThermometrValue();
		EventDonation msDonation = ms.clickDonationButton();
		int donationPrice = msDonation.getDonationItemPrice();
		msDonation.setDonationItemQuantity();
		msDonation.addToCartDonationItem();
		PaymentInfo payment = msDonation.clickCompleteCheckout();
		payment.paymentDataFilling();
		payment.setPromocode15();
		donationPrice = donationPrice - payment.getPromoCodeValue();
		payment.acceptTermsAndconditions();
		payment.clickContinue();
		payment.clickCompleteButton();
		payment.checkThankYouMessage();
		payment.thankYouPageLocatorsCheck();
		ms.clickHomeButton();
		int afterDonateValue = ms.getThermometrValue();
		dashboard.switchToOldWindow();
		ms.checkDonateThermometrAfterBuyingItems(beforeDonateValue, afterDonateValue, donationPrice);
	}
	
	@Test(dependsOnMethods = { "login" })
	public void donationBuySponsorItemWithPromoCode() {
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditFirstEvent();
		dashboard.switchToEvent();
		Event ms = new Event(driver);
		int beforeDonateValue = ms.getThermometrValue();
		EventDonation msDonation = ms.clickSponsorButton();
		int sponsorPrice = msDonation.getSponsorItemPrice();
		msDonation.setSponsorItemQuantity();
		msDonation.addToCartSponsorItem();
		PaymentInfo payment = msDonation.clickCompleteCheckout();
		payment.paymentDataFilling();
		payment.setPromocode15();
		sponsorPrice = sponsorPrice - payment.getPromoCodeValue();
		payment.acceptTermsAndconditions();
		payment.clickContinue();
		payment.clickCompleteButton();
		payment.checkThankYouMessage();
		payment.thankYouPageLocatorsCheck();
		ms.clickHomeButton();
		int afterDonateValue = ms.getThermometrValue();
		dashboard.switchToOldWindow();
		ms.checkDonateThermometrAfterBuyingItems(beforeDonateValue, afterDonateValue, sponsorPrice);
	}
	
	
	
	@Test(dependsOnMethods = { "login" })
	public void donationBuyRegistrationItemAndDonation() {	
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.searchBy("FirstEvent");
		dashboard.clickEditFirstEvent();
		dashboard.switchToEvent();
		Event ms = new Event(driver);
		int beforeDonateValue = ms.getThermometrValue();
		EventDonation msDonation = ms.clickRegistrationButton();
		int regPrice = msDonation.getRegItemPrice()+ Integer.parseInt(utils.Data.donationAmount);
		msDonation.setRegItemQuantity();
		msDonation.addToCartRegItem();
		msDonation.setDonationAmountInOrder(utils.Data.donationAmount);
		PaymentInfo payment = msDonation.clickCompleteCheckout();
		payment.paymentDataFilling();
		payment.acceptTermsAndconditions();
		payment.clickContinue();
		payment.clickCompleteButton();
		payment.checkThankYouMessage();
		payment.thankYouPageLocatorsCheck();
		ms.clickHomeButton();
		int afterDonateValue = ms.getThermometrValue();
		dashboard.switchToOldWindow();
		ms.checkDonateThermometrAfterBuyingItems(beforeDonateValue, afterDonateValue, regPrice);
	}
	
	@Test(dependsOnMethods = { "login" })
	public void donationBuyDonationItemAndDonation() {	
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditFirstEvent();
		dashboard.switchToEvent();
		Event ms = new Event(driver);
		int beforeDonateValue = ms.getThermometrValue();
		EventDonation msDonation = ms.clickDonationButton();
		int donationPrice = msDonation.getDonationItemPrice()+ Integer.parseInt(utils.Data.donationAmount);
		msDonation.setDonationItemQuantity();
		msDonation.addToCartDonationItem();
		msDonation.setDonationAmountInOrder(utils.Data.donationAmount);
		PaymentInfo payment = msDonation.clickCompleteCheckout();
		payment.paymentDataFilling();
		payment.acceptTermsAndconditions();
		payment.clickContinue();
		payment.clickCompleteButton();
		payment.checkThankYouMessage();
		payment.thankYouPageLocatorsCheck();
		ms.clickHomeButton();
		int afterDonateValue = ms.getThermometrValue();
		dashboard.switchToOldWindow();
		ms.checkDonateThermometrAfterBuyingItems(beforeDonateValue, afterDonateValue, donationPrice);
	}
	
	@Test(dependsOnMethods = { "login" })
	public void donationBuySponsorItemAndDonation() {
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditFirstEvent();
		dashboard.switchToEvent();
		Event ms = new Event(driver);
		int beforeDonateValue = ms.getThermometrValue();
		EventDonation msDonation = ms.clickSponsorButton();
		int sponsorPrice = msDonation.getSponsorItemPrice()+ Integer.parseInt(utils.Data.donationAmount);
		msDonation.setSponsorItemQuantity();
		msDonation.addToCartSponsorItem();
		msDonation.setDonationAmountInOrder(utils.Data.donationAmount);
		PaymentInfo payment = msDonation.clickCompleteCheckout();
		payment.paymentDataFilling();
		payment.acceptTermsAndconditions();
		payment.clickContinue();
		payment.clickCompleteButton();
		payment.checkThankYouMessage();
		payment.thankYouPageLocatorsCheck();
		ms.clickHomeButton();
		int afterDonateValue = ms.getThermometrValue();
		dashboard.switchToOldWindow();
		ms.checkDonateThermometrAfterBuyingItems(beforeDonateValue, afterDonateValue, sponsorPrice);
	}
	
	@Test(dependsOnMethods = { "login" })
	public void donationBuyRegistrationItemAndDonationWithPromocode() {	
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditFirstEvent();
		dashboard.switchToEvent();
		Event ms = new Event(driver);
		int beforeDonateValue = ms.getThermometrValue();
		EventDonation msDonation = ms.clickRegistrationButton();
		int regPrice = msDonation.getRegItemPrice()+ Integer.parseInt(utils.Data.donationAmount);
		msDonation.setRegItemQuantity();
		msDonation.addToCartRegItem();
		msDonation.setDonationAmountInOrder(utils.Data.donationAmount);
		PaymentInfo payment = msDonation.clickCompleteCheckout();
		payment.paymentDataFilling();
		payment.setPromocode15();
		regPrice = regPrice - payment.getPromoCodeValue();
		payment.acceptTermsAndconditions();
		payment.clickContinue();
		payment.clickCompleteButton();
		payment.checkThankYouMessage();
		payment.thankYouPageLocatorsCheck();
		ms.clickHomeButton();
		int afterDonateValue = ms.getThermometrValue();
		dashboard.switchToOldWindow();
		ms.checkDonateThermometrAfterBuyingItems(beforeDonateValue, afterDonateValue, regPrice);
	}
	
	@Test(dependsOnMethods = { "login" })
	public void donationBuyDonationItemAndDonationWithPromocode() {	
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditFirstEvent();
		dashboard.switchToEvent();
		Event ms = new Event(driver);
		int beforeDonateValue = ms.getThermometrValue();
		EventDonation msDonation = ms.clickDonationButton();
		int donationPrice = msDonation.getDonationItemPrice()+ Integer.parseInt(utils.Data.donationAmount);
		msDonation.setDonationItemQuantity();
		msDonation.addToCartDonationItem();
		msDonation.setDonationAmountInOrder(utils.Data.donationAmount);
		PaymentInfo payment = msDonation.clickCompleteCheckout();
		payment.paymentDataFilling();
		payment.setPromocode15();
		donationPrice = donationPrice - payment.getPromoCodeValue();
		payment.acceptTermsAndconditions();
		payment.clickContinue();
		payment.clickCompleteButton();
		payment.checkThankYouMessage();
		payment.thankYouPageLocatorsCheck();
		ms.clickHomeButton();
		int afterDonateValue = ms.getThermometrValue();
		dashboard.switchToOldWindow();
		ms.checkDonateThermometrAfterBuyingItems(beforeDonateValue, afterDonateValue, donationPrice);
	}
	
	@Test(dependsOnMethods = { "login" })
	public void donationBuySponsorItemAndDonationWithPromocode() {	
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditFirstEvent();
		dashboard.switchToEvent();
		Event ms = new Event(driver);
		int beforeDonateValue = ms.getThermometrValue();
		EventDonation msDonation = ms.clickSponsorButton();
		int sponsorPrice = msDonation.getSponsorItemPrice()+ Integer.parseInt(utils.Data.donationAmount);
		msDonation.setSponsorItemQuantity();
		msDonation.addToCartSponsorItem();
		msDonation.setDonationAmountInOrder(utils.Data.donationAmount);
		PaymentInfo payment = msDonation.clickCompleteCheckout();
		payment.paymentDataFilling();
		payment.setPromocode15();
		sponsorPrice = sponsorPrice - payment.getPromoCodeValue();
		payment.acceptTermsAndconditions();
		payment.clickContinue();
		payment.clickCompleteButton();
		payment.checkThankYouMessage();
		payment.thankYouPageLocatorsCheck();
		ms.clickHomeButton();
		int afterDonateValue = ms.getThermometrValue();
		dashboard.switchToOldWindow();
		ms.checkDonateThermometrAfterBuyingItems(beforeDonateValue, afterDonateValue, sponsorPrice);
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
	public void createEventGeneralInfoCheck() {
		HomepageDashboard dashboard = new HomepageDashboard(driver);
		Menu menu = dashboard.clickMenu();
		menu.clickHomepageDashboard();
		dashboard.searchBy("One more test");
		dashboard.clickManage();
		CreateEvent create = dashboard.clickCreateEvent();
		create.fillEventDescription();
		create.setTime();
		create.setEventDate();
		create.fillEventDataWithSpecificSundomain("micrositegeneral","450", "test1");
		EventEdit msEdit = create.clickSaveAndDone();
		msEdit.makeSitePublic();
		Event ms = msEdit.clickViewSite();
		ms.checkEventGeneralInfo("micrositegeneral", Misc.getCurrentDate("MsFormat"), "Test description");
		msEdit.switchToMainWindow();
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
	public void createEventBackgroundColorsCheck() {
		HomepageDashboard dashboard = new HomepageDashboard(driver);
		Menu menu = dashboard.clickMenu();
		menu.clickHomepageDashboard();
		dashboard.searchBy("One more test");
		dashboard.clickManage();
		CreateEvent create = dashboard.clickCreateEvent();
		create.fillEventDataWithSpecificSundomain("backcolors","569", "test2");
		create.clickSaveAndContinue();
		WebsiteAppearance appearance = new WebsiteAppearance(driver);
		appearance.setBackgroundColor(utils.Data.red);
		appearance.setNavigationBackgroundColor(utils.Data.blue);
		appearance.setButtonBackgroundColor(utils.Data.yellow);
		EventEdit msEdit = create.clickSaveAndDoneFromAppearancePage();
		msEdit.makeSitePublic();
		Event ms = msEdit.clickViewSite();
		ms.checkBackgroundColors(utils.Data.red, utils.Data.blue, utils.Data.yellow);
		msEdit.switchToMainWindow();
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" }) 
	public void createEventHtmlBanner() {
		HomepageDashboard dashboard = new HomepageDashboard(driver);
		Menu menu = dashboard.clickMenu();
		menu.clickHomepageDashboard();
		dashboard.searchBy("One more test");
		dashboard.clickManage();
		CreateEvent create = dashboard.clickCreateEvent();
		create.fillEventDataWithSpecificSundomain("htmlbanner","765", "test3");
		create.clickSaveAndContinue();
		WebsiteAppearance appearance = new WebsiteAppearance(driver);
		appearance.setHtmlBanner(utils.Data.htmlBannerValue);
		EventEdit msEdit = create.clickSaveAndDoneFromAppearancePage();
		msEdit.makeSitePublic();
		Event ms = msEdit.clickViewSite();
		ms.checkMastheadBanner(utils.Data.htmlBannerValue);
		msEdit.switchToMainWindow();
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" }) 
	public void createEventNavigationPages() {
		HomepageDashboard dashboard = new HomepageDashboard(driver);
		Menu menu = dashboard.clickMenu();
		menu.clickHomepageDashboard();
		dashboard.searchBy("One more test");
		dashboard.clickManage();
		CreateEvent create = dashboard.clickCreateEvent();
		create.fillEventDataWithSpecificSundomain("navpages","235", "test4");
		create.clickSaveAndContinue();
		NavigationPages navPages = create.clickNavigationPages();
		navPages.clickCreatePage();
		navPages.setPageTitle("test title");
		navPages.setNavigationLabel("test navigation label");
		navPages.setPagePermalink("testlink");
		navPages.setPageContent("long content");
		navPages.clickSaveButton();
		EventEdit msEdit = navPages.switchToAdvancedView();
		msEdit.makeSitePublic();
		Event ms = msEdit.clickViewSite();
		NavigationPage navPage = ms.clickNavigationLabel("test navigation label");
		navPage.checkNavPage("test title", "long content");
		msEdit.switchToMainWindow();
	}

	@Test(dependsOnMethods = { "login" }, groups = { "Profile" }) 
	public void createEventItems() {
		HomepageDashboard dashboard = new HomepageDashboard(driver);
		Menu menu = dashboard.clickMenu();
		menu.clickHomepageDashboard();
		dashboard.searchBy("One more test");
		dashboard.clickManage();
		CreateEvent create = dashboard.clickCreateEvent();
		create.fillEventDataWithSpecificSundomain("items","255", "test5");
		create.clickSaveAndContinue();
		Items items = create.clickTicketsAndItems();
		items.clickCreateItem();
		items.changeItemCategory("Donation");
		items.setItemName("Test item");
		items.setAmount("25");
		items.setTaxDeductible("50");
		items.clickSaveButton();
		EventEdit msEdit = items.switchToAdvancedView();
		msEdit.makeSitePublic();
		Event ms = msEdit.clickViewSite();
		EventDonation msDonation = ms.clickDonationButton();
		msDonation.checkItemInfo("25", "Test item");
		msEdit.switchToMainWindow();
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
	public void copyEventWithView() { 
		HomepageDashboard dashboard = new HomepageDashboard(driver);
		Menu menu = dashboard.clickMenu();
		menu.clickHomepageDashboard();
		dashboard.searchBy("One more test");
		dashboard.clickManage();
		CreateEvent create = dashboard.clickCreateEvent();
		create.setEventInfo();
		create.clickSaveAndContinue();
		WebsiteAppearance appearance = new WebsiteAppearance(driver);
		appearance.setAppearance();
		appearance.clickSaveAndContinue();
		WebsiteNavigationPages websiteNavigationPage = new WebsiteNavigationPages(driver);
		websiteNavigationPage.clickCreatePage();
		websiteNavigationPage.createNavigationPage();
		websiteNavigationPage.clickContinue();
		Items items = new Items(driver);
		items.createItem(items.getRegistrationItem());
		items.createItem(items.getSponsorshipItem());
		items.createItem(items.getDonashionItem());
		appearance.clickAppearance();
		EventEdit msEdit = create.clickSaveAndDoneFromAppearancePage();
		msEdit.makeSitePublic();
		dashboard.clickMenu();
		EventDashboard msDashboard = menu.clickEvent();
		msDashboard.searchBy("copyEventWithView");
		msDashboard.clickCopy();
		msEdit.clickWizzardView();
		create.setUrl("copyEventWithViewNew");
		create.clickSaveAndDone();
		Event ms = msEdit.clickViewSite();
		ms.checkEventGeneralInfo("copyEventWithView-copy", Misc.getCurrentDate("MsFormat"), "Test description");
		ms.checkBackgroundColors(utils.Data.blue, utils.Data.red, utils.Data.yellow);
		msEdit.switchToMainWindow(); 
	}
	
	@Test(dependsOnMethods = { "login", "createCategoryForMsTemplateView"  }, groups = { "Profile" }, priority = 1) 
	public void saveAsTemplateCheckByView() {
		HomepageDashboard dashboard = new HomepageDashboard(driver);
		Menu menu = dashboard.clickMenu();
		menu.clickHomepageDashboard();
		dashboard.searchBy("One more test");
		dashboard.clickManage();
		CreateEvent create = dashboard.clickCreateEvent();
		create.fillEventDescription();
		create.setTime();
		create.setEventDate();
		create.fillEventData("templateForView","450");
		create.clickSaveAndContinue();
		WebsiteAppearance appearance = new WebsiteAppearance(driver);
		appearance.setBackgroundColor(utils.Data.blue);
		appearance.setNavigationBackgroundColor(utils.Data.red);
		appearance.setButtonBackgroundColor(utils.Data.yellow);
		EventEdit msEdit = create.clickSaveAndDoneFromAppearancePage();
		msEdit.makeSitePublic();
		dashboard.clickMenu();
		EventDashboard msDashboard = menu.clickEvent();
		msDashboard.searchBy("templateForView");
		EventTemplates msTemplate = msDashboard.clickSaveAsTemplate();
		msTemplate.selectCategory("CategoryForMsTemplateView");
		msTemplate.changeName("templateForView");
		msTemplate.clickSave();
		msTemplate.searchBy("templateForView");
		Event ms = msTemplate.clickView();
		ms.checkEventGeneralInfo("templateForView", Misc.getCurrentDate("MsFormat"), "Test description");
		ms.checkBackgroundColors(utils.Data.blue, utils.Data.red, utils.Data.yellow);
		msTemplate.switchToMainWindow();
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" }, priority = 2)
	public void createCategoryForMsTemplateView() {
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		Menu menu = dashboard.clickMenu();
		Categories categories = menu.clickCategories();
		CreateCategory createCategory = categories.clickCreateCategory();
		createCategory.setCategoryName("CategoryForMsTemplateView");
		createCategory.setCategoryDescription("Automatically created by test");
		createCategory.clickSaveButton();
		categories.search("CategoryForMsTemplateView");
		categories.checkIfCreatedCategoryExist("CategoryForMsTemplateView");
	}
	
	@AfterMethod
	public void setScreenshot(ITestResult result) {
		if (!result.isSuccess()){
			String methodName = result.getName();
			String filename = null;
			try
			{
				SimpleDateFormat formater = new SimpleDateFormat("yyyy_MM_dd_hh_mm_ss");
				File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
				filename = formater.format(Calendar.getInstance().getTime()) + "_" + methodName + ".png";
				String separator = System.getProperty("file.separator");
				String path = System.getProperty("user.dir") + separator + "test-output" + separator + "screens" + separator;
				FileUtils.copyFile(scrFile, new File(path + filename));
				Reporter.setCurrentTestResult(result);
				Reporter.log("<img width='100%' src='" + "screens" + separator + filename + "'/>");
			}
			catch (IOException e1)
			{
				e1.printStackTrace();
			}
			if (driver.getWindowHandles().size() > 1) {
				handler.switchToOldWindow(driver);
			}	
		}
	}

	@AfterClass
	public void afterClass() {
		try
		{
			driver.close();
			driver.quit();
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
	} 

}
