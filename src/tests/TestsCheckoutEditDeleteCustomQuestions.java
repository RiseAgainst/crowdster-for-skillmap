package tests;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import eventDashboardPages.EventDashboard;
import eventPages.CheckoutCustomQuestions;
import organizationPages.OrganizationDashboard;
import pages.Login;
import utils.Data;
import utils.DriverSetup;
import utils.WindowHandler;

public class TestsCheckoutEditDeleteCustomQuestions {
	private WebDriver driver;
	private WindowHandler handler;
	private String username	= Data.userLogin;
	private String password	= Data.userPassword;
	
	@BeforeClass
	public void beforeClass() {
		driver = DriverSetup.getDriver("desktop");
		DriverSetup.initialSetup(driver);
		this.handler = new WindowHandler();
	}
	
	@Test
	public void loginPage() {
		Login loginPage = new Login(driver);
		ArrayList<String> locators = pages.Login.getLocators();
		loginPage.locatorsCheck(locators);
	}

	@Test(dependsOnMethods = { "loginPage" })
	public void login() {
		Login loginPage = new Login(driver);
		OrganizationDashboard result = loginPage.loginAsUser(username, password);
		loginPage.checkIfLoginSuccessful(result.getTitleText());
	}
	
	@Test(dependsOnMethods = { "login" }, priority = 1)
	public void editPastDateQuestion(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditSecondOneEvent();
		CheckoutCustomQuestions checkoutCustomQuestions = dashboard.navigateToCheckoutCustomQuestions();
		checkoutCustomQuestions.clickEditLastQuestion();
		String questionTypeText = checkoutCustomQuestions.getPastDateType();
		checkoutCustomQuestions.editCustomQuestion(questionTypeText);
		String rankOrder = checkoutCustomQuestions.getPastDateRankOrder();
		dashboard.switchToEvent();
		checkoutCustomQuestions.clickEventDonationBtn();
		checkoutCustomQuestions.setQuickDonationAmount();
		checkoutCustomQuestions.clickCompleteCheckout();
		checkoutCustomQuestions.checkEditedRequiredCustomQuestion(questionTypeText, rankOrder);
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" }, priority = 2)
	public void deletePastDateQuestion(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditSecondOneEvent();
		CheckoutCustomQuestions checkoutCustomQuestions = dashboard.navigateToCheckoutCustomQuestions();
		int customQuestionsCountBefore = checkoutCustomQuestions.getCustomQuestionsCount();
		checkoutCustomQuestions.clickDeleteLastQuestion();
		dashboard.handelJavaScriptAlert();
		int customQuestionsCountAfter = checkoutCustomQuestions.getCustomQuestionsCount();
		checkoutCustomQuestions.checkIsDeletedCustomQuestion(customQuestionsCountBefore, customQuestionsCountAfter);
	}
	
	@Test(dependsOnMethods = { "login" }, priority = 3)
	public void editFutureDateQuestion(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditSecondOneEvent();
		CheckoutCustomQuestions checkoutCustomQuestions = dashboard.navigateToCheckoutCustomQuestions();
		checkoutCustomQuestions.clickEditLastQuestion();
		String questionTypeText = checkoutCustomQuestions.getFutureDateType();
		checkoutCustomQuestions.editCustomQuestion(questionTypeText);
		String rankOrder = checkoutCustomQuestions.getFutureDateRankOrder();
		dashboard.switchToEvent();
		checkoutCustomQuestions.clickEventDonationBtn();
		checkoutCustomQuestions.setQuickDonationAmount();
		checkoutCustomQuestions.clickCompleteCheckout();
		checkoutCustomQuestions.checkEditedRequiredCustomQuestion(questionTypeText, rankOrder);
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" }, priority = 4)
	public void deleteFutureDateQuestion(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditSecondOneEvent();
		CheckoutCustomQuestions checkoutCustomQuestions = dashboard.navigateToCheckoutCustomQuestions();
		int customQuestionsCountBefore = checkoutCustomQuestions.getCustomQuestionsCount();
		checkoutCustomQuestions.clickDeleteLastQuestion();
		dashboard.handelJavaScriptAlert();
		int customQuestionsCountAfter = checkoutCustomQuestions.getCustomQuestionsCount();
		checkoutCustomQuestions.checkIsDeletedCustomQuestion(customQuestionsCountBefore, customQuestionsCountAfter);
	}
	
	@Test(dependsOnMethods = { "login" }, priority = 5)
	public void editConfirmationCheckboxQuestion(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditSecondOneEvent();
		CheckoutCustomQuestions checkoutCustomQuestions = dashboard.navigateToCheckoutCustomQuestions();
		checkoutCustomQuestions.clickEditLastQuestion();
		String questionTypeText = checkoutCustomQuestions.getConfirmationCheckboxType();
		checkoutCustomQuestions.editCustomQuestion(questionTypeText);
		String rankOrder = checkoutCustomQuestions.getConfirmationCheckboxRankOrder();
		dashboard.switchToEvent();
		checkoutCustomQuestions.clickEventDonationBtn();
		checkoutCustomQuestions.setQuickDonationAmount();
		checkoutCustomQuestions.clickCompleteCheckout();
		checkoutCustomQuestions.checkEditedConfirmationCheckboxCustomQuestion(questionTypeText, rankOrder);
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" }, priority = 6)
	public void deleteConfirmationCheckboxQuestion(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditSecondOneEvent();
		CheckoutCustomQuestions checkoutCustomQuestions = dashboard.navigateToCheckoutCustomQuestions();
		int customQuestionsCountBefore = checkoutCustomQuestions.getCustomQuestionsCount();
		checkoutCustomQuestions.clickDeleteLastQuestion();
		dashboard.handelJavaScriptAlert();
		int customQuestionsCountAfter = checkoutCustomQuestions.getCustomQuestionsCount();
		checkoutCustomQuestions.checkIsDeletedCustomQuestion(customQuestionsCountBefore, customQuestionsCountAfter);
	}
	
	@Test(dependsOnMethods = { "login" }, priority = 7)
	public void editTextBlockQuestion(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditSecondOneEvent();
		CheckoutCustomQuestions checkoutCustomQuestions = dashboard.navigateToCheckoutCustomQuestions();
		checkoutCustomQuestions.clickEditLastQuestion();
		String questionTypeText = checkoutCustomQuestions.getTextBlockType();
		checkoutCustomQuestions.editTextBlockCustomQuestion(questionTypeText);
		String rankOrder = checkoutCustomQuestions.getTextBlockRankOrder();
		dashboard.switchToEvent();
		checkoutCustomQuestions.clickEventDonationBtn();
		checkoutCustomQuestions.setQuickDonationAmount();
		checkoutCustomQuestions.clickCompleteCheckout();
		checkoutCustomQuestions.checkEditCustomQuestion(questionTypeText, rankOrder);
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" }, priority = 8)
	public void deleteTextBlockQuestion(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditSecondOneEvent();
		CheckoutCustomQuestions checkoutCustomQuestions = dashboard.navigateToCheckoutCustomQuestions();
		int customQuestionsCountBefore = checkoutCustomQuestions.getCustomQuestionsCount();
		checkoutCustomQuestions.clickDeleteLastQuestion();
		dashboard.handelJavaScriptAlert();
		int customQuestionsCountAfter = checkoutCustomQuestions.getCustomQuestionsCount();
		checkoutCustomQuestions.checkIsDeletedCustomQuestion(customQuestionsCountBefore, customQuestionsCountAfter);
	}
	
	@Test(dependsOnMethods = { "login" }, priority = 9)
	public void editFileUploadQuestion(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditSecondOneEvent();
		CheckoutCustomQuestions checkoutCustomQuestions = dashboard.navigateToCheckoutCustomQuestions();
		checkoutCustomQuestions.clickEditLastQuestion();
		String questionTypeText = checkoutCustomQuestions.getFileUploadType();
		checkoutCustomQuestions.editCustomQuestion(questionTypeText);
		String rankOrder = checkoutCustomQuestions.getFileUploadRankOrder();
		dashboard.switchToEvent();
		checkoutCustomQuestions.clickEventDonationBtn();
		checkoutCustomQuestions.setQuickDonationAmount();
		checkoutCustomQuestions.clickCompleteCheckout();
		checkoutCustomQuestions.checkEditedRequiredCustomQuestion(questionTypeText, rankOrder);
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" }, priority = 10)
	public void deleteFileUploadQuestion(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditSecondOneEvent();
		CheckoutCustomQuestions checkoutCustomQuestions = dashboard.navigateToCheckoutCustomQuestions();
		int customQuestionsCountBefore = checkoutCustomQuestions.getCustomQuestionsCount();
		checkoutCustomQuestions.clickDeleteLastQuestion();
		dashboard.handelJavaScriptAlert();
		int customQuestionsCountAfter = checkoutCustomQuestions.getCustomQuestionsCount();
		checkoutCustomQuestions.checkIsDeletedCustomQuestion(customQuestionsCountBefore, customQuestionsCountAfter);
	}
	
	@Test(dependsOnMethods = { "login" }, priority = 11)
	public void editCountrySelectQuestion(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditSecondOneEvent();
		CheckoutCustomQuestions checkoutCustomQuestions = dashboard.navigateToCheckoutCustomQuestions();
		checkoutCustomQuestions.clickEditLastQuestion();
		String questionTypeText = checkoutCustomQuestions.getCountrySelectType();
		checkoutCustomQuestions.editCustomQuestion(questionTypeText);
		String rankOrder = checkoutCustomQuestions.getCountrySelectRankOrder();
		dashboard.switchToEvent();
		checkoutCustomQuestions.clickEventDonationBtn();
		checkoutCustomQuestions.setQuickDonationAmount();
		checkoutCustomQuestions.clickCompleteCheckout();
		checkoutCustomQuestions.checkEditedRequiredCustomQuestion(questionTypeText, rankOrder);
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" }, priority = 12)
	public void deleteCountrySelectQuestion(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditSecondOneEvent();
		CheckoutCustomQuestions checkoutCustomQuestions = dashboard.navigateToCheckoutCustomQuestions();
		int customQuestionsCountBefore = checkoutCustomQuestions.getCustomQuestionsCount();
		checkoutCustomQuestions.clickDeleteLastQuestion();
		dashboard.handelJavaScriptAlert();
		int customQuestionsCountAfter = checkoutCustomQuestions.getCustomQuestionsCount();
		checkoutCustomQuestions.checkIsDeletedCustomQuestion(customQuestionsCountBefore, customQuestionsCountAfter);
	}
	
	@Test(dependsOnMethods = { "login" }, priority = 13)
	public void editStateSelectQuestion(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditSecondOneEvent();
		CheckoutCustomQuestions checkoutCustomQuestions = dashboard.navigateToCheckoutCustomQuestions();
		checkoutCustomQuestions.clickEditLastQuestion();
		String questionTypeText = checkoutCustomQuestions.getStateSelectType();
		checkoutCustomQuestions.editCustomQuestion(questionTypeText);
		String rankOrder = checkoutCustomQuestions.getStateSelectRankOrder();
		dashboard.switchToEvent();
		checkoutCustomQuestions.clickEventDonationBtn();
		checkoutCustomQuestions.setQuickDonationAmount();
		checkoutCustomQuestions.clickCompleteCheckout();
		checkoutCustomQuestions.checkEditedRequiredCustomQuestion(questionTypeText, rankOrder);
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" }, priority = 14)
	public void deleteStateSelectQuestion(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditSecondOneEvent();
		CheckoutCustomQuestions checkoutCustomQuestions = dashboard.navigateToCheckoutCustomQuestions();
		int customQuestionsCountBefore = checkoutCustomQuestions.getCustomQuestionsCount();
		checkoutCustomQuestions.clickDeleteLastQuestion();
		dashboard.handelJavaScriptAlert();
		int customQuestionsCountAfter = checkoutCustomQuestions.getCustomQuestionsCount();
		checkoutCustomQuestions.checkIsDeletedCustomQuestion(customQuestionsCountBefore, customQuestionsCountAfter);
	}
	
	@Test(dependsOnMethods = { "login" }, priority = 15)
	public void editCheckBoxQuestion(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditSecondOneEvent();
		CheckoutCustomQuestions checkoutCustomQuestions = dashboard.navigateToCheckoutCustomQuestions();
		checkoutCustomQuestions.clickEditLastQuestion();
		String questionTypeText = checkoutCustomQuestions.getCheckBoxType();
		checkoutCustomQuestions.editCustomQuestion(questionTypeText);
		String rankOrder = checkoutCustomQuestions.getCheckBoxRankOrder();
		dashboard.switchToEvent();
		checkoutCustomQuestions.clickEventDonationBtn();
		checkoutCustomQuestions.setQuickDonationAmount();
		checkoutCustomQuestions.clickCompleteCheckout();
		checkoutCustomQuestions.checkEditedRequiredCustomQuestion(questionTypeText, rankOrder);
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" }, priority = 16)
	public void deleteCheckBoxQuestion(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditSecondOneEvent();
		CheckoutCustomQuestions checkoutCustomQuestions = dashboard.navigateToCheckoutCustomQuestions();
		int customQuestionsCountBefore = checkoutCustomQuestions.getCustomQuestionsCount();
		checkoutCustomQuestions.clickDeleteLastQuestion();
		dashboard.handelJavaScriptAlert();
		int customQuestionsCountAfter = checkoutCustomQuestions.getCustomQuestionsCount();
		checkoutCustomQuestions.checkIsDeletedCustomQuestion(customQuestionsCountBefore, customQuestionsCountAfter);
	}
	
	@Test(dependsOnMethods = { "login" }, priority = 17)
	public void editSelectBoxQuestion(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditSecondOneEvent();
		CheckoutCustomQuestions checkoutCustomQuestions = dashboard.navigateToCheckoutCustomQuestions();
		checkoutCustomQuestions.clickEditLastQuestion();
		String questionTypeText = checkoutCustomQuestions.getSelectBoxType();
		checkoutCustomQuestions.editCustomQuestion(questionTypeText);
		String rankOrder = checkoutCustomQuestions.getSelectBoxRankOrder();
		dashboard.switchToEvent();
		checkoutCustomQuestions.clickEventDonationBtn();
		checkoutCustomQuestions.setQuickDonationAmount();
		checkoutCustomQuestions.clickCompleteCheckout();
		checkoutCustomQuestions.checkEditedRequiredCustomQuestion(questionTypeText, rankOrder);
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" }, priority = 18)
	public void deleteSelectBoxQuestion(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditSecondOneEvent();
		CheckoutCustomQuestions checkoutCustomQuestions = dashboard.navigateToCheckoutCustomQuestions();
		int customQuestionsCountBefore = checkoutCustomQuestions.getCustomQuestionsCount();
		checkoutCustomQuestions.clickDeleteLastQuestion();
		dashboard.handelJavaScriptAlert();
		int customQuestionsCountAfter = checkoutCustomQuestions.getCustomQuestionsCount();
		checkoutCustomQuestions.checkIsDeletedCustomQuestion(customQuestionsCountBefore, customQuestionsCountAfter);
	}
	
	@Test(dependsOnMethods = { "login" }, priority = 19)
	public void editWriteInEssayQuestion(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditSecondOneEvent();
		CheckoutCustomQuestions checkoutCustomQuestions = dashboard.navigateToCheckoutCustomQuestions();
		checkoutCustomQuestions.clickEditLastQuestion();
		String questionTypeText = checkoutCustomQuestions.getWriteInEssyType();
		checkoutCustomQuestions.editCustomQuestion(questionTypeText);
		String rankOrder = checkoutCustomQuestions.getWriteInEssyRankOrder();
		dashboard.switchToEvent();
		checkoutCustomQuestions.clickEventDonationBtn();
		checkoutCustomQuestions.setQuickDonationAmount();
		checkoutCustomQuestions.clickCompleteCheckout();
		checkoutCustomQuestions.checkEditedRequiredCustomQuestion(questionTypeText, rankOrder);
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" }, priority = 20)
	public void deleteWriteInEssayQuestion(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditSecondOneEvent();
		CheckoutCustomQuestions checkoutCustomQuestions = dashboard.navigateToCheckoutCustomQuestions();
		int customQuestionsCountBefore = checkoutCustomQuestions.getCustomQuestionsCount();
		checkoutCustomQuestions.clickDeleteLastQuestion();
		dashboard.handelJavaScriptAlert();
		int customQuestionsCountAfter = checkoutCustomQuestions.getCustomQuestionsCount();
		checkoutCustomQuestions.checkIsDeletedCustomQuestion(customQuestionsCountBefore, customQuestionsCountAfter);
	}
	
	@Test(dependsOnMethods = { "login" }, priority = 21)
	public void editWriteIn1LineQuestion(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditSecondOneEvent();
		CheckoutCustomQuestions checkoutCustomQuestions = dashboard.navigateToCheckoutCustomQuestions();
		checkoutCustomQuestions.clickEditLastQuestion();
		String questionTypeText = checkoutCustomQuestions.getWriteInOneLineType();
		checkoutCustomQuestions.editCustomQuestion(questionTypeText);
		String rankOrder = checkoutCustomQuestions.getWriteInOneLineRankOrder();
		dashboard.switchToEvent();
		checkoutCustomQuestions.clickEventDonationBtn();
		checkoutCustomQuestions.setQuickDonationAmount();
		checkoutCustomQuestions.clickCompleteCheckout();
		checkoutCustomQuestions.checkEditedRequiredCustomQuestion(questionTypeText, rankOrder);
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" }, priority = 22)
	public void deleteWriteIn1LineQuestion(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditSecondOneEvent();
		CheckoutCustomQuestions checkoutCustomQuestions = dashboard.navigateToCheckoutCustomQuestions();
		int customQuestionsCountBefore = checkoutCustomQuestions.getCustomQuestionsCount();
		checkoutCustomQuestions.clickDeleteLastQuestion();
		dashboard.handelJavaScriptAlert();
		int customQuestionsCountAfter = checkoutCustomQuestions.getCustomQuestionsCount();
		checkoutCustomQuestions.checkIsDeletedCustomQuestion(customQuestionsCountBefore, customQuestionsCountAfter);
	}
	
	@AfterMethod
	public void setScreenshot(ITestResult result) {
		if (!result.isSuccess()){
			String methodName = result.getName();
			String filename = null;
			try
			{
				SimpleDateFormat formater = new SimpleDateFormat("yyyy_MM_dd_hh_mm_ss");
				File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
				filename = formater.format(Calendar.getInstance().getTime()) + "_" + methodName + ".png";
				String separator = System.getProperty("file.separator");
				String path = System.getProperty("user.dir") + separator + "test-output" + separator + "screens" + separator;
				FileUtils.copyFile(scrFile, new File(path + filename));
				Reporter.setCurrentTestResult(result);
				Reporter.log("<img width='100%' src='" + "screens" + separator + filename + "'/>");
			}
			catch (IOException e1)
			{
				e1.printStackTrace();
			}
			if (driver.getWindowHandles().size() > 1) {
				handler.switchToOldWindow(driver);
			}
		}
	}
	
	@AfterClass
	public void afterClass() {
		try
		{
			driver.close();
			driver.quit();
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
	} 
}
