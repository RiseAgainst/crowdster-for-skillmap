package tests;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import organizationPages.OrganizationDashboard;
import pages.AccountSettings;
import pages.Login;
import utils.Data;
import utils.DriverSetup;
import utils.WindowHandler;

public class TestsAccountSettings {
	private WebDriver 		driver;
	private WindowHandler 	handler;
	private String 			username	= 	Data.userLogin;
	private String 			password	= 	Data.userPassword;
	private Login 			loginPage;
	
	@BeforeClass
	public void beforeClass() {
		driver = DriverSetup.getDriver("desktop");
		DriverSetup.initialSetup(driver);
		this.handler = new WindowHandler();
		loginPage = new Login(driver);
	}
	
	@Test
	public void loginPage() {
		ArrayList<String> locators = pages.Login.getLocators();
		loginPage.locatorsCheck(locators);
	}

	@Test(dependsOnMethods = { "loginPage" })
	public void login() {
		OrganizationDashboard result = loginPage.loginAsUser(username, password);
		loginPage.checkIfLoginSuccessful(result.getTitleText());
	}
	
	@Test(dependsOnMethods = { "login" })
	public void changeAccountInformation() {
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		AccountSettings account = dashboard.clickOnAccountSettings();
		dashboard.searchOnUserTable("Owner");
		account.goToAccountInformation();
		account.changeFirstLastNamePhone();
		account.clickSaveBtn();
		dashboard.signOut();
		loginPage.loginAsUser(username, password);
		dashboard.clickOnAccountSettings();
		dashboard.searchOnUserTable("Owner");
		account.goToAccountInformation();
		account.checkFirstNameChanged();
		account.checkLastNameChanged();
		account.checkPhoneChanged();
	}
	
	@Test(dependsOnMethods = { "login" })
	public void changeAccountPassword(){
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		AccountSettings account = dashboard.clickOnAccountSettings();
		dashboard.searchOnUserTable("Owner");
		account.goToAccountInformation();
		account.changePassword();
		account.clickSaveBtn();
		dashboard.signOut();
		loginPage.loginAsUser(username, account.getPasswordNew());
		dashboard.clickOnAccountSettings();
		dashboard.searchOnUserTable("Owner");
		account.goToAccountInformation();
		account.setOldPassword();
		account.clickSaveBtn();
		dashboard.signOut();
		loginPage.loginAsUser(username, password);
	}
	
	@Test(dependsOnMethods = { "login" })
	public void createReadOnlyUser(){
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		AccountSettings account = dashboard.clickOnAccountSettings();
		int usersCountBefore = account.getUsersCout();
		account.clickCreateUserBtn();
		account.createNewUser();
		int usersCountAfter = account.getUsersCout();
		account.checkUsersCountAfterCreated(usersCountBefore, usersCountAfter);
	}
	
	@Test(dependsOnMethods = { "login" })
	public void failToCreateExistingUser(){
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		AccountSettings account = dashboard.clickOnAccountSettings();
		int usersCountBefore = account.getUsersCout();
		account.clickCreateUserBtn();
		account.createNewUser();
		dashboard.clickOnAccountSettings();
		int usersCountAfter = account.getUsersCout();
		account.checkUsersCountAfterCreateExistingUser(usersCountBefore, usersCountAfter);
	}
	
	@Test(dependsOnMethods = { "login" })
	public void setOrganizationImage(){
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		AccountSettings account = dashboard.clickOnAccountSettings();
		account.clickBranding();
		account.clickIfExistingRemoveButtons();
		account.setOrganizationImage();
		account.clickSaveBtn();
		dashboard.signOut();
		loginPage.loginAsUser(username, password);
		account.checkOrganizationImg();
	}
	
	@Test(dependsOnMethods = { "login" })
	public void setLogoImage(){
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		AccountSettings account = dashboard.clickOnAccountSettings();
		account.clickBranding();
		account.clickIfExistingRemoveButtons();
		account.setLogoImage();
		account.clickSaveBtn();
		dashboard.signOut();
		loginPage.loginAsUser(username, password);
		account.checkLogoImg();
	}
	
	@AfterMethod
	public void setScreenshot(ITestResult result) {
		if (!result.isSuccess()){
			String methodName = result.getName();
			String filename = null;
			try
			{
				SimpleDateFormat formater = new SimpleDateFormat("yyyy_MM_dd_hh_mm_ss");
				File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
				filename = formater.format(Calendar.getInstance().getTime()) + "_" + methodName + ".png";
				String separator = System.getProperty("file.separator");
				String path = System.getProperty("user.dir") + separator + "test-output" + separator + "screens" + separator;
				FileUtils.copyFile(scrFile, new File(path + filename));
				Reporter.setCurrentTestResult(result);
				Reporter.log("<img width='100%' src='" + "screens" + separator + filename + "'/>");
			}
			catch (IOException e1)
			{
				e1.printStackTrace();
			}
			if (driver.getWindowHandles().size() > 1) {
				handler.switchToOldWindow(driver);
			}
		}
	}
	
	@AfterClass
	public void afterClass() {
		try
		{
			driver.close();
			driver.quit();
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
	} 
}
