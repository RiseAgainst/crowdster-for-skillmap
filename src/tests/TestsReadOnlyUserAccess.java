package tests;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import eventDashboardPages.EventDashboard;
import eventPages.Event;
import organizationPages.OrganizationDashboard;
import pages.AccountSettings;
import pages.Login;
import utils.Data;
import utils.DriverSetup;
import utils.WindowHandler;

public class TestsReadOnlyUserAccess {
	private WebDriver 		driver;
	private WindowHandler 	handler;
	private String 			username	= 	Data.readOnlyUserLogin;
	private String 			password	= 	Data.userPassword;
	private Login 			loginPage;
	
	@BeforeClass
	public void beforeClass() {
		driver = DriverSetup.getDriver("desktop");
		DriverSetup.initialSetup(driver);
		this.handler = new WindowHandler();
		loginPage = new Login(driver);
	}
	
	@Test
	public void loginPage() {
		ArrayList<String> locators = pages.Login.getLocators();
		loginPage.locatorsCheck(locators);
	}

	@Test(dependsOnMethods = { "loginPage" })
	public void login() {
		OrganizationDashboard result = loginPage.loginAsUser(username, password);
		loginPage.checkIfLoginSuccessful(result.getTitleText());
	}
	
	@Test(dependsOnMethods = { "login" })
	public void checkHoveringAnEventDisplaingOnlyViewAndEditBtns(){
		EventDashboard dashboard = new EventDashboard(driver);
		WebElement eventElement = dashboard.hoverOverFirstEvent();
		Event event = new Event(driver);
		event.checkEventHoverToDisplayOnlyViewAndEdit(eventElement);
	}
	
	@Test(dependsOnMethods = { "login" })
	public void checkEventDashboardToHaveOnly4Options(){
		EventDashboard dashboard = new EventDashboard(driver);
		Event event = dashboard.clickEditFirstEvent();
		event.checkEventDashboardOnly4Options();
	}
	
	@Test(dependsOnMethods = { "login" })
	public void checkWhatYouCanDoContainerForReadOnlySpecificContent(){
		Event event = new Event(driver);
		event.checkWhatYouCanDoBoxForReadOnlySpecificContent();
	}
	
	@Test(dependsOnMethods = { "login" })
	public void checkEditAccountInformation(){
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		AccountSettings account = dashboard.clickOnAccountSettings();
		account.checkAccountInformationInputs();
	}
	
	@Test(dependsOnMethods = { "login" })
	public void checkBrandingNoOptions(){
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		AccountSettings account = dashboard.clickOnAccountSettings();
		account.clickBranding();
		account.checkBrandingNoOptions();
	}
	
	@AfterMethod
	public void setScreenshot(ITestResult result) {
		if (!result.isSuccess()){
			String methodName = result.getName();
			String filename = null;
			try
			{
				SimpleDateFormat formater = new SimpleDateFormat("yyyy_MM_dd_hh_mm_ss");
				File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
				filename = formater.format(Calendar.getInstance().getTime()) + "_" + methodName + ".png";
				String separator = System.getProperty("file.separator");
				String path = System.getProperty("user.dir") + separator + "test-output" + separator + "screens" + separator;
				FileUtils.copyFile(scrFile, new File(path + filename));
				Reporter.setCurrentTestResult(result);
				Reporter.log("<img width='100%' src='" + "screens" + separator + filename + "'/>");
			}
			catch (IOException e1)
			{
				e1.printStackTrace();
			}
			if (driver.getWindowHandles().size() > 1) {
				handler.switchToOldWindow(driver);
			}
		}
	}
	
	@AfterClass
	public void afterClass() {
		try
		{
			driver.close();
			driver.quit();
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
	} 
}
