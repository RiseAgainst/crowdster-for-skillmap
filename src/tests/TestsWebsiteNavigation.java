package tests;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import eventDashboardPages.EventDashboard;
import eventDashboardPages.NavigationPages;
import eventPages.NavigationPage;
import organizationPages.OrganizationDashboard;
import utils.Data;
import utils.DriverSetup;
import utils.WindowHandler;
import pages.Login;

public class TestsWebsiteNavigation {
	private WebDriver		driver;
	private WindowHandler 	handler;
	private String			username	= Data.userLogin;
	private String			password	= Data.userPassword;
	
	@BeforeClass
	public void beforeClass() {
		driver = DriverSetup.getDriver("desktop");
		DriverSetup.initialSetup(driver);
		handler = new WindowHandler();
	}
	
	@Test
	public void loginPage() {
		Login loginPage = new Login(driver);
		ArrayList<String> locators = pages.Login.getLocators();
		loginPage.locatorsCheck(locators);
	}

	@Test(dependsOnMethods = { "loginPage" })
	public void login() {
		Login loginPage = new Login(driver);
		OrganizationDashboard result = loginPage.loginAsUser(username, password);
		loginPage.checkIfLoginSuccessful(result.getTitleText());
	}
	
	@Test(dependsOnMethods = { "login" }, priority = 1)
	public void createEventNavigationPage(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditFirstEvent();		
		NavigationPages navigationPages = dashboard.navigateToNavigation();
		NavigationPage navigationPage = navigationPages.clickCreatePage();	
		navigationPage.createNavigationPage();
		dashboard.switchToEvent();
		navigationPage.checkNavigationPage();
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" }, priority = 2)
	public void editEventNavigationPage(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditFirstEvent();		
		NavigationPages navigationPages = dashboard.navigateToNavigation();
		NavigationPage navigationPage = navigationPages.clickEdit();
		navigationPage.editNavigationPage();
		navigationPage.checkSuccessMessage();
		dashboard.switchToEvent();
		navigationPage.checkNavigationPageEdit();
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" }, priority = 3)
	public void failToEditNavigationPageWithImproperData(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditFirstEvent();		
		NavigationPages navigationPages = dashboard.navigateToNavigation();
		NavigationPage navigationPage = navigationPages.clickEdit();
		navigationPage.failToEditNavigatinPageWithoutRequiredFields();
		navigationPage.failToEditNavigatinPageWithoutData();
	}
	
	@Test(dependsOnMethods = { "login" }, priority = 4)
	public void createEventNavigationPageWithPrivateOptions(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditFirstEvent();		
		NavigationPages navigationPages = dashboard.navigateToNavigation();
		NavigationPage navigationPage = navigationPages.clickCreatePage();	
		navigationPage.createNavigationPageWithSpecificOptions();
		dashboard.switchToEvent();
		Boolean isPrivate = navigationPage.isPrivateNaviPage();
		navigationPage.checkIsPrivateNavigationPage(isPrivate);
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" }, priority = 5)
	public void failToCreateNavigatinPageWithoutData(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditFirstEvent();		
		NavigationPages navigationPages = dashboard.navigateToNavigation();
		NavigationPage navigationPage = navigationPages.clickCreatePage();
		navigationPage.failToCreateNavigatinPageWithoutData();
		navigationPage.checkIsShownValidationMessage();	
	}
	
	@Test(dependsOnMethods = { "login" }, priority = 6)
	public void failToCreateNavigatinPageWithoutRequiredFields(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditFirstEvent();		
		NavigationPages navigationPages = dashboard.navigateToNavigation();
		NavigationPage navigationPage = navigationPages.clickCreatePage();
		navigationPage.failToCreateNavigatinPageWithoutRequiredFields();
		navigationPage.checkIsShownValidationMessage();	
	}
	
	@Test(dependsOnMethods = { "login" }, priority = 7)
	public void deleteNavigationPage(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditFirstEvent();		
		NavigationPages navigationPages = dashboard.navigateToNavigation();
		int navigationPagesCountBefore = navigationPages.getNavigationPagesCount();
		NavigationPage navigationPage = navigationPages.clickDelete();
		navigationPage.deleteNavigationPage();
		navigationPages.clickNavigation();
		int navigationPagesCountAfter = navigationPages.getNavigationPagesCount();
		navigationPage.checkNavigationPagesCount(navigationPagesCountBefore, navigationPagesCountAfter);		
	}
	
	@AfterMethod
	public void setScreenshot(ITestResult result) {
		if (!result.isSuccess()){
			String methodName = result.getName();
			String filename = null;
			try
			{
				SimpleDateFormat formater = new SimpleDateFormat("yyyy_MM_dd_hh_mm_ss");
				File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
				filename = formater.format(Calendar.getInstance().getTime()) + "_" + methodName + ".png";
				String separator = System.getProperty("file.separator");
				String path = System.getProperty("user.dir") + separator + "test-output" + separator + "screens" + separator;
				FileUtils.copyFile(scrFile, new File(path + filename));
				Reporter.setCurrentTestResult(result);
				Reporter.log("<img width='100%' src='" + "screens" + separator + filename + "'/>");
			}
			catch (IOException e1)
			{
				e1.printStackTrace();
			}
			if (driver.getWindowHandles().size() > 1) {
				handler.switchToOldWindow(driver);
			}
		}
	}
	
	@AfterClass
	public void afterClass() {
		try
		{
			driver.close();
			driver.quit();
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
	} 
}
