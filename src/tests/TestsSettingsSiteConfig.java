package tests;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import eventDashboardPages.EventDashboard;
import eventDashboardPages.SettingsSiteConfig;
import organizationPages.OrganizationDashboard;
import pages.Login;
import utils.Data;
import utils.DriverSetup;
import utils.WindowHandler;

public class TestsSettingsSiteConfig {
	private WebDriver driver;
	private WindowHandler 	handler;
	private String username	= Data.userLogin;
	private String password	= Data.userPassword;
	
	@BeforeClass
	public void beforeClass() {
		driver = DriverSetup.getDriver("desktop");
		DriverSetup.initialSetup(driver);
		handler = new WindowHandler();
	}
	
	@Test
	public void loginPage() {
		Login loginPage = new Login(driver);
		ArrayList<String> locators = pages.Login.getLocators();
		loginPage.locatorsCheck(locators);
	}

	@Test(dependsOnMethods = { "loginPage" }, groups = { "Profile" })
	public void login() {
		Login loginPage = new Login(driver);
		OrganizationDashboard result = loginPage.loginAsUser(username, password);
		loginPage.checkIfLoginSuccessful(result.getTitleText());
	}
	
	@Test(dependsOnMethods = { "login" })
	public void changeSiteEventName(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditRaisedEvent();
		SettingsSiteConfig siteConfig = dashboard.navigateToSiteConfiguration();
		siteConfig.changeSiteEventName();
		dashboard.switchToEvent();
		siteConfig.checkChangedName();
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" })
	public void changeSiteEventLocationName(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditRaisedEvent();
		SettingsSiteConfig siteConfig = dashboard.navigateToSiteConfiguration();
		siteConfig.changeSiteEventLocationName();
		dashboard.switchToEvent();
		siteConfig.checkChangedLocationName();
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" })
	public void changeSiteEventAddress1(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditRaisedEvent();
		SettingsSiteConfig siteConfig = dashboard.navigateToSiteConfiguration();
		siteConfig.changeSiteEventAddress1();
		dashboard.switchToEvent();
		siteConfig.checkChangedAddress1();
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" })
	public void changeSiteEventAddress2(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditRaisedEvent();
		SettingsSiteConfig siteConfig = dashboard.navigateToSiteConfiguration();
		siteConfig.changeSiteEventAddress2();
		dashboard.switchToEvent();
		siteConfig.checkChangedAddress2();
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" })
	public void changeSiteEventCity(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditRaisedEvent();
		SettingsSiteConfig siteConfig = dashboard.navigateToSiteConfiguration();
		siteConfig.changeSiteEventCity();
		dashboard.switchToEvent();
		siteConfig.checkChangedCity();
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" })
	public void checkSiteEventUrl(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditRaisedEvent();
		SettingsSiteConfig siteConfig = dashboard.navigateToSiteConfiguration();
		String siteEventUtl = siteConfig.getSiteEventURL();
		dashboard.switchToEvent();
		siteConfig.checkSiteEventURL(siteEventUtl);
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" })
	public void changeSiteEventState(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditRaisedEvent();
		SettingsSiteConfig siteConfig = dashboard.navigateToSiteConfiguration();
		siteConfig.changeSiteEventState();
		dashboard.switchToEvent();
		siteConfig.checkChangedState();
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" })
	public void changeSiteEventZip(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditRaisedEvent();
		SettingsSiteConfig siteConfig = dashboard.navigateToSiteConfiguration();
		siteConfig.changeSiteEventZip();
		dashboard.switchToEvent();
		siteConfig.checkChangedZip();
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" })
	public void changeSiteEventContactPhone(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditRaisedEvent();
		SettingsSiteConfig siteConfig = dashboard.navigateToSiteConfiguration();
		siteConfig.changeSiteEventContactPhone();
		dashboard.switchToEvent();
		siteConfig.checkChangedContactPhone();
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" })
	public void enableSearch(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditRaisedEvent();
		SettingsSiteConfig siteConfig = dashboard.navigateToSiteConfiguration();
		siteConfig.changeSiteEventShowSearch();
		dashboard.switchToEvent();
		Boolean isDisplayed = siteConfig.isDisplayedSearch();
		siteConfig.checkShowSearch(isDisplayed);
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" })
	public void enableTeams(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditRaisedEvent();
		SettingsSiteConfig siteConfig = dashboard.navigateToSiteConfiguration();
		siteConfig.changeEnableTeams();
		dashboard.switchToEvent();
		Boolean isDisplayed = siteConfig.isDisplayedTeamButtons();
		siteConfig.checkEnableTeams(isDisplayed);
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" })
	public void siteFundraisingGoal (){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditRaisedEvent();
		SettingsSiteConfig siteConfig = dashboard.navigateToSiteConfiguration();
		siteConfig.changeSiteFundraisingGoal();
		dashboard.switchToEvent();
		String fundraisingGoal = siteConfig.getFinancialGoal();
		siteConfig.checkFundraisingGoal(fundraisingGoal);
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" }, priority = 1)
	public void enableIndividualPages(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditRaisedEvent();
		SettingsSiteConfig siteConfig = dashboard.navigateToSiteConfiguration();
		siteConfig.enableIndividualPages();
		dashboard.switchToEvent();
		Boolean isDisplayed = siteConfig.isDisplayedCreatePageWidget();
		siteConfig.checkEnabledCreatePageWidget(isDisplayed);
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" }, priority = 2)
	public void disableIndividualPages(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditRaisedEvent();
		SettingsSiteConfig siteConfig = dashboard.navigateToSiteConfiguration();
		siteConfig.disableIndividualPages();
		dashboard.switchToEvent();
		Boolean isDisplayed = siteConfig.isDisplayedCreatePageWidget();
		siteConfig.checkDesabledCreatePageWidget(isDisplayed);
		dashboard.switchToOldWindow();
	}
	
	@AfterMethod
	public void setScreenshot(ITestResult result) {
		if (!result.isSuccess()){
			String methodName = result.getName();
			String filename = null;
			try
			{
				SimpleDateFormat formater = new SimpleDateFormat("yyyy_MM_dd_hh_mm_ss");
				File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
				filename = formater.format(Calendar.getInstance().getTime()) + "_" + methodName + ".png";
				String separator = System.getProperty("file.separator");
				String path = System.getProperty("user.dir") + separator + "test-output" + separator + "screens" + separator;
				FileUtils.copyFile(scrFile, new File(path + filename));
				Reporter.setCurrentTestResult(result);
				Reporter.log("<img width='100%' src='" + "screens" + separator + filename + "'/>");
			}
			catch (IOException e1)
			{
				e1.printStackTrace();
			}
			if (driver.getWindowHandles().size() > 1) {
				handler.switchToOldWindow(driver);
			}
		}
	}
	
	@AfterClass
	public void afterClass() {
		try
		{
			driver.close();
			driver.quit();
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
	} 
}
