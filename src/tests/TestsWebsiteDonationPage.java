package tests;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import eventDashboardPages.EventDashboard;
import eventPages.DonationPage;
import organizationPages.OrganizationDashboard;
import pages.Login;
import utils.Data;
import utils.DriverSetup;

public class TestsWebsiteDonationPage {
	private WebDriver driver;
	private String username	= Data.userLogin;
	private String password	= Data.userPassword;
	
	@BeforeClass
	public void beforeClass() {
		driver = DriverSetup.getDriver("desktop");
		DriverSetup.initialSetup(driver);
	}
	
	@Test
	public void loginPage() {
		Login loginPage = new Login(driver);
		ArrayList<String> locators = pages.Login.getLocators();
		loginPage.locatorsCheck(locators);
	}

	@Test(dependsOnMethods = { "loginPage" })
	public void login() {
		Login loginPage = new Login(driver);
		OrganizationDashboard result = loginPage.loginAsUser(username, password);
		loginPage.checkIfLoginSuccessful(result.getTitleText());
	}
	
	@Test(dependsOnMethods = { "login" })
	public void setDonationPageTitle(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditFirstEvent();
		DonationPage donationPage =  dashboard.navigateToDonationPage();
		donationPage.fillDonationPageTitle();
		donationPage.clickSaveBtn();
		donationPage.goToDonationPage();
		donationPage.checkDonationPageTitle();	
		donationPage.goToMainPage();
	}
	
	@Test(dependsOnMethods = { "login" })
	public void setLeftSideContent(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditFirstEvent();
		DonationPage donationPage =  dashboard.navigateToDonationPage();
		dashboard.switchToFirstIframe();
		donationPage.fillLeftSideContent();
		dashboard.switchToDefaultContent();
		donationPage.clickSaveBtn();
		donationPage.goToDonationPage();
		donationPage.checkLeftSideContent();
		donationPage.goToMainPage();
	}
	
	@Test(dependsOnMethods = { "login" })
	public void setRightSideContent(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditFirstEvent();
		DonationPage donationPage =  dashboard.navigateToDonationPage();
		dashboard.switchToSecondIframe();
		donationPage.fillRightSideContent();
		dashboard.switchToDefaultContent();
		donationPage.clickSaveBtn();
		donationPage.goToDonationPage();
		donationPage.checkRightSideContent();
		donationPage.goToMainPage();
	}
	
	@Test(dependsOnMethods = { "login" })
	public void setNextPageContent(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditFirstEvent();
		DonationPage donationPage =  dashboard.navigateToDonationPage();
		dashboard.switchToThirdIframe();
		donationPage.fillNextPageContent();
		dashboard.switchToDefaultContent();
		donationPage.clickSaveBtn();
		donationPage.goToDonationPage();
		donationPage.goToNextPageContent();
		donationPage.cehckNextPageContent();
		donationPage.goToMainPage();
	}
	
	@Test(dependsOnMethods = { "login" })
	public void showNavigation(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditFirstEvent();
		DonationPage donationPage =  dashboard.navigateToDonationPage();
		donationPage.clickShowNavigation();
		donationPage.clickSaveBtn();
		donationPage.goToDonationPage();
		donationPage.checkNavigation();
		donationPage.goToMainPage();
	}
	
	@AfterMethod
	public void setScreenshot(ITestResult result) {
		if (!result.isSuccess()){
			String methodName = result.getName();
			String filename = null;
			try
			{
				SimpleDateFormat formater = new SimpleDateFormat("yyyy_MM_dd_hh_mm_ss");
				File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
				filename = formater.format(Calendar.getInstance().getTime()) + "_" + methodName + ".png";
				String separator = System.getProperty("file.separator");
				String path = System.getProperty("user.dir") + separator + "test-output" + separator + "screens" + separator;
				FileUtils.copyFile(scrFile, new File(path + filename));
				Reporter.setCurrentTestResult(result);
				Reporter.log("<img width='100%' src='" + "screens" + separator + filename + "'/>");
			}
			catch (IOException e1)
			{
				e1.printStackTrace();
			}
		}
	}
	
	@AfterClass
	public void afterClass() {
		try
		{
			driver.close();
			driver.quit();
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
	} 
}
