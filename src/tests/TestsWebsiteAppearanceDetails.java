package tests;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeGroups;
import org.testng.annotations.Test;

import eventDashboardPages.EventDashboard;
import eventDashboardPages.NavigationPages;
import eventPages.Appearance;
import eventPages.AppearanceDetails;
import eventPages.NavigationPage;
import organizationPages.OrganizationDashboard;
import pages.Login;
import utils.Data;
import utils.DriverSetup;

public class TestsWebsiteAppearanceDetails {
	private WebDriver driver;
	private String username	= Data.userLogin;
	private String password	= Data.userPassword;
	
	@BeforeClass
	public void beforeClass() {
		driver = DriverSetup.getDriver("desktop");
		DriverSetup.initialSetup(driver);
	}
	
	@Test
	public void loginPage() {
		Login loginPage = new Login(driver);
		ArrayList<String> locators = pages.Login.getLocators();
		loginPage.locatorsCheck(locators);
	}

	@Test(dependsOnMethods = { "loginPage" })
	public void login() {
		Login loginPage = new Login(driver);
		OrganizationDashboard result = loginPage.loginAsUser(username, password);
		loginPage.checkIfLoginSuccessful(result.getTitleText());
	}
	
	@BeforeGroups(groups = { "navigation" })
	public void createNavigation(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditFirstEvent();		
		NavigationPages navigationPages = dashboard.navigateToNavigation();
		int navigationPagesCount = navigationPages.getNavigationPagesCount();
		if(navigationPagesCount == 0){
			NavigationPage navigationPage = navigationPages.clickCreatePage();	
			navigationPage.createNavigationPage();
		}
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "navigation" })
	public void setNavigationBackgroundColor(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditFirstEvent();
		AppearanceDetails appearanceDetails = dashboard.navigateToAppearanceDetails();
		String navBackgrColorRGB = appearanceDetails.setNavigationBackgroundColor();
		appearanceDetails.clickNavigationSaveBtn();
		dashboard.switchToEvent();
		appearanceDetails.checkNavigationBackgroundColor(navBackgrColorRGB);
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "navigation" })
	public void setNavigationTextColor(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditFirstEvent();
		AppearanceDetails appearanceDetails = dashboard.navigateToAppearanceDetails();
		String navTextColorRGB = appearanceDetails.setNavigationTextColor();
		appearanceDetails.clickNavigationSaveBtn();
		dashboard.switchToEvent();
		appearanceDetails.checkNavigationTextColor(navTextColorRGB);
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "navigation" })
	public void setNavigationItemBackgroundColor(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditFirstEvent();
		AppearanceDetails appearanceDetails = dashboard.navigateToAppearanceDetails();
		String navItemBackgrColorRGB = appearanceDetails.setNavItemBackgroundColor();
		appearanceDetails.clickNavigationSaveBtn();
		dashboard.switchToEvent();
		appearanceDetails.checkNavigationItemBackgroundColor(navItemBackgrColorRGB);
		dashboard.switchToOldWindow();
	    
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "navigation" })
	public void setNavigationTextColorOn(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditFirstEvent();
		AppearanceDetails appearanceDetails = dashboard.navigateToAppearanceDetails();
		String navTextColorOnRGB = appearanceDetails.setNavTextColorOn();
		appearanceDetails.clickLabel();
		appearanceDetails.clickNavigationSaveBtn();
		dashboard.switchToEvent();
		appearanceDetails.clickNavigationItemOn();
		appearanceDetails.checkNavigationTextColorOn(navTextColorOnRGB);
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "navigation" })
	public void setNavigationItemBackgroundColorOn(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditFirstEvent();
		AppearanceDetails appearanceDetails = dashboard.navigateToAppearanceDetails();
		String navItemBackgrColorOnRGB = appearanceDetails.setNavItemBackgroundColorOn();
		appearanceDetails.clickLabel();
		appearanceDetails.clickNavigationSaveBtn();
		dashboard.switchToEvent();
		appearanceDetails.clickNavigationItemOn();
		appearanceDetails.checkNavigationItemBackgroundColorOn(navItemBackgrColorOnRGB);
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "navigation" })
	public void setNavigationTextColorHover(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditFirstEvent();
		AppearanceDetails appearanceDetails = dashboard.navigateToAppearanceDetails();
		String navTextColorHover = appearanceDetails.setNavTextColorHover();
		appearanceDetails.clickLabel();
		appearanceDetails.clickNavigationSaveBtn();
		dashboard.switchToEvent();
		appearanceDetails.clickNavigationItemOn();
		appearanceDetails.checkNavigationTextColorHover(navTextColorHover);
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "navigation" })
	public void setNavigationItemBackgroundColorHover(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditFirstEvent();
		AppearanceDetails appearanceDetails = dashboard.navigateToAppearanceDetails();
		String navItemBackgroundColorRGB = appearanceDetails.setNavItemBackgroundColorHover();
		appearanceDetails.clickLabel();
		appearanceDetails.clickNavigationSaveBtn();
		dashboard.switchToEvent();
		appearanceDetails.clickNavigationItemOn();
		appearanceDetails.checkNavigationItemBackgroundColorHover(navItemBackgroundColorRGB);
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" })
	public void changeButtonsTextColor(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditFirstEvent();
		AppearanceDetails appearanceDetails = dashboard.navigateToAppearanceDetails();
		appearanceDetails.clickButtonsTab();
		String buttonTextColorRGB = appearanceDetails.setButtonsTextColor();
		appearanceDetails.clickButtonsSaveBtn();
		dashboard.switchToEvent();
		appearanceDetails.checkButtonTextColor(buttonTextColorRGB);
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" })
	public void changeButtonsBackgrounColor(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditFirstEvent();
		AppearanceDetails appearanceDetails = dashboard.navigateToAppearanceDetails();
		appearanceDetails.clickButtonsTab();
		String buttonBackgColorRGB = appearanceDetails.setButtonsBackgroundColor();
		appearanceDetails.clickButtonsSaveBtn();
		dashboard.switchToEvent();
		appearanceDetails.checkButtonBackgroindColor(buttonBackgColorRGB);
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" })
	public void changeButtonsTextColorHover(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditFirstEvent();
		AppearanceDetails appearanceDetails = dashboard.navigateToAppearanceDetails();
		appearanceDetails.clickButtonsTab();
		String buttonTextColorHoverRGB = appearanceDetails.setButtonsTextColorHover();
		appearanceDetails.clickLabel();
		appearanceDetails.clickButtonsSaveBtn();
		dashboard.switchToEvent();
		appearanceDetails.checkButtonTextColorHover(buttonTextColorHoverRGB);
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" })
	public void changeButtonsBackgroundColorHover(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditFirstEvent();
		AppearanceDetails appearanceDetails = dashboard.navigateToAppearanceDetails();
		appearanceDetails.clickButtonsTab();
		String backgroundColorHoverRGB = appearanceDetails.setButtonsBackgroundColorHover();
		appearanceDetails.clickLabel();
		appearanceDetails.clickButtonsSaveBtn();
		dashboard.switchToEvent();
		appearanceDetails.checkButtonBackgroundColorHover(backgroundColorHoverRGB);
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" })
	public void changeWidgetHeaderTextColor(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditFirstEvent();
		AppearanceDetails appearanceDetails = dashboard.navigateToAppearanceDetails();
		appearanceDetails.clickWidgetTab();
		String widgetHeaderTextColorRGB = appearanceDetails.setWidgetHeaderTextColor();
		appearanceDetails.clickLabel();
		appearanceDetails.clickWidgetSaveBtn();
		dashboard.switchToEvent();
		appearanceDetails.checkWidgetHeaderTextColor(widgetHeaderTextColorRGB);
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" })
	public void changeWidgetHeaderBackgroundColor(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditFirstEvent();
		AppearanceDetails appearanceDetails = dashboard.navigateToAppearanceDetails();
		appearanceDetails.clickWidgetTab();
		String widgetBackgroundColorRGB = appearanceDetails.setWidgetHeaderBackgroundColor();
		appearanceDetails.clickLabel();
		appearanceDetails.clickWidgetSaveBtn();
		dashboard.switchToEvent();
		appearanceDetails.checkWidgetHeaderBackgroundColor(widgetBackgroundColorRGB);
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" })
	public void changeInnerBackgroundColor(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditFirstEvent();
		AppearanceDetails appearanceDetails = dashboard.navigateToAppearanceDetails();
		appearanceDetails.clickInnerBackgroundTab();
		String innerBackgrounColorRGB = appearanceDetails.setInnerBackgroundColor();
		appearanceDetails.clickLabel();
		appearanceDetails.clickInnerBackgroundSaveBtn();
		dashboard.switchToEvent();
		appearanceDetails.checkInnerBackgroundColor(innerBackgrounColorRGB);
		dashboard.switchToOldWindow();
	}
	
	@BeforeGroups(groups = { "footer" })
	public void setEventFooter(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditFirstEvent();
		Appearance appearance =  dashboard.navigateToAppearance();
		dashboard.switchToFirstIframe();
		appearance.fillEventFooterText();
		dashboard.switchToDefaultContent();
		appearance.clickSaveBtn();
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "footer" })
	public void changeFooterBackgroundColor(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditFirstEvent();
		AppearanceDetails appearanceDetails = dashboard.navigateToAppearanceDetails();
		appearanceDetails.clickFooterTab();
		String footerBackgroundColorRGB = appearanceDetails.setFooterBackgroundColor();
		appearanceDetails.clickFooterSaveBtn();
		dashboard.switchToEvent();
		appearanceDetails.checkFooterBackgroundColor(footerBackgroundColorRGB);
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "footer" })
	public void changeFooterTextColor(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditFirstEvent();
		AppearanceDetails appearanceDetails = dashboard.navigateToAppearanceDetails();
		appearanceDetails.clickFooterTab();
		String footerColorRGB = appearanceDetails.setFooterTextColor();
		appearanceDetails.clickLabel();
		appearanceDetails.clickFooterSaveBtn();
		dashboard.switchToEvent();
		appearanceDetails.checkFooterTextColor(footerColorRGB);
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" })
	public void changeSearchBoxTextColor(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditFirstEvent();
		AppearanceDetails appearanceDetails = dashboard.navigateToAppearanceDetails();
		appearanceDetails.clickSearchBoxTab();
		String searchBoxTextColorRGB = appearanceDetails.setSearchBoxTextColor();
		appearanceDetails.clickLabel();
		appearanceDetails.clickSearchBoxSaveBtn();
		dashboard.switchToEvent();
		appearanceDetails.checkSearchBoxTextColor(searchBoxTextColorRGB);
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" })
	public void changeSearchBoxBackgroundColor(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditFirstEvent();
		AppearanceDetails appearanceDetails = dashboard.navigateToAppearanceDetails();
		appearanceDetails.clickSearchBoxTab();
		String backgroundColorRGB = appearanceDetails.setSearchBoxBackgroundColor();
		appearanceDetails.clickLabel();
		appearanceDetails.clickSearchBoxSaveBtn();
		dashboard.switchToEvent();
		appearanceDetails.checkSearchBoxBackgroundColor(backgroundColorRGB);
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" })
	public void changeLoginTextColor(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditFirstEvent();
		AppearanceDetails appearanceDetails = dashboard.navigateToAppearanceDetails();
		appearanceDetails.clickLoginTab();
		String loginTextColorRGB = appearanceDetails.setLoginTextColor();
		appearanceDetails.clickLabel();
		appearanceDetails.clickLoginSaveBtn();
		dashboard.switchToEvent();
		appearanceDetails.checkLoginTextColor(loginTextColorRGB);
		dashboard.switchToOldWindow();
	}
	
	@AfterMethod
	public void setScreenshot(ITestResult result) {
		if (!result.isSuccess()){
			String methodName = result.getName();
			String filename = null;
			try
			{
				SimpleDateFormat formater = new SimpleDateFormat("yyyy_MM_dd_hh_mm_ss");
				File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
				filename = formater.format(Calendar.getInstance().getTime()) + "_" + methodName + ".png";
				String separator = System.getProperty("file.separator");
				String path = System.getProperty("user.dir") + separator + "test-output" + separator + "screens" + separator;
				FileUtils.copyFile(scrFile, new File(path + filename));
				Reporter.setCurrentTestResult(result);
				Reporter.log("<img width='100%' src='" + "screens" + separator + filename + "'/>");
			}
			catch (IOException e1)
			{
				e1.printStackTrace();
			}
		}
	}
	
	@AfterClass
	public void afterClass() {
		try
		{
			driver.close();
			driver.quit();
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
	} 
}
