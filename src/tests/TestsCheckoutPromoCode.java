package tests;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeGroups;
import org.testng.annotations.Test;

import eventDashboardPages.EventDashboard;
import eventPages.CheckoutItems;
import eventPages.CheckoutPromoCode;
import organizationPages.OrganizationDashboard;
import pages.Login;
import utils.Data;
import utils.DriverSetup;
import utils.WindowHandler;

public class TestsCheckoutPromoCode {
	private WebDriver driver;
	private WindowHandler handler;
	private String username	= Data.userLogin;
	private String password	= Data.userPassword;
	
	@BeforeClass
	public void beforeClass() {
		driver = DriverSetup.getDriver("desktop");
		DriverSetup.initialSetup(driver);
		this.handler = new WindowHandler();
	}
	
	@Test
	public void loginPage() {
		Login loginPage = new Login(driver);
		ArrayList<String> locators = pages.Login.getLocators();
		loginPage.locatorsCheck(locators);
	}

	@Test(dependsOnMethods = { "loginPage" })
	public void login() {
		Login loginPage = new Login(driver);
		OrganizationDashboard result = loginPage.loginAsUser(username, password);
		loginPage.checkIfLoginSuccessful(result.getTitleText());
	}
	
	@BeforeGroups(groups = { "promo-code" })
	public void createDonationItem(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditSecondOneEvent();
		CheckoutItems checkoutItem = dashboard.navigateToCheckoutItems();
		int itemsCount = checkoutItem.getItemsCount();
		if(itemsCount == 0){
			checkoutItem.clickCreateItemBtn();
			checkoutItem.createDonationItem();
		}
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "promo-code" }, priority = 1)
	public void createPromoCodeUnlimited(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditSecondOneEvent();
		CheckoutPromoCode checkoutPromoCode = dashboard.navigateToCheckoutPromoCode();
		checkoutPromoCode.clickCreatePromoCodeBtn();
		checkoutPromoCode.createPromoCode(checkoutPromoCode.getUnlimitedPromoCodeName(), checkoutPromoCode.getUnlimitedPromoCode());
		checkoutPromoCode.clickSaveBtn();
		dashboard.switchToEvent();
		checkoutPromoCode.clickEventDonationBtn();
		checkoutPromoCode.setDonationQuantity();
		String totalPayment = checkoutPromoCode.clickAddToCart();
		checkoutPromoCode.clickCompleteCheckout();
		checkoutPromoCode.paymentDataFilling(checkoutPromoCode.getUnlimitedPromoCode());
		checkoutPromoCode.checkTotalPaymentWithPromotion(totalPayment);
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" }, priority = 2)
	public void editPromoCode(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditSecondOneEvent();
		CheckoutPromoCode checkoutPromoCode = dashboard.navigateToCheckoutPromoCode();
		checkoutPromoCode.clickEditPromoCode();
		checkoutPromoCode.editPromoCode();
		dashboard.switchToEvent();
		checkoutPromoCode.clickEventDonationBtn();
		checkoutPromoCode.setDonationQuantity();
		String totalPayment = checkoutPromoCode.clickAddToCart();
		checkoutPromoCode.clickCompleteCheckout();
		checkoutPromoCode.paymentDataFilling(checkoutPromoCode.getEditCodeValue());
		checkoutPromoCode.checkTotalPaymentWithPromotion(totalPayment);
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" }, priority = 3)
	public void deletePromoCode(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditSecondOneEvent();
		CheckoutPromoCode checkoutPromoCode = dashboard.navigateToCheckoutPromoCode();
		int promoCodesCountBefore = checkoutPromoCode.getPromoCodesCount();
		checkoutPromoCode.clickDeletePromoCode();
		dashboard.handelJavaScriptAlert();
		checkoutPromoCode.goToPromoCode();
		int promoCodesCountAfter = checkoutPromoCode.getPromoCodesCount();
		checkoutPromoCode.checkIsDeletedPromoCode(promoCodesCountBefore, promoCodesCountAfter);
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "promo-code" }, priority = 4)
	public void createPromoCodeLimited(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditSecondOneEvent();
		CheckoutPromoCode checkoutPromoCode = dashboard.navigateToCheckoutPromoCode();
		checkoutPromoCode.clickCreatePromoCodeBtn();
		checkoutPromoCode.createPromoCode(checkoutPromoCode.getLimitedPromoCodeName(), checkoutPromoCode.getLimitedPomoCode());
		checkoutPromoCode.setLimitedPromoCode(checkoutPromoCode.getLimitedPromoCodeQuantity());
		checkoutPromoCode.clickSaveBtn();
		dashboard.switchToEvent();
		checkoutPromoCode.clickEventDonationBtn();
		checkoutPromoCode.setDonationQuantity();
		String totalPayment = checkoutPromoCode.clickAddToCart();
		checkoutPromoCode.clickCompleteCheckout();
		checkoutPromoCode.paymentDataFilling(checkoutPromoCode.getLimitedPomoCode());
		checkoutPromoCode.checkTotalPaymentWithPromotion(totalPayment);
		checkoutPromoCode.clickCompleteBtn();
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "promo-code" }, priority = 5)
	public void failToUseOutOfLimitPromoCode(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditSecondOneEvent();
		CheckoutPromoCode checkoutPromoCode = dashboard.navigateToCheckoutPromoCode();
		dashboard.switchToEvent();
		checkoutPromoCode.clickEventDonationBtn();
		checkoutPromoCode.setDonationQuantity();
		checkoutPromoCode.clickAddToCart();
		checkoutPromoCode.clickCompleteCheckout();
		checkoutPromoCode.paymentDataFilling(checkoutPromoCode.getLimitedPomoCode());
		checkoutPromoCode.checkPromoCodeErrorMessage();
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "promo-code" }, priority = 6)
	public void failToUseOutOfDatePromoCode(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditSecondOneEvent();
		CheckoutPromoCode checkoutPromoCode = dashboard.navigateToCheckoutPromoCode();
		checkoutPromoCode.clickCreatePromoCodeBtn();
		checkoutPromoCode.createPromoCodeOutOfDate(checkoutPromoCode.getOutOfDatePromoCode(), checkoutPromoCode.getOutOfDatePromoCode());
		checkoutPromoCode.clickSaveBtn();
		dashboard.switchToEvent();
		checkoutPromoCode.clickEventDonationBtn();
		checkoutPromoCode.setDonationQuantity();
		checkoutPromoCode.clickAddToCart();
		checkoutPromoCode.clickCompleteCheckout();
		checkoutPromoCode.paymentDataFilling(checkoutPromoCode.getOutOfDatePromoCode());
		checkoutPromoCode.checkPromoCodeErrorMessage();
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "promo-code" })
	public void failToUseNotExistedPromoCode(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditSecondOneEvent();
		CheckoutPromoCode checkoutPromoCode = dashboard.navigateToCheckoutPromoCode();
		dashboard.switchToEvent();
		checkoutPromoCode.clickEventDonationBtn();
		checkoutPromoCode.setDonationQuantity();
		checkoutPromoCode.clickAddToCart();
		checkoutPromoCode.clickCompleteCheckout();
		checkoutPromoCode.paymentDataFilling(checkoutPromoCode.getNoExistedPromoCode());
		checkoutPromoCode.checkPromoCodeErrorMessage();
		dashboard.switchToOldWindow();
	}
	
	@AfterMethod
	public void setScreenshot(ITestResult result) {
		if (!result.isSuccess()){
			String methodName = result.getName();
			String filename = null;
			try
			{
				SimpleDateFormat formater = new SimpleDateFormat("yyyy_MM_dd_hh_mm_ss");
				File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
				filename = formater.format(Calendar.getInstance().getTime()) + "_" + methodName + ".png";
				String separator = System.getProperty("file.separator");
				String path = System.getProperty("user.dir") + separator + "test-output" + separator + "screens" + separator;
				FileUtils.copyFile(scrFile, new File(path + filename));
				Reporter.setCurrentTestResult(result);
				Reporter.log("<img width='100%' src='" + "screens" + separator + filename + "'/>");
			}
			catch (IOException e1)
			{
				e1.printStackTrace();
			}
			if (driver.getWindowHandles().size() > 1) {
				handler.switchToOldWindow(driver);
			}
		}
	}
	
	@AfterClass
	public void afterClass() {
		try
		{
			driver.close();
			driver.quit();
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
	} 
}
