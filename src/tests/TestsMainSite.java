package tests;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import organizationPages.CreateOrganization;
import organizationPages.ManageOrganizationDashboard;
import organizationPages.OrganizationDashboard;
import organizationPages.OrganizationSettings;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;

import displayServices.Categories;
import displayServices.CreateCategory;
import displayServices.EventTemplates;
import eventDashboardPages.CreateEvent;
import eventDashboardPages.Items;
import eventDashboardPages.EventDashboard;
import eventDashboardPages.EventEdit;
import eventDashboardPages.HomepageDashboard;
import eventDashboardPages.NavigationPages;
import eventDashboardPages.WebsiteAppearance;
import homepageDashboardPages.HomepageEdit;
import pages.Login;
import pages.Menu;
import pages.Registration;
import utils.Data;
import utils.DriverSetup;
import utils.WindowHandler;


public class TestsMainSite {
	
	private WebDriver		driver;
	private WindowHandler 	handler;
	private String			username	= Data.userLogin;
	private String			password	= Data.userPassword;
	
	@BeforeClass
	public void beforeClass() {
		driver = DriverSetup.getDriver("desktop");
		DriverSetup.initialSetup(driver);
		this.handler = new WindowHandler();
	}
	
	@Test
	public void loginPage() {
		Login loginPage = new Login(driver);
		ArrayList<String> locators = pages.Login.getLocators();
		loginPage.locatorsCheck(locators);
	}

	@Test(dependsOnMethods = { "loginPage" }, groups = { "Profile" })
	public void login() {
		Login loginPage = new Login(driver);
		OrganizationDashboard result = loginPage.loginAsUser(username, password);
		loginPage.checkIfLoginSuccessful(result.getTitleText());
	}
	
	@Test( groups = { "Profile" })
	public void registrationSuccess() {
		Registration registration = new Registration(driver);
		registration.dataFilling();
		registration.checkLoginAfterRegistration();
	}
	
	@Test( groups = { "Profile" })
	public void registrationFailed() {
		Registration registration = new Registration(driver);
		registration.clickContinue();
		registration.checkErrorMessagesCount();
		registration.locatorsCheck();
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
	public void organizationDashboardShowEntries() {
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		dashboard.showEntries();
		dashboard.locatorsCheck();
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
	public void organizationDashboardSortByName() {
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		dashboard.searchClear();
		dashboard.sortByName();
		dashboard.checkSorting(1);
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
	public void organizationDashboardSortByRaisedCount() {
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		dashboard.searchClear();
		dashboard.sortByRaisedCount();
		dashboard.checkIntSortingForParsedValues(2);
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
	public void organizationDashboardSortByEventsCount() {
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		dashboard.searchClear();
		dashboard.sortByEventsCount();
		dashboard.checkIntSorting(3);
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
	public void organizationDashboardSortBySupersitesCount() {
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		dashboard.searchClear();
		dashboard.sortBySupersitesCount();
		dashboard.checkIntSorting(4);
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
	public void organizationDashboardSortByCreateDate() {
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		dashboard.searchClear();
		dashboard.sortByCreateDate();
		dashboard.checkSorting(5);
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
	public void organizationDashboardSearchByName() {
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		dashboard.searchBy("Another");
		dashboard.checkDataAfterSearch(1, "Another");
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
	public void organizationDashboardSearchByCreateDate() {
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		dashboard.searchBy("08-31-2015");
		dashboard.checkDataAfterSearch(5, "08-31-2015");
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
	public void organizationDashboardOverviewTotalRaised() {
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		dashboard.checkOverviewNumbers(dashboard.getTotalRaisedValue(), dashboard.getTotalRaisedValueFromTable());
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
	public void organizationDashboardOverviewSupersitesCount() {
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		dashboard.checkOverviewNumbers(dashboard.getSupersitesValue(), dashboard.getValueFromTable(4));
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
	public void organizationDashboardOverviewEventsCount() {
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		dashboard.checkOverviewNumbers(dashboard.geEventsValue(), dashboard.getValueFromTable(3));
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
	public void organizationDashboardOrganizationCount() {
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		dashboard.searchClear();
		dashboard.checkOverviewNumbers(dashboard.getOrganizationCountValue(), dashboard.getCountFromTableFirstColumn());
	}
	
//	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
//	public void organizationDashboardHelp() {
//		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
//		Help help = dashboard.clickHelp();
//		help.clickOrganizationExplained();
//		help.clickSsExplained();
//		help.clickMsExplained();
//		help.clickDisplayExplained();
//		help.clickSystemExplained();
//		help.locatorsCheck();
//		help.clickCloseHelp();
//	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
	public void manageOrganizationDashboardPagesCount() {
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		dashboard.searchBy("TestOrganization");
		dashboard.clickManageOnFirstElement();
		dashboard.checkOverviewNumbers(dashboard.getPagesValue(), dashboard.getValueFromTable(3));
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
	public void manageOrganizationDashboardSupersitesCount() {
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		dashboard.searchBy("TestOrganization");
		dashboard.clickManageOnFirstElement();
		dashboard.checkOverviewNumbers(dashboard.getSupersiteCountValue(), dashboard.getCountFromTableFirstColumn());
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
	public void manageOrganizationDashboardSortByName() {
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		dashboard.searchBy("TestOrganization");
		dashboard.clickManageOnFirstElement();
		dashboard.searchClear();
		dashboard.sortByName();
		dashboard.checkSorting(1);
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
	public void manageOrganizationDashboardSortByRaisedCount() {
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		dashboard.searchBy("TestOrganization");
		dashboard.clickManageOnFirstElement();
		dashboard.searchClear();
		dashboard.sortByRaisedCount();
		dashboard.checkIntSortingForParsedValues(2);
	}
	
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
	public void manageOrganizationDashboardSortByPagesCount() {
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		dashboard.searchBy("TestOrganization");
		dashboard.clickManageOnFirstElement();
		dashboard.searchClear();
		ManageOrganizationDashboard manage = new ManageOrganizationDashboard(driver);
		manage.sortByPageCount();
		dashboard.checkSorting(3);
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
	public void manageOrganizationDashboardSortByEventsCount() {
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		dashboard.searchBy("TestOrganization");
		dashboard.clickManageOnFirstElement();
		dashboard.searchClear();
		ManageOrganizationDashboard manage = new ManageOrganizationDashboard(driver);
		manage.sortByEventsCount();
		dashboard.checkNumbersSorting(4);
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
	public void manageOrganizationDashboardSearchByName() {
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		dashboard.searchBy("TestOrganization");
		dashboard.clickManageOnFirstElement();
		dashboard.searchBy("One");
		dashboard.checkDataAfterSearch(1, "One");
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
	public void manageOrganizationDashboardSearchByCreateDate() {
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		dashboard.searchBy("TestOrganization");
		dashboard.clickManageOnFirstElement();
		dashboard.searchBy("08-31-2015");
		dashboard.checkDataAfterSearch(5, "08-31-2015");
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
	public void manageOrganizationDashboardOverviewTotalRaised() {
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		dashboard.searchBy("TestOrganization");
		dashboard.clickManageOnFirstElement();
		dashboard.checkOverviewNumbers(dashboard.getTotalRaisedValue(), dashboard.getTotalRaisedValueFromTable());
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
	public void manageOrganizationDashboardOverviewPagesCount() {
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		dashboard.searchBy("TestOrganization");
		dashboard.clickManageOnFirstElement();
		dashboard.checkOverviewNumbers(dashboard.getPagesValue(), dashboard.getValueFromTable(3));
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
	public void manageOrganizationDashboardOverviewEventsCount() {
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		dashboard.searchBy("TestOrganization");
		dashboard.clickManageOnFirstElement();
		dashboard.checkOverviewNumbers(dashboard.geEventsValue(), dashboard.getValueFromTable(4));
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
	public void manageOrganizationDashboardShowEntries() {
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		dashboard.searchBy("TestOrganization");
		dashboard.clickManageOnFirstElement();
		dashboard.showEntries();
		ManageOrganizationDashboard manage = new ManageOrganizationDashboard(driver);
		manage.locatorsCheck();
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
	public void createOrganization() {
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		CreateOrganization createSol = dashboard.clickCreateOrganization();
		createSol.setOrganizationName(utils.Data.organizationName);
		createSol.clickCreateOrganization();
		Menu menu = dashboard.clickMenu();
		menu.clickOrganizationDashboard();
		dashboard.checkData(utils.Data.organizationName);
		dashboard.clickMenu();
		ManageOrganizationDashboard manage = menu.manageOrganization(utils.Data.organizationName);
		manage.checkName(utils.Data.organizationName);
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
	public void createOrganizationWithEmptyName() {
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		CreateOrganization createSol = dashboard.clickCreateOrganization();
		createSol.setOrganizationName("");
		createSol.clickCreateOrganization();
		createSol.checkErrorMessage();
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
	public void createOrganizationAfterError() {
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		CreateOrganization createSol = dashboard.clickCreateOrganization();
		createSol.setOrganizationName("");
		createSol.clickCreateOrganization();
		createSol.checkErrorMessage();
		createSol.setOrganizationName(utils.Data.organizationNameErr);
		createSol.clickCreateOrganization();
		Menu menu = dashboard.clickMenu();
		menu.clickOrganizationDashboard();
		dashboard.checkDataAfterError(utils.Data.organizationNameErr);
		dashboard.clickMenu();
		ManageOrganizationDashboard manage = menu.manageOrganization(utils.Data.organizationNameErr);
		manage.checkName(utils.Data.organizationNameErr);
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
	public void updateOrganizationNameBySettings() {
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		Menu menu = dashboard.clickMenu();
		ManageOrganizationDashboard manage = menu.manageOrganization(utils.Data.uniqueSolName);
		OrganizationSettings settings = manage.clickSettings();
		settings.setOrganizationName(utils.Data.organizationRename);
		settings.clickSubmit();
		dashboard.clickMenu();
		menu.clickOrganizationDashboard();
		dashboard.checkRenamedOrganization(utils.Data.organizationRename);
		dashboard.clickMenu();
		menu.manageOrganization(utils.Data.organizationRename);
		manage.checkName(utils.Data.organizationRename);
		manage.clickSettings();
		settings.setOrganizationName(utils.Data.uniqueSolName);
		settings.clickSubmit();
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
	public void updateSetEmptyOrganizationNameBySettings() {
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		CreateOrganization createSol = dashboard.clickCreateOrganization();
		createSol.setOrganizationName(utils.Data.organizationForTest);
		createSol.clickCreateOrganization();
		Menu menu = dashboard.clickMenu();
		ManageOrganizationDashboard manage = menu.manageOrganization(utils.Data.organizationForTest);
		OrganizationSettings settings = manage.clickSettings();
		settings.setOrganizationName("");
		settings.clickSubmit();
		settings.checkErrorMessageOnSettings();
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
	public void updateSetOrganizationNameAfterErrorBySettings() {
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		Menu menu = dashboard.clickMenu();
		ManageOrganizationDashboard manage = menu.manageOrganization(utils.Data.uniqueSolName);
		OrganizationSettings settings = manage.clickSettings();
		settings.setOrganizationName("");
		settings.clickSubmit();
		settings.setOrganizationName(utils.Data.organizationRename);
		settings.clickSubmit();
		dashboard.clickMenu();
		menu.clickOrganizationDashboard();
		dashboard.checkRenamedOrganization(utils.Data.organizationRename);
		dashboard.clickMenu();
		menu.manageOrganization(utils.Data.organizationRename);
		manage.checkName(utils.Data.organizationRename);
		manage.clickSettings();
		settings.setOrganizationName(utils.Data.uniqueSolName);
		settings.clickSubmit();
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
	public void eventDashboardSortByName() {
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		Menu menu = dashboard.clickMenu();
		EventDashboard msDashboard = menu.clickEvent();
		msDashboard.showEntries();
		msDashboard.searchClear();
		msDashboard.sortByName();
		msDashboard.checkSorting(1);
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
	public void eventDashboardSortBySsName() {
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		Menu menu = dashboard.clickMenu();
		EventDashboard msDashboard = menu.clickEvent();
		msDashboard.showEntries();
		msDashboard.searchClear();
		msDashboard.sortBySsName();
		msDashboard.checkSorting(2);
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
	public void eventDashboardSortByRaisedCount() {
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		Menu menu = dashboard.clickMenu();
		EventDashboard msDashboard = menu.clickEvent();
		msDashboard.showEntries();
		msDashboard.searchClear();
		msDashboard.sortByRaisedCount();
		msDashboard.checkSortingForIntegerValues(3);
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
	public void eventDashboardSortByDonorsCount() {
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		Menu menu = dashboard.clickMenu();
		EventDashboard msDashboard = menu.clickEvent();
		msDashboard.showEntries();
		msDashboard.searchClear();
		msDashboard.sortByDonorsCount();
		msDashboard.checkSorting(4);
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
	public void eventDashboardSortByPageCount() {
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		Menu menu = dashboard.clickMenu();
		EventDashboard msDashboard = menu.clickEvent();
		msDashboard.showEntries();
		msDashboard.searchClear();
		msDashboard.sortByPageCount();
		msDashboard.checkSorting(5);
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
	public void eventDashboardSortByCreateDate() {
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		Menu menu = dashboard.clickMenu();
		EventDashboard msDashboard = menu.clickEvent();
		msDashboard.showEntries();
		msDashboard.searchClear();
		msDashboard.sortByCreateDate();
		msDashboard.checkSorting(6);
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
	public void eventDashboardSearchByName() {
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		Menu menu = dashboard.clickMenu();
		EventDashboard msDashboard = menu.clickEvent();
		msDashboard.searchBy("Anna");
		msDashboard.checkDataAfterSearch(1, "Anna");
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
	public void eventDashboardSearchBySsName() {
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		Menu menu = dashboard.clickMenu();
		EventDashboard msDashboard = menu.clickEvent();
		msDashboard.searchBy("FirstSupersite");
		msDashboard.checkDataAfterSearch(2, "FirstSupersite");
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
	public void eventDashboardOverviewTotalRaised() {
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		Menu menu = dashboard.clickMenu();
		EventDashboard msDashboard = menu.clickEvent();
		msDashboard.showEntries();
		msDashboard.checkOverviewNumbers(msDashboard.getTotalRaisedValue(), msDashboard.getTotalRaisedValueFromTable());
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
	public void eventDashboardOverviewDonors() {
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		Menu menu = dashboard.clickMenu();
		EventDashboard msDashboard = menu.clickEvent();
		msDashboard.showEntries();
		msDashboard.checkOverviewNumbers(msDashboard.getDonorsCount(), msDashboard.getValueFromTable(4));
		
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
	public void eventDashboardOverviewPages() {
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		Menu menu = dashboard.clickMenu();
		EventDashboard msDashboard = menu.clickEvent();
		msDashboard.showEntries();
		msDashboard.checkOverviewNumbers(msDashboard.getPagesValue(), msDashboard.getValueFromTable(5));
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
	public void eventDashboardEventsCount() {
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		Menu menu = dashboard.clickMenu();
		EventDashboard msDashboard = menu.clickEvent();
		msDashboard.showEntries();
		msDashboard.searchClear();
		msDashboard.checkOverviewNumbers(msDashboard.getEventsCountValue(), msDashboard.getCountFromTableFirstColumn());
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
	public void createEvent() {
		HomepageDashboard dashboard = new HomepageDashboard(driver);
		dashboard.searchBy("One more test");
		dashboard.clickManage();
		CreateEvent create = dashboard.clickCreateEvent();
		create.fillEventDescription();
		create.setTime();
		create.setEventDate();
		create.fillEventData();
		EventEdit msEdit = create.clickSaveAndDone();
		msEdit.clickWizzardView();
		create.checkCreatedEvent();
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
	public void createEventWithFractionalFundraisingGoal() {
		HomepageDashboard dashboard = new HomepageDashboard(driver);
		dashboard.searchBy("One more test");
		dashboard.clickManage();
		CreateEvent create = dashboard.clickCreateEvent();
		create.fillEventDescription();
		create.setTime();
		create.setEventDate();
		create.fillEventData("fractGoal","125.50");
		EventEdit msEdit = create.clickSaveAndDone();
		msEdit.clickWizzardView();
		create.checkFundraisingGoal("125.50");
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
	public void createEventWithChangedUrl() {
		HomepageDashboard dashboard = new HomepageDashboard(driver);
		dashboard.searchBy("One more test");
		dashboard.clickManage();
		CreateEvent create = dashboard.clickCreateEvent();
		create.fillEventData("createChangeUrl","125");
		create.setUrl("changedurl");
		EventEdit msEdit = create.clickSaveAndDone();
		msEdit.clickWizzardView();
		create.checkUrl("changedurl");
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
	public void createEventAppearanceBackgroundColors() {
		HomepageDashboard dashboard = new HomepageDashboard(driver);
		dashboard.searchBy("One more test");
		dashboard.clickManage();
		CreateEvent create = dashboard.clickCreateEvent();
		create.fillEventData("createBackColors","225");
		create.clickSaveAndContinue();
		WebsiteAppearance appearance = new WebsiteAppearance(driver);
		appearance.setBackgroundColor(utils.Data.red);
		appearance.setNavigationBackgroundColor(utils.Data.blue);
		appearance.setButtonBackgroundColor(utils.Data.green);
		EventEdit msEdit = create.clickSaveAndDoneFromAppearancePage();
		msEdit.clickWizzardView();
		create.clickAppearance();
		appearance.checkBackgroundColors(utils.Data.red, utils.Data.blue, utils.Data.green);
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
	public void createEventAppearanceHtmlBanner() {
		HomepageDashboard dashboard = new HomepageDashboard(driver);
		dashboard.searchBy("One more test");
		dashboard.clickManage();
		CreateEvent create = dashboard.clickCreateEvent();
		create.fillEventData("createHtmlBanner","225");
		create.clickSaveAndContinue();
		WebsiteAppearance appearance = new WebsiteAppearance(driver);
		appearance.setHtmlBanner(utils.Data.htmlBannerValue);
		EventEdit msEdit = create.clickSaveAndDoneFromAppearancePage();
		msEdit.clickWizzardView();
		create.clickAppearance();
		appearance.checkHtmlBanner(utils.Data.htmlBannerValue);
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
	public void createEventNavigationPages() {
		HomepageDashboard dashboard = new HomepageDashboard(driver);
		dashboard.searchBy("One more test");
		dashboard.clickManage();
		CreateEvent create = dashboard.clickCreateEvent();
		create.fillEventData("createNavPages","235");
		create.clickSaveAndContinue();
		NavigationPages navPages = create.clickNavigationPages();
		navPages.clickCreatePage();
		navPages.setPageTitle("test title");
		navPages.setNavigationLabel("test navigation label");
		navPages.setPagePermalink("testlink");
		navPages.setPageContent("long content");
		navPages.clickSaveButton();
		navPages.clickNavPageEdit("test title");
		navPages.checkNavigationPage("test title", "test navigation label", "testlink", "long content");
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
	public void createEventTicketsAndItems() {
		HomepageDashboard dashboard = new HomepageDashboard(driver);
		dashboard.searchBy("One more test");
		dashboard.clickManage();
		CreateEvent create = dashboard.clickCreateEvent();
		create.fillEventData("createTicketsAndItems","255");
		create.clickSaveAndContinue();
		Items items = create.clickTicketsAndItems();
		items.clickCreateItem();
		items.changeItemCategory("Donation");
		items.setItemName("Test item");
		items.setAmount("25");
		items.setTaxDeductible("50");
		items.clickSaveButton();
		items.clickItemsEdit("Test item");
		items.checkItemCategory(items.getItemCategory("Test item"), "Donation");
		items.checkItem("Test item", "25", "50");
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
	public void createEventValidationErrors() {
		HomepageDashboard dashboard = new HomepageDashboard(driver);
		dashboard.searchBy("One more test");
		dashboard.clickManage();
		CreateEvent create = dashboard.clickCreateEvent();
		create.clickSaveAndContinue();
		create.checkErrorCount();
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
	public void createCategoryForMsTemplate() {
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		Menu menu = dashboard.clickMenu();
		Categories categories = menu.clickCategories();
		CreateCategory createCategory = categories.clickCreateCategory();
		createCategory.setCategoryName("CategoryForMsTemplate");
		createCategory.setCategoryDescription("Automatically created by test");
		createCategory.clickSaveButton();
		categories.search("CategoryForMsTemplate");
		categories.checkIfCreatedCategoryExist("CategoryForMsTemplate");
	}
	
	@Test(dependsOnMethods = { "login", "createCategoryForMsTemplate" }, groups = { "Profile" }) 
	public void saveAsTemplate() {
		OrganizationDashboard dashboard = new OrganizationDashboard(driver);
		dashboard.searchBy("TestOrganization");
		dashboard.clickManageOnFirstElement();
		HomepageEdit ssEdit = new HomepageEdit(driver);
		dashboard.searchBy("One more test");
		dashboard.clickManageOnFirstElement();
		CreateEvent create = ssEdit.clickCreateEvent();
		create.fillEventDescription();
		create.setTime();
		create.setEventDate();
		create.fillEventData("msForTemplate","450");
		EventEdit msEdit = create.clickSaveAndDone();
		msEdit.makeSitePublic();
		dashboard.clickMenu();
		Menu menu = new Menu(driver);
		EventDashboard msDashboard = menu.clickEvent();
		msDashboard.searchBy("msForTemplate");
		EventTemplates msTemplate = msDashboard.clickSaveAsTemplate();
		msTemplate.selectCategory("CategoryForMsTemplate");
		msTemplate.clickSave();
		msTemplate.searchBy("msForTemplate");
		msTemplate.clickEdit();
		create.customCheckCreatedEvent("msForTemplate", "450");
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
	public void copyEventCheckGeneralInfoAndColors() {
		HomepageDashboard dashboard = new HomepageDashboard(driver);
		dashboard.searchBy("One more test");
		dashboard.clickManage();
		CreateEvent create = dashboard.clickCreateEvent();
		create.fillEventDescription();
		create.setTime();
		create.setEventDate();
		create.fillEventData("siteForCopyGenInfo","150");
		create.clickSaveAndContinue();
		WebsiteAppearance appearance = new WebsiteAppearance(driver);
		appearance.setBackgroundColor(utils.Data.blue);
		appearance.setNavigationBackgroundColor(utils.Data.red);
		appearance.setButtonBackgroundColor(utils.Data.yellow);
		EventEdit msEdit = create.clickSaveAndDoneFromAppearancePage();
		msEdit.makeSitePublic();
		dashboard.clickMenu();
		Menu menu = new Menu(driver);
		EventDashboard msDashboard = menu.clickEvent();
		msDashboard.searchBy("siteForCopyGenInfo");
		msDashboard.clickCopy();
		msEdit.clickWizzardView();
		create.customCheckCreatedEvent("siteForCopyGenInfo", "150");
		create.clickAppearance();
		appearance.checkBackgroundColors(utils.Data.blue, utils.Data.red, utils.Data.yellow);
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
	public void copyEventCheckNavigationPage() {
		HomepageDashboard dashboard = new HomepageDashboard(driver);
		dashboard.searchBy("One more test");
		dashboard.clickManage();
		CreateEvent create = dashboard.clickCreateEvent();
		create.fillEventData("siteForCopyNavPage","235");
		create.clickSaveAndContinue();
		NavigationPages navPages = create.clickNavigationPages();
		navPages.clickCreatePage();
		navPages.setPageTitle("test title");
		navPages.setNavigationLabel("test navigation label");
		navPages.setPagePermalink("testlink");
		navPages.setPageContent("long content");
		navPages.clickSaveButton();
		navPages.switchToAdvancedView();
		dashboard.clickMenu();
		Menu menu = new Menu(driver);
		EventDashboard msDashboard = menu.clickEvent();
		msDashboard.searchBy("siteForCopyNavPage");
		EventEdit msEdit = msDashboard.clickCopy();
		msEdit.clickWizzardView();
		create.clickNavigationPages();
		navPages.clickNavPageEdit("test title");
		navPages.checkNavigationPage("test title", "test navigation label", "testlink", "long content");
	}
	
	@Test(dependsOnMethods = { "login" }, groups = { "Profile" })
	public void copyEventCheckItem() {
		HomepageDashboard dashboard = new HomepageDashboard(driver);
		dashboard.searchBy("One more test");
		dashboard.clickManage();
		CreateEvent create = dashboard.clickCreateEvent();
		create.fillEventData("siteForCopyItem","255");
		create.clickSaveAndContinue();
		Items items = create.clickTicketsAndItems();
		items.clickCreateItem();
		items.changeItemCategory("Donation");
		items.setItemName("Test item");
		items.setAmount("25");
		items.setTaxDeductible("50");
		items.clickSaveButton();
		items.switchToAdvancedView();
		dashboard.clickMenu();
		Menu menu = new Menu(driver);
		EventDashboard msDashboard = menu.clickEvent();
		msDashboard.searchBy("siteForCopyItem");
		EventEdit msEdit = msDashboard.clickCopy();
		msEdit.clickWizzardView();
		create.clickTicketsAndItems();
		items.clickItemsEdit("Test item");
		items.checkItemCategory(items.getItemCategory("Test item"), "Donation");
		items.checkItem("Test item", "25", "50");
	}

	
	@AfterMethod
	public void setScreenshot(ITestResult result) {
		
		if (!result.isSuccess()){
			String methodName = result.getName();
			String filename = null;
			try
			{
				SimpleDateFormat formater = new SimpleDateFormat("yyyy_MM_dd_hh_mm_ss");
				File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
				filename = formater.format(Calendar.getInstance().getTime()) + "_" + methodName + ".png";
				String separator = System.getProperty("file.separator");
				String path = System.getProperty("user.dir") + separator + "test-output" + separator + "screens" + separator;
				FileUtils.copyFile(scrFile, new File(path + filename));
				Reporter.setCurrentTestResult(result);
				Reporter.log("<img width='100%' src='" + "screens" + separator + filename + "'/>");
				
			}
			catch (IOException e1)
			{
				e1.printStackTrace();
			}
			if (driver.getWindowHandles().size() > 1) {
				handler.switchToOldWindow(driver);
			}	
		}
	}

	@AfterClass
	public void afterClass() {
		try
		{
			driver.close();
			driver.quit();
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
	} 

}
