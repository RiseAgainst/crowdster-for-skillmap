package tests;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import eventDashboardPages.EventDashboard;
import eventDashboardPages.WebsiteMessages;
import eventPages.Event;
import eventPages.EventDonation;
import eventPages.PaymentInfo;
import organizationPages.OrganizationDashboard;
import pages.Login;
import utils.Data;
import utils.DriverSetup;
import utils.WindowHandler;

public class TestsWebsiteMessages {
	private WebDriver		driver;
	private WindowHandler 	handler;
	private String			username	= Data.userLogin;
	private String			password	= Data.userPassword;
	
	@BeforeClass
	public void beforeClass() {
		driver = DriverSetup.getDriver("desktop");
		DriverSetup.initialSetup(driver);
		handler = new WindowHandler();
	}
	
	@Test
	public void loginPage() {
		Login loginPage = new Login(driver);
		ArrayList<String> locators = pages.Login.getLocators();
		loginPage.locatorsCheck(locators);
	}

	@Test(dependsOnMethods = { "loginPage" })
	public void login() {
		Login loginPage = new Login(driver);
		OrganizationDashboard result = loginPage.loginAsUser(username, password);
		loginPage.checkIfLoginSuccessful(result.getTitleText());
	}
	
	@Test(dependsOnMethods = { "login" })
	public void thankYouMessageAfterDonation(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditFirstEvent();		
		WebsiteMessages websiteMessages = dashboard.navigateToWebsiteMessages();
		dashboard.switchToFirstIframe();
		websiteMessages.fillThankYouMessage();
		dashboard.switchToDefaultContent();
		Event event = websiteMessages.clickSubmitBtn();
		dashboard.switchToEvent();
		EventDonation msDonation = event.clickDonationButton();	
		msDonation.setDonationAmount(utils.Data.donationAmount);
		PaymentInfo payment = msDonation.clickCompleteCheckout();
		payment.paymentDataFilling();
		payment.acceptTermsAndconditions();
		payment.clickContinue();
		payment.clickCompleteButton();
		websiteMessages.checkThankYouMessage();
		dashboard.switchToOldWindow();
	
	}
	
	@Test(dependsOnMethods = { "login" })
	public void uploadSocialImage(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditFirstEvent();		
		WebsiteMessages websiteMessages = dashboard.navigateToWebsiteMessages();
		websiteMessages.selectUploadImage(websiteMessages.getFileNameToUpload());
		websiteMessages.clickSubmitBtn();
		dashboard.switchToEvent();
		websiteMessages.checkUploadedImageNameOnAnyMetaElement();
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" })
	public void facebookTitle(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditFirstEvent();		
		WebsiteMessages websiteMessages = dashboard.navigateToWebsiteMessages();
		websiteMessages.fillFacebookTitle();
		websiteMessages.clickSubmitBtn();
		dashboard.switchToEvent();
		websiteMessages.checkFacebookTitleOnAnyMetaElement();
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" })
	public void facebookMessage(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditFirstEvent();		
		WebsiteMessages websiteMessages = dashboard.navigateToWebsiteMessages();
		websiteMessages.fillFacebookMessage();
		websiteMessages.clickSubmitBtn();
		dashboard.switchToEvent();
		websiteMessages.checkFacebookMessageOnAnyMetaElement();
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" })
	public void twitterMessage(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditFirstEvent();		
		WebsiteMessages websiteMessages = dashboard.navigateToWebsiteMessages();
		websiteMessages.fillTwitterMessage();
		websiteMessages.clickSubmitBtn();
		dashboard.switchToEvent();
		websiteMessages.clickTwitterIcon();
		websiteMessages.checkTwitterMessage();
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" })
	public void emailMessage(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditFirstEvent();		
		WebsiteMessages websiteMessages = dashboard.navigateToWebsiteMessages();
		websiteMessages.fillEmailMessage();
		websiteMessages.clickSubmitBtn();
		dashboard.switchToEvent();
		websiteMessages.clickEmailIcon();
		dashboard.switchToThirdIframe();
		websiteMessages.checkEmailMessage();
		dashboard.switchToDefaultContent();
		dashboard.switchToOldWindow();
	}
	
	@AfterMethod
	public void setScreenshot(ITestResult result) {
		if (!result.isSuccess()){
			String methodName = result.getName();
			String filename = null;
			try
			{
				SimpleDateFormat formater = new SimpleDateFormat("yyyy_MM_dd_hh_mm_ss");
				File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
				filename = formater.format(Calendar.getInstance().getTime()) + "_" + methodName + ".png";
				String separator = System.getProperty("file.separator");
				String path = System.getProperty("user.dir") + separator + "test-output" + separator + "screens" + separator;
				FileUtils.copyFile(scrFile, new File(path + filename));
				Reporter.setCurrentTestResult(result);
				Reporter.log("<img width='100%' src='" + "screens" + separator + filename + "'/>");
			}
			catch (IOException e1)
			{
				e1.printStackTrace();
			}
			if (driver.getWindowHandles().size() > 1) {
				handler.switchToOldWindow(driver);
			}
		}
	}
	
	@AfterClass
	public void afterClass() {
		try
		{
			driver.close();
			driver.quit();
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
	} 
}
