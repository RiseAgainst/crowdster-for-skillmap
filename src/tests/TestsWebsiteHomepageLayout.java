package tests;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import eventDashboardPages.EventDashboard;
import eventDashboardPages.WebsiteHomepageLayout;
import organizationPages.OrganizationDashboard;
import pages.Login;
import utils.Data;
import utils.DriverSetup;
import utils.WindowHandler;

public class TestsWebsiteHomepageLayout {
	private WebDriver 		driver;
	private WindowHandler 	handler;
	private String username	= Data.userLogin;
	private String password	= Data.userPassword;
	
	@BeforeClass
	public void beforeClass() {
		driver = DriverSetup.getDriver("desktop");
		DriverSetup.initialSetup(driver);
		handler = new WindowHandler();
	}
	
	@Test
	public void loginPage() {
		Login loginPage = new Login(driver);
		ArrayList<String> locators = pages.Login.getLocators();
		loginPage.locatorsCheck(locators);
	}

	@Test(dependsOnMethods = { "loginPage" }, groups = { "Profile" })
	public void login() {
		Login loginPage = new Login(driver);
		OrganizationDashboard result = loginPage.loginAsUser(username, password);
		loginPage.checkIfLoginSuccessful(result.getTitleText());
	}
	
	@Test(dependsOnMethods = { "login" })
	public void displayCreatePageWidget(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditSecondOneEvent();
		WebsiteHomepageLayout homepageLayout =  dashboard.navigateToHomepageLayout();
		homepageLayout.addCreatePageWidget();
		dashboard.switchToEvent();
		Boolean isDisplayed = homepageLayout.isDisplayedEventWidget(homepageLayout.getEventCreatePageWidgetIdLocator());
		homepageLayout.checkIsDisplayedEventWidget(isDisplayed);
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" })
	public void displayFundraisingWidget(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditSecondOneEvent();
		WebsiteHomepageLayout homepageLayout =  dashboard.navigateToHomepageLayout();
		homepageLayout.addFundraisingWidget();
		dashboard.switchToEvent();
		Boolean isDisplayed = homepageLayout.isDisplayedEventWidget(homepageLayout.getEventFundraisingIdLocator());
		homepageLayout.checkIsDisplayedEventWidget(isDisplayed);
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" })
	public void displayMapAndDirectionsWidget(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditSecondOneEvent();
		WebsiteHomepageLayout homepageLayout =  dashboard.navigateToHomepageLayout();
		homepageLayout.addMapAndDirectionsWidget();
		dashboard.switchToEvent();
		Boolean isDisplayed = homepageLayout.isDisplayedEventWidget(homepageLayout.getEventFundraisingIdLocator());
		homepageLayout.checkIsDisplayedEventWidget(isDisplayed);
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" })
	public void displayCampaignsMainWidget(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditSecondOneEvent();
		WebsiteHomepageLayout homepageLayout =  dashboard.navigateToHomepageLayout();
		homepageLayout.addCampaignsMainWidget();
		dashboard.switchToEvent();
		Boolean isDisplayed = homepageLayout.isDisplayedEventWidget(homepageLayout.getCampaignsIdLocator());
		homepageLayout.checkIsDisplayedEventWidget(isDisplayed);
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" })
	public void displayFreeFormHtmlWidget(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditSecondOneEvent();
		WebsiteHomepageLayout homepageLayout =  dashboard.navigateToHomepageLayout();
		homepageLayout.addFreeFormHtmlWidget();
		homepageLayout.clickEditFreeFormHtml();
		dashboard.switchToIframe(homepageLayout.getIframCssLocator());
		homepageLayout.fillTextEditor();
		dashboard.switchToDefaultContent();
		homepageLayout.clickSaveBtn();
		dashboard.switchToEvent();
		homepageLayout.checkFreeFormText();
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" })
	public void displayFreeFormHtml2Widget(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditSecondOneEvent();
		WebsiteHomepageLayout homepageLayout =  dashboard.navigateToHomepageLayout();
		homepageLayout.addFreeFormHtml2Widget();
		homepageLayout.clickEditFreeFormHtml2();
		dashboard.switchToIframe(homepageLayout.getIframCssLocator());
		homepageLayout.fillTextEditor();
		dashboard.switchToDefaultContent();
		homepageLayout.clickSaveBtn();
		dashboard.switchToEvent();
		homepageLayout.checkFreeForm2Text();
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" })
	public void editEventName(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditSecondOneEvent();
		WebsiteHomepageLayout homepageLayout =  dashboard.navigateToHomepageLayout();
		homepageLayout.clickEventNameInput();
		homepageLayout.editEventName();
		dashboard.switchToEvent();
		homepageLayout.checkEventName();
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" })
	public void changeEventDateTime(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditSecondOneEvent();
		WebsiteHomepageLayout homepageLayout =  dashboard.navigateToHomepageLayout();
		homepageLayout.clickEventDateInput();
		String eventDateTime = homepageLayout.changeEventDateTime();
		dashboard.switchToEvent();
		homepageLayout.checkEventDateTime(eventDateTime);
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" })
	public void customizeBanerTextEditor(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditSecondOneEvent();
		WebsiteHomepageLayout homepageLayout =  dashboard.navigateToHomepageLayout();
		homepageLayout.clickCustomizeYourBaner();
		dashboard.switchToIframe(homepageLayout.getIframCssLocator());
		homepageLayout.fillTextEditor();
		dashboard.switchToDefaultContent();
		homepageLayout.clickSaveButton();
		dashboard.switchToEvent();
		homepageLayout.checkBanerText();
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" })
	public void editEventDescription(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditSecondOneEvent();
		WebsiteHomepageLayout homepageLayout =  dashboard.navigateToHomepageLayout();
		homepageLayout.clickEditEventDescritpion();
		dashboard.switchToIframe(homepageLayout.getIframCssLocator());
		homepageLayout.fillTextEditor();
		dashboard.switchToDefaultContent();
		homepageLayout.clickSaveBtnDescription();
		dashboard.switchToEvent();
		homepageLayout.checkDescriptionText();
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" })
	public void uploadPhotosTextEditor(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditSecondOneEvent();
		WebsiteHomepageLayout homepageLayout =  dashboard.navigateToHomepageLayout();
		homepageLayout.clickUploadPhotos();
		dashboard.switchToIframe(homepageLayout.getIframCssLocator());
		homepageLayout.fillTextEditor();
		dashboard.switchToDefaultContent();
		homepageLayout.clickSaveButton();
		dashboard.switchToEvent();
		homepageLayout.checkUploadPhotosTextEditorText();
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" }, priority = 1)
	public void editDonationBtnText(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditSecondOneEvent();
		WebsiteHomepageLayout homepageLayout =  dashboard.navigateToHomepageLayout();
		homepageLayout.clickEditDonationBtn();
		homepageLayout.editDonationBtnText();
		dashboard.switchToEvent();
		homepageLayout.checkDonationBtnEditedText();
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" }, priority = 2)
	public void customizeBanerImage(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditSecondOneEvent();
		WebsiteHomepageLayout homepageLayout =  dashboard.navigateToHomepageLayout();
		homepageLayout.clickCustomizeYourBaner();
		homepageLayout.clickUploadAFile();
		homepageLayout.clickUploadNewFile();
		homepageLayout.uploadImage();
		homepageLayout.clickSaveButton();
		dashboard.switchToEvent();
		homepageLayout.checkBanerImage();
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" }, priority = 3)
	public void uploadPhoto(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditSecondOneEvent();
		WebsiteHomepageLayout homepageLayout =  dashboard.navigateToHomepageLayout();
		homepageLayout.clickUploadPhotos();
		homepageLayout.clickUploadAFile();
		homepageLayout.selectImage();
		homepageLayout.clickSaveButton();
		dashboard.switchToEvent();
		homepageLayout.checkUploadPhoto();
		dashboard.switchToOldWindow();
	}
	
	@AfterMethod
	public void setScreenshot(ITestResult result) {
		if (!result.isSuccess()){
			String methodName = result.getName();
			String filename = null;
			try
			{
				SimpleDateFormat formater = new SimpleDateFormat("yyyy_MM_dd_hh_mm_ss");
				File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
				filename = formater.format(Calendar.getInstance().getTime()) + "_" + methodName + ".png";
				String separator = System.getProperty("file.separator");
				String path = System.getProperty("user.dir") + separator + "test-output" + separator + "screens" + separator;
				FileUtils.copyFile(scrFile, new File(path + filename));
				Reporter.setCurrentTestResult(result);
				Reporter.log("<img width='100%' src='" + "screens" + separator + filename + "'/>");
			}
			catch (IOException e1)
			{
				e1.printStackTrace();
			}
			if (driver.getWindowHandles().size() > 1) {
				handler.switchToOldWindow(driver);
			}
		}
	}
	
	@AfterClass
	public void afterClass() {
		try
		{
			driver.close();
			driver.quit();
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
	} 
}
