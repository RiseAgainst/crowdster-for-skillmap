package tests;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import eventDashboardPages.EventDashboard;
import eventDashboardPages.WebsiteForms;
import organizationPages.OrganizationDashboard;
import pages.Login;
import utils.Data;
import utils.DriverSetup;
import utils.WindowHandler;

public class TestsWebsiteForms {
	private WebDriver		driver;
	private WindowHandler 	handler;
	private String			username	= Data.userLogin;
	private String			password	= Data.userPassword;
	
	@BeforeClass
	public void beforeClass() {
		driver = DriverSetup.getDriver("desktop");
		DriverSetup.initialSetup(driver);
		handler = new WindowHandler();
	}
	
	@Test
	public void loginPage() {
		Login loginPage = new Login(driver);
		ArrayList<String> locators = pages.Login.getLocators();
		loginPage.locatorsCheck(locators);
	}

	@Test(dependsOnMethods = { "loginPage" })
	public void login() {
		Login loginPage = new Login(driver);
		OrganizationDashboard result = loginPage.loginAsUser(username, password);
		loginPage.checkIfLoginSuccessful(result.getTitleText());
	}
	
	@Test(dependsOnMethods = { "login" }, priority = -1)
	public void createForm(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditFirstEvent();		
		WebsiteForms websiteForms = dashboard.navigateToWebsiteForms();
		int formsCountBefore = dashboard.getTableRowsCount(dashboard.getTableIdLocator());
		websiteForms.clickCreateFormBtn();
		websiteForms.fillPageTitle();
		websiteForms.fillNavigationLabel();
		dashboard.switchToFirstIframe();
		websiteForms.fillLeftSideContent();
		dashboard.switchToDefaultContent();
		dashboard.switchToSecondIframe();
		websiteForms.fillRightSideContent();
		dashboard.switchToDefaultContent();
		websiteForms.clickToggleElements();
		dashboard.switchToThirdIframe();
		websiteForms.fillNextPageContent();
		dashboard.switchToDefaultContent();
		websiteForms.clickSaveBtn();
		int formsCountAfter = dashboard.getTableRowsCount(dashboard.getTableIdLocator());
		websiteForms.checkFormsCountAfterCreated(formsCountBefore, formsCountAfter);
	}
	
	@Test(dependsOnMethods = { "login" })
	public void checkPageTitle(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditFirstEvent();		
		WebsiteForms websiteForms = dashboard.navigateToWebsiteForms();
		dashboard.switchToEvent();
		websiteForms.clickOnForm();
		websiteForms.checkFormPageTitle();
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" })
	public void checkNavigationLabel(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditFirstEvent();		
		WebsiteForms websiteForms = dashboard.navigateToWebsiteForms();
		dashboard.switchToEvent();
		websiteForms.checkNavigationLabel();
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" })
	public void checkLeftSideContent(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditFirstEvent();		
		WebsiteForms websiteForms = dashboard.navigateToWebsiteForms();
		dashboard.switchToEvent();
		websiteForms.clickOnForm();
		websiteForms.checkLeftSideContent();
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" })
	public void checkRightSideContent(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditFirstEvent();		
		WebsiteForms websiteForms = dashboard.navigateToWebsiteForms();
		dashboard.switchToEvent();
		websiteForms.clickOnForm();
		websiteForms.checkLeftSideContent();
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" })
	public void checkNextPageContent(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditFirstEvent();		
		WebsiteForms websiteForms = dashboard.navigateToWebsiteForms();
		dashboard.switchToEvent();
		websiteForms.clickOnForm();
		websiteForms.clickContinueBtn();
		websiteForms.checkNextPageContent();
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" }, priority = 1)
	public void deleteForm(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditFirstEvent();		
		WebsiteForms websiteForms = dashboard.navigateToWebsiteForms();
		int formsCountBefore = dashboard.getTableRowsCount(dashboard.getTableIdLocator());
		websiteForms.clickDeleteForm(dashboard.getTableIdLocator());
		dashboard.handelJavaScriptAlert();
		int formsCountAfter = dashboard.getTableRowsCount(dashboard.getTableIdLocator());
		websiteForms.checkFormsCountAfterDeleted(formsCountBefore, formsCountAfter);
	}
	
	@AfterMethod
	public void setScreenshot(ITestResult result) {
		if (!result.isSuccess()){
			String methodName = result.getName();
			String filename = null;
			try
			{
				SimpleDateFormat formater = new SimpleDateFormat("yyyy_MM_dd_hh_mm_ss");
				File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
				filename = formater.format(Calendar.getInstance().getTime()) + "_" + methodName + ".png";
				String separator = System.getProperty("file.separator");
				String path = System.getProperty("user.dir") + separator + "test-output" + separator + "screens" + separator;
				FileUtils.copyFile(scrFile, new File(path + filename));
				Reporter.setCurrentTestResult(result);
				Reporter.log("<img width='100%' src='" + "screens" + separator + filename + "'/>");
			}
			catch (IOException e1)
			{
				e1.printStackTrace();
			}
			if (driver.getWindowHandles().size() > 1) {
				handler.switchToOldWindow(driver);
			}
		}
	}
	
	@AfterClass
	public void afterClass() {
		try
		{
			driver.close();
			driver.quit();
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
	} 
}
