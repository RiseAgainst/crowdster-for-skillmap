package tests;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import eventDashboardPages.HomepageDashboard;
import homepageDashboardPages.HomepageNavigation;
import organizationPages.OrganizationDashboard;
import pages.Login;
import pages.Menu;
import utils.Data;
import utils.DriverSetup;
import utils.WindowHandler;

public class TestsHomepageNavigation {
	private WebDriver driver;
	private WindowHandler 	handler;
	private String username	= Data.userLogin;
	private String password	= Data.userPassword;
	
	@BeforeClass
	public void beforeClass() {
		driver = DriverSetup.getDriver("desktop");
		DriverSetup.initialSetup(driver);
		handler = new WindowHandler();
	}
	
	@Test
	public void loginPage() {
		Login loginPage = new Login(driver);
		ArrayList<String> locators = pages.Login.getLocators();
		loginPage.locatorsCheck(locators);
	}

	@Test(dependsOnMethods = { "loginPage" }, groups = { "Profile" })
	public void login() {
		Login loginPage = new Login(driver);
		OrganizationDashboard result = loginPage.loginAsUser(username, password);
		loginPage.checkIfLoginSuccessful(result.getTitleText());
	}
	
	@Test(dependsOnMethods = { "login" }, priority = 1)
	public void createNavigationPage(){
		HomepageDashboard dashboard = new HomepageDashboard(driver);
		Menu menu = dashboard.clickMenu();
		menu.clickHomepageDashboard();
		dashboard.searchBy("FirstSupersite");
		dashboard.clickManage();
		HomepageNavigation homepageNavigation = dashboard.navigateToNavigation();
		int navigationPagesCountBefore = homepageNavigation.getNavigationPagesCount();
		homepageNavigation.clickCreatePageBtn();
		homepageNavigation.createNavigationPage();
		int navigationPagesCountAfter = homepageNavigation.getNavigationPagesCount();
		homepageNavigation.checkNavigationPagesCountAfterCreate(navigationPagesCountBefore, navigationPagesCountAfter);		
	}
	
	@Test(dependsOnMethods = { "login" }, priority = 2)
	public void checkNavigationPage(){
		HomepageDashboard dashboard = new HomepageDashboard(driver);
		Menu menu = dashboard.clickMenu();
		menu.clickHomepageDashboard();
		dashboard.searchBy("FirstSupersite");
		dashboard.clickManage();
		HomepageNavigation homepageNavigation = dashboard.navigateToNavigation();
		dashboard.switchToHomepage();
		homepageNavigation.clickNavigationPage();
		homepageNavigation.checkHomepageNavigationPage();
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" }, priority = 3)
	public void createNavigationSubPage(){
		HomepageDashboard dashboard = new HomepageDashboard(driver);
		Menu menu = dashboard.clickMenu();
		menu.clickHomepageDashboard();
		dashboard.searchBy("FirstSupersite");
		dashboard.clickManage();
		HomepageNavigation homepageNavigation = dashboard.navigateToNavigation();
		int navigationSubpagesCountBefore = homepageNavigation.getNavigationSubPagesCount();
		homepageNavigation.clickAddSubpageBtn();
		homepageNavigation.createNavigationSubPage();
		int navigationSubpagesCountAfter = homepageNavigation.getNavigationSubPagesCount();
		homepageNavigation.checkNavigationSubpagesCountAfterCreate(navigationSubpagesCountBefore, navigationSubpagesCountAfter);
	}
	
	@Test(dependsOnMethods = { "login" }, priority = 4)
	public void checkNavigationSubpage(){
		HomepageDashboard dashboard = new HomepageDashboard(driver);
		Menu menu = dashboard.clickMenu();
		menu.clickHomepageDashboard();
		dashboard.searchBy("FirstSupersite");
		dashboard.clickManage();
		HomepageNavigation homepageNavigation = dashboard.navigateToNavigation();
		dashboard.switchToHomepage();
		homepageNavigation.clickNavigationSubpage();
		homepageNavigation.checkHomepageNavigationSubpage();
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" }, priority = 5)
	public void editNavigationPage(){
		HomepageDashboard dashboard = new HomepageDashboard(driver);
		Menu menu = dashboard.clickMenu();
		menu.clickHomepageDashboard();
		dashboard.searchBy("FirstSupersite");
		dashboard.clickManage();
		HomepageNavigation homepageNavigation = dashboard.navigateToNavigation();
		homepageNavigation.clickEditNavigationPage();
		homepageNavigation.editNavigationPage();
		dashboard.switchToHomepage();
		homepageNavigation.clickNavigationPageEdited();
		homepageNavigation.checkHomepageNavigationPageEdited();
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" }, priority = 6)
	public void editNavigationSubpage(){
		HomepageDashboard dashboard = new HomepageDashboard(driver);
		Menu menu = dashboard.clickMenu();
		menu.clickHomepageDashboard();
		dashboard.searchBy("FirstSupersite");
		dashboard.clickManage();
		HomepageNavigation homepageNavigation = dashboard.navigateToNavigation();
		homepageNavigation.clickEditNavigationSubpage();
		homepageNavigation.editNavigationSubpage();
		dashboard.switchToHomepage();
		homepageNavigation.clickNavigationSubpageEdited();
		homepageNavigation.checkHomepageNavigationSubpageEdited();
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" }, priority = 7)
	public void setPrivateNavigationSubpage(){
		HomepageDashboard dashboard = new HomepageDashboard(driver);
		Menu menu = dashboard.clickMenu();
		menu.clickHomepageDashboard();
		dashboard.searchBy("FirstSupersite");
		dashboard.clickManage();
		HomepageNavigation homepageNavigation = dashboard.navigateToNavigation();
		homepageNavigation.clickEditNavigationSubpage();
		homepageNavigation.clickDontShowInNavigation();
		homepageNavigation.clickEditSaveBtn();
		dashboard.switchToHomepage();
		homepageNavigation.checkPrivateNavigationSubpage();
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" }, priority = 8)
	public void setPrivateNavigationPage(){
		HomepageDashboard dashboard = new HomepageDashboard(driver);
		Menu menu = dashboard.clickMenu();
		menu.clickHomepageDashboard();
		dashboard.searchBy("FirstSupersite");
		dashboard.clickManage();
		HomepageNavigation homepageNavigation = dashboard.navigateToNavigation();
		homepageNavigation.clickEditNavigationPage();
		homepageNavigation.clickDontShowInNavigation();
		homepageNavigation.clickEditSaveBtn();
		dashboard.switchToHomepage();
		homepageNavigation.checkPrivateNavigationPage();
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" }, priority = 9)
	public void deleteNavigationSubpage(){
		HomepageDashboard dashboard = new HomepageDashboard(driver);
		Menu menu = dashboard.clickMenu();
		menu.clickHomepageDashboard();
		dashboard.searchBy("FirstSupersite");
		dashboard.clickManage();
		HomepageNavigation homepageNavigation = dashboard.navigateToNavigation();
		int navigationSubpagesCountBefore = homepageNavigation.getNavigationSubPagesCount();
		homepageNavigation.clickDeleteNavigationSubpage();
		dashboard.handelJavaScriptAlert();
		int navigationSubpagesCountAfter = homepageNavigation.getNavigationSubPagesCount();
		homepageNavigation.checkNavigationSubpagesCountAfterDelete(navigationSubpagesCountBefore, navigationSubpagesCountAfter);
	}
	
	@Test(dependsOnMethods = { "login" }, priority = 10)
	public void deleteNavigationPage(){
		HomepageDashboard dashboard = new HomepageDashboard(driver);
		Menu menu = dashboard.clickMenu();
		menu.clickHomepageDashboard();
		dashboard.searchBy("FirstSupersite");
		dashboard.clickManage();
		HomepageNavigation homepageNavigation = dashboard.navigateToNavigation();
		int navigationPagesCountBefore = homepageNavigation.getNavigationPagesCount();
		homepageNavigation.clickDeleteNavigationPage();
		dashboard.handelJavaScriptAlert();
		int navigationPagesCountAfter = homepageNavigation.getNavigationPagesCount();
		homepageNavigation.checkNavigationPagesCountAfterDelete(navigationPagesCountBefore, navigationPagesCountAfter);
	}
	
	@AfterMethod
	public void setScreenshot(ITestResult result) {
		if (!result.isSuccess()){
			String methodName = result.getName();
			String filename = null;
			try
			{
				SimpleDateFormat formater = new SimpleDateFormat("yyyy_MM_dd_hh_mm_ss");
				File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
				filename = formater.format(Calendar.getInstance().getTime()) + "_" + methodName + ".png";
				String separator = System.getProperty("file.separator");
				String path = System.getProperty("user.dir") + separator + "test-output" + separator + "screens" + separator;
				FileUtils.copyFile(scrFile, new File(path + filename));
				Reporter.setCurrentTestResult(result);
				Reporter.log("<img width='100%' src='" + "screens" + separator + filename + "'/>");
			}
			catch (IOException e1)
			{
				e1.printStackTrace();
			}
			if (driver.getWindowHandles().size() > 1) {
				handler.switchToOldWindow(driver);
			}
		}
	}
	
	@AfterClass
	public void afterClass() {
		try
		{
			driver.close();
			driver.quit();
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
	} 
}
