package tests;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import eventDashboardPages.EventDashboard;
import eventPages.CheckoutPromoCode;
import eventPages.Event;
import eventPages.EventDonation;
import eventPages.PaymentInfo;
import organizationPages.OrganizationDashboard;
import pages.Login;
import utils.Data;
import utils.DriverSetup;
import utils.WindowHandler;

public class TestsCheckoutPromoCodeScope {
	private WebDriver driver;
	private WindowHandler handler;
	private String username	= Data.userLogin;
	private String password	= Data.userPassword;
	
	@BeforeClass
	public void beforeClass() {
		driver = DriverSetup.getDriver("desktop");
		DriverSetup.initialSetup(driver);
		this.handler = new WindowHandler();
	}
	
	@Test
	public void loginPage() {
		Login loginPage = new Login(driver);
		ArrayList<String> locators = pages.Login.getLocators();
		loginPage.locatorsCheck(locators);
	}

	@Test(dependsOnMethods = { "loginPage" })
	public void login() {
		Login loginPage = new Login(driver);
		OrganizationDashboard result = loginPage.loginAsUser(username, password);
		loginPage.checkIfLoginSuccessful(result.getTitleText());
	}
	
	@Test(dependsOnMethods = { "login" }, priority = 1)
	public void createPromoCodeWithAllScopeEntries(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditFirstEvent();
		CheckoutPromoCode checkoutPromoCode = dashboard.navigateToCheckoutPromoCode();
		int promoCodesCountBefore = checkoutPromoCode.getPromoCodesCount();
		checkoutPromoCode.clickCreatePromoCodeBtn();
		checkoutPromoCode.createPromoCodeWithAllScopeEntries();
		checkoutPromoCode.setLimitedPromoCode("2");
		checkoutPromoCode.clickSaveBtn();
		int promoCodesCountAfter = checkoutPromoCode.getPromoCodesCount();
		checkoutPromoCode.checkIsAddedNewPromoCode(promoCodesCountBefore, promoCodesCountAfter);
	}
	
	@Test(dependsOnMethods = { "login" }, priority = 2)
	public void editPromoCodeWithSomeScopeEntries(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditFirstEvent();
		CheckoutPromoCode checkoutPromoCode = dashboard.navigateToCheckoutPromoCode();
		int promoCodesCountBefore = checkoutPromoCode.getPromoCodesCount();
		checkoutPromoCode.clickEditPromoCodeWithScopeEntries();
		checkoutPromoCode.editPromoCodeWithSomeScopeEntries();
		checkoutPromoCode.clickSaveBtn();
		int promoCodesCountAfter = checkoutPromoCode.getPromoCodesCount();
		checkoutPromoCode.checkPromoCodesCountAfterEdit(promoCodesCountBefore, promoCodesCountAfter);
	}
	
	@Test(dependsOnMethods = { "login" }, priority = 3)
	public void promoCodeScopePromotion(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditFirstEvent();
		CheckoutPromoCode checkoutPromoCode = dashboard.navigateToCheckoutPromoCode();
		dashboard.switchToEvent();
		Event event = new Event(driver);
		EventDonation eventDonation = event.clickRegistrationButton();
		int totalAmout =  eventDonation.addToCartPackageTest();
		PaymentInfo paymentInfo = eventDonation.clickCompleteCheckout();
		paymentInfo.setItemPromoCode(checkoutPromoCode.getPromoCode());
		paymentInfo.paymentDataFilling();
		paymentInfo.acceptTermsAndconditions();
		paymentInfo.clickContinue();
		checkoutPromoCode.checkRegistrationPaymentTotal(totalAmout);
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" }, priority = 4)
	public void promoCodeScopeOutsideOfItem(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditFirstEvent();
		CheckoutPromoCode checkoutPromoCode = dashboard.navigateToCheckoutPromoCode();
		dashboard.switchToEvent();
		Event event = new Event(driver);
		EventDonation eventDonation = event.clickSponsorButton();
		eventDonation.addToCartSponsorshipTest();
		PaymentInfo paymentInfo = eventDonation.clickCompleteCheckout();
		paymentInfo.setItemPromoCode(checkoutPromoCode.getPromoCode());
		paymentInfo.paymentDataFilling();
		paymentInfo.acceptTermsAndconditions();
		paymentInfo.clickContinue();
		checkoutPromoCode.checkInvalidPromoCodeErrorMsg();
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" }, priority = 5)
	public void donationWithPromoCodeGlobalAndScope(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditFirstEvent();
		CheckoutPromoCode checkoutPromoCode = dashboard.navigateToCheckoutPromoCode();
		dashboard.switchToEvent();
		Event event = new Event(driver);
		EventDonation eventDonation = event.clickDonationButton();
		eventDonation.setDonationAmount("100");
		int totalAmount = eventDonation.addToCartDonationTest();
		PaymentInfo paymentInfo = eventDonation.clickCompleteCheckout();
		paymentInfo.setItemPromoCode(checkoutPromoCode.getPromoCode());
		paymentInfo.paymentDataFilling();
		int promoCodeValue = paymentInfo.setPromocode15();
		paymentInfo.acceptTermsAndconditions();
		paymentInfo.clickContinue();
		checkoutPromoCode.checkDonationPaymentTotal(totalAmount, promoCodeValue);
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" }, priority = 6)
	public void failtToAppliedGlobalPromoCodeToScope(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditFirstEvent();
		CheckoutPromoCode checkoutPromoCode = dashboard.navigateToCheckoutPromoCode();
		dashboard.switchToEvent();
		Event event = new Event(driver);
		EventDonation eventDonation = event.clickSponsorButton();
		eventDonation.addToCartSponsorshipTest();
		PaymentInfo paymentInfo = eventDonation.clickCompleteCheckout();
		paymentInfo.setItemPromoCode(paymentInfo.getPromoCodeTextValue());
		paymentInfo.paymentDataFilling();
		paymentInfo.acceptTermsAndconditions();
		paymentInfo.clickContinue();
		checkoutPromoCode.checkInvalidPromoCodeErrorMsg();
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" }, priority = 7)
	public void failtToAppliedPromoCodeScopeToGlobal(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditFirstEvent();
		CheckoutPromoCode checkoutPromoCode = dashboard.navigateToCheckoutPromoCode();
		dashboard.switchToEvent();
		Event event = new Event(driver);
		EventDonation eventDonation = event.clickSponsorButton();
		eventDonation.addToCartSponsorshipTest();
		PaymentInfo paymentInfo = eventDonation.clickCompleteCheckout();
		paymentInfo.setPromocode(checkoutPromoCode.getPromoCode());
		paymentInfo.paymentDataFilling();
		paymentInfo.acceptTermsAndconditions();
		paymentInfo.clickContinue();
		checkoutPromoCode.checkInvalidPromoCodeErrorMsg();
		dashboard.switchToOldWindow();
	}
	
	@AfterMethod
	public void setScreenshot(ITestResult result) {
		if (!result.isSuccess()){
			String methodName = result.getName();
			String filename = null;
			try
			{
				SimpleDateFormat formater = new SimpleDateFormat("yyyy_MM_dd_hh_mm_ss");
				File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
				filename = formater.format(Calendar.getInstance().getTime()) + "_" + methodName + ".png";
				String separator = System.getProperty("file.separator");
				String path = System.getProperty("user.dir") + separator + "test-output" + separator + "screens" + separator;
				FileUtils.copyFile(scrFile, new File(path + filename));
				Reporter.setCurrentTestResult(result);
				Reporter.log("<img width='100%' src='" + "screens" + separator + filename + "'/>");
			}
			catch (IOException e1)
			{
				e1.printStackTrace();
			}
			if (driver.getWindowHandles().size() > 1) {
				handler.switchToOldWindow(driver);
			}
		}
	}
	
	@AfterClass
	public void afterClass() {
		try
		{
			driver.close();
			driver.quit();
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
	} 
}
