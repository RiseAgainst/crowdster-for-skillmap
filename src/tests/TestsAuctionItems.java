package tests;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import eventDashboardPages.AuctionItem;
import eventDashboardPages.EventDashboard;
import organizationPages.OrganizationDashboard;
import pages.Login;
import utils.Data;
import utils.DriverSetup;
import utils.WindowHandler;

public class TestsAuctionItems {
	private WebDriver 			driver;
	private WindowHandler 		handler;
	private String username	= 	Data.userLogin;
	private String password	= 	Data.userPassword;
	
	@BeforeClass
	public void beforeClass() {
		driver = DriverSetup.getDriver("desktop");
		DriverSetup.initialSetup(driver);
		this.handler = new WindowHandler();
	}
	
	@Test
	public void loginPage() {
		Login loginPage = new Login(driver);
		ArrayList<String> locators = pages.Login.getLocators();
		loginPage.locatorsCheck(locators);
	}

	@Test(dependsOnMethods = { "loginPage" })
	public void login() {
		Login loginPage = new Login(driver);
		OrganizationDashboard result = loginPage.loginAsUser(username, password);
		loginPage.checkIfLoginSuccessful(result.getTitleText());
	}
	
	@Test(dependsOnMethods = { "login" }, priority = -1)
	public void createAuctionItem(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditFirstEvent();
		AuctionItem auctionItem = dashboard.navigateToAuction();
		int auctionsCountBefore = dashboard.getTableRowsCount(dashboard.getTableIdLocator());
		auctionItem.clickCreateAuctionItemBtn();
		auctionItem.fillAuctionItem();
		auctionItem.clickSubmitBtn();
		auctionItem.clickDashboardAuctionItems();
		int auctionsCountAfter = dashboard.getTableRowsCount(dashboard.getTableIdLocator());
		auctionItem.checkAuctionsCountAfterCreate(auctionsCountBefore, auctionsCountAfter);
	}
	
	@Test(dependsOnMethods = { "login" })
	public void checkAuctionNameAndDescription(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditFirstEvent();
		AuctionItem auctionItem = dashboard.navigateToAuction();
		dashboard.switchToEvent();
		auctionItem.clickAuctionRegistrationBtn();
		auctionItem.checkAuctionNameAndDescription();
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" })
	public void checkOpeningPriceAndMinimumRaise(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditFirstEvent();
		AuctionItem auctionItem = dashboard.navigateToAuction();
		dashboard.switchToEvent();
		auctionItem.clickAuctionRegistrationBtn();
		auctionItem.clickMoreInformationBtn();
		auctionItem.checkAuctionOpeningPriceAndMinimumRaise();
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" })
	public void checkDonorAndItemCode(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditFirstEvent();
		AuctionItem auctionItem = dashboard.navigateToAuction();
		dashboard.switchToEvent();
		auctionItem.clickAuctionRegistrationBtn();
		auctionItem.clickMoreInformationBtn();
		auctionItem.checkDonorAndItemCode();
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" })
	public void checkBiddingStartEndDate(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditFirstEvent();
		AuctionItem auctionItem = dashboard.navigateToAuction();
		auctionItem.clickEditAuction();
		String biddingStartDate = auctionItem.getBiddingStartDate();
		String biddingEndDate = auctionItem.getBiddingEndDate();
		dashboard.switchToEvent();
		auctionItem.clickAuctionRegistrationBtn();
		auctionItem.clickMoreInformationBtn();
		auctionItem.checkBiddingStartEndDate(biddingStartDate, biddingEndDate);
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" })
	public void checkAuctionItemImage(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditFirstEvent();
		AuctionItem auctionItem = dashboard.navigateToAuction();
		dashboard.switchToEvent();
		auctionItem.clickAuctionRegistrationBtn();
		auctionItem.checkAuctionItemImg();
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" }, priority = 1)
	public void checkDeleteAuctionItem(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditFirstEvent();
		AuctionItem auctionItem = dashboard.navigateToAuction();
		int auctionsCountBefore = dashboard.getTableRowsCount(dashboard.getTableIdLocator());
		auctionItem.clickDeleteAuction();
		dashboard.handelJavaScriptAlert();
		int auctionsCountAfter = dashboard.getTableRowsCount(dashboard.getTableIdLocator());
		auctionItem.checkAuctionsCountAfterDelete(auctionsCountBefore, auctionsCountAfter);
	}
	
	@AfterMethod
	public void setScreenshot(ITestResult result) {
		if (!result.isSuccess()){
			String methodName = result.getName();
			String filename = null;
			try
			{
				SimpleDateFormat formater = new SimpleDateFormat("yyyy_MM_dd_hh_mm_ss");
				File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
				filename = formater.format(Calendar.getInstance().getTime()) + "_" + methodName + ".png";
				String separator = System.getProperty("file.separator");
				String path = System.getProperty("user.dir") + separator + "test-output" + separator + "screens" + separator;
				FileUtils.copyFile(scrFile, new File(path + filename));
				Reporter.setCurrentTestResult(result);
				Reporter.log("<img width='100%' src='" + "screens" + separator + filename + "'/>");
			}
			catch (IOException e1)
			{
				e1.printStackTrace();
			}
			if (driver.getWindowHandles().size() > 1) {
				handler.switchToOldWindow(driver);
			}
		}
	}
	
	@AfterClass
	public void afterClass() {
		try
		{
			driver.close();
			driver.quit();
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
	} 
}
