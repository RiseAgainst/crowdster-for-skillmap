package tests;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import eventDashboardPages.EventDashboard;
import eventPages.CheckoutCustomQuestions;
import organizationPages.OrganizationDashboard;
import pages.Login;
import utils.Data;
import utils.DriverSetup;
import utils.WindowHandler;

public class TestsCheckoutCreateCustomQuestions {
	private WebDriver driver;
	private WindowHandler handler;
	private String username	= Data.userLogin;
	private String password	= Data.userPassword;
	
	@BeforeClass
	public void beforeClass() {
		driver = DriverSetup.getDriver("desktop");
		DriverSetup.initialSetup(driver);
		this.handler = new WindowHandler();
	}
	
	@Test
	public void loginPage() {
		Login loginPage = new Login(driver);
		ArrayList<String> locators = pages.Login.getLocators();
		loginPage.locatorsCheck(locators);
	}

	@Test(dependsOnMethods = { "loginPage" })
	public void login() {
		Login loginPage = new Login(driver);
		OrganizationDashboard result = loginPage.loginAsUser(username, password);
		loginPage.checkIfLoginSuccessful(result.getTitleText());
	}
	
	@Test(dependsOnMethods = { "login" }, priority = 1)
	public void createWriteInOneLineQuestion(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditSecondOneEvent();
		CheckoutCustomQuestions checkoutCustomQuestions = dashboard.navigateToCheckoutCustomQuestions();
		checkoutCustomQuestions.clickCreateQuestion();
		String questionTypeText = checkoutCustomQuestions.getWriteInOneLineType();
		checkoutCustomQuestions.selectQuestionType(questionTypeText);
		String questionRankOrder = checkoutCustomQuestions.getWriteInOneLineRankOrder();
		checkoutCustomQuestions.createCustomQuestion(questionTypeText, questionRankOrder);
		dashboard.switchToEvent();
		checkoutCustomQuestions.clickEventDonationBtn();
		checkoutCustomQuestions.setQuickDonationAmount();
		checkoutCustomQuestions.clickCompleteCheckout();
		checkoutCustomQuestions.checkRequiredCustomQuestion(questionTypeText, questionRankOrder);
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" }, priority = 2)
	public void createWriteInEssayQuestion(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditSecondOneEvent();
		CheckoutCustomQuestions checkoutCustomQuestions = dashboard.navigateToCheckoutCustomQuestions();
		checkoutCustomQuestions.clickCreateQuestion();
		String questionTypeText = checkoutCustomQuestions.getWriteInEssyType();
		checkoutCustomQuestions.selectQuestionType(questionTypeText);
		String questionRankOrder = checkoutCustomQuestions.getWriteInEssyRankOrder();
		checkoutCustomQuestions.createCustomQuestion(questionTypeText, questionRankOrder);
		dashboard.switchToEvent();
		checkoutCustomQuestions.clickEventDonationBtn();
		checkoutCustomQuestions.setQuickDonationAmount();
		checkoutCustomQuestions.clickCompleteCheckout();
		checkoutCustomQuestions.checkRequiredCustomQuestion(questionTypeText, questionRankOrder);
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" }, priority = 3)
	public void createSelectBoxQuestion(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditSecondOneEvent();
		CheckoutCustomQuestions checkoutCustomQuestions = dashboard.navigateToCheckoutCustomQuestions();
		checkoutCustomQuestions.clickCreateQuestion();
		String questionTypeText = checkoutCustomQuestions.getSelectBoxType();
		checkoutCustomQuestions.selectQuestionType(questionTypeText);
		String questionRankOrder = checkoutCustomQuestions.getSelectBoxRankOrder();
		checkoutCustomQuestions.createCustomQuestion(questionTypeText, questionRankOrder);
		dashboard.switchToEvent();
		checkoutCustomQuestions.clickEventDonationBtn();
		checkoutCustomQuestions.setQuickDonationAmount();
		checkoutCustomQuestions.clickCompleteCheckout();
		checkoutCustomQuestions.checkRequiredCustomQuestion(questionTypeText, questionRankOrder);
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" }, priority = 4)
	public void createCheckBoxQuestion(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditSecondOneEvent();
		CheckoutCustomQuestions checkoutCustomQuestions = dashboard.navigateToCheckoutCustomQuestions();
		checkoutCustomQuestions.clickCreateQuestion();
		String questionTypeText = checkoutCustomQuestions.getCheckBoxType();
		checkoutCustomQuestions.selectQuestionType(questionTypeText);
		String questionRankOrder = checkoutCustomQuestions.getCheckBoxRankOrder();
		checkoutCustomQuestions.createCustomQuestion(questionTypeText, questionRankOrder);
		dashboard.switchToEvent();
		checkoutCustomQuestions.clickEventDonationBtn();
		checkoutCustomQuestions.setQuickDonationAmount();
		checkoutCustomQuestions.clickCompleteCheckout();
		checkoutCustomQuestions.checkRequiredCustomQuestion(questionTypeText, questionRankOrder);
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" }, priority = 5)
	public void createStateSelectQuestion(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditSecondOneEvent();
		CheckoutCustomQuestions checkoutCustomQuestions = dashboard.navigateToCheckoutCustomQuestions();
		checkoutCustomQuestions.clickCreateQuestion();
		String questionTypeText = checkoutCustomQuestions.getStateSelectType();
		checkoutCustomQuestions.selectQuestionType(questionTypeText);
		String questionRankOrder = checkoutCustomQuestions.getStateSelectRankOrder();
		checkoutCustomQuestions.createCustomQuestion(questionTypeText, questionRankOrder);
		dashboard.switchToEvent();
		checkoutCustomQuestions.clickEventDonationBtn();
		checkoutCustomQuestions.setQuickDonationAmount();
		checkoutCustomQuestions.clickCompleteCheckout();
		checkoutCustomQuestions.checkRequiredCustomQuestion(questionTypeText, questionRankOrder);
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" }, priority = 6)
	public void createCountrySelectQuestion(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditSecondOneEvent();
		CheckoutCustomQuestions checkoutCustomQuestions = dashboard.navigateToCheckoutCustomQuestions();
		checkoutCustomQuestions.clickCreateQuestion();
		String questionTypeText = checkoutCustomQuestions.getCountrySelectType();
		checkoutCustomQuestions.selectQuestionType(questionTypeText);
		String questionRankOrder = checkoutCustomQuestions.getCountrySelectRankOrder();
		checkoutCustomQuestions.createCustomQuestion(questionTypeText, questionRankOrder);
		dashboard.switchToEvent();
		checkoutCustomQuestions.clickEventDonationBtn();
		checkoutCustomQuestions.setQuickDonationAmount();
		checkoutCustomQuestions.clickCompleteCheckout();
		checkoutCustomQuestions.checkRequiredCustomQuestion(questionTypeText, questionRankOrder);
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" }, priority = 7)
	public void createFileUploadQuestion(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditSecondOneEvent();
		CheckoutCustomQuestions checkoutCustomQuestions = dashboard.navigateToCheckoutCustomQuestions();
		checkoutCustomQuestions.clickCreateQuestion();
		String questionTypeText = checkoutCustomQuestions.getFileUploadType();
		checkoutCustomQuestions.selectQuestionType(questionTypeText);
		String questionRankOrder = checkoutCustomQuestions.getFileUploadRankOrder();
		checkoutCustomQuestions.createCustomQuestion(questionTypeText, questionRankOrder);
		dashboard.switchToEvent();
		checkoutCustomQuestions.clickEventDonationBtn();
		checkoutCustomQuestions.setQuickDonationAmount();
		checkoutCustomQuestions.clickCompleteCheckout();
		checkoutCustomQuestions.checkRequiredCustomQuestion(questionTypeText, questionRankOrder);
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" }, priority = 8)
	public void createTextBlockQuestion(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditSecondOneEvent();
		CheckoutCustomQuestions checkoutCustomQuestions = dashboard.navigateToCheckoutCustomQuestions();
		checkoutCustomQuestions.clickCreateQuestion();
		String questionTypeText = checkoutCustomQuestions.getTextBlockType();
		checkoutCustomQuestions.selectQuestionType(questionTypeText);
		String questionRankOrder = checkoutCustomQuestions.getTextBlockRankOrder();
		checkoutCustomQuestions.createTextBlockCustomQuestion(questionTypeText, questionRankOrder);
		dashboard.switchToEvent();
		checkoutCustomQuestions.clickEventDonationBtn();
		checkoutCustomQuestions.setQuickDonationAmount();
		checkoutCustomQuestions.clickCompleteCheckout();
		checkoutCustomQuestions.checkCustomQuestion(questionTypeText, questionRankOrder);
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" }, priority = 9)
	public void createConfirmationCheckboxQuestion(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditSecondOneEvent();
		CheckoutCustomQuestions checkoutCustomQuestions = dashboard.navigateToCheckoutCustomQuestions();
		checkoutCustomQuestions.clickCreateQuestion();
		String questionTypeText = checkoutCustomQuestions.getConfirmationCheckboxType();
		checkoutCustomQuestions.selectQuestionType(questionTypeText);
		String questionRankOrder = checkoutCustomQuestions.getConfirmationCheckboxRankOrder();
		checkoutCustomQuestions.createCustomQuestion(questionTypeText, questionRankOrder);
		dashboard.switchToEvent();
		checkoutCustomQuestions.clickEventDonationBtn();
		checkoutCustomQuestions.setQuickDonationAmount();
		checkoutCustomQuestions.clickCompleteCheckout();
		checkoutCustomQuestions.checkConfirmationCheckboxCustomQuestion(questionTypeText, questionRankOrder);
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" }, priority = 10)
	public void createFutureDateQuestion(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditSecondOneEvent();
		CheckoutCustomQuestions checkoutCustomQuestions = dashboard.navigateToCheckoutCustomQuestions();
		checkoutCustomQuestions.clickCreateQuestion();
		String questionTypeText = checkoutCustomQuestions.getFutureDateType();
		checkoutCustomQuestions.selectQuestionType(questionTypeText);
		String questionRankOrder = checkoutCustomQuestions.getFutureDateRankOrder();
		checkoutCustomQuestions.createCustomQuestion(questionTypeText, questionRankOrder);
		dashboard.switchToEvent();
		checkoutCustomQuestions.clickEventDonationBtn();
		checkoutCustomQuestions.setQuickDonationAmount();
		checkoutCustomQuestions.clickCompleteCheckout();
		checkoutCustomQuestions.checkRequiredCustomQuestion(questionTypeText, questionRankOrder);
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" }, priority = 11)
	public void createPastDateQuestion(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditSecondOneEvent();
		CheckoutCustomQuestions checkoutCustomQuestions = dashboard.navigateToCheckoutCustomQuestions();
		checkoutCustomQuestions.clickCreateQuestion();
		String questionTypeText = checkoutCustomQuestions.getPastDateType();
		checkoutCustomQuestions.selectQuestionType(questionTypeText);
		String questionRankOrder = checkoutCustomQuestions.getPastDateRankOrder();
		checkoutCustomQuestions.createCustomQuestion(questionTypeText, questionRankOrder);
		dashboard.switchToEvent();
		checkoutCustomQuestions.clickEventDonationBtn();
		checkoutCustomQuestions.setQuickDonationAmount();
		checkoutCustomQuestions.clickCompleteCheckout();
		checkoutCustomQuestions.checkRequiredCustomQuestion(questionTypeText, questionRankOrder);
		dashboard.switchToOldWindow();
	}
	
	@AfterMethod
	public void setScreenshot(ITestResult result) {
		if (!result.isSuccess()){
			String methodName = result.getName();
			String filename = null;
			try
			{
				SimpleDateFormat formater = new SimpleDateFormat("yyyy_MM_dd_hh_mm_ss");
				File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
				filename = formater.format(Calendar.getInstance().getTime()) + "_" + methodName + ".png";
				String separator = System.getProperty("file.separator");
				String path = System.getProperty("user.dir") + separator + "test-output" + separator + "screens" + separator;
				FileUtils.copyFile(scrFile, new File(path + filename));
				Reporter.setCurrentTestResult(result);
				Reporter.log("<img width='100%' src='" + "screens" + separator + filename + "'/>");
			}
			catch (IOException e1)
			{
				e1.printStackTrace();
			}
			if (driver.getWindowHandles().size() > 1) {
				handler.switchToOldWindow(driver);
			}
		}
	}
	
	@AfterClass
	public void afterClass() {
		try
		{
			driver.close();
			driver.quit();
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
	} 
}
