package tests;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import eventDashboardPages.EventDashboard;
import eventPages.Appearance;
import organizationPages.OrganizationDashboard;
import pages.Login;
import utils.Data;
import utils.DriverSetup;

public class TestsWebsiteAppearance {
	private WebDriver driver;
	private String username	= Data.userLogin;
	private String password	= Data.userPassword;
	
	@BeforeClass
	public void beforeClass() {
		driver = DriverSetup.getDriver("desktop");
		DriverSetup.initialSetup(driver);
	}
	
	@Test
	public void loginPage() {
		Login loginPage = new Login(driver);
		ArrayList<String> locators = pages.Login.getLocators();
		loginPage.locatorsCheck(locators);
	}

	@Test(dependsOnMethods = { "loginPage" })
	public void login() {
		Login loginPage = new Login(driver);
		OrganizationDashboard result = loginPage.loginAsUser(username, password);
		loginPage.checkIfLoginSuccessful(result.getTitleText());
	}
	
	@Test(dependsOnMethods = { "login" }, priority = 1)
	public void backgroundColor(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditFirstEvent();
		Appearance appearance =  dashboard.navigateToAppearance();
		String appearanceBackgroundClolor = appearance.setBackgroundColor();
		appearance.clickSaveBtn();
		dashboard.switchToEvent();
		appearance.checkBackgroundColor(appearanceBackgroundClolor);
		dashboard.switchToOldWindow();
		
	}
	
	@Test(dependsOnMethods = { "login" }, priority = 2)
	public void backgroundImage(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditFirstEvent();
		Appearance appearance =  dashboard.navigateToAppearance();
		appearance.clickUploadNewFile();
		appearance.selectNewFile();
		appearance.clickSubmitUploadNewFile();
		appearance.clickSaveBtn();
		dashboard.switchToEvent();
		appearance.checkBackgroundImage();
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" }, priority = 3)
	public void lockBackgroundImage(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditFirstEvent();
		Appearance appearance =  dashboard.navigateToAppearance();
		appearance.clickLockBackgroundImage();
		appearance.clickSaveBtn();
		dashboard.switchToEvent();
		appearance.checkLockBackgroundImage();
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" }, priority = 4)
	public void tileBackgroundImageHorizontally(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditFirstEvent();
		Appearance appearance =  dashboard.navigateToAppearance();
		appearance.clickTileBackgroundImageHorizontally();
		appearance.clickSaveBtn();
		dashboard.switchToEvent();
		appearance.checkTileBackgroundImageHorizontally();
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" }, priority = 5)
	public void tileBackgroundImageVertically(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditFirstEvent();
		Appearance appearance =  dashboard.navigateToAppearance();
		appearance.clickTileBackgroundImageVertically();
		appearance.clickSaveBtn();
		dashboard.switchToEvent();
		appearance.checkTileBackgroundImageVertically();
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" }, priority = 6)
	public void footerText(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditFirstEvent();
		Appearance appearance =  dashboard.navigateToAppearance();
		dashboard.switchToFirstIframe();
		appearance.fillFooterText();
		dashboard.switchToDefaultContent();
		appearance.clickSaveBtn();
		dashboard.switchToEvent();
		appearance.checkFooterText();
		dashboard.switchToOldWindow();
		
	}
	
	@AfterMethod
	public void setScreenshot(ITestResult result) {
		if (!result.isSuccess()){
			String methodName = result.getName();
			String filename = null;
			try
			{
				SimpleDateFormat formater = new SimpleDateFormat("yyyy_MM_dd_hh_mm_ss");
				File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
				filename = formater.format(Calendar.getInstance().getTime()) + "_" + methodName + ".png";
				String separator = System.getProperty("file.separator");
				String path = System.getProperty("user.dir") + separator + "test-output" + separator + "screens" + separator;
				FileUtils.copyFile(scrFile, new File(path + filename));
				Reporter.setCurrentTestResult(result);
				Reporter.log("<img width='100%' src='" + "screens" + separator + filename + "'/>");
			}
			catch (IOException e1)
			{
				e1.printStackTrace();
			}
		}
	}
	
	@AfterClass
	public void afterClass() {
		try
		{
			driver.close();
			driver.quit();
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
	} 
}
