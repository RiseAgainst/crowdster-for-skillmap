package tests;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import eventDashboardPages.EventDashboard;
import eventDashboardPages.SettingsOfflineDonation;
import organizationPages.OrganizationDashboard;
import pages.Login;
import utils.Data;
import utils.DriverSetup;
import utils.WindowHandler;

public class TestsSettingsOfflineDonation {
	private WebDriver driver;
	private WindowHandler 	handler;
	private String username	= Data.userLogin;
	private String password	= Data.userPassword;
	
	@BeforeClass
	public void beforeClass() {
		driver = DriverSetup.getDriver("desktop");
		DriverSetup.initialSetup(driver);
		handler = new WindowHandler();
	}
	
	@Test
	public void loginPage() {
		Login loginPage = new Login(driver);
		ArrayList<String> locators = pages.Login.getLocators();
		loginPage.locatorsCheck(locators);
	}

	@Test(dependsOnMethods = { "loginPage" }, groups = { "Profile" })
	public void login() {
		Login loginPage = new Login(driver);
		OrganizationDashboard result = loginPage.loginAsUser(username, password);
		loginPage.checkIfLoginSuccessful(result.getTitleText());
	}
	
	@Test(dependsOnMethods = { "login" }, priority = 1)
	public void createOfflineDonation(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditEvent();
		SettingsOfflineDonation setOfflineDon =  dashboard.navigateToSettingsOfflineDonation();
		int donationTotalAmountBefore = setOfflineDon.getDonationsAmount();
		setOfflineDon.clickAddOfflineDonationsBtn();
		int donationValue = setOfflineDon.createOfflineDonation();
		int donationTotalAmountAfter = setOfflineDon.getDonationsAmount();
		setOfflineDon.checkDonationTotalAmount(donationTotalAmountBefore, donationValue, donationTotalAmountAfter);
	}
	
	@Test(dependsOnMethods = { "login" }, priority = 2)
	public void editOfflineDonation(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditEvent();
		SettingsOfflineDonation setOfflineDon =  dashboard.navigateToSettingsOfflineDonation();
		int donationTotalAmountBefore = setOfflineDon.getDonationsAmount();
		setOfflineDon.clickEditBtn();
		int donationValueEdited = setOfflineDon.editOfflineDonation();
		int donationTotalAmountAfter = setOfflineDon.getDonationsAmount();
		setOfflineDon.checkDonationTotalAmountAfterEdit(donationTotalAmountBefore, donationValueEdited, donationTotalAmountAfter);
	}
	
	@Test(dependsOnMethods = { "login" }, priority = 3)
	public void deleteOfflineDonation(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditEvent();
		SettingsOfflineDonation setOfflineDon =  dashboard.navigateToSettingsOfflineDonation();
		int tableRowsCountBefore = setOfflineDon.getTableRowsCount();
		setOfflineDon.deleteOfflineDonation();
		int tableRowsCountAfter = setOfflineDon.getTableRowsCount();
		setOfflineDon.checkIsDeleteOfflineDonation(tableRowsCountBefore, tableRowsCountAfter);
	}
	
	@Test(dependsOnMethods = { "login" })
	public void failToCreateOfflineDonationWithEmptyData(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditEvent();
		SettingsOfflineDonation setOfflineDon =  dashboard.navigateToSettingsOfflineDonation();
		int tableRowsCountBefore = setOfflineDon.getTableRowsCount();
		setOfflineDon.clickAddOfflineDonationsBtn();
		setOfflineDon.createOfflineDonationWithEmptyData();
		int tableRowsCountAfter = setOfflineDon.getTableRowsCount();
		setOfflineDon.checkOfflineDonationsCount(tableRowsCountBefore, tableRowsCountAfter);
	}
	
	@Test(dependsOnMethods = { "login" })
	public void assignDonationToUser(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditEvent();
		SettingsOfflineDonation setOfflineDon =  dashboard.navigateToSettingsOfflineDonation();
		setOfflineDon.switchToEvenet();
		int userRaisedDonationBefore = setOfflineDon.getUserRaisedDonation();
		setOfflineDon.switchToOldWindow();
		setOfflineDon.clickAddOfflineDonationsBtn();
		int donationValue = setOfflineDon.createOfflineDonationAssignToUser();
		setOfflineDon.switchToEvenet();
		int userRaisedDonationAfter = setOfflineDon.getUserRaisedDonation();
		setOfflineDon.checkUserRaisedDonation(userRaisedDonationBefore, donationValue, userRaisedDonationAfter);
		setOfflineDon.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" })
	public void assignDonationToTeamOwner(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditEvent();
		SettingsOfflineDonation setOfflineDon =  dashboard.navigateToSettingsOfflineDonation();
		setOfflineDon.switchToEvenet();
		int teamRaisedDonationBefore = setOfflineDon.getTestTeamRaisedDonation();
		int ownerRaisedDonationBefore = setOfflineDon.getTeamOwnerRaisedDonation();
		setOfflineDon.switchToOldWindow();
		setOfflineDon.clickAddOfflineDonationsBtn();
		int donationValue = setOfflineDon.createOfflineDonationAssignToTeamOwner();
		setOfflineDon.switchToEvenet();
		int teamRaisedDonationAfter = setOfflineDon.getTestTeamRaisedDonation();
		int ownerRaisedDonationAfter = setOfflineDon.getTeamOwnerRaisedDonation();
		setOfflineDon.checkTeamRaisedDonation(teamRaisedDonationBefore, donationValue, teamRaisedDonationAfter);
		setOfflineDon.checkOwnerRaisedDonation(ownerRaisedDonationBefore, donationValue, ownerRaisedDonationAfter);
		setOfflineDon.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" })
	public void assignDonationToTeamUser(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditEvent();
		SettingsOfflineDonation setOfflineDon =  dashboard.navigateToSettingsOfflineDonation();
		setOfflineDon.switchToEvenet();
		int teamRaisedDonationBefore = setOfflineDon.getTestTeamRaisedDonation();
		int teamUserRaisedDonationBefore = setOfflineDon.getTeamUserRaisedDonation();
		setOfflineDon.switchToOldWindow();
		setOfflineDon.clickAddOfflineDonationsBtn();
		int donationValue = setOfflineDon.createOfflineDonationAssignToTeamUser();
		setOfflineDon.switchToEvenet();
		int teamRaisedDonationAfter = setOfflineDon.getTestTeamRaisedDonation();
		int teamUserRaisedDonationAfter = setOfflineDon.getTeamUserRaisedDonation();
		setOfflineDon.checkTeamRaisedDonation(teamRaisedDonationBefore, donationValue, teamRaisedDonationAfter);
		setOfflineDon.checkTeamUserRaisedDonation(teamUserRaisedDonationBefore, donationValue, teamUserRaisedDonationAfter);
		setOfflineDon.switchToOldWindow();
	}
	
	@AfterMethod
	public void setScreenshot(ITestResult result) {
		if (!result.isSuccess()){
			String methodName = result.getName();
			String filename = null;
			try
			{
				SimpleDateFormat formater = new SimpleDateFormat("yyyy_MM_dd_hh_mm_ss");
				File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
				filename = formater.format(Calendar.getInstance().getTime()) + "_" + methodName + ".png";
				String separator = System.getProperty("file.separator");
				String path = System.getProperty("user.dir") + separator + "test-output" + separator + "screens" + separator;
				FileUtils.copyFile(scrFile, new File(path + filename));
				Reporter.setCurrentTestResult(result);
				Reporter.log("<img width='100%' src='" + "screens" + separator + filename + "'/>");
			}
			catch (IOException e1)
			{
				e1.printStackTrace();
			}
			if (driver.getWindowHandles().size() > 1) {
				handler.switchToOldWindow(driver);
			}
		}
	}
	
	@AfterClass
	public void afterClass() {
		try
		{
			driver.close();
			driver.quit();
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
	} 
}
