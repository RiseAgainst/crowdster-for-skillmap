package tests;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import eventDashboardPages.EventDashboard;
import eventPages.CheckoutPackages;
import organizationPages.OrganizationDashboard;
import pages.Login;
import utils.Data;
import utils.DriverSetup;
import utils.WindowHandler;

public class TestsCheckoutPackages {
	private WebDriver driver;
	private WindowHandler handler;
	private String username	= Data.userLogin;
	private String password	= Data.userPassword;
	
	@BeforeClass
	public void beforeClass() {
		driver = DriverSetup.getDriver("desktop");
		DriverSetup.initialSetup(driver);
		this.handler = new WindowHandler();
	}
	
	@Test
	public void loginPage() {
		Login loginPage = new Login(driver);
		ArrayList<String> locators = pages.Login.getLocators();
		loginPage.locatorsCheck(locators);
	}

	@Test(dependsOnMethods = { "loginPage" })
	public void login() {
		Login loginPage = new Login(driver);
		OrganizationDashboard result = loginPage.loginAsUser(username, password);
		loginPage.checkIfLoginSuccessful(result.getTitleText());
	}
	
	@Test(dependsOnMethods = { "login" }, priority = 1)
	public void createRegistrationPackage(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditSecondOneEvent();
		CheckoutPackages checkoutPackage = dashboard.navigateToCheckoutPackages();
		checkoutPackage.clickCreatePackage();
		checkoutPackage.createRegistrationItem();
		dashboard.switchToEvent();
		checkoutPackage.goToEventRegistration();
		checkoutPackage.checkRegistrationPackage();
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" }, priority = 2)
	public void editRegistrationPackage(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditSecondOneEvent();
		CheckoutPackages checkoutPackage = dashboard.navigateToCheckoutPackages();
		checkoutPackage.clickEditPackage();
		checkoutPackage.editItem();
		dashboard.switchToEvent();
		checkoutPackage.goToEventRegistration();
		checkoutPackage.checkRegistrationItemEdited();
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" }, priority = 3)
	public void deleteRegistrationItem(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditSecondOneEvent();
		CheckoutPackages checkoutPackage = dashboard.navigateToCheckoutPackages();
		int packagesCountBefore = checkoutPackage.getItemsCount();
		checkoutPackage.clickDeleteItem();
		dashboard.handelJavaScriptAlert();
		int packagesCountAfter = checkoutPackage.getItemsCount();
		checkoutPackage.checkPackagesCount(packagesCountBefore, packagesCountAfter);
	}
	
	@Test(dependsOnMethods = { "login" }, priority = 4)
	public void createSponsorshipPackage(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditSecondOneEvent();
		CheckoutPackages checkoutPackage = dashboard.navigateToCheckoutPackages();
		checkoutPackage.clickCreatePackage();
		checkoutPackage.createSponsorshipItem();
		dashboard.switchToEvent();
		checkoutPackage.goToEventSponsorship();
		checkoutPackage.checkSponsorshipPackage();
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" }, priority = 5)
	public void editSponsorshipPackage(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditSecondOneEvent();
		CheckoutPackages checkoutPackage = dashboard.navigateToCheckoutPackages();
		checkoutPackage.clickEditPackage();
		checkoutPackage.editItem();
		dashboard.switchToEvent();
		checkoutPackage.goToEventSponsorship();
		checkoutPackage.checkSponsorshipPackageEdited();
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" }, priority = 6)
	public void deleteSponsorshipPackage(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditSecondOneEvent();
		CheckoutPackages checkoutPackage = dashboard.navigateToCheckoutPackages();
		int packagesCountBefore = checkoutPackage.getItemsCount();
		checkoutPackage.clickDeleteItem();
		dashboard.handelJavaScriptAlert();
		int packagesCountAfter = checkoutPackage.getItemsCount();
		checkoutPackage.checkPackagesCount(packagesCountBefore, packagesCountAfter);
	}
	
	@Test(dependsOnMethods = { "login" }, priority = 7)
	public void createDonationPackage(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditSecondOneEvent();
		CheckoutPackages checkoutPackage = dashboard.navigateToCheckoutPackages();
		checkoutPackage.clickCreatePackage();
		checkoutPackage.createDonationItem();
		dashboard.switchToEvent();
		checkoutPackage.goToEventDonation();
		checkoutPackage.checkDonationPackage();
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" }, priority = 8)
	public void editDonationPackage(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditSecondOneEvent();
		CheckoutPackages checkoutPackage = dashboard.navigateToCheckoutPackages();
		checkoutPackage.clickEditPackage();
		checkoutPackage.editItem();
		dashboard.switchToEvent();
		checkoutPackage.goToEventDonation();
		checkoutPackage.checkDonationPackageEdited();
		dashboard.switchToOldWindow();
	}
	
	@Test(dependsOnMethods = { "login" }, priority = 9)
	public void deleteDonationPackage(){
		EventDashboard dashboard = new EventDashboard(driver);
		dashboard.clickEditSecondOneEvent();
		CheckoutPackages checkoutPackage = dashboard.navigateToCheckoutPackages();
		int packagesCountBefore = checkoutPackage.getItemsCount();
		checkoutPackage.clickDeleteItem();
		dashboard.handelJavaScriptAlert();
		int packagesCountAfter = checkoutPackage.getItemsCount();
		checkoutPackage.checkPackagesCount(packagesCountBefore, packagesCountAfter);
	}
	
	@AfterMethod
	public void setScreenshot(ITestResult result) {
		if (!result.isSuccess()){
			String methodName = result.getName();
			String filename = null;
			try
			{
				SimpleDateFormat formater = new SimpleDateFormat("yyyy_MM_dd_hh_mm_ss");
				File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
				filename = formater.format(Calendar.getInstance().getTime()) + "_" + methodName + ".png";
				String separator = System.getProperty("file.separator");
				String path = System.getProperty("user.dir") + separator + "test-output" + separator + "screens" + separator;
				FileUtils.copyFile(scrFile, new File(path + filename));
				Reporter.setCurrentTestResult(result);
				Reporter.log("<img width='100%' src='" + "screens" + separator + filename + "'/>");
			}
			catch (IOException e1)
			{
				e1.printStackTrace();
			}
			if (driver.getWindowHandles().size() > 1) {
				handler.switchToOldWindow(driver);
			}	
		}
	}
	
	@AfterClass
	public void afterClass() {
		try
		{
			driver.close();
			driver.quit();
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
	} 
}
