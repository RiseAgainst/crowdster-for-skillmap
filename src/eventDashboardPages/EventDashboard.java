package eventDashboardPages;

import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;

import pages.Menu;

import displayServices.EventTemplates;
import eventPages.Appearance;
import eventPages.AppearanceDetails;
import eventPages.CheckoutCustomQuestions;
import eventPages.DonationPage;
import eventPages.Event;
import eventPages.CheckoutItems;
import eventPages.CheckoutPackages;
import eventPages.CheckoutPromoCode;
import utils.DriverUtils;
import utils.Misc;
import utils.WaitTool;
import utils.WebTable;
import utils.WindowHandler;



public class EventDashboard {
	private WebDriver		driver;
	private WindowHandler 	handler;
	private WebDriverWait 	wait;
	private static final int 	WAIT_4_ELEMENT		= WaitTool.DEFAULT_WAIT_4_ELEMENT;
	public static String		PAGE_URL			= utils.Data.baseURL + "/auth/site/display/my/sites.do";
	private static String micrositeTable			= "//*[@id='sites-table']";
	private static String sortByName				= "//*[@id='sites-table']/thead/tr/th[1]";
	private static String sortBySsName				= "//*[@id='sites-table']/thead/tr/th[2]";
	private static String sortByRaisedCount			= "//*[@id='sites-table']/thead/tr/th[3]";
	private static String sortByDonorsCount			= "//*[@id='sites-table']/thead/tr/th[4]";
	private static String sortByPageCount			= "//*[@id='sites-table']/thead/tr/th[5]";
	private static String sortByCreateDate			= "//*[@id='sites-table']/thead/tr/th[6]";
	//private static String searchInput				= "//*[@id='sites-table_filter']/label/input";
	private static String searchInput				= "/html/body/div[4]/div/div[1]/div[2]/div/div[2]/label/input";
	private static String selectEntries				= "//*[@id='sites-table_length']/label/select";
	private static String overviewNumber			= "numbers";
	
	private static By eventsTableIdLocator 			= By.id("sites-table");
	private static By siteConfigIdLocator 			= By.id("site-configration-nav");
	private static By offlineDonationIdLocator 		= By.id("offline-donation-nav");
	private static By settingsMainTabIdLocator  	= By.id("settingsMainTab");
	private static By mobileNavigationIdLocator 	= By.id("mnav");
	private static By mobileAppearanceIdLocator 	= By.id("mapp");
	private static By mobileIdLocator 				= By.id("mobile-li");
	private static By webSiteIdLocator				= By.id("website-li");
	private static By homepageLayoutId				= By.id("homepage-layout-nav");
	private static By viewSiteIdLocator				= By.id("viewsitehrefId");
	private static By donationPageLinkLocator		= By.linkText("Donation Page");
	private static By iframeCssLocator				= By.cssSelector("iframe");
	private static By appearanceLinkTextLocator 	= By.linkText("Appearance");
	private static By appearanceDetailsLocator		= By.linkText("Appearance Details");
	private static By checkoutIdLocator				= By.id("registration-li");
	private static By auctionIdLocator				= By.id("auction-li");
	private static By manageCampaignsIdLocator		= By.id("manageCampaignsTab");
	private static By checkoutItemIdLocator			= By.id("itemtab");
	private static By checkoutPackageIdLocator		= By.id("package-tab");
	private static By checkoutCustomQuestionTextLinkLocator = By.linkText("Custom Questions");
	private static By checkoutPromoCodeTextLinkLocator 		= By.linkText("Promo Code");
	private static By auctionItemsTextLinkLocator 	= By.linkText("Auction Items");
	private static By configurationTextLinkLocator 	= By.linkText("Configuration");
	private static By editEventLinkTextLocator		= By.linkText("Edit");
	private static By websiteMsgsLinkTextLocator	= By.linkText("Website Messages");
	private static By websiteFormsLinkTextLocator	= By.linkText("Website Forms");
	private static By websiteFormsTableIdLocator 	= By.id("sites-table");
	
	private static String micrositeCountXpath		= "//*[@id='siteWidth']/div[1]/div[1]/div[2]/h2";
	private static String tableFirstRowXpath 		= "/html/body/div[4]/div/div[1]/div[2]/div/table/tbody/tr[1]";
	private static String editEventLinkText			= "Edit";
	private static String navigationId				= "sub-pages-nav";
	private static String firstEventName			= "FirstEvent";
	private static String secondOneEventName		= "SecondOne";
	private static String eventWithDonation			= "Another";
	private static String mainMenuIdSelector		= "main_menu_icon";
	
	public EventDashboard(WebDriver driver) {
		this.driver = driver;
		this.handler = new WindowHandler();
		this.wait = new WebDriverWait(driver, 5);
		if (!isAt())
		{
			Menu menu = clickMenu();
			menu.clickEventDashboard();
		}
	}
	
	public boolean isAt() {
		return driver.getCurrentUrl().equals(PAGE_URL);
	}
	
	public By getIframeLocator(){
		return iframeCssLocator;
	}
	
	public By getTableIdLocator(){
		return websiteFormsTableIdLocator;
	}
	
	public void switchToEvent(){
		handler.getOldWindow(driver);
		DriverUtils.click(viewSiteIdLocator, driver);
		WaitTool.waitForJQueryProcessing(driver, 5);
		handler.switchToNewWindow(driver);
	}
	
	public void switchToOldWindow(){
		handler.switchToOldWindow(driver);
	}
	
	public void switchToIframe(){
		WebElement iFrame= WaitTool.waitForElement(driver, iframeCssLocator, WAIT_4_ELEMENT);
		driver.switchTo().frame(iFrame);
	}
	
	public void switchToIframe(By locator){
		WebElement iFrame= WaitTool.waitForElement(driver, locator, WAIT_4_ELEMENT);
		driver.switchTo().frame(iFrame);
	}
	
	public void switchToFirstIframe(){
		List<WebElement> iFrames = WaitTool.waitForListElementsPresent(driver, iframeCssLocator, WAIT_4_ELEMENT);
		driver.switchTo().frame(iFrames.get(0));
	}
	
	public void switchToSecondIframe(){
		List<WebElement> iFrames = WaitTool.waitForListElementsPresent(driver, iframeCssLocator, WAIT_4_ELEMENT);
		driver.switchTo().frame(iFrames.get(1));
	}
	
	public void switchToThirdIframe(){
		List<WebElement> iFrames = WaitTool.waitForListElementsPresent(driver, iframeCssLocator, WAIT_4_ELEMENT);
		driver.switchTo().frame(iFrames.get(2));
	}
	
	public void switchToDefaultContent(){
		driver.switchTo().defaultContent();
	}
	
	public void showEntries() {
		new Select(driver.findElement(By.xpath(selectEntries))).selectByVisibleText("50");
	}
	
	public void searchBy(String searchValue) {
		WebElement searchInputElement = WaitTool.waitForElement(driver, By.xpath(searchInput), WAIT_4_ELEMENT);
		searchInputElement.clear();
		searchInputElement.sendKeys(searchValue);
		utils.WaitTool.waitForJQueryProcessing(driver, 5);
	}
	

	public void searchClear() {
		driver.findElement(By.xpath(searchInput)).clear();
		driver.findElement(By.xpath(searchInput)).sendKeys(Keys.ENTER);
		utils.WaitTool.waitForJQueryProcessing(driver, 5);
	}
	
	public EventTemplates clickSaveAsTemplate() {
		WebElement table 	= driver.findElement(By.xpath(micrositeTable));
		List<WebElement>firstRow	= table.findElements(By.tagName("tr"));
		Actions action = new Actions(driver);
		action.moveToElement(firstRow.get(1)).perform();
		firstRow.get(1).findElement(By.linkText("Save as Template")).click();
		utils.WaitTool.waitForJQueryProcessing(driver, 5);
		return new EventTemplates(driver);
	}
	
	public EventEdit clickCopy() {
		WebElement table 	= driver.findElement(By.xpath(micrositeTable));
		List<WebElement>firstRow	= table.findElements(By.tagName("tr"));
		Actions action = new Actions(driver);
		action.moveToElement(firstRow.get(1)).perform();
		firstRow.get(1).findElement(By.linkText("Copy")).click();
		utils.WaitTool.waitForJQueryProcessing(driver, 5);
		return new EventEdit(driver);
	}
	
	public void handelJavaScriptAlert(){		
		wait.until(ExpectedConditions.alertIsPresent());
		driver.switchTo().alert().accept();
		WaitTool.waitForJQueryProcessing(driver, WAIT_4_ELEMENT);
	}
	
	public void sortByName() {
		DriverUtils.click(By.xpath(sortByName), driver);
		utils.WaitTool.waitForJQueryProcessing(driver, 5);
	}
	
	public void sortBySsName() {
		DriverUtils.click(By.xpath(sortBySsName), driver);
		utils.WaitTool.waitForJQueryProcessing(driver, 5);
	}
	
	public void sortByRaisedCount() {
		DriverUtils.click(By.xpath(sortByRaisedCount), driver);
		utils.WaitTool.waitForJQueryProcessing(driver, 5);
	}
	
	public void sortByDonorsCount() {
		DriverUtils.click(By.xpath(sortByDonorsCount), driver);
		utils.WaitTool.waitForJQueryProcessing(driver, 5);
	}
	
	public void sortByPageCount() {
		DriverUtils.click(By.xpath(sortByPageCount), driver);
		utils.WaitTool.waitForJQueryProcessing(driver, 5);
	}
	
	public void sortByCreateDate() {
		DriverUtils.click(By.xpath(sortByCreateDate), driver);
		utils.WaitTool.waitForJQueryProcessing(driver, 5);
	}
	
	public int getTotalRaisedValue() {
		List<WebElement> overviewNumbers = driver.findElements(By.className(overviewNumber));
		String totalRaiseValue = overviewNumbers.get(0).getText();
		totalRaiseValue = totalRaiseValue.replaceAll(",", "");
		return Integer.parseInt((totalRaiseValue.substring(totalRaiseValue.indexOf("$") + 1)));
	}
	
	public int getDonorsCount() {
		List<WebElement> overviewNumbers = driver.findElements(By.className(overviewNumber));
		String donorsValue = overviewNumbers.get(1).getText();
		return Integer.parseInt(donorsValue);
	}
	
	public int getPagesValue() {
		List<WebElement> overviewNumbers = driver.findElements(By.className(overviewNumber));
		String pagesValue = overviewNumbers.get(2).getText();
		return Integer.parseInt(pagesValue);
	}
	
	public int getEventsCountValue() {
		String micrositeText = driver.findElement(By.xpath(micrositeCountXpath)).getText();
		String micrositeCount = micrositeText.substring(micrositeText.indexOf("(") + 1, micrositeText.indexOf(")"));
		return Integer.parseInt(micrositeCount);
	}
	
	
	public int getCountFromTableFirstColumn() {
		String[] micrositeCountFromTable = WebTable.getDataFromColumn(micrositeTable, 1, driver);
		return micrositeCountFromTable.length;
	}
	
	public int getTotalRaisedValueFromTable() {
		String[] totalRaisedFromTable = WebTable.getDataFromColumn(micrositeTable, 3, driver);
		for (int i = 0; i < totalRaisedFromTable.length; i++) {
			totalRaisedFromTable[i] = totalRaisedFromTable[i].replaceAll(",", "");
			
		}
		int[] results = new int[totalRaisedFromTable.length];
		int sum = 0;
		for (int i = 0; i < totalRaisedFromTable.length; i++) {
			results[i] = Integer.parseInt(totalRaisedFromTable[i].substring(totalRaisedFromTable[i].indexOf("$") + 1));
			sum = sum + results[i];
		}
		return sum;
	}
	
	public int getValueFromTable(int column) {
		String[] totalRaisedFromTable = WebTable.getDataFromColumn(micrositeTable, column, driver);
		int[] results = new int[totalRaisedFromTable.length];
		int sum = 0;
		for (int i = 0; i < totalRaisedFromTable.length; i++) {
			results[i] = Integer.parseInt(totalRaisedFromTable[i]);
			sum = sum + results[i];
		}
		return sum;
	}
	
	public void checkOverviewNumbers(int valueFromOverviewNubmer, int valueFromTable) {
		Assert.assertTrue(valueFromOverviewNubmer == valueFromTable , "Something wrong with overview numbers");
	}
	
	public void checkDataAfterSearch(int column, String keyword) {
		String[] result = WebTable.getDataFromColumn(micrositeTable, column, driver);
		for (int i = 0; i < result.length; i++)
		{
			if (!result[i].contains(keyword))
			{
				Reporter.log("'" + result[i] + "' does not contain keyword: '" + keyword + "'");
				Assert.fail("'" + result[i] + "' does not contain keyword: '" + keyword + "'");
                System.out.println(result[i]);
			}
		}
	}
	
	public void checkData(String solName) {
		String result = WebTable.getFirstElementFromColumn(micrositeTable, 1, driver);
		Assert.assertTrue(solName.equals(result), "New solution doesn't contain expected name");
	}
	
	public void checkRenamedOrganization(String solName) {
		String[] result = WebTable.getDataFromColumn(micrositeTable, 1, driver);
		Boolean checkResult = false;
		for(int i = 0; i < result.length; i++){
			if (result[i].equals(solName)) {
				checkResult = true;
			}
		}
		Assert.assertTrue(checkResult, "Renamed solution doesn't contains in solution table");
	}
	
	public void checkSortingForIntegerValues(int column) {
		String[] data = WebTable.getDataFromColumn(micrositeTable, column, driver);
		for (int i = 0; i < data.length; i++) {
			data[i] = data[i].replaceAll(",", "");
			
		}
		int[] results = new int[data.length];
		for (int i = 0; i < data.length; i++) {
			results[i] = Integer.parseInt(data[i].substring(data[i].indexOf("$") + 1));	
		}
		int[] initial = new int[results.length];
		System.arraycopy(results, 0, initial, 0, results.length);
		Arrays.sort(results);
		System.out.println(Arrays.toString(initial));
		System.out.println(Arrays.toString(results));
		if (!Misc.intArraysEqual(initial, results)) {
			Reporter.log("Data are NOT sorted properly");
			Assert.fail("Data are NOT sorted properly");
		}
		else {
			Reporter.log("Data are sorted OK");
		}
	}
	
	public void checkSorting(int column) {
		String[] data = WebTable.getDataFromColumn(micrositeTable, column, driver);
		String[] initial = new String[data.length];
		System.arraycopy(data, 0, initial, 0, data.length);
		Arrays.sort(data, String.CASE_INSENSITIVE_ORDER);
		if (!Misc.stringArraysEqual(initial, data)) {
			Reporter.log("Data are NOT sorted properly");
			Assert.fail("Data are NOT sorted properly");
		}
		else {
			Reporter.log("Data are sorted OK");
		}
	}
	
	public int getTableRowsCount(By tableIdLocator){
		WebElement webTableElement = WaitTool.waitForElement(driver, tableIdLocator, WAIT_4_ELEMENT);
		WebTable webTable = new WebTable(webTableElement, driver);
		return webTable.getRowCount();
	}
	
	public WebElement hoverOverFirstEvent(){
		this.searchBy(firstEventName);
		WebElement eventElement = WebTable.getFirstElementFromColumnAsWebElementByIdLocator(eventsTableIdLocator, 1, driver);
		Actions builder = new Actions(driver);
		builder.moveToElement(eventElement).perform();
		return eventElement;
	}
	
	public Menu clickMenu() {
		if(DriverUtils.checkBrowser(driver).equals("chrome")){
			JavascriptExecutor jse = (JavascriptExecutor)driver;
			jse.executeScript("document.getElementById('" + mainMenuIdSelector + "').click();");
		}else {
			WaitTool.waitForElement(driver, By.id(mainMenuIdSelector), WAIT_4_ELEMENT).click();
		}
		WaitTool.waitForJQueryProcessing(driver, 5);
		return new Menu(driver);
	}
	
	public void clickEditEvent(){
		WebElement tr = WaitTool.waitForElement(driver, By.xpath(tableFirstRowXpath), WAIT_4_ELEMENT);
		Actions builder = new Actions(driver);
		builder.moveToElement(tr).perform();		
		WaitTool.waitForElement(driver, By.linkText(editEventLinkText), WAIT_4_ELEMENT).click();
	}
	
	public Event clickEditFirstEvent(){
		this.searchBy(firstEventName);
		WebElement eventElement = WebTable.getFirstElementFromColumnAsWebElementByIdLocator(eventsTableIdLocator, 1, driver);
		Actions builder = new Actions(driver);
		builder.moveToElement(eventElement).perform();		
		WaitTool.waitForElement(driver, editEventLinkTextLocator, WAIT_4_ELEMENT).click();	
		return new Event(driver);
	}
	
	public void clickEditSecondOneEvent(){
		this.searchBy(secondOneEventName);
		WebElement eventElement = WebTable.getFirstElementFromColumnAsWebElementByIdLocator(eventsTableIdLocator, 1, driver);
		Actions builder = new Actions(driver);
		builder.moveToElement(eventElement).perform();		
		WaitTool.waitForElement(driver, By.linkText(editEventLinkText), WAIT_4_ELEMENT).click();
	}
	
	public void clickEditLastEvent(){
		WebElement eventElement = WebTable.getLastElementFromColumnAsWebElementByIdLocator(eventsTableIdLocator, 1, driver);
		Actions builder = new Actions(driver);
		builder.moveToElement(eventElement).perform();		
		WaitTool.waitForElement(driver, By.linkText(editEventLinkText), WAIT_4_ELEMENT).click();
	}
	
	public void clickEditRaisedEvent(){
		this.searchBy(eventWithDonation);
		WebElement eventElement = WebTable.getFirstElementFromColumnAsWebElementByIdLocator(eventsTableIdLocator, 1, driver);
		Actions builder = new Actions(driver);
		builder.moveToElement(eventElement).perform();		
		WaitTool.waitForElement(driver, By.linkText(editEventLinkText), WAIT_4_ELEMENT).click();
	}

	public NavigationPages navigateToNavigation() {
		WaitTool.waitForElement(driver, webSiteIdLocator, WAIT_4_ELEMENT).click();
		WaitTool.waitForElement(driver, By.id(navigationId), WAIT_4_ELEMENT).click();
		WaitTool.waitForJQueryProcessing(driver, 5);
		return new NavigationPages(driver);
	}
	
	public WebsiteMessages navigateToWebsiteMessages() {
		WaitTool.waitForElement(driver, webSiteIdLocator, WAIT_4_ELEMENT).click();
		WaitTool.waitForElement(driver, websiteMsgsLinkTextLocator, WAIT_4_ELEMENT).click(); 
		WaitTool.waitForJQueryProcessing(driver, 5);
		return new WebsiteMessages(driver);
	}
	
	public WebsiteForms navigateToWebsiteForms() {
		WaitTool.waitForElement(driver, webSiteIdLocator, WAIT_4_ELEMENT).click();
		WaitTool.waitForElement(driver, websiteFormsLinkTextLocator, WAIT_4_ELEMENT).click(); 
		WaitTool.waitForJQueryProcessing(driver, 5);
		return new WebsiteForms(driver);
	}
	
	public MobileAppearance navigateToMobileAppearance(){
		WaitTool.waitForElement(driver, mobileIdLocator, WAIT_4_ELEMENT).click();
		WaitTool.waitForElement(driver, mobileAppearanceIdLocator, WAIT_4_ELEMENT).click();
		WaitTool.waitForJQueryProcessing(driver, 5);
		return new MobileAppearance(driver);
	}
	
	public MobileNavigation navigateToMobileNavigation(){
		WaitTool.waitForElement(driver, mobileIdLocator, WAIT_4_ELEMENT).click();
		WaitTool.waitForElement(driver, mobileNavigationIdLocator, WAIT_4_ELEMENT).click();
		WaitTool.waitForJQueryProcessing(driver, 5);
		return new MobileNavigation(driver);
	}
	
	public SettingsSiteConfig navigateToSiteConfiguration() {
		WaitTool.waitForElement(driver, settingsMainTabIdLocator, WAIT_4_ELEMENT).click();
		WaitTool.waitForElement(driver, siteConfigIdLocator, WAIT_4_ELEMENT).click();
		WaitTool.waitForJQueryProcessing(driver, 5);
		return new SettingsSiteConfig(driver);
	}
	
	public SettingsOfflineDonation navigateToSettingsOfflineDonation(){
		WaitTool.waitForElement(driver, settingsMainTabIdLocator, WAIT_4_ELEMENT).click();
		WaitTool.waitForElement(driver, offlineDonationIdLocator, WAIT_4_ELEMENT).click();
		WaitTool.waitForJQueryProcessing(driver, 5);
		return new SettingsOfflineDonation(driver);
	}
	
	public WebsiteHomepageLayout navigateToHomepageLayout(){
		WaitTool.waitForElement(driver, webSiteIdLocator, WAIT_4_ELEMENT).click();
		WaitTool.waitForElement(driver, homepageLayoutId, WAIT_4_ELEMENT).click();
		WaitTool.waitForJQueryProcessing(driver, 5);
		return new WebsiteHomepageLayout(driver);
	}
	
	public DonationPage navigateToDonationPage(){
		WaitTool.waitForElement(driver, webSiteIdLocator, WAIT_4_ELEMENT).click();
		WaitTool.waitForElement(driver, donationPageLinkLocator, WAIT_4_ELEMENT).click();
		WaitTool.waitForJQueryProcessing(driver, 5);
		return new DonationPage(driver);
	}
	
	public Appearance navigateToAppearance(){
		WaitTool.waitForElement(driver, webSiteIdLocator, WAIT_4_ELEMENT).click();
		WaitTool.waitForElement(driver, appearanceLinkTextLocator, WAIT_4_ELEMENT).click();
		WaitTool.waitForJQueryProcessing(driver, 5);
		return new Appearance(driver);
	}
	
	public AppearanceDetails navigateToAppearanceDetails(){
		WaitTool.waitForElement(driver, webSiteIdLocator, WAIT_4_ELEMENT).click();
		WaitTool.waitForElement(driver, appearanceDetailsLocator, WAIT_4_ELEMENT).click();
		WaitTool.waitForJQueryProcessing(driver, 5);
		return new AppearanceDetails(driver);
	}
	
	public CheckoutItems navigateToCheckoutItems(){
		WaitTool.waitForElement(driver, checkoutIdLocator, WAIT_4_ELEMENT).click();
		WaitTool.waitForElement(driver, checkoutItemIdLocator, WAIT_4_ELEMENT).click();
		WaitTool.waitForJQueryProcessing(driver, 5);
		return new CheckoutItems(driver);
	}
	
	public CheckoutPackages navigateToCheckoutPackages(){
		WaitTool.waitForElement(driver, checkoutIdLocator, WAIT_4_ELEMENT).click();
		WaitTool.waitForElement(driver, checkoutPackageIdLocator, WAIT_4_ELEMENT).click();
		WaitTool.waitForJQueryProcessing(driver, 5);
		return new CheckoutPackages(driver);
	}
	
	public CheckoutCustomQuestions navigateToCheckoutCustomQuestions(){
		WaitTool.waitForElement(driver, checkoutIdLocator, WAIT_4_ELEMENT).click();
		WaitTool.waitForElement(driver, checkoutCustomQuestionTextLinkLocator, WAIT_4_ELEMENT).click();
		WaitTool.waitForJQueryProcessing(driver, 5);
		return new CheckoutCustomQuestions(driver);
	}
	
	public CheckoutPromoCode navigateToCheckoutPromoCode(){
		WaitTool.waitForElement(driver, checkoutIdLocator, WAIT_4_ELEMENT).click();
		WaitTool.waitForElement(driver, checkoutPromoCodeTextLinkLocator, WAIT_4_ELEMENT).click();
		WaitTool.waitForJQueryProcessing(driver, 5);
		return new CheckoutPromoCode(driver);
	}
	
	public AuctionItem navigateToAuction(){
		WaitTool.waitForElement(driver, auctionIdLocator, WAIT_4_ELEMENT).click();
		WaitTool.waitForElement(driver, auctionItemsTextLinkLocator, WAIT_4_ELEMENT).click();
		WaitTool.waitForJQueryProcessing(driver, 5);
		return new AuctionItem(driver);
	}
	
	public ManageCampaignsConfig navigateToManageCampaignsConfiguration(){
		WaitTool.waitForElement(driver, manageCampaignsIdLocator, WAIT_4_ELEMENT).click();
		WaitTool.waitForElement(driver, configurationTextLinkLocator, WAIT_4_ELEMENT).click();
		WaitTool.waitForJQueryProcessing(driver, 5);
		return new ManageCampaignsConfig(driver);
	}
}
