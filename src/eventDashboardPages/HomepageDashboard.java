package eventDashboardPages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import homepageDashboardPages.HomepageEventWidgetLayout;
import homepageDashboardPages.HomepageLayout;
import homepageDashboardPages.HomepageMasthead;
import homepageDashboardPages.HomepageNavigation;
import homepageDashboardPages.HomepageSubPageLayout;
import pages.Menu;
import utils.DriverUtils;
import utils.WaitTool;
import utils.WebTable;
import utils.WindowHandler;

public class HomepageDashboard {
	private WebDriver			driver;
	private WindowHandler 		handler;
	private WebDriverWait 		wait;
	private static final int 	WAIT_4_ELEMENT			= WaitTool.DEFAULT_WAIT_4_ELEMENT;
	public static String		PAGE_URL				= utils.Data.baseURL + "/auth/member/digest/my/supersites.do";
	private static String 		mainMenuIdSelector		= "main_menu_icon";
	
	private static By viewSiteIdLocator					= By.id("homepage_view_site");
	private static By iframeCssLocator					= By.cssSelector("iframe");
	private static By searchInputXpathLoxator			= By.xpath("//*[@id='sites-table_filter']/label/input");
	private static By tableIdLocator					= By.id("sites-table");
	private static By manageLinkTextLocator				= By.linkText("Manage");
	private static By createEventBtnIdLocator			= By.id("micro-lic-popup");
	private static By homepageLayoutDashboardIdLocator	= By.id("supersiteLayOutMainTab");
	private static By mastheadLinkTextLocator			= By.linkText("Masthead");
	private static By homepageLayoutLinkTextLocator		= By.linkText("Home Page Layout");
	private static By homepageEventsWidgetLayoutLinkTextLocator		= By.linkText("MS Widget Layout");
	private static By homepageSubPageLayoutLinkTextLocator			= By.linkText("Sub-Page Layout");
	private static By homepageNavigationIdLocator		= By.id("homepage_navigation");
	
	
	public HomepageDashboard(WebDriver driver) {
		this.driver = driver;
		this.handler = new WindowHandler();
		this.wait = new WebDriverWait(driver, 5);
		if (!isAt())
		{
			Menu menu = clickMenu();
			menu.clickHomepageDashboard();
		}
	}
	
	public boolean isAt() {
			return driver.getCurrentUrl().equals(PAGE_URL);
	}
	
	public Menu clickMenu() {
		if(DriverUtils.checkBrowser(driver).equals("chrome")){
			JavascriptExecutor jse = (JavascriptExecutor)driver;
			jse.executeScript("document.getElementById('" + mainMenuIdSelector + "').click();");
		}else {
			WaitTool.waitForElement(driver, By.id(mainMenuIdSelector), WAIT_4_ELEMENT).click();
		}
		WaitTool.waitForJQueryProcessing(driver, 5);
		return new Menu(driver);
//		DriverUtils.click(By.id(mainMenuIdSelector), driver);
//		WaitTool.waitForJQueryProcessing(driver, 5);
//		return new Menu(driver);
	}
	
	public void searchBy(String searchValue) {
		searchClear();
		WaitTool.waitForElement(driver, searchInputXpathLoxator, WAIT_4_ELEMENT).sendKeys(searchValue);
		WaitTool.waitForJQueryProcessing(driver, 5);
	}

	public void searchClear() {
		WebElement searchInputElement = WaitTool.waitForElement(driver, searchInputXpathLoxator, WAIT_4_ELEMENT);
		searchInputElement.clear();
		searchInputElement.sendKeys(Keys.ENTER);
		WaitTool.waitForJQueryProcessing(driver, 5);
	}
	
	public void clickManage() {
		WebElement firstRow = WebTable.getFirstElementFromColumnAsWebElementByIdLocator(tableIdLocator, 1, driver);
		Actions builder = new Actions(driver);
		builder.moveToElement(firstRow).perform();		
		WaitTool.waitForElement(driver, manageLinkTextLocator, WAIT_4_ELEMENT).click();
		WaitTool.waitForJQueryProcessing(driver, WAIT_4_ELEMENT);
	}
	
	public CreateEvent clickCreateEvent() {
		DriverUtils.click(createEventBtnIdLocator, driver);
		return new CreateEvent(driver);
	}
	
	public void switchToEvent(){
		handler.getOldWindow(driver);
		DriverUtils.click(viewSiteIdLocator, driver);
		handler.switchToNewWindow(driver);
	}
	
	public void switchToHomepage(){
		handler.getOldWindow(driver);
		DriverUtils.click(viewSiteIdLocator, driver);
		handler.switchToNewWindow(driver);
	}
	
	public void switchToOldWindow(){
		handler.switchToOldWindow(driver);
	}
	
	public void switchToIframe(){
		WebElement iFrame= WaitTool.waitForElement(driver, iframeCssLocator, WAIT_4_ELEMENT);
		driver.switchTo().frame(iFrame);
	}
	
	public void switchToDefaultContent(){
		driver.switchTo().defaultContent();
	}
	
	public void handelJavaScriptAlert(){		
		wait.until(ExpectedConditions.alertIsPresent());
		driver.switchTo().alert().accept();
		WaitTool.waitForJQueryProcessing(driver, WAIT_4_ELEMENT);
	}
	
	public HomepageMasthead navigateToMasthead(){
		WaitTool.waitForElement(driver, homepageLayoutDashboardIdLocator, WAIT_4_ELEMENT).click();
		WaitTool.waitForElement(driver, mastheadLinkTextLocator, WAIT_4_ELEMENT).click();
		WaitTool.waitForJQueryProcessing(driver, 5);
		return new HomepageMasthead(driver);
	}
	
	public HomepageLayout navigateToHomepageLayout(){
		WaitTool.waitForElement(driver, homepageLayoutDashboardIdLocator, WAIT_4_ELEMENT).click();
		WaitTool.waitForElement(driver, homepageLayoutLinkTextLocator, WAIT_4_ELEMENT).click();
		WaitTool.waitForJQueryProcessing(driver, 5);
		return new HomepageLayout(driver);
	}
	
	public HomepageEventWidgetLayout navigateToHomepageEventWidgetLayout(){
		WaitTool.waitForElement(driver, homepageLayoutDashboardIdLocator, WAIT_4_ELEMENT).click();
		WaitTool.waitForElement(driver, homepageEventsWidgetLayoutLinkTextLocator, WAIT_4_ELEMENT).click();
		WaitTool.waitForJQueryProcessing(driver, 5);
		return new HomepageEventWidgetLayout(driver);
	}
	
	public HomepageSubPageLayout navigateToHomepageSubPageLayout(){
		WaitTool.waitForElement(driver, homepageLayoutDashboardIdLocator, WAIT_4_ELEMENT).click();
		WaitTool.waitForElement(driver, homepageSubPageLayoutLinkTextLocator, WAIT_4_ELEMENT).click();
		WaitTool.waitForJQueryProcessing(driver, 5);
		return new HomepageSubPageLayout(driver);
	}
	
	public HomepageNavigation navigateToNavigation(){
		WaitTool.waitForElement(driver, homepageNavigationIdLocator, WAIT_4_ELEMENT).click();
		WaitTool.waitForJQueryProcessing(driver, 5);
		return new HomepageNavigation(driver);
	}
}
