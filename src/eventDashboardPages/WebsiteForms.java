package eventDashboardPages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;

import utils.DriverUtils;
import utils.WaitTool;
import utils.WebTable;

public class WebsiteForms {
	private WebDriver 			driver;
	private static final int 	WAIT_4_ELEMENT			= WaitTool.DEFAULT_WAIT_4_ELEMENT;
	
	private static String pageTitleValue				= "TEST Page title";
	private static String navigationLabelValue			= "TEST Navigation label";
	private static String leftSideContentValue			= "TEST Left side content.";
	private static String rightSideContentValue			= "TEST Right side content.";
	private static String nextPageContentValue			= "TEST Next page content.";
	
	private static By createFormBtnIdLocator			= By.id("create_form_btn");
	private static By saveBtnIdLocator					= By.id("pageBtn");
	private static By pageTitleNameLocator				= By.name("name");
	private static By navigationLabelNameLocator		= By.name("navigationLabel");
	private static By bodyElementTagLocator				= By.tagName("body");
	private static By toggleButtonsCssLocator			= By.cssSelector(".toggle");
	private static By naviLabelsCssLocator				= By.cssSelector(".drop");
	private static By pageTitleCssLocator				= By.cssSelector("div.page-container-left-header");
	private static By leftSideContentCssLocator			= By.cssSelector("div.content-pane");
	private static By rightSideContentCssLocator		= By.cssSelector("div.page-container-right-inner");
	private static By continueBtnNameLocator			= By.name("submit-button");
	private static By deleteLinkTextLocator				= By.linkText("Delete");
	
	public WebsiteForms(WebDriver driver) {
		this.driver = driver;
	}
	
	public void clickCreateFormBtn(){
		DriverUtils.click(createFormBtnIdLocator, driver);
		WaitTool.waitForJQueryProcessing(driver, 5);
	}
	
	public void clickDeleteForm(By tableLocator){
		WebElement firstFormElement = WebTable.getFirstElementFromColumnAsWebElementByIdLocator(tableLocator, 1, driver);
		Actions builder = new Actions(driver);
		builder.moveToElement(firstFormElement).perform();	
		firstFormElement.findElement(deleteLinkTextLocator).click();
	}
	
	public void clickSaveBtn(){
		DriverUtils.click(saveBtnIdLocator, driver);
		WaitTool.waitForJQueryProcessing(driver, 5);
	}
	
	public void clickOnForm(){
		DriverUtils.clickElement(this.getFormNavigationLabel(), driver);
		WaitTool.waitForJQueryProcessing(driver, 5);
	}
	
	public void clickContinueBtn(){
		DriverUtils.click(continueBtnNameLocator, driver);
		WaitTool.waitForJQueryProcessing(driver, 5);
	}
	
	public void clickToggleElements(){
		List<WebElement> toggleElements = WaitTool.waitForListElementsPresent(driver, toggleButtonsCssLocator, WAIT_4_ELEMENT);
		for(WebElement toggleElement : toggleElements){
			DriverUtils.clickElement(toggleElement, driver);
		}
	}
	
	public void fillPageTitle(){
		WaitTool.waitForElement(driver, pageTitleNameLocator, WAIT_4_ELEMENT).sendKeys(pageTitleValue);
	}
	
	public void fillNavigationLabel(){
		WaitTool.waitForElement(driver, navigationLabelNameLocator, WAIT_4_ELEMENT).sendKeys(navigationLabelValue);
	}
	
	public void fillLeftSideContent(){
		WaitTool.waitForElement(driver, bodyElementTagLocator, WAIT_4_ELEMENT).sendKeys(leftSideContentValue);
	}
	
	public void fillRightSideContent(){
		WaitTool.waitForElement(driver, bodyElementTagLocator, WAIT_4_ELEMENT).sendKeys(rightSideContentValue);
	}
	
	public void fillNextPageContent(){
		WaitTool.waitForElement(driver, bodyElementTagLocator, WAIT_4_ELEMENT).sendKeys(nextPageContentValue);
	}
	
	private WebElement getFormNavigationLabel(){
		List<WebElement> navigationLabels = WaitTool.waitForListElementsPresent(driver, naviLabelsCssLocator, WAIT_4_ELEMENT);
		WebElement formNavigationLabel = navigationLabels.get(1).findElement(By.tagName("a"));
		return formNavigationLabel;
	}
	
	private String getElementText(By elementLocator){
		return WaitTool.waitForElement(driver, elementLocator, WAIT_4_ELEMENT).getText().trim();
	}
	
	//Asserts
	
	public void checkFormsCountAfterCreated(int formsCountBefore, int formsCountAfter){
		Assert.assertTrue(formsCountAfter == formsCountBefore + 1, "Forms count are not plus one.");
	}
	
	public void checkFormPageTitle(){
		String pageTitle = WaitTool.waitForElement(driver, pageTitleCssLocator, WAIT_4_ELEMENT).getText();
		Assert.assertTrue(pageTitle.equals(pageTitleValue), "Wrong page title.");
	}
	
	public void checkNavigationLabel(){
		String eventNavigationLabelTest = this.getFormNavigationLabel().getText().trim();
		Assert.assertTrue(eventNavigationLabelTest.equals(navigationLabelValue), "Wrong navigation label.");
	}
	
	public void checkLeftSideContent(){
		String leftSideContentText = this.getElementText(leftSideContentCssLocator);
		Assert.assertTrue(leftSideContentText.equals(leftSideContentValue), "Wrong left side content.");
	}
	
	public void checkRightSideContent(){
		String rightSideContentText = this.getElementText(rightSideContentCssLocator);
		Assert.assertTrue(rightSideContentText.equals(rightSideContentValue), "Wrong right side content.");
	}
	
	public void checkNextPageContent(){
		String nextPageContentText = this.getElementText(leftSideContentCssLocator);
		Assert.assertTrue(nextPageContentText.equals(nextPageContentValue), "Wrong next page content.");
	}
	
	public void checkFormsCountAfterDeleted(int formsCountBefore, int formsCountAfter){
		Assert.assertTrue(formsCountAfter == formsCountBefore - 1, "Forms count are not less.");
	}
}
