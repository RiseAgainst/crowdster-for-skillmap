package eventDashboardPages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import eventPages.NavigationPage;
import utils.DriverUtils;
import utils.WaitTool;

public class NavigationPages {

	private  WebDriver		driver;
	private static final int WAIT_4_ELEMENT				= WaitTool.DEFAULT_WAIT_4_ELEMENT;
	
	private static By navigationPagesLabelsCssLocator 	= By.cssSelector(".site-rows b");
	private static By editBtnLinkTextLocator 		  	= By.linkText("Edit");
	private static By deleteBtnLinkTextLocator 		  	= By.linkText("Delete");
	private static By pageTitleNameLocator 			  	= By.name("name");
	private static By navigationLabelNameLocator 	  	= By.name("navigationLabel");
	private static By permalinkNameLocator 	 		  	= By.name("permalink");
	private static By saveBtnIdLocator 		  			= By.id("savepage");
	private static By createPageBtnIdLoator				= By.id("create-page");
	
	private static String pageContentFrame		= "//*[@id='cke_3_contents']/iframe";
	private static String getPageContentFrame	= "//*[@id='cke_4_contents']/iframe";
	private static String pageContent			= "html/body";
	private static String navPagesRow			= "wiz-row";
	private static String navPageTitle			= "title";
	private static String advancedView			= "/html/body/div[3]/div/a";
	private static String newNavigPageEditXpath = "/html/body/div[4]/div[1]/div[3]/div/div[2]/div/div[3]/div/div[3][last()]/a[1]";

	public NavigationPages(WebDriver driver) {
		this.driver = driver;
	}
	
	public NavigationPage clickCreatePage() {
		DriverUtils.click(createPageBtnIdLoator, driver);
		return new NavigationPage(driver);
	}
	
	public NavigationPage clickEdit() {
	    WaitTool.waitForElement(driver, editBtnLinkTextLocator, WAIT_4_ELEMENT).click();
		return new NavigationPage(driver);
	}
	
	public NavigationPage clickDelete() {
		WaitTool.waitForElement(driver, deleteBtnLinkTextLocator, WAIT_4_ELEMENT).click();
		return new NavigationPage(driver);
	}
	
	public void setPageTitle(String pageTitleValue) {
		List<WebElement> names = WaitTool.waitForListElementsPresent(driver, pageTitleNameLocator, WAIT_4_ELEMENT);
		names.get(1).sendKeys(pageTitleValue);
	}
	
	public void setNavigationLabel(String navigationLabelValue) {
		WaitTool.waitForElement(driver, navigationLabelNameLocator, WAIT_4_ELEMENT).sendKeys(navigationLabelValue);
	}
	
	public void setPagePermalink(String permaLinkValue) {
		WaitTool.waitForElement(driver, permalinkNameLocator, WAIT_4_ELEMENT).clear();
		WaitTool.waitForElement(driver, permalinkNameLocator, WAIT_4_ELEMENT).sendKeys(permaLinkValue);
	}
	
	public void setPageContent(String pageContentValue) {
		driver.switchTo().frame(driver.findElement(By.xpath(pageContentFrame)));
		driver.findElement(By.xpath(pageContent)).sendKeys(pageContentValue);
		driver.switchTo().defaultContent();
	}
	
	public String getPageTitle() {
		List<WebElement> names = WaitTool.waitForListElementsPresent(driver, pageTitleNameLocator, WAIT_4_ELEMENT);
		return names.get(2).getAttribute("value");	
	}
	
	public String getNavigationLabel() {
		List<WebElement> labels = WaitTool.waitForListElementsPresent(driver, navigationLabelNameLocator, WAIT_4_ELEMENT);
		return labels.get(1).getAttribute("value");
	}
	
	public String getPagePermalink() {
		List<WebElement> links = WaitTool.waitForListElementsPresent(driver, permalinkNameLocator, WAIT_4_ELEMENT);
		return links.get(1).getAttribute("value");
	}
	
	public String getPageContent() {
		driver.switchTo().frame(driver.findElement(By.xpath(getPageContentFrame)));
		String pageContentVal = driver.findElement(By.xpath(pageContent)).getText();
		driver.switchTo().defaultContent();
		return pageContentVal;
	}
	
	public void clickNavigation(){
		WaitTool.waitForElement(driver, By.linkText("Navigation"), WAIT_4_ELEMENT).click();
	}
	
	public void clickNavPageEdit(String navPageName) {
		List<WebElement> rows = WaitTool.waitForListElementsPresent(driver, By.className(navPagesRow), WAIT_4_ELEMENT);
		for(WebElement e: rows) {
			WebElement navPageTitleElement = e.findElement(By.className(navPageTitle));
			Boolean isjQueryReady = WaitTool.waitForJQueryProcessing(driver, WaitTool.DEFAULT_WAIT_4_PAGE);
			if(isjQueryReady){
				if(navPageTitleElement.getText().equals(navPageName)){
					e.findElement(editBtnLinkTextLocator).click();
				}
				}
			}
		utils.WaitTool.waitForJQueryProcessing(driver, 5);
		}
		
	public EventEdit switchToAdvancedView() {
		DriverUtils.click(By.xpath(advancedView), driver);
		return new EventEdit(driver);
	}
	public void clickSaveButton() {
		DriverUtils.click(saveBtnIdLocator, driver);
	}
	
	public void clickEditNewNavigationPage(){
		DriverUtils.click(By.xpath(newNavigPageEditXpath), driver);
	}
	
	public void checkNavigationPage(String pageTitleValue, String navigationTitleValue, String permaLinkValue, String pageContentValue) {
		Assert.assertTrue(getPageTitle().equals(pageTitleValue), "Wrong page title");
		Assert.assertTrue(getNavigationLabel().equals(navigationTitleValue), "Wrong navigation label value");
		Assert.assertTrue(getPagePermalink().equals(permaLinkValue), "Wrong permalink value");
	//	Assert.assertTrue(getPageContent().equals(pageContentValue), "Wrong page content value");
	//TO DO 
	//Need to research why getText return empty result
	}
	
	public int getNavigationPagesCount(){
		List<WebElement> navigationPagesLabels = WaitTool.waitForListElementsPresent(driver, 
				navigationPagesLabelsCssLocator, WAIT_4_ELEMENT);
		return navigationPagesLabels.size();
	}
}
