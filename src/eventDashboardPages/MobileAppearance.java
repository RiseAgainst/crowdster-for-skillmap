package eventDashboardPages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import utils.WaitTool;
import utils.WindowHandler;

public class MobileAppearance {
	private WebDriver 		driver;
	private WindowHandler 	handler;
	private static final int WAIT_4_ELEMENT				= WaitTool.DEFAULT_WAIT_4_ELEMENT;
	private static By saveBtnIdLocator 					= By.id("saveBtn");
	private static By viewSiteIdLocator 				= By.id("viewsitehrefId");
	private static By headerIdLocator 					= By.id("mobileMastheadCode");
	private static By contentIdLocator 					= By.id("content");
	private static By menuIconColorIdLocator			= By.id("mobileNavBgColor");
	private static By mobileSiteEventHeadarCssLocator 	= By.cssSelector(".mobile-header-bar");
	private static By minicolortsSwatchElementCssLocator = By.cssSelector(".minicolors-swatch");
	private static By minicolorsSwatchCssLocator 		= By.cssSelector("span");
	private static By menuContainerCssLocator 			= By.cssSelector(".menu-container");
	private static By mbSiteContainerCssLocator 		= By.cssSelector(".mobile-site-container");
	
	private String header 					= "Header Test";
	private String homepageMainContent 		= "Content Test ala bdfsgdsfdsfsd";
	private String menuIconColor 			= "#28e066";
			
	public MobileAppearance(WebDriver driver) {
		this.driver = driver;
		this.handler = new WindowHandler();
	}
	
	private void clickSaveBtn(){
		WaitTool.waitForElement(driver, saveBtnIdLocator, WAIT_4_ELEMENT).click();
	}
	
	private void switchToMobileEventTab(){
		handler.getOldWindow(driver);
		WaitTool.waitForElement(driver, viewSiteIdLocator, WAIT_4_ELEMENT).click();
		handler.switchToNewWindow(driver); 			    
	}
	
	public void switchToMainWindow(){
		this.handler.switchToOldWindow(driver);
	}
	
	public void setMobileHeader(){
		WebElement headerElement = WaitTool.waitForElement(driver, headerIdLocator, WAIT_4_ELEMENT);
		headerElement.clear();
		headerElement.sendKeys(header);
		this.clickSaveBtn();
	}
	
	public String getMobileSiteEventHeader(){
		this.switchToMobileEventTab();
		String mobileSiteEventHeader = WaitTool.waitForElement(driver, mobileSiteEventHeadarCssLocator, WAIT_4_ELEMENT).getText();
		return mobileSiteEventHeader;
	}
	
	public void setMobileSiteEventHomepageMainContent(){
		WebElement homepageMainContentElement = WaitTool.waitForElement(driver, contentIdLocator, WAIT_4_ELEMENT);
		homepageMainContentElement.clear();
		homepageMainContentElement.sendKeys(homepageMainContent);
		this.clickSaveBtn();
	}
	
	public String getMobileSiteEventHomepageMainContent(){
		this.switchToMobileEventTab();
		WebElement mobileSiteEventHomepageMainContentElement = WaitTool.waitForListElementsPresent(driver, mbSiteContainerCssLocator, WAIT_4_ELEMENT).get(1);
		String mobileSiteEventHomepageMainContentText = mobileSiteEventHomepageMainContentElement.getText();
		String mobileSiteEventHomepageMainContent = mobileSiteEventHomepageMainContentText.substring(0, mobileSiteEventHomepageMainContentText.indexOf("Registration") -2);
		return mobileSiteEventHomepageMainContent;
	}
	
	public String setMobileSiteEventMenuIconColor(){
		WebElement menuIconColorElement = WaitTool.waitForElement(driver, menuIconColorIdLocator, WAIT_4_ELEMENT);
		menuIconColorElement.clear();
		menuIconColorElement.sendKeys(menuIconColor);
		this.clickSaveBtn();
		WebElement minicolorsSwatchElement = WaitTool.waitForElement(driver, minicolortsSwatchElementCssLocator, WAIT_4_ELEMENT);
		String minicolorsSwatchStyle = minicolorsSwatchElement.findElement(minicolorsSwatchCssLocator).getAttribute("style");
		return minicolorsSwatchStyle;
	}
	
	public String getMobileSiteEventMenuIconColor(){
		this.switchToMobileEventTab();
		WebElement mobileSiteEventMenuContainerElement = WaitTool.waitForElement(driver, menuContainerCssLocator, WAIT_4_ELEMENT);
		String backgroundColor = mobileSiteEventMenuContainerElement.getAttribute("style");
		return backgroundColor;
	}
	
	//Asserts
	public void checkMobileSiteEventHeader(String mobileSiteEventHeader){
		Assert.assertTrue(header.equals(mobileSiteEventHeader), "Mobile Site Event Header is not correct.");
	}
	
	public void checkMobileSiteHomepageMainContent(String mobileSiteEventHomepageMainContent){
		Assert.assertTrue(homepageMainContent.equals(mobileSiteEventHomepageMainContent), "Mobile Site Event Header Homepage Main Content is not correct.");
	}
	
	public void checkMobileMenuIconColor(String minicolorsSwatchStyle, String backgroundColor){
		Assert.assertTrue(minicolorsSwatchStyle.equals(backgroundColor), "Mobile Menu Icon Color is not correct.");
	}
}
