package eventDashboardPages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import utils.DriverUtils;
import utils.FileUploader;
import utils.WaitTool;
import utils.WebTable;

public class ManageCampaignsConfig {
	private WebDriver				driver;
	private FileUploader			fileUploader;
	private static final int 		WAIT_4_ELEMENT		= WaitTool.DEFAULT_WAIT_4_ELEMENT;
	
	private static String statusUpdateValue				= "TEST Status update text";
	private static String theStoryValue					= "TEST The story text";
	private static String defaultTeamStoryValue			= "TEST Default team story text";
	private static String teamImageFileName				= "grandmaster.jpg";
	private static String teamCampaignImageFileName		= "randomLogo.png";
	private static String financialGoalValue			= "1200";
	private static String memberNameValue				= "testmember";
	private static String memberEmailValue				= "testmember@test.com";
	private static String memberPasswordValue			= "123456";
	private static String teamName						= "Test Team";
	private static String offlineDonationName			= "TEST offline donation";
	private static String offlineDonationValue			= "35";
	
	private static By bodyElemenetTagLocator			= By.tagName("body");
	private static By imgElemenetTagLocator				= By.tagName("img");
	private static By toggleCssLocator					= By.cssSelector("a.toggle");
	private static By uploadTeamImageIdLocator			= By.id("file2-addToLib");
	private static By uploadCampaignTeamImageIdLocator 	= By.id("file3-addToLib");
	private static By fileInputNameLocator				= By.name("file");
	private static By fileSubmitBtnIdLocator			= By.id("uploadmediaSubmit");
	private static By financialGoalIdLocator			= By.id("financialGoal");
	private static By saveBtnIdLocator					= By.id("editCampaigBtn");
	private static By successMsgCssLocator				= By.cssSelector("div.message-popup");
	private static By eventJoinTeamBtnIdLocator			= By.id("join-team-btn");
	private static By eventStartTeamBtnIdLocator		= By.id("start-team-btn-Id");
	private static By memberFirstNameIdLocator			= By.id("firstname");
	private static By memberLastNameIdLocator			= By.id("lastname");
	private static By memberEmailIdLocator				= By.id("email");
	private static By memberPasswordIdLocator			= By.id("password");
	private static By memberConfirmPasswordIdLocator	= By.id("confirmpassword");
	private static By teamInformationIdLocator			= By.id("media-select");
	private static By termsIdLocator					= By.id("terms");
	private static By submitBtnIdLocator				= By.id("join_team_submit");
	private static By viewMyPageBtnLinkTextLocator		= By.linkText("View My Page");
	private static By statusUpdateContainerCssLocator	= By.cssSelector("div.quote");
	private static By storyContainerCssLocator			= By.cssSelector("div.story-container");
	private static By eventSearchProjectInputIdLocator	= By.id("searchInput-proj");
	private static By eventTestmemberCssLocator			= By.cssSelector("#page-list-tiles > li");
	private static By thermoFundraisingGoalIdLocator	= By.id("thermo-fundraising-goal");
	private static By eventLogInLinkTextLocator			= By.linkText("Log In");
	private static By eventHomeLinkTextLocator			= By.linkText(" Home ");
	private static By logInBtnNameLocator				= By.name("log-in-btn");
	private static By eventCreateTeamImageCssLocator	= By.cssSelector("div.show-blob");
	private static By mhpOfflineDonationsIdLocator 		= By.id("mhp_offline_donation");
	private static By mhpAddOfflineDonationsLinkText	= By.linkText("Add Offline Donations");
	private static By mhpOfflineDonNameNameLocator		= By.name("offlineName");
	private static By mhpOfflineDonValueNameLocator		= By.name("newValue");
	private static By mhpAddDonorXpathLocator			= By.xpath("html/body/div[2]/div[2]/div[1]/div/div/div[1]/div[2]/form/input[9]");
	private static By mhpOfflineDonationsTableCssLocator	= By.cssSelector("table.table-items");
	
	public ManageCampaignsConfig(WebDriver driver){
		this.driver = driver;
		this.fileUploader = new FileUploader(driver);
	}
	
	public void clickSaveBtn(){
		DriverUtils.click(saveBtnIdLocator, driver);
	}
	
	public void clickJoinATeamBtn(){
		DriverUtils.click(eventJoinTeamBtnIdLocator, driver);
	}
	
	public void clickStartATeamBtn(){
		DriverUtils.click(eventStartTeamBtnIdLocator, driver);
	}
	
	public void clickViewMyPageBtn(){
		WaitTool.waitForElement(driver, viewMyPageBtnLinkTextLocator, WAIT_4_ELEMENT).click();
	}
	
	public void clickHome(){
		WaitTool.waitForElement(driver, eventHomeLinkTextLocator, WAIT_4_ELEMENT).click();
		WaitTool.waitForJQueryProcessing(driver, 5);
	}
	
	public void clickMHPOfflineDonations(){
		DriverUtils.click(mhpOfflineDonationsIdLocator, driver);
	}
	
	public void clickAddOfflineDonations(){
		WaitTool.waitForElement(driver, mhpAddOfflineDonationsLinkText, WAIT_4_ELEMENT).click();
	}
	
	public void clickAddDonorBtn(){
		DriverUtils.click(mhpAddDonorXpathLocator, driver);
	}
	
	public void goToTestmemberPage(){
		this.searchTestmember();
		this.clickTestmember();
	}
	
	private void searchTestmember(){
		WaitTool.waitForElement(driver, eventSearchProjectInputIdLocator, WAIT_4_ELEMENT).sendKeys(memberNameValue);
	}
	
	private void clickTestmember(){
		DriverUtils.click(eventTestmemberCssLocator, driver);
		WaitTool.waitForJQueryProcessing(driver, 5);
	}
	
	public void setMemberInformation(){
		WaitTool.waitForElement(driver, memberFirstNameIdLocator, WAIT_4_ELEMENT).sendKeys(memberNameValue);
		WaitTool.waitForElement(driver, memberLastNameIdLocator, WAIT_4_ELEMENT).sendKeys(memberNameValue);
		WaitTool.waitForElement(driver, memberEmailIdLocator, WAIT_4_ELEMENT).sendKeys(memberEmailValue);
		WaitTool.waitForElement(driver, memberPasswordIdLocator, WAIT_4_ELEMENT).sendKeys(memberPasswordValue);
		WaitTool.waitForElement(driver, memberConfirmPasswordIdLocator, WAIT_4_ELEMENT).sendKeys(memberPasswordValue);
		DriverUtils.selectDropDownOptionByVisibleText(driver, teamInformationIdLocator, teamName);
		DriverUtils.click(termsIdLocator, driver);
		DriverUtils.click(submitBtnIdLocator, driver);
	}
	
	public void fillStatusUpdate(){
		WebElement bodyElement = WaitTool.waitForElement(driver, bodyElemenetTagLocator, WAIT_4_ELEMENT);
		bodyElement.clear();
		bodyElement.sendKeys(statusUpdateValue);
	}
	
	public void fillTheStory(){
		WebElement bodyElement = WaitTool.waitForElement(driver, bodyElemenetTagLocator, WAIT_4_ELEMENT);
		bodyElement.clear();
		bodyElement.sendKeys(theStoryValue);
	}
	
	public void fillDefaultTeamStory(){
		WebElement bodyElement = WaitTool.waitForElement(driver, bodyElemenetTagLocator, WAIT_4_ELEMENT);
		bodyElement.clear();
		bodyElement.sendKeys(defaultTeamStoryValue);
	}
	
	public void clickToggleButtons(){
		List<WebElement> toggleElements = WaitTool.waitForListElementsPresent(driver, toggleCssLocator, WAIT_4_ELEMENT);
		for (WebElement webElement : toggleElements) {
			String elementClassAttribute = webElement.getAttribute("class");
			if(!elementClassAttribute.contains("checked")){
				DriverUtils.clickElement(webElement, driver);
			}
		}
	}
	
	public void setTeamImage(){
		DriverUtils.click(uploadTeamImageIdLocator, driver);
		this.fileUploader.uploadFile(teamImageFileName, fileInputNameLocator);
		DriverUtils.click(fileSubmitBtnIdLocator, driver);
	}
	
	public void setTeamCampaignImage(){
		DriverUtils.click(uploadCampaignTeamImageIdLocator, driver);
		this.fileUploader.uploadFile(teamCampaignImageFileName, fileInputNameLocator);
		DriverUtils.click(fileSubmitBtnIdLocator, driver);
	}
	
	public void setFinancialGoal(){
		WebElement financialGoalElement = WaitTool.waitForElement(driver, financialGoalIdLocator, WAIT_4_ELEMENT);
		financialGoalElement.clear();
		financialGoalElement.sendKeys(financialGoalValue);
	}
	
	public String getFinancialGoal(){
		String financialGoal = WaitTool.waitForElement(driver, financialGoalIdLocator, WAIT_4_ELEMENT).getAttribute("value");
		financialGoal = financialGoal.substring(0, financialGoal.indexOf(".")).replace(",", "");
		return financialGoal;
	}
	
	private String getEventFinancialGoal(){
		String financialGoal = WaitTool.waitForElement(driver, thermoFundraisingGoalIdLocator, WAIT_4_ELEMENT).getText();
		financialGoal = financialGoal.substring(financialGoal.indexOf("$") + 1, financialGoal.indexOf("G") - 1);
		financialGoal = financialGoal.replace(",", "");
		return financialGoal;
	}
	
	public void eventLogInIfNecessary(){
		WebElement logInElement = WaitTool.waitForElement(driver, eventLogInLinkTextLocator, WAIT_4_ELEMENT);
		if(logInElement != null){
			logInElement.click();
			WaitTool.waitForElement(driver, memberEmailIdLocator, WAIT_4_ELEMENT).sendKeys(memberEmailValue);
			WaitTool.waitForElement(driver, memberPasswordIdLocator, WAIT_4_ELEMENT).sendKeys(memberPasswordValue);
			DriverUtils.click(logInBtnNameLocator, driver);
			WaitTool.waitForJQueryProcessing(driver, 5);
		}
	}
	
	public void fillOfflineDonation(){
		WaitTool.waitForElement(driver, mhpOfflineDonNameNameLocator, WAIT_4_ELEMENT).sendKeys(offlineDonationName);
		WaitTool.waitForElement(driver, mhpOfflineDonValueNameLocator, WAIT_4_ELEMENT).sendKeys(offlineDonationValue);
	}
	
	private String getEventCreateATeamDesription(){
		String descritpionText = WaitTool.waitForElement(driver, bodyElemenetTagLocator, WAIT_4_ELEMENT).getText();
		return descritpionText;
	}
	
	private String getImageSrc(int imgIndex){
		List<WebElement> teamImageContainerElement = WaitTool.waitForListElementsPresent(driver, eventCreateTeamImageCssLocator, WAIT_4_ELEMENT);
		return teamImageContainerElement.get(imgIndex).findElement(imgElemenetTagLocator).getAttribute("src");
	}
	
	public int getOfflineDonationsCount(){
		WebElement tableElement = WaitTool.waitForElement(driver, mhpOfflineDonationsTableCssLocator, WAIT_4_ELEMENT);
		if(tableElement == null){
			return 0;
		}
		List<WebElement> tableItems = WebTable.getElementsFromColumnByTableElement(tableElement, 1, driver);
		return tableItems.size();
	}
	
	// Asserts
	
	public void checkSuccessMessage(){
		WebElement successMsgElement = WaitTool.waitForElement(driver, successMsgCssLocator, WAIT_4_ELEMENT);
		Assert.assertTrue(successMsgElement.isDisplayed(), "Success message doesn't dispay.");
	}
	
	public void checkStatusUpdate(){
		String statusUpdateText = WaitTool.waitForElement(driver, statusUpdateContainerCssLocator, WAIT_4_ELEMENT).getText();
		Assert.assertTrue(statusUpdateText.equals(statusUpdateValue), "Wrong Status update text.");
	}
	
	public void checkTheStory(){
		String theStoryText = WaitTool.waitForElement(driver, storyContainerCssLocator, WAIT_4_ELEMENT).getText();
		Assert.assertTrue(theStoryText.equals(theStoryValue), "Wrong Story text.");
	}
	
	public void checkFinancialGoal(String financialGoal){
		Assert.assertTrue(getEventFinancialGoal().equals(financialGoal), "Wrong financial goal.");
	}
	
	public void checkCreateATeamDescritpion(){
		Assert.assertTrue(getEventCreateATeamDesription().equals(defaultTeamStoryValue), "Wrong create a team description.");
	}
	
	public void checkTeamImage(){
		String teamImageSrc = getImageSrc(0);
		teamImageFileName = teamImageFileName.substring(0, teamImageFileName.indexOf("."));
		Assert.assertTrue(teamImageSrc.contains(teamImageFileName), "Team image contains wrong image.");
	}
	
	public void checkTeamCampaignImage(){
		String teamCampaignImageSrc = getImageSrc(1);
		teamCampaignImageFileName = teamCampaignImageFileName.substring(0, teamCampaignImageFileName.indexOf("."));
		Assert.assertTrue(teamCampaignImageSrc.contains(teamCampaignImageFileName), "Team campaign image contains wrong image.");
	}
	
	public void checkOfflineDonationsAfterCreate(int offlineDonationsCountBefore, int offlineDonationsCountAfter){
		Assert.assertTrue(offlineDonationsCountAfter == offlineDonationsCountBefore + 1, "Wrong offline donation count after create.");
	}
}
