package eventDashboardPages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import utils.DriverUtils;
import utils.WaitTool;
import utils.WebTable;
import utils.WindowHandler;

public class SettingsOfflineDonation {
	
	private WebDriver			driver;
	private WindowHandler 		handler;
	private WebTable 			webTable;
	private WebDriverWait 		wait;
	private static final int WAIT_4_ELEMENT			= WaitTool.DEFAULT_WAIT_4_ELEMENT;
	private static By addOfflineDonBtnLocator		= By.name("add-offline-donation");
	private static By nameNameLocator				= By.name("offlineName");
	private static By amountIdLocator				= By.id("donationAmount");
	private static By noteNameLocator				= By.name("note");
	private static By saveBtnIdLocator				= By.id("editOffLineDonationBtn");
	private static By editBtnLinkLocator			= By.linkText("Edit");
	private static By viewSiteIdLocator				= By.id("viewsitehrefId");
	private static By thermoTotalRaisedIdLocator	= By.id("thermo-total-raised");
	private static By searchInputIdLocator			= By.id("searchInput-proj");
	private static By dropDownIdLocator				= By.id("campaignId");
	private static By totalDonationIdLocator		= By.id("total-donations");
	private static By searchLocator					= By.id("project-search");
	private static By teamsIdLocator				= By.id("teamsId");
	private static By individualsIdLocator			= By.id("individualsId");
	private static By tableRowsCssLocator 			= By.cssSelector(".donation-items-table");
	private static By deleteLinkTextLocator 		= By.linkText("Delete");
	
	private String name								= "Donation test name";
	private String amount							= "25";
	private String amountEdit						= "35";
	private String note								= "This is test note.";
	
	public SettingsOfflineDonation(WebDriver driver){
		this.driver = driver;
		this.handler = new WindowHandler();
		this.wait = new WebDriverWait(driver, WAIT_4_ELEMENT);
	}
	
	public void clickAddOfflineDonationsBtn(){
		DriverUtils.click(addOfflineDonBtnLocator, driver);
	}
	
	public void clickSaveBtn(){
		DriverUtils.click(saveBtnIdLocator, driver);
	}
	
	public void clickEditBtn(){
		DriverUtils.click(editBtnLinkLocator, driver);
	}
	
	public void switchToEvenet(){
		handler.getOldWindow(driver);
		DriverUtils.click(viewSiteIdLocator, driver);
		handler.switchToNewWindow(driver);
	}
	
	public void switchToOldWindow(){
		handler.switchToOldWindow(driver);
	}
	
	public int getDonationsAmount(){
		this.switchToEvenet();
		String donationsAmount = WaitTool.waitForElement(driver,thermoTotalRaisedIdLocator , WAIT_4_ELEMENT).getText();
		donationsAmount = donationsAmount.substring(1, donationsAmount.indexOf('.'));
		this.switchToOldWindow();
		return Integer.parseInt(donationsAmount);
	}
	
	public int createOfflineDonation(){
		WaitTool.waitForElement(driver, nameNameLocator, WAIT_4_ELEMENT).sendKeys(name);
		WaitTool.waitForElement(driver, amountIdLocator, WAIT_4_ELEMENT).sendKeys(amount);
		WaitTool.waitForElement(driver, noteNameLocator, WAIT_4_ELEMENT).sendKeys(note);
		this.clickSaveBtn();
		return Integer.parseInt(amount);
	}
	
	public int createOfflineDonationAssignToTeamOwner(){
		DriverUtils.selectDropDownOptionByVisibleText(driver, dropDownIdLocator, "Team Owner");
		return this.createOfflineDonation();
	}
	
	public int createOfflineDonationAssignToTeamUser(){
		DriverUtils.selectDropDownOptionByVisibleText(driver, dropDownIdLocator, "User Second");
		return this.createOfflineDonation();
	}
	
	public int createOfflineDonationAssignToUser(){
		DriverUtils.selectDropDownOptionByVisibleText(driver, dropDownIdLocator, "Test Third");
		return this.createOfflineDonation();
	}
	
	public void createOfflineDonationWithEmptyData(){
		this.clickSaveBtn();
		wait.until(ExpectedConditions.alertIsPresent());
		driver.switchTo().alert().accept();
		WaitTool.waitForJQueryProcessing(driver, 5);
	}
	
	public int editOfflineDonation(){
		WebElement donationAmountInputElement = WaitTool.waitForElement(driver, amountIdLocator, WAIT_4_ELEMENT);
		donationAmountInputElement.clear();
		donationAmountInputElement.sendKeys(amountEdit);
		this.clickSaveBtn();
		return Integer.parseInt(amountEdit);
	}
	
	public int getTableRowsCount(){
		WebElement table = WaitTool.waitForElement(driver, tableRowsCssLocator, WAIT_4_ELEMENT);
		webTable = new WebTable(table, driver);
		return webTable.getRowCount();
	}
	
	public void deleteOfflineDonation(){
		DriverUtils.click(deleteLinkTextLocator, driver);
		wait.until(ExpectedConditions.alertIsPresent());
		driver.switchTo().alert().accept();
		WaitTool.waitForJQueryProcessing(driver, 5);
	}
	
	public int getTestTeamRaisedDonation(){
		DriverUtils.click(teamsIdLocator, driver);
		String raisedDonationText = WaitTool.waitForElement(driver, totalDonationIdLocator, WAIT_4_ELEMENT).getText();
		raisedDonationText = raisedDonationText.substring(2);
		return Integer.parseInt(raisedDonationText);
	}
	
	public int getTeamOwnerRaisedDonation(){
		WaitTool.waitForElement(driver, searchInputIdLocator, WAIT_4_ELEMENT).sendKeys("Team Owner");
		DriverUtils.click(searchLocator, driver);
		DriverUtils.click(individualsIdLocator, driver);
		String raisedDonationText = WaitTool.waitForElement(driver, totalDonationIdLocator, WAIT_4_ELEMENT).getText();
		raisedDonationText = raisedDonationText.substring(2);
		return Integer.parseInt(raisedDonationText);
	}
	
	public int getTeamUserRaisedDonation(){
		WaitTool.waitForElement(driver, searchInputIdLocator, WAIT_4_ELEMENT).sendKeys("Second");
		DriverUtils.click(searchLocator, driver);
		String raisedDonationText = WaitTool.waitForElement(driver, totalDonationIdLocator, WAIT_4_ELEMENT).getText();
		raisedDonationText = raisedDonationText.substring(2);
		return Integer.parseInt(raisedDonationText);
	}
	
	public int getUserRaisedDonation(){
		WaitTool.waitForElement(driver, searchInputIdLocator, WAIT_4_ELEMENT).sendKeys("Third");
		DriverUtils.click(searchLocator, driver);
		String raisedDonationText = WaitTool.waitForElement(driver, totalDonationIdLocator, WAIT_4_ELEMENT).getText();
		raisedDonationText = raisedDonationText.substring(2);
		return Integer.parseInt(raisedDonationText);
	}
	
	//Asserts
	public void checkDonationTotalAmount(int donationTotalAmountBefore, int donationValue, int donationTotalAmountAfter) {
		Assert.assertTrue((donationTotalAmountBefore +  donationValue) == donationTotalAmountAfter, "Donation total amount is not correct.");
	}
	
	public void checkDonationTotalAmountAfterEdit(int donationTotalAmountBefore, int donationValue,  int donationTotalAmountAfter) {
		int newDonationTotalAmount = donationTotalAmountBefore - Integer.parseInt(amount);
		newDonationTotalAmount += donationValue;
		Assert.assertTrue(newDonationTotalAmount == donationTotalAmountAfter, "Donation total amount is not correct.");
	}
	
	public void checkIsDeleteOfflineDonation(int tableRowsCountBefore, int tableRowsCountAfter){
		Assert.assertTrue(tableRowsCountBefore > tableRowsCountAfter, "Offline donation is not deleted.");
	}
	
	public void checkOfflineDonationsCount(int tableRowsCountBefore, int tableRowsCountAfter){
		Assert.assertTrue(tableRowsCountBefore == tableRowsCountAfter, "Offline donations count is not the same.");
	}
	
	public void checkTeamRaisedDonation(int teamRaisedDonationBefore, int donationValue, int teamRaisedDonationAfter){
		Assert.assertTrue((teamRaisedDonationBefore + donationValue) == teamRaisedDonationAfter, "Team raised donation is not correct.");
	}
	
	public void checkOwnerRaisedDonation(int ownerRaisedDonationBefore, int donationValue, int ownerRaisedDonationAfter){
		Assert.assertTrue((ownerRaisedDonationBefore + donationValue) == ownerRaisedDonationAfter, "Owner raised donation is not correct.");
	}
	
	public void checkUserRaisedDonation(int userRaisedDonationBefore, int donationValue, int userRaisedDonationAfter){
		Assert.assertTrue((userRaisedDonationBefore + donationValue) == userRaisedDonationAfter, "User raised donation is not correct.");
	}
	
	public void checkTeamUserRaisedDonation(int teamUserRaisedDonationBefore, int donationValue, int teamUserRaisedDonationAfter){
		Assert.assertTrue((teamUserRaisedDonationBefore + donationValue) == teamUserRaisedDonationAfter, "Team user raised donation is not correct.");
	}

}
