package eventDashboardPages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import utils.WaitTool;

public class WebsiteHomepageLayout {
	private WebDriver				driver;
	private static final int 		WAIT_4_ELEMENT		= WaitTool.DEFAULT_WAIT_4_ELEMENT;
	private static final String 	imagePath			= "\\files\\testLogo.png";
	
	private static By createPageMapNameLocator			= By.name("getYourOwnPageAdd");
	private static By areaCssLoctor						= By.cssSelector("area");
	private static By eventCreatePageWidgetIdLocator	= By.id("createpage-btn");
	private static By fundraisingIdLocator 				= By.id("fundraising-1");
	private static By mapAndDirectionsIdLocator			= By.id("map-3");
	private static By campaignsMainIdLocator			= By.id("home-page-campaigns-main-widget");
	private static By eventCampaignsMainIdLocator		= By.id("main-campaigns-containerId");
	private static By eventNameInputIdLocator			= By.id("event-name");
	private static By eventNameNameLocator				= By.name("name");
	private static By saveBtnIdLocator					= By.id("saveBtn");
	private static By saveBtnXpathLocator				= By.xpath("/html/body/div[6]/form/p/input[1]");
	private static By saveBtnFreeFormXpathLocator		= By.xpath("/html/body/div[6]/form/p[2]/input[1]");
	private static By eventNameCssLocator				= By.cssSelector("h1");
	private static By layoutEventDateIdLocator			= By.id("event-date");
	private static By eventDateTimeIdLocator 			= By.id("event_datetime");
	private static By amPmNameLocator 					= By.name("amPm");
	private static By minutesIdLocator					= By.id("minutes");
	private static By hoursIdLocator 					= By.id("hours");
	private static By addTimeLinkTextLocator			= By.linkText("Add Time");
	private static By removeTimeLinkTextLocator			= By.linkText("Remove Time");
	private static By dayLinkTextLocator 				= By.linkText("10");
	private static By nextLinkTextLocator				= By.linkText("Next");
	private static By siteConfigFormIdLocator 			= By.id("siteConfigForm");
	private static By freeFormHtmlIdLocator				= By.id("free-form-html");
	private static By freeFormHtml2IdLocator			= By.id("free-form-html2");
	private static By iframeCssLocator					= By.cssSelector("iframe");
	private static By bodyCssLocator 					= By.cssSelector("body");
	private static By widgetEditIconCssLocator 			= By.cssSelector(".widget-edit-icon");
	private static By customizeBanerIdLocator			= By.id("upload-banner-lb");
	private static By heroContainerCssLocator 			= By.cssSelector(".hero-container");
	private static By descriptionCssLocator 			= By.cssSelector(".body");
	private static By banerTextIdLocator 				= By.id("banner-masthead-code");
	private static By mediaElementIdLocator 			= By.id("mediaId1");
	private static By mainHeaderCssLocator			    = By.cssSelector(".main-header");
	private static By imgElementCssLocator 				= By.cssSelector("img");
	private static By uploadTableCssLocator				= By.cssSelector(".media-upload-table");
	private static By uploadFileSubmitBtnIdLocator 		= By.id("uploadmediaSubmit");
	private static By browseFileElementNameLocator 		= By.name("file");
	private static By uploadNewFileIdLocator 			= By.id("upload-to-media");
	private static By uploadAFileIdLocator 				= By.id("file");
	private static By uploadPhotosBtnIdLocator 			= By.id("upload-photos");
	private static By inputElementCssLocator 			= By.cssSelector("input");
	private static By descriptionIdLocator 				= By.id("home-page-description");
	private static By donationBtnElementIdLocator 		= By.id("donation-text-button");
	private static By donationBtnTextInputNameLocator 	= By.name("donationText");
	private static By donationBtnIdLocator 				= By.id("donation-btn-button");
	private static By siteEventDateIdLocator			= By.id("siteEventDate");
	
	private static String donationBtnText 				= "Donation test";
	private String newEventName							= "SecondOne Event new name test";	
	private String hours								= "8";
	private String minutes								= "20";
	private String textEditorContent					= "Text editor some text test.";
	
	public WebsiteHomepageLayout(WebDriver driver){
		this.driver = driver;
	}
	
	public void clickSaveButton(){
		WaitTool.waitForElement(driver, saveBtnIdLocator, WAIT_4_ELEMENT).click();
	}
	
	public void clickSaveBtnDescription(){
		WaitTool.waitForElement(driver, saveBtnXpathLocator, WAIT_4_ELEMENT).click();
	}
	
	public By getEventCreatePageWidgetIdLocator() {
		return eventCreatePageWidgetIdLocator;
	}
	
	public By getEventFundraisingIdLocator(){
		return fundraisingIdLocator;
	}
	
	public By getMapAndDirectionsIdLocator(){
		return mapAndDirectionsIdLocator;
	}
	
	public By getCampaignsIdLocator(){
		return eventCampaignsMainIdLocator;
	}
	
	public By getIframCssLocator(){
		return iframeCssLocator;
	}
	
	public void clickEventNameInput(){
		WaitTool.waitForElement(driver, eventNameInputIdLocator, WAIT_4_ELEMENT).click();
	}
	
	public void clickEventDateInput(){
		WaitTool.waitForElement(driver, layoutEventDateIdLocator, WAIT_4_ELEMENT).click();
	}
	
	public void clickCustomizeYourBaner(){
		WaitTool.waitForElement(driver, customizeBanerIdLocator, WAIT_4_ELEMENT).click();
		WaitTool.waitForJQueryProcessing(driver, 5);
	}
	
	public void clickEditEventDescritpion(){
		WebElement descriptionElemenet = WaitTool.waitForElement(driver, descriptionIdLocator, WAIT_4_ELEMENT);
		descriptionElemenet.findElement(inputElementCssLocator).click();
	}
	
	public void clickUploadPhotos(){
		WaitTool.waitForElement(driver, uploadPhotosBtnIdLocator, WAIT_4_ELEMENT).click();
	}
	
	public void clickUploadAFile(){
		WaitTool.waitForElement(driver, uploadAFileIdLocator, WAIT_4_ELEMENT).click();
	}
	
	public void clickUploadNewFile(){
		WaitTool.waitForElement(driver, uploadNewFileIdLocator, WAIT_4_ELEMENT).click();
	}
	
	public String changeEventDateTime(){
		WebElement formElement = WaitTool.waitForElement(driver, siteConfigFormIdLocator, WAIT_4_ELEMENT);
		formElement.findElement(imgElementCssLocator).click();
		utils.WaitTool.waitForJQueryProcessing(driver, 5);	
		WaitTool.waitForElement(driver, nextLinkTextLocator, WAIT_4_ELEMENT).click();
		WaitTool.waitForElement(driver, dayLinkTextLocator, WAIT_4_ELEMENT).click();
		this.setTimeOfEvent();
		String eventDateTime = WaitTool.waitForElement(driver, siteEventDateIdLocator, WAIT_4_ELEMENT).getAttribute("value");
		WaitTool.waitForElement(driver, saveBtnXpathLocator, WAIT_4_ELEMENT).click();
		return eventDateTime;
	}
	
	public void setTimeOfEvent(){
		WebElement removeTimeBtnElemenet = WaitTool.waitForElement(driver, removeTimeLinkTextLocator, WAIT_4_ELEMENT);
		if(removeTimeBtnElemenet != null){
			removeTimeBtnElemenet.click();
		}
		WaitTool.waitForElement(driver, addTimeLinkTextLocator, WAIT_4_ELEMENT).click();
		WebElement hoursElemenet = WaitTool.waitForElement(driver, hoursIdLocator, WAIT_4_ELEMENT);
		hoursElemenet.clear();
		hoursElemenet.sendKeys(hours);
		WebElement minutesElemenet = WaitTool.waitForElement(driver, minutesIdLocator, WAIT_4_ELEMENT);
		minutesElemenet.clear();
		minutesElemenet.sendKeys(minutes);
		List<WebElement> radioBtnElements = WaitTool.waitForListElementsPresent(driver, amPmNameLocator, WAIT_4_ELEMENT);
		radioBtnElements.get(1).click();
	}
	
	public void editEventName(){
		WebElement nameInput = WaitTool.waitForElement(driver, eventNameNameLocator, WAIT_4_ELEMENT);
		nameInput.clear();
		nameInput.sendKeys(newEventName);
		WaitTool.waitForElement(driver, saveBtnXpathLocator, WAIT_4_ELEMENT).click();
	}
	
	private void addWidget(By locator){
		WebElement createPageElement = WaitTool.waitForElement(driver, locator, WAIT_4_ELEMENT);
		createPageElement.findElement(areaCssLoctor).click();
		WaitTool.waitForJQueryProcessing(driver, WAIT_4_ELEMENT);
	}
	
	public void addCreatePageWidget(){
		this.addWidget(createPageMapNameLocator);
	}
	
	public void addFundraisingWidget(){
		this.addWidget(fundraisingIdLocator);
	}
	
	public void addMapAndDirectionsWidget(){
		this.addWidget(mapAndDirectionsIdLocator);
	}
	
	public void addCampaignsMainWidget(){
		WebElement campaignsMainElement = WaitTool.waitForElement(driver, campaignsMainIdLocator, WAIT_4_ELEMENT);
		campaignsMainElement.findElements(imgElementCssLocator).get(1).click();;
	}
	
	public void addFreeFormHtmlWidget(){
		WebElement freeFormHtmlElement = WaitTool.waitForElement(driver, freeFormHtmlIdLocator, WAIT_4_ELEMENT);
		freeFormHtmlElement.findElements(imgElementCssLocator).get(1).click();
	}
	
	public void clickEditFreeFormHtml(){
		WebElement freeFormHtmlElement = WaitTool.waitForElement(driver, freeFormHtmlIdLocator, WAIT_4_ELEMENT);
		freeFormHtmlElement.findElement(widgetEditIconCssLocator).click();
	}
	
	public void clickEditFreeFormHtml2(){
		WebElement freeFormHtmlElement = WaitTool.waitForElement(driver, freeFormHtml2IdLocator, WAIT_4_ELEMENT);
		freeFormHtmlElement.findElement(widgetEditIconCssLocator).click();
	}
	
	public void addFreeFormHtml2Widget(){
		WebElement freeFormHtml2Element = WaitTool.waitForElement(driver, freeFormHtml2IdLocator, WAIT_4_ELEMENT);
		freeFormHtml2Element.findElements(imgElementCssLocator).get(1).click();
	}
	
	public void fillTextEditor(){
		WebElement bodyElemenet = WaitTool.waitForElement(driver, bodyCssLocator, WAIT_4_ELEMENT);
		bodyElemenet.clear();
		bodyElemenet.sendKeys(textEditorContent);
	}
	
	public void clickSaveBtn(){
		WaitTool.waitForElement(driver, saveBtnFreeFormXpathLocator, WAIT_4_ELEMENT).click();
	}
	
	public Boolean isDisplayedEventWidget(By locator){
		Boolean isDisplayed = false;
		WebElement eventWidget = WaitTool.waitForElementPresent(driver, locator, WAIT_4_ELEMENT);
		if(eventWidget != null){
			isDisplayed = eventWidget.isDisplayed();
		}	
		return isDisplayed;
	}
	
	private Boolean checkFreeFormDisplayedText(By locator){
		Boolean checkFreeForm = false;
		WebElement freeFormElemenet = WaitTool.waitForElement(driver, locator, WAIT_4_ELEMENT);
		if(freeFormElemenet != null){
			String eventFreeFormText = freeFormElemenet.getText();
			checkFreeForm = textEditorContent.equals(eventFreeFormText);
		}
		return checkFreeForm;
	}
	
	public void uploadImage(){
		WebElement selectFileElement = WaitTool.waitForElement(driver, browseFileElementNameLocator, WAIT_4_ELEMENT);
		String projectPath = System.getProperty("user.dir");
		String filePath = projectPath + imagePath;
		selectFileElement.sendKeys(filePath);
		WaitTool.waitForElement(driver, uploadFileSubmitBtnIdLocator, WAIT_4_ELEMENT).click();
	}
	
	public void selectImage(){
		WebElement mediaTableElement = WaitTool.waitForElement(driver, uploadTableCssLocator, WAIT_4_ELEMENT);
		mediaTableElement.findElement(imgElementCssLocator).click();
	}
	
	public void clickEditDonationBtn(){
		WebElement donationBtnElement = WaitTool.waitForElement(driver, donationBtnElementIdLocator, WAIT_4_ELEMENT);
		donationBtnElement.findElement(imgElementCssLocator).click();
	}
	
	public void editDonationBtnText(){
		WebElement donationTextElement = WaitTool.waitForElement(driver, donationBtnTextInputNameLocator, WAIT_4_ELEMENT);
		donationTextElement.clear();
		donationTextElement.sendKeys(donationBtnText);
		WaitTool.waitForElement(driver, saveBtnXpathLocator, WAIT_4_ELEMENT).click();
	}
	
	private Boolean isImageDisplayed(){
		Boolean isDisplaying = false;
		WebElement mainHeaderElement = WaitTool.waitForElement(driver, mainHeaderCssLocator, WAIT_4_ELEMENT);
		WebElement imageElement = mainHeaderElement.findElement(imgElementCssLocator);
		if(imageElement != null){
			isDisplaying = true;
		}
		return isDisplaying;
	}
	
	private Boolean isPhotoDisplayed(){
		Boolean hasMedia = false;
		WebElement mediaElement = WaitTool.waitForElement(driver, mediaElementIdLocator, WAIT_4_ELEMENT);
		if(mediaElement != null){
			hasMedia = true;
		}
		return hasMedia;
	}
	
	//Asserts
	public void checkIsDisplayedEventWidget(Boolean isDisplayed){
		Assert.assertTrue(isDisplayed, "Widget is not displayed.");
	}
	
	public void checkEventName(){
		Boolean isPresent = WaitTool.waitForTextPresent(driver, eventNameCssLocator, newEventName, WAIT_4_ELEMENT);
		Assert.assertTrue(isPresent, "Event name is not correct.");
	}
	
	public void checkEventDateTime(String eventDateTime){
		String dateTime = WaitTool.waitForElement(driver, eventDateTimeIdLocator, WAIT_4_ELEMENT).getText();
		Assert.assertTrue(dateTime.contains(eventDateTime), "Event Date Time is not correct.");
	}
	
	public void checkFreeFormText(){
		Assert.assertTrue(this.checkFreeFormDisplayedText(freeFormHtmlIdLocator), "Free Form widget is not displayed or text is not correct.");
	}
	
	public void checkFreeForm2Text(){
		Assert.assertTrue(this.checkFreeFormDisplayedText(freeFormHtml2IdLocator), "Free Form2 widget is not displayed or text is not correct.");
	}
	
	public void checkBanerText(){
		Boolean isPresent = WaitTool.waitForTextPresent(driver, banerTextIdLocator, textEditorContent, WAIT_4_ELEMENT);
		Assert.assertTrue(isPresent, "Baner text is not correct.");
	}
	
	public void checkBanerImage(){
		Assert.assertTrue(isImageDisplayed(), "Image is not displayed.");
	}
	
	public void checkDescriptionText(){
		Boolean isPresent = WaitTool.waitForTextPresent(driver, descriptionCssLocator, textEditorContent, WAIT_4_ELEMENT);
		Assert.assertTrue(isPresent, "Description text is not correct.");
	}
	
	public void checkUploadPhotosTextEditorText(){
		Boolean isPresent = WaitTool.waitForTextPresent(driver, heroContainerCssLocator, textEditorContent, WAIT_4_ELEMENT);
		Assert.assertTrue(isPresent, "Upload Photos text editr text is not correct.");
	}
	
	public void checkUploadPhoto(){
		Assert.assertTrue(isPhotoDisplayed(), "Photo is not displayed.");
	}
	
	public void checkDonationBtnEditedText(){
		Boolean isEdited = WaitTool.waitForTextPresent(driver, donationBtnIdLocator, donationBtnText, WAIT_4_ELEMENT);
		Assert.assertTrue(isEdited, "Donation button text is not edited.");
	}

}
