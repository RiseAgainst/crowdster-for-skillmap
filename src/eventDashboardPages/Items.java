package eventDashboardPages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import utils.DriverUtils;
import utils.WaitTool;

public class Items {
	private  WebDriver		driver;
	private static final int WAIT_4_ELEMENT	= WaitTool.DEFAULT_WAIT_4_ELEMENT;
	
	private static By iframeElementCssLocator	= By.cssSelector("iframe");
	private static By bodyElementCssLocator		= By.cssSelector("body");
	private static By saveBtnItemIdLocator		= By.id("save");
	private static By itemNameIdLocator			= By.id("create-item-name");
	private static By titleCssLocator			= By.cssSelector(".title");
	
	private static String registrationItem		= "Registration";
	private static String sponsorshipItem		= "Sponsorship";
	private static String donationItem			= "Donation";
	private static String createItemButton		= "create-item";
	private static String itemCategory			= "category";
	private static String itemName				= "name";
	private static String amount				= "value";
	private static String taxDeductible			= "taxDeductRate";
	private static String isItemAvailable		= "showItem";
	private static String saveButton			= "//*[@id='save']";
	private static String itemsRow				= "wiz-row";
	private static String editButtonText		= "Edit";
	private static String itemForm				= "crowdster-form";
	private static String advancedView			= "/html/body/div[3]/div/a";
	private static String itemNameValue			= "Item name test";
	private static String descriptionValue 		= "Description text test";
	private static String amountValue			= "10";
	
	public Items(WebDriver driver) {
		this.driver = driver;
	}
	
	public String getRegistrationItem(){
		return registrationItem;
	}
	public String getSponsorshipItem(){
		return sponsorshipItem;
	}
	public String getDonashionItem(){
		return donationItem;
	}
	
	public void clickCreateItem() {
		DriverUtils.click(By.id(createItemButton), driver);
		utils.WaitTool.waitForJQueryProcessing(driver, 5);
	}
	
	public void changeItemCategory(String category) {
		new Select(driver.findElement(By.name(itemCategory))).selectByVisibleText(category);
	}
	
	public void setItemName(String name) {
		List<WebElement> names = driver.findElements(By.name(itemName));
		names.get(2).sendKeys(name);
	}
	
	public void setAmount(String amountValue) {
		driver.findElement(By.name(amount)).sendKeys(amountValue);
	}
	
	public void setTaxDeductible(String taxDeductibleValue) {
		driver.findElement(By.name(taxDeductible)).clear();
		driver.findElement(By.name(taxDeductible)).sendKeys(taxDeductibleValue);
	}
	
	public void changeAvailableStatus() {
		WebElement switcher = WaitTool.waitForElement(driver, By.name(isItemAvailable), WAIT_4_ELEMENT);
		DriverUtils.clickChildElement(switcher, By.tagName("a"), driver);
	}
	
	public void clickSaveButton() {
		DriverUtils.click(By.xpath(saveButton), driver);
		utils.WaitTool.waitForJQueryProcessing(driver, 5);
	}
	
	public String getItemName() {
		List<WebElement> names = driver.findElements(By.name(itemName));
		return names.get(3).getAttribute("value");	
	}
	
	public EventEdit switchToAdvancedView() {
		DriverUtils.click(By.xpath(advancedView), driver);
		return new EventEdit(driver);
	}
	
	public String getItemCategory(String itemNameValue) {
		List<WebElement> itemForms = driver.findElements(By.className(itemForm));
		Select select = null ;
		for(WebElement e: itemForms) {
			if(e.isDisplayed()) {
			if(e.findElement(By.tagName("span")).getText().contains(itemNameValue)) {
				 select = new Select(e.findElement(By.name(itemCategory)));
			}
			}  
		}
		return select.getFirstSelectedOption().getText();
	}
	
	public String getAmountValue() {
		List<WebElement> amounts = driver.findElements(By.name(amount));
		String amountVal = amounts.get(1).getAttribute("value");
		amountVal = amountVal.substring(0, amountVal.indexOf("."));
		return amountVal ;
	}
	
	public String getTaxDeductibleValue() {
		List<WebElement> taxAmounts = driver.findElements(By.name(taxDeductible));
		String taxVal = taxAmounts.get(1).getAttribute("value");
		taxVal = taxVal.substring(0, taxVal.indexOf("."));
		return taxVal;
	}
	
	public String getAvailableStatus() {
		WebElement switcher = driver.findElement(By.name(isItemAvailable));
		return switcher.findElement(By.tagName("a")).getText();
	}
	
	
	
	public void clickItemsEdit(String navPageName) {
		List<WebElement> rows = driver.findElements(By.className(itemsRow));
		for(WebElement e: rows) {
			if(e.findElement(titleCssLocator).getText().equals(navPageName)) {
				e.findElement(By.linkText(editButtonText)).click();
			}
			}
		utils.WaitTool.waitForJQueryProcessing(driver, 5);
	}
	
	public void createItem(String item){
		this.clickCreateItem();
		this.changeItemCategory(item);
		WaitTool.waitForElement(driver, itemNameIdLocator, WAIT_4_ELEMENT).sendKeys(itemNameValue);
		this.switchToIframe();
		WaitTool.waitForElement(driver, bodyElementCssLocator, WAIT_4_ELEMENT).sendKeys(descriptionValue);
		this.switchToDefaultContent();
		this.setAmount(amountValue);
		DriverUtils.click(saveBtnItemIdLocator, driver);
		WaitTool.waitForJQueryProcessing(driver, 5);
	}
	
	private void switchToIframe(){
		List<WebElement> iFrames = WaitTool.waitForListElementsPresent(driver, iframeElementCssLocator, WAIT_4_ELEMENT);
		driver.switchTo().frame(iFrames.get(4));
	}
	
	private void switchToDefaultContent(){
		driver.switchTo().defaultContent();
	}
	
	//Asserts
	public void checkItemCategory(String actualCategory, String expectedCategory) {
		Assert.assertTrue(actualCategory.equals(expectedCategory), "Wrong item category ");
	}
	
	public void checkItem(String itemNameValue, String amountValue, String taxDeductibleValue) {
		Assert.assertTrue(getItemName().equals(itemNameValue), "Wrong item name ");
		Assert.assertTrue(getAmountValue().equals(amountValue), "Wrong amount  value");
		Assert.assertTrue(getTaxDeductibleValue().equals(taxDeductibleValue), "Wrong tax deductible value");
	}
	
}
