package eventDashboardPages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import eventPages.Event;
import utils.DriverUtils;
import utils.FileUploader;
import utils.WaitTool;

public class WebsiteMessages {
	private WebDriver 			driver;
	private FileUploader 		fileUploader;
	private static final int 	WAIT_4_ELEMENT		= WaitTool.DEFAULT_WAIT_4_ELEMENT;
	
	private static By submitBtnIdLocator			= By.id("submitBtn");
	private static By bodyElementTagNameLocator		= By.tagName("body");
	private static By thankYouMessageCssLocator		= By.cssSelector(".thanks-message");
	private static By metaElementTagNameLocator		= By.tagName("meta");
	private static By facebookTitleNameLocator		= By.name("fbTitle");
	private static By facebookMessageIdLocator		= By.id("fbMessage");
	private static By twitterMessageIdLocator		= By.id("twitterMessage");
	private static By eventTwitterIconCssLocator	= By.cssSelector(".aticon-twitter");
	private static By twitterStatusIdLocator		= By.id("status");
	private static By emailMessageIdLocator			= By.id("emailMessage");
	private static By eventEmailIconCssLocator		= By.cssSelector(".aticon-email");
	private static By eventEmailNoteIdLocator		= By.id("note");
	
	private static String thankYouMessageValue		= "TEST Thank you message after donation.";
	private static String fileNameToUpload			= "testLogo.png";
	private static String facebookTitleValue		= "TEST Facebook TITLE";
	private static String facebookMessageValue		= "TEST Facebook MeSsaGe!";
	private static String twitterMessageValue		= "TEST Twitter MeSsAge!!";
	private static String emailMessageValue			= "TEST E-mail message.";
	
	public WebsiteMessages(WebDriver driver) {
		this.driver = driver;
		this.fileUploader = new FileUploader(driver);
	}
	
	public String getFileNameToUpload(){
		return fileNameToUpload;
	}
	
	public Event clickSubmitBtn(){
		DriverUtils.click(submitBtnIdLocator, driver);
		return new Event(driver);
	}
	
	public void clickTwitterIcon(){
		DriverUtils.click(eventTwitterIconCssLocator, driver);
	}
	
	public void clickEmailIcon(){
		DriverUtils.click(eventEmailIconCssLocator, driver);
	}
	
	public void fillThankYouMessage(){
		WaitTool.waitForJQueryProcessing(driver, 5);
		WaitTool.waitForElement(driver, bodyElementTagNameLocator, WAIT_4_ELEMENT).sendKeys(thankYouMessageValue);
	}
	
	public void fillFacebookTitle(){
		WaitTool.waitForElement(driver, facebookTitleNameLocator, WAIT_4_ELEMENT).sendKeys(facebookTitleValue);
	}
	
	public void fillFacebookMessage(){
		WaitTool.waitForElement(driver, facebookMessageIdLocator, WAIT_4_ELEMENT).sendKeys(facebookMessageValue);
	}
	
	public void fillTwitterMessage(){
		WaitTool.waitForElement(driver, twitterMessageIdLocator, WAIT_4_ELEMENT).sendKeys(twitterMessageValue);
	}
	
	public void fillEmailMessage(){
		WaitTool.waitForElement(driver, emailMessageIdLocator, WAIT_4_ELEMENT).sendKeys(emailMessageValue);
	}
	
	public void selectUploadImage(String fileName){
		fileUploader.selectUploadFileImage(fileName);
	}
	
	private Boolean hasUploadedImageNameOnAnyMetaElement(){
		fileNameToUpload = fileNameToUpload.substring(0, fileNameToUpload.indexOf('.'));
		List<WebElement> metaElements = WaitTool.waitForListElementsPresent(driver, metaElementTagNameLocator, WAIT_4_ELEMENT);
		for(WebElement metaElement : metaElements){
			String metaElementContentAttribute = metaElement.getAttribute("content");
			if(metaElementContentAttribute.contains(fileNameToUpload)){
				return true;
			}
		}
		return false;
	}
	
	private Boolean hasFacebookTitleOrMessageOnAnyMetaElement(String titleOrMessage){
		List<WebElement> metaElements = WaitTool.waitForListElementsPresent(driver, metaElementTagNameLocator, WAIT_4_ELEMENT);
		for(WebElement metaElement : metaElements){
			String metaElementContentAttribute = metaElement.getAttribute("content");
			if(metaElementContentAttribute.contains(titleOrMessage)){
				return true;
			}
		}
		return false;
	}
	
	private String getTwitterMessage(){
		// Store the current window handle
		String winHandleBefore = driver.getWindowHandle();

		// Perform the click operation that opens new window

		// Switch to new window opened
		for(String winHandle : driver.getWindowHandles()){
		    driver.switchTo().window(winHandle);
		}

		// Perform the actions on new window
		WebElement twitterStatusElement = WaitTool.waitForElement(driver, twitterStatusIdLocator, WAIT_4_ELEMENT);
		String twitterMessage = twitterStatusElement.getText();
		// Close the new window, if that window no more required
		driver.close();

		// Switch back to original browser (first window)
		driver.switchTo().window(winHandleBefore);

		// Continue with original browser (first window)
		return twitterMessage;
	}
	
	private String getEmailMessage(){
		WebElement twitterStatusElement = WaitTool.waitForElement(driver, eventEmailNoteIdLocator, WAIT_4_ELEMENT);
		String twitterMessage = twitterStatusElement.getText();
		return twitterMessage;
	}
	
	//Asserts
	
	public void checkThankYouMessage(){
		String eventThankYouMessage = WaitTool.waitForElement(driver, thankYouMessageCssLocator, WAIT_4_ELEMENT).getText();
		Assert.assertTrue(eventThankYouMessage.contains(thankYouMessageValue), "Wrong thank you message.");
	}
	
	public void checkUploadedImageNameOnAnyMetaElement(){
		Assert.assertTrue(hasUploadedImageNameOnAnyMetaElement(), "Uploaded file name not contains at any meta element.");
	}
	
	public void checkFacebookTitleOnAnyMetaElement(){
		Assert.assertTrue(hasFacebookTitleOrMessageOnAnyMetaElement(facebookTitleValue), "Facebook title not contains at any meta element.");
	}
	
	public void checkFacebookMessageOnAnyMetaElement(){
		Assert.assertTrue(hasFacebookTitleOrMessageOnAnyMetaElement(facebookMessageValue), "Facebook message not contains at any meta element.");
	}
	
	public void checkTwitterMessage(){
		Assert.assertTrue(getTwitterMessage().contains(twitterMessageValue), "Twitter message not contains at the twitter status.");
	}
	
	public void checkEmailMessage(){
		Assert.assertTrue(getEmailMessage().contains(emailMessageValue), "Email message not contains at the email note.");
	}
}
