package eventDashboardPages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import utils.DriverUtils;
import utils.WaitTool;

public class SettingsSiteConfig {
	private WebDriver		driver;
	private static final int WAIT_4_ELEMENT		 = WaitTool.DEFAULT_WAIT_4_ELEMENT;
	private static By eventNameIdLocator		 = By.id("nameId");
	private static By financialGoalIdLocator 	 = By.id("financialGoal");
	private static By createPageBtnIdLocator 	 = By.id("createpage-btn");
	private static By locationNameLocator 		 = By.name("location");
	private static By addressOneNameLocator 	 = By.name("addressOne");
	private static By addressTwoNameLocator 	 = By.name("addressTwo");
	private static By cityNameLocator 			 = By.name("city");
	private static By stateNameLocator 			 = By.name("state");
	private static By zipNameLocator 			 = By.name("zip");
	private static By contactPhoneNameLocator 	 = By.name("contactPhone");
	private static By editSiteConfigBtnIdLocator = By.id("editsiteconfigBtn");
	private static By searchInputNameLocator 	 = By.name("searchInput");
	private static By mapAndDirectionsIdLocator  = By.id("map-3");
	private static By eventNameCssLocator 		 = By.cssSelector("h1");
	private static By loginBtnIdLocator 		 = By.id("loginButton");
	private static By jointTeamBtnIdLocator 	 = By.id("join-team-btn");
	private static By startTeamBtnIdLocator 	 = By.id("start-team-btn");
	private static By enableMobileXpathLocator   = By.xpath("/html/body/div[4]/div[1]/div[3]/div/div[2]/form/a[3]");
	private static By mobileSiteContainerCssLocator = By.cssSelector(".mobile-site-container");
	private static By enableIndividualPagesXpathLocator = By.xpath("/html/body/div[4]/div[1]/div[3]/div/div[2]/form/a[1]");
	private static By enableTeamsXpathLocator 	 = By.xpath("/html/body/div[4]/div[1]/div[3]/div/div[2]/form/a[4]");
	private static By showSearchXpathLocator 	 = By.xpath("/html/body/div[4]/div[1]/div[3]/div/div[2]/form/a[2]");
	private static By subdomainIdLocator		 = By.id("subdomainId");
	private static By eventThermoFundrasingGoalIdLocator = By.id("thermo-fundraising-goal");
	
	private String newEventName = "Another one Event with changed name";
	private String financialGoal = "2000";
	private String locationName = "River Forest";
	private String address1 	= "7439 Division St";
	private String address2 	= "Address 2 test";
	private String city 		= "Chicago";
	private String state 		= "IL";
	private String zip 			= "60305";
	private String contactPhone = "+1234567890";
	
	public SettingsSiteConfig(WebDriver driver) {
		this.driver = driver;
	}
	
	private void clickSaveButton(){
		DriverUtils.click(editSiteConfigBtnIdLocator, driver);
		WaitTool.waitForJQueryProcessing(driver, 5);
	}
	
	public void changeSiteEventName(){
		WebElement eventNameElement = WaitTool.waitForElement(driver, eventNameIdLocator, WAIT_4_ELEMENT);
		eventNameElement.clear();
		eventNameElement.sendKeys(newEventName);
		this.clickSaveButton();
	}
	
	public void changeSiteFundraisingGoal(){
		WebElement siteFundraisingGoalElement = WaitTool.waitForElement(driver, financialGoalIdLocator, WAIT_4_ELEMENT);
		siteFundraisingGoalElement.clear();
		siteFundraisingGoalElement.sendKeys(financialGoal);
		this.clickSaveButton();
	}
	
	public void changeSiteEventLocationName(){
		WebElement locationNameElement = WaitTool.waitForElement(driver, locationNameLocator, WAIT_4_ELEMENT);
		locationNameElement.clear();
		locationNameElement.sendKeys(locationName);
		this.clickSaveButton();
	}
	
	public void changeSiteEventAddress1(){
		WebElement address1Element = WaitTool.waitForElement(driver, addressOneNameLocator, WAIT_4_ELEMENT);
		address1Element.clear();
		address1Element.sendKeys(address1);
		this.clickSaveButton();
	}
	
	public void changeSiteEventAddress2(){
		WebElement address2Element = WaitTool.waitForElement(driver, addressTwoNameLocator, WAIT_4_ELEMENT);
		address2Element.clear();
		address2Element.sendKeys(address2);	
		this.clickSaveButton();
	}
	
	public void changeSiteEventCity(){
		WebElement cityElement = WaitTool.waitForElement(driver, cityNameLocator, WAIT_4_ELEMENT);
		cityElement.clear();
		cityElement.sendKeys(city);		
		this.clickSaveButton();
	}
	
	public String getSiteEventURL(){
		String urlInputText = WaitTool.waitForElement(driver, subdomainIdLocator, WAIT_4_ELEMENT).getAttribute("value");
		StringBuilder sb = new StringBuilder();
		sb.append("http://");
		sb.append(urlInputText);
		sb.append(".karma.com/");
		return sb.toString();
	}
	
	public void changeSiteEventState(){
		WebElement stateElement = WaitTool.waitForElement(driver, stateNameLocator, WAIT_4_ELEMENT);
		stateElement.clear();
		stateElement.sendKeys(state);	
		this.clickSaveButton();
	}
	
	public void changeSiteEventZip(){
		WebElement zipElement = WaitTool.waitForElement(driver, zipNameLocator, WAIT_4_ELEMENT);
		zipElement.clear();
		zipElement.sendKeys(zip);
		this.clickSaveButton();
	}
	
	public void changeSiteEventContactPhone(){
		WebElement contactPhoneElement = WaitTool.waitForElement(driver, contactPhoneNameLocator, WAIT_4_ELEMENT);
		contactPhoneElement.clear();
		contactPhoneElement.sendKeys(contactPhone);
		this.clickSaveButton();
	}
	
	public void changeSiteEventShowSearch(){
		WebElement showSearchElement = WaitTool.waitForElement(driver, showSearchXpathLocator, WAIT_4_ELEMENT);	
		WaitTool.resetImplicitWait(driver, 1);
		String showSearchClass = showSearchElement.getAttribute("class");
		if(!showSearchClass.contains("checked")){
			DriverUtils.clickElement(showSearchElement, driver);
		}
		this.clickSaveButton();
	}
	
	public void changeEnableTeams(){
		WebElement enableTeamsElement = WaitTool.waitForElement(driver, enableTeamsXpathLocator, WAIT_4_ELEMENT);	
		WaitTool.resetImplicitWait(driver, 1);
		String enableTeamsClass = enableTeamsElement.getAttribute("class");
		if(!enableTeamsClass.contains("checked")){
			DriverUtils.clickElement(enableTeamsElement, driver);
		}
		this.clickSaveButton();
	}
	
	public void enableIndividualPages(){
		WebElement enableIndividualPagesElement = WaitTool.waitForElement(driver, enableIndividualPagesXpathLocator, WAIT_4_ELEMENT);	
		WaitTool.resetImplicitWait(driver, 1);
		String enableIndividualPagesClass = enableIndividualPagesElement.getAttribute("class");
		if(!enableIndividualPagesClass.contains("checked")){
			DriverUtils.clickElement(enableIndividualPagesElement, driver);
		}
		this.clickSaveButton();
	}
	
	public void disableIndividualPages(){
		WebElement enableIndividualPagesElement = WaitTool.waitForElement(driver, enableIndividualPagesXpathLocator, WAIT_4_ELEMENT);	
		WaitTool.resetImplicitWait(driver, 1);
		String enableIndividualPagesClass = enableIndividualPagesElement.getAttribute("class");
		if(enableIndividualPagesClass.contains("checked")){
			DriverUtils.clickElement(enableIndividualPagesElement, driver);
		}
		this.clickSaveButton();
	}
	
	public void changeMobile(){
		WebElement enableMobileElement = WaitTool.waitForElement(driver, enableMobileXpathLocator, WAIT_4_ELEMENT);	
		WaitTool.resetImplicitWait(driver, 1);
		String enableMobileClass = enableMobileElement.getAttribute("class");
		if(!enableMobileClass.contains("checked")){
			DriverUtils.clickElement(enableMobileElement, driver);
		}
		this.clickSaveButton();
	}
	
	public Boolean isDisplayedTeamButtons(){
		Boolean isDisplayed = false;
		WebElement startTeamElement = WaitTool.waitForElement(driver, startTeamBtnIdLocator, WAIT_4_ELEMENT);
		WebElement joinTeamElement = WaitTool.waitForElement(driver, jointTeamBtnIdLocator, WAIT_4_ELEMENT);
		if(startTeamElement != null && joinTeamElement != null){
			isDisplayed = startTeamElement.isDisplayed() && joinTeamElement.isDisplayed();
		}
		return isDisplayed;
	}
	
	public Boolean isDisplayedLogIn(){
		Boolean isDisplayed = false;
		WebElement logInElement = WaitTool.waitForElement(driver, loginBtnIdLocator, WAIT_4_ELEMENT);
		if(logInElement != null){
			isDisplayed = logInElement.isDisplayed();
		}
		return isDisplayed;
	}
	
	public Boolean isDisplayedCreatePageWidget(){
		WebElement createPageWidgetElement = WaitTool.waitForElement(driver, createPageBtnIdLocator, WAIT_4_ELEMENT);
		if(createPageWidgetElement != null){
			return true;
		}
		return false;
	}
	
	public String getFinancialGoal(){
		String termoFundraisingGoal = WaitTool.waitForElement(driver, eventThermoFundrasingGoalIdLocator, WAIT_4_ELEMENT).getText();
		termoFundraisingGoal = termoFundraisingGoal.substring(termoFundraisingGoal.indexOf("$") + 1,
				termoFundraisingGoal.indexOf("G") - 1).replaceAll(",","");
	    return termoFundraisingGoal;
	}
	
	public Boolean isDisplayedSearch(){
		Boolean isDisplayed = false;
		WebElement searchInputElement = WaitTool.waitForElement(driver, searchInputNameLocator, WAIT_4_ELEMENT);
		if(searchInputElement != null){
			isDisplayed = searchInputElement.isDisplayed();
		}
		return isDisplayed;
	}
	
	//Asserts
	public void checkChangedName(){
		String changedName = WaitTool.waitForElement(driver, eventNameCssLocator, WAIT_4_ELEMENT).getText();
		Assert.assertTrue(newEventName.equals(changedName), "Event name is not changed.");
	}
	
	public void checkChangedLocationName(){
		String locatorText = WaitTool.waitForElement(driver, mapAndDirectionsIdLocator, WAIT_4_ELEMENT).getText();
		Assert.assertTrue(locatorText.contains(locationName), "Location Name is not changed.");	
	}
	
	public void checkChangedAddress1(){
		String locatorText = WaitTool.waitForElement(driver, mapAndDirectionsIdLocator, WAIT_4_ELEMENT).getText();
		Assert.assertTrue(locatorText.contains(address1), "Address1 is not changed.");		
	}
	
	public void checkChangedAddress2(){
		String locatorText = WaitTool.waitForElement(driver, mapAndDirectionsIdLocator, WAIT_4_ELEMENT).getText();
		Assert.assertTrue(locatorText.contains(address2), "Address2 is not changed.");		
	}
	
	public void checkChangedCity(){
		String locatorText = WaitTool.waitForElement(driver, mapAndDirectionsIdLocator, WAIT_4_ELEMENT).getText();
		Assert.assertTrue(locatorText.contains(city), "City is not changed.");		
	}
	
	public void checkChangedState(){
		String locatorText = WaitTool.waitForElement(driver, mapAndDirectionsIdLocator, WAIT_4_ELEMENT).getText();
		Assert.assertTrue(locatorText.contains(state), "State is not changed.");		
	}
	
	public void checkChangedZip(){
		String locatorText = WaitTool.waitForElement(driver, mapAndDirectionsIdLocator, WAIT_4_ELEMENT).getText();
		Assert.assertTrue(locatorText.contains(zip), "Zip is not changed.");		
	}
	
	public void checkChangedContactPhone(){
		String locatorText = WaitTool.waitForElement(driver, mapAndDirectionsIdLocator, WAIT_4_ELEMENT).getText();
		Assert.assertTrue(locatorText.contains(contactPhone), "Contact Phone is not changed.");		
	}
	
	public void checkShowSearch(Boolean isDisplayed){
		Assert.assertTrue(isDisplayed, "Search input is not displayed.");	
	}
	
	public void checkEnableTeams(Boolean isDisplayed){
		Assert.assertTrue(isDisplayed, "Team buttons are not displayed.");
	}
	
	public void checkEnabledCreatePageWidget(Boolean isDisplayed){
		Assert.assertTrue(isDisplayed, "'Create a Page' widget is not displayed.");
	}
	
	public void checkDesabledCreatePageWidget(Boolean isDisplayed){
		Assert.assertFalse(isDisplayed, "'Create a Page' widget is displayed.");
	}
	
	public void checkMobileVersion(){
		Boolean isOnMobileVersion = WaitTool.waitForElement(driver, mobileSiteContainerCssLocator, WAIT_4_ELEMENT).isDisplayed();
		Assert.assertTrue(isOnMobileVersion, "Not in mobile version.");		
	}
	
	public void checkFundraisingGoal(String fundraisingGoal){
		Assert.assertTrue(fundraisingGoal.equals(financialGoal), "Fundraising Goal is not correct.");	
	}
	
	public void checkSiteEventURL(String siteEventUtl){
		String currentUrl = driver.getCurrentUrl();
		Assert.assertTrue(siteEventUtl.equals(currentUrl), "Event URL is not correct.");
	}
}
