package eventDashboardPages;


import mobilePages.EventMobile;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import eventPages.Event;
import otherSitesPages.Twitter;
import pages.ShareViaEmail;
import utils.DriverUtils;
import utils.WindowHandler;

public class EventEdit {
	private  WebDriver		driver;
	private WindowHandler winHandler;
	
	private static By wizardViewBtnIdLocator	= By.id("wizard-view");
	
	private static String promoteOnTwitter	 	= "crowdsterTweet-text";
	private static String shareViaEmail		 	= "//*[@id='mailtoID-text']";
	private static String viewSiteXpath		 	= "//*[@id='viewsitehrefId']";
	private static String publicPrivateChange	= "pp-toggle-link";
	
	public EventEdit(WebDriver driver) {
		this.driver = driver;
		this.winHandler = new WindowHandler();
	}
	
	public Event clickViewSite() {
		winHandler.getOldWindow(driver);
		DriverUtils.click(By.xpath(viewSiteXpath), driver);
		winHandler.switchToNewWindow(driver);
		utils.WaitTool.waitForJQueryProcessing(driver, 5);
		return new Event(driver);
	}
	
	public EventMobile clickViewSiteMobile() {
		winHandler.getOldWindow(driver);
		DriverUtils.click(By.xpath(viewSiteXpath), driver);
		winHandler.switchToNewWindow(driver);
		utils.WaitTool.waitForJQueryProcessing(driver, 5);
		return new EventMobile(driver);
	}
	
	
	public void switchToMainWindow() {
		winHandler.switchToOldWindow(driver);
	}
	
	public void switchToMainWindowAfterTest(WebDriver driver) {
		winHandler.switchToOldWindow(driver);
	}

	public Twitter clickPromoteOnTwitter() {
		winHandler.getOldWindow(driver);
		DriverUtils.click(By.id(promoteOnTwitter), driver);
		winHandler.switchToNewWindow(driver);
		return new Twitter(driver);
	}
	
	public ShareViaEmail clickShareViaEmail() {
		DriverUtils.click(By.xpath(shareViaEmail), driver);
		return new ShareViaEmail(driver);
	}
	
	public void clickWizzardView() {
		DriverUtils.click(wizardViewBtnIdLocator, driver);
	}
	
	public void makeSitePublic() {
		WebElement link = driver.findElement(By.className(publicPrivateChange));
		if(link.getText().contains("public"))
			link.click();
	}
		
	
}
