package eventDashboardPages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import utils.DriverUtils;
import utils.FileUploader;
import utils.Misc;
import utils.WaitTool;

public class AuctionItem {
	private WebDriver 			driver;
	private FileUploader 		fileUploader;
	private static final int 	WAIT_4_ELEMENT			= WaitTool.DEFAULT_WAIT_4_ELEMENT;
	
	private static By createAuctionItemBtnIdLocator		= By.id("create_auction_item");
	private static By nameNameLocator					= By.name("name");
	private static By descriptionNameLocator			= By.name("fckEditor_description");
	private static By itemCodeNameLocator				= By.name("itemCode");
	private static By donorNameLocator					= By.name("source");
	private static By openingPriceNameLocator			= By.name("openingPrice");
	private static By minimumIncrementValueNameLocator	= By.name("incrementValue");
	private static By fairMarketValueNameLocator		= By.name("actualValue");
	private static By buyNowPriceNameLocator			= By.name("buyNowPrice");
	private static By rankOrderNameLocator				= By.name("rank");
	private static By submitBtnNameLocator				= By.name("submit");
	private static By datepickerCssLocator				= By.cssSelector("img.ui-datepicker-trigger");
	private static By eventAuctionBtnIdLocator			= By.id("auction_btn");
	private static By eventAuctionNameTagLocator		= By.tagName("h3");
	private static By eventAictionDescriptionCssLocator = By.cssSelector("div.ai-desc");
	private static By eventMoreInformetionBtnNameLocator 	= By.name("auction-more-information-btn");
	private static By eventAuctionOpeningPriceIdLocator = By.id("auction_opening_price");
	private static By eventAuctionMinimumRaiseIdLocator = By.id("auction_minimum_raise");
	private static By eventAuctionDonorIdLocator		= By.id("auction_donor");
	private static By eventAuctionItemCodeIdLocator		= By.id("auction_item_code");
	private static By editAuctionLinkTextLocator		= By.linkText("Edit");
	private static By deleteAuctionLinkTextLocator		= By.linkText("Delete");
	private static By auctionBiddingStartDateIdLocator	= By.id("startdate");
	private static By auctionBiddingEndDateIdLocator	= By.id("date");
	private static By eventAuctionStartDateIdLocator	= By.id("auction_start_date");
	private static By eventAuctionEndDateIdLocator		= By.id("auction_end_date");
	private static By auctionItemsTextLinkLocator 		= By.linkText("Auction Items");
	private static By eventAuctionImgContainer			= By.cssSelector("div.auction-img-container");
	private static By eventAuctionImgTagLocator			= By.tagName("img");
	
	
	private static String nameValue						= "TEST auction name";
	private static String descriptionValue				= "TEST auction description. TEST auction description. TEST auction description.";
	private static String itemCodeValue					= "111";
	private static String donorValue					= "TEST auction donor";
	private static String openingPriceValue				= "100";
	private static String minimumIncrementValue			= "10";
	private static String fairMarketValue				= "300";
	private static String buyNowPriceValue				= "200";
	private static String rankOrderValue				= "1";
	private static String fileNameToUpload				= "randomLogo.png";
	
	public AuctionItem(WebDriver driver) {
		this.driver = driver;
		this.fileUploader = new FileUploader(driver);
	}
	
	public void clickCreateAuctionItemBtn(){
		DriverUtils.click(createAuctionItemBtnIdLocator, driver);
	}
	
	public void clickSubmitBtn(){
		DriverUtils.click(submitBtnNameLocator, driver);
	}
	
	public void clickAuctionRegistrationBtn(){
		DriverUtils.click(eventAuctionBtnIdLocator, driver);
	}
	
	public void clickMoreInformationBtn(){
		DriverUtils.click(eventMoreInformetionBtnNameLocator, driver);
	}
	
	public void clickEditAuction(){
		WaitTool.waitForElement(driver, editAuctionLinkTextLocator, WAIT_4_ELEMENT).click();
	}
	
	public void clickDeleteAuction(){
		WaitTool.waitForElement(driver, deleteAuctionLinkTextLocator, WAIT_4_ELEMENT).click();
	}
	
	public void clickDashboardAuctionItems(){
		DriverUtils.click(auctionItemsTextLinkLocator, driver);
		WaitTool.waitForJQueryProcessing(driver, 5);
	}
	
	public void fillAuctionItem(){
		WaitTool.waitForElement(driver, nameNameLocator, WAIT_4_ELEMENT).sendKeys(nameValue);
		WaitTool.waitForElement(driver, descriptionNameLocator, WAIT_4_ELEMENT).sendKeys(descriptionValue);
		WaitTool.waitForElement(driver, itemCodeNameLocator, WAIT_4_ELEMENT).sendKeys(itemCodeValue);
		WaitTool.waitForElement(driver, donorNameLocator, WAIT_4_ELEMENT).sendKeys(donorValue);
		WaitTool.waitForElement(driver, openingPriceNameLocator, WAIT_4_ELEMENT).sendKeys(openingPriceValue);
		WaitTool.waitForElement(driver, minimumIncrementValueNameLocator, WAIT_4_ELEMENT).sendKeys(minimumIncrementValue);
		WaitTool.waitForElement(driver, fairMarketValueNameLocator, WAIT_4_ELEMENT).sendKeys(fairMarketValue);
		WaitTool.waitForElement(driver, buyNowPriceNameLocator, WAIT_4_ELEMENT).sendKeys(buyNowPriceValue);
		this.setStartEndDate();
		this.selectUploadImage(fileNameToUpload);
		WaitTool.waitForElement(driver, rankOrderNameLocator, WAIT_4_ELEMENT).sendKeys(rankOrderValue);
	}
	
	public String getBiddingStartDate(){
		return WaitTool.waitForElement(driver, auctionBiddingStartDateIdLocator, WAIT_4_ELEMENT).getAttribute("value");
	}
	
	public String getBiddingEndDate(){
		return WaitTool.waitForElement(driver, auctionBiddingEndDateIdLocator, WAIT_4_ELEMENT).getAttribute("value");
	}
	
	private void selectUploadImage(String fileName){
		fileUploader.selectUploadFileImage(fileName);
	}
	
	private void setStartEndDate(){
		List<WebElement> datePickers = WaitTool.waitForListElementsPresent(driver, datepickerCssLocator, WAIT_4_ELEMENT);
		DriverUtils.clickElement(datePickers.get(0), driver);
		int currentDay = Integer.parseInt(Misc.getCurrentDate("Day")) + 1;
		DriverUtils.click(By.linkText(String.valueOf(currentDay)), driver);
		WaitTool.waitForJQueryProcessing(driver, 3);
		DriverUtils.clickElement(datePickers.get(1), driver);
		DriverUtils.click(By.linkText("Next"), driver);
		DriverUtils.click(By.linkText("25"), driver);
		WaitTool.waitForJQueryProcessing(driver, 3);
	}
	
	private String getElementTextByLocator(By locator){
		return WaitTool.waitForElement(driver, locator, WAIT_4_ELEMENT).getText();
	}
	
	//Asserts
	
	public void checkAuctionsCountAfterCreate(int auctionsCountBefore, int auctionsCountAfter){
		Assert.assertTrue(auctionsCountAfter == auctionsCountBefore + 1, "Wrong auctions count.");
	}
	
	public void checkAuctionsCountAfterDelete(int auctionsCountBefore, int auctionsCountAfter){
		Assert.assertTrue(auctionsCountAfter == auctionsCountBefore - 1, "Wrong auctions count.");
	}
	
	public void checkAuctionNameAndDescription(){
		Assert.assertTrue(this.getElementTextByLocator(eventAuctionNameTagLocator).equals(nameValue), "Wrong auction's name.");
		Assert.assertTrue(this.getElementTextByLocator(eventAictionDescriptionCssLocator).equals(descriptionValue), "Wrong auction's description.");
	}
	
	public void checkAuctionOpeningPriceAndMinimumRaise(){
		String openingPrice = this.getElementTextByLocator(eventAuctionOpeningPriceIdLocator);
		openingPrice = openingPrice.substring(1);
		String minimumRaise = this.getElementTextByLocator(eventAuctionMinimumRaiseIdLocator);
		minimumRaise = minimumRaise.substring(1);
		Assert.assertTrue(openingPrice.equals(openingPriceValue), "Wrong auction's opening price.");
		Assert.assertTrue(minimumRaise.equals(minimumIncrementValue), "Wrong auction's minimum raise.");
	}
	
	public void checkDonorAndItemCode(){
		Assert.assertTrue(this.getElementTextByLocator(eventAuctionDonorIdLocator).equals(donorValue), "Wrong auction's donor.");
		Assert.assertTrue(this.getElementTextByLocator(eventAuctionItemCodeIdLocator).equals(itemCodeValue), "Wrong auction's item code.");
	}
	
	public void checkBiddingStartEndDate(String biddingStartDate, String biddingEndDate){
		Assert.assertTrue(this.getElementTextByLocator(eventAuctionStartDateIdLocator).equals(biddingStartDate), "Wrong auction's bidding start date.");
		Assert.assertTrue(this.getElementTextByLocator(eventAuctionEndDateIdLocator).equals(biddingEndDate), "Wrong auction's bidding end date.");
	}
	
	public void checkAuctionItemImg(){
		WebElement auctionImgContainerElement = WaitTool.waitForElement(driver, eventAuctionImgContainer, WAIT_4_ELEMENT);
		WebElement auctionImg = auctionImgContainerElement.findElement(eventAuctionImgTagLocator);
		String acutionImgSource = auctionImg.getAttribute("src");
		fileNameToUpload = fileNameToUpload.substring(0, fileNameToUpload.indexOf("."));
		Assert.assertTrue(acutionImgSource.contains(fileNameToUpload), "Wrong auction's image.");
	}
}
