package eventDashboardPages;

import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import utils.WaitTool;
import utils.WindowHandler;

public class MobileNavigation {
	private WebDriver 		driver;
	private WindowHandler 	handler;
	private WebDriverWait 	wait;
	private static final int WAIT_4_ELEMENT			= WaitTool.DEFAULT_WAIT_4_ELEMENT;
		
	private static By createPageBtnCssLocator  		= By.cssSelector(".crowdster-form-button");
	private static By nameLocator					= By.name("name");
	private static By contentLocator				= By.id("content");
	private static By urlLocator					= By.name("url");
	private static By isPrivateLocator				= By.name("isPrivate");
	private static By subBtnLocator					= By.id("subBtn");
	private static By submitBtnLocator				= By.id("submitBtn");
	private static By viewSiteIdLocator				= By.id("viewsitehrefId");
	private static By navPageContentClassNameLocator = By.className("body");
	private static By navPageNameClassNameLocator 	= By.className("heading");
	private static By menuIconIdLocator 			= By.id("menu-icon");
	
	private String navigationPageName				= "Navigation page name test";
	private String navigationPageContent			= "Navigation page content test";
	private String navigationPageUrl				= "";
	private String navigationPageNameEdit			= "Edit Navigation page name test";
	private String navigationPageContentEdit		= "Edit Navigation page content test";
	private String navigationPageUrlEdit			= "";
	private String navigationSubpageName			= "Navigation subpage name test";
	private String navigationSubpageContent			= "Navigation subpage content test";
	private String navigationSubpageUrl				= "";
	private String navigationSubpageNameEdit		= "Edit Navigation subpage name test";
	private String navigationSubpageContentEdit		= "Edit Navigation subpage content test";
	private String navigationSubpageUrlEdit			= "";
	private String privateNavigationPageName		= "Private Navigation page name test";
	private String privateNavigationPageContent		= "Private Navigation page content test";
	private String privateNavigationSubpageName	= "Private Navigation subpage name test";
	private String privateNavigationSubpageContent	= "Private Navigation subpage content test";
	
	public MobileNavigation(WebDriver driver) {
		this.driver = driver;
		this.handler = new WindowHandler();
		this.wait = new WebDriverWait(driver, WebDriverWait.DEFAULT_SLEEP_TIMEOUT);
	}
	
	public void clickCreatePage(){	
		WaitTool.waitForElement(driver, createPageBtnCssLocator, WAIT_4_ELEMENT).click();
	}
	
	public void clickEditPage(){	
		WaitTool.waitForElement(driver, By.linkText("Edit"), WAIT_4_ELEMENT).click();
	}
	
	public void clickAddSubpage(){	
		WaitTool.waitForElement(driver, By.name("add-subpage"), WAIT_4_ELEMENT).click();
	}
	
	public void clickEditSubpage(){
		List<WebElement> editBtnsElements = WaitTool.waitForListElementsPresent(driver, By.linkText("Edit"), WAIT_4_ELEMENT);
		editBtnsElements.get(1).click();
	}
	
	public void clickDeleteSubpage(){
		List<WebElement> deleteBtnsElements = WaitTool.waitForListElementsPresent(driver, By.linkText("Delete"), WAIT_4_ELEMENT);
		deleteBtnsElements.get(1).click();
	}
	
	private void clickSubBtn(){
		WaitTool.waitForElement(driver, subBtnLocator, WAIT_4_ELEMENT).click();
	}
	
	private void clickSubmitBtn(){
		WaitTool.waitForElement(driver, submitBtnLocator, WAIT_4_ELEMENT).click();
	}
	
	private void clickVewSite(){
		WaitTool.waitForElement(driver, viewSiteIdLocator, WAIT_4_ELEMENT).click();
	}
	
	public void clickNavigationDashboard(){
		WaitTool.waitForElement(driver, By.id("mnav"), WAIT_4_ELEMENT).click();
	}
	
	public void waitSwitchAcceptJsAlert(){
		wait.until(ExpectedConditions.alertIsPresent());
		driver.switchTo().alert().accept();
	}
	
	public int getNaviPagesCount(){
		List<WebElement> naviPages = WaitTool.waitForListElementsPresent(driver, By.cssSelector(".site-rows"), WAIT_4_ELEMENT);
		return naviPages.size();
	}
	
	public void createNavigationPage(){
		WaitTool.waitForElement(driver, nameLocator, WAIT_4_ELEMENT).sendKeys(navigationPageName);
		WaitTool.waitForElement(driver, contentLocator, WAIT_4_ELEMENT).sendKeys(navigationPageContent);
		WaitTool.waitForElement(driver, urlLocator, WAIT_4_ELEMENT).sendKeys(navigationPageUrl);
		this.clickSubBtn();
	}
	
	public void createPrivateNavigationPage(){
		WaitTool.waitForElement(driver, nameLocator, WAIT_4_ELEMENT).sendKeys(privateNavigationPageName);
		WaitTool.waitForElement(driver, contentLocator, WAIT_4_ELEMENT).sendKeys(privateNavigationPageContent);
		WaitTool.waitForElement(driver, isPrivateLocator, WAIT_4_ELEMENT).click();
		this.clickSubBtn();
	}
	
	public void editNavigationPage(){
		WebElement naviPageNameElement = WaitTool.waitForElement(driver, nameLocator, WAIT_4_ELEMENT);
		WebElement naviPageContentElement = WaitTool.waitForElement(driver, contentLocator, WAIT_4_ELEMENT);
		WebElement naviPageUrlElement = WaitTool.waitForElement(driver, urlLocator, WAIT_4_ELEMENT);
		naviPageNameElement.clear();
		naviPageNameElement.sendKeys(navigationPageNameEdit);
		naviPageContentElement.clear();
		naviPageContentElement.sendKeys(navigationPageContentEdit);
		naviPageUrlElement.clear();
		naviPageUrlElement.sendKeys(navigationPageUrlEdit);
		this.clickSubmitBtn();
	}
	
	public void deleteNavigationPage(){
		WaitTool.waitForElement(driver, By.linkText("Delete"), WAIT_4_ELEMENT).click();
	}
	
	public void createNavigationSubpage(){
		WebElement naviPageNameElement = WaitTool.waitForElement(driver, nameLocator, WAIT_4_ELEMENT);
		WebElement naviPageContentElement = WaitTool.waitForElement(driver, contentLocator, WAIT_4_ELEMENT);
		WebElement naviPageUrlElement = WaitTool.waitForElement(driver, urlLocator, WAIT_4_ELEMENT);
		naviPageNameElement.sendKeys(navigationSubpageName);
		naviPageContentElement.sendKeys(navigationSubpageContent);
		naviPageUrlElement.sendKeys(navigationSubpageUrl);
		this.clickSubmitBtn();
	}
	
	public void createPrivateNavigationSubpage(){
		WebElement naviPrivateSubpageNameElement = WaitTool.waitForElement(driver, nameLocator, WAIT_4_ELEMENT);
		WebElement naviPrivateSubpageContentElement = WaitTool.waitForElement(driver, contentLocator, WAIT_4_ELEMENT);
		naviPrivateSubpageNameElement.sendKeys(privateNavigationSubpageName);
		naviPrivateSubpageContentElement.sendKeys(privateNavigationSubpageContent);
		WaitTool.waitForElement(driver, isPrivateLocator, WAIT_4_ELEMENT).click();
		this.clickSubmitBtn();
	}
	
	public void editNavigationSubpage(){
		WebElement naviSubpageNameElement = WaitTool.waitForElement(driver, nameLocator, WAIT_4_ELEMENT);
		WebElement naviSubpageContentElement = WaitTool.waitForElement(driver, contentLocator, WAIT_4_ELEMENT);
		WebElement naviSubpageUrlElement = WaitTool.waitForElement(driver, urlLocator, WAIT_4_ELEMENT);
		naviSubpageNameElement.clear();
		naviSubpageNameElement.sendKeys(navigationSubpageNameEdit);
		naviSubpageContentElement.clear();
		naviSubpageContentElement.sendKeys(navigationSubpageContentEdit);
		naviSubpageUrlElement.clear();
		naviSubpageUrlElement.sendKeys(navigationSubpageUrlEdit);
		this.clickSubmitBtn();
	}
	
	public void switchToMobileSiteEvent(){
    	this.handler.getOldWindow(driver);
    	this.clickVewSite();
    	this.handler.switchToNewWindow(driver);
	}
	
	public void switchToMainWindow(){
		this.handler.switchToOldWindow(driver);
	}
    
    public void clickMobileMenuNavigationPageElement(){
    	WaitTool.waitForElement(driver, menuIconIdLocator, WAIT_4_ELEMENT).click();
    	WaitTool.waitForElement(driver, By.linkText(navigationPageName), WAIT_4_ELEMENT).click();
    }
    
    public void clickMobileMenuNavigationPageEditElement(){
    	WaitTool.waitForElement(driver, menuIconIdLocator, WAIT_4_ELEMENT).click();
    	WaitTool.waitForElement(driver, By.linkText(navigationPageNameEdit), WAIT_4_ELEMENT).click();
    }
    
    public void clickMobileMenuNavigationSubpageElement(){
    	WaitTool.waitForElement(driver, menuIconIdLocator, WAIT_4_ELEMENT).click();
    	WaitTool.waitForElement(driver, By.linkText(navigationSubpageName), WAIT_4_ELEMENT).click();
    }
    
    public void clickMobileMenuNavigationSubpageEditedElement(){
    	WaitTool.waitForElement(driver, menuIconIdLocator, WAIT_4_ELEMENT).click();
    	WaitTool.waitForElement(driver, By.linkText(navigationSubpageNameEdit), WAIT_4_ELEMENT).click();
    }
    
    public Boolean isPrivateNavigationPage(){
    	Boolean isPrivate = false;
    	WaitTool.waitForElement(driver, menuIconIdLocator, WAIT_4_ELEMENT).click();
    	WebElement privateNavigationPageElement = WaitTool.waitForElement(driver, By.linkText(privateNavigationPageName), WAIT_4_ELEMENT);
    	if(privateNavigationPageElement == null){
    		isPrivate = true;
    	}
    	return isPrivate;
    }
    
    public Boolean isPrivateNavigationSubpage(){
    	Boolean isPrivate = false;
    	WaitTool.waitForElement(driver, menuIconIdLocator, WAIT_4_ELEMENT).click();
    	WebElement privateNavigationPageElement = WaitTool.waitForElement(driver, By.linkText(privateNavigationSubpageName), WAIT_4_ELEMENT);
    	WaitTool.waitForJQueryProcessing(driver, WAIT_4_ELEMENT);
    	if(privateNavigationPageElement == null){
    		isPrivate = true;
    	}
    	return isPrivate;
    }
    
    //Asserts   
    public void checkMobileNavigationPage(){
    	String navigationPageNameText = WaitTool.waitForElement(driver, navPageNameClassNameLocator, WAIT_4_ELEMENT).getText();
    	String navigationPageContentText = WaitTool.waitForElement(driver, navPageContentClassNameLocator, WAIT_4_ELEMENT).getText();
    	Assert.assertTrue("Navigation page name is not correct.", navigationPageName.equals(navigationPageNameText));
    	Assert.assertTrue("Navigation page content is not correct.", navigationPageContent.equals(navigationPageContentText));
    }
    
    public void checkEditedMobileNavigationPage(){
    	String navigationPageEditedNameText = WaitTool.waitForElement(driver, navPageNameClassNameLocator, WAIT_4_ELEMENT).getText();
    	String navigationPageEditedContentText = WaitTool.waitForElement(driver, navPageContentClassNameLocator, WAIT_4_ELEMENT).getText();
    	Assert.assertTrue("Navigation page name is not edited correct.", navigationPageNameEdit.equals(navigationPageEditedNameText));
    	Assert.assertTrue("Navigation page content is not edited correct.", navigationPageContentEdit.equals(navigationPageEditedContentText));
    }
    
    public void checkMobileNavigationSubpage(){
    	String navigationSubpageNameText = WaitTool.waitForElement(driver, navPageNameClassNameLocator, WAIT_4_ELEMENT).getText();
    	String navigationSubpageContentText = WaitTool.waitForElement(driver, navPageContentClassNameLocator, WAIT_4_ELEMENT).getText();
    	Assert.assertTrue("Navigation subpage name is not correct.", navigationSubpageName.equals(navigationSubpageNameText));
    	Assert.assertTrue("Navigation subpage content is not correct.", navigationSubpageContent.equals(navigationSubpageContentText));
    }
    
    public void checkMobileNavigationSubpageEdited(){
    	String navigationSubpageNameEditedText = WaitTool.waitForElement(driver, navPageNameClassNameLocator, WAIT_4_ELEMENT).getText();
    	String navigationSubpageContentEditedText = WaitTool.waitForElement(driver, navPageContentClassNameLocator, WAIT_4_ELEMENT).getText();
    	Assert.assertTrue("Navigation subpage name is not edited correct.", navigationSubpageNameEdit.equals(navigationSubpageNameEditedText));
    	Assert.assertTrue("Navigation subpage content is not edited correct.", navigationSubpageContentEdit.equals(navigationSubpageContentEditedText));
    }
    
    public void checkNavigationPagesCount(int naviPagesCountBefore, int naviPagesCountAfter){
    	Assert.assertTrue("Navigation pages count is the same. Somthing wrong happened.", naviPagesCountBefore != naviPagesCountAfter);
    }
    
    public void checkForPrivateNavigationPage(Boolean isPrivate){
    	Assert.assertTrue("Navigation page is visibale and is not private.", isPrivate);
    }
    
    public void checkForPrivateNavigationSubpage(Boolean isPrivate){
    	Assert.assertTrue("Navigation subpage is visibale and is not private.", isPrivate);
    }
}
