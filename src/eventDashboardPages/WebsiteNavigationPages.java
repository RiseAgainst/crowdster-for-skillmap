package eventDashboardPages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import utils.DriverUtils;
import utils.WaitTool;

public class WebsiteNavigationPages {
	private WebDriver driver;
	private static final int WAIT_4_ELEMENT			= WaitTool.DEFAULT_WAIT_4_ELEMENT;
	
	private static By createPageBtnIdLocator		= By.id("create-page");
	private static By pageTitleIdLocator			= By.id("navi-page-title");
	private static By navigationLabelNameLocator	= By.name("navigationLabel");
	private static By pagePermalinkNameLocator		= By.name("permalink");
	private static By iframeElementCssLocator		= By.cssSelector("iframe");
	private static By bodyElementCssLocator			= By.cssSelector("body");
	private static By saveBtnIdLocator				= By.id("savepage");
	private static By continueBtnIdLocator			= By.id("navi-pages-continue");
	
	private static String pageTitleValue			= "Page Title test";
	private static String navigationLabelValue		= "Navigation Label test";
	private static String permalinkValue			= "http://www.permalinktest.info";
	private static String pageContentValue			= "Page Content test";
	
	public WebsiteNavigationPages(WebDriver driver) {
		this.driver = driver;
	}
	
	public void clickCreatePage(){
		DriverUtils.click(createPageBtnIdLocator, driver);
	}
	
	public void createNavigationPage(){
		WaitTool.waitForElement(driver, pageTitleIdLocator, WAIT_4_ELEMENT).sendKeys(pageTitleValue);
		WaitTool.waitForElement(driver, navigationLabelNameLocator, WAIT_4_ELEMENT).sendKeys(navigationLabelValue);
		WaitTool.waitForElement(driver, pagePermalinkNameLocator, WAIT_4_ELEMENT).sendKeys(permalinkValue);
		this.switchToIframe();
		WaitTool.waitForElement(driver, bodyElementCssLocator, WAIT_4_ELEMENT).sendKeys(pageContentValue);
		this.switchToDefaultContent();
		DriverUtils.click(saveBtnIdLocator, driver);
		WaitTool.waitForJQueryProcessing(driver, 5);
	}
	
	public void clickContinue(){
		DriverUtils.click(continueBtnIdLocator, driver);
	}
	
	private void switchToIframe(){
		List<WebElement> iFrames = WaitTool.waitForListElementsPresent(driver, iframeElementCssLocator, WAIT_4_ELEMENT);
		driver.switchTo().frame(iFrames.get(2));
	}
	
	private void switchToDefaultContent(){
		driver.switchTo().defaultContent();
	}
}
