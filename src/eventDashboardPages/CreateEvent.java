package eventDashboardPages;


import java.util.List;
import java.util.UUID;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import utils.DriverUtils;
import utils.Misc;

public class CreateEvent {
	private  WebDriver		driver;
	
	private static By saveAndContinueBtnIdLocator 	= By.id("save-continue");
	
	private static String eventName 			    = "name";
	private static String datePicker			    = "//*[@id='createMicrositeForm']/img"; 
	private static String dateValueXpath		    = "//*[@id='eventDate']";
	private static String addTimeButton			    = "//*[@id='no-time-div']/a";
	private static String hours					    = "//*[@id='hours']";
	private static String minutes				    = "//*[@id='minutes']";
	private static String orgName				    = "nonprofitName";
	private static String eventDescriptionFrame	    = "//*[@id='cke_1_contents']/iframe";
	private static String eventDescription		    = "/html/body";
	private static String fundrasingGoal		    = "fundraise";
	private static String urlXpath				    = "//*[@id='subdomainId']";
	private static String urlValueName			    = "subdomain";
	private static String saveAndDone			    = "cm-continue";
	private static String errorClass			    = "error";
	                                                
	private static String orgNameValue	 		    = "Test organization";
	private static String descriptionValue		    = "Test description";
	private static String fundraisingGoalValue	    = "5000";
	private static String hoursValue			    = "12";
	private static String minutesValue			    = "30";
	                                                
	private static String appearanceTab			    = "//*[@id='ui-accordion-accordion-header-1']";
	private static String navigationPagesTab	    = "//*[@id='ui-accordion-accordion-header-2']";
	private static String ticketsAndItemsTab	    = "//*[@id='ui-accordion-accordion-header-3']";
	
	

	
	public CreateEvent(WebDriver driver) {
		this.driver = driver;
	}
	
	public WebsiteAppearance clickAppearance() {
		DriverUtils.click(By.xpath(appearanceTab), driver);
		utils.WaitTool.waitForJQueryProcessing(driver, 5);
		return new WebsiteAppearance(driver);
	}
	
	public NavigationPages clickNavigationPages() {
		DriverUtils.click(By.xpath(navigationPagesTab), driver);
		utils.WaitTool.waitForJQueryProcessing(driver, 5);
		return new NavigationPages(driver);
	}
	
	public Items clickTicketsAndItems() {
		DriverUtils.click(By.xpath(ticketsAndItemsTab), driver);
		utils.WaitTool.waitForJQueryProcessing(driver, 5);
		return new Items(driver);
	}
	
	public void fillEventData() {
		driver.findElement(By.id(eventName)).sendKeys(utils.Data.createdMsName);
		driver.findElement(By.id(orgName)).sendKeys(orgNameValue);
		WebElement fundraisingGoal = driver.findElement(By.id(fundrasingGoal));
		fundraisingGoal.clear();
		fundraisingGoal.sendKeys(fundraisingGoalValue);
}
	
	public void fillEventData(String eventNameValue,String fundGoal) {
		driver.findElement(By.id(eventName)).sendKeys(eventNameValue);
		driver.findElement(By.id(orgName)).sendKeys(orgNameValue);
		driver.findElement(By.id(fundrasingGoal)).clear();
		driver.findElement(By.id(fundrasingGoal)).sendKeys(fundGoal);
		driver.findElement(By.id("subdomainId")).clear();
		String randomString = UUID.randomUUID().toString();
		driver.findElement(By.id("subdomainId")).sendKeys(randomString);
	}
	
	public void fillEventDataWithSpecificSundomain(String eventNameValue,String fundGoal, String subdomain) {
		driver.findElement(By.id(eventName)).sendKeys(eventNameValue);
		driver.findElement(By.id(orgName)).sendKeys(orgNameValue);
		driver.findElement(By.id(fundrasingGoal)).clear();
		driver.findElement(By.id(fundrasingGoal)).sendKeys(fundGoal);
		driver.findElement(By.id("subdomainId")).clear();
		driver.findElement(By.id("subdomainId")).sendKeys(subdomain);
	}
	
	public void fillEventDescription() {
		driver.switchTo().frame(driver.findElement(By.xpath(eventDescriptionFrame)));
		driver.findElement(By.xpath(eventDescription)).sendKeys(descriptionValue);
		driver.switchTo().defaultContent();
	}
	
	public EventEdit clickSaveAndDone() {
		DriverUtils.click(By.id(saveAndDone), driver);
		return new EventEdit(driver);
	}
	
	public EventEdit clickSaveAndDoneFromAppearancePage() {
		List<WebElement> allButtons = driver.findElements(By.id(saveAndDone));
		DriverUtils.clickElement(allButtons.get(1), driver);
		return new EventEdit(driver);
	}
	
	public void clickSaveAndContinue() {
		DriverUtils.click(saveAndContinueBtnIdLocator, driver);
		utils.WaitTool.waitForJQueryProcessing(driver, 5);
	}
	
	public void setEventDate() {
		DriverUtils.click(By.xpath(datePicker), driver);
		utils.WaitTool.waitForJQueryProcessing(driver, 5);	
		driver.findElement(By.linkText(Misc.getCurrentDate("Day"))).click();
	}
	
	public void setUrl(String urlValue) {
		driver.findElement(By.xpath(urlXpath)).clear();
		driver.findElement(By.xpath(urlXpath)).sendKeys(urlValue);
	}
	
	public void setTime() {
		DriverUtils.click(By.xpath(addTimeButton), driver);
		utils.WaitTool.waitForJQueryProcessing(driver, 5);	
		driver.findElement(By.xpath(hours)).sendKeys(hoursValue);
		driver.findElement(By.xpath(minutes)).sendKeys(minutesValue);
		utils.WaitTool.waitForJQueryProcessing(driver, 5);	
	}
	
	public int getErrorCount() {
		List<WebElement> errors = driver.findElements(By.className(errorClass));
		return errors.size();
	}
	
	public String getEventName() {
		return driver.findElement(By.id(eventName)).getAttribute("value");
	}
	
	public String getOrgName() {
		return driver.findElement(By.id(orgName)).getAttribute("value");
	}
	
	public String getEventDate() {
		return driver.findElement(By.xpath(dateValueXpath)).getAttribute("value");
	}
	
	public String getHours() {
		return driver.findElement(By.xpath(hours)).getAttribute("value");
	}
	
	public String getMinutes() {
		return driver.findElement(By.xpath(minutes)).getAttribute("value");
	}
	
	public String getEventDescription() {
		driver.switchTo().frame(driver.findElement(By.xpath(eventDescriptionFrame)));
		String description = driver.findElement(By.xpath(eventDescription)).getText();
		driver.switchTo().defaultContent();
		return description;
	}
	
	public String getFundraisingGoal() {
		String goal = driver.findElement(By.id(fundrasingGoal)).getAttribute("value");
		goal = goal.substring(0,goal.indexOf("."));
		goal = goal.replaceAll(",", "");
		return goal;
	}
	
	public String getFundraisingGoal(String type) {
		String goal = driver.findElement(By.id(fundrasingGoal)).getAttribute("value");
		goal = goal.replaceAll(",", "");
		return goal;
	}
	
	public String getUrl() {
		return driver.findElement(By.name(urlValueName)).getAttribute("value");
	}
	
	public void setEventInfo(){
		this.fillEventDescription();
		this.setTime();
		this.setEventDate();
		this.fillEventData("copyEventWithView","150");
	}
	
	//Asserts
	public void checkCreatedEvent() {
		Assert.assertTrue(getEventName().equals(utils.Data.createdMsName), "Wrong event name");
		Assert.assertTrue(getOrgName().equals(orgNameValue),"Wrong organization name");
		Assert.assertTrue(getEventDescription().equals(descriptionValue),"Wrong event description");
		Assert.assertTrue(getFundraisingGoal().equals(fundraisingGoalValue),"Wrong fundraising goal");
		Assert.assertTrue(getEventDate().equals(Misc.getCurrentDate("EventFormat")),"Wrong event date");
		Assert.assertTrue(getHours().equals(hoursValue), "Wrong hours value");
		Assert.assertTrue(getMinutes().equals(minutesValue), "Wrong minutes value");	
	}
	
	public void customCheckCreatedEvent(String eventNameValue, String goalValue) {
		Assert.assertTrue(getEventName().contains(eventNameValue), "Wrong event name");
		Assert.assertTrue(getOrgName().equals(orgNameValue),"Wrong organization name");
		Assert.assertTrue(getFundraisingGoal().equals(goalValue),"Wrong fundraising goal");
		Assert.assertTrue(getEventDate().equals(Misc.getCurrentDate("EventFormat")),"Wrong event date");
		Assert.assertTrue(getHours().equals(hoursValue), "Wrong hours value");
		Assert.assertTrue(getMinutes().equals(minutesValue), "Wrong minutes value");	
	}
	
	public void checkFundraisingGoal(String fundrasiingGoal) {
		Assert.assertTrue(getFundraisingGoal("Fractional").equals(fundrasiingGoal),"Wrong fundraising goal");
	}
	
	public void checkUrl(String urlValue) {
		Assert.assertTrue(getUrl().equals(urlValue),"Wrong url");
	}
	
	public void checkErrorCount() {
		Assert.assertTrue(getErrorCount() == 6, "Something wrong with validation");
	}
}
