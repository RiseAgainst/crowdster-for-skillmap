package eventDashboardPages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import utils.DriverUtils;
import utils.WaitTool;

public class WebsiteAppearance {
	private  WebDriver		driver;
	private static By saveAndContinueBtnIdLocator 			= By.id("save-continue-appearance");
	
	private static String backgroundColor 					= "//*[@id='buttonBackgroundColor-ap-field']";
	private static String uploadHtmlBannerFrame				= "//*[@id='cke_2_contents']/iframe";
	private static String navigationBackgroundColor			= "//*[@id='navBackgroundColor-ap-field']";
	private static String navBackgroundColorMiniSwitcher	= "//*[@id='siteConfigForm']/span[6]/span[1]/span";
	private static String buttonBackgroundColor				= "buttonItemBackgroundColor";
	private static String buttonBackgroundColorMiniSwitcher	= "//*[@id='siteConfigForm']/span[8]/span[1]/span";
	private static String htmlBanner						= "/html/body";
	
	
	public WebsiteAppearance(WebDriver driver) {
		this.driver = driver;
	}
	
	public void clickAppearance(){
		DriverUtils.click(By.id("ui-accordion-accordion-header-1"), driver);
		WaitTool.waitForJQueryProcessing(driver, 5);
	}
	
	public void clickSaveAndContinue() {
		DriverUtils.click(saveAndContinueBtnIdLocator, driver);
		utils.WaitTool.waitForJQueryProcessing(driver, 5);
	}
	
	public void setBackgroundColor(String color) {
		driver.findElement(By.xpath(backgroundColor)).sendKeys(color);
		utils.WaitTool.waitForJQueryProcessing(driver, 5);
	}
	
	public void setNavigationBackgroundColor(String color) {
		driver.findElement(By.xpath(navigationBackgroundColor)).sendKeys(color);
		utils.WaitTool.waitForJQueryProcessing(driver, 5);
		DriverUtils.click(By.xpath(navBackgroundColorMiniSwitcher), driver);
		utils.WaitTool.waitForJQueryProcessing(driver, 5);
	}
	
	public void setButtonBackgroundColor(String color) {
		driver.findElement(By.name(buttonBackgroundColor)).sendKeys(color);
		utils.WaitTool.waitForJQueryProcessing(driver, 5);
		DriverUtils.click(By.xpath(buttonBackgroundColorMiniSwitcher), driver);
		utils.WaitTool.waitForJQueryProcessing(driver, 5);
	}
	
	public void setHtmlBanner(String htmlBannerValue) {
		driver.switchTo().frame(driver.findElement(By.xpath(uploadHtmlBannerFrame)));
		driver.findElement(By.xpath(htmlBanner)).sendKeys(htmlBannerValue);
		driver.switchTo().defaultContent();
	}
	
	public String getBackgroundColor() {
		return driver.findElement(By.xpath(backgroundColor)).getAttribute("value");
	}
	
	public String getNavigationBackgroundColor() {
		return driver.findElement(By.xpath(navigationBackgroundColor)).getAttribute("value");
	}
	
	public String getButtonBackgroundColor() {
		return driver.findElement(By.name(buttonBackgroundColor)).getAttribute("value");
	}
	
	public String getHtmlBannerValue() {
		driver.switchTo().frame(driver.findElement(By.xpath(uploadHtmlBannerFrame)));
		String bannerValue = driver.findElement(By.xpath(htmlBanner)).getText();
		driver.switchTo().defaultContent();
		return bannerValue;
	}
	
	public void setAppearance(){
		this.setBackgroundColor(utils.Data.blue);
		this.setNavigationBackgroundColor(utils.Data.red);
		this.setButtonBackgroundColor(utils.Data.yellow);
	}
	
	//Asserts
	public void checkBackgroundColors(String backgroundColorVal, String navBackgroundColorVal, String buttonBackgroundVal) {
		Assert.assertTrue(getBackgroundColor().equals(backgroundColorVal), "Wrong background color");
		Assert.assertTrue(getNavigationBackgroundColor().equals(navBackgroundColorVal), "Wrong navigation background color");
		Assert.assertTrue(getButtonBackgroundColor().equals(buttonBackgroundVal), "Wrong button background color");
	}
	
	public void checkHtmlBanner(String htmlBannerValue) {
		Assert.assertTrue(getHtmlBannerValue().equals(htmlBannerValue), "Wrong html banner value");
	}
	
	

}
