package eventPages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;

import utils.DriverUtils;
import utils.WaitTool;

public class AppearanceDetails {
	private WebDriver				driver;
	private static final int 		WAIT_4_ELEMENT			= WaitTool.DEFAULT_WAIT_4_ELEMENT;
	
	//Locators
	private static By saveNavigationBtnIdLocator			= By.id("navigation-submit");
	private static By navBackgroundColorInputIdLocator 		= By.id("navBackgroundColor-ap-field");
	private static By eventTopNavCssLocator					= By.cssSelector("table.siteTopNav");
	private static By navTextColorInputNameLocator			= By.name("navTextColor");
	private static By navigationLabelsLinkCssLocator		= By.cssSelector("#sitepmenu a");
	private static By navigationLabelsLiCssLocator			= By.cssSelector("#sitepmenu li");
	private static By navItemBackgrColorInputIdLocator		= By.id("navItemBackgroundColor-ap-field");
	private static By navTextColorOnNameLocator				= By.name("navTextColorOn");
	private static By navItemBackgroundColorOnIdLocator		= By.id("navItemBackgroundColorOn-ap-field");
	private static By navTextColorHoverNameLocator			= By.name("navTextColorHover");
	private static By navItemBackgroundColorIdLocator		= By.id("navItemBackgroundColorHover-ap-field");
	private static By selectedLabelCssLocator 				= By.cssSelector("a.onThis");
	private static By siteMenuIdLocator 					= By.id("sitepmenu");
	private static By h1ElementCssLocator 					= By.cssSelector("h1");
	private static By subMenuCsslocator						= By.cssSelector(".appearance-details-sub-menus");
	private static By buttonTextColorNameLocator			= By.name("buttonTextColor");
	private static By eventButtonElementCssLocator			= By.cssSelector(".m-layout-reg-button");
	private static By buttonBackgroundColorIdLocator		= By.id("buttonBackgroundColor-ap-field");
	private static By headerTextColorNameLocator			= By.name("widgetHeaderTextColor");
	private static By widgetHeaderElementCssLocator			= By.cssSelector(".m-side-container-heading");
	private static By widgetHeaderBackgrColorNameLocator	= By.name("widgetHeaderBackgroundColor");
	private static By innerBackgroundColorNameLocator		= By.name("mainBackgroundColor");
	private static By eventMainBody							= By.id("siteMain");
	private static By saveButtonsIdLocator					= By.id("appBtn");
	private static By saveWidgetIdLocator					= By.id("widgetBtn");
	private static By saveInnerBackgroundIdLocator			= By.id("detailsBtn");
	private static By footerBackgroundColorNameLocator		= By.name("footerBackgroundColor");
	private static By footerSaveBtnIdLocator				= By.id("footerBtn");							
	private static By eventFooterCssLocator					= By.cssSelector(".site-footer");
	private static By footerTextColorNameLocator			= By.name("footerTextColor");
	private static By searchBoxTextColorNameLocator			= By.name("searchBoxTextColor");
	private static By searchBoxSaveBtnIdLocator				= By.id("sendboxBtn");
	private static By searchInputNameLocator				= By.name("searchInput");
	private static By searchBoxBackgroundColorNameLocator	= By.name("searchBoxBackgroundColor");
	private static By loginTextColorNameLocator				= By.name("loginTextColor");
	private static By saveLoginIdLocator					= By.id("loginBtn");
	private static By eventLoginBtnIdLocator				= By.id("loginButton");
	private static By buttonTextColorHoverNameLocator		= By.name("buttonTextColorHover");
	private static By buttonBackgrColorHoverIdLocator		= By.id("buttonBackgroundColorHover-ap-field");
	private static By labelCssLocator						= By.cssSelector(".form-field-label");
	
	//Xpath Locators
	private static By navBackgrColorSwatchXpathLocator 		= By.xpath("/html/body/div[4]/div[1]/div[3]/div/div[2]/div/div/form/div/span[2]/span[1]/span");
	private static By navTextColorSwatchXpathLocator		= By.xpath("/html/body/div[4]/div[1]/div[3]/div/div[2]/div/div/form/div/span[4]/span[1]/span");
	private static By navItemBackgrColorSwatchXpathLocator	= By.xpath("/html/body/div[4]/div[1]/div[3]/div/div[2]/div/div/form/div/span[6]/span[1]/span");
	private static By navItemTextColorOnXpathLocator		= By.xpath("/html/body/div[4]/div[1]/div[3]/div/div[2]/div/div/form/div/span[8]/span[1]/span");
	private static By navItemBackgrColorOnXpathLocator		= By.xpath("/html/body/div[4]/div[1]/div[3]/div/div[2]/div/div/form/div/span[10]/span[1]/span");
	private static By navTextColorHoverXpathLocator 		= By.xpath("/html/body/div[4]/div[1]/div[3]/div/div[2]/div/div/form/div/span[12]/span[1]/span");
	private static By navItemBackgrColorHoverXpathLocator	= By.xpath("/html/body/div[4]/div[1]/div[3]/div/div[2]/div/div/form/div/span[14]/span[1]/span");
	private static By buttonTextColorSwatchXpathLocator		= By.xpath("/html/body/div[4]/div[1]/div[3]/div/div[2]/div/div/form/div/span[2]/span[1]/span");
	private static By buttonBackColorSwatchXpathLocator		= By.xpath("/html/body/div[4]/div[1]/div[3]/div/div[2]/div/div/form/div/span[4]/span[1]/span");
	private static By widgetHeaderTxtColorSwatchXpahLocator	= By.xpath("/html/body/div[4]/div[1]/div[3]/div/div[2]/div/div/form/div/span[2]/span[1]/span");
	private static By widgetHeaderBackgrColorSwatchXpathLoc	= By.xpath("/html/body/div[4]/div[1]/div[3]/div/div[2]/div/div/form/div/span[4]/span[1]/span");
	private static By innerBackgroundSwatchXpathLocator		= By.xpath("/html/body/div[4]/div[1]/div[3]/div/div[2]/div/div/form/div/span[2]/span[1]/span");
	private static By footerBackgrColorSwatchXpathLocator	= By.xpath("/html/body/div[4]/div[1]/div[3]/div/div[2]/div/div/form/div/span[2]/span[1]/span");
	private static By footerTextColorSwatchXpathLocator		= By.xpath("/html/body/div[4]/div[1]/div[3]/div/div[2]/div/div/form/div/span[4]/span[1]/span");
	private static By searchBoxTextColorSwatchXpathLocator	= By.xpath("/html/body/div[4]/div[1]/div[3]/div/div[2]/div/div/form/div/span[2]/span[1]/span");
	private static By searchBoxBackgrColorSwatchXpathLocator= By.xpath("/html/body/div[4]/div[1]/div[3]/div/div[2]/div/div/form/div/span[4]/span[1]/span");
	private static By loginSwatchXpathLocator				= By.xpath("/html/body/div[4]/div[1]/div[3]/div/div[2]/div/div/form/div/span[2]/span[1]/span");
	private static By buttonTextColorHoverXpathLocator		= By.xpath("/html/body/div[4]/div[1]/div[3]/div/div[2]/div/div/form/div/span[6]/span[1]/span");
	private static By buttonBackgrColorHoverSwatchXpathLoc	= By.xpath("/html/body/div[4]/div[1]/div[3]/div/div[2]/div/div/form/div/span[8]/span[1]/span");
	
	private String styleElement								= "style";
	private String navBackgroundColor						= "#db7777";
	private String navTextColor								= "#2f2761";
	private String navItemBackgroundColor					= "#dff554";
	private String navTextColorOn							= "#3ae8c5";
	private String navItemBackgroundColorOn					= "#ff0000";
	private String navTextColorHover						= "#000000";
	private String navItemBackgroundColorHover				= "#ffffff";
	private String buttonTextColor							= "#4848e0";
	private String buttonBackgroundColor					= "#eb6767";
	private String widgetHeaderTextColor					= "#a92dad";
	private String widgetHeaderBackgroundColor				= "#080202";
	private String innerBackgroundColor						= "#f57d2c";
	private String footerBackgroundColor					= "#081f5e";
	private String footerTextColor							= "#dfe653";
	private String searchBoxTextColor						= "#ed5757";
	private String searchBoxBackgroundColor					= "#292394";
	private String loginTextColor							= "#1c0303";
	private String buttonTextColorHover						= "#b8f500";
	private String buttonBackgroudColorHover				= "#000000";
	
	public AppearanceDetails(WebDriver driver){
		this.driver = driver;
	}
	
	public void clickNavigationSaveBtn(){
		DriverUtils.click(saveNavigationBtnIdLocator, driver);
	}
	
	public void clickButtonsSaveBtn(){
		DriverUtils.click(saveButtonsIdLocator, driver);
	}
	
	public void clickWidgetSaveBtn(){
		DriverUtils.click(saveWidgetIdLocator, driver);
	}
	
	public void clickInnerBackgroundSaveBtn(){
		DriverUtils.click(saveInnerBackgroundIdLocator, driver);
	}
	
	public void clickFooterSaveBtn(){
		DriverUtils.click(footerSaveBtnIdLocator, driver);
	}
	
	public void clickSearchBoxSaveBtn(){
		DriverUtils.click(searchBoxSaveBtnIdLocator, driver);
	}
	
	public void clickLoginSaveBtn(){
		DriverUtils.click(saveLoginIdLocator, driver);
	}
	
	public void clickLabel(){
		DriverUtils.click(labelCssLocator, driver);
	}
	
	public void clickNavigationItemOn(){
		List<WebElement> naviLabelsElements = WaitTool.waitForListElementsPresent(driver, navigationLabelsLinkCssLocator, WAIT_4_ELEMENT);
		DriverUtils.clickElement(naviLabelsElements.get(1), driver);
		WaitTool.waitForJQueryProcessing(driver, 5);
	}
	
	public void clickButtonsTab(){
		this.clickTab(1);
	}
	
	public void clickWidgetTab(){
		this.clickTab(2);
	}
	
	public void clickInnerBackgroundTab(){
		this.clickTab(3);
	}
	
	public void clickFooterTab(){
		this.clickTab(4);
	}
	
	public void clickSearchBoxTab(){
		this.clickTab(5);
	}
	
	public void clickLoginTab(){
		this.clickTab(6);
	}
	
	private void clickTab(int index ){
		WebElement subMenu = WaitTool.waitForElement(driver, subMenuCsslocator, WAIT_4_ELEMENT);
		List<WebElement> tabs = subMenu.findElements(By.cssSelector("li"));
		DriverUtils.clickElement(tabs.get(index), driver);
	}
	
	public String setNavigationBackgroundColor(){
		return this.setColor(navBackgroundColorInputIdLocator, navBackgroundColor, navBackgrColorSwatchXpathLocator);
	}
	
	public String setNavigationTextColor(){
		return this.setColor(navTextColorInputNameLocator, navTextColor, navTextColorSwatchXpathLocator);
	}
	
	public String setNavItemBackgroundColor(){
		return this.setColor(navItemBackgrColorInputIdLocator, navItemBackgroundColor, navItemBackgrColorSwatchXpathLocator);
	}
	
	public String setNavTextColorOn(){
		return this.setColor(navTextColorOnNameLocator, navTextColorOn, navItemTextColorOnXpathLocator);
	}
	
	public String setNavItemBackgroundColorOn(){
		return this.setColor(navItemBackgroundColorOnIdLocator, navItemBackgroundColorOn, navItemBackgrColorOnXpathLocator);
	}
	
	public String setNavTextColorHover(){
		return this.setColor(navTextColorHoverNameLocator, navTextColorHover, navTextColorHoverXpathLocator);
	}
	
	public String setNavItemBackgroundColorHover(){
		return this.setColor(navItemBackgroundColorIdLocator, navItemBackgroundColorHover, navItemBackgrColorHoverXpathLocator);
	}
	
	public String setButtonsTextColor(){
		return this.setColor(buttonTextColorNameLocator, buttonTextColor, buttonTextColorSwatchXpathLocator);
	}
	
	public String setButtonsBackgroundColor(){
		return this.setColor(buttonBackgroundColorIdLocator, buttonBackgroundColor, buttonBackColorSwatchXpathLocator);
	}
	
	public String setButtonsTextColorHover(){
		return this.setColor(buttonTextColorHoverNameLocator, buttonTextColorHover, buttonTextColorHoverXpathLocator);
	}
	
	public String setButtonsBackgroundColorHover(){
		return this.setColor(buttonBackgrColorHoverIdLocator, buttonBackgroudColorHover, buttonBackgrColorHoverSwatchXpathLoc);
	}
	
	public String setWidgetHeaderTextColor(){
		return this.setColor(headerTextColorNameLocator, widgetHeaderTextColor, widgetHeaderTxtColorSwatchXpahLocator);
	}
	
	public String setWidgetHeaderBackgroundColor(){
		return this.setColor(widgetHeaderBackgrColorNameLocator, widgetHeaderBackgroundColor, widgetHeaderBackgrColorSwatchXpathLoc);
	}
	
	public String setInnerBackgroundColor(){
		return this.setColor(innerBackgroundColorNameLocator, innerBackgroundColor, innerBackgroundSwatchXpathLocator);
	}
	
	public String setFooterBackgroundColor(){
		return this.setColor(footerBackgroundColorNameLocator, footerBackgroundColor, footerBackgrColorSwatchXpathLocator);
	}
	
	public String setFooterTextColor(){
		return this.setColor(footerTextColorNameLocator, footerTextColor, footerTextColorSwatchXpathLocator);
	}
	
	public String setSearchBoxTextColor(){
		return this.setColor(searchBoxTextColorNameLocator, searchBoxTextColor, searchBoxTextColorSwatchXpathLocator);
	}
	
	public String setSearchBoxBackgroundColor(){
		return this.setColor(searchBoxBackgroundColorNameLocator, searchBoxBackgroundColor, searchBoxBackgrColorSwatchXpathLocator);
	}
	
	public String setLoginTextColor(){
		return this.setColor(loginTextColorNameLocator, loginTextColor, loginSwatchXpathLocator);
	}
	
	private String setColor(By miniColorsInputLocator, String color, By miniColorsSwatchLocator){
		WebElement navBackgrColorElement = WaitTool.waitForElement(driver, miniColorsInputLocator, WAIT_4_ELEMENT);
		navBackgrColorElement.clear();
		navBackgrColorElement.sendKeys(color);
		String navBackgrColorRGB = WaitTool.waitForElement(driver, miniColorsSwatchLocator, WAIT_4_ELEMENT).getAttribute(styleElement);
		navBackgrColorRGB = navBackgrColorRGB.substring(navBackgrColorRGB.indexOf("(") + 1, navBackgrColorRGB.indexOf(")"));
		return navBackgrColorRGB;
	}
	
	//Asserts
	public void checkNavigationBackgroundColor(String navBackgrColorRGB){
		WebElement eventTopNavElement = WaitTool.waitForElement(driver, eventTopNavCssLocator, WAIT_4_ELEMENT);
		String backgroundColorRGB = eventTopNavElement.getCssValue("background-color");
		Assert.assertTrue(backgroundColorRGB.contains(navBackgrColorRGB), "Navigation background color is not correct.");
	}
	
	public void checkNavigationTextColor(String navTextColorRGB){
		List<WebElement> navigationLabelLinkElements = WaitTool.waitForListElementsPresent(driver, navigationLabelsLinkCssLocator, WAIT_4_ELEMENT);
		String navigationLabelTextColorRGB = navigationLabelLinkElements.get(1).getCssValue("color");
		Assert.assertTrue(navigationLabelTextColorRGB.contains(navTextColorRGB), "Navigation label text color is not correct.");
	}
	
	public void checkNavigationItemBackgroundColor(String navItemBackgrColorRGB){
		List<WebElement> navigationLabelLiElements = WaitTool.waitForListElementsPresent(driver, navigationLabelsLiCssLocator, WAIT_4_ELEMENT);
		String navigationLabelBackgroundColorRGB = navigationLabelLiElements.get(1).getCssValue("background-color");
		Assert.assertTrue(navigationLabelBackgroundColorRGB.contains(navItemBackgrColorRGB), "Navigation Item background color is not correct.");
	}
	
	public void checkNavigationTextColorOn(String navTextColorOnRGB){
		DriverUtils.click(h1ElementCssLocator, driver);
		WebElement navigationLabelLiElement = WaitTool.waitForElement(driver, siteMenuIdLocator, WAIT_4_ELEMENT);
		WebElement navSelectedLabel = navigationLabelLiElement.findElement(selectedLabelCssLocator);
		String navigationTextColorOnRGB = navSelectedLabel.getCssValue("color");
		Assert.assertTrue(navigationTextColorOnRGB.contains(navTextColorOnRGB), "Navigation text color on is not correct.");
	}
	
	public void checkNavigationItemBackgroundColorOn(String navItemBackgrColorOnRGB){
		DriverUtils.click(h1ElementCssLocator, driver);
		WebElement navigationLabelLiElement = WaitTool.waitForElement(driver, siteMenuIdLocator, WAIT_4_ELEMENT);
		WebElement navSelectedLabel = navigationLabelLiElement.findElement(selectedLabelCssLocator);
		String navigationLabelBackgroundColorRGB = navSelectedLabel.getCssValue("background-color");
		Assert.assertTrue(navigationLabelBackgroundColorRGB.contains(navItemBackgrColorOnRGB), "Navigation Item background color on is not correct.");
	}
	
	public void checkNavigationTextColorHover(String navTextColorHover){
		List<WebElement> navigationLabelLiElements = WaitTool.waitForListElementsPresent(driver, navigationLabelsLiCssLocator, WAIT_4_ELEMENT);
		String navigationTextColorOnRGB = navigationLabelLiElements.get(1).getCssValue("color");
		Assert.assertTrue(navigationTextColorOnRGB.contains(navTextColorHover), "Navigation text color hover is not correct.");
	}
	
	public void checkNavigationItemBackgroundColorHover(String navItemBackgroundColorRGB){
		List<WebElement> navigationLabelLiElements = WaitTool.waitForListElementsPresent(driver, navigationLabelsLinkCssLocator, WAIT_4_ELEMENT);
		WebElement navigationLabelElement = navigationLabelLiElements.get(1);
		Actions builder = new Actions(driver);
		builder.moveToElement(navigationLabelElement).perform();	
		String navigationTextColorOnRGB = navigationLabelElement.getCssValue("background-color");
		Assert.assertTrue(navigationTextColorOnRGB.contains(navItemBackgroundColorRGB), "Navigation background color hover is not correct.");
	}
	
	public void checkButtonTextColor(String buttonTextColorRGB){
		WebElement buttonElement = WaitTool.waitForElement(driver, eventButtonElementCssLocator, WAIT_4_ELEMENT);
		String buttonTextColor = buttonElement.getCssValue("color");
		Assert.assertTrue(buttonTextColor.contains(buttonTextColorRGB), "Button text color is not correct.");
	}
	
	public void checkButtonBackgroindColor(String buttonBackgColorRGB){
		WebElement buttonElement = WaitTool.waitForElement(driver, eventButtonElementCssLocator, WAIT_4_ELEMENT);
		String buttonBackgroundColor = buttonElement.getCssValue("background-color");
		Assert.assertTrue(buttonBackgroundColor.contains(buttonBackgColorRGB), "Button background color is not correct.");
	}
	
	public void checkButtonTextColorHover(String buttonTextColorHoverRGB){
		WebElement buttonElement = WaitTool.waitForElement(driver, eventButtonElementCssLocator, WAIT_4_ELEMENT);
		Actions builder = new Actions(driver);
		builder.moveToElement(buttonElement).perform();	
		String buttonTextColorHover = buttonElement.getCssValue("color");
		Assert.assertTrue(buttonTextColorHover.contains(buttonTextColorHoverRGB), "Button text color hover is not correct.");
	}
	
	public void checkButtonBackgroundColorHover(String backgroundColorHoverRGB){
		WebElement buttonElement = WaitTool.waitForElement(driver, eventButtonElementCssLocator, WAIT_4_ELEMENT);
		Actions builder = new Actions(driver);
		builder.moveToElement(buttonElement).perform();	
		String buttonBackgroundColorHover = buttonElement.getCssValue("background-color");
		Assert.assertTrue(buttonBackgroundColorHover.contains(backgroundColorHoverRGB), "Button background color hover is not correct.");
	}
	
	public void checkWidgetHeaderTextColor(String widgetHeaderTextColorRGB){
		WebElement widgetHeader = WaitTool.waitForElement(driver, widgetHeaderElementCssLocator, WAIT_4_ELEMENT);
		String widgetHeaderTextColor = widgetHeader.getCssValue("color");
		Assert.assertTrue(widgetHeaderTextColor.contains(widgetHeaderTextColorRGB), "Widget header text color is not correct.");
	}
	
	public void checkWidgetHeaderBackgroundColor(String widgetBackgroundColorRGB){
		WebElement widgetHeader = WaitTool.waitForElement(driver, widgetHeaderElementCssLocator, WAIT_4_ELEMENT);
		String widgetHeaderBackgroundColor = widgetHeader.getCssValue("background-color");
		Assert.assertTrue(widgetHeaderBackgroundColor.contains(widgetBackgroundColorRGB), "Widget header background color is not correct.");
	}
	
	public void checkInnerBackgroundColor(String innerBackgrounColorRGB){
		String innerBackgroundColor = WaitTool.waitForElement(driver, eventMainBody, WAIT_4_ELEMENT).getCssValue("background-color");
		Assert.assertTrue(innerBackgroundColor.contains(innerBackgrounColorRGB), "Inner background color is not correct.");
	}
	
	public void checkFooterBackgroundColor(String footerBackgroundColorRGB){
		String footerBackgroundColor = WaitTool.waitForElement(driver, eventFooterCssLocator, WAIT_4_ELEMENT).getCssValue("background-color");
		Assert.assertTrue(footerBackgroundColor.contains(footerBackgroundColorRGB), "Footer background color is not correct.");
	}
	
	public void checkFooterTextColor(String footerColorRGB){
		String footerTextColor = WaitTool.waitForElement(driver, eventFooterCssLocator, WAIT_4_ELEMENT).getCssValue("color");
		Assert.assertTrue(footerTextColor.contains(footerColorRGB), "Footer text color is not correct.");
	}
	
	public void checkSearchBoxTextColor(String searchBoxTextColorRGB){
		String searchBoxTextColor = WaitTool.waitForElement(driver, searchInputNameLocator, WAIT_4_ELEMENT).getCssValue("color");
		Assert.assertTrue(searchBoxTextColor.contains(searchBoxTextColorRGB), "Search input text color is not correct.");
	}
	
	public void checkSearchBoxBackgroundColor(String backgroundColorRGB){
		String searchBoxBackgroundColor = WaitTool.waitForElement(driver, searchInputNameLocator, WAIT_4_ELEMENT).getCssValue("background-color");
		Assert.assertTrue(searchBoxBackgroundColor.contains(backgroundColorRGB), "Search input background color is not correct.");
	}
	
	public void checkLoginTextColor(String loginTextColorRGB){
		String loginTextColor = WaitTool.waitForElement(driver, eventLoginBtnIdLocator, WAIT_4_ELEMENT).getCssValue("color");
		Assert.assertTrue(loginTextColor.contains(loginTextColorRGB), "Login text color is not correct.");
	}
	
}
