package eventPages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import utils.DriverUtils;
import utils.FileUploader;
import utils.WaitTool;

public class Appearance {
	private WebDriver				driver;
	private FileUploader			fileUploader;
	private static final int 		WAIT_4_ELEMENT		= WaitTool.DEFAULT_WAIT_4_ELEMENT;
	
	public static By saveBtnLocator						= By.id("siteConfigBtn");
	public static By backgroundColorIdLocator			= By.id("backgroundColor");
	public static By eventBodyCssLocator				= By.cssSelector("body");
	public static By minicolorsCssLocator				= By.cssSelector(".minicolors-swatch > span");
	public static By newFileUploadIdLocator				= By.id("file-addToLib");
	public static By selectFileNameLocator				= By.name("file");
	public static By uploadmediaSubmitIdLocator			= By.id("uploadmediaSubmit");
	public static By lockImageIdLocator					= By.id("checkbox-lockImage");
	public static By imageHorizontallyIdLocator			= By.id("checkbox-imageHorizontally");
	public static By imageVerticallyIdLocator			= By.id("checkbox-imageVertically");
	public static By eventFooterCssLocator				= By.cssSelector(".site-footer");
	
	public static String backgroundColor				= utils.Data.green;
	public static String styleAttribute					= "style";
	public static String testLogoImageName				= utils.Data.testLogoImageName;
	public static String footerText						= "Footer text test. Footer text test. Footer text test.";
	
	public Appearance(WebDriver driver){
		this.driver = driver;
		this.fileUploader = new FileUploader(driver);
	}
	
	public void clickSaveBtn(){
		DriverUtils.click(saveBtnLocator, driver);
	}
	
	public String setBackgroundColor(){
		WebElement backgroundColorElement = WaitTool.waitForElement(driver, backgroundColorIdLocator, WAIT_4_ELEMENT);
		backgroundColorElement.clear();
		backgroundColorElement.sendKeys(backgroundColor);
		WebElement minicolorsElement = WaitTool.waitForElement(driver, minicolorsCssLocator, WAIT_4_ELEMENT);
		String backgroundColor = minicolorsElement.getAttribute("style");
		return backgroundColor;
	}
	
	public void clickUploadNewFile(){
		DriverUtils.click(newFileUploadIdLocator, driver);
	}
	
	public void selectNewFile(){
		this.fileUploader.uploadFile(testLogoImageName, selectFileNameLocator);
	}
	
	public void clickSubmitUploadNewFile(){
		DriverUtils.click(uploadmediaSubmitIdLocator, driver);
	}
	
	public void clickLockBackgroundImage(){
		DriverUtils.click(lockImageIdLocator, driver);
	}
	
	public void clickTileBackgroundImageHorizontally(){
		this.clickLockBackgroundImage(); // lock off
		WaitTool.waitForJQueryProcessing(driver, 5);
		DriverUtils.click(imageHorizontallyIdLocator, driver);
	}
	
	public void clickTileBackgroundImageVertically(){
		DriverUtils.click(imageVerticallyIdLocator, driver);
	}
	
	public void fillFooterText(){
		WebElement footerTextElement = WaitTool.waitForElement(driver, eventBodyCssLocator, WAIT_4_ELEMENT);
		footerTextElement.clear();
		footerTextElement.sendKeys(footerText);
	}
	
	public void fillEventFooterText(){
		WebElement footerTextElement = WaitTool.waitForElement(driver, eventBodyCssLocator, WAIT_4_ELEMENT);
		footerTextElement.sendKeys(footerText);
	}
	
	private String getBodyStyleAttribute(){
		WebElement bodyElement = WaitTool.waitForElement(driver, eventBodyCssLocator, WAIT_4_ELEMENT);
		return bodyElement.getAttribute(styleAttribute);
	}
	
	//Asserts
	public void checkBackgroundColor(String appearanceBackgroundClolor){
		Assert.assertTrue(getBodyStyleAttribute().equals(appearanceBackgroundClolor), "Background color is not correct.");
	}
	
	public void checkBackgroundImage(){
		String bodyStyle = getBodyStyleAttribute();
		String imageName = testLogoImageName.substring(0, testLogoImageName.indexOf("."));
		Assert.assertTrue(bodyStyle.contains("background-image: url"), "Background image is not displayed.");
		Assert.assertTrue(bodyStyle.contains(imageName), "Background image name is not the same.");
	}
	
	public void checkLockBackgroundImage(){
		Assert.assertTrue(getBodyStyleAttribute().contains("background-size: cover;"), "Background image is not locked.");
	}
	
	public void checkTileBackgroundImageHorizontally(){
		Assert.assertTrue(getBodyStyleAttribute().contains("background-repeat: repeat-x;"), "Background image is not displayed horizontally.");
	}
	
	public void checkTileBackgroundImageVertically(){
		Assert.assertTrue(getBodyStyleAttribute().contains("background-repeat: repeat;"), "Background image is not displayed vertically.");
	}
	
	public void checkFooterText(){
		String eventFooterText = WaitTool.waitForElement(driver, eventFooterCssLocator, WAIT_4_ELEMENT).getText();
		Assert.assertTrue(eventFooterText.contains(footerText), "Footer text is not correct.");
	}
}
