package eventPages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import utils.DriverUtils;
import utils.WaitTool;

import org.openqa.selenium.support.Color;


public class Event {
	private WebDriver		driver;
	private static final int WAIT_4_ELEMENT 			= WaitTool.DEFAULT_WAIT_4_ELEMENT;
	
	private static By donateAmountInputNameLocator		= By.name("value");
	private static By donateBtnIdLocator				= By.id("event-donate-btn");
	private static By teamOwnerDonateBtnIdLocator		= By.id("team-owner-donate-btn");
	private static By homeBtnLinkTextLocator			= By.linkText(" Home ");
	private static By donationBtnIdLocator				= By.id("donation-btn-button");
	private static By registrationBtnIdLocator			= By.id("registration-btn-button");
	private static By sponsorBtnIdLocator				= By.id("sponsor-btn-button");
	private static By hyperlinkTagNameLocator			= By.tagName("a");
	private static By dashboardNavigationsCssLocator 	= By.cssSelector("ul.nav-box > li");
	private static By gettingStartedEeasyCssLocator		= By.cssSelector("ul.getting-started-easy > li");
	private static By createEventBtnIdLocator			= By.id("micro-lic-popup");
	
	private static String thermometrText		= "//*[@id='fundraising-1']/div/div[2]/div[1]/div[1]";
	private static String teamClass				= "team-tag";
	private static String eventName				= "//*[@id='siteMain']/div[2]/div[1]/h1";
	private static String eventDate				= "//*[@id='siteMain']/div[2]/div[1]/h2/span";
	private static String eventDescription		= "body";
	private static String backgroundXpath		= "/html/body";
	private static String navigationBar			= "siteTopNav";
	private static String mastheadBanner		= "banner-masthead-code";
	
	public Event(WebDriver driver) {
		this.driver = driver;
	}
	
	public EventDonation clickDonationButton() {
		DriverUtils.click(donationBtnIdLocator, driver);
		utils.WaitTool.waitForJQueryProcessing(driver, 5);
		return new EventDonation(driver);
	}
	
	public EventDonation clickRegistrationButton() {
		DriverUtils.click(registrationBtnIdLocator, driver);
		utils.WaitTool.waitForJQueryProcessing(driver, 5);
		return new EventDonation(driver);
	}
	
	public EventDonation clickSponsorButton() {
		DriverUtils.click(sponsorBtnIdLocator, driver);
		utils.WaitTool.waitForJQueryProcessing(driver, 5);
		return new EventDonation(driver);
	}
	
	public int getThermometrValue() {
		String thermometrTxt = driver.findElement(By.xpath(thermometrText)).getText();
		String value = thermometrTxt.substring(thermometrTxt.indexOf("$") + 1,thermometrTxt.indexOf("."));
		value = value.replaceAll(",","");
		return Integer.parseInt(value);
	}
	
	
	public void clickHomeButton() {
		DriverUtils.click(homeBtnLinkTextLocator, driver);
		utils.WaitTool.waitForJQueryProcessing(driver, 5);
	}
	
	public EventTeam goToTeamPage() {
		DriverUtils.click(By.className(teamClass), driver);
		return new EventTeam(driver);
	}
	
	public String getEventName() {
		return driver.findElement(By.xpath(eventName)).getText();
	}
	
	public String getEventDate() {
		String eventDateVal = driver.findElement(By.xpath(eventDate)).getText();
		eventDateVal = eventDateVal.substring(0,eventDateVal.indexOf("   "));
		eventDateVal = eventDateVal.replaceAll(" ", "");
		return eventDateVal;
	}
	
	public String getEventDescription() {
		return driver.findElement(By.className(eventDescription)).getText();
	}
	
	public String getBackgroundColor() {
		String rgbValue = driver.findElement(By.xpath(backgroundXpath)).getCssValue("background-color");
		return Color.fromString(rgbValue).asHex();
	}
	
	public String getNavigationBackgroundColor() {
		String rgbValue = driver.findElement(By.className(navigationBar)).getCssValue("background-color");
		return Color.fromString(rgbValue).asHex();
	}
	
	public String getButtonBackgroundColor() {
		String rgbValue =  driver.findElement(donationBtnIdLocator).getCssValue("background-color");
		return Color.fromString(rgbValue).asHex();
	}
	
	public String getMastheadBanner() {
		return driver.findElement(By.id(mastheadBanner)).getText();
	}
	
	public NavigationPage clickNavigationLabel(String labelName) {
		WebElement navBar = driver.findElement(By.className(navigationBar));
		List<WebElement> links = navBar.findElements(By.tagName("a"));
		for(WebElement e :links) {
			if(e.getText().contains(labelName)) {
				DriverUtils.clickElement(e, driver);
				break;
			}
		}
		return new NavigationPage(driver);
	}
	
	public void setTeamDonation(){
		WebElement donateAmountInput = WaitTool.waitForElement(driver, donateAmountInputNameLocator, WAIT_4_ELEMENT);
		donateAmountInput.clear();
		donateAmountInput.sendKeys(utils.Data.donationAmount);
		DriverUtils.click(donateBtnIdLocator, driver);
	}
	
	public void setTeamOwnerDonation(){
		WebElement donateAmountInput = WaitTool.waitForElement(driver, donateAmountInputNameLocator, WAIT_4_ELEMENT);
		donateAmountInput.clear();
		donateAmountInput.sendKeys(utils.Data.donationAmount);
		DriverUtils.click(teamOwnerDonateBtnIdLocator, driver);
	}	
	
	private boolean hasEventHoverDisplayOnlyViewAndEdit(WebElement eventElement){
		List<WebElement> eventHyperlinkElements = eventElement.findElements(hyperlinkTagNameLocator);
		for (WebElement webElement : eventHyperlinkElements) {
			if(webElement.getText().matches("Copy|Save as Template|Delete")){
				return false;
			}
		}
		return true;
	}
	
	private int getEventDashboardOptionsCount(){
		int count = 0;
		List<WebElement> dashboardNavigationElements = WaitTool.waitForListElementsPresent(driver, dashboardNavigationsCssLocator, WAIT_4_ELEMENT);
		for (WebElement webElement : dashboardNavigationElements) {
			if(webElement.isDisplayed()){
				count += 1;
			}
		}
		return count;
	}
	
	private boolean isGettingStartedEasyCountEqualsTo1(){
		List<WebElement> gettingStartedEasyElements =  WaitTool.waitForListElementsPresent(driver, gettingStartedEeasyCssLocator, WAIT_4_ELEMENT);
		if(gettingStartedEasyElements.size() == 1){
			return true;
		}
		return false;
	}
	
	private boolean notDisplayingCreateEventBtn(){
		WebElement createEventBtn = WaitTool.waitForElement(driver, createEventBtnIdLocator, WAIT_4_ELEMENT);
		if(createEventBtn == null){
			return true;
		}
		return false;
	}
	
	//Asserts
	public void checkDonateThermometr(int beforeDonation, int afterDonation) {
		Assert.assertTrue(beforeDonation + Integer.parseInt(utils.Data.donationAmount) == afterDonation,"Something went wrong with donation");
	}
	
	public void checkDonateThermometrAfterBuyingItems(int beforeDonation, int afterDonation, int itemsPrice) {
		Assert.assertTrue(beforeDonation + itemsPrice == afterDonation,"Something went wrong with donation");
	}
	
	public void checkEventGeneralInfo(String eventNameValue, String eventDateValue, String descriptionValue) {
		Assert.assertTrue(getEventName().equals(eventNameValue), "Wrong event name");
		Assert.assertTrue(getEventDate().equals(eventDateValue), "Wrong event date");
		Assert.assertTrue(getEventDescription().equals(descriptionValue), "Wrong description value");
	}
	
	public void checkBackgroundColors(String backgroungColor, String navigationBackgroundColor, String buttonBackgroundColor) {
		Assert.assertTrue(getBackgroundColor().equals(backgroungColor), "Wrong background color");
		Assert.assertTrue(getNavigationBackgroundColor().equals(navigationBackgroundColor), "Wrong navigation background color");
		Assert.assertTrue(getButtonBackgroundColor().equals(buttonBackgroundColor), "Wrong button background color");
	}
	
	public void checkMastheadBanner(String expectedValue) {
		Assert.assertTrue(getMastheadBanner().equals(expectedValue), "Wrong masthead banner");
	}
	
	public void checkEventHoverToDisplayOnlyViewAndEdit(WebElement eventElement){
		Assert.assertTrue(hasEventHoverDisplayOnlyViewAndEdit(eventElement), "Hovering over an event doesn't display only 'view' & 'edit' btns");
	}
	
	public void checkEventDashboardOnly4Options(){
		Assert.assertTrue(getEventDashboardOptionsCount() == 4, "Event dashboard navigations are not only 4.");
	}
	
	public void checkWhatYouCanDoBoxForReadOnlySpecificContent(){
		Assert.assertTrue(isGettingStartedEasyCountEqualsTo1(), "Not only one getting started easy content.");
		Assert.assertTrue(notDisplayingCreateEventBtn(), "Create event button is displaying.");
	}
}
