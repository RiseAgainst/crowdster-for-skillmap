package eventPages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import org.testng.Assert;

import utils.DriverUtils;
import utils.FileUploader;
import utils.WaitTool;
import utils.WebTable;

public class CheckoutItems {
	private WebDriver 			driver;
	private FileUploader 		fileUploader;
	private static final int 	WAIT_4_ELEMENT			= WaitTool.DEFAULT_WAIT_4_ELEMENT;
	
	private static By iframeElementCssLocator			= By.cssSelector("iframe");
	private static By bodyElementCssLocator				= By.cssSelector("body");
	private static By createItemBtnIdLocator			= By.id("checkout-create-item");
	private static By nameNameLocator					= By.name("name");
	private static By uploadNewFileIdLocator			= By.id("file-addToLib");
	private static By selectFileNameLocator				= By.name("file");
	private static By submitUploadFileIdLocator			= By.id("uploadmediaSubmit");
	private static By amountIdLocator					= By.id("amount");
	private static By saveBtnIdLocator					= By.id("saveBtn");
	private static By registrationBtnIdLocator			= By.id("registration-btn-button");
	private static By registrationTabIdLocator			= By.id("registration");
	private static By itemDescriptionIdLocator 			= By.id("checkout-item-description");
	private static By itemTitleIdLocator 				= By.id("checkout-item-title");
	private static By itemAmountIdLocator 				= By.id("checkout-item-amount");
	private static By itemMediaIdLocator 				= By.id("checkout-item-media");
	private static By itemSponsorDescriptionIdLocator 	= By.id("checkout-sponsor-description");
	private static By itemSponsorTitleIdLocator 		= By.id("checkout-sponsor-title");
	private static By itemSponsorAmountIdLocator 		= By.id("checkout-sponsor-amount");
	private static By itemSponsorMediaIdLocator 		= By.id("checkout-sponsor-media");
	private static By itemDonationDescriptionIdLocator 	= By.id("checkout-donation-description");
	private static By itemDonationTitleIdLocator 		= By.id("checkout-donation-title");
	private static By itemDonationAmountIdLocator 		= By.id("checkout-donation-amount");
	private static By itemDonationMediaIdLocator 		= By.id("checkout-donation-media");
	private static By itemCategoryNameSelector 			= By.name("category");
	private static By eventSponsorBtnIdLocator			= By.id("sponsor-btn-button");
	private static By eventSponsorTabIdLocator			= By.id("sponsor");
	private static By eventDonationBtnIdLocator			= By.id("donation-btn-button");
	private static By itemsTableIdLocator				= By.id("items-sort");
	private static By editTextLinkLocator				= By.linkText("Edit");
	private static By deleteTextLinkLocator				= By.linkText("Delete");
	private static By removeImgBtnCssLocator			= By.cssSelector(".remove-button");
	
	private static String randomLogofileName			= utils.Data.randomLogoImageName;
	private static String testLogofileName				= utils.Data.testLogoImageName;
	private static String itemTitleValue				= "Item name test";
	private static String itemDescriptionValue			= "Checkout item description test";
	private static String amountValue					= "50";

	private static String sponsorshipCategory			= "Sponsorship";
	private static String donationCategory				= "Donation";
	private static String editItemTitleValue			= "Item name edited test";
	private static String editItemDescriptionValue		= "Checkout item description edited test";
	private static String editAmountValue				= "70";
	
	public CheckoutItems(WebDriver driver){
		this.driver = driver;
		this.fileUploader = new FileUploader(driver);
	}
	
	public void clickSaveItemBtn(){
		DriverUtils.click(saveBtnIdLocator, driver);
	}
	
	public void clickCreateItemBtn(){
		DriverUtils.click(createItemBtnIdLocator, driver);
	}
	
	public void goToEventRegistration(){
		DriverUtils.click(registrationBtnIdLocator, driver);
		DriverUtils.click(registrationTabIdLocator, driver);
		WaitTool.waitForJQueryProcessing(driver, WAIT_4_ELEMENT);
	}
	
	public void goToEventSponsor(){
		DriverUtils.click(eventSponsorBtnIdLocator, driver);
		DriverUtils.click(eventSponsorTabIdLocator, driver);
		WaitTool.waitForJQueryProcessing(driver, WAIT_4_ELEMENT);
	}
	
	public void goToEventDonation(){
		DriverUtils.click(eventDonationBtnIdLocator, driver);
		WaitTool.waitForJQueryProcessing(driver, WAIT_4_ELEMENT);
	}
	
	public void createRegistrationItem(){
		this.createItem();
	}
	
	public void createSponsorshipItem(){
		DriverUtils.selectDropDownOptionByVisibleText(driver, itemCategoryNameSelector, sponsorshipCategory);
		this.createItem();

	}
	
	public void createDonationItem(){
		DriverUtils.selectDropDownOptionByVisibleText(driver, itemCategoryNameSelector, donationCategory);
		this.createItem();
	}
	
	private void createItem(){
		WaitTool.waitForElement(driver, nameNameLocator, WAIT_4_ELEMENT).sendKeys(itemTitleValue);
		this.fillDescription(itemDescriptionValue);
		this.uploadSelectItemImage(randomLogofileName);
		WaitTool.waitForElement(driver, amountIdLocator, WAIT_4_ELEMENT).sendKeys(amountValue);
		this.clickSaveItemBtn();
	}
	
	public void editItem(){
		WebElement itemTitleElement = WaitTool.waitForElement(driver, nameNameLocator, WAIT_4_ELEMENT);
		itemTitleElement.clear();
		itemTitleElement.sendKeys(editItemTitleValue);
		this.fillDescription(editItemDescriptionValue);
		DriverUtils.click(removeImgBtnCssLocator, driver);
		this.uploadSelectItemImage(testLogofileName);
		WebElement itemAmountElement = WaitTool.waitForElement(driver, amountIdLocator, WAIT_4_ELEMENT);
		itemAmountElement.clear();
		itemAmountElement.sendKeys(editAmountValue);
		this.clickSaveItemBtn();
	}
	
	private void fillDescription(String descriptionValue){
		this.switchToIframe();
		WebElement iframeBody = WaitTool.waitForElement(driver, bodyElementCssLocator, WAIT_4_ELEMENT);
		iframeBody.clear();
		iframeBody.sendKeys(descriptionValue);
		this.switchToDefaultContent();
	}
	
	private void uploadSelectItemImage(String fileName){
		DriverUtils.click(uploadNewFileIdLocator, driver);
		WaitTool.waitForJQueryProcessing(driver, 5);
		this.fileUploader.uploadFile(fileName, selectFileNameLocator);
		DriverUtils.click(submitUploadFileIdLocator, driver);
	}
	
	private void switchToIframe(){
		WebElement iFrame= WaitTool.waitForElement(driver, iframeElementCssLocator, WAIT_4_ELEMENT);
		driver.switchTo().frame(iFrame);
	}
	
	private void switchToDefaultContent(){
		driver.switchTo().defaultContent();
	}
	
	public void clickEditItem(){
		this.moveCursorToElement();
		DriverUtils.click(editTextLinkLocator, driver);
	}
	
	public void clickDeleteItem(){
		this.moveCursorToElement();
		DriverUtils.click(deleteTextLinkLocator, driver);
	}
	
	private void moveCursorToElement(){
		WebElement firstItem = WebTable.getFirstElementFromColumnAsWebElementByIdLocator(itemsTableIdLocator, 1, driver);
		Actions builder = new Actions(driver);
		builder.moveToElement(firstItem).perform();
	}
	
	public int getItemsCount(){
		List<WebElement> tableItems = WebTable.getElementsFromColumnByIdLocator(itemsTableIdLocator, 1, driver);
		return tableItems.size();
	}
	
	private void checkItem(By mediaItem, By amountItem, By titleItem, By descriptionItem, 
			String amountValueParam, String titleValueParam, String descriptionValueParam){
		WebElement imgElement = WaitTool.waitForElement(driver, mediaItem, WAIT_4_ELEMENT);
		String itemAmout = WaitTool.waitForElement(driver, amountItem, WAIT_4_ELEMENT).getText();
		itemAmout = itemAmout.substring(1, itemAmout.indexOf("."));
		String itemTile = WaitTool.waitForElement(driver, titleItem, WAIT_4_ELEMENT).getText();
		String itemDescription = WaitTool.waitForElement(driver, descriptionItem, WAIT_4_ELEMENT).getText();
		
		Assert.assertTrue(imgElement.isDisplayed(), "Image is not displayed.");
		Assert.assertTrue(itemAmout.equals(amountValueParam), "Wrong item amount.");
		Assert.assertTrue(itemTile.equals(titleValueParam), "Item title is not correct.");
		Assert.assertTrue(itemDescription.equals(descriptionValueParam), "Item Description is not correct");
	}
	
	//Asserts
	public void checkRegistrationItem(){
		this.checkItem(itemMediaIdLocator, itemAmountIdLocator, itemTitleIdLocator, itemDescriptionIdLocator, 
				amountValue, itemTitleValue ,itemDescriptionValue);
	}
	
	public void checkSponsorItem(){
		this.checkItem(itemSponsorMediaIdLocator, itemSponsorAmountIdLocator, itemSponsorTitleIdLocator, itemSponsorDescriptionIdLocator, 
				amountValue, itemTitleValue ,itemDescriptionValue);
	}
	
	public void checkDonationItem(){
		this.checkItem(itemDonationMediaIdLocator, itemDonationAmountIdLocator, itemDonationTitleIdLocator, itemDonationDescriptionIdLocator, 
				amountValue, itemTitleValue ,itemDescriptionValue);
	}
	
	public void checkRegistrationItemEdited(){
		this.checkItem(itemMediaIdLocator, itemAmountIdLocator, itemTitleIdLocator, itemDescriptionIdLocator, 
				editAmountValue, editItemTitleValue ,editItemDescriptionValue);
	}
	
	public void checkSponsorItemEdited(){
		this.checkItem(itemSponsorMediaIdLocator, itemSponsorAmountIdLocator, itemSponsorTitleIdLocator, itemSponsorDescriptionIdLocator, 
				editAmountValue, editItemTitleValue ,editItemDescriptionValue);
	}
	
	public void checkDonationItemEdited(){
		this.checkItem(itemDonationMediaIdLocator, itemDonationAmountIdLocator, itemDonationTitleIdLocator, itemDonationDescriptionIdLocator, 
				editAmountValue, editItemTitleValue ,editItemDescriptionValue);
	}
	
	public void checkItemsCount(int beforeDelete, int afterDelete){
		Assert.assertTrue(afterDelete == beforeDelete - 1, "The count of items is the same.");
	}
}
