package eventPages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;

import utils.DriverUtils;
import utils.FileUploader;
import utils.WaitTool;
import utils.WebTable;

public class CheckoutPackages {
	private WebDriver 			driver;
	private FileUploader 		fileUploader;
	private static final int 	WAIT_4_ELEMENT			= WaitTool.DEFAULT_WAIT_4_ELEMENT;
	
	private static By saveBtmIdLocator					= By.id("saveBtn");
	private static By iframeElementCssLocator			= By.cssSelector("iframe");
	private static By bodyElementCssLocator				= By.cssSelector("body");
	private static By editTextLinkLocator				= By.linkText("Edit");
	private static By deleteTextLinkLocator				= By.linkText("Delete");
	private static By createPackageBtnIdLocator			= By.id("checkout-create-package");
	private static By packageNameNameLocator			= By.name("name");
	private static By packageAmountNameLocator			= By.name("value");
	private static By uploadNewFileIdLocator			= By.id("file-addToLib");
	private static By selectFileNameLocator				= By.name("file");
	private static By submitUploadFileIdLocator			= By.id("uploadmediaSubmit");
	private static By registrationBtnIdLocator			= By.id("registration-btn-button");
	private static By sponsorshipBtnIdLocator			= By.id("sponsor-btn-button");
	private static By donationBtnIdLocator				= By.id("donation-btn-button");
	private static By itemDescriptionIdLocator 			= By.id("checkout-item-description");
	private static By itemTitleIdLocator 				= By.id("checkout-item-title");
	private static By itemAmountIdLocator 				= By.id("checkout-item-amount");
	private static By itemMediaIdLocator 				= By.id("checkout-item-media");
	private static By itemSponsorDescriptionIdLocator 	= By.id("checkout-sponsor-description");
	private static By itemSponsorTitleIdLocator 		= By.id("checkout-sponsor-title");
	private static By itemSponsorAmountIdLocator 		= By.id("checkout-sponsor-amount");
	private static By itemSponsorMediaIdLocator 		= By.id("checkout-sponsor-media");
	private static By itemDonationDescriptionIdLocator 	= By.id("checkout-donation-description");
	private static By itemDonationTitleIdLocator 		= By.id("checkout-donation-title");
	private static By itemDonationAmountIdLocator 		= By.id("checkout-donation-amount");
	private static By itemDonationMediaIdLocator 		= By.id("checkout-donation-media");
	private static By removeImgBtnCssLocator			= By.cssSelector(".remove-button");
	private static By packagesTableCssLocator			= By.cssSelector(".donation-items-table");
	private static By itemCategoryNameSelector 			= By.name("category");
	
	private static String sponsorshipCategory			= "Sponsorship";
	private static String donationCategory				= "Donation";
	private static String randomLogofileName			= utils.Data.randomLogoImageName;
	private static String testLogofileName				= utils.Data.testLogoImageName;
	private static String packageNameValue				= "Package name test";
	private static String packageDescriptionValue		= "Checkout package description test";
	private static String amountValue					= "50";
	private static String editPackageTitleValue			= "Package name edited test";
	private static String editPackageDescriptionValue	= "Checkout package description edited test";
	private static String editAmountValue				= "70";
	
	public CheckoutPackages(WebDriver driver){
		this.driver = driver;
		this.fileUploader = new FileUploader(driver);
	}
	
	public void clickCreatePackage(){
		DriverUtils.click(createPackageBtnIdLocator, driver);
	}
	
	public void clickSavePackage(){
		DriverUtils.click(saveBtmIdLocator, driver);
	}
	
	public void clickEditPackage(){
		this.moveCursorToElement();
		WaitTool.waitForElement(driver, editTextLinkLocator, WAIT_4_ELEMENT).click();
	}
	
	public void clickDeleteItem(){
		this.moveCursorToElement();
		WaitTool.waitForElement(driver, deleteTextLinkLocator, WAIT_4_ELEMENT).click();
	}
	
	private void moveCursorToElement(){
		WebElement firstPackage = WebTable.getFirstElementFromColumnAsWebElementByIdLocator(packagesTableCssLocator, 1, driver);
		Actions builder = new Actions(driver);
		builder.moveToElement(firstPackage).perform();
	}
	
	public void goToEventRegistration(){
		DriverUtils.click(registrationBtnIdLocator, driver);
	}
	
	public void goToEventSponsorship(){
		DriverUtils.click(sponsorshipBtnIdLocator, driver);
	}
	
	public void goToEventDonation(){
		DriverUtils.click(donationBtnIdLocator, driver);
	}
	
	public void createRegistrationItem(){
		this.createItem();
	}
	
	public void createSponsorshipItem(){
		DriverUtils.selectDropDownOptionByVisibleText(driver, itemCategoryNameSelector, sponsorshipCategory);
		this.createItem();
	}
	
	public void createDonationItem(){
		DriverUtils.selectDropDownOptionByVisibleText(driver, itemCategoryNameSelector, donationCategory);
		this.createItem();
	}
	
	private void createItem(){
		WaitTool.waitForElement(driver, packageNameNameLocator, WAIT_4_ELEMENT).sendKeys(packageNameValue);
		this.fillDescription(packageDescriptionValue);
		this.uploadSelectItemImage(randomLogofileName);
		WaitTool.waitForElement(driver, packageAmountNameLocator, WAIT_4_ELEMENT).sendKeys(amountValue);
		this.clickSavePackage();
	}
	
	public void editItem(){
		WebElement packageNameElement = WaitTool.waitForElement(driver, packageNameNameLocator, WAIT_4_ELEMENT);
		packageNameElement.clear();
		packageNameElement.sendKeys(editPackageTitleValue);
		this.fillDescription(editPackageDescriptionValue);
		WaitTool.waitForElement(driver, removeImgBtnCssLocator, WAIT_4_ELEMENT).click();
		this.uploadSelectItemImage(testLogofileName);
		WebElement packageAmountElement = WaitTool.waitForElement(driver, packageAmountNameLocator, WAIT_4_ELEMENT);
		packageAmountElement.clear();
		packageAmountElement.sendKeys(editAmountValue);
		this.clickSavePackage();
	}
	
	public int getItemsCount(){
		List<WebElement> tableItems = WebTable.getElementsFromColumnByIdLocator(packagesTableCssLocator, 1, driver);
		return tableItems.size();
	}
	
	private void fillDescription(String descriptionValue){
		this.switchToIframe();
		WebElement iframeBody = WaitTool.waitForElement(driver, bodyElementCssLocator, WAIT_4_ELEMENT);
		iframeBody.clear();
		iframeBody.sendKeys(descriptionValue);
		this.switchToDefaultContent();
	}
	
	private void switchToIframe(){
		WebElement iFrame= WaitTool.waitForElement(driver, iframeElementCssLocator, WAIT_4_ELEMENT);
		driver.switchTo().frame(iFrame);
	}
	
	private void switchToDefaultContent(){
		driver.switchTo().defaultContent();
	}
	
	private void uploadSelectItemImage(String fileName){
		DriverUtils.click(uploadNewFileIdLocator, driver);
		this.fileUploader.uploadFile(fileName, selectFileNameLocator);
		DriverUtils.click(submitUploadFileIdLocator, driver);
	}

	private void checkItem(By mediaItem, By amountItem, By titleItem, By descriptionItem, 
			String amountValueParam, String nameValueParam, String descriptionValueParam){
		WebElement imgElement = WaitTool.waitForElement(driver, mediaItem, WAIT_4_ELEMENT);
		String itemAmout = WaitTool.waitForElement(driver, amountItem, WAIT_4_ELEMENT).getText();
		itemAmout = itemAmout.substring(1, itemAmout.indexOf("."));
		String itemTile = WaitTool.waitForElement(driver, titleItem, WAIT_4_ELEMENT).getText();
		String itemDescription = WaitTool.waitForElement(driver, descriptionItem, WAIT_4_ELEMENT).getText();
		
		Assert.assertTrue(imgElement.isDisplayed(), "Image is not displayed.");
		Assert.assertTrue(itemAmout.equals(amountValueParam), "Wrong item amount.");
		Assert.assertTrue(itemTile.equals(nameValueParam), "Item title is not correct.");
		Assert.assertTrue(itemDescription.equals(descriptionValueParam), "Item Description is not correct");
	}
	
	//Asserts
	public void checkRegistrationPackage(){
		this.checkItem(itemMediaIdLocator, itemAmountIdLocator, itemTitleIdLocator, itemDescriptionIdLocator, 
				amountValue, packageNameValue ,packageDescriptionValue);
	}
	
	public void checkRegistrationItemEdited(){
		this.checkItem(itemMediaIdLocator, itemAmountIdLocator, itemTitleIdLocator, itemDescriptionIdLocator, 
				editAmountValue, editPackageTitleValue ,editPackageDescriptionValue);
	}
	
	public void checkPackagesCount(int beforeDelete, int afterDelete){
		Assert.assertTrue(beforeDelete != afterDelete, "The count of packages is the same.");		
	}
	
	public void checkSponsorshipPackage(){
		this.checkItem(itemSponsorMediaIdLocator, itemSponsorAmountIdLocator, itemSponsorTitleIdLocator, itemSponsorDescriptionIdLocator, 
				amountValue, packageNameValue ,packageDescriptionValue);
	}
	
	public void checkSponsorshipPackageEdited(){
		this.checkItem(itemSponsorMediaIdLocator, itemSponsorAmountIdLocator, itemSponsorTitleIdLocator, itemSponsorDescriptionIdLocator, 
				editAmountValue, editPackageTitleValue ,editPackageDescriptionValue);
	}
	
	public void checkDonationPackage(){
		this.checkItem(itemDonationMediaIdLocator, itemDonationAmountIdLocator, itemDonationTitleIdLocator, itemDonationDescriptionIdLocator, 
				amountValue, packageNameValue ,packageDescriptionValue);
	}
	
	public void checkDonationPackageEdited(){
		this.checkItem(itemDonationMediaIdLocator, itemDonationAmountIdLocator, itemDonationTitleIdLocator, itemDonationDescriptionIdLocator, 
				editAmountValue, editPackageTitleValue ,editPackageDescriptionValue);
	}
}
