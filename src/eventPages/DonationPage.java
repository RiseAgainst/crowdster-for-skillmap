package eventPages;

import java.util.UUID;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import utils.DriverUtils;
import utils.WaitTool;

public class DonationPage {
	private WebDriver				driver;
	private static final int 		WAIT_4_ELEMENT		= WaitTool.DEFAULT_WAIT_4_ELEMENT;
	
	private static By donationPageTitleNameLocator		= By.name("name");
	private static By saveBtnIdLocator					= By.id("pageBtn");
	private static By donationLinkIdLocator				= By.id("donation-link");
	private static By donationPageHeaderElement			= By.cssSelector(".page-container-left-header");
	private static By bodyCssLocator					= By.cssSelector("body");
	private static By leftSideContentCssLocator			= By.cssSelector(".page-container-left-inner");
	private static By rightSideContentCssLocator		= By.cssSelector(".page-container-right-inner");
	
	private String donationPageTitle					= "Donation page title test";
	private String leftSideContent						= "Left side content test. Left side content test.";
	private String rightSideContent						= "Right side content test. Right side content test.";
	private String nextPageContent						= "Next page content test. Next side content test.";
	
	//Payment Data
	private static String firstNameValue 		= "Walter";
	private static String lastNameValue  		= "White";
	private static String emailValue	 		= "ww@testdesktop.qwe";
	private static String cardNameValue			= "Visa";
	private static String cardNumberValue		= "4111111111111111";
	private static String securityCodeValue		= "123";
	private static String addressValue			= "Test street 20";
	private static String cityValue				= "Chicago";
	private static String postalCodeValue		= "777";
	private static String stateValue			= "Illinois";
	private static String expiryYearValue		= "2020";
	private static String donationAmountValue	= "10";
	
	//Selectors
	private static String firstNameId 			= "firstName";
	private static String lastNameId  			= "lastName";
	private static String emailId	  			= "email";
	private static String creditCardNumberId	= "creditCardNumberId";
	private static String securityCodeName		= "card-cvc";
	private static String addressName			= "address";
	private static String cityName				= "city";
	private static String postalCodeName		= "zip";
	
	public DonationPage(WebDriver driver){
		this.driver = driver;
	}
	
	public void clickSaveBtn(){
		WaitTool.waitForElement(driver, saveBtnIdLocator, WAIT_4_ELEMENT).click();
		WaitTool.waitForJQueryProcessing(driver, 5);
	}
	
	public void clickComplete(){
		WaitTool.waitForElement(driver, By.name("Place Order"), WAIT_4_ELEMENT).click();
		WaitTool.waitForJQueryProcessing(driver, 5);
	}
	
	public void clickContinue(){
		WaitTool.waitForElement(driver, By.name("submit-button"), WAIT_4_ELEMENT).click();
		WaitTool.waitForJQueryProcessing(driver, 5);
	}
	
	public void clickShowNavigation(){
		WaitTool.waitForListElementsPresent(driver, By.cssSelector("form > a"), WAIT_4_ELEMENT).get(0).click();
	}
	
	public void fillDonationPageTitle(){
		WebElement pageTitleElement = WaitTool.waitForElement(driver, donationPageTitleNameLocator, WAIT_4_ELEMENT);
		pageTitleElement.clear();
		pageTitleElement.sendKeys(donationPageTitle);
	}
	
	public String getDonationLink(){
		return WaitTool.waitForElement(driver, donationLinkIdLocator, WAIT_4_ELEMENT).getText();
	}
	
	public void goToDonationPage(){
		WaitTool.waitForJQueryProcessing(driver, 5);
		driver.get(getDonationLink());
	}
	
	public void goToMainPage(){
		driver.get(utils.Data.organizationDashboardUrl);
	}
	
	public void fillLeftSideContent(){
		WebElement textEditorBody = WaitTool.waitForElement(driver, bodyCssLocator, WAIT_4_ELEMENT);
		textEditorBody.clear();
		textEditorBody.sendKeys(leftSideContent);
	}
	
	public void fillRightSideContent(){
		WebElement textEditorBody = WaitTool.waitForElement(driver, bodyCssLocator, WAIT_4_ELEMENT);
		textEditorBody.clear();
		textEditorBody.sendKeys(rightSideContent);
	}
	
	public void fillNextPageContent(){
		WebElement textEditorBody = WaitTool.waitForElement(driver, bodyCssLocator, WAIT_4_ELEMENT);
		textEditorBody.clear();
		textEditorBody.sendKeys(nextPageContent);
	}
	
	public void goToNextPageContent(){
		WaitTool.waitForElement(driver, By.id("donationAmount"), WAIT_4_ELEMENT).sendKeys(donationAmountValue);
		WaitTool.waitForElement(driver, By.id(firstNameId), WAIT_4_ELEMENT).sendKeys(firstNameValue);
		WaitTool.waitForElement(driver, By.id(lastNameId), WAIT_4_ELEMENT).sendKeys(lastNameValue);
		String randomEmail = UUID.randomUUID().toString().substring(0, 5) + emailValue;
		WaitTool.waitForElement(driver, By.id(emailId), WAIT_4_ELEMENT).sendKeys(randomEmail);
		WaitTool.waitForElement(driver, By.id("legalTerms"), WAIT_4_ELEMENT).click();
		WaitTool.waitForElement(driver, By.name("cardholderName"), WAIT_4_ELEMENT).sendKeys(cardNameValue);
		WaitTool.waitForElement(driver, By.id(creditCardNumberId), WAIT_4_ELEMENT).sendKeys(cardNumberValue);
		DriverUtils.selectDropDownOptionByVisibleText(driver, By.name("card-expiry-year"), expiryYearValue);	
		WaitTool.waitForElement(driver, By.name(securityCodeName), WAIT_4_ELEMENT).sendKeys(securityCodeValue);
		WaitTool.waitForElement(driver, By.name(addressName), WAIT_4_ELEMENT).sendKeys(addressValue);
		WaitTool.waitForElement(driver, By.name(cityName), WAIT_4_ELEMENT).sendKeys(cityValue);
		DriverUtils.selectDropDownOptionByVisibleText(driver, By.id("pick-state"), stateValue);
		WaitTool.waitForElement(driver, By.name(postalCodeName), WAIT_4_ELEMENT).sendKeys(postalCodeValue);	
		this.clickContinue();
		this.clickComplete();
	}
	
	private Boolean isDisplayedNavigation(){
		Boolean isDisplayed = false;
		WebElement navigationElement = WaitTool.waitForElement(driver, By.cssSelector(".siteTopNav"), WAIT_4_ELEMENT);
		if(navigationElement != null){
			isDisplayed = navigationElement.isDisplayed();
		}
		return isDisplayed;
	}
	
	//Assert
	public void checkDonationPageTitle(){
		String donationPageTitleText = WaitTool.waitForElement(driver, donationPageHeaderElement, WAIT_4_ELEMENT).getText();
		Assert.assertTrue(donationPageTitle.equals(donationPageTitleText), "Donation page title is not correct.");
	}
	
	public void checkLeftSideContent(){
		String leftSideContentText = WaitTool.waitForElement(driver, leftSideContentCssLocator, WAIT_4_ELEMENT).getText();
		Assert.assertTrue(leftSideContent.equals(leftSideContentText), "Left side content is not correct.");
	}
	
	public void checkRightSideContent(){
		String rightSideContentText = WaitTool.waitForElement(driver, rightSideContentCssLocator, WAIT_4_ELEMENT).getText();
		Assert.assertTrue(rightSideContent.equals(rightSideContentText), "Right side content is not correct.");
	}
	
	public void cehckNextPageContent(){
		WebElement thanksMsgElement = WaitTool.waitForElement(driver, By.cssSelector(".thanks-message"), WAIT_4_ELEMENT);
		String nextPageContentText = thanksMsgElement.findElement(By.cssSelector("p")).getText();
		Assert.assertTrue(nextPageContent.equals(nextPageContentText), "Next page text content is not correct.");
	}
	
	public void checkNavigation(){
		Assert.assertTrue(isDisplayedNavigation(), "Navigation is not displayed.");
	}
}
