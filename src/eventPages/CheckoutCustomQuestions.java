package eventPages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import utils.DriverUtils;
import utils.WaitTool;
import utils.WebTable;

public class CheckoutCustomQuestions {
	private WebDriver 			driver;
	private static final int 	WAIT_4_ELEMENT 			= WaitTool.DEFAULT_WAIT_4_ELEMENT;

	private static By createQuestionBtnIdLocator 		= By.id("create-question-btn");
	private static By selectQuestionTypeIdLocator 		= By.id("questionType");
	private static By questiontextInputIdLocator		= By.id("question");
	private static By checkboxElementsCssLocator		= By.cssSelector(".toggle");
	private static By submitBtnIdLocator				= By.id("submitBtn");
	private static By clickEventDonationIdLocator		= By.id("donation-btn-button");
	private static By eventQuickDonationAmountIdLocator	= By.id("quick-donate-fieldId");
	private static By eventCompleteCheckoutBtnIdLocator = By.id("complete-checkout-btn");
	private static By rankOrderNameLocaotr				= By.name("rank");
	private static By eventConfirmationCheckboxTextIdLocator = By.id("confirmation-checkbox-question");
	private static By iframeCssLocator					= By.cssSelector("iframe");
	private static By bodyElementCssLocator				= By.cssSelector("body");
	private static By questionTableIdLocator			= By.id("custom-question-table");
	private static By editLinkTextLocator				= By.linkText("Edit");
	private static By deleteLinkTextLocator				= By.linkText("Delete");

	private static String eventCustomQuestionCssSelector= ".question-";
	private static String writeInOneLineType 			= "Write-in 1-line";
	private static String writeInEssyType 				= "Write-in Essay";
	private static String selectBoxType 				= "Select Box";
	private static String checkBoxType 					= "Check Box";
	private static String stateSelectType 				= "State Select";
	private static String countrySelectType 			= "Country Select";
	private static String fileUploadType 				= "File Upload";
	private static String textBlockType 				= "Text Block";
	private static String confirmationCheckboxType 		= "Confirmation Checkbox";
	private static String futureDateType 				= "Future Date";
	private static String pastDateType 					= "Past Date";
	private static String writeInOneLineRankOrder 		= "1";
	private static String writeInEssyRankOrder 			= "2";
	private static String selectBoxRankOrder 			= "3";
	private static String checkBoxRankOrder 			= "4";
	private static String stateSelectRankOrder 			= "5";
	private static String countrySelectRankOrder 		= "6";
	private static String fileUploadRankOrder 			= "7";
	private static String textBlockRankOrder 			= "8";
	private static String confirmationCheckboxRankOrder = "9";
	private static String futureDateRankOrder 			= "10";
	private static String pastDateRankOrder 			= "11";
	private static String editText						= " edit test";

	public CheckoutCustomQuestions(WebDriver driver) {
		this.driver = driver;
	}
	
	public String getWriteInOneLineType() {
		return writeInOneLineType;
	}
	
	public String getWriteInEssyType() {
		return writeInEssyType;
	}

	public String getSelectBoxType() {
		return selectBoxType;
	}

	public String getCheckBoxType() {
		return checkBoxType;
	}

	public String getStateSelectType() {
		return stateSelectType;
	}

	public String getCountrySelectType() {
		return countrySelectType;
	}

	public String getFileUploadType() {
		return fileUploadType;
	}

	public String getTextBlockType() {
		return textBlockType;
	}

	public String getConfirmationCheckboxType() {
		return confirmationCheckboxType;
	}

	public String getFutureDateType() {
		return futureDateType;
	}

	public String getPastDateType() {
		return pastDateType;
	}

	public String getWriteInOneLineRankOrder() {
		return writeInOneLineRankOrder;
	}

	public String getWriteInEssyRankOrder() {
		return writeInEssyRankOrder;
	}

	public String getSelectBoxRankOrder() {
		return selectBoxRankOrder;
	}

	public String getCheckBoxRankOrder() {
		return checkBoxRankOrder;
	}

	public String getStateSelectRankOrder() {
		return stateSelectRankOrder;
	}

	public String getCountrySelectRankOrder() {
		return countrySelectRankOrder;
	}

	public String getFileUploadRankOrder() {
		return fileUploadRankOrder;
	}

	public String getTextBlockRankOrder() {
		return textBlockRankOrder;
	}

	public String getConfirmationCheckboxRankOrder() {
		return confirmationCheckboxRankOrder;
	}

	public String getFutureDateRankOrder() {
		return futureDateRankOrder;
	}

	public String getPastDateRankOrder() {
		return pastDateRankOrder;
	}

	public void clickCreateQuestion() {
		DriverUtils.click(createQuestionBtnIdLocator, driver);
	}
	
	public void clickEventDonationBtn(){
		DriverUtils.click(clickEventDonationIdLocator, driver);
	}
	
	public void clickCompleteCheckout(){
		DriverUtils.click(eventCompleteCheckoutBtnIdLocator, driver);
	}
	
	public void clickEditLastQuestion(){
		WebElement lastQuestionElement = WebTable.getLastElementFromColumnAsWebElementByIdLocator(questionTableIdLocator, 4, driver);
		lastQuestionElement.findElement(editLinkTextLocator).click();
	}
	
	public void clickDeleteLastQuestion(){
		WebElement lastQuestionElement = WebTable.getLastElementFromColumnAsWebElementByIdLocator(questionTableIdLocator, 4, driver);
		lastQuestionElement.findElement(deleteLinkTextLocator).click();
	}
	
	public void setQuickDonationAmount(){
		WaitTool.waitForElement(driver, eventQuickDonationAmountIdLocator, WAIT_4_ELEMENT).sendKeys(utils.Data.donationAmount);
	}

	public void selectQuestionType(String questionType) {
		DriverUtils.selectDropDownOptionByVisibleText(driver, selectQuestionTypeIdLocator, questionType);
	}
	
	public void createCustomQuestion(String questionText, String rankOrder){
		WaitTool.waitForElement(driver, questiontextInputIdLocator, WAIT_4_ELEMENT).sendKeys(questionText);
		WebElement rankOrderElement = WaitTool.waitForElement(driver, rankOrderNameLocaotr, WAIT_4_ELEMENT);
		rankOrderElement.clear();
		rankOrderElement.sendKeys(rankOrder);
		this.setDefaultRequiredStandartDonationQuestion();
		DriverUtils.click(submitBtnIdLocator, driver);
	}
	
	public void createTextBlockCustomQuestion(String questionText, String rankOrder){
		WaitTool.waitForElement(driver, questiontextInputIdLocator, WAIT_4_ELEMENT).sendKeys(questionText);
		this.switchToIframe();
		WaitTool.waitForElement(driver, bodyElementCssLocator, WAIT_4_ELEMENT).sendKeys(questionText);
		this.switchToDefaultContent();
		WebElement rankOrderElement = WaitTool.waitForElement(driver, rankOrderNameLocaotr, WAIT_4_ELEMENT);
		rankOrderElement.clear();
		rankOrderElement.sendKeys(rankOrder);
		List<WebElement> checkboxes = WaitTool.waitForListElementsPresent(driver, checkboxElementsCssLocator, WAIT_4_ELEMENT);
		DriverUtils.clickElement(checkboxes.get(0), driver);
		DriverUtils.clickElement(checkboxes.get(2), driver);
		DriverUtils.click(submitBtnIdLocator, driver);
	}
	
	public void editCustomQuestion(String questionTypeText){
		WebElement textQuestionElement = WaitTool.waitForElement(driver, questiontextInputIdLocator, WAIT_4_ELEMENT);
		textQuestionElement.clear();
		textQuestionElement.sendKeys(questionTypeText + editText);
		DriverUtils.click(submitBtnIdLocator, driver);
	}
	
	public void editTextBlockCustomQuestion(String questionText){
		String questionEditText = questionText + editText;
		WebElement labelElement = WaitTool.waitForElement(driver, questiontextInputIdLocator, WAIT_4_ELEMENT);
		labelElement.clear();
		labelElement.sendKeys(questionEditText);
		this.switchToIframe();
		WebElement bodyElement = WaitTool.waitForElement(driver, bodyElementCssLocator, WAIT_4_ELEMENT);
		bodyElement.clear();
		bodyElement.sendKeys(questionEditText);
		this.switchToDefaultContent();
		DriverUtils.click(submitBtnIdLocator, driver);
	}
	
	public int getCustomQuestionsCount(){
		List<WebElement> tableItems = WebTable.getElementsFromColumnByIdLocator(questionTableIdLocator, 1, driver);
		if(tableItems == null){
			return 0;
		}
		return tableItems.size();
	}
	
	private void setDefaultRequiredStandartDonationQuestion(){
		List<WebElement> checkboxes = WaitTool.waitForListElementsPresent(driver, checkboxElementsCssLocator, WAIT_4_ELEMENT);
		for(WebElement checkbox : checkboxes){
			DriverUtils.clickElement(checkbox, driver);
		}
	}
	
	private void switchToIframe(){
		WebElement iFrame= WaitTool.waitForElement(driver, iframeCssLocator, WAIT_4_ELEMENT);
		driver.switchTo().frame(iFrame);
	}
	
	private void switchToDefaultContent(){
		driver.switchTo().defaultContent();
	}
	
	//Asserts
	public void checkRequiredCustomQuestion(String questionTypeText, String rankOrder){
		String questionText = WaitTool.waitForElement(driver, By.cssSelector(eventCustomQuestionCssSelector + rankOrder), WAIT_4_ELEMENT).getText();
		Assert.assertTrue((questionTypeText + " *").equals(questionText), "Wrong custom question -" + questionTypeText);
	}
	
	public void checkCustomQuestion(String questionTypeText, String rankOrder){
		String questionText = WaitTool.waitForElement(driver, By.cssSelector(eventCustomQuestionCssSelector + rankOrder), WAIT_4_ELEMENT).getText();
		Assert.assertTrue((questionTypeText).equals(questionText), "Wrong custom question -" + questionTypeText);
	}
	
	public void checkConfirmationCheckboxCustomQuestion(String questionTypeText, String rankOrder){
		String questionText = WaitTool.waitForElement(driver, eventConfirmationCheckboxTextIdLocator, WAIT_4_ELEMENT).getText();
		Assert.assertTrue((questionTypeText).equals(questionText), "Wrong custom question -" + questionTypeText);
	}
	
	public void checkEditedRequiredCustomQuestion(String questionTypeText, String rankOrder){
		String questionText = WaitTool.waitForElement(driver, By.cssSelector(eventCustomQuestionCssSelector + rankOrder), WAIT_4_ELEMENT).getText();
		Assert.assertTrue((questionTypeText + editText + " *").equals(questionText), "Wrong edited custom question -" + questionTypeText);
	}
	
	public void checkEditedConfirmationCheckboxCustomQuestion(String questionTypeText, String rankOrder){
		String questionText = WaitTool.waitForElement(driver, eventConfirmationCheckboxTextIdLocator, WAIT_4_ELEMENT).getText();
		Assert.assertTrue((questionTypeText + editText).equals(questionText), "Wrong custom question -" + questionTypeText);
	}
	
	public void checkEditCustomQuestion(String questionTypeText, String rankOrder){
		String questionText = WaitTool.waitForElement(driver, By.cssSelector(eventCustomQuestionCssSelector + rankOrder), WAIT_4_ELEMENT).getText();
		Assert.assertTrue((questionTypeText + editText).equals(questionText), "Wrong custom question -" + questionTypeText);
	}
	
	public void checkIsDeletedCustomQuestion(int customQuestionsCountBeforeDelete, int customQuestionsCountAfterDelete){
		Assert.assertTrue(customQuestionsCountAfterDelete == customQuestionsCountBeforeDelete - 1, "Custom question is not deleted.");
	}
}
