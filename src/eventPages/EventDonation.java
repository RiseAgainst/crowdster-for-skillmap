package eventPages;

import mobilePages.PaymentInfoMobile;
import utils.DriverUtils;

import java.util.List;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import utils.WaitTool;

public class EventDonation {
	private WebDriver			driver;
	private static final int 	WAIT_4_ELEMENT			= WaitTool.DEFAULT_WAIT_4_ELEMENT;
	
	private static String donationAtleastMessage 	  	= "Donation must be at least $10";
	private static String donationShouldBeLessMessage 	= "Donation Amount should be less than $1,000,000";
	private static String donationLimitMessage		  	= "There are only 1 available.";
	
	private static String orderInfoDonationInput	= "altValue";
	private static String mobileQuickDonate			= "quickDonateImage";
	private static String mobileCompleteCheckout	= ".m-sub-right > div:nth-child(1) > form:nth-child(3) > input:nth-child(4)";
	private static String regItemPrice				= "/html/body/div[2]/div/div[2]/div[1]/div[2]/ul/li[4]/div[1]/div[1]";
	private static String regItemLimitAddToCart		= "/html/body/div[2]/div/div[2]/div[1]/div[2]/ul/li[3]/a/img";
	private static String regItemLimitPrice			= "/html/body/div[2]/div/div[2]/div[1]/div[2]/ul/li[3]/div[1]/div[1]";
	private static String sponsorItemCountInput		= "//*[@id='quantity1362146005820Txt']";
	private static String sponsorItemAddToCart		= "//*[@id='quantity1362146005820']";
	private static String sponsorItemPrice			= "//*[@id='item-1362146005820']/div[1]/div[1]/span";
	private static String donationItemCountInput	= "/html/body/div[2]/div/div[2]/div[1]/div[2]/ul/li[2]/div[2]/div[2]/input"; 
	private static String donationItemAddToCart		= "/html/body/div[2]/div/div[2]/div[1]/div[2]/ul/li[2]/a/img";
	private static String donationItemPrice			= "//*[@id='item-1362146005821']/div[1]/div[1]/span";
	private static String itemsQuantity				= "2";
	
	private static String regItemName				= "/html/body/div[2]/div/div[2]/div[1]/div[2]/ul/li[4]/div[1]/div[1]/span[2]";
	private static String donationItemName			= "//*[@id='item-1362146005821']/div[1]/div[1]";
	private static String sponsorItemName			= "//*[@id='item-1362146005820']/div[1]/div[1]";
	
	private static String itemInfo					= "title";
	
	private static By quickDonateFieldIdLocator		= By.id("quick-donate-fieldId");
	private static By completeCheckoutIdLocator		= By.id("complete-checkout-btn");
	private static By quantityCssLocator			= By.cssSelector("input.quantity");
	private static By registrationLinkTextLocator	= By.linkText("Registration");
	private static By donationLinkTextLocator		= By.linkText("Donation");
	private static By sponsorLinkTextLocator		= By.linkText("Sponsor");
	private static By addToCartBtnCssLocator 		= By.cssSelector("a.add-to-cart-button");
	private static By donationAddToCartBtnNameLocator	= By.name("add-to-cart-btn");
	private static By itemTotalIdLocator			= By.id("total-number");
	
	public EventDonation(WebDriver driver) {
		this.driver = driver;
	}
	
	public void clickRegistrationTab(){
		WaitTool.waitForElement(driver, registrationLinkTextLocator, WAIT_4_ELEMENT).click();
		WaitTool.waitForJQueryProcessing(driver, 5);
	}
	
	public void clickDonationTab(){
		WaitTool.waitForElement(driver, donationLinkTextLocator, WAIT_4_ELEMENT).click();
		WaitTool.waitForJQueryProcessing(driver, 5);
	}
	
	public void clickSponsorTab(){
		WaitTool.waitForElement(driver, sponsorLinkTextLocator, WAIT_4_ELEMENT).click();
		WaitTool.waitForJQueryProcessing(driver, 5);
	}
	
	public void setDonationAmount(String donationAmout) {
		WaitTool.waitForElement(driver, quickDonateFieldIdLocator, WAIT_4_ELEMENT).sendKeys(donationAmout);
	}
	
	public void setDonationAmountInOrder(String donationAmout) {
		driver.findElement(By.id(orderInfoDonationInput)).sendKeys(donationAmout);
	}
	
	public void clickQuickDonateMobile() {
		DriverUtils.click(By.className(mobileQuickDonate), driver);
	}
	
	public PaymentInfo clickCompleteCheckout() {
		DriverUtils.click(completeCheckoutIdLocator, driver);
		return new PaymentInfo(driver);
	}
	
	public PaymentInfoMobile clickMobileCompleteCheckout() {
		DriverUtils.click(By.cssSelector(mobileCompleteCheckout), driver);
		return new PaymentInfoMobile(driver);
	}
	
	public int getRegItemPrice() {
		String text = driver.findElement(By.xpath(regItemPrice)).getText();
		String price = text.substring(text.indexOf("$") + 1,text.indexOf("."));
		return Integer.parseInt(itemsQuantity) * Integer.parseInt(price) ;
	}
	

	
	public void setRegItemQuantity() {
		List<WebElement> quantityElements = WaitTool.waitForListElementsPresent(driver, quantityCssLocator, WAIT_4_ELEMENT);
		quantityElements.get(3).sendKeys(itemsQuantity);
	}
	
	public void addToCartRegItem() {
		List<WebElement> quantityElements = WaitTool.waitForListElementsPresent(driver, addToCartBtnCssLocator, WAIT_4_ELEMENT);
		DriverUtils.clickElement(quantityElements.get(3).findElement(By.tagName("img")), driver);
		utils.WaitTool.waitForJQueryProcessing(driver, 5);
	}
	
	public int addToCartPackageTest() {
		List<WebElement> quantityElements = WaitTool.waitForListElementsPresent(driver, addToCartBtnCssLocator, WAIT_4_ELEMENT);
		DriverUtils.clickElement(quantityElements.get(1).findElement(By.tagName("img")), driver);
		utils.WaitTool.waitForJQueryProcessing(driver, 5);
		String totalAmount = WaitTool.waitForElement(driver, itemTotalIdLocator, WAIT_4_ELEMENT).getText();
		totalAmount = totalAmount.substring(0, totalAmount.indexOf("."));
		return Integer.parseInt(totalAmount);
	}
	
	public int addToCartDonationTest() {
		List<WebElement> quantityElements = WaitTool.waitForListElementsPresent(driver, addToCartBtnCssLocator, WAIT_4_ELEMENT);
		DriverUtils.clickElement(quantityElements.get(1).findElement(By.tagName("img")), driver);
		utils.WaitTool.waitForJQueryProcessing(driver, 5);
		String totalAmount = WaitTool.waitForElement(driver, itemTotalIdLocator, WAIT_4_ELEMENT).getText();
		totalAmount = totalAmount.substring(0, totalAmount.indexOf("."));
		return Integer.parseInt(totalAmount);
	}
	
	public void addToCartSponsorshipTest() {
		List<WebElement> quantityElements = WaitTool.waitForListElementsPresent(driver, addToCartBtnCssLocator, WAIT_4_ELEMENT);
		DriverUtils.clickElement(quantityElements.get(1).findElement(By.tagName("img")), driver);
		utils.WaitTool.waitForJQueryProcessing(driver, 5);
	}
	
	public int getRegLimitItemPrice() {
		String text = driver.findElement(By.xpath(regItemLimitPrice)).getText();
		String price = text.substring(text.indexOf("$") + 1,text.indexOf("."));
		return Integer.parseInt(itemsQuantity) * Integer.parseInt(price) ;
	}
	
	public void setRegLimitItemQuantity() {
		List<WebElement> quantityElements = WaitTool.waitForListElementsPresent(driver, quantityCssLocator, WAIT_4_ELEMENT);
		quantityElements.get(2).sendKeys(itemsQuantity);
	}
	
	public void addToCartRegLimitItem() {
		DriverUtils.click(By.xpath(regItemLimitAddToCart), driver);
		utils.WaitTool.waitForJQueryProcessing(driver, 5);
	}
	
	public int getSponsorItemPrice() {
		String text = driver.findElement(By.xpath(sponsorItemPrice)).getText();
		String price = text.substring(text.indexOf("$") + 1,text.indexOf("."));
		return Integer.parseInt(itemsQuantity) * Integer.parseInt(price) ;
	}
	
	public String getItemPrice() {
		String text = driver.findElement(By.className(itemInfo)).getText();
		String price = text.substring(text.indexOf("$") + 1,text.indexOf("."));
		return price ;
	}
	
	public String getItemName() {
		String text = driver.findElement(By.className(itemInfo)).getText();
		String name = text.substring(text.indexOf(" "));
		name = name.trim();
		return name ;
	}
	
	public void setSponsorItemQuantity() {
		driver.findElement(By.xpath(sponsorItemCountInput)).sendKeys(itemsQuantity);
	}
	
	public void addToCartSponsorItem() {
		DriverUtils.click(By.xpath(sponsorItemAddToCart), driver);
		utils.WaitTool.waitForJQueryProcessing(driver, 5);
	}
	
	public int getDonationItemPrice() {
		String text = driver.findElement(By.xpath(donationItemPrice)).getText();
		String price = text.substring(text.indexOf("$") + 1,text.indexOf("."));
		return Integer.parseInt(itemsQuantity) * Integer.parseInt(price) ;
	}
	
	public void setDonationItemQuantity() {
		driver.findElement(By.xpath(donationItemCountInput)).sendKeys(itemsQuantity);
	}
	
	public void addToCartDonationItem() {
		DriverUtils.click(By.xpath(donationItemAddToCart), driver);
		utils.WaitTool.waitForJQueryProcessing(driver, 5);
	}
	
	public String getRegItemName() {
		String text = driver.findElement(By.xpath(regItemName)).getText();
		String name = text.substring(text.indexOf(" ") + 1);
		return name;
	}
	
	public String getDonationItemName() {
		String text = driver.findElement(By.xpath(donationItemName)).getText();
		String name = text.substring(text.indexOf(" ") + 1);
		return name;
	}
	
	public String getSponsorItemName() {
		String text = driver.findElement(By.xpath(sponsorItemName)).getText();
		String name = text.substring(text.indexOf(" ") + 1);
		return name;
	}
	
	public String getMessageFromAlert() {
		Alert alert = driver.switchTo().alert();
		String alertMessage = alert.getText();
        alert.accept();
        WaitTool.waitForJQueryProcessing(driver, WaitTool.DEFAULT_WAIT_4_PAGE);
        return alertMessage;
	}
	
	public void checkLowerBoundaryMessage() {
		Assert.assertTrue(getMessageFromAlert().equals(donationAtleastMessage), "Something goes wrong with lower boundary");
	}
	
	public void checkUpperBoundaryMessage() {
		Assert.assertTrue(getMessageFromAlert().equals(donationShouldBeLessMessage), "Something goes wrong with upper boundary");
	}
	
	public void checkLimitMessage() {
		Assert.assertTrue(getMessageFromAlert().equals(donationLimitMessage), "Something goes wrong with item limit");
	}
	
	public void checkItemInfo(String itemPriceVal, String itemNameVal) {
		Assert.assertTrue(getItemPrice().equals(itemPriceVal), "Wrong item price");
		Assert.assertTrue(getItemName().equals(itemNameVal), "Wrong item name");
	}

	
	
}
