package eventPages;


import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import utils.DriverUtils;
import utils.WaitTool;


public class EventTeam {
	
	private WebDriver		driver;
	
	private static String teamThermometrClass	= "thermo-detail-top";
	private static String teamTabId				= "team";
	private static String teamMember			= "info";
	
	private static By teamMembersCssLocator		= By.cssSelector(".team-leader-list li");
	
	
	public EventTeam(WebDriver driver) {
		this.driver = driver;
	}
	
	public int getTeamThermometrValue() {
		String thermometrTxt = driver.findElement(By.className(teamThermometrClass)).getText();
		return Integer.parseInt(thermometrTxt.substring(thermometrTxt.indexOf("$") + 1,thermometrTxt.indexOf(".")));
	}
	
	public void clickTeamTab() {
		DriverUtils.click(By.id(teamTabId), driver);
	}
	
	public void goToTeamMemberPage() {
		DriverUtils.click(By.className(teamMember), driver);
	}
	
	public void clickSecondTeam(){
		List<WebElement> teamMembers = WaitTool.waitForListElementsPresent(driver, teamMembersCssLocator, WaitTool.DEFAULT_WAIT_4_ELEMENT);
		DriverUtils.clickElement(teamMembers.get(1), driver);
	}
	
	
}
