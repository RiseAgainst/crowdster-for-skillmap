package eventPages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import utils.DriverUtils;
import utils.Misc;
import utils.WaitTool;
import utils.WebTable;

public class CheckoutPromoCode {
	private WebDriver 			driver;
	private PaymentInfo 		payment;
	private static final int 	WAIT_4_ELEMENT 			= WaitTool.DEFAULT_WAIT_4_ELEMENT;
	
	private static By createPromoCodeBtnIdLocator		= By.id("create-promo-code-btn");
	private static By nameIdLocator						= By.id("nameId");
	private static By codeIdLocator						= By.id("codeId");
	private static By discountValueIdLocator			= By.id("discountValueId");
	private static By datePickerCssLocator				= By.cssSelector(".ui-datepicker-trigger");
	private static By datePickerNextMonthLinkTextLocator= By.linkText("Next");
	private static By savePromoCodeIdLocator			= By.id("save-promo-code-btn");
	private static By eventDonationBtnIdLocator			= By.id("donation-btn-button");
	private static By eventQuantityInputCssLocator		= By.cssSelector(".donation-quantity-input");
	private static By eventAddToCartBtnNameLocator		= By.name("add-to-cart-btn");
	private static By eventCompleteCheckoutIdLocator	= By.id("complete-checkout-btn");
	private static By eventPromoCodeIdLocator			= By.id("promoCode");
	private static By eventTotalPaymentIdLocator		= By.id("total-number");
	private static By promoCodeTableIdLocator			= By.id("promo-code-table");
	private static By editPromoCodeLinkTextLocator		= By.linkText("Edit");
	private static By deletePromoCodeLinkTextLocator	= By.linkText("Delete");
	private static By eventPromoCodePriceIdLocator		= By.id("promo-code-price");
	private static By eventPaymentSummary				= By.id("payment-summary");
	private static By checkoutPromoCodeTextLinkLocator 	= By.linkText("Promo Code");
	private static By promoCodeTypeNameLocator			= By.name("type");
	private static By promoCodeQuantityIdLocator		= By.id("codeQuantity");
	private static By eventCompleteBtnNameLocator		= By.name("Place Order");
	private static By eventPromoCodeErrorMsgIdLocator	= By.id("promo-code-invalid-error");
	private static By itemsScopeInputIdLocator			= By.id("site_items_chosen");
	private static By paymentSummeryIdLocator			= By.id("payment-summary");
	
	private static String unlimitedPromoCodeNameValue	= "Unlimited Promo code name test";
	private static String unlimitedPromoCodeValue		= "Unlimited promotion test";
	private static String limitedPromoCodeNameValue		= "Limited Promo code name test";
	private static String limitedPromoCodeValue			= "Limited promotion test";
	private static String discountValue					= "10";
	private static String endDayPromoCode				= "20";
	private static String eventDonationQuantityValue	= "3";
	private static String editNameValue					= "Promo code name edit test";
	private static String editCodeValue					= "promotion edit test";
	private static String editDiscountValue				= "20";
	private static String quantityValue					= "1";
	private static String eventPromoCodeErrorMessage	= "Promo code not valid.";
	private static String noExistedPromoCode			= "No existed Promo code";
	private static String outOfDatePromoCode			= "Out of date Promo code";
	private static String promoCodeWichScopeName		= "Promo code with scope entries";
	private static String promoCodeWichScope			= "promo code scope";
	
	public CheckoutPromoCode(WebDriver driver) {
		this.driver = driver;
		this.payment = new PaymentInfo(driver);
	}
	
	public void goToPromoCode(){
		WaitTool.waitForElement(driver, checkoutPromoCodeTextLinkLocator, WAIT_4_ELEMENT).click();
	}
	
	public void clickCreatePromoCodeBtn(){
		DriverUtils.click(createPromoCodeBtnIdLocator, driver);
	}
	
	public void clickEditPromoCode(){
		WebElement promoCodesTableFirstElement = WebTable.getFirstElementFromColumnAsWebElementByIdLocator(promoCodeTableIdLocator, 6, driver);
		promoCodesTableFirstElement.findElement(editPromoCodeLinkTextLocator).click();
	}
	
	public void clickEditPromoCodeWithScopeEntries(){
		WebElement promoCodesTableFirstElement = WebTable.getLastElementFromColumnAsWebElementByIdLocator(promoCodeTableIdLocator, 6, driver);
		promoCodesTableFirstElement.findElement(editPromoCodeLinkTextLocator).click();
	}
	
	public void clickDeletePromoCode(){
		WebElement promoCodesTableFirstElement = WebTable.getFirstElementFromColumnAsWebElementByIdLocator(promoCodeTableIdLocator, 6, driver);
		promoCodesTableFirstElement.findElement(deletePromoCodeLinkTextLocator).click();
	}
	
	public void clickEventDonationBtn(){
		DriverUtils.click(eventDonationBtnIdLocator, driver);
	}
	
	public void clickSaveBtn(){
		DriverUtils.click(savePromoCodeIdLocator, driver);
	}
	
	public void clickCompleteBtn(){
		DriverUtils.click(eventCompleteBtnNameLocator, driver);
		WaitTool.waitForJQueryProcessing(driver, 5);
	}
	
	public String clickAddToCart(){
		DriverUtils.click(eventAddToCartBtnNameLocator, driver);
		WaitTool.waitForJQueryProcessing(driver, 5);
		WebElement paymentTotalElement = WaitTool.waitForElement(driver, eventTotalPaymentIdLocator, WAIT_4_ELEMENT);
		String paymentTotal = paymentTotalElement.getText();
		paymentTotal = paymentTotal.substring(0, paymentTotal.lastIndexOf("."));
		return paymentTotal;
	}
	
	public void clickCompleteCheckout(){
		DriverUtils.click(eventCompleteCheckoutIdLocator, driver);
	}
	
	public String getPromoCode(){
		return promoCodeWichScope;
	}
	
	public String getUnlimitedPromoCode() {
		return unlimitedPromoCodeValue;
	}
	
	public String getLimitedPomoCode() {
		return limitedPromoCodeValue;
	}
	
	public String getUnlimitedPromoCodeName() {
		return unlimitedPromoCodeNameValue;
	}
	
	public String getLimitedPromoCodeName() {
		return limitedPromoCodeNameValue;
	}
	
	public String getLimitedPromoCodeQuantity() {
		return quantityValue;
	}

	public String getEditCodeValue() {
		return editCodeValue;
	}
	
	public String getNoExistedPromoCode() {
		return noExistedPromoCode;
	}
	
	public String getOutOfDatePromoCode() {
		return outOfDatePromoCode;
	}

	public void setDonationQuantity(){
		WaitTool.waitForElement(driver, eventQuantityInputCssLocator, WAIT_4_ELEMENT).sendKeys(eventDonationQuantityValue);
	}
	
	public int getPromoCodesCount(){
		List<WebElement> tableItems = WebTable.getElementsFromColumnByIdLocator(promoCodeTableIdLocator, 1, driver);
		return tableItems.size();
	}
	
	public void createPromoCode(String promoCodeName, String promoCodeValue){
		WaitTool.waitForElement(driver, nameIdLocator, WAIT_4_ELEMENT).sendKeys(promoCodeName);
		WaitTool.waitForElement(driver, codeIdLocator, WAIT_4_ELEMENT).sendKeys(promoCodeValue);
		WebElement discountValueElement = WaitTool.waitForElement(driver, discountValueIdLocator, WAIT_4_ELEMENT);
		discountValueElement.clear();
		discountValueElement.sendKeys(discountValue);
		this.setStartDate();
		this.setEndDate();
	}
	
	public void createPromoCodeOutOfDate(String promoCodeName, String promoCodeValue){
		WaitTool.waitForElement(driver, nameIdLocator, WAIT_4_ELEMENT).sendKeys(promoCodeName);
		WaitTool.waitForElement(driver, codeIdLocator, WAIT_4_ELEMENT).sendKeys(promoCodeValue);
		WebElement discountValueElement = WaitTool.waitForElement(driver, discountValueIdLocator, WAIT_4_ELEMENT);
		discountValueElement.clear();
		discountValueElement.sendKeys(discountValue);
		this.setStartDateDelayed();
		this.setEndDateDelayed();
	}
	
	public void createPromoCodeWithAllScopeEntries(){
		this.createPromoCode(promoCodeWichScopeName, promoCodeWichScope);
		this.setPromoCodeAllScopeEntries();
	}
	
	public void editPromoCodeWithSomeScopeEntries(){
		this.removePromoCodeAllScopeEntries();
		this.setPromoCodeSomeScopeEntries();
	}
	
	private void setPromoCodeSomeScopeEntries(){
		DriverUtils.click(itemsScopeInputIdLocator, driver);
		int maxlength = WaitTool.waitForListElementsPresent(driver, By.cssSelector("li.active-result"), WAIT_4_ELEMENT).size();
		for (int i = 0; i < maxlength - 2; i++) {
			DriverUtils.click(By.cssSelector("li.active-result"), driver);
			DriverUtils.click(By.cssSelector("li.search-choice"), driver);
		}
	}
	
	private void setPromoCodeAllScopeEntries(){
		DriverUtils.click(itemsScopeInputIdLocator, driver);
		int maxlength = WaitTool.waitForListElementsPresent(driver, By.cssSelector("li.active-result"), WAIT_4_ELEMENT).size();
		for (int i = 0; i < maxlength; i++) {
			DriverUtils.click(By.cssSelector("li.active-result"), driver);
			DriverUtils.click(By.cssSelector("li.search-choice"), driver);
		}
	}
	
	private void removePromoCodeAllScopeEntries(){
		List<WebElement> entries = WaitTool.waitForListElementsPresent(driver, By.cssSelector("a.search-choice-close"), WAIT_4_ELEMENT);
		for (WebElement webElement : entries) {
			DriverUtils.clickElement(webElement, driver);
		}
	}
	
	public void setLimitedPromoCode(String limit){
		List<WebElement> promoCodeTypesElement = WaitTool.waitForListElementsPresent(driver, promoCodeTypeNameLocator, WAIT_4_ELEMENT);
		for(WebElement type : promoCodeTypesElement){
			String value = type.getAttribute("value");
			if(value.equals("1")){
				DriverUtils.clickElement(type, driver);
				WaitTool.waitForElement(driver, promoCodeQuantityIdLocator, WAIT_4_ELEMENT).sendKeys(limit);
				break;
			}
		}
	}
	
	public void editPromoCode(){
		WebElement nameElement = WaitTool.waitForElement(driver, nameIdLocator, WAIT_4_ELEMENT);
		nameElement.clear();
		nameElement.sendKeys(editNameValue);
		WebElement codeElement = WaitTool.waitForElement(driver, codeIdLocator, WAIT_4_ELEMENT);
		codeElement.clear();
		codeElement.sendKeys(editCodeValue);
		WebElement discountValueElement = WaitTool.waitForElement(driver, discountValueIdLocator, WAIT_4_ELEMENT);
		discountValueElement.clear();
		discountValueElement.sendKeys(editDiscountValue);
		this.setEndDateDelayed();
		this.clickSaveBtn();
	}
	
	public void paymentDataFilling(String code){
		payment.paymentDataFilling();
		WaitTool.waitForElement(driver, eventPromoCodeIdLocator, WAIT_4_ELEMENT).sendKeys(code);
		payment.acceptTermsAndconditions();
		payment.clickContinue();
		WaitTool.waitForJQueryProcessing(driver, 5);
	}
	
	private void setStartDate(){
		WaitTool.waitForElement(driver, datePickerCssLocator, WAIT_4_ELEMENT).click();
		String currentDay = Misc.getCurrentDate("Day");
		WaitTool.waitForElement(driver, By.linkText(currentDay), WAIT_4_ELEMENT).click();
	}
	
	private void setStartDateDelayed(){
		WaitTool.waitForElement(driver, datePickerCssLocator, WAIT_4_ELEMENT).click();
		String currentDay = Misc.getCurrentDate("Day");
		WaitTool.waitForElement(driver, datePickerNextMonthLinkTextLocator, WAIT_4_ELEMENT).click();
		WaitTool.waitForElement(driver, By.linkText(currentDay), WAIT_4_ELEMENT).click();
	}
	
	private void setEndDate(){
		List<WebElement> datePickers = WaitTool.waitForListElementsPresent(driver, datePickerCssLocator, WAIT_4_ELEMENT);
		datePickers.get(1).click();
		WaitTool.waitForElement(driver, datePickerNextMonthLinkTextLocator, WAIT_4_ELEMENT).click();
		WaitTool.waitForElement(driver, By.linkText(endDayPromoCode), WAIT_4_ELEMENT).click();
	}
	
	private void setEndDateDelayed(){
		List<WebElement> datePickers = WaitTool.waitForListElementsPresent(driver, datePickerCssLocator, WAIT_4_ELEMENT);
		datePickers.get(1).click();
		WaitTool.waitForElement(driver, datePickerNextMonthLinkTextLocator, WAIT_4_ELEMENT).click();
		WaitTool.waitForElement(driver, datePickerNextMonthLinkTextLocator, WAIT_4_ELEMENT).click();
		WaitTool.waitForElement(driver, datePickerNextMonthLinkTextLocator, WAIT_4_ELEMENT).click();
		WaitTool.waitForElement(driver, By.linkText(endDayPromoCode), WAIT_4_ELEMENT).click();
	}
	
	private Boolean isTotalPaymentWithPromotion(String totalPayment){
		WebElement summaryTotalElement = WaitTool.waitForElement(driver, eventPaymentSummary, WAIT_4_ELEMENT);
		String promotionPayment = summaryTotalElement.getText();
		promotionPayment = promotionPayment.substring(promotionPayment.indexOf("$") + 1, promotionPayment.indexOf("."));
		String promoCodePrice = WaitTool.waitForElement(driver, eventPromoCodePriceIdLocator, WAIT_4_ELEMENT).getText(); 
		promoCodePrice = promoCodePrice.substring(promoCodePrice.indexOf("$") + 1, promoCodePrice.indexOf("."));
		Integer discountValueParsted = Integer.parseInt(promoCodePrice);
		Integer totalPaymentParsed = Integer.parseInt(totalPayment);
		Integer promotionPaymentParsed = Integer.parseInt(promotionPayment);
		return totalPaymentParsed.equals(promotionPaymentParsed + discountValueParsted);
	}
	
	private int getPaymentValue(){
		String payment = WaitTool.waitForElement(driver, paymentSummeryIdLocator, WAIT_4_ELEMENT).getText();
		payment = payment.substring(1, payment.indexOf("."));
		return Integer.parseInt(payment);
	}
	
	private boolean isShownIvalidPromoCodeErrorMsg(){
		WebElement errorMsgElement = WaitTool.waitForElement(driver, eventPromoCodeErrorMsgIdLocator, WAIT_4_ELEMENT);
		if(errorMsgElement == null){
			return false;
		} else{
			if(errorMsgElement.isDisplayed()){
				return true;
			}
		}
		return false;
	}
	
	//Asserts
	public void checkTotalPaymentWithPromotion(String totalPayment){
		Assert.assertTrue(isTotalPaymentWithPromotion(totalPayment), "Wrong promotion payment.");
	}
	
	public void checkIsDeletedPromoCode(int promoCodesCountBeforeDelete, int promoCodesCountAfterDelete){
		Assert.assertTrue(promoCodesCountBeforeDelete - 1 == promoCodesCountAfterDelete, "Promo code is not deleted.");
	}
	
	public void checkPromoCodeErrorMessage(){
		String eventPromoCodeErrorMessageText = WaitTool.waitForElement(driver, eventPromoCodeErrorMsgIdLocator, WAIT_4_ELEMENT).getText();
		Assert.assertTrue(eventPromoCodeErrorMessage.equals(eventPromoCodeErrorMessageText), "Wrong promo code error message.");
	}
	
	public void checkIsAddedNewPromoCode(int promoCodesCountBeforeCreate, int promoCodesCountAfterCreate){
		Assert.assertTrue(promoCodesCountAfterCreate == promoCodesCountBeforeCreate + 1, "New promo code was added.");
	}
	
	public void checkPromoCodesCountAfterEdit(int promoCodesCountBeforeEdit, int promoCodesCountAfterEdit){
		Assert.assertTrue(promoCodesCountBeforeEdit == promoCodesCountAfterEdit, "Promo code was edited.");
	}
	
	public void checkRegistrationPaymentTotal(int totalAmount){
		Assert.assertTrue(getPaymentValue() == (totalAmount - Integer.parseInt(discountValue)), "Wrong payment total.");
	}
	
	public void checkDonationPaymentTotal(int totalAmount, int promoCodeValue){
		int discountPromoValue = Integer.parseInt(discountValue) + promoCodeValue;
		Assert.assertTrue(getPaymentValue() == (totalAmount - discountPromoValue), "Wrong payment total.");
	}
	
	public void checkInvalidPromoCodeErrorMsg(){
		Assert.assertTrue(isShownIvalidPromoCodeErrorMsg(), "Invalid Promo code error message doesn't show.");
	}
}
