package eventPages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import utils.WaitTool;

public class NavigationPage {
	
	private WebDriver			driver;
	private  WebDriverWait    	wait;
	private static final long TIMEOUT           = WebDriverWait.DEFAULT_SLEEP_TIMEOUT;
	private static final int WAIT_4_ELEMENT		= WaitTool.DEFAULT_WAIT_4_ELEMENT;
	private static String successMessage  		= "Your page was saved successfully.";
	private static String toggleChecked   		= "toggle checked";
	
	private static By titleIdLocator 			= By.id("nameId");
	private static By saveButtonIdLocator 		= By.id("pageBtn");
	private static By iframeLocator 			= By.tagName("iframe");
	private static By permalinkIdLocator 		= By.id("permalinkId");
	private static By rankOrderNameLocator 		= By.name("rank");
	private static By labelIdLocator		    = By.id("navigationLabel");
	private static By urlNameLocator			= By.name("url");
	private static By checkboxesCssLocator		= By.cssSelector(".toggle");
	private static By messagePopupCssLocator	= By.cssSelector(".message-popup-inner");
	private static By errorMsgCssSelector		= By.cssSelector(".error");
	
	private static String pageContent			= "body";
	private static String contentXpath 			= "/html/body";
	
	private static String title					= "Test Page";
	private static String navigationLabel		= "Navigation Label";
	private static String permaLink				= "Page Permalink";
	private static String url					= "";
	private static String rankOrder				= "0";
	private static String content				= "Content test";
	
	private static String titleEdit				= "Test Page Edit";
	private static String navigationLabelEdit	= "Navigation Lable Edit";
	private static String permaLinkEdit			= "Page Permalink Edit";
	private static String urlEdit				= "";
	private static String rankOrderEdit			= "3";
	private static String contentEdit			= "Content test Edit";
	private static String privateNaviPageLabel  = "Private navi page";
	private static String privateNaviPageTitle  = "Private navi page";
	
	public NavigationPage(WebDriver driver) {
		this.driver = driver;
		this.wait = new WebDriverWait(driver, TIMEOUT);
	}
	
	public String getPagecontent() {
		return driver.findElement(By.className(pageContent)).getText();
	}
	
	public String getLink() {
		return driver.getCurrentUrl();
	}
	
	private String getPageTitle(){
		return WaitTool.waitForElement(driver, By.cssSelector("h1"), WAIT_4_ELEMENT).getText();
	}
	
	private String getPageNavigationLabel(){
		return WaitTool.waitForElement(driver, By.cssSelector(".onThis"), WAIT_4_ELEMENT).getText().trim();
	}
	
	private String getPageContent(){
		return WaitTool.waitForElement(driver, By.cssSelector(".body"), WAIT_4_ELEMENT).getText();
	}
	
	private void clickSaveButton() {
		WaitTool.waitForElement(driver, saveButtonIdLocator, WAIT_4_ELEMENT).click();
	}
	
	private void setPageTitle(String pageTitleValue) {
		WaitTool.waitForElement(driver, titleIdLocator, WAIT_4_ELEMENT).sendKeys(pageTitleValue);
	}
	
	private void setNavigationLabel(String pageNavigationLabelValue) {
		WaitTool.waitForElement(driver, labelIdLocator, WAIT_4_ELEMENT).sendKeys(pageNavigationLabelValue);
	}
	
	private void setPagePermalink(String pagePermalinkValue) {
		WaitTool.waitForElement(driver, permalinkIdLocator, WAIT_4_ELEMENT).sendKeys(pagePermalinkValue);
	}
	
	private void setPageContent(String pageContentValue) {
		WebElement iFrame= WaitTool.waitForElement(driver, iframeLocator, WAIT_4_ELEMENT);
		driver.switchTo().frame(iFrame);
		WaitTool.waitForElement(driver, By.xpath(contentXpath), WAIT_4_ELEMENT).sendKeys(pageContentValue);
		driver.switchTo().defaultContent();
	}
	
	private void setPageUrl(String pageUrlValue) {
		WaitTool.waitForElement(driver, urlNameLocator, WAIT_4_ELEMENT).sendKeys(pageUrlValue);
	}
	
	private void setPageRankOrder(String pageRankOrderValue) {
		WaitTool.waitForElement(driver, rankOrderNameLocator, WAIT_4_ELEMENT).sendKeys(pageRankOrderValue);
	}
	
	public void createNavigationPage(){
		this.setPageTitle(title);
		this.setNavigationLabel(navigationLabel);
		this.setPagePermalink(permaLink);
		this.setPageContent(content);
		this.setPageUrl(url);
		this.setPageRankOrder(rankOrder);
		this.clickSaveButton();
	}
	
	public Boolean isPrivateNaviPage(){
		Boolean isPrivate = false;
		WebElement privateNavigationPageElement = WaitTool.waitForElement(driver, By.linkText(privateNaviPageLabel), WAIT_4_ELEMENT);
		if(privateNavigationPageElement == null){
			isPrivate = true;
		}
		return isPrivate;
	}
	
	public void createNavigationPageWithSpecificOptions(){
		this.setPageTitle(privateNaviPageTitle);
		this.setNavigationLabel(privateNaviPageLabel);
		List<WebElement> checkboxes = WaitTool.waitForListElementsPresent(driver, checkboxesCssLocator, WAIT_4_ELEMENT);
		checkboxes.get(0).click();
		checkboxes.get(1).click();		
		this.clickSaveButton();
	}
	
	public void editNavigationPage(){
		this.clearFields();
		this.setPageTitle(titleEdit);
		this.setNavigationLabel(navigationLabelEdit);
		this.setPagePermalink(permaLinkEdit);
		this.setPageContent(contentEdit);
		this.setPageUrl(urlEdit);
		this.setPageRankOrder(rankOrderEdit);
		this.clickSaveButton();
	}
	
	private void clearFields(){
		WaitTool.waitForElement(driver, titleIdLocator, WAIT_4_ELEMENT).clear();
		WaitTool.waitForElement(driver, labelIdLocator, WAIT_4_ELEMENT).clear();
		WaitTool.waitForElement(driver, permalinkIdLocator, WAIT_4_ELEMENT).clear();
		WebElement iFrame= WaitTool.waitForElement(driver, iframeLocator, WAIT_4_ELEMENT);
		driver.switchTo().frame(iFrame);
		WaitTool.waitForElement(driver, By.xpath(contentXpath), WAIT_4_ELEMENT).clear();
		driver.switchTo().defaultContent();	
		WaitTool.waitForElement(driver, urlNameLocator, WAIT_4_ELEMENT).clear();
		WaitTool.waitForElement(driver, rankOrderNameLocator, WAIT_4_ELEMENT).clear();
	}
	
	public void failToCreateNavigatinPageWithoutData(){
		WaitTool.waitForElement(driver, saveButtonIdLocator, WAIT_4_ELEMENT).click();
	}
	
	public void failToCreateNavigatinPageWithoutRequiredFields(){
		this.setPagePermalink(permaLink);
		this.setPageContent(content);
		this.setPageUrl(url);
		this.setPageRankOrder(rankOrder);
		this.clickSaveButton();
	}
	
	public void failToEditNavigatinPageWithoutRequiredFields(){
		WaitTool.waitForElement(driver, titleIdLocator, WAIT_4_ELEMENT).clear();
		WaitTool.waitForElement(driver, labelIdLocator, WAIT_4_ELEMENT).clear();
		
		this.clickSaveButton();
		wait.until(ExpectedConditions.alertIsPresent());
		driver.switchTo().alert().accept();
	}
	
	public void failToEditNavigatinPageWithoutData(){
		this.clearFields();
		this.clickSaveButton();
		wait.until(ExpectedConditions.alertIsPresent());
		driver.switchTo().alert().accept();
	}
	
	public void deleteNavigationPage(){
		wait.until(ExpectedConditions.alertIsPresent());
		driver.switchTo().alert().accept();
	}
	
	//Asserts
	public void checkNavPage(String titleValue, String contentValue) {
		Assert.assertTrue(getPageTitle().equals(titleValue), "Wrong page title");
		Assert.assertTrue(getPagecontent().equals(contentValue), "Wrong page content");
	}
	
	public void checkNavigationPage(){
		checkNavigationPageTitleLabelContent(title, navigationLabel, content);
	}
	
	public void checkNavigationPageEdit(){
		checkNavigationPageTitleLabelContent(titleEdit, navigationLabelEdit, contentEdit);
	}
	
	private void checkNavigationPageTitleLabelContent(String pageTitle, String pageNavigationLabel, String pageContent) {
		List<WebElement> navigationLabelsElements = WaitTool.waitForListElementsPresent(driver, By.cssSelector(".drop"), WAIT_4_ELEMENT);
		
		for(WebElement naviLabelElement: navigationLabelsElements){
			if(naviLabelElement.getText().trim().equals(pageNavigationLabel)){
				naviLabelElement.findElement(By.cssSelector("a")).click();
				break;
			}
		}
		
		Assert.assertTrue(getPageTitle().equals(pageTitle), "Wrong page title.");
		Assert.assertTrue(getPageNavigationLabel().equals(pageNavigationLabel), "Wrong navigation label.");
		Assert.assertTrue(getPageContent().equals(pageContent), "Wrong page content value.");
	}
	
	public void checkSuccessMessage(){
		String messagePopup = WaitTool.waitForElement(driver, messagePopupCssLocator, WAIT_4_ELEMENT).getText();
		Assert.assertTrue(messagePopup.equals(successMessage), "Something wrong with success message");
	}
	
	public void checkNavigationPagesCount(int navigationPagesCount, int navigationPagesCountUpdated){
		Assert.assertNotEquals(navigationPagesCount, navigationPagesCountUpdated, "Navigation Pages Count is the same.");
	}

	public void checkNavigationPageLabelAfterCreaded(String newNavigationPageLable) {
		Assert.assertTrue(navigationLabel.equals(newNavigationPageLable), "Wrong Navigation Page Lable.");
	}
	
	public void checkToggleButtons(){
		List<WebElement> checkboxes = WaitTool.waitForListElementsPresent(driver, checkboxesCssLocator, WAIT_4_ELEMENT);
		String onlyShowWhenLoggedInClassAttribute = checkboxes.get(0).getAttribute("class");
		String dontShowInNavigationClassAttribute = checkboxes.get(1).getAttribute("class");
		
		Assert.assertTrue(onlyShowWhenLoggedInClassAttribute.equals(toggleChecked), 
				"'Only show when Logged In' must be checked but it's not");
		Assert.assertTrue(dontShowInNavigationClassAttribute.equals(toggleChecked), 
				"'Don't show when logged in' must be checked but it's not");
	}
	
	public void checkIsPrivateNavigationPage(Boolean isPrivate){
		Assert.assertTrue(isPrivate, "Navigation page is not private.");
	}
	
	public void checkIsShownValidationMessage(){
		WebElement validationMsgElemet = WaitTool.waitForElement(driver, errorMsgCssSelector, WAIT_4_ELEMENT);
		Assert.assertTrue(validationMsgElemet.isDisplayed(), "Missing validation message.");
	}
}
