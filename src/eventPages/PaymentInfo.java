package eventPages;


import java.util.ArrayList;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import utils.DriverUtils;
import utils.Misc;
import utils.WaitTool;

public class PaymentInfo {
	private WebDriver			driver;
	private static final int 	WAIT_4_ELEMENT 		= WaitTool.DEFAULT_WAIT_4_ELEMENT;
	
	//Payment Data
	private static String firstNameValue 			= "Walter";
	private static String lastNameValue  			= "White";
	private static String emailValue	 			= "ww@testdesktop.qwe";
	private static String cardNameValue				= "Visa";
	private static String cardNumberValue			= "4111111111111111";
	private static String securityCodeValue			= "123";
	private static String addressValue				= "Test street 20";
	private static String cityValue					= "City of heaven";
	private static String postalCodeValue			= "777";
	private static String promocodeValue			= "15";
	private static String thankYouMessageValue		= "Help us Spread the word!";
	private  String paymentData[] = { firstNameValue, lastNameValue, emailValue, "FirstSupersite", "FirstEvent", "date", utils.Data.donationAmountForReports, "Online", "Site"};
	
	//Locators
	private static String firstNameId 				= "firstName";
	private static String lastNameId  				= "lastName";
	private static String emailId	  				= "email";
	private static String creditCardNumberId		= "creditCardNumberId";
	private static String securityCodeName			= "card-cvc";
	private static String addressName				= "address";
	private static String cityName					= "city";
	private static String postalCodeName			= "zip";
	private static String termsAndConditionsId		= "legalTerms";
	private static String continueButtonName		= "submit-button";
	private static String completeButtonName		= "Place Order";
	private static String promocodeInput			= "promoCode";
	private static String expirationDateYear		= "card-expiry-year";
	private static String totalValue				= "summary-total-row";
	
	//Thank you page
	private static String joinTeamId				= "join-team-btn";
	private static String createPageId				= "create-your-page";
	private static String thankYouMessage			= "/html/body/div[2]/div/div/div[1]/div/form/p";
	
	private static By cardNameNameLocator			= By.name("cardholderName");
	private static By itemPromoCodeInputCssLocator	= By.cssSelector("input.promocode-peritem");
	
	public PaymentInfo(WebDriver driver) {
		this.driver = driver;
	}
	
	public static ArrayList<String> getThankYouPageLocators() {
		ArrayList<String> locators = new ArrayList<String>();
	//	locators.add("id," + startTeamId + ", Start Team");
		locators.add("id," + joinTeamId + ", Join Team");
		locators.add("id," + createPageId + ", Create Page");
		return locators;
	}
	
	public void selectExpirationDate() {
		new Select(driver.findElement(By.name(expirationDateYear))).selectByVisibleText("2020");
	}
	
	
	public void paymentDataFilling() {
		driver.findElement(By.id(firstNameId)).sendKeys(firstNameValue);
		driver.findElement(By.id(lastNameId)).sendKeys(lastNameValue);
		driver.findElement(By.id(emailId)).sendKeys(emailValue);
		driver.findElement(cardNameNameLocator).sendKeys(cardNameValue);
		driver.findElement(By.id(creditCardNumberId)).sendKeys(cardNumberValue);
		driver.findElement(By.name(securityCodeName)).sendKeys(securityCodeValue);
		driver.findElement(By.name(addressName)).sendKeys(addressValue);
		driver.findElement(By.name(cityName)).sendKeys(cityValue);
		driver.findElement(By.name(postalCodeName)).sendKeys(postalCodeValue);
		driver.findElement(By.name(securityCodeName)).sendKeys(securityCodeValue);
		selectExpirationDate();
	}
	
	public void paymentTeamDataFilling() {
		driver.findElement(By.id(firstNameId)).sendKeys(firstNameValue);
		driver.findElement(By.id(lastNameId)).sendKeys(lastNameValue);
		driver.findElement(By.id(emailId)).sendKeys(emailValue);
		driver.findElement(cardNameNameLocator).sendKeys(cardNameValue);
		driver.findElement(By.id(creditCardNumberId)).sendKeys(cardNumberValue);
		driver.findElement(By.name(securityCodeName)).sendKeys(securityCodeValue);
		driver.findElement(By.name(addressName)).sendKeys(addressValue);
		driver.findElement(By.name(cityName)).sendKeys(cityValue);
		driver.findElement(By.name(postalCodeName)).sendKeys(postalCodeValue);
		driver.findElement(By.name(securityCodeName)).sendKeys(securityCodeValue);
		selectExpirationDate();
	}
	
	public int getPromoCodeValue() {
		return Integer.parseInt(promocodeValue);
	}
	
	public String getPromoCodeTextValue() {
		return promocodeValue;
	}
	
	public int setPromocode15() {
		driver.findElement(By.id(promocodeInput)).sendKeys(promocodeValue);
		return Integer.parseInt(promocodeValue);
	}
	
	public void setPromocode(String promoCode) {
		driver.findElement(By.id(promocodeInput)).sendKeys(promoCode);
	}

	public void acceptTermsAndconditions() {
		DriverUtils.click(By.id(termsAndConditionsId), driver);
	}
	
	public void clickContinue() {
		DriverUtils.click(By.name(continueButtonName), driver);
		WaitTool.waitForJQueryProcessing(driver, 5);
	}
	
	public void clickCompleteButton() {
		DriverUtils.click(By.name(completeButtonName), driver);
		WaitTool.waitForJQueryProcessing(driver, 5);
	}
	
	public String getTotalValue() {
		String text = driver.findElement(By.className(totalValue)).getText();
		String name = text.substring(text.indexOf(" ") + 1, text.lastIndexOf("0"));
		return name;
	}
	
	public String [] getPaymentData() {
		paymentData[5] = Misc.getCurrentDate("ReportFormat");
		return paymentData; 
	}
	
	public void setItemPromoCode(String promoCode){
		WaitTool.waitForElement(driver, itemPromoCodeInputCssLocator, WAIT_4_ELEMENT).sendKeys(promoCode);
	}
	
	//Asserts
	public void thankYouPageLocatorsCheck() {
		Assert.assertTrue(Misc.checklocators(getThankYouPageLocators(), driver), "All locators should be present");
	}
	
	public void checkThankYouMessage() {
		Assert.assertTrue(driver.findElement(By.xpath(thankYouMessage)).getText().equals(thankYouMessageValue),"Wrong message !");
	}
	
}
