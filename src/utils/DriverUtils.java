package utils;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;

public class DriverUtils {
	private static final int 	WAIT_4_ELEMENT		= WaitTool.DEFAULT_WAIT_4_ELEMENT;
	private static String 		valueAttribute		= "value";
	
	public static void click(By locator, WebDriver driver){
		WebElement elementToClick = WaitTool.waitForElementClickable(driver, locator, WAIT_4_ELEMENT);
         if (DriverUtils.checkBrowser(driver).equals("chrome")) {
        	 WaitTool.waitForJavaScriptCondition(driver, "window.scrollTo(0, (" + elementToClick.getLocation().y + "- (window.innerHeight / 2)))", 1);
         } 
         elementToClick.click();
	}
	
	public static void clickElement(WebElement elementToClick, WebDriver driver){
		 Capabilities capabilities = ((RemoteWebDriver) driver).getCapabilities();
		 
         if (capabilities.getBrowserName().equals("chrome")) {
        	 WaitTool.waitForJavaScriptCondition(driver, "window.scrollTo(0, (" + elementToClick.getLocation().y + "- (window.innerHeight / 2)))", 1);
         } 
         elementToClick.click();
	}
	
	public static void clickChildElement(WebElement parent, By locator, WebDriver driver){
		WebElement elementToClick = parent.findElement(locator);
		 Capabilities capabilities = ((RemoteWebDriver) driver).getCapabilities();
		 
         if (capabilities.getBrowserName().equals("chrome")) {
        	 WaitTool.waitForJavaScriptCondition(driver, "window.scrollTo(0, (" + elementToClick.getLocation().y + "- (window.innerHeight / 2)))", 1);
         } 
         elementToClick.click();
	}
	
	public static void selectDropDownOptionByVisibleText(WebDriver driver, By locator, String visibleText){
		WebElement element = WaitTool.waitForElement(driver, locator, WAIT_4_ELEMENT);
		Select selectByVisibleText = new Select(element);
		selectByVisibleText.selectByVisibleText(visibleText);
	}
	
	public static String checkBrowser(WebDriver driver){
	 Capabilities capabilities = ((RemoteWebDriver) driver).getCapabilities();
		 return capabilities.getBrowserName();
	}
	
	public static void setElementValue(By elementLocator, String value, WebDriver driver){
		WebElement element = WaitTool.waitForElement(driver, elementLocator, WAIT_4_ELEMENT);
		element.clear();
		element.sendKeys(value);
	}
	
	public static String getElementValueAttribute(By elementLocator, WebDriver driver){
		return WaitTool.waitForElement(driver, elementLocator, WAIT_4_ELEMENT).getAttribute(valueAttribute);
	}
	
	public static int getTableElementsCount(By tableIdLocator, WebDriver driver){
		List<WebElement> tableItems = WebTable.getElementsFromColumnByIdLocator(tableIdLocator, 1, driver);
		return tableItems.size();
	}
	
	public static void clickIfExistingRemoveButtons(By removeBtnLocator, WebDriver driver){
		List<WebElement> removeBtnElements = WaitTool.waitForListElementsPresent(driver, removeBtnLocator, WAIT_4_ELEMENT);
		if(removeBtnElements != null){
			for (WebElement webElement : removeBtnElements) {
				if(webElement.isDisplayed()){
					DriverUtils.clickElement(webElement, driver);
					WaitTool.waitForJQueryProcessing(driver, 5);
				}
			}
		}
	}
}
