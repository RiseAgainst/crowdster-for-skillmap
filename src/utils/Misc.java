package utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.testng.Reporter;

public class Misc {
	
	public static boolean stringArraysEqual(String[] actual, String[] expected){
		
		if (actual.length != expected.length){ 
			return false;
		}
	    
		for (int i = 0; i < actual.length; i++){
	        if (!actual[i].equals(expected[i])){
	        	System.out.println("Actual: "+actual[i]+", but Expected: "+expected[i]);
	            return false;
	        }
	    }
	    return true;
	}
	
	public static boolean intArraysEqual(int[] actual, int[] expected){
	
		if (actual.length != expected.length){ 
			return false;
		}
	    
		for (int i = 0; i < actual.length; i++){
	        if (!(actual[i] == expected[i])){
	        	System.out.println("Actual: "+actual[i]+", but Expected: "+expected[i]);
	            return false;
	        }
	    }
	    return true;
	}
	
	
	public static boolean checklocators(ArrayList<String> locators, WebDriver driver) {
		boolean locatorsOK = true;
		for (String entry : locators)
		{
			String string = entry;
			int splitter = string.indexOf(",");
			int splitter2 = string.lastIndexOf(",");
			String by = string.substring(0, splitter);
			String locator = string.substring(splitter + 1, splitter2);
			if (by.equals("id"))
			{
				try
				{
					driver.findElement(By.id(locator));
				}
				catch (NoSuchElementException e)
				{
					String element = string.substring(splitter2 + 1);
					System.out.println("Can't find an element: " + element);
					Reporter.log("<span style='color:red'>Can't find an element: </span>" + element + "<br>");
					locatorsOK = false;
				}
			}
			else if (by.equals("name"))
			{
				try
				{
					driver.findElement(By.name(locator));
				}
				catch (NoSuchElementException e)
				{
					String element = string.substring(splitter2 + 1);
					System.out.println("Can't find an element: " + element);
					Reporter.log("<span style='color:red'>Can't find an element: </span>" + element + "<br>");
					locatorsOK = false;
				}
			}
			else if (by.equals("xpath"))
			{
				try
				{
					driver.findElement(By.xpath(locator));
				}
				catch (NoSuchElementException e)
				{
					String element = string.substring(splitter2 + 1);
					System.out.println("Can't find an element: " + element);
					Reporter.log("<span style='color:red'>Can't find an element: </span>" + element + "<br>");
					locatorsOK = false;
				}
			}
			else if (by.equals("css"))
			{
				try
				{
					driver.findElement(By.cssSelector(locator));
				}
				catch (NoSuchElementException e)
				{
					String element = string.substring(splitter2 + 1);
					System.out.println("Can't find an element: " + element);
					Reporter.log("<span style='color:red'>Can't find an element: </span>" + element + "<br>");
					locatorsOK = false;
				}
			}
			else if (by.equals("classname"))
			{
				try
				{
					driver.findElement(By.className(locator));
				}
				catch (NoSuchElementException e)
				{
					String element = string.substring(splitter2 + 1);
					System.out.println("Can't find an element: " + element);
					Reporter.log("<span style='color:red'>Can't find an element: </span>" + element + "<br>");
					locatorsOK = false;
				}
			}
			else
			{
				System.out.println("Add findElement by " + by);
				locatorsOK = false;
			}
		}
		return locatorsOK;
	}
	
	public static String getCurrentDate(String dateFormatType) {
		DateFormat dateFormat = new SimpleDateFormat();
		Date date = new Date();
		switch(dateFormatType) {
			case "Day":
				dateFormat = new SimpleDateFormat("d");
				break;
			case "ReportFormat":
				dateFormat = new SimpleDateFormat("MM/dd/yyyy");	
				break;
			case "EventFormat":
				dateFormat = new SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH);
				break;
			case  "MsFormat":
				dateFormat = new SimpleDateFormat("MMMMd,yyyy",Locale.ENGLISH);
		}
		return dateFormat.format(date);
	}
	
}
