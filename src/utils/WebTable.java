package utils;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class WebTable {
	private WebElement			mWebTable;
	private WebDriver	driver;
	
	public WebTable(WebElement _webTable, WebDriver driver) {
		this.driver = driver;
		WebElement tbody = _webTable.findElement(By.tagName("tbody"));
		setWebTable(tbody);
	}
	
	public void setWebTable(WebElement _webTable) {
		this.mWebTable = _webTable;
	}
	
	public int getRowCount() {
		List<WebElement> tableRows = mWebTable.findElements(By.tagName("tr"));
		WebElement emptyElement = WaitTool.waitForElement(driver, By.cssSelector("td.dataTables_empty"), WaitTool.DEFAULT_WAIT_4_ELEMENT);
		
		if(emptyElement != null){
			return 0;
		}
		
        ArrayList<WebElement> id = new ArrayList<WebElement>();
        int i = 0 ;
        while(i < tableRows.size()){
            String s = tableRows.get(i).getAttribute("style");
            if(s.equals("display: none;")){
                i++;
            }else{
                id.add(tableRows.get(i));
                i++;
            }
       }

        return id.size();
	}
	
	public int getColumnCount() {
		List<WebElement> tableRows = mWebTable.findElements(By.tagName("tr"));
		WebElement headerRow = tableRows.get(0);
		List<WebElement> tableCols = headerRow.findElements(By.tagName("td"));
		return tableCols.size();
	}
	
	public WebElement getCellData(int rowIdx, int colIdx) {
		List<WebElement> tableRows = mWebTable.findElements(By.tagName("tr"));
        ArrayList<WebElement> id = new ArrayList<WebElement>();
        for(int i=0; i < rowIdx;i++) {
        	id.add(tableRows.get(i));
        }
        WebElement currentRow = id.get(rowIdx-1);
		List<WebElement> tableCols = currentRow.findElements(By.tagName("td"));
		WebElement cell = tableCols.get(colIdx-1);
		return cell;
	}

	public static WebElement getElement(String tablename, int row, int column, WebDriver mDriver) {
		WebElement tableElement = mDriver.findElement(By.xpath(tablename));
		WebTable table = new WebTable(tableElement, mDriver);
		WebElement link = table.getCellData(row, column);
		return link;
	}
	
	public static WebElement getFirstElementFromColumnAsWebElement(String tablename, int column, WebDriver mDriver) {
		WebElement tableElement = mDriver.findElement(By.xpath(tablename));
		WebTable table = new WebTable(tableElement, mDriver);
		WebElement link = table.getCellData(1, column);
		return link;
	}
	
	public static WebElement getFirstElementFromColumnAsWebElementByIdLocator(By idLocator, int column, WebDriver mDriver) {
		WebElement tableElement = mDriver.findElement(idLocator);
		WebTable table = new WebTable(tableElement, mDriver);
		WebElement link = table.getCellData(1, column);
		return link;
	}
	
	public static String getFirstElementFromColumn(String tablename, int column, WebDriver mDriver) {
		WebElement tableElement = mDriver.findElement(By.xpath(tablename));
		WebTable table = new WebTable(tableElement, mDriver);
		WebElement link = table.getCellData(1, column);
		return link.getText();
	}
	
	public static String getLastElementFromColumn(String tablename, int column, WebDriver mDriver) {
		WebElement tableElement = mDriver.findElement(By.xpath(tablename));
		WebTable table = new WebTable(tableElement, mDriver);
		WebElement link = table.getCellData(table.getRowCount(), column);
		return link.getText();
	}
	
	public static WebElement getLastElementFromColumnAsWebElement(String tablename, int column, WebDriver mDriver) {
		WebElement tableElement = mDriver.findElement(By.id(tablename));
		WebTable table = new WebTable(tableElement, mDriver);
		WebElement link = table.getCellData(table.getRowCount(), column);
		return link;
	}
	
	public static WebElement getLastElementFromColumnAsWebElementByIdLocator(By locator, int column, WebDriver mDriver) {
		WebElement tableElement = mDriver.findElement(locator);
		WebTable table = new WebTable(tableElement, mDriver);
		WebElement link = table.getCellData(table.getRowCount(), column);
		return link;
	}
	
	public static String[] getDataFromColumn(String tablename, int column, WebDriver mDriver) {
		WebElement tableElement = mDriver.findElement(By.xpath(tablename));
		WebTable table = new WebTable(tableElement, mDriver);
		// Getting number of rows with data in the table
		int rows = table.getRowCount();
		// Creating an array and filling it with the data
		String[] data = new String[rows];
		for (int i = 1; i <= rows; i++)
		{
			String temp = table.getCellData(i, column).getText();
			data[i - 1] = temp;
		}
		return data;
	}
	
	public static int[] getIntegerArrayDataFromColumn(String tablename, int column, WebDriver mDriver) {
		WebElement tableElement = mDriver.findElement(By.xpath(tablename));
		WebTable table = new WebTable(tableElement, mDriver);
		// Getting number of rows with data in the table
		int rows = table.getRowCount();
		// Creating an array and filling it with the data
		int[] data = new int[rows];
		for (int i = 0; i < rows; i++)
		{
			String cellData = table.getCellData(i + 1, column).getText();
			data[i] = Integer.parseInt(cellData);
		}
		return data;
	}
	
	public static List<WebElement> getElementsFromColumn(String tablename, int column, WebDriver mDriver) {
		WebElement tableElement = mDriver.findElement(By.xpath(tablename));
		WebTable table = new WebTable(tableElement, mDriver);
		int rows = table.getRowCount();
		List<WebElement> data = new ArrayList<WebElement>();
		for (int i = 1; i <= rows; i++)
		{
			data.add(table.getCellData(i, column));
		}
		return data;
	}
	
	public static List<WebElement> getElementsFromColumnByIdLocator(By idLocator, int column, WebDriver mDriver) {
		WebElement tableElement = mDriver.findElement(idLocator);
		WebTable table = new WebTable(tableElement, mDriver);
		int rows = table.getRowCount();
		List<WebElement> data = new ArrayList<WebElement>();
		for (int i = 1; i <= rows; i++)
		{
			data.add(table.getCellData(i, column));
		}
		return data;
	}
	
	public static List<WebElement> getElementsFromColumnByTableElement(WebElement tableElement, int column, WebDriver mDriver) {
		WebTable table = new WebTable(tableElement, mDriver);
		int rows = table.getRowCount();
		List<WebElement> data = new ArrayList<WebElement>();
		for (int i = 1; i <= rows; i++)
		{
			data.add(table.getCellData(i, column));
		}
		return data;
	}
	
	public static String[] getDataFromLastRow(String tablename, WebDriver mDriver) {
		WebElement tableElement = mDriver.findElement(By.xpath(tablename));
		WebTable table = new WebTable(tableElement, mDriver);
		// Getting number of rows with data in the table
		int rows = table.getRowCount();
		int column = table.getColumnCount();
		// Creating an array and filling it with the data
		String[] data = new String[column];
		for (int i = 1; i <= column; i++)
		{
			String temp = table.getCellData(rows, i).getText();
			data[i - 1] = temp;
		}
		return data;
	}

}
