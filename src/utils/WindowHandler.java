package utils;

import java.util.Set;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

public class WindowHandler {
	public static String originalWindow;
	public Set<String> oldWindowsSet;
	
	public void getOldWindow(WebDriver driver) {
		originalWindow = driver.getWindowHandle();
		oldWindowsSet = driver.getWindowHandles();
	}

	public void switchToNewWindow(WebDriver driver) {
		String newWindow = (new WebDriverWait(driver, 10))
	            .until(new ExpectedCondition<String>() {
	                public String apply(WebDriver driver) {
	                    Set<String> newWindowsSet = driver.getWindowHandles();
	                    newWindowsSet.removeAll(oldWindowsSet);
	                    return newWindowsSet.size() > 0 ? 
	                                 newWindowsSet.iterator().next() : null;
	                  }
	                }
	            );
	 driver.switchTo().window(newWindow);
	 
}


	 public void switchToOldWindow(WebDriver driver) {
		 driver.close();
	     driver.switchTo().window(originalWindow); 
	 }
	
	
}
