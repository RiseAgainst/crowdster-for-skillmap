package utils;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class FileUploader {
	private WebDriver driver;
	private static String uploadButtonName = "file";
	
	private static By uploadNewFileIdLocator		= By.id("file-addToLib");
	private static By fileInputNameLocator			= By.name("file");
	private static By fileSubmitBtnIdLocator		= By.id("uploadmediaSubmit");
	
	public FileUploader(WebDriver driver){
		this.driver = driver;
	}

	public void uploadFile(String fileName) {
		String separator = System.getProperty("file.separator");
		String path = System.getProperty("user.dir") + separator + "files" + separator + fileName;
		driver.findElement(By.name(uploadButtonName)).sendKeys(path);
		utils.WaitTool.waitForJQueryProcessing(driver, 15);
	}
	
	public void uploadFile(String fileName, By locator) {
		String separator = System.getProperty("file.separator");
		String path = System.getProperty("user.dir") + separator + "files" + separator + fileName;
		WaitTool.waitForElement(driver, locator, WaitTool.DEFAULT_WAIT_4_ELEMENT).sendKeys(path);
		WaitTool.waitForJQueryProcessing(driver, 15);
	}
	
	public void selectUploadFileImage(String fileName){
		DriverUtils.click(uploadNewFileIdLocator, driver);
		this.uploadFile(fileName, fileInputNameLocator);
		DriverUtils.click(fileSubmitBtnIdLocator, driver);
	}

}
