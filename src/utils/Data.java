package utils;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

public class Data {
	
	public static String where 						= getData("where");
	public static String baseURL 					= getData("url");
	public static String userLogin 					= getData("userLogin");
	public static String readOnlyUserLogin 			= getData("readOnlyUserLogin");
	public static String userPassword				= getData("userPassword");	
	public static String newPassword				= getData("newPassword");
	public static String newEmail					= getData("newEmail");
	public static String donationAmount				= getData("donationAmount");
	public static String donationAmountForReports	= getData("donationAmoutForReports");
	public static String organizationDashboardUrl	= getData("organizationDashboardUrl");
	public static String browser					= getData("browser");
	public static String organizationName			= "createdSolution";
	public static String organizationNameErr		= "afterErrSolution";
	public static String uniqueSolName				= "UniqueOrganization";
	public static String organizationRename			= "renamedSolution";
	public static String organizationForTest		= "TestSolutionName";
	public static String testEmail					= "po4ta416@ukr.net";
	public static String createdMsName				= "new event";
	public static String green						= "#36b53c";
	public static String red						= "#e02d2d";
	public static String yellow						= "#d7de8e";
	public static String blue						= "#394cdb";
	public static String htmlBannerValue			= "test banner";
	public static String testLogoImageName			= "testLogo.png";
	public static String randomLogoImageName		= "randomLogo.png";
		
	public static String getData(String data) {
		File xmlFile = new File("data.xml");
		Document document = null;
		DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder documentBuilder;
		try
		{
			documentBuilder = documentBuilderFactory.newDocumentBuilder();
			document = documentBuilder.parse(xmlFile);
			document.getDocumentElement().normalize();
		}
		catch (ParserConfigurationException e)
		{
			System.out.println("Parser Configuration Exception");
			e.printStackTrace();
		}
		catch (SAXException e)
		{
			System.out.println("SAX Exception");
			e.printStackTrace();
		}
		catch (IOException e)
		{
			System.out.println("IO Exception");
			e.printStackTrace();
		}

		String result = document.getDocumentElement().getElementsByTagName(data).item(0).getChildNodes().item(0).getNodeValue();
		return result;
	}

}
