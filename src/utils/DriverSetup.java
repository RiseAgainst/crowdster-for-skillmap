package utils;

import java.net.MalformedURLException;


import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.LocalFileDetector;
import org.openqa.selenium.remote.RemoteWebDriver;

public class DriverSetup {
	private static WebDriver		driver;
	private static FirefoxProfile 	profile = new FirefoxProfile();
	private static String 			where 	= Data.where;
	private static String 			browser = Data.browser;

	public static WebDriver getDriver(String driverType) {
		profile.setPreference("browser.helperApps.neverAsk.saveToDisk", "application/pdf, application/vnd.fdf, application/x-msdos-program, "
				+ "application/x-unknown-application-octet-stream, application/vnd.ms-powerpoint, application/excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,"
				+ "application/vnd.ms-publisher, application/x-unknown-message-rfc822, application/vnd.ms-excel, application/msword, application/x-mspublisher, application/x-tar, "
				+ "application/zip, application/x-gzip,application/x-stuffit,application/vnd.ms-works, application/powerpoint, "
				+ "application/rtf, application/postscript, application/x-gtar, video/quicktime, video/x-msvideo, video/mpeg, "
				+ "audio/x-wav, audio/x-midi, audio/x-aiff, application/vnd.ms-excel, application/vnd.ms-office, appliation/msexcel, application/x-msexcel, "
				+ "application/x-ms-excel, application/x-excel, application/x-dos_ms_excel, application/xls, application/x-xls"
				+ "text/csv, application/csv");
		if(driverType.equals("mobile")) {
			String userAgent = "Mozilla/5.0 (Linux; U; Android 4.0.3; ko-kr; LG-L160L Build/IML74K) AppleWebkit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30";
			profile.setPreference("general.useragent.override", userAgent);
			browser = "firefox";
		}
		if (where.equals("remote")) // in case if we will need remote test launch
		{
			try
			{
				DesiredCapabilities capability = DesiredCapabilities.firefox();
				capability.setCapability(FirefoxDriver.PROFILE, profile);
				driver = new RemoteWebDriver(new URL("In case of remote testing (adress)"), capability);
				driver = new Augmenter().augment(driver);
				((RemoteWebDriver) driver).setFileDetector(new LocalFileDetector());
			}
			catch (MalformedURLException e)
			{
				System.out.println("Something went terribly wrong");
				e.printStackTrace();
			}
		}
		else if (where.equals("local"))
		{	
			if(browser.equalsIgnoreCase("Firefox")){
				DesiredCapabilities capability = DesiredCapabilities.firefox();
				capability.setCapability(FirefoxDriver.PROFILE, profile);
				//capability.setCapability(CapabilityType.PAGE_LOAD_STRATEGY, "eager");
				driver = new FirefoxDriver(capability);
			} else if (browser.equalsIgnoreCase("Chrome")){
				System.setProperty("webdriver.chrome.driver", "C:\\workspace\\Crowdster-tests\\browsers-drivers\\chromedriver.exe");
				driver = new ChromeDriver();
			} else if (browser.equalsIgnoreCase("ie")) {
                System.setProperty("webdriver.ie.driver", "C:\\workspace\\Crowdster-tests\\browsers-drivers\\IEDriverServer.exe");
                DesiredCapabilities dc = DesiredCapabilities.internetExplorer();
                //dc.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);  //If IE fail to work, please remove this and remove enable protected mode for all the 4 zones from Internet options
                driver = new InternetExplorerDriver(dc);
			} 
		}
		else
		{
			DesiredCapabilities capability = DesiredCapabilities.firefox();
			capability.setCapability(FirefoxDriver.PROFILE, profile);
			driver = new FirefoxDriver(capability);
		}
		return driver;
	}

	public static void initialSetup(WebDriver driver) {
		String loginURL = Data.baseURL;
		driver.manage().window().maximize(); // maximize window to avoid user-menu dropdown
		driver.manage().timeouts().implicitlyWait(7, TimeUnit.SECONDS);
		driver.get(loginURL);
	}
	
}
