-- MySQL dump 10.13  Distrib 5.5.44, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: markslist
-- ------------------------------------------------------
-- Server version	5.5.44-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `activity`
--

DROP TABLE IF EXISTS `activity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `activity` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `deleted` datetime DEFAULT NULL,
  `objectId` bigint(20) DEFAULT NULL,
  `objectType` int(11) DEFAULT NULL,
  `activityType` int(11) DEFAULT NULL,
  `value` float DEFAULT NULL,
  `memberId` bigint(20) DEFAULT NULL,
  `firstName` varchar(55) DEFAULT NULL,
  `lastName` varchar(55) DEFAULT NULL,
  `fromCampaignId` bigint(20) DEFAULT NULL,
  `offlineDonationId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `activity`
--

LOCK TABLES `activity` WRITE;
/*!40000 ALTER TABLE `activity` DISABLE KEYS */;
INSERT INTO `activity` VALUES (1,'2015-09-01 12:46:45',NULL,1362146005771,1,2,120,1362146005764,'Ron','Red',0,0);
/*!40000 ALTER TABLE `activity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `administrator`
--

DROP TABLE IF EXISTS `administrator`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `administrator` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `created` datetime DEFAULT NULL,
  `isSuperuser` tinyint(4) DEFAULT NULL,
  `isFactchecker` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `administrator`
--

LOCK TABLES `administrator` WRITE;
/*!40000 ALTER TABLE `administrator` DISABLE KEYS */;
INSERT INTO `administrator` VALUES (1,'factcheck','factcheck@411karma.com','F5m4YfpD9sC88OXVg6Kl9Ml1hsQ=','2005-04-28 12:30:00',0,1),(2,'admin','admin@411karma.com','HzfPyxc9E/EMoC/4mebtPMPHlg4=','2005-04-28 12:30:00',1,0),(3,'Michael Mintz','michaelm@vwainc.com','9lPD4BZ0GkZLujvOHlTUOnqF5qI=','2007-01-18 05:59:49',0,1),(4,'Felisa Diggs','felisad@vwainc.com','F3MIHoNwET6OBYkK83yaaX4L6P4=','2007-01-18 05:59:58',0,1),(5,'Demetria Katsanos','demetriak@vwainc.com','oh+hLcsMAh4GFA1JDUYfjDvAdeg=','2007-02-19 13:12:09',0,1),(6,'Test Neelam','great@karma411.com','HzfPyxc9E/EMoC/4mebtPMPHlg4=','2010-05-11 02:22:49',1,0);
/*!40000 ALTER TABLE `administrator` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `alias`
--

DROP TABLE IF EXISTS `alias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alias` (
  `id` bigint(20) NOT NULL,
  `created` datetime DEFAULT NULL,
  `memberId` bigint(20) NOT NULL,
  `embedId` bigint(20) NOT NULL,
  `embedSystemId` bigint(20) NOT NULL,
  `siteId` bigint(20) DEFAULT NULL,
  `type` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alias`
--

LOCK TABLES `alias` WRITE;
/*!40000 ALTER TABLE `alias` DISABLE KEYS */;
/*!40000 ALTER TABLE `alias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `allowedhosts`
--

DROP TABLE IF EXISTS `allowedhosts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `allowedhosts` (
  `id` bigint(20) NOT NULL,
  `created` datetime DEFAULT NULL,
  `host_name` varchar(50) DEFAULT NULL,
  `host_url` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `allowedhosts`
--

LOCK TABLES `allowedhosts` WRITE;
/*!40000 ALTER TABLE `allowedhosts` DISABLE KEYS */;
/*!40000 ALTER TABLE `allowedhosts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `analyticsdata`
--

DROP TABLE IF EXISTS `analyticsdata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `analyticsdata` (
  `id` bigint(20) NOT NULL DEFAULT '0',
  `recordDateTime` datetime DEFAULT NULL,
  `hostName` varchar(200) DEFAULT NULL,
  `pagePath` varchar(400) DEFAULT NULL,
  `hours` int(11) DEFAULT NULL,
  `campaignId` bigint(20) DEFAULT NULL,
  `memberId` bigint(20) DEFAULT NULL,
  `siteId` mediumtext,
  `superSiteId` bigint(20) DEFAULT NULL,
  `pageViews` int(11) DEFAULT NULL,
  `uniquePageViews` int(11) DEFAULT NULL,
  `visitors` int(11) DEFAULT NULL,
  `newVisits` int(11) DEFAULT NULL,
  `analyticsDate` datetime DEFAULT NULL,
  `month` int(11) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `analyticsdata`
--

LOCK TABLES `analyticsdata` WRITE;
/*!40000 ALTER TABLE `analyticsdata` DISABLE KEYS */;
/*!40000 ALTER TABLE `analyticsdata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `appearancedetails`
--

DROP TABLE IF EXISTS `appearancedetails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `appearancedetails` (
  `id` bigint(20) NOT NULL DEFAULT '0',
  `siteId` bigint(20) DEFAULT NULL,
  `navbackgroundcolor` varchar(8) DEFAULT NULL,
  `navbackgroundcolorgradienttop` varchar(8) DEFAULT NULL,
  `navbackgroundcolorgradientbottom` varchar(8) DEFAULT NULL,
  `navtextcolor` varchar(8) DEFAULT NULL,
  `navitembackgroundcolor` varchar(8) DEFAULT NULL,
  `navitembackgroundcolorgradienttop` varchar(8) DEFAULT NULL,
  `navitembackgroundcolorgradientbottom` varchar(8) DEFAULT NULL,
  `navtextcoloron` varchar(8) DEFAULT NULL,
  `navitembackgroundcoloron` varchar(8) DEFAULT NULL,
  `navitembackgroundcolorgradienttopon` varchar(8) DEFAULT NULL,
  `navitembackgroundcolorgradientbottomon` varchar(8) DEFAULT NULL,
  `navtextcolorhover` varchar(8) DEFAULT NULL,
  `navitembackgroundcolorhover` varchar(8) DEFAULT NULL,
  `navitembackgroundcolorgradienttophover` varchar(8) DEFAULT NULL,
  `navitembackgroundcolorgradientbottomhover` varchar(8) DEFAULT NULL,
  `buttontextcolor` varchar(8) DEFAULT NULL,
  `buttonitembackgroundcolor` varchar(8) DEFAULT NULL,
  `buttonitembackgroundcolorgradienttop` varchar(8) DEFAULT NULL,
  `buttonitembackgroundcolorgradientbottom` varchar(8) DEFAULT NULL,
  `buttontextcoloron` varchar(8) DEFAULT NULL,
  `buttonitembackgroundcoloron` varchar(8) DEFAULT NULL,
  `buttonitembackgroundcolorgradienttopon` varchar(8) DEFAULT NULL,
  `buttonitembackgroundcolorgradientbottomon` varchar(8) DEFAULT NULL,
  `buttontextcolorhover` varchar(8) DEFAULT NULL,
  `buttonitembackgroundcolorhover` varchar(8) DEFAULT NULL,
  `buttonitembackgroundcolorgradienttophover` varchar(8) DEFAULT NULL,
  `buttonitembackgroundcolorgradientbottomhover` varchar(8) DEFAULT NULL,
  `widgetheadertextcolor` varchar(8) DEFAULT NULL,
  `widgetheaderbackgroundcolor` varchar(8) DEFAULT NULL,
  `widgetheaderbackgroundcolorgradienttop` varchar(8) DEFAULT NULL,
  `widgetheaderbackgroundcolorgradientbottom` varchar(8) DEFAULT NULL,
  `mainbackgroundcolor` varchar(8) DEFAULT NULL,
  `footerbackgroundcolor` varchar(8) DEFAULT NULL,
  `footerbackgroundcolorgradientbottom` varchar(8) DEFAULT NULL,
  `footerbackgroundcolorgradienttop` varchar(8) DEFAULT NULL,
  `footertextcolor` varchar(8) DEFAULT NULL,
  `footerlinkcolor` varchar(8) DEFAULT NULL,
  `searchboxtextcolor` varchar(8) DEFAULT NULL,
  `searchboxbackgroundcolor` varchar(8) DEFAULT NULL,
  `searchboxbackgroundcolorgradientbottom` varchar(8) DEFAULT NULL,
  `searchboxbackgroundcolorgradienttop` varchar(8) DEFAULT NULL,
  `logintextcolor` varchar(8) DEFAULT NULL,
  `themeId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `appearancedetails_siteId` (`siteId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `appearancedetails`
--

LOCK TABLES `appearancedetails` WRITE;
/*!40000 ALTER TABLE `appearancedetails` DISABLE KEYS */;
INSERT INTO `appearancedetails` VALUES (1362145798080,1362145798075,'#033e63',NULL,NULL,NULL,'#3871d9',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0),(1362146005148,1362146005143,'#033e63',NULL,NULL,NULL,'#3871d9',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0),(1362146005157,1362146005152,'#033e63',NULL,NULL,NULL,'#3871d9',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0),(1362146005166,1362146005161,'#033e63',NULL,NULL,NULL,'#3871d9',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0),(1362146005175,1362146005170,'#033e63',NULL,NULL,NULL,'#3871d9',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0),(1362146005184,1362146005179,'#033e63',NULL,NULL,NULL,'#3871d9',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0),(1362146005193,1362146005188,'#033e63',NULL,NULL,NULL,'#3871d9',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0);
/*!40000 ALTER TABLE `appearancedetails` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `approvalstatus`
--

DROP TABLE IF EXISTS `approvalstatus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `approvalstatus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `objectType` int(11) DEFAULT NULL,
  `objectId` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `approvalstatus`
--

LOCK TABLES `approvalstatus` WRITE;
/*!40000 ALTER TABLE `approvalstatus` DISABLE KEYS */;
/*!40000 ALTER TABLE `approvalstatus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auctionconfig`
--

DROP TABLE IF EXISTS `auctionconfig`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auctionconfig` (
  `id` bigint(20) unsigned NOT NULL,
  `siteId` bigint(20) DEFAULT NULL,
  `emailText` varchar(500) DEFAULT NULL,
  `auctionTermsConditions` varchar(500) DEFAULT NULL,
  `auctionRegistrationEmail` varchar(500) DEFAULT NULL,
  `bidNotificationEmail` varchar(500) DEFAULT NULL,
  `outBidNotificationEmail` varchar(500) DEFAULT NULL,
  `winnerEmail` varchar(500) DEFAULT NULL,
  `ask_billing_address` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auctionconfig`
--

LOCK TABLES `auctionconfig` WRITE;
/*!40000 ALTER TABLE `auctionconfig` DISABLE KEYS */;
/*!40000 ALTER TABLE `auctionconfig` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auctionitem`
--

DROP TABLE IF EXISTS `auctionitem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auctionitem` (
  `id` bigint(20) NOT NULL DEFAULT '0',
  `itemcode` varchar(45) NOT NULL,
  `source` varchar(45) DEFAULT NULL,
  `actualvalue` float DEFAULT NULL,
  `openingprice` float NOT NULL,
  `buynowprice` float NOT NULL,
  `endtime` datetime DEFAULT NULL,
  `quantity` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `incrementvalue` float NOT NULL DEFAULT '0',
  `hasliveauction` tinyint(1) DEFAULT NULL,
  `createdtime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastmodifiedtime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `campaignitemid` bigint(20) DEFAULT NULL,
  `winnerId` bigint(20) DEFAULT NULL,
  `closed` tinyint(1) unsigned NOT NULL,
  `rank` tinyint(3) unsigned DEFAULT NULL,
  `showOnAuctionList` tinyint(4) DEFAULT '1',
  `startTime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `campaignitemid` (`campaignitemid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auctionitem`
--

LOCK TABLES `auctionitem` WRITE;
/*!40000 ALTER TABLE `auctionitem` DISABLE KEYS */;
/*!40000 ALTER TABLE `auctionitem` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auctionitembid`
--

DROP TABLE IF EXISTS `auctionitembid`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auctionitembid` (
  `id` bigint(20) NOT NULL DEFAULT '0',
  `auctionItemId` bigint(20) DEFAULT NULL,
  `memberId` bigint(20) DEFAULT NULL,
  `value` float DEFAULT NULL,
  `type` int(10) unsigned NOT NULL,
  `ccid` bigint(20) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `customerId` bigint(20) unsigned DEFAULT NULL,
  `siteMemberId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `auctionitemid` (`auctionItemId`),
  KEY `memberid` (`memberId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auctionitembid`
--

LOCK TABLES `auctionitembid` WRITE;
/*!40000 ALTER TABLE `auctionitembid` DISABLE KEYS */;
/*!40000 ALTER TABLE `auctionitembid` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `background`
--

DROP TABLE IF EXISTS `background`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `background` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `memberId` bigint(20) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modTime` datetime DEFAULT NULL,
  `type` tinyint(4) DEFAULT NULL,
  `orgName` varchar(100) DEFAULT NULL,
  `description` varchar(2500) DEFAULT NULL,
  `role` varchar(40) DEFAULT NULL,
  `degree` tinyint(4) DEFAULT NULL,
  `city` varchar(25) DEFAULT NULL,
  `state` varchar(20) DEFAULT NULL,
  `country` int(11) DEFAULT NULL,
  `startYear` year(4) DEFAULT NULL,
  `endYear` year(4) DEFAULT NULL,
  `URL` varchar(60) DEFAULT NULL,
  `factCheckRequestId` int(11) DEFAULT NULL,
  `isUnreachable` tinyint(4) DEFAULT NULL,
  `factCheckTime` datetime DEFAULT NULL,
  `contactInfo` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `memberId` (`memberId`),
  KEY `factCheckRequestId` (`factCheckRequestId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `background`
--

LOCK TABLES `background` WRITE;
/*!40000 ALTER TABLE `background` DISABLE KEYS */;
/*!40000 ALTER TABLE `background` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `campaign`
--

DROP TABLE IF EXISTS `campaign`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `campaign` (
  `id` bigint(20) NOT NULL DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modTime` datetime DEFAULT NULL,
  `endTime` datetime DEFAULT NULL,
  `memberId` bigint(20) DEFAULT NULL,
  `name` varchar(95) DEFAULT NULL,
  `description` varchar(5000) DEFAULT NULL,
  `imagePath` tinyint(4) DEFAULT NULL,
  `filename` varchar(50) DEFAULT NULL,
  `charityId` bigint(20) DEFAULT NULL,
  `url` varchar(150) DEFAULT NULL,
  `financialGoal` float DEFAULT NULL,
  `charityName` varchar(85) DEFAULT NULL,
  `deleteTime` datetime DEFAULT NULL,
  `type` tinyint(4) DEFAULT NULL,
  `numberGoal` int(11) DEFAULT NULL,
  `teamCampaignId` bigint(20) DEFAULT NULL,
  `masterCampaignId` bigint(20) DEFAULT NULL,
  `pageAbstract` varchar(400) DEFAULT NULL,
  `siteId` bigint(20) DEFAULT NULL,
  `deadline` datetime DEFAULT NULL,
  `mediaURL` varchar(500) DEFAULT NULL,
  `needAlerts` tinyint(4) DEFAULT NULL,
  `pollId` bigint(20) DEFAULT NULL,
  `page_image_blob_id` bigint(20) DEFAULT NULL,
  `tributeHeader` varchar(45) DEFAULT NULL,
  `tributeName` varchar(45) DEFAULT NULL,
  `mediaId` bigint(20) DEFAULT '0',
  `teamMediaId` bigint(20) DEFAULT NULL,
  `team_campaign_media_id` bigint(20) DEFAULT NULL,
  `default_team_story` varchar(5000) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `memberId` (`memberId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `campaign`
--

LOCK TABLES `campaign` WRITE;
/*!40000 ALTER TABLE `campaign` DISABLE KEYS */;
INSERT INTO `campaign` VALUES (1362145798074,'2015-08-31 17:56:35','2015-08-31 17:56:35',NULL,1362146005137,'One more test',NULL,0,NULL,1362145798073,NULL,100,NULL,NULL,0,5,NULL,NULL,NULL,0,NULL,NULL,0,NULL,0,NULL,NULL,0,0,0,NULL),(1362146005142,'2015-08-31 07:23:04','2015-08-31 07:23:04',NULL,1362146005137,'FirstSupersite',NULL,0,NULL,1362146005141,NULL,100,NULL,NULL,0,5,NULL,NULL,NULL,0,NULL,NULL,0,NULL,0,NULL,NULL,0,0,0,NULL),(1362146005151,'2015-08-31 07:23:22','2015-08-31 07:23:22',NULL,1362146005137,'SecondSupersite',NULL,0,NULL,1362146005150,NULL,100,NULL,NULL,0,5,NULL,NULL,NULL,0,NULL,NULL,0,NULL,0,NULL,NULL,0,0,0,NULL),(1362146005160,'2015-08-31 07:23:41','2015-08-31 07:23:41',NULL,1362146005137,'ThirdSupersite',NULL,0,NULL,1362146005159,NULL,100,NULL,NULL,0,5,NULL,NULL,NULL,0,NULL,NULL,0,NULL,0,NULL,NULL,0,0,0,NULL),(1362146005169,'2015-08-31 07:24:08','2015-08-31 07:24:08',NULL,1362146005137,'TestSupersite',NULL,0,NULL,1362146005168,NULL,100,NULL,NULL,0,5,NULL,NULL,NULL,0,NULL,NULL,0,NULL,0,NULL,NULL,0,0,0,NULL),(1362146005178,'2015-08-31 07:27:42','2015-08-31 07:27:42',NULL,1362146005137,'SoftwareSS',NULL,0,NULL,1362146005177,NULL,100,NULL,NULL,0,5,NULL,NULL,NULL,0,NULL,NULL,0,NULL,0,NULL,NULL,0,0,0,NULL),(1362146005187,'2015-08-31 07:29:55','2015-08-31 07:29:55',NULL,1362146005137,'TestSsName',NULL,0,NULL,1362146005186,NULL,100,NULL,NULL,0,5,NULL,NULL,NULL,0,NULL,NULL,0,NULL,0,NULL,NULL,0,0,0,NULL),(1362146005198,'2015-08-31 07:37:39','2015-08-31 07:37:39',NULL,1362146005137,'FirstEvent',NULL,0,NULL,1362146005197,NULL,100,NULL,NULL,0,5,NULL,NULL,NULL,0,NULL,NULL,0,NULL,0,NULL,NULL,0,0,0,NULL),(1362146005205,'2015-08-31 07:38:23','2015-08-31 07:38:23',NULL,1362146005137,'AnotherOneEvenet',NULL,0,NULL,1362146005204,NULL,100,NULL,NULL,0,5,NULL,NULL,NULL,0,NULL,NULL,0,NULL,0,NULL,NULL,0,0,0,NULL),(1362146005212,'2015-08-31 07:44:23','2015-08-31 07:44:23',NULL,1362146005137,'Event',NULL,0,NULL,1362146005211,NULL,100,NULL,NULL,0,5,NULL,NULL,NULL,0,NULL,NULL,0,NULL,0,NULL,NULL,0,0,0,NULL),(1362146005219,'2015-08-31 07:47:15','2015-08-31 07:47:15',NULL,1362146005137,'Unique Event',NULL,0,NULL,1362146005218,NULL,100,NULL,NULL,0,5,NULL,NULL,NULL,0,NULL,NULL,0,NULL,0,NULL,NULL,0,0,0,NULL),(1362146005226,'2015-08-31 07:53:07','2015-08-31 07:53:07',NULL,1362146005137,'Anna Event',NULL,0,NULL,1362146005225,NULL,100,NULL,NULL,0,5,NULL,NULL,NULL,0,NULL,NULL,0,NULL,0,NULL,NULL,0,0,0,NULL),(1362146005233,'2015-08-31 07:53:46','2015-08-31 07:53:46',NULL,1362146005137,'Qwe Event',NULL,0,NULL,1362146005232,NULL,100,NULL,NULL,0,5,NULL,NULL,NULL,0,NULL,NULL,0,NULL,0,NULL,NULL,0,0,0,NULL),(1362146005240,'2015-08-31 07:54:48','2015-08-31 07:54:48',NULL,1362146005137,'EventMicro',NULL,0,NULL,1362146005239,NULL,100,NULL,NULL,0,5,NULL,NULL,NULL,0,NULL,NULL,0,NULL,0,NULL,NULL,0,0,0,NULL),(1362146005247,'2015-08-31 07:56:37','2015-08-31 07:56:37',NULL,1362146005137,'FirstOne',NULL,0,NULL,1362146005246,NULL,100,NULL,NULL,0,5,NULL,NULL,NULL,0,NULL,NULL,0,NULL,0,NULL,NULL,0,0,0,NULL),(1362146005254,'2015-08-31 07:57:25','2015-08-31 07:57:25',NULL,1362146005137,'SecondOne',NULL,0,NULL,1362146005253,NULL,100,NULL,NULL,0,5,NULL,NULL,NULL,0,NULL,NULL,0,NULL,0,NULL,NULL,0,0,0,NULL),(1362146005771,'2015-09-01 12:46:45','2015-09-01 12:46:45',NULL,1362146005764,'My support of AnotherOneEvenet','',0,NULL,1362146005204,'ronred',100,NULL,NULL,0,5,NULL,1362146005205,'',1362146005206,NULL,NULL,1,NULL,0,'','',0,0,0,NULL),(1362146005780,'2015-09-01 12:47:23','2015-09-01 12:47:23',NULL,1362146005778,'My support of FirstEvent','',0,NULL,1362146005197,'usertfirst',100,NULL,NULL,0,5,NULL,1362146005198,'',1362146005199,NULL,NULL,1,NULL,0,'','',0,0,0,NULL),(1362146005789,'2015-09-01 12:47:56','2015-09-01 12:47:57',NULL,1362146005787,'My support of FirstEvent','',0,NULL,1362146005197,'teamowner',100,NULL,NULL,0,5,NULL,1362146005796,'',1362146005199,NULL,NULL,1,NULL,0,'','',0,0,0,NULL),(1362146005796,'2015-09-01 12:47:57','2015-09-01 12:47:57',NULL,1362146005787,'Test Team','<p>long story</p>\r\n',0,NULL,1362146005197,'teamowner1',4000,NULL,NULL,6,5,NULL,NULL,NULL,1362146005199,NULL,NULL,0,NULL,0,NULL,NULL,0,0,0,NULL),(1362146005803,'2015-09-01 12:48:32','2015-09-01 12:48:37',NULL,1362146005801,'My support of FirstEvent',NULL,0,NULL,1362146005197,'usersecond',100,'Test organization',NULL,0,5,NULL,1362146005796,'',1362146005199,NULL,NULL,1,NULL,0,'','',0,0,0,NULL),(1362146005812,'2015-09-01 12:48:59','2015-09-01 12:48:59',NULL,1362146005759,'My support of FirstEvent','',0,NULL,1362146005197,'testthird',100,NULL,NULL,0,5,NULL,1362146005198,'',1362146005199,NULL,NULL,1,NULL,0,'','',0,0,0,NULL);
/*!40000 ALTER TABLE `campaign` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `campaign_poll`
--

DROP TABLE IF EXISTS `campaign_poll`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `campaign_poll` (
  `id` bigint(20) DEFAULT NULL,
  `campaignId` bigint(20) DEFAULT NULL,
  `pollId` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `campaign_poll`
--

LOCK TABLES `campaign_poll` WRITE;
/*!40000 ALTER TABLE `campaign_poll` DISABLE KEYS */;
/*!40000 ALTER TABLE `campaign_poll` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `campaigndetails`
--

DROP TABLE IF EXISTS `campaigndetails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `campaigndetails` (
  `id` bigint(20) DEFAULT NULL,
  `url` varchar(150) DEFAULT NULL,
  `deadline` date DEFAULT NULL,
  `causeType` tinyint(4) DEFAULT NULL,
  `mediaURL` varchar(1000) DEFAULT NULL,
  `needAlerts` tinyint(4) DEFAULT NULL,
  `pollId` int(11) DEFAULT NULL,
  `teamId` int(11) DEFAULT NULL,
  `campaignId` bigint(20) DEFAULT NULL,
  `financialInfo` text,
  `page_image_blob_id` bigint(20) DEFAULT NULL,
  KEY `campaigndetails_index_id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `campaigndetails`
--

LOCK TABLES `campaigndetails` WRITE;
/*!40000 ALTER TABLE `campaigndetails` DISABLE KEYS */;
/*!40000 ALTER TABLE `campaigndetails` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `campaignitem`
--

DROP TABLE IF EXISTS `campaignitem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `campaignitem` (
  `id` bigint(20) NOT NULL DEFAULT '0',
  `campaignId` bigint(20) DEFAULT NULL,
  `value` float DEFAULT NULL,
  `taxDeductRate` float DEFAULT NULL,
  `name` varchar(500) DEFAULT NULL,
  `description` varchar(1000) DEFAULT NULL,
  `hasQuantity` tinyint(4) DEFAULT NULL,
  `campaignItemGroupId` bigint(20) DEFAULT NULL,
  `socialValue` float DEFAULT NULL,
  `deleteTime` datetime DEFAULT NULL,
  `fileName` varchar(100) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `multipleRegistrants` tinyint(4) NOT NULL DEFAULT '0',
  `showItem` tinyint(4) NOT NULL DEFAULT '1',
  `onlyDonate` tinyint(4) NOT NULL DEFAULT '0',
  `availableQuantity` int(11) DEFAULT NULL,
  `soldQuantity` int(11) DEFAULT NULL,
  `category` int(11) DEFAULT NULL,
  `blobId` bigint(20) DEFAULT NULL,
  `rankorder` int(11) DEFAULT '0',
  `mediaId` bigint(20) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `campaignId` (`campaignId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `campaignitem`
--

LOCK TABLES `campaignitem` WRITE;
/*!40000 ALTER TABLE `campaignitem` DISABLE KEYS */;
INSERT INTO `campaignitem` VALUES (1362146005819,1362146005198,30,100,'Registration test','<p>reg</p>\r\n',0,0,0,NULL,NULL,0,1,1,0,0,0,3,0,0,0),(1362146005820,1362146005198,25,100,'Sponsorship test','',0,0,0,NULL,NULL,0,1,1,0,0,0,1,0,0,0),(1362146005821,1362146005198,35,100,'Donation test','<p>qwer</p>\r\n',0,0,0,NULL,NULL,0,1,1,0,0,0,2,0,0,0),(1362146005822,1362146005198,45,100,'Reg limit','<p>qwer</p>\r\n',1,0,0,NULL,NULL,0,1,1,0,1,0,3,0,0,0),(1362146005823,1362146005198,35,100,'Sponsorship limit','<p>qwer</p>\r\n',1,0,0,NULL,NULL,0,1,1,0,1,0,1,0,0,0),(1362146005824,1362146005198,50,100,'Donation limit','<p>limit</p>\r\n',1,0,0,NULL,NULL,0,1,1,0,1,0,2,0,0,0),(1362146005825,1362146005198,70,100,'Package test','<p>qwer</p>\r\n',0,0,0,NULL,NULL,1,1,1,0,0,0,3,0,0,0),(1362146005826,1362146005198,80,100,'Limit package','',1,0,0,NULL,NULL,1,1,1,0,1,0,3,0,0,0);
/*!40000 ALTER TABLE `campaignitem` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `campaignitemgroup`
--

DROP TABLE IF EXISTS `campaignitemgroup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `campaignitemgroup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `campaignId` bigint(20) DEFAULT NULL,
  `name` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `campaignId` (`campaignId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `campaignitemgroup`
--

LOCK TABLES `campaignitemgroup` WRITE;
/*!40000 ALTER TABLE `campaignitemgroup` DISABLE KEYS */;
/*!40000 ALTER TABLE `campaignitemgroup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `campaignklogcount`
--

DROP TABLE IF EXISTS `campaignklogcount`;
/*!50001 DROP VIEW IF EXISTS `campaignklogcount`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `campaignklogcount` (
  `id` tinyint NOT NULL,
  `count` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `campaignlog`
--

DROP TABLE IF EXISTS `campaignlog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `campaignlog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `campaignId` bigint(20) DEFAULT NULL,
  `viewingMemberId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `campaignId` (`campaignId`),
  KEY `viewingMemberId` (`viewingMemberId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `campaignlog`
--

LOCK TABLES `campaignlog` WRITE;
/*!40000 ALTER TABLE `campaignlog` DISABLE KEYS */;
/*!40000 ALTER TABLE `campaignlog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `campaignmessage`
--

DROP TABLE IF EXISTS `campaignmessage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `campaignmessage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `body` varchar(12500) DEFAULT NULL,
  `campaignId` bigint(20) DEFAULT NULL,
  `memberId` bigint(20) DEFAULT NULL,
  `type` tinyint(4) DEFAULT NULL,
  `filename` varchar(50) DEFAULT NULL,
  `imagePath` tinyint(4) DEFAULT NULL,
  `title` varchar(16) DEFAULT NULL,
  `mastheadCode` text,
  `customTabContentInFile` int(11) DEFAULT NULL,
  `masthead_image_blob_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `campaignId` (`campaignId`),
  KEY `memberId` (`memberId`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `campaignmessage`
--

LOCK TABLES `campaignmessage` WRITE;
/*!40000 ALTER TABLE `campaignmessage` DISABLE KEYS */;
INSERT INTO `campaignmessage` VALUES (1,'2015-09-01 12:46:45',NULL,1362146005771,1362146005764,5,NULL,0,NULL,'',0,0),(2,'2015-09-01 12:46:45',NULL,1362146005771,1362146005764,1,NULL,0,NULL,NULL,0,0),(3,'2015-09-01 12:46:45',NULL,1362146005771,1362146005764,2,NULL,0,NULL,NULL,0,0),(4,'2015-09-01 12:47:23',NULL,1362146005780,1362146005778,5,NULL,0,NULL,'',0,0),(5,'2015-09-01 12:47:23',NULL,1362146005780,1362146005778,1,NULL,0,NULL,NULL,0,0),(6,'2015-09-01 12:47:23',NULL,1362146005780,1362146005778,2,NULL,0,NULL,NULL,0,0),(7,'2015-09-01 12:47:56',NULL,1362146005789,1362146005787,5,NULL,0,NULL,'',0,0),(8,'2015-09-01 12:47:56',NULL,1362146005789,1362146005787,1,NULL,0,NULL,NULL,0,0),(9,'2015-09-01 12:47:56',NULL,1362146005789,1362146005787,2,NULL,0,NULL,NULL,0,0),(10,'2015-09-01 12:47:57',NULL,1362146005796,1362146005787,1,NULL,0,NULL,NULL,0,0),(11,'2015-09-01 12:47:57',NULL,1362146005796,1362146005787,2,NULL,0,NULL,NULL,0,0),(12,'2015-09-01 12:47:57',NULL,1362146005796,1362146005787,5,NULL,0,NULL,NULL,0,0),(13,'2015-09-01 12:48:32',NULL,1362146005803,1362146005801,5,NULL,0,NULL,'',0,0),(14,'2015-09-01 12:48:32',NULL,1362146005803,1362146005801,1,NULL,0,NULL,NULL,0,0),(15,'2015-09-01 12:48:32',NULL,1362146005803,1362146005801,2,NULL,0,NULL,NULL,0,0),(16,'2015-09-01 12:48:59',NULL,1362146005812,1362146005759,5,NULL,0,NULL,'',0,0),(17,'2015-09-01 12:48:59',NULL,1362146005812,1362146005759,1,NULL,0,NULL,NULL,0,0),(18,'2015-09-01 12:48:59',NULL,1362146005812,1362146005759,2,NULL,0,NULL,NULL,0,0);
/*!40000 ALTER TABLE `campaignmessage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `campaignnotification`
--

DROP TABLE IF EXISTS `campaignnotification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `campaignnotification` (
  `id` bigint(20) NOT NULL,
  `campaign_id` bigint(20) DEFAULT NULL,
  `message` varchar(1000) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `campaignnotification`
--

LOCK TABLES `campaignnotification` WRITE;
/*!40000 ALTER TABLE `campaignnotification` DISABLE KEYS */;
INSERT INTO `campaignnotification` VALUES (1362146005774,1362146005771,'You have created a page','2015-09-01 12:46:45',NULL),(1362146005783,1362146005780,'You have created a page','2015-09-01 12:47:23',NULL),(1362146005792,1362146005789,'You have created a page','2015-09-01 12:47:56',NULL),(1362146005798,1362146005789,'You have created a team','2015-09-01 12:47:57',NULL),(1362146005806,1362146005803,'You have created a page','2015-09-01 12:48:32',NULL),(1362146005815,1362146005812,'You have created a page','2015-09-01 12:48:59',NULL);
/*!40000 ALTER TABLE `campaignnotification` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `campaignnumberdonations`
--

DROP TABLE IF EXISTS `campaignnumberdonations`;
/*!50001 DROP VIEW IF EXISTS `campaignnumberdonations`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `campaignnumberdonations` (
  `id` tinyint NOT NULL,
  `count` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `campaignnumberofflinedonations`
--

DROP TABLE IF EXISTS `campaignnumberofflinedonations`;
/*!50001 DROP VIEW IF EXISTS `campaignnumberofflinedonations`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `campaignnumberofflinedonations` (
  `id` tinyint NOT NULL,
  `count` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `campaignoptout`
--

DROP TABLE IF EXISTS `campaignoptout`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `campaignoptout` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `groupId` int(10) unsigned NOT NULL,
  `campaignId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `campaignoptout`
--

LOCK TABLES `campaignoptout` WRITE;
/*!40000 ALTER TABLE `campaignoptout` DISABLE KEYS */;
/*!40000 ALTER TABLE `campaignoptout` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `campaignsetting`
--

DROP TABLE IF EXISTS `campaignsetting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `campaignsetting` (
  `id` bigint(20) NOT NULL,
  `campaignId` bigint(20) DEFAULT NULL,
  `showStory` tinyint(4) DEFAULT NULL,
  `showMedia` tinyint(4) DEFAULT NULL,
  `private` tinyint(4) DEFAULT '1',
  `applicationStatus` tinyint(1) DEFAULT NULL,
  `campaignStatus` tinyint(1) DEFAULT NULL,
  `show_offline_donation` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `campaignsetting`
--

LOCK TABLES `campaignsetting` WRITE;
/*!40000 ALTER TABLE `campaignsetting` DISABLE KEYS */;
INSERT INTO `campaignsetting` VALUES (1362146005773,1362146005771,0,0,1,0,0,0),(1362146005782,1362146005780,0,0,1,0,0,0),(1362146005791,1362146005789,0,0,1,0,0,0),(1362146005799,1362146005796,1,1,1,0,0,1),(1362146005805,1362146005803,0,0,1,0,0,0),(1362146005814,1362146005812,0,0,1,0,0,0);
/*!40000 ALTER TABLE `campaignsetting` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `campaignsettings`
--

DROP TABLE IF EXISTS `campaignsettings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `campaignsettings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `campaignId` bigint(20) DEFAULT NULL,
  `showSupporters` tinyint(4) DEFAULT NULL,
  `showMapUs` tinyint(4) DEFAULT NULL,
  `showPromote` tinyint(4) DEFAULT NULL,
  `showCustomTab` tinyint(4) DEFAULT NULL,
  `embedURL` varchar(1000) DEFAULT NULL,
  `allowDonations` tinyint(4) DEFAULT '1',
  `showThermometer` tinyint(4) NOT NULL DEFAULT '1',
  `showInviteActivity` tinyint(4) NOT NULL DEFAULT '1',
  `showZipCode` tinyint(4) DEFAULT NULL,
  `showTeamMembersTab` tinyint(4) NOT NULL DEFAULT '1',
  `showStories` tinyint(4) NOT NULL DEFAULT '1',
  `showCommunication` tinyint(4) NOT NULL DEFAULT '1',
  `showAuctionTab` tinyint(4) NOT NULL DEFAULT '1',
  `blobid` bigint(10) DEFAULT NULL,
  `imageproperty` varchar(15) DEFAULT NULL,
  `backgroundcolor` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `campaignId` (`campaignId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `campaignsettings`
--

LOCK TABLES `campaignsettings` WRITE;
/*!40000 ALTER TABLE `campaignsettings` DISABLE KEYS */;
/*!40000 ALTER TABLE `campaignsettings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `campaignsponsor`
--

DROP TABLE IF EXISTS `campaignsponsor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `campaignsponsor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `campaignId` bigint(20) DEFAULT NULL,
  `companyId` int(11) DEFAULT NULL,
  `value` float DEFAULT NULL,
  `type` tinyint(4) DEFAULT NULL,
  `message` varchar(500) DEFAULT NULL,
  `url` varchar(150) DEFAULT NULL,
  `logopath` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `campaignId` (`campaignId`),
  KEY `companyId` (`companyId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `campaignsponsor`
--

LOCK TABLES `campaignsponsor` WRITE;
/*!40000 ALTER TABLE `campaignsponsor` DISABLE KEYS */;
/*!40000 ALTER TABLE `campaignsponsor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `campaignstat`
--

DROP TABLE IF EXISTS `campaignstat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `campaignstat` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `totalRaised` float DEFAULT NULL,
  `totalDonors` int(11) DEFAULT NULL,
  `totalEmailsSent` int(11) DEFAULT NULL,
  `memberId` bigint(20) DEFAULT NULL,
  `campaignId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `campaignstat`
--

LOCK TABLES `campaignstat` WRITE;
/*!40000 ALTER TABLE `campaignstat` DISABLE KEYS */;
INSERT INTO `campaignstat` VALUES (1,'2015-09-01 13:15:25',0,0,0,1362146005137,1362145798074),(2,'2015-09-01 13:15:25',0,0,0,1362146005137,1362146005142),(3,'2015-09-01 13:15:25',0,0,0,1362146005137,1362146005151),(4,'2015-09-01 13:15:25',0,0,0,1362146005137,1362146005160),(5,'2015-09-01 13:15:25',0,0,0,1362146005137,1362146005169),(6,'2015-09-01 13:15:25',0,0,0,1362146005137,1362146005178),(7,'2015-09-01 13:15:25',0,0,0,1362146005137,1362146005187),(8,'2015-09-01 13:15:25',0,0,0,1362146005137,1362146005198),(9,'2015-09-01 13:15:25',0,0,0,1362146005137,1362146005205),(10,'2015-09-01 13:15:25',0,0,0,1362146005137,1362146005212),(11,'2015-09-01 13:15:25',100,2,0,1362146005137,1362146005219),(12,'2015-09-01 13:15:25',0,0,0,1362146005137,1362146005226),(13,'2015-09-01 13:15:25',0,0,0,1362146005137,1362146005233),(14,'2015-09-01 13:15:25',0,0,0,1362146005137,1362146005240),(15,'2015-09-01 13:15:25',0,0,0,1362146005137,1362146005247),(16,'2015-09-01 13:15:25',0,0,0,1362146005137,1362146005254),(17,'2015-09-01 13:15:25',120,1,0,1362146005764,1362146005771),(18,'2015-09-01 13:15:25',0,0,0,1362146005778,1362146005780),(19,'2015-09-01 13:15:25',0,0,0,1362146005787,1362146005789),(20,'2015-09-01 13:15:25',0,0,0,1362146005787,1362146005796),(21,'2015-09-01 13:15:25',0,0,0,1362146005801,1362146005803),(22,'2015-09-01 13:15:25',0,0,0,1362146005759,1362146005812);
/*!40000 ALTER TABLE `campaignstat` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `campaigntotaldonations`
--

DROP TABLE IF EXISTS `campaigntotaldonations`;
/*!50001 DROP VIEW IF EXISTS `campaigntotaldonations`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `campaigntotaldonations` (
  `id` tinyint NOT NULL,
  `value` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `campaigntotalofflinedonations`
--

DROP TABLE IF EXISTS `campaigntotalofflinedonations`;
/*!50001 DROP VIEW IF EXISTS `campaigntotalofflinedonations`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `campaigntotalofflinedonations` (
  `id` tinyint NOT NULL,
  `value` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category` (
  `id` bigint(20) NOT NULL,
  `created` datetime DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `description` varchar(2000) DEFAULT NULL,
  `imageId` bigint(20) DEFAULT NULL,
  `rank` tinyint(4) DEFAULT NULL,
  `memberId` bigint(20) DEFAULT NULL,
  `mediaId` bigint(20) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ccaccounts`
--

DROP TABLE IF EXISTS `ccaccounts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ccaccounts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `type` tinyint(4) NOT NULL,
  `ccparams` varchar(100) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ccaccounts`
--

LOCK TABLES `ccaccounts` WRITE;
/*!40000 ALTER TABLE `ccaccounts` DISABLE KEYS */;
/*!40000 ALTER TABLE `ccaccounts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `charity`
--

DROP TABLE IF EXISTS `charity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `charity` (
  `id` bigint(20) NOT NULL DEFAULT '0',
  `name` varchar(85) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `state` varchar(2) DEFAULT NULL,
  `charityAffiliateId` int(11) DEFAULT NULL,
  `disableTime` datetime DEFAULT NULL,
  `nonProfitStatus` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `charity_ibfk_1` (`charityAffiliateId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `charity`
--

LOCK TABLES `charity` WRITE;
/*!40000 ALTER TABLE `charity` DISABLE KEYS */;
INSERT INTO `charity` VALUES (1362145798073,'One more test',NULL,NULL,NULL,NULL,NULL),(1362146005141,'FirstSupersite',NULL,NULL,NULL,NULL,NULL),(1362146005150,'SecondSupersite',NULL,NULL,NULL,NULL,NULL),(1362146005159,'ThirdSupersite',NULL,NULL,NULL,NULL,NULL),(1362146005168,'TestSupersite',NULL,NULL,NULL,NULL,NULL),(1362146005177,'SoftwareSS',NULL,NULL,NULL,NULL,NULL),(1362146005186,'TestSsName',NULL,NULL,NULL,NULL,NULL),(1362146005197,'Test organization',NULL,NULL,NULL,NULL,NULL),(1362146005204,'EA',NULL,NULL,NULL,NULL,NULL),(1362146005211,'qwer',NULL,NULL,NULL,NULL,NULL),(1362146005218,'Test org',NULL,NULL,NULL,NULL,NULL),(1362146005225,'Test qwe',NULL,NULL,NULL,NULL,NULL),(1362146005232,'zxcv',NULL,NULL,NULL,NULL,NULL),(1362146005239,'qwds',NULL,NULL,NULL,NULL,NULL),(1362146005246,'First organization',NULL,NULL,NULL,NULL,NULL),(1362146005253,'asdf',NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `charity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `charityaffiliate`
--

DROP TABLE IF EXISTS `charityaffiliate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `charityaffiliate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(85) DEFAULT NULL,
  `description` varchar(2500) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `url` varchar(150) DEFAULT NULL,
  `address` varchar(50) DEFAULT NULL,
  `city` varchar(25) DEFAULT NULL,
  `state` varchar(25) DEFAULT NULL,
  `country` int(11) DEFAULT NULL,
  `zip` varchar(10) DEFAULT NULL,
  `bankName` varchar(30) DEFAULT NULL,
  `bankRouting` varchar(30) DEFAULT NULL,
  `logoPath` int(11) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `charityId` bigint(20) DEFAULT NULL,
  `verified` datetime DEFAULT NULL,
  `receiptGreeting` varchar(50) DEFAULT NULL,
  `receiptBody` varchar(2500) DEFAULT NULL,
  `receiptClosing` varchar(50) DEFAULT NULL,
  `type` tinyint(4) DEFAULT NULL,
  `partnerId` int(11) DEFAULT NULL,
  `contactInfo` text,
  PRIMARY KEY (`id`),
  KEY `charityId` (`charityId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `charityaffiliate`
--

LOCK TABLES `charityaffiliate` WRITE;
/*!40000 ALTER TABLE `charityaffiliate` DISABLE KEYS */;
/*!40000 ALTER TABLE `charityaffiliate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `charityaffiliateadmin`
--

DROP TABLE IF EXISTS `charityaffiliateadmin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `charityaffiliateadmin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `memberId` bigint(20) DEFAULT NULL,
  `charityAffiliateId` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `memberId` (`memberId`),
  KEY `charityAffiliateId` (`charityAffiliateId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `charityaffiliateadmin`
--

LOCK TABLES `charityaffiliateadmin` WRITE;
/*!40000 ALTER TABLE `charityaffiliateadmin` DISABLE KEYS */;
/*!40000 ALTER TABLE `charityaffiliateadmin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `charityrecommendation`
--

DROP TABLE IF EXISTS `charityrecommendation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `charityrecommendation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(40) DEFAULT NULL,
  `description` varchar(2500) DEFAULT NULL,
  `memberId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `charityrecommendation`
--

LOCK TABLES `charityrecommendation` WRITE;
/*!40000 ALTER TABLE `charityrecommendation` DISABLE KEYS */;
/*!40000 ALTER TABLE `charityrecommendation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `circleoftrust`
--

DROP TABLE IF EXISTS `circleoftrust`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `circleoftrust` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `inviterId` bigint(20) DEFAULT NULL,
  `invitedId` bigint(20) DEFAULT NULL,
  `body` varchar(2500) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `acceptTime` datetime DEFAULT NULL,
  `rejectTime` datetime DEFAULT NULL,
  `terminateTime` datetime DEFAULT NULL,
  `relationship` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `invitedId` (`invitedId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `circleoftrust`
--

LOCK TABLES `circleoftrust` WRITE;
/*!40000 ALTER TABLE `circleoftrust` DISABLE KEYS */;
/*!40000 ALTER TABLE `circleoftrust` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comment`
--

DROP TABLE IF EXISTS `comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `objectType` int(11) DEFAULT NULL,
  `objectId` bigint(20) DEFAULT NULL,
  `body` longtext,
  `anonymous` tinyint(4) DEFAULT NULL,
  `memberId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comment`
--

LOCK TABLES `comment` WRITE;
/*!40000 ALTER TABLE `comment` DISABLE KEYS */;
/*!40000 ALTER TABLE `comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `company`
--

DROP TABLE IF EXISTS `company`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `company` (
  `id` bigint(20) NOT NULL DEFAULT '0',
  `name` varchar(85) DEFAULT NULL,
  `stylesheet` varchar(2000) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `url` varchar(150) DEFAULT NULL,
  `address` varchar(50) DEFAULT NULL,
  `city` varchar(25) DEFAULT NULL,
  `state` varchar(25) DEFAULT NULL,
  `country` int(11) DEFAULT NULL,
  `zip` varchar(10) DEFAULT NULL,
  `logoId` bigint(20) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `verified` datetime DEFAULT NULL,
  `type` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `company`
--

LOCK TABLES `company` WRITE;
/*!40000 ALTER TABLE `company` DISABLE KEYS */;
INSERT INTO `company` VALUES (1362145515586,NULL,'','2014-09-18 10:29:46',NULL,NULL,NULL,NULL,0,NULL,0,NULL,NULL,0),(1362145515588,NULL,'Crowdster','2014-09-18 10:29:53',NULL,NULL,NULL,NULL,0,NULL,1362145515583,NULL,NULL,0),(1362145530071,NULL,'zcassa','2014-09-25 21:14:48',NULL,NULL,NULL,NULL,0,NULL,0,NULL,NULL,0),(1362145553455,NULL,'','2014-10-07 04:34:27',NULL,NULL,NULL,NULL,0,NULL,1362145556173,NULL,NULL,0);
/*!40000 ALTER TABLE `company` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `companyadmin`
--

DROP TABLE IF EXISTS `companyadmin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `companyadmin` (
  `id` bigint(20) NOT NULL DEFAULT '0',
  `memberId` bigint(20) DEFAULT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `memberId` (`memberId`),
  KEY `companyId` (`companyId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `companyadmin`
--

LOCK TABLES `companyadmin` WRITE;
/*!40000 ALTER TABLE `companyadmin` DISABLE KEYS */;
INSERT INTO `companyadmin` VALUES (1362145515587,86480,1362145515586,'2014-09-18 10:29:46'),(1362145515589,86480,1362145515588,'2014-09-18 10:29:53'),(1362145530072,1362145522588,1362145530071,'2014-09-25 21:14:48'),(1362145553456,1362145550157,1362145553455,'2014-10-07 04:34:27');
/*!40000 ALTER TABLE `companyadmin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contactus`
--

DROP TABLE IF EXISTS `contactus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contactus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `subject` varchar(100) DEFAULT NULL,
  `telephone` varchar(20) DEFAULT NULL,
  `comments` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contactus`
--

LOCK TABLES `contactus` WRITE;
/*!40000 ALTER TABLE `contactus` DISABLE KEYS */;
/*!40000 ALTER TABLE `contactus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `coupon`
--

DROP TABLE IF EXISTS `coupon`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `coupon` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `charityId` bigint(20) DEFAULT NULL,
  `campaignId` int(11) DEFAULT NULL,
  `pollId` int(11) DEFAULT NULL,
  `value` float DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `redeemTime` datetime DEFAULT NULL,
  `pendingTime` datetime DEFAULT NULL,
  `spendTime` datetime DEFAULT NULL,
  `memberId` bigint(20) DEFAULT NULL,
  `code` varchar(8) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`),
  KEY `charityId` (`charityId`),
  KEY `memberId` (`memberId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `coupon`
--

LOCK TABLES `coupon` WRITE;
/*!40000 ALTER TABLE `coupon` DISABLE KEYS */;
/*!40000 ALTER TABLE `coupon` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `couponredemption`
--

DROP TABLE IF EXISTS `couponredemption`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `couponredemption` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `memberId` bigint(20) DEFAULT NULL,
  `couponId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `memberId` (`memberId`),
  KEY `couponId` (`couponId`),
  CONSTRAINT `couponredemption_ibfk_2` FOREIGN KEY (`couponId`) REFERENCES `coupon` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `couponredemption`
--

LOCK TABLES `couponredemption` WRITE;
/*!40000 ALTER TABLE `couponredemption` DISABLE KEYS */;
/*!40000 ALTER TABLE `couponredemption` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `creditcard`
--

DROP TABLE IF EXISTS `creditcard`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `creditcard` (
  `id` bigint(20) NOT NULL DEFAULT '0',
  `memberId` bigint(20) DEFAULT NULL,
  `ccType` tinyint(4) DEFAULT NULL,
  `cardholderName` varchar(80) DEFAULT NULL,
  `number` varchar(100) NOT NULL,
  `token` varchar(100) DEFAULT NULL,
  `expirationMonth` tinyint(2) NOT NULL,
  `expirationYear` int(11) NOT NULL,
  `address` varchar(200) DEFAULT NULL,
  `address2` varchar(200) DEFAULT NULL,
  `city` varchar(75) DEFAULT NULL,
  `state` varchar(25) DEFAULT NULL,
  `province` varchar(70) DEFAULT NULL,
  `country` int(11) DEFAULT NULL,
  `zip` varchar(10) DEFAULT NULL,
  `expired` tinyint(4) DEFAULT NULL,
  `error` varchar(100) DEFAULT NULL,
  `deleteTime` datetime DEFAULT NULL,
  `code` varchar(50) DEFAULT NULL,
  `countrycode` char(3) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `memberId` (`memberId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `creditcard`
--

LOCK TABLES `creditcard` WRITE;
/*!40000 ALTER TABLE `creditcard` DISABLE KEYS */;
INSERT INTO `creditcard` VALUES (1362146005766,1362146005764,0,'visa','nMlM2DUw4BU=','tok_6u2E25LMYdmTzf',0,0,'qwe','','dsa','XX','XX',0,'123',0,NULL,'2015-09-01 12:46:22','ZiC2Eexa9qs=','US'),(1362146005833,1362146005831,0,'visa','nMlM2DUw4BU=','tok_6u2M73ZJNiqd03',0,0,'qwe','','qwe','XX','XX',0,'321',0,NULL,'2015-09-01 12:54:25','ZiC2Eexa9qs=','US'),(1362146005841,1362146005839,0,'visa','nMlM2DUw4BU=','tok_6u2N4xlmlmJiOM',0,0,'asdsvb','','qwecfdg','XX','XX',0,'654',0,NULL,'2015-09-01 12:56:10','ZiC2Eexa9qs=','US');
/*!40000 ALTER TABLE `creditcard` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `crmrequestresponse`
--

DROP TABLE IF EXISTS `crmrequestresponse`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `crmrequestresponse` (
  `id` bigint(20) NOT NULL,
  `created` datetime DEFAULT NULL,
  `queue_id` bigint(20) DEFAULT NULL,
  `type` tinyint(4) DEFAULT NULL,
  `request_string` varchar(1000) DEFAULT NULL,
  `response_string` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `crmrequestresponse`
--

LOCK TABLES `crmrequestresponse` WRITE;
/*!40000 ALTER TABLE `crmrequestresponse` DISABLE KEYS */;
/*!40000 ALTER TABLE `crmrequestresponse` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `crmsetting`
--

DROP TABLE IF EXISTS `crmsetting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `crmsetting` (
  `id` bigint(20) NOT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `site_id` bigint(20) DEFAULT NULL,
  `type` tinyint(4) DEFAULT NULL,
  `apikey` varchar(200) DEFAULT NULL,
  `login` varchar(20) DEFAULT NULL,
  `password` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `crmsetting`
--

LOCK TABLES `crmsetting` WRITE;
/*!40000 ALTER TABLE `crmsetting` DISABLE KEYS */;
INSERT INTO `crmsetting` VALUES (1362145786069,'2015-04-09 09:29:36',NULL,1362145765622,1,'je%2bXp6cgiCJxfTn0mJV03Nmxigk67oGD2RwFtAlAmjjHxyZYMHS1KhaMRZICl6hi0IhfD76St3UKnS74HUORHf48DNJB1OBs5KD2bGE5zGPbX8pQbuR5Vggp4STJvOXy','','');
/*!40000 ALTER TABLE `crmsetting` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `defaultfee`
--

DROP TABLE IF EXISTS `defaultfee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `defaultfee` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `objectType` tinyint(4) DEFAULT NULL,
  `objectSubType` tinyint(4) DEFAULT NULL,
  `NPA_status` tinyint(4) DEFAULT NULL,
  `karmaFee` float DEFAULT NULL,
  `partnerFee` float DEFAULT NULL,
  `karmaflatfee` float DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `defaultfee`
--

LOCK TABLES `defaultfee` WRITE;
/*!40000 ALTER TABLE `defaultfee` DISABLE KEYS */;
/*!40000 ALTER TABLE `defaultfee` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `directedgift`
--

DROP TABLE IF EXISTS `directedgift`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `directedgift` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(85) DEFAULT NULL,
  `description` varchar(2500) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `url` varchar(150) DEFAULT NULL,
  `charityAffiliateId` int(11) DEFAULT NULL,
  `retired` datetime DEFAULT NULL,
  `totalDonations` float DEFAULT NULL,
  `imagePath` int(11) DEFAULT NULL,
  `rank` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `charityAffiliateId` (`charityAffiliateId`),
  CONSTRAINT `directedgift_ibfk_1` FOREIGN KEY (`charityAffiliateId`) REFERENCES `charityaffiliate` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `directedgift`
--

LOCK TABLES `directedgift` WRITE;
/*!40000 ALTER TABLE `directedgift` DISABLE KEYS */;
/*!40000 ALTER TABLE `directedgift` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `domainnamemapkey`
--

DROP TABLE IF EXISTS `domainnamemapkey`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `domainnamemapkey` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `campaignId` bigint(20) DEFAULT NULL,
  `domainName` varchar(100) DEFAULT NULL,
  `mapkey` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `domainnamemapkey`
--

LOCK TABLES `domainnamemapkey` WRITE;
/*!40000 ALTER TABLE `domainnamemapkey` DISABLE KEYS */;
/*!40000 ALTER TABLE `domainnamemapkey` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `emailqueue`
--

DROP TABLE IF EXISTS `emailqueue`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `emailqueue` (
  `id` bigint(20) unsigned NOT NULL,
  `created` datetime DEFAULT NULL,
  `objectId` bigint(20) unsigned NOT NULL,
  `updated` datetime DEFAULT NULL,
  `deleted` datetime DEFAULT NULL,
  `processed` datetime DEFAULT NULL,
  `status` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `emailqueue`
--

LOCK TABLES `emailqueue` WRITE;
/*!40000 ALTER TABLE `emailqueue` DISABLE KEYS */;
INSERT INTO `emailqueue` VALUES (1362146005138,'2015-08-31 06:56:38',1,NULL,NULL,NULL,2),(1362146005195,'2015-08-31 07:31:48',2,NULL,NULL,NULL,2),(1362146005776,'2015-09-01 12:46:45',0,NULL,NULL,NULL,2),(1362146005785,'2015-09-01 12:47:23',0,NULL,NULL,NULL,2),(1362146005794,'2015-09-01 12:47:57',0,NULL,NULL,NULL,2),(1362146005808,'2015-09-01 12:48:32',0,NULL,NULL,NULL,2),(1362146005817,'2015-09-01 12:48:59',0,NULL,NULL,NULL,2);
/*!40000 ALTER TABLE `emailqueue` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `emailqueuestore`
--

DROP TABLE IF EXISTS `emailqueuestore`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `emailqueuestore` (
  `id` bigint(20) unsigned NOT NULL,
  `created` datetime DEFAULT NULL,
  `name` varchar(40) DEFAULT NULL,
  `value` text,
  `emailQueueId` bigint(20) unsigned NOT NULL,
  `deleted` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `emailqueuestore`
--

LOCK TABLES `emailqueuestore` WRITE;
/*!40000 ALTER TABLE `emailqueuestore` DISABLE KEYS */;
INSERT INTO `emailqueuestore` VALUES (1362146005139,'2015-08-31 06:56:38','1362146005138','{\"id\":1362146005138,\"created\":1441004198928,\"objectId\":1,\"status\":0,\"fromEmail\":\"info@crowdster.com\",\"receipientsEmails\":[\"gilyashov.alexandr@gmail.com\"],\"body\":\"<html><head><title>html, body { font-family: Verdana, Arial, sans-serif; width: 650px; font-size: 14px; } #top { width: 650px; } #top #logo { float: left; } #top #karmalogo { float: left; padding-top: 12px; font-size: 24px; color: #b6dd1c; font-weight: bold;} #top #email_type { float: right; padding-top: 12px; font-size: 24px; color: #b6dd1c; font-weight: bold; } #main { margin-top: 15px; border-top: 3px solid #4d71ab; float: left; padding-left: 30px; padding-right: 30px; padding-top: 20px; padding-bottom: 20px; } #main p { float: left; width: 100%; margin: 0px; padding: 0px; margin-top: 5px; margin-bottom: 5px; } #bottomNav { width: 650px; float: left; padding-left: 10px; border-top: 1px solid gray; } #bottom { float: left; width: 650px; border-top: 3px solid #4d71ab; margin-top: 15px; } #bottom #copyright { font-size: 12px; color: gray; clear: left; padding: 0; margin: 0; padding-top: 12px; text-align: right; } #main .email { background-color: #fffdeb; padding: 20px; } #main .spaceBelow { margin-bottom: 20px; width: 400px; } #main .closing { width: 400px; } .backgroundTable { margin: 0px; padding: 0px; } .backgroundTable table { margin: 10px; padding: 0px; float: left; background-color: #fffdeb; } #main td { border: 0px; }.backgroundTable td { text-align: left; font-size: 12px; border-bottom: 1px solid gray; } .backgroundTable th { text-align: left; font-size: 12px; font-weight: bold; white-space: nowrap; border-bottom: 1px solid gray; } .photo { float: right; width: 90px; margin: 0px; margin-top: -35px;  padding: 0px; border: 3px solid gray; } .personalMessage { float: left; padding: 15px; width: 300px; margin-top: 10px; background-color: #fffdeb; border: 1px solid gray; } #bottomNav li { font-size: 12px; }</title></head><body style=\\\"font-family: Arial; font-size: 12px;\\\"><table width=600><tr><td width=10>&nbsp;</td><td width=590><font face=\\\"Arial\\\">Dear Alex,<br><br>Congratulations you have successfully joined Karma411!<br><br>&nbsp;&nbsp;Username/email: gilyashov.alexandr@gmail.com<br>&nbsp;&nbsp;Password: 123456<br><br>Thank you for joining us.  Use and enjoy the site, and pass it on!<br><br>The Crowdster Team</font></td></tr></table></body></html>\",\"subject\":\"Welcome to Karma411!\",\"blastId\":1,\"emailSenderType\":2,\"emailMessageType\":0,\"simpleEmail\":false}',1362146005138,NULL),(1362146005196,'2015-08-31 07:31:48','1362146005195','{\"id\":1362146005195,\"created\":1441006308180,\"objectId\":2,\"status\":0,\"fromEmail\":\"info@crowdster.com\",\"receipientsEmails\":[\"reg@test.qwe\"],\"body\":\"<html><head><title>html, body { font-family: Verdana, Arial, sans-serif; width: 650px; font-size: 14px; } #top { width: 650px; } #top #logo { float: left; } #top #karmalogo { float: left; padding-top: 12px; font-size: 24px; color: #b6dd1c; font-weight: bold;} #top #email_type { float: right; padding-top: 12px; font-size: 24px; color: #b6dd1c; font-weight: bold; } #main { margin-top: 15px; border-top: 3px solid #4d71ab; float: left; padding-left: 30px; padding-right: 30px; padding-top: 20px; padding-bottom: 20px; } #main p { float: left; width: 100%; margin: 0px; padding: 0px; margin-top: 5px; margin-bottom: 5px; } #bottomNav { width: 650px; float: left; padding-left: 10px; border-top: 1px solid gray; } #bottom { float: left; width: 650px; border-top: 3px solid #4d71ab; margin-top: 15px; } #bottom #copyright { font-size: 12px; color: gray; clear: left; padding: 0; margin: 0; padding-top: 12px; text-align: right; } #main .email { background-color: #fffdeb; padding: 20px; } #main .spaceBelow { margin-bottom: 20px; width: 400px; } #main .closing { width: 400px; } .backgroundTable { margin: 0px; padding: 0px; } .backgroundTable table { margin: 10px; padding: 0px; float: left; background-color: #fffdeb; } #main td { border: 0px; }.backgroundTable td { text-align: left; font-size: 12px; border-bottom: 1px solid gray; } .backgroundTable th { text-align: left; font-size: 12px; font-weight: bold; white-space: nowrap; border-bottom: 1px solid gray; } .photo { float: right; width: 90px; margin: 0px; margin-top: -35px;  padding: 0px; border: 3px solid gray; } .personalMessage { float: left; padding: 15px; width: 300px; margin-top: 10px; background-color: #fffdeb; border: 1px solid gray; } #bottomNav li { font-size: 12px; }</title></head><body style=\\\"font-family: Arial; font-size: 12px;\\\"><table width=600><tr><td width=10>&nbsp;</td><td width=590><font face=\\\"Arial\\\">Dear Reg,<br><br>Congratulations you have successfully joined Karma411!<br><br>&nbsp;&nbsp;Username/email: reg@test.qwe<br>&nbsp;&nbsp;Password: qweqwe<br><br>Thank you for joining us.  Use and enjoy the site, and pass it on!<br><br>The Crowdster Team</font></td></tr></table></body></html>\",\"subject\":\"Welcome to Karma411!\",\"blastId\":2,\"emailSenderType\":2,\"emailMessageType\":0,\"simpleEmail\":false}',1362146005195,NULL),(1362146005777,'2015-09-01 12:46:45','1362146005776','{\"id\":1362146005776,\"created\":1441111605636,\"objectId\":0,\"status\":0,\"fromEmail\":\"info@crowdster.com\",\"receipientsEmails\":[\"ron@red.qwe\"],\"body\":\"<html><body><p>Thank you for creating a personal fundraising page on the AnotherOneEvenet website. Below, you&#39;ll find the information you&#39;ll need to log in.</p><br><br><p><b>Username:</b> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ron@red.qwe<br><b>Forgot Password?</b> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href=\'http://www.crowdsterqa.com//site/showForgotPasswordForm.do?siteId=1362146005206\'> click here</a> to reset.<br><br><a href=\'http://www.crowdsterqa.com//site/displaySite.do?siteIdCode=BGNQ98RG\'> Click here to visit the AnotherOneEvenet website </a><br/><br/>AnotherOneEvenet</body></html>\",\"subject\":\"Welcome to AnotherOneEvenet\",\"blastId\":0,\"emailSenderType\":2,\"emailMessageType\":0,\"simpleEmail\":false}',1362146005776,NULL),(1362146005786,'2015-09-01 12:47:24','1362146005785','{\"id\":1362146005785,\"created\":1441111643989,\"objectId\":0,\"status\":0,\"fromEmail\":\"info@crowdster.com\",\"receipientsEmails\":[\"qwe1@qwe1.qwe\"],\"body\":\"<html><body><p>Thank you for creating a personal fundraising page on the FirstEvent website. Below, you&#39;ll find the information you&#39;ll need to log in.</p><br><br><p><b>Username:</b> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;qwe1@qwe1.qwe<br><b>Forgot Password?</b> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href=\'http://www.crowdsterqa.com//site/showForgotPasswordForm.do?siteId=1362146005199\'> click here</a> to reset.<br><br><a href=\'http://www.crowdsterqa.com//site/displaySite.do?siteIdCode=7N03E4NI\'> Click here to visit the FirstEvent website </a><br/><br/>FirstEvent</body></html>\",\"subject\":\"Welcome to FirstEvent\",\"blastId\":0,\"emailSenderType\":2,\"emailMessageType\":0,\"simpleEmail\":false}',1362146005785,NULL),(1362146005795,'2015-09-01 12:47:57','1362146005794','{\"id\":1362146005794,\"created\":1441111677015,\"objectId\":0,\"status\":0,\"fromEmail\":\"info@crowdster.com\",\"receipientsEmails\":[\"team@test.qwe\"],\"body\":\"<html><body><p>Thank you for creating a personal fundraising page on the FirstEvent website. Below, you&#39;ll find the information you&#39;ll need to log in.</p><br><br><p><b>Username:</b> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;team@test.qwe<br><b>Forgot Password?</b> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href=\'http://www.crowdsterqa.com//site/showForgotPasswordForm.do?siteId=1362146005199\'> click here</a> to reset.<br><br><a href=\'http://www.crowdsterqa.com//site/displaySite.do?siteIdCode=7N03E4NI\'> Click here to visit the FirstEvent website </a><br/><br/>FirstEvent</body></html>\",\"subject\":\"Welcome to FirstEvent\",\"blastId\":0,\"emailSenderType\":2,\"emailMessageType\":0,\"simpleEmail\":false}',1362146005794,NULL),(1362146005809,'2015-09-01 12:48:32','1362146005808','{\"id\":1362146005808,\"created\":1441111712963,\"objectId\":0,\"status\":0,\"fromEmail\":\"info@crowdster.com\",\"receipientsEmails\":[\"qwe2@qwe2.qwe\"],\"body\":\"<html><body><p>Thank you for creating a personal fundraising page on the FirstEvent website. Below, you&#39;ll find the information you&#39;ll need to log in.</p><br><br><p><b>Username:</b> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;qwe2@qwe2.qwe<br><b>Forgot Password?</b> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href=\'http://www.crowdsterqa.com//site/showForgotPasswordForm.do?siteId=1362146005199\'> click here</a> to reset.<br><br><a href=\'http://www.crowdsterqa.com//site/displaySite.do?siteIdCode=7N03E4NI\'> Click here to visit the FirstEvent website </a><br/><br/>FirstEvent</body></html>\",\"subject\":\"Welcome to FirstEvent\",\"blastId\":0,\"emailSenderType\":2,\"emailMessageType\":0,\"simpleEmail\":false}',1362146005808,NULL),(1362146005818,'2015-09-01 12:48:59','1362146005817','{\"id\":1362146005817,\"created\":1441111739295,\"objectId\":0,\"status\":0,\"fromEmail\":\"info@crowdster.com\",\"receipientsEmails\":[\"qwe3@qwe3.qwe\"],\"body\":\"<html><body><p>Thank you for creating a personal fundraising page on the FirstEvent website. Below, you&#39;ll find the information you&#39;ll need to log in.</p><br><br><p><b>Username:</b> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;qwe3@qwe3.qwe<br><b>Forgot Password?</b> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href=\'http://www.crowdsterqa.com//site/showForgotPasswordForm.do?siteId=1362146005199\'> click here</a> to reset.<br><br><a href=\'http://www.crowdsterqa.com//site/displaySite.do?siteIdCode=7N03E4NI\'> Click here to visit the FirstEvent website </a><br/><br/>FirstEvent</body></html>\",\"subject\":\"Welcome to FirstEvent\",\"blastId\":0,\"emailSenderType\":2,\"emailMessageType\":0,\"simpleEmail\":false}',1362146005817,NULL);
/*!40000 ALTER TABLE `emailqueuestore` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `emailtracks`
--

DROP TABLE IF EXISTS `emailtracks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `emailtracks` (
  `id` int(11) NOT NULL,
  `mailMessageId` int(11) DEFAULT NULL,
  `receiverEmailAddress` varchar(50) DEFAULT NULL,
  `emailOpened` enum('Y','N') DEFAULT NULL,
  `emailClicked` enum('Y','N') DEFAULT NULL,
  `hasDonated` enum('Y','N') DEFAULT NULL,
  `amountDonated` decimal(50,0) DEFAULT NULL,
  `emailType` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `mailMessageId` (`mailMessageId`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `emailtracks`
--

LOCK TABLES `emailtracks` WRITE;
/*!40000 ALTER TABLE `emailtracks` DISABLE KEYS */;
/*!40000 ALTER TABLE `emailtracks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `embeddata`
--

DROP TABLE IF EXISTS `embeddata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `embeddata` (
  `id` bigint(20) NOT NULL,
  `created` datetime DEFAULT NULL,
  `fpcUid` bigint(20) NOT NULL,
  `firstName` varchar(50) DEFAULT NULL,
  `lastName` varchar(50) DEFAULT NULL,
  `email` varchar(50) NOT NULL,
  `imageUrl` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `embeddata`
--

LOCK TABLES `embeddata` WRITE;
/*!40000 ALTER TABLE `embeddata` DISABLE KEYS */;
/*!40000 ALTER TABLE `embeddata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `embeddomain`
--

DROP TABLE IF EXISTS `embeddomain`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `embeddomain` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `url` varchar(800) NOT NULL,
  `resizeFileUrl` varchar(800) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `embeddomain`
--

LOCK TABLES `embeddomain` WRITE;
/*!40000 ALTER TABLE `embeddomain` DISABLE KEYS */;
/*!40000 ALTER TABLE `embeddomain` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `employmentopportunities`
--

DROP TABLE IF EXISTS `employmentopportunities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `employmentopportunities` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `resumename` varchar(200) DEFAULT NULL,
  `telephone` varchar(20) DEFAULT NULL,
  `comments` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employmentopportunities`
--

LOCK TABLES `employmentopportunities` WRITE;
/*!40000 ALTER TABLE `employmentopportunities` DISABLE KEYS */;
/*!40000 ALTER TABLE `employmentopportunities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eventrsvp`
--

DROP TABLE IF EXISTS `eventrsvp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eventrsvp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `societyEventId` int(11) DEFAULT NULL,
  `societyId` int(11) DEFAULT NULL,
  `societyMemberId` int(11) DEFAULT NULL,
  `memberName` varchar(50) DEFAULT NULL,
  `response` tinyint(4) DEFAULT NULL,
  `comment` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `societyEventId` (`societyEventId`),
  KEY `societyId` (`societyId`),
  KEY `societyMemberId` (`societyMemberId`),
  CONSTRAINT `eventrsvp_ibfk_1` FOREIGN KEY (`societyEventId`) REFERENCES `societyevent` (`id`) ON DELETE CASCADE,
  CONSTRAINT `eventrsvp_ibfk_2` FOREIGN KEY (`societyId`) REFERENCES `society` (`id`) ON DELETE CASCADE,
  CONSTRAINT `eventrsvp_ibfk_3` FOREIGN KEY (`societyMemberId`) REFERENCES `societymember` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eventrsvp`
--

LOCK TABLES `eventrsvp` WRITE;
/*!40000 ALTER TABLE `eventrsvp` DISABLE KEYS */;
/*!40000 ALTER TABLE `eventrsvp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `events`
--

DROP TABLE IF EXISTS `events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `events` (
  `eventname` varchar(35) NOT NULL,
  `class` varchar(150) NOT NULL,
  `type` tinyint(4) NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `times` varchar(100) NOT NULL,
  `target` varchar(100) DEFAULT NULL,
  `parameters` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`eventname`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `events`
--

LOCK TABLES `events` WRITE;
/*!40000 ALTER TABLE `events` DISABLE KEYS */;
INSERT INTO `events` VALUES ('AddCRMTransactionToQueue','com.karma.queue.event.AddCRMTransactionToQueue',1,1,'*/6 * * * *',NULL,NULL),('CampaignStatEvent','com.karma.system.event.task.CampaignStatEvent',1,1,'5,10,15,20,25,30,35,40,54,50,55 * * * *',NULL,NULL),('DATABASE_QUEUE','com.karma.queue.event.ProcessPersistanceQueue',1,1,'*/5 * * * *',NULL,NULL),('Email_QUEUE','com.karma.queue.event.ProcessEmailQueue',1,1,'*/5 * * * *',NULL,NULL),('GoogleAnalyticsEvent','com.karma.system.event.task.GoogleAnalyticsEvent',1,1,'* 4 * * *',NULL,NULL),('MediaEvent','com.karma.system.event.task.MediaEvent',1,1,'* 4 * * *',NULL,NULL),('ProcessQueue','com.karma.queue.event.ProcessQueue',1,1,'*/5  *  *  *  *',NULL,NULL),('SiteStatEvent','com.karma.system.event.task.SiteStatEvent',1,1,'5,10,15,20,25,30,35,40,54,50,55 * * * *',NULL,NULL),('SolutionStatEvent','com.karma.system.event.task.SolutionStatEvent',1,1,'5,10,15,20,25,30,35,40,54,50,55 * * * *',NULL,NULL),('SuperSiteStatEvent','com.karma.system.event.task.SuperSiteStatEvent',1,1,'5,10,15,20,25,30,35,40,54,50,55 * * * *',NULL,NULL);
/*!40000 ALTER TABLE `events` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `expandedsite`
--

DROP TABLE IF EXISTS `expandedsite`;
/*!50001 DROP VIEW IF EXISTS `expandedsite`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `expandedsite` (
  `id` tinyint NOT NULL,
  `idCode` tinyint NOT NULL,
  `created` tinyint NOT NULL,
  `siteType` tinyint NOT NULL,
  `name` tinyint NOT NULL,
  `url` tinyint NOT NULL,
  `mastheadCode` tinyint NOT NULL,
  `masterCampaignId` tinyint NOT NULL,
  `adminId` tinyint NOT NULL,
  `isDeleted` tinyint NOT NULL,
  `templateType` tinyint NOT NULL,
  `waiverText` tinyint NOT NULL,
  `deleteTime` tinyint NOT NULL,
  `mobileMastheadCode` tinyint NOT NULL,
  `dateOfEvent` tinyint NOT NULL,
  `location` tinyint NOT NULL,
  `sitebannerblobid` tinyint NOT NULL,
  `address_1` tinyint NOT NULL,
  `address_2` tinyint NOT NULL,
  `city` tinyint NOT NULL,
  `state` tinyint NOT NULL,
  `zip` tinyint NOT NULL,
  `contact_phone` tinyint NOT NULL,
  `dashbaord_widgets_list` tinyint NOT NULL,
  `subdomain` tinyint NOT NULL,
  `thermometerstyle` tinyint NOT NULL,
  `alertEmailAddresses` tinyint NOT NULL,
  `themeId` tinyint NOT NULL,
  `isTemplate` tinyint NOT NULL,
  `overrideOrder` tinyint NOT NULL,
  `event_time` tinyint NOT NULL,
  `settings` tinyint NOT NULL,
  `mediaId` tinyint NOT NULL,
  `presetdonations` tinyint NOT NULL,
  `recurringoptions` tinyint NOT NULL,
  `folder` tinyint NOT NULL,
  `totalRaised` tinyint NOT NULL,
  `totalDonors` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `facebook_image`
--

DROP TABLE IF EXISTS `facebook_image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facebook_image` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `imageName` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `facebook_image`
--

LOCK TABLES `facebook_image` WRITE;
/*!40000 ALTER TABLE `facebook_image` DISABLE KEYS */;
/*!40000 ALTER TABLE `facebook_image` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `facebookmember`
--

DROP TABLE IF EXISTS `facebookmember`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facebookmember` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `facebookid` int(11) NOT NULL,
  `memberId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `facebookmember`
--

LOCK TABLES `facebookmember` WRITE;
/*!40000 ALTER TABLE `facebookmember` DISABLE KEYS */;
/*!40000 ALTER TABLE `facebookmember` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `factcheckrequest`
--

DROP TABLE IF EXISTS `factcheckrequest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `factcheckrequest` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `memberId` bigint(20) DEFAULT NULL,
  `requesterId` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modTime` datetime DEFAULT NULL,
  `status` tinyint(2) DEFAULT NULL,
  `notes` varchar(2500) DEFAULT NULL,
  `backgroundId` int(11) DEFAULT NULL,
  `type` tinyint(4) DEFAULT NULL,
  `orgName` varchar(100) DEFAULT NULL,
  `description` varchar(2500) DEFAULT NULL,
  `role` varchar(40) DEFAULT NULL,
  `degree` tinyint(4) DEFAULT NULL,
  `city` varchar(25) DEFAULT NULL,
  `state` char(2) DEFAULT NULL,
  `country` int(11) DEFAULT NULL,
  `startYear` year(4) DEFAULT NULL,
  `endYear` year(4) DEFAULT NULL,
  `URL` varchar(60) DEFAULT NULL,
  `administratorId` int(11) DEFAULT NULL,
  `adminTime` datetime DEFAULT NULL,
  `factCheckNotes` varchar(2500) DEFAULT NULL,
  `memberDob` datetime DEFAULT NULL,
  `charityAffiliateId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `memberId` (`memberId`),
  KEY `requesterId` (`requesterId`),
  KEY `backgroundId` (`backgroundId`),
  KEY `administratorId` (`administratorId`),
  KEY `charityAffiliateId` (`charityAffiliateId`),
  CONSTRAINT `factcheckrequest_ibfk_1` FOREIGN KEY (`backgroundId`) REFERENCES `background` (`id`) ON DELETE SET NULL,
  CONSTRAINT `factcheckrequest_ibfk_2` FOREIGN KEY (`charityAffiliateId`) REFERENCES `charityaffiliate` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `factcheckrequest`
--

LOCK TABLES `factcheckrequest` WRITE;
/*!40000 ALTER TABLE `factcheckrequest` DISABLE KEYS */;
/*!40000 ALTER TABLE `factcheckrequest` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `failedtransaction`
--

DROP TABLE IF EXISTS `failedtransaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `failedtransaction` (
  `id` bigint(20) NOT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `site_id` bigint(20) DEFAULT NULL,
  `campaign_id` bigint(20) DEFAULT NULL,
  `error_msg` varchar(500) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `failedtransaction`
--

LOCK TABLES `failedtransaction` WRITE;
/*!40000 ALTER TABLE `failedtransaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `failedtransaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `favoritecharity`
--

DROP TABLE IF EXISTS `favoritecharity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `favoritecharity` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `memberId` bigint(20) DEFAULT NULL,
  `charityId` bigint(20) DEFAULT NULL,
  `rank` tinyint(3) DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL,
  `showCharity` tinyint(4) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `memberId` (`memberId`),
  KEY `charityId` (`charityId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `favoritecharity`
--

LOCK TABLES `favoritecharity` WRITE;
/*!40000 ALTER TABLE `favoritecharity` DISABLE KEYS */;
/*!40000 ALTER TABLE `favoritecharity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fee`
--

DROP TABLE IF EXISTS `fee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fee` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `objectType` tinyint(4) DEFAULT NULL,
  `objectId` bigint(20) DEFAULT NULL,
  `karmaFee` float DEFAULT NULL,
  `karmaflatfee` float DEFAULT NULL,
  `partnerFee` float DEFAULT NULL,
  `useRule` tinyint(4) DEFAULT NULL,
  `ruleId` int(10) unsigned DEFAULT NULL,
  `defaultParameter` varchar(250) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `useHierarchy` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=39 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fee`
--

LOCK TABLES `fee` WRITE;
/*!40000 ALTER TABLE `fee` DISABLE KEYS */;
INSERT INTO `fee` VALUES (1,0,1362146005142,0,0,0,0,0,NULL,'2015-08-31 07:23:04',1),(2,15,1362146005143,0,0,0,0,0,NULL,'2015-08-31 07:23:04',1),(3,0,1362146005151,0,0,0,0,0,NULL,'2015-08-31 07:23:22',1),(4,15,1362146005152,0,0,0,0,0,NULL,'2015-08-31 07:23:22',1),(5,0,1362146005160,0,0,0,0,0,NULL,'2015-08-31 07:23:41',1),(6,15,1362146005161,0,0,0,0,0,NULL,'2015-08-31 07:23:42',1),(7,0,1362146005169,0,0,0,0,0,NULL,'2015-08-31 07:24:08',1),(8,15,1362146005170,0,0,0,0,0,NULL,'2015-08-31 07:24:08',1),(9,0,1362146005178,0,0,0,0,0,NULL,'2015-08-31 07:27:42',1),(10,15,1362146005179,0,0,0,0,0,NULL,'2015-08-31 07:27:42',1),(11,0,1362146005187,0,0,0,0,0,NULL,'2015-08-31 07:29:55',1),(12,15,1362146005188,0,0,0,0,0,NULL,'2015-08-31 07:29:55',1),(13,0,1362146005198,0,0,0,0,0,NULL,'2015-08-31 07:37:39',1),(14,15,1362146005199,0,0,0,0,0,NULL,'2015-08-31 07:37:39',1),(15,0,1362146005205,0,0,0,0,0,NULL,'2015-08-31 07:38:23',1),(16,15,1362146005206,0,0,0,0,0,NULL,'2015-08-31 07:38:23',1),(17,0,1362146005212,0,0,0,0,0,NULL,'2015-08-31 07:44:23',1),(18,15,1362146005213,0,0,0,0,0,NULL,'2015-08-31 07:44:23',1),(19,0,1362146005219,0,0,0,0,0,NULL,'2015-08-31 07:47:15',1),(20,15,1362146005220,0,0,0,0,0,NULL,'2015-08-31 07:47:15',1),(21,0,1362146005226,0,0,0,0,0,NULL,'2015-08-31 07:53:07',1),(22,15,1362146005227,0,0,0,0,0,NULL,'2015-08-31 07:53:07',1),(23,0,1362146005233,0,0,0,0,0,NULL,'2015-08-31 07:53:46',1),(24,15,1362146005234,0,0,0,0,0,NULL,'2015-08-31 07:53:46',1),(25,0,1362146005240,0,0,0,0,0,NULL,'2015-08-31 07:54:48',1),(26,15,1362146005241,0,0,0,0,0,NULL,'2015-08-31 07:54:48',1),(27,0,1362146005247,0,0,0,0,0,NULL,'2015-08-31 07:56:37',1),(28,15,1362146005248,0,0,0,0,0,NULL,'2015-08-31 07:56:38',1),(29,0,1362146005254,0,0,0,0,0,NULL,'2015-08-31 07:57:25',1),(30,15,1362146005255,0,0,0,0,0,NULL,'2015-08-31 07:57:25',1),(31,0,1362145798074,0,0,0,0,0,NULL,'2015-08-31 17:56:35',1),(32,15,1362145798075,0,0,0,0,0,NULL,'2015-08-31 17:56:35',1),(33,0,1362146005771,0,0,0,0,0,NULL,'2015-09-01 12:46:45',1),(34,0,1362146005780,0,0,0,0,0,NULL,'2015-09-01 12:47:23',1),(35,0,1362146005789,0,0,0,0,0,NULL,'2015-09-01 12:47:56',1),(36,6,1362146005796,0,0,0,0,0,NULL,'2015-09-01 12:47:57',1),(37,0,1362146005803,0,0,0,0,0,NULL,'2015-09-01 12:48:32',1),(38,0,1362146005812,0,0,0,0,0,NULL,'2015-09-01 12:48:59',1);
/*!40000 ALTER TABLE `fee` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `feerule`
--

DROP TABLE IF EXISTS `feerule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `feerule` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(250) DEFAULT NULL,
  `description` varchar(250) DEFAULT NULL,
  `className` varchar(250) DEFAULT NULL,
  `type` tinyint(4) DEFAULT NULL,
  `defaultParameter` varchar(250) DEFAULT NULL,
  `rank` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `feerule`
--

LOCK TABLES `feerule` WRITE;
/*!40000 ALTER TABLE `feerule` DISABLE KEYS */;
INSERT INTO `feerule` VALUES (11,'Site Create Rule','Rule for microsite creation','org.karma.fee.rules.SiteRule',6,'',0),(12,'Campaign Create Rule','Rule executed during campaign create','org.karma.fee.rules.CampaignRule',1,'',0);
/*!40000 ALTER TABLE `feerule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `formcampaignitemrel`
--

DROP TABLE IF EXISTS `formcampaignitemrel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `formcampaignitemrel` (
  `id` bigint(20) NOT NULL,
  `form_id` bigint(20) DEFAULT NULL,
  `campaignitem_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `formcampaignitemrel`
--

LOCK TABLES `formcampaignitemrel` WRITE;
/*!40000 ALTER TABLE `formcampaignitemrel` DISABLE KEYS */;
/*!40000 ALTER TABLE `formcampaignitemrel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `formsitememberrel`
--

DROP TABLE IF EXISTS `formsitememberrel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `formsitememberrel` (
  `id` bigint(20) NOT NULL,
  `sitemember_id` bigint(20) DEFAULT NULL,
  `form_id` bigint(20) DEFAULT NULL,
  `pollresponse_id` bigint(20) DEFAULT NULL,
  `site_id` bigint(20) DEFAULT NULL,
  `campaign_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `formsitememberrel`
--

LOCK TABLES `formsitememberrel` WRITE;
/*!40000 ALTER TABLE `formsitememberrel` DISABLE KEYS */;
/*!40000 ALTER TABLE `formsitememberrel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `geocode`
--

DROP TABLE IF EXISTS `geocode`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `geocode` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `latitude` varchar(40) DEFAULT NULL,
  `longitude` varchar(40) DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL,
  `description` varchar(10) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `city` varchar(75) DEFAULT NULL,
  `state` varchar(20) DEFAULT NULL,
  `zip` varchar(10) DEFAULT NULL,
  `eventDateString` varchar(45) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `siteId` bigint(20) DEFAULT NULL,
  `masterCampaignId` bigint(20) DEFAULT NULL,
  `campaignItemId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=118 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `geocode`
--

LOCK TABLES `geocode` WRITE;
/*!40000 ALTER TABLE `geocode` DISABLE KEYS */;
INSERT INTO `geocode` VALUES (1,'2014-06-13 14:28:04','40.8718861','-73.6203548','EAC Golf for Good','IEHDH2WD','30 St. Andrews Lane','Glen Cove','NY','',NULL,'crowdsterdemo@karma411.com',290,6061,0),(2,'2014-06-13 14:28:04','40.875862','-73.40999599999999','M1','X4XJNKJF','12 Beal Ct','Huntington','New York','11743',NULL,'paul@karma411.com',292,6070,0),(3,'2014-06-13 14:28:05','40.8369659','-73.68781899999999','Paul testing','QRXOXAEL','15 Evelyn Road','Port Washington','New York','11050',NULL,'john.murcott@karma411.com',293,6075,0),(4,'2014-06-13 14:28:06','40.7053677','-74.0026724','The New York City Heart Ball','SPVJ3WTG','Pier 86','New York','New York','10036',NULL,'crowdsterdemo@karma411.com',307,6098,0),(5,'2014-06-13 14:28:06','40.7053677','-74.0026724','NYC Heart Ball 2013','MLLLFUC5','Pier 86','New York','New York','10036',NULL,'solution@karma411.com',311,6102,0),(6,'2014-06-13 14:28:07','40.8377388','-73.6900903','Walk for a Cure','LQUN8EOF','69 Sandy Hollow Road','Port Washington','New York','11050',NULL,'nicole@karma411.com',330,6170,0),(7,'2014-06-13 14:28:07','40.8377388','-73.6900903','Another Great Walk','O3TKQBD7','69 Sandy Hollow Road','Port Washington','New York','11050',NULL,'nicole@karma411.com',332,6177,0),(8,'2014-06-13 14:28:07','40.8596875','-73.5913825','North Shore Land Alliance','939TWDEI','150 Piping Rock Rd','Locust Valley','','11560',NULL,'paul@karma411.com',333,6178,0),(9,'2014-06-13 14:28:08','40.875162','-73.621277','Test event','GX62CO42','27 Broadfield Place','Glen Cove','NY','11542',NULL,'hmtctest2@karma411.com',1362144754597,1362144754595,0),(10,'2014-06-13 14:28:08','40.8377388','-73.6900903','A great day here','ODIC8O0K','69 Sandy Hollow Road','Port Washington','New York','11050',NULL,'hmtctest@karma411.com',1362144754611,1362144754609,0),(11,'2014-06-13 14:28:08','40.8780163','-73.6414339','School Program','A67DS3S6','Welwyn Preserve','Glen Cove','NY','11542',NULL,'hmtc5@karma411.com',1362144754616,1362144754614,0),(12,'2014-06-13 14:28:08','40.6467664','-73.1570589','Wine festival 2013','PXGF20S7','','Ocean Beach','NY','',NULL,'hmtc2@karma411.com',1362144754621,1362144754619,0),(13,'2014-06-13 14:28:09','42.8142432','-73.9395687','Anthony\'s Surprise 50th Party','PNV7V4W0','Poconos','Here','PA','12345',NULL,'hmtc4@karma411.com',1362144754626,1362144754624,0),(14,'2014-06-13 14:28:09','40.8377388','-73.6900903','Golf ','9NZ9GMYH','69 sandy hollow road','Port Washington','NY','11050',NULL,'hmtc6@karma411.com',1362144754633,1362144754631,0),(15,'2014-06-13 14:28:10','29.19199','-81.11852700000001','Girls Golf','ZXOTZUVO','100 International Golf Drive','Daytona Beach','Florida','32124',NULL,'girlsgolfdemo@karma411.com',1362144946615,1362144946613,0),(16,'2014-06-13 14:28:10','33.6834408','-111.9660359','LPGA Extravaganza ','2A22V1Q0','5350 East Marriott Drive','Phoenix','Arizona','85054',NULL,'LPGAextravdemo@karma411.com',1362144946676,1362144946674,0),(17,'2014-06-13 14:28:10','40.8624508','-73.8803708','GC Spring Gala 2014','6WKUN8KG','2900 Southern Blvd.','Bronx','New York','10458',NULL,'GCdemo@karma411.com',1362144958160,1362144958158,0),(18,'2014-06-13 14:28:10','40.7407587','-73.4581242','2014 ISLANDERS GOLF OUTING ','GU2YO8I2','99 Quaker Meetinghouse Road','Farmingdale','NY','11735',NULL,'Isles.demo@karma411.com',1362144958218,1362144958216,0),(19,'2014-06-13 14:28:11','40.7407587','-73.4581242','Summer Golf Kick-Off','URT7ME0E','99 Quaker Meetinghouse Road','Farmingdale','NY','11730',NULL,'ksakitis@gmail.com',1362144963153,1362144963151,0),(20,'2014-06-13 14:28:11','38.903397','-77.024384','2014 Dream Gala','ULXGVIY8','901 Massachusets Avenue, NW','Washington DC','','',NULL,'ksakitis@gmail.com',1362144963179,1362144963177,0),(21,'2014-06-13 14:28:11','25.8015409','-80.1380226','18th Annual 2014 Fins Weekend','26S6K8KV','2301 Alton Road','Miami Beach',' FL ','33140',NULL,'Fins.Weekenddemo@karma411.com',1362144964575,1362144964573,0),(22,'2014-06-13 14:28:12','40.16646','-82.891083','Caring for Kids Charity Golf Classic','BKAHAU55','5000 Club Drive','Westervile','OH','43082',NULL,'NFLAlumnidemo@karma411.com',1362145063075,1362145063073,0),(23,'2014-06-13 14:28:12','44.982648','-93.1729','The Color Run MLB All-Star 5K','2UEUNDX4',' 1265 Snelling Ave. N.',' St. Paul','MN','55108',NULL,'MLBDemo@karma411.com',1362145066112,1362145066110,0),(24,'2014-06-13 14:28:12','25.7776117','-80.2192431','Fish \'N Chips Casino Party','VS0GV0RH','501 Marlins Way','Miami','FL','',NULL,'Marlinsdemo@karma411.com',1362145083577,1362145083575,0),(25,'2014-06-13 14:28:12','34.04516450000001','-118.2666085','ESPY Celebrity Golf Classic','XUGKV5GN','900 west olympic blvd','LA','CA','90015',NULL,'espydemo@karma411.com',1362145098084,1362145098082,0),(26,'2014-06-13 14:28:13','38.8976815','-77.0181231','Breath of Life Gala','T5LMUB2S','401 F st NW','Washington','DC','20001',NULL,'CysticFibrosisdemo@karma411.com',1362145122575,1362145122573,0),(27,'2014-06-13 14:28:13','25.70674','-80.42791199999999','HEAT Scramble Golf Classic','OLU4BFF6','6401 Kendale Lakes Drive','Miami','FL','33183',NULL,'MiamiHeatdemo@karma411.com',1362145146079,1362145146077,0),(28,'2014-06-13 14:28:13','40.3421939','-74.4599462','The 4th Annual NY Yankees Charity Golf Tournament','L91M1JC8','375 Forsgate Drive','Monroe Township','NJ','',NULL,'yankeesdemo@karma411.com',1362145189084,1362145189082,0),(29,'2014-06-13 15:18:18','40.8461402','-73.41999','Project Hope','4Z0WIRL9','299 9th Street','Huntington Station','New York','11797',NULL,'robert.friedman@karma411.com',336,1362144736581,0),(30,'2014-06-13 15:18:18','32.7801399','-96.80045109999999','Summer Conservators Fundraiser','RNIABTWV','','dallas','Texas','',NULL,'paul@karma411.com',337,1362144738071,0),(31,'2014-06-13 15:18:18','40.910762','-73.150672','Renewal: Half Day Silent Retreat','TQU1B9UY','200 Harbor Road','Stony Brook','NY','11790',NULL,'caroline.monahan@karma411.com',1362144740574,1362144740572,0),(32,'2014-06-13 15:18:18','29.5663139','77.3898602','Sports','2HPYDJYM','face1','noida','up','247777',NULL,'sachin@karma411.com',1362144742071,1362144742069,0),(33,'2014-06-13 15:18:20','34.0732204','-118.2409737','LA Marathon - Los Angeles, CA','P0D8GHQ7','1000 Elysian Park Ave','Los Angeles','CA','90012',NULL,'Stacey.Test@karma411.com',1362144796074,1362144796072,0),(34,'2014-06-13 15:18:22','42.8836496','-78.88119209999999','Jim Kelly Celebrity Classic','PFCTLWF0','120 Church Street','Buffalo','NY','14202',NULL,'kellydemo@karma411.com',1362144997076,1362144997074,0),(35,'2014-06-13 15:18:22','40.4480946','-80.0042488','5K Home Run','97EY7NKD','115 Federal Street','Pittsburgh','PA','15212',NULL,'piratesdemo@karma411.com',1362144997202,1362144997200,0),(36,'2014-06-13 15:18:22','40.7180228','-73.14528659999999','Annual Community Service Award Dinner','VNK9097T','150 River Rd','Great River','NY','11739',NULL,'ksakitis@gmail.com',1362145001866,1362145001864,0),(37,'2014-06-13 15:18:22','41.6916313','-87.5967915','Field of Greens','O9WQVHY1','11001 South Doty Avenue East ','Chicago','Illinois ','60628',NULL,'whitesox@karma411.com',1362145007607,1362145007605,0),(38,'2014-06-13 15:18:23','40.9518739','-72.316923','19th Annual Artists Against Abuse Benefit','WUU0N5QV','739 Butter Lane','Bridgehampton','NY','11932',NULL,'paul@karma411.com',1362145012396,1362145012394,0),(39,'2014-06-13 15:18:23','36.0416966','-115.1838488','Gridiron Greats of Las Vegas Hall of Fame Dinner','7YZ4KBZY','3333 Blue Diamond Road','Las Vegas','Nevada','89139',NULL,'Halloffame@karma411.com',1362145012430,1362145012428,0),(40,'2014-06-13 15:18:23','42.4630106','-70.9442098','Annual Event and Silent Auction','JLPWY4VE','590 Washington Street','Lynn','MA','',NULL,'mmcdermott@thecalebgroup.org',1362145012454,1362145012452,0),(41,'2014-06-13 15:18:23','41.068554','-73.988101','Drive for Zero Golf Classic','MKERL0IY','One Ahlmeyer Drive ','West Nyack','New York','10994',NULL,'UNICIF@karma411.com',1362145014575,1362145014573,0),(42,'2014-06-13 15:18:23','41.0084327','-73.8227198','Dystel Memorial Golf Classic','XAQ9W79S','300 Underhill Road','Scarsdale','NY','10583',NULL,'Dysteldemo@karma411.com',1362145014645,1362145014643,0),(43,'2014-06-13 15:18:24','40.7710841','-73.9829202','2nd Annual Spring Arts Gala','CJP0MI48','61 W 62nd St','New York','NY','',NULL,'ymcademo@karma411.com',1362145058075,1362145058073,0),(44,'2014-06-13 15:18:25','41.1239776','-73.8614796','11th Annual Celebrity Golf Outing &amp; Dinner','HPMG6H2Y','777 Albany Post Road','Scarborough','NY','10510',NULL,'Seandemo@karma411.com',1362145232099,1362145232097,0),(45,'2014-06-13 15:18:25','40.734386','-73.9928307','- NY Knicks Charity Event - Knicks Bowl 15 -','5LSQMM6B','110 University Place','New York','New York','10003',NULL,'nykdemo@karma411.com',1362145299197,1362145299195,0),(46,'2014-06-17 14:49:32','40.767427','-73.60084619999999','Tribute Dinner 2013','Y4FMQBVD','21 Old Westbury Rd','Old Westbury','New York','11568',NULL,'hmtc2@karma411.com',1362144755618,1362144755616,0),(47,'2014-06-17 14:49:32','40.8780163','-73.6414339','Children\'s Memorial Garden Cleanup','WMSJ7I5B','Welwyn Preserve','Glen Cove','New York','11542',NULL,'hmtc2@karma411.com',1362144755639,1362144755637,0),(48,'2014-06-17 14:49:33','40.76142','-74.0009082','Brooklyn Nets BASKETBOWL','YS1XI2I2','624-660 W 42nd street','New York','NY','10036',NULL,'brooklyn.nets@karma411.com',1362144940385,1362144940383,0),(49,'2014-06-17 14:49:33','39.0970478','-84.5068158','REDLEGS RUN','4MNARKCL','100 Joe Nuxhall Way, ','Cincinnati ','Ohio','45202',NULL,'reds.demo@karma411.com',1362144946090,1362144946088,0),(50,'2014-06-17 14:49:36','40.79204410000001','-73.5398476','The Joseph C. Dey, Jr. Golf Tournament!','ZQGD6TKK','','Jericho','New York','11753',NULL,'JCDGdemo@karma411.com',1362145314158,1362145314156,0),(51,'2014-06-17 15:08:55','37.7859447','-122.4110567','Play Ball Luncheon','WB5Y30OT','333 O\'Farrell Street','San Fran','CA','94102',NULL,'SFLunchdemo@karma411.com',1362144993459,1362144993457,0),(52,'2014-06-17 15:08:55','42.545919','-71.55215299999999','Loaves &amp; Fishes 30th Anniversary Celebration Golf Tournament','I803D45G','146 Shaker Rd','Harvard','MA','01451',NULL,'ksakitis@gmail.com',1362145014660,1362145014658,0),(53,'2014-06-17 15:08:56','40.7908363','-73.5382231','26th Annual American Heart Association Golf Classic','NOPW47Y8','Cedar Swamp Road/Route 107','Jericho','NY','11753',NULL,'AHAdemo@karma411.com',1362145014811,1362145014809,0),(54,'2014-06-17 15:08:56','40.75368539999999','-73.9991637','Annual Wingo Gala','ASEL6UK0','1 Penn Station','New York City','NY','10001',NULL,'ksakitis@gmail.com',1362145014860,1362145014858,0),(55,'2014-06-17 15:13:31','28.640234','77.36228799999999','vinay','T4Z89OAL','face 1','noida','up','1234',NULL,'vinay13@gmil.com',1362144878537,1362144878535,0),(56,'2014-06-17 15:13:31','28.640234','77.36228799999999','vinay','39XBTEF2','face 2','noida','up','23456',NULL,'vinay13@gmail.com',1362144896457,1362144896455,0),(57,'2014-06-17 15:13:31','33.4941704','-111.9260519','The Suns Golf Classic','ZWE290I8','','Scottsdale','AZ','',NULL,'Sandbox@Karma411.com',1362144911887,1362144911885,0),(58,'2014-06-17 15:13:32','40.8167557','-73.4650156','United Way Luncheon','UMURXDP8','8325 Jericho Tpke','Woodbury','NY','11797',NULL,'Sandbox@Karma411.com',1362144938583,1362144938581,0),(59,'2014-06-17 15:13:34','42.348662','-83.0592629','Habitat Detroit Red Carpet Bash!','L3R1FKN4','3711 Woodward Ave','Detroit','Michigan','48201',NULL,'RCBdemo@karma411.com',1362145314326,1362145314324,0),(60,'2014-06-26 20:27:35','40.7982129','-73.962114','The 10th Annual American Society Event','FQCIRUA1','123 Boulevard','NY','NY','',NULL,'caroline.monahan@karma411.com',1362144758098,1362144758096,0),(61,'2014-06-26 20:27:35','40.7351018','-73.6879082','CHANGE','RQJG8WKY','2 Change street','Chi city','NY','11040',NULL,'demo@aol.com',1362144783580,1362144783578,0),(62,'2014-06-26 20:27:35','40.7138855','-73.6027352','EProcessing Microsite','SOKQZ00A','1000 Fulton Ave','hempstead','New York','11549',NULL,'demo@aol.com',1362144783585,1362144783583,0),(63,'2014-06-26 20:27:36','41.7478399','-74.08542','Stacey\'s Org Presents: Test Site 1','USL252EF','84 Main St','New Paltz','NY','12561',NULL,'Stacey.Test@karma411.com',1362144789674,1362144789672,0),(64,'2014-06-26 20:27:37','42.5498816','-83.6394924','Kevin VanDam Charity Classic','TFSHFTEC','2240 W. Buno Road','Milford','Michigan','48380',NULL,'ksakitis@gmail.com',1362144966152,1362144966150,0),(65,'2014-06-26 20:27:37','42.4268461','-83.1252193','The 2013 Detroit Lions Invitational','LF939BEX','17911 Hamilton Rd','Detroit','Michigan','48203',NULL,'ksakitis@gmail.com',1362144972479,1362144972477,0),(66,'2014-06-26 20:27:37','40.724503','-73.640548','A Celebration Gala','XF0P0Z1R','45 Seventh Street','Garden City','NY','11530',NULL,'Gurwindemo@karma411.com',1362144972520,1362144972518,0),(67,'2014-06-26 20:27:37','41.8781136','-87.6297982','Midtown 5K Run &amp; Walk to benefit Bears Care','XBMWFH4C','','Chicago','IL','',NULL,'ksakitis@gmail.com',1362144972593,1362144972591,0),(68,'2014-06-26 20:27:38','41.0256225','-72.47425989999999','5th Annual Healing Heart 5K Run/Walk','Y3A77KVW','34515 Main Rd',' Cutchogue','NY','11935',NULL,'Healingheartdemo@karma411.com',1362145019918,1362145019916,0),(69,'2014-06-26 20:27:38','43.6342134','-70.33678789999999','Strive Rocks 2014 Dance Marathon','D3IQ2IER','364 Maine Mall Rd','Portland','ME','04106',NULL,'strivedemo@karma411.com',1362145034631,1362145034629,0),(70,'2014-06-26 20:27:38','41.8621253','-87.61678669999999','Bears Care Gala 2014','3D5G8FEK','1410 S. Museum Campus Drive','Chicago','IL','60605',NULL,'bearsgalademo@karma411.com',1362145035774,1362145035772,0),(71,'2014-06-26 20:27:38','40.7060535','-74.0092784','Man &amp; Woman of the Year (MWOY) campaign','1Y8RWDDJ','55 Wall Street ','New York','NY','10005',NULL,'MWOYdemo@karma411.com',1362145035863,1362145035861,0),(72,'2014-06-26 20:27:39','29.0846585','-82.36688699999999','sachin taliyan','FSOO8SZW','noida','delhi','asdf','34432',NULL,'sachin@karma411.com',1362145040638,1362145040636,0),(73,'2014-06-26 20:27:39','42.0930998','-71.2659789','10th Annual SCORES Cup Boston','2TL4YFUT','1 Patriot Pl','Foxboro','MA','02035',NULL,'ksakitis@gmail.com',1362145040679,1362145040677,0),(74,'2014-06-26 20:27:39','42.5378638','-86.16927869999999','2014 CELEBRATION OF LEARNING','PKCBPNRF','64 St. &amp; 5 Ave','New York City','NY','10021',NULL,'ksakitis@gmail.com',1362145046640,1362145046638,0),(75,'2014-06-26 20:30:32','43.0038707','-78.14330489999999','27th Annual Jim Kelly Celebrity Classic','BFCAX1TW','5122 Clinton Street Rd','Batavia','New York','14020',NULL,'ksakitis@gmail.com',1362144979076,1362144979074,0),(76,'2014-07-22 17:40:11','44.0802445','-69.1682591','Annual Celebration Dinner','8NXACZEM','30 High Street','Thomaston','Maine','04861',NULL,'johnsmith@crowdster.com',1362145324888,1362145324886,0),(77,'2014-07-22 20:22:48','40.7516797','-73.70868519999999','Fundraiser Advanced','BT3IRNL5','26707 76th Ave','New Hyde Park','New York','11040',NULL,'rmh.demo@crowdster.com',1362145346578,1362145346575,0),(78,'2014-07-22 20:54:22','43.1536036','-77.7323665','The Household Sale','X3HN2C4O','Elmgrove Road','Rochester','NY','14624',NULL,'rmh.demo@crowdster.com',1362145346701,1362145346699,0),(80,'2014-07-23 02:20:27','40.8725933','-73.53043199999999','Oyster Bay Annual Spring Luncheon','A80XGWEV','1 Main Street','Oyster Bay','NY','11771',NULL,'pm-lunch@karma411.com',1362145346676,1362145346674,0),(83,'2014-07-25 20:53:04','43.1536036','-77.7323665','The Household Sale','UYIKY49L','Elmgrove Road','Rochester','NY','14624',NULL,'rmh.demo@crowdster.com',1362145346121,1362145346119,0),(84,'2014-07-25 20:53:04','43.1536036','-77.7323665','The Household Sale Fundraiser','14G6DJU0','Elmgrove Road','Rochester','NY','14624',NULL,'rmh.demo@crowdster.com',1362145346134,1362145346132,0),(85,'2014-08-04 15:37:02','28.3768802','-81.5141484','International Catholic Stewardship Council Dinner','AARFSNE5','1900 E Buena Vista Drive','','','32836',NULL,'demo@crowdster.com',1362145388081,1362145388080,0),(86,'2014-08-06 11:39:50','40.8725933','-73.53043199999999','Local Luncheon','BVE3SFP3','1 Main street','Oyster Bay','NY','11771',NULL,'crowdsterqatest4@gmail.com',1362145362618,1362145362617,0),(87,'2015-06-17 14:19:25','40.776467','-73.6028278','2015 Walk of Love - Old Westbury Gardens','EXDNR11N','71 Old Westbury Road','Westbury','NY','16802',NULL,'brian@crowdster.com',1362145762716,1362145762715,0),(88,'2015-06-17 14:19:25','40.8198945','-73.59522419999999','Test QA','6CFA2XL2','720 Northern Blvd','Brookville','NY ','11548',NULL,'brian@crowdster.com',1362145779605,1362145779604,0),(89,'2015-06-17 18:36:59','21.3464529','-157.9196777','Rally for Duke Aiona For Governor','R3OT7NF1','99-500 Salt Lake Boulevard ','Honolulu','HI ','96818',NULL,'Axia@karma411.com',1362145445570,1362145445569,0),(90,'2015-06-17 18:36:59','21.3464529','-157.9196777','Arts, Science, and Athletics Rally and Fundraiser for&lt;br&gt;Duke Aiona For Governor-copy-3','LYIIDGVJ','99-500 Salt Lake Boulevard ','Honolulu','HI ','96818',NULL,'Axia@karma411.com',1362145451018,1362145451017,0),(91,'2015-06-17 18:36:59','49.4876178','8.4740023','Kmk56-copy','GK3NV85U','s2','s4','s5','s6',NULL,'sachin@karma411.com',1362145452770,1362145452769,0),(92,'2015-06-17 18:37:01','40.8198945','-73.59522419999999','Winter Gala 2015-copy','GODQCTFJ','720 Northern Blvd','Brookville','New York','11548',NULL,'Steven.stavrou@crowdster.com',1362145533142,1362145533141,0),(93,'2015-06-17 18:37:09','40.719807','-74.008314','First Annual Luncheon ','DOC4ZKDF','120 Hudson St New York, NY 10013','New York','New York','10013',NULL,'summer2015@crowdster.com',1362145827382,1362145827381,0),(94,'2015-06-17 18:37:10','42.3461949','-71.0456368','Pax East 2016','9TTSUNZT','415 Summer St','Boston','MA','02210',NULL,'summer2015@crowdster.com',1362145827390,1362145827389,0),(95,'2015-06-17 18:53:58','40.719109','-73.998813','Winter Gala','4CJDIO20','212 New York Street','New York','New York','10002',NULL,'stevewstav@gmail.com',1362145524589,1362145524588,0),(96,'2015-06-17 18:53:58','40.7135097','-73.9859414','Test Template 3b','JBF4X31G','212 Gala Street','GalaYork','GalaYork','10002',NULL,'Steven.stavrou@crowdster.com',1362145525632,1362145525631,0),(97,'2015-06-17 18:53:59','28.4806214','77.5221598','test 89','NOB2FIES','apha-2 , commercial belt','greater noida','uttar pradesh','201308',NULL,'tiwarishivang8@gmail.com',1362145550215,1362145550214,0),(98,'2015-06-17 18:54:05','29.5663139','77.3898602','Selebration','J0JCAVPV','face3','noida','Up','247777',NULL,'sachin@karma411.com',1362145819582,1362145819581,0),(99,'2015-06-17 18:54:07','39.9544167','-75.1480749','Summer Bike-A-Thon','KWSHSZQJ','5th and Race St.','Philadelphia','PA','19106',NULL,'summer2015@crowdster.com',1362145828842,1362145828841,0),(100,'2015-06-17 18:54:08','40.9521594','-73.74648309999999','Celebrity Golf Challenge','G3JOY40N','851 Fenimore','Mamaroneck','NY','10543',NULL,'summer2015@crowdster.com',1362145832086,1362145832085,0),(101,'2015-06-17 18:54:53','49.4876178','8.4740023','Kmk56','8GB58NPG','s2','s4','s5','s6',NULL,'sachin@karma411.com',1362145388680,1362145388679,0),(102,'2015-06-17 18:55:04','-33.4902928','-70.7336842','Ebola Testing Full MS','8GKW5VFY','2b2, Community Hall','Brooklyn','Alaska','12345',NULL,'sachin@karma411.com',1362145807102,1362145807101,0),(103,'2015-06-17 19:28:57','40.8155959','-73.6227136','Team Testing 2-23-15','XNKQZUPQ','','Brookville','NY','11548',NULL,'brian@crowdster.com',1362145736588,1362145736587,0),(104,'2015-06-26 13:55:09','40.6934457','-73.75212309999999','notarealevent','D2PBITEW','200 street','brookville','NY','11778',NULL,'stonybrook@karma411.com',1362145418091,1362145418090,0),(105,'2015-06-26 13:55:09','40.6934457','-73.75212309999999','notarealevent-copy','MSRHQLXI','200 street','brookville','NY','11778',NULL,'stonybrook@karma411.com',1362145418153,1362145418152,0),(106,'2015-06-26 14:02:21','32.9551571','-97.05404159999999','Partnership Golf - Dallas','K2P1OSDA','1600 Fairway Drive','Grapevine','TX','76051',NULL,'AKF@crowdster.com',1362145771600,1362145771599,0),(107,'2015-06-30 15:59:01','-33.4902928','-70.7336842','Ebola Foundation - -- --','OAW9N88R','2b2',' Brooklyn','Alaska','12345',NULL,'sachin@karma411.com',1362145727087,1362145727086,0),(108,'2015-06-30 15:59:19','21.3464529','-157.9196777','Arts, Science, and Athletics Rally and Fundraiser for&lt;br&gt;Duke Aiona For Governor','QSBJKTQA','99-500 Salt Lake Boulevard ','Honolulu','HI ','96818',NULL,'Axia@karma411.com',1362145426578,1362145426577,0),(109,'2015-06-30 15:59:19','21.3464529','-157.9196777','Arts, Science, and Athletics Rally and Fundraiser for&lt;br&gt;Duke Aiona For Governor-copy','KRRZN6JR','99-500 Salt Lake Boulevard ','Honolulu','HI ','96818',NULL,'Axia@karma411.com',1362145427086,1362145427085,0),(110,'2015-06-30 21:20:09','21.3464529','-157.9196777','Arts, Science, and Athletics Rally and Fundraiser for&lt;br&gt;Duke Aiona For Governor-copy-1','IJL2WM4R','99-500 Salt Lake Boulevard ','Honolulu','HI ','96818',NULL,'Axia@karma411.com',1362145427105,1362145427104,0),(111,'2015-06-30 21:20:29','40.8198945','-73.59522419999999','Test Site 1','8K5OVPZF','720 Northern Blvd','Brookville','NY','11548',NULL,'ahv@crowdster.com',1362145821592,1362145821591,0),(112,'2015-06-30 21:20:29','40.8198945','-73.59522419999999','Test Site 2','FKKC61CG','720 Northern Blvd','Brookville','NY','11548',NULL,'ahv@crowdster.com',1362145821636,1362145821635,0),(113,'2015-06-30 22:19:31','40.804215','-73.5236199','Test Microsite 2-10-15','I1O2JBCL','111 Eileen Way','Syosset','NY','11791',NULL,'brian@crowdster.com',1362145721607,1362145721606,0),(114,'2015-07-06 14:57:29','40.7250632','-73.9976946','DAN SPIEGEL TEST EVENT','KGUBAL4N','Address 1','City','State','10012',NULL,'brian@crowdster.com',1362145844669,1362145844668,0),(115,'2015-07-06 14:58:15','40.7978331','-73.0337405','DDI Walk Event','O95M86VS','249 Buckley Road','Holtsville','New York','11742',NULL,'summer2015@crowdster.com',1362145835087,1362145835086,0),(116,'2015-07-09 14:03:32','40.9505633','-73.0612807','Marine Cleanup Day  --  Belle Terre Beach','I99GRK11','1 Cliff Rd','Port Jefferson','NY','11777',NULL,'summer2015@crowdster.com',1362145833613,1362145833612,0),(117,'2015-07-09 16:36:35','45.564796','-122.681189','White Cane Walk for Independence','U07V08KU','','Portland','Maryland','',NULL,'Sandbox@Karma411.com',1362145411614,1362145411613,0);
/*!40000 ALTER TABLE `geocode` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `groupmember`
--

DROP TABLE IF EXISTS `groupmember`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `groupmember` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groupId` int(11) DEFAULT NULL,
  `memberId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `groupId` (`groupId`),
  KEY `memberId` (`memberId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `groupmember`
--

LOCK TABLES `groupmember` WRITE;
/*!40000 ALTER TABLE `groupmember` DISABLE KEYS */;
/*!40000 ALTER TABLE `groupmember` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `image`
--

DROP TABLE IF EXISTS `image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `image` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `objectId` int(11) DEFAULT NULL,
  `objectType` int(11) DEFAULT NULL,
  `memberId` bigint(20) DEFAULT NULL,
  `rank` int(11) DEFAULT NULL,
  `title` varchar(400) DEFAULT NULL,
  `description` text,
  `showOnMainSite` tinyint(4) DEFAULT NULL,
  `hideImage` tinyint(4) DEFAULT NULL,
  `fileName` varchar(200) DEFAULT NULL,
  `directory` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `image`
--

LOCK TABLES `image` WRITE;
/*!40000 ALTER TABLE `image` DISABLE KEYS */;
/*!40000 ALTER TABLE `image` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `interest`
--

DROP TABLE IF EXISTS `interest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `interest` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `memberId` bigint(20) DEFAULT NULL,
  `rank` tinyint(3) DEFAULT NULL,
  `name` varchar(40) DEFAULT NULL,
  `rankTotal` int(11) DEFAULT NULL,
  `voteTotal` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `name` (`name`),
  KEY `memberId` (`memberId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `interest`
--

LOCK TABLES `interest` WRITE;
/*!40000 ALTER TABLE `interest` DISABLE KEYS */;
/*!40000 ALTER TABLE `interest` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `interestvote`
--

DROP TABLE IF EXISTS `interestvote`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `interestvote` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tagId` int(11) DEFAULT NULL,
  `memberId` bigint(20) DEFAULT NULL,
  `rank` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tagId` (`tagId`),
  KEY `memberId` (`memberId`),
  CONSTRAINT `interestvote_ibfk_1` FOREIGN KEY (`tagId`) REFERENCES `interest` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `interestvote`
--

LOCK TABLES `interestvote` WRITE;
/*!40000 ALTER TABLE `interestvote` DISABLE KEYS */;
/*!40000 ALTER TABLE `interestvote` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `invitation`
--

DROP TABLE IF EXISTS `invitation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `invitation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `inviterId` bigint(20) DEFAULT NULL,
  `campaignId` bigint(20) DEFAULT NULL,
  `invitedId` bigint(20) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `acceptTime` datetime DEFAULT NULL,
  `value` float DEFAULT NULL,
  `charityId` bigint(20) DEFAULT NULL,
  `couponId` int(11) DEFAULT NULL,
  `type` tinyint(4) DEFAULT NULL,
  `societyId` int(11) DEFAULT NULL,
  `pollId` int(11) DEFAULT NULL,
  `rejectTime` datetime DEFAULT NULL,
  `inclusionVotes` int(11) DEFAULT NULL,
  `lastNag` datetime DEFAULT NULL,
  `memo` varchar(120) DEFAULT NULL,
  `approveTime` datetime DEFAULT NULL,
  `extData` varchar(1000) DEFAULT NULL,
  `retireTime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `inviterId` (`inviterId`),
  KEY `invitedId` (`invitedId`),
  KEY `societyId` (`societyId`),
  KEY `pollId` (`pollId`),
  KEY `campaignId` (`campaignId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `invitation`
--

LOCK TABLES `invitation` WRITE;
/*!40000 ALTER TABLE `invitation` DISABLE KEYS */;
/*!40000 ALTER TABLE `invitation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `karmablob`
--

DROP TABLE IF EXISTS `karmablob`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `karmablob` (
  `id` bigint(20) NOT NULL DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `uri` varchar(200) DEFAULT NULL,
  `filename` varchar(200) DEFAULT NULL,
  `mimetype` varchar(100) DEFAULT NULL,
  `url` varchar(200) DEFAULT NULL,
  `external` varchar(100) DEFAULT NULL,
  `height` varchar(15) DEFAULT NULL,
  `width` varchar(15) DEFAULT NULL,
  `alias` varchar(400) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `karmablob`
--

LOCK TABLES `karmablob` WRITE;
/*!40000 ALTER TABLE `karmablob` DISABLE KEYS */;
/*!40000 ALTER TABLE `karmablob` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `karmaproperty`
--

DROP TABLE IF EXISTS `karmaproperty`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `karmaproperty` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `keyCode` varchar(100) DEFAULT NULL,
  `value` longtext,
  `locale` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `keyCode` (`keyCode`),
  UNIQUE KEY `keyCode_2` (`keyCode`),
  UNIQUE KEY `keyCode_3` (`keyCode`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `karmaproperty`
--

LOCK TABLES `karmaproperty` WRITE;
/*!40000 ALTER TABLE `karmaproperty` DISABLE KEYS */;
INSERT INTO `karmaproperty` VALUES (5,'2013-03-20 14:09:26','facebook_api_key','1559081697704129','US'),(6,'2013-03-20 14:10:00','facebook_secret','71d51ce233b2147288af79a004a9b4ac','US'),(7,'2013-06-11 00:26:26','global_secret_key','sk_test_HBPN4YhwUbCznXnkyI7zAt4d','en_US'),(8,'2013-06-11 00:26:26','global_publishable_key','pk_test_FejRDSmCN67f2KyLcWXXb2Zv','en_US'),(9,'2013-06-11 00:26:26','global_connect_publishable_key','pk_test_PUPVc6WA0ux3UHU8At6rkgTH','en_US'),(10,'2013-06-11 00:26:26','global_connect_access_token','sk_test_n946K7EA3Fn5DbORUJdJhliE','en_US'),(11,'2013-06-11 00:27:18','global_client_id','ca_1nDbzHoXxlnMvalgOeXZuGBCg9RdDA0x','en_US'),(12,'2013-08-22 16:22:49','google_analytics_username','support@karma411.com','en_US'),(13,'2013-08-22 16:22:49','google_analytics_password','Karma411!','en_US'),(14,'2013-12-05 08:48:03','subscription_publishable_key','pk_test_PNEuDR4lODkImtjqCXotVXvJ','en_US'),(15,'2013-12-05 08:48:03','subscription_access_token','sk_test_LyB9Y7xZOhOzjKBLpGLaggs7','en_US'),(16,'2014-04-15 14:23:21','site_default_country_prefix_1362145040638','CA','en_us'),(17,'2014-04-15 14:27:54','site_currency_prefix_1362145040638','CAD','en_us'),(18,'2014-04-17 07:12:43','site_email_subject_prefix_1362145040638','MymailSubject','en_us'),(19,'2014-04-17 07:14:09','site_email_fromname_prefix_1362145040638','my mail name','en_us'),(20,'2014-06-24 13:47:30','google_analytics_account','UA-2344914-6','en_US'),(21,'2014-06-24 13:47:46','google_analytics_url','crowdsterdemo.com','en_US'),(22,'2014-06-24 18:55:44','google_analytics_table_Id','87559503','en_US'),(23,'2014-09-17 11:47:21','shorten_url_api_key','AIzaSyBEoMW98MzcrvKPN95773UDNc1pRAbwTkw','en_US'),(24,'2015-03-26 10:39:28','enable_sub_pages_btn_1362145765622','true','en_US'),(25,'2015-04-07 08:18:30','mom_day_cure_site_id','1362145765622','en_US'),(26,'2015-04-09 13:29:41','enable_ga_tags_1362145765622','true','en_US'),(27,'2015-04-10 08:30:20','thank_you_page_ga_tags_1362145765622','<!-- Facebook Conversion Code for MD-Donation -->\r\n<script>(function() {\r\n  var _fbq = window._fbq || (window._fbq = []);\r\n  if (!_fbq.loaded) {\r\n    var fbds = document.createElement(\'script\');\r\n    fbds.async = true;\r\n    fbds.src = \'//connect.facebook.net/en_US/fbds.js\';\r\n    var s = document.getElementsByTagName(\'script\')[0];\r\n    s.parentNode.insertBefore(fbds, s);\r\n    _fbq.loaded = true;\r\n  }\r\n})();\r\nwindow._fbq = window._fbq || [];\r\nwindow._fbq.push([\'track\', \'6025963205664\', {\'value\':\'0.00\',\'currency\':\'USD\'}]);\r\n</script>\r\n<noscript><img height=\"1\" width=\"1\" alt=\"\" style=\"display:none\" src=\"https://www.facebook.com/tr?ev=6025963205664&amp;cd[value]=0.00&amp;cd[currency]=USD&amp;noscript=1\" /></noscript>','en_US'),(28,'2015-04-16 05:30:38','site_email_subject_prefix_1362145765622','Donation Receipt for Mother\'s Day: Honor Women Who CURE','en_US'),(29,'2015-06-02 13:27:10','fb_meta_tag_app_id','1559081697704129','en_US');
/*!40000 ALTER TABLE `karmaproperty` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `klog`
--

DROP TABLE IF EXISTS `klog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `klog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `memberId` bigint(20) DEFAULT NULL,
  `value` float DEFAULT NULL,
  `charityId` bigint(20) DEFAULT NULL,
  `klogId` int(11) DEFAULT NULL,
  `anonymous` tinyint(4) DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `body` varchar(4000) DEFAULT NULL,
  `charityName` varchar(85) DEFAULT NULL,
  `campaignId` bigint(20) DEFAULT NULL,
  `showDonation` tinyint(4) DEFAULT NULL,
  `valuePaid` tinyint(4) DEFAULT NULL,
  `totalValue` float DEFAULT NULL,
  `filename` varchar(50) DEFAULT NULL,
  `url` varchar(150) DEFAULT NULL,
  `directedGiftId` int(11) DEFAULT NULL,
  `deleteTime` datetime DEFAULT NULL,
  `mediaURL` varchar(1000) DEFAULT NULL,
  `societyId` int(11) DEFAULT NULL,
  `publishDate` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `memberId` (`memberId`),
  KEY `charityId` (`charityId`),
  KEY `klogId` (`klogId`),
  KEY `campaignId` (`campaignId`),
  KEY `directedGiftId` (`directedGiftId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `klog`
--

LOCK TABLES `klog` WRITE;
/*!40000 ALTER TABLE `klog` DISABLE KEYS */;
/*!40000 ALTER TABLE `klog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `language`
--

DROP TABLE IF EXISTS `language`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `language` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `language`
--

LOCK TABLES `language` WRITE;
/*!40000 ALTER TABLE `language` DISABLE KEYS */;
/*!40000 ALTER TABLE `language` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `listenerregistry`
--

DROP TABLE IF EXISTS `listenerregistry`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `listenerregistry` (
  `id` bigint(20) unsigned NOT NULL,
  `listener` varchar(150) DEFAULT NULL,
  `blocking` tinyint(4) DEFAULT NULL,
  `operation` tinyint(4) DEFAULT NULL,
  `opertationtype` tinyint(4) DEFAULT NULL,
  `resource` varchar(100) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `deleted` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `listenerregistry`
--

LOCK TABLES `listenerregistry` WRITE;
/*!40000 ALTER TABLE `listenerregistry` DISABLE KEYS */;
INSERT INTO `listenerregistry` VALUES (1,'org.karma.listener.impl.CampaignListener',1,0,1,'campaign','2013-04-23 21:59:29',NULL),(3,'org.karma.listener.impl.SiteListener',1,0,1,'site','2013-04-23 21:59:30',NULL),(4,'org.karma.listener.impl.SitenavListener',1,0,1,'sitenav','2013-04-23 21:59:30',NULL),(6,'org.karma.listener.impl.CampaignListener',1,1,1,'campaign','2013-04-23 21:59:30',NULL),(7,'org.karma.listener.impl.PageListener',1,1,1,'page','2013-04-23 21:59:30',NULL),(8,'org.karma.listener.impl.SiteListener',1,1,1,'site','2013-05-13 03:43:17',NULL);
/*!40000 ALTER TABLE `listenerregistry` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `loginlog`
--

DROP TABLE IF EXISTS `loginlog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `loginlog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `memberId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `memberId` (`memberId`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `loginlog`
--

LOCK TABLES `loginlog` WRITE;
/*!40000 ALTER TABLE `loginlog` DISABLE KEYS */;
INSERT INTO `loginlog` VALUES (1,'2015-08-31 06:46:08',86450),(2,'2015-08-31 06:46:56',86452),(3,'2015-08-31 06:57:54',1362146005137),(4,'2015-08-31 07:19:14',1362146005137),(5,'2015-08-31 07:30:24',1362146005137),(6,'2015-08-31 07:33:32',1362146005137),(7,'2015-08-31 07:36:31',1362146005137),(8,'2015-08-31 07:45:42',1362146005137),(9,'2015-08-31 07:54:03',1362146005137),(10,'2015-08-31 07:57:36',1362146005137),(11,'2015-08-31 07:57:41',1362146005137),(12,'2015-08-31 08:38:51',1362146005137),(13,'2015-08-31 10:43:01',1362146005137),(14,'2015-08-31 11:11:14',1362146005137),(15,'2015-08-31 11:12:41',1362146005137),(16,'2015-08-31 11:15:12',1362146005137),(17,'2015-08-31 11:16:42',1362146005137),(18,'2015-08-31 11:27:07',1362146005137),(19,'2015-08-31 11:28:53',1362146005137),(20,'2015-08-31 11:29:48',1362146005137),(21,'2015-08-31 11:36:06',1362146005137),(22,'2015-08-31 11:41:39',1362146005137),(23,'2015-08-31 11:45:40',1362146005137),(24,'2015-08-31 11:46:26',1362146005137),(25,'2015-08-31 11:47:18',1362146005137),(26,'2015-08-31 11:49:48',1362146005137),(27,'2015-08-31 11:50:56',1362146005137),(28,'2015-08-31 11:51:35',1362146005137),(29,'2015-08-31 11:52:45',1362146005137),(30,'2015-08-31 12:16:38',1362146005137),(31,'2015-08-31 12:17:15',1362146005137),(32,'2015-08-31 12:29:09',1362146005137),(33,'2015-08-31 12:29:33',1362146005137),(34,'2015-08-31 13:34:13',1362146005137),(35,'2015-08-31 13:36:52',1362146005137),(36,'2015-08-31 14:34:24',86450),(37,'2015-08-31 14:50:31',1362146005137),(38,'2015-08-31 14:52:28',1362146005137),(39,'2015-08-31 15:01:52',1362146005137),(40,'2015-08-31 17:51:13',1362146005137),(41,'2015-08-31 18:31:07',1362146005137);
/*!40000 ALTER TABLE `loginlog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mailblast`
--

DROP TABLE IF EXISTS `mailblast`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mailblast` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `memberId` bigint(20) DEFAULT NULL,
  `objectType` tinyint(3) unsigned NOT NULL,
  `objectId` bigint(20) DEFAULT NULL,
  `blastType` tinyint(3) unsigned NOT NULL,
  `created` datetime NOT NULL,
  `rImpactJobId` int(10) unsigned NOT NULL,
  `messageId` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `objectId` (`objectId`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mailblast`
--

LOCK TABLES `mailblast` WRITE;
/*!40000 ALTER TABLE `mailblast` DISABLE KEYS */;
INSERT INTO `mailblast` VALUES (1,0,6,0,7,'2015-08-31 06:56:38',0,0),(2,0,6,0,7,'2015-08-31 07:31:48',0,0);
/*!40000 ALTER TABLE `mailblast` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mailblastmember`
--

DROP TABLE IF EXISTS `mailblastmember`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mailblastmember` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `blastId` int(10) unsigned NOT NULL,
  `memberId` bigint(20) DEFAULT NULL,
  `created` datetime NOT NULL,
  `lastNag` datetime DEFAULT NULL,
  `acceptTime` datetime DEFAULT NULL,
  `rejectTime` datetime DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `blastId` (`blastId`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mailblastmember`
--

LOCK TABLES `mailblastmember` WRITE;
/*!40000 ALTER TABLE `mailblastmember` DISABLE KEYS */;
INSERT INTO `mailblastmember` VALUES (1,1,1362146005137,'2015-08-31 06:56:38',NULL,NULL,NULL,NULL),(2,2,1362146005194,'2015-08-31 07:31:48',NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `mailblastmember` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mailmessage`
--

DROP TABLE IF EXISTS `mailmessage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mailmessage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `emailTitle` varchar(250) NOT NULL,
  `memberId` bigint(20) DEFAULT NULL,
  `emailSubject` varchar(250) DEFAULT NULL,
  `emailMessage` text,
  `createdFrom` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1254 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mailmessage`
--

LOCK TABLES `mailmessage` WRITE;
/*!40000 ALTER TABLE `mailmessage` DISABLE KEYS */;
INSERT INTO `mailmessage` VALUES (1247,'2013 All Star 5K',86466,'Please visit our campaign: 2013 All Star 5K','<title></title>\r\n<table width=\"600\">\r\n	<tbody>\r\n		<tr>\r\n			<td width=\"10\">&nbsp;</td>\r\n			<td width=\"590\">\r\n			<table style=\"margin: 0px; margin-bottom: 4px;\">\r\n				<tbody>\r\n					<tr>\r\n						<td align=\"left\" valign=\"top\">&nbsp;</td>\r\n						<td align=\"left\" valign=\"top\">\r\n						<p style=\"font-size: 14px;\"><font face=\"Arial\">Please visit:<br />\r\n						<a href=\"http://crowdster.karma411.com/crowdster/campaign/viewDetails.do?campaignId=6081&amp;email=${memberEmail}&amp;firstName=${memberFirstName}&amp;lastName=${memberLastName}\" style=\"font-weight: bold; font-size: 15px; color: blue;\" target=\"new\">2013 All Star 5K</a><br />\r\n						to benefit Stephen Siller Tunnel to Towers Foundation.</font></p>\r\n						</td>\r\n					</tr>\r\n				</tbody>\r\n			</table>\r\n			<br clear=\"all\" />\r\n			<font face=\"Arial\">Dear [FRIEND],</font><br />\r\n			<br />\r\n			&nbsp;\r\n			<p><font face=\"Arial\">abc</font></p>\r\n			<br />\r\n			<font face=\"Arial\">Brian De Montreux<br clear=\"all\" />\r\n			<img src=\"http://crowdster.karma411.com/crowdster/images/member/1/86466.jpg\" width=\"90\" /><br clear=\"all\" />\r\n			<br clear=\"all\" />\r\n			<font color=\"gray\">Trouble with above links? Please copy URL: http://crowdster.karma411.com/crowdster/campaign/viewDetails.do?campaignId=6081&amp;email=${memberEmail}&amp;firstName=${memberFirstName}&amp;lastName=${memberLastName}</font><br clear=\"all\" />\r\n			To unsubscribe from this campaign <a href=\"http://crowdster.karma411.com/crowdster/member/unsubscribe/confirm.do?mailBlastId=${mailBlastId}&amp;memberId=${memberId}\">click here</a></font></td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n',2),(1248,'Brian De Montreux\'s support for NYC Heart Ball 2013',86459,'Please visit our campaign: Brian De Montreux\'s support for NYC Heart Ball 2013','<title></title>\r\n<table width=\"600\">\r\n	<tbody>\r\n		<tr>\r\n			<td width=\"10\">&nbsp;</td>\r\n			<td width=\"590\">\r\n			<table style=\"margin: 0px; margin-bottom: 4px;\">\r\n				<tbody>\r\n					<tr>\r\n						<td align=\"left\" valign=\"top\">&nbsp;</td>\r\n						<td align=\"left\" valign=\"top\">\r\n						<p style=\"font-size: 14px;\"><font face=\"Arial\">Please visit:<br />\r\n						<a href=\"https://nycheartball2013.karma411demo.com/campaign/viewDetails.do?campaignId=6103&amp;email=${memberEmail}&amp;firstName=${memberFirstName}&amp;lastName=${memberLastName}\" style=\"font-weight: bold; font-size: 15px; color: blue;\" target=\"new\">Brian De Montreux&#39;s support for NYC Heart Ball 2013</a><br />\r\n						to benefit American Heart Association Inc..</font></p>\r\n						</td>\r\n					</tr>\r\n				</tbody>\r\n			</table>\r\n			<br clear=\"all\" />\r\n			<font face=\"Arial\">Dear [FRIEND],<br />\r\n			<br />\r\n			<br />\r\n			Will you please help me with this important campaign: Brian De Montreux&#39;s support for NYC Heart Ball 2013 (please see the link above)?<br />\r\n			<br />\r\n			American Heart Association Inc. is an organization I care a lot about! By the time you read this, some of our friends have likely already participated in this campaign. I hope you do too!<br />\r\n			<br />\r\n			Sincerely,<br />\r\n			<br />\r\n			Brian De Montreux<br clear=\"all\" />\r\n			<img src=\"/userfiles/0/2/ski_1363902277606.jpeg\" width=\"90\" /><br clear=\"all\" />\r\n			<br clear=\"all\" />\r\n			<font color=\"gray\">Trouble with above links? Please copy URL: https://nycheartball2013.karma411demo.com/campaign/viewDetails.do?campaignId=6103&amp;email=${memberEmail}&amp;firstName=${memberFirstName}&amp;lastName=${memberLastName}</font><br clear=\"all\" />\r\n			To unsubscribe from this campaign <a href=\"http://karma411demo.com/crowdster/member/unsubscribe/confirm.do?mailBlastId=${mailBlastId}&amp;memberId=${memberId}\">click here</a></font></td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n',2),(1249,'Joan Smith\'s support for NYC Heart Ball 2013',86494,'Please visit our campaign: Joan Smith\'s support for NYC Heart Ball 2013','<title></title>\r\n<table width=\"600\">\r\n	<tbody>\r\n		<tr>\r\n			<td width=\"10\">&nbsp;</td>\r\n			<td width=\"590\">\r\n			<table style=\"margin: 0px; margin-bottom: 4px;\">\r\n				<tbody>\r\n					<tr>\r\n						<td align=\"left\" valign=\"top\">&nbsp;</td>\r\n						<td align=\"left\" valign=\"top\">\r\n						<p style=\"font-size: 14px;\"><font face=\"Arial\">Please visit:<br />\r\n						<a href=\"https://nycheartball2013.karma411demo.com/campaign/viewDetails.do?campaignId=6107&amp;email=${memberEmail}&amp;firstName=${memberFirstName}&amp;lastName=${memberLastName}\" style=\"font-weight: bold; font-size: 15px; color: blue;\" target=\"new\">Joan Smith&#39;s support for NYC Heart Ball 2013</a><br />\r\n						to benefit American Heart Association Inc..</font></p>\r\n						</td>\r\n					</tr>\r\n				</tbody>\r\n			</table>\r\n			<br clear=\"all\" />\r\n			<font face=\"Arial\">Dear [FRIEND],<br />\r\n			<br />\r\n			<br />\r\n			Will you please help me with this important campaign: Joan Smith&#39;s support for NYC Heart Ball 2013 (please see the link above)?<br />\r\n			<br />\r\n			American Heart Association Inc. is an organization I care a lot about! By the time you read this, some of our friends have likely already participated in this campaign. I hope you do too!<br />\r\n			<br />\r\n			Sincerely,<br />\r\n			<br />\r\n			Joan Smith<br clear=\"all\" />\r\n			<br clear=\"all\" />\r\n			<font color=\"gray\">Trouble with above links? Please copy URL: https://nycheartball2013.karma411demo.com/campaign/viewDetails.do?campaignId=6107&amp;email=${memberEmail}&amp;firstName=${memberFirstName}&amp;lastName=${memberLastName}</font><br clear=\"all\" />\r\n			To unsubscribe from this campaign <a href=\"http://karma411demo.com/crowdster/member/unsubscribe/confirm.do?mailBlastId=${mailBlastId}&amp;memberId=${memberId}\">click here</a></font></td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n',2),(1250,'FD Walkathon 2013',86505,'Please visit our campaign: FD Walkathon 2013','<title></title>\r\n<table width=\"600\">\r\n	<tbody>\r\n		<tr>\r\n			<td width=\"10\">&nbsp;</td>\r\n			<td width=\"590\">\r\n			<table style=\"margin: 0px; margin-bottom: 4px;\">\r\n				<tbody>\r\n					<tr>\r\n						<td align=\"left\" valign=\"top\">&nbsp;</td>\r\n						<td align=\"left\" valign=\"top\">\r\n						<p style=\"font-size: 14px;\"><font face=\"Arial\">Please visit:<br />\r\n						<a href=\"https://fdwalkathon2013.karma411demo.com/campaign/viewDetails.do?campaignId=6141&amp;email=${memberEmail}&amp;firstName=${memberFirstName}&amp;lastName=${memberLastName}\" style=\"font-weight: bold; font-size: 15px; color: blue;\" target=\"new\">FD Walkathon 2013</a><br />\r\n						to benefit Familial Dysautonomia Hope.</font></p>\r\n						</td>\r\n					</tr>\r\n				</tbody>\r\n			</table>\r\n			<br clear=\"all\" />\r\n			<font face=\"Arial\">Dear [FRIEND],</font><br />\r\n			<br />\r\n			&nbsp;\r\n			<p><font face=\"Arial\">Will you please help me with this important campaign: Familial Dysautonomia (please see the link above)?</font></p>\r\n			<font face=\"Arial\"> </font>\r\n\r\n			<p><font face=\"Arial\"><span style=\"color: rgb(0, 0, 0); font-family: Arial; font-size: small; line-height: normal; text-align: -webkit-left; \">Familial dysautonomia (FD) is a rare genetic disease that affects the autonomic and sensory nervous systems of children from birth. The most striking symptoms of FD are reduced sensitivity to pain and temperature, and the inability to produce tears. But FD is much more than &ldquo;no pain and no tears&rdquo;, it affects every major system of the body, causing severe respiratory, cardiac, orthopedic, digestive and vision problems. &nbsp;</span></font></p>\r\n			<font face=\"Arial\"> </font>\r\n\r\n			<p><font face=\"Arial\">I am participating in their annual FD Walk to raise money for research and treatments to help the FD community to find a CURE!<br />\r\n			<br />\r\n			By the time you read this, some of our friends have likely already participated in this campaign. I hope you do too!<br />\r\n			<br />\r\n			Sincerely,</font></p>\r\n\r\n			<p>&nbsp;</p>\r\n			<font face=\"Arial\">Rachael Eisenson<br clear=\"all\" />\r\n			<br clear=\"all\" />\r\n			<font color=\"gray\">Trouble with above links? Please copy URL: https://fdwalkathon2013.karma411demo.com/campaign/viewDetails.do?campaignId=6141&amp;email=${memberEmail}&amp;firstName=${memberFirstName}&amp;lastName=${memberLastName}</font><br clear=\"all\" />\r\n			To unsubscribe from this campaign <a href=\"http://karma411demo.com/crowdster/member/unsubscribe/confirm.do?mailBlastId=${mailBlastId}&amp;memberId=${memberId}\">click here</a></font></td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n',2),(1251,'alex canet\'s support for paul1',86470,'Please visit our campaign: alex canet\'s support for paul1','<title></title>\r\n<table width=\"600\">\r\n	<tbody>\r\n		<tr>\r\n			<td width=\"10\">&nbsp;</td>\r\n			<td width=\"590\">\r\n			<table style=\"margin: 0px; margin-bottom: 4px;\">\r\n				<tbody>\r\n					<tr>\r\n						<td align=\"left\" valign=\"top\">&nbsp;</td>\r\n						<td align=\"left\" valign=\"top\">\r\n						<p style=\"font-size: 14px;\"><font face=\"Arial\">Please visit:<br />\r\n						<a href=\"https://paul1.karma411demo.com/campaign/viewDetails.do?campaignId=6157&amp;email=${memberEmail}&amp;firstName=${memberFirstName}&amp;lastName=${memberLastName}\" style=\"font-weight: bold; font-size: 15px; color: blue;\" target=\"new\">alex canet&#39;s support for paul1</a><br />\r\n						to benefit Autism Speaks Inc..</font></p>\r\n						</td>\r\n					</tr>\r\n				</tbody>\r\n			</table>\r\n			<br clear=\"all\" />\r\n			<font face=\"Arial\">Dear [FRIEND],<br />\r\n			<br />\r\n			<br />\r\n			Will you please help me with this important campaign: alex canet&#39;s support for paul1 (please see the link above)?<br />\r\n			<br />\r\n			Autism Speaks Inc. is an organization I care a lot about! By the time you read this, some of our friends have likely already participated in this campaign. I hope you do too!<br />\r\n			<br />\r\n			Sincerely,<br />\r\n			<br />\r\n			alex canet<br clear=\"all\" />\r\n			<br clear=\"all\" />\r\n			<font color=\"gray\">Trouble with above links? Please copy URL: https://paul1.karma411demo.com/campaign/viewDetails.do?campaignId=6157&amp;email=${memberEmail}&amp;firstName=${memberFirstName}&amp;lastName=${memberLastName}</font><br clear=\"all\" />\r\n			To unsubscribe from this campaign <a href=\"http://karma411demo.com/crowdster/member/unsubscribe/confirm.do?mailBlastId=${mailBlastId}&amp;memberId=${memberId}\">click here</a></font></td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n',2),(1252,'Nicole Bettan\'s support for Another Great Walk',86502,'Please visit our campaign: Nicole Bettan\'s support for Another Great Walk','<title></title>\r\n<table width=\"600\">\r\n	<tbody>\r\n		<tr>\r\n			<td width=\"10\">&nbsp;</td>\r\n			<td width=\"590\">\r\n			<table style=\"margin: 0px; margin-bottom: 4px;\">\r\n				<tbody>\r\n					<tr>\r\n						<td align=\"left\" valign=\"top\">&nbsp;</td>\r\n						<td align=\"left\" valign=\"top\">\r\n						<p style=\"font-size: 14px;\"><font face=\"Arial\">Please visit:<br />\r\n						<a href=\"https://anothergreatwalk.karma411demo.com/campaign/viewDetails.do?campaignId=6179&amp;email=${memberEmail}&amp;firstName=${memberFirstName}&amp;lastName=${memberLastName}\" style=\"font-weight: bold; font-size: 15px; color: blue;\" target=\"new\">Nicole Bettan&#39;s support for Another Great Walk</a><br />\r\n						to benefit Center for the Acceleration of African American Business Caaab.</font></p>\r\n						</td>\r\n					</tr>\r\n				</tbody>\r\n			</table>\r\n			<br clear=\"all\" />\r\n			<font face=\"Arial\">Dear [FRIEND],</font><br />\r\n			<br />\r\n			&nbsp;\r\n			<p><font face=\"Arial\">null<span style=\"color: rgb(0, 0, 0); font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px; line-height: 17px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum claritatem. Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius. Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc putamus parum claram, anteposuerit litterarum formas humanitatis per seacula quarta decima et quinta decima. Eodem modo typi, qui nunc nobis videntur parum clari, fiant sollemnes in futurum.</span></font></p>\r\n			<br />\r\n			<font face=\"Arial\">Nicole Bettan<br clear=\"all\" />\r\n			<img src=\"/userfiles/2/2/profile picture_1365627137871.PNG\" width=\"90\" /><br clear=\"all\" />\r\n			<br clear=\"all\" />\r\n			<font color=\"gray\">Trouble with above links? Please copy URL: https://anothergreatwalk.karma411demo.com/campaign/viewDetails.do?campaignId=6179&amp;email=${memberEmail}&amp;firstName=${memberFirstName}&amp;lastName=${memberLastName}</font><br clear=\"all\" />\r\n			To unsubscribe from this campaign <a href=\"http://karma411demo.com/crowdster/member/unsubscribe/confirm.do?mailBlastId=${mailBlastId}&amp;memberId=${memberId}\">click here</a></font></td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n',2),(1253,'auction',86480,'Please visit our campaign: auction','<title></title>\r\n<table width=\"600\">\r\n	<tbody>\r\n		<tr>\r\n			<td width=\"10\">&nbsp;</td>\r\n			<td width=\"590\">\r\n			<table style=\"margin: 0px; margin-bottom: 4px;\">\r\n				<tbody>\r\n					<tr>\r\n						<td align=\"left\" valign=\"top\">&nbsp;</td>\r\n						<td align=\"left\" valign=\"top\">\r\n						<p style=\"font-size: 14px;\"><font face=\"Arial\">Please visit:<br />\r\n						<a href=\"https://karma411demo.com/campaign/viewDetails.do?campaignId=1362144738576&amp;email=${memberEmail}&amp;firstName=${memberFirstName}&amp;lastName=${memberLastName}\" style=\"font-weight: bold; font-size: 15px; color: blue;\" target=\"new\">auction</a><br />\r\n						to benefit Yuva Hindi Sansthan Inc..</font></p>\r\n						</td>\r\n					</tr>\r\n				</tbody>\r\n			</table>\r\n			<br clear=\"all\" />\r\n			<font face=\"Arial\">Dear [FRIEND],<br />\r\n			<br />\r\n			<br />\r\n			Will you please help me with this important campaign: auction (please see the link above)?<br />\r\n			<br />\r\n			Yuva Hindi Sansthan Inc. is an organization I care a lot about! By the time you read this, some of our friends have likely already participated in this campaign. I hope you do too!<br />\r\n			<br />\r\n			Sincerely,<br />\r\n			<br />\r\n			sachin tomar<br clear=\"all\" />\r\n			<img src=\"/userfiles/10/0/images (2).jpg\" width=\"90\" /><br clear=\"all\" />\r\n			<br clear=\"all\" />\r\n			<font color=\"gray\">Trouble with above links? Please copy URL: https://karma411demo.com/campaign/viewDetails.do?campaignId=1362144738576&amp;email=${memberEmail}&amp;firstName=${memberFirstName}&amp;lastName=${memberLastName}</font><br clear=\"all\" />\r\n			To unsubscribe from this campaign <a href=\"http://karma411demo.com/crowdster/member/unsubscribe/confirm.do?mailBlastId=${mailBlastId}&amp;memberId=${memberId}\">click here</a></font></td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n',2);
/*!40000 ALTER TABLE `mailmessage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mailsources`
--

DROP TABLE IF EXISTS `mailsources`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mailsources` (
  `id` int(11) NOT NULL,
  `mailSourceId` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mailsources`
--

LOCK TABLES `mailsources` WRITE;
/*!40000 ALTER TABLE `mailsources` DISABLE KEYS */;
/*!40000 ALTER TABLE `mailsources` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `marketingmessage`
--

DROP TABLE IF EXISTS `marketingmessage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `marketingmessage` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `name` varchar(400) DEFAULT NULL,
  `description` text,
  `response` text,
  `keyword` varchar(400) DEFAULT NULL,
  `imagePath` int(11) DEFAULT NULL,
  `mastheadCode` text,
  `fileName` varchar(400) DEFAULT NULL,
  `formType` int(11) NOT NULL DEFAULT '0',
  `formHeader` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `marketingmessage`
--

LOCK TABLES `marketingmessage` WRITE;
/*!40000 ALTER TABLE `marketingmessage` DISABLE KEYS */;
/*!40000 ALTER TABLE `marketingmessage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `mastercampaignnumberdonations`
--

DROP TABLE IF EXISTS `mastercampaignnumberdonations`;
/*!50001 DROP VIEW IF EXISTS `mastercampaignnumberdonations`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `mastercampaignnumberdonations` (
  `id` tinyint NOT NULL,
  `value` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `mastercampaignnumberofflinedonations`
--

DROP TABLE IF EXISTS `mastercampaignnumberofflinedonations`;
/*!50001 DROP VIEW IF EXISTS `mastercampaignnumberofflinedonations`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `mastercampaignnumberofflinedonations` (
  `id` tinyint NOT NULL,
  `value` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `mastercampaigntotaldonations`
--

DROP TABLE IF EXISTS `mastercampaigntotaldonations`;
/*!50001 DROP VIEW IF EXISTS `mastercampaigntotaldonations`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `mastercampaigntotaldonations` (
  `id` tinyint NOT NULL,
  `value` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `mastercampaigntotalofflinedonations`
--

DROP TABLE IF EXISTS `mastercampaigntotalofflinedonations`;
/*!50001 DROP VIEW IF EXISTS `mastercampaigntotalofflinedonations`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `mastercampaigntotalofflinedonations` (
  `id` tinyint NOT NULL,
  `value` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `media`
--

DROP TABLE IF EXISTS `media`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `media` (
  `id` bigint(20) unsigned NOT NULL,
  `blobId` bigint(20) DEFAULT NULL,
  `siteId` bigint(20) DEFAULT NULL,
  `superSiteId` bigint(20) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  `fileName` varchar(200) DEFAULT NULL,
  `uri` varchar(200) DEFAULT NULL,
  `memberId` bigint(20) DEFAULT NULL,
  `source` tinyint(4) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `media`
--

LOCK TABLES `media` WRITE;
/*!40000 ALTER TABLE `media` DISABLE KEYS */;
/*!40000 ALTER TABLE `media` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `mediadata`
--

DROP TABLE IF EXISTS `mediadata`;
/*!50001 DROP VIEW IF EXISTS `mediadata`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `mediadata` (
  `id` tinyint NOT NULL,
  `blobId` tinyint NOT NULL,
  `siteId` tinyint NOT NULL,
  `superSiteId` tinyint NOT NULL,
  `name` tinyint NOT NULL,
  `description` tinyint NOT NULL,
  `fileName` tinyint NOT NULL,
  `uri` tinyint NOT NULL,
  `memberId` tinyint NOT NULL,
  `source` tinyint NOT NULL,
  `created` tinyint NOT NULL,
  `updated` tinyint NOT NULL,
  `tagsname` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `mediatag`
--

DROP TABLE IF EXISTS `mediatag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mediatag` (
  `id` bigint(20) unsigned NOT NULL,
  `mediaId` bigint(20) DEFAULT NULL,
  `tagId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mediatag`
--

LOCK TABLES `mediatag` WRITE;
/*!40000 ALTER TABLE `mediatag` DISABLE KEYS */;
/*!40000 ALTER TABLE `mediatag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `member`
--

DROP TABLE IF EXISTS `member`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `member` (
  `id` bigint(20) NOT NULL DEFAULT '0',
  `idCode` varchar(8) NOT NULL,
  `firstName` varchar(40) NOT NULL,
  `lastName` varchar(40) NOT NULL,
  `email` varchar(100) DEFAULT NULL,
  `password` varchar(100) NOT NULL,
  `password_salt` varchar(60) DEFAULT NULL,
  `activated` datetime DEFAULT NULL,
  `middleName` varchar(40) DEFAULT NULL,
  `nickname` varchar(40) DEFAULT NULL,
  `photoPath` int(11) DEFAULT NULL,
  `nameVerified` datetime DEFAULT NULL,
  `pseudonym` varchar(50) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `deleteTime` datetime DEFAULT NULL,
  `showAccess` varchar(15) DEFAULT NULL,
  `memberPhotoMediaId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `member`
--

LOCK TABLES `member` WRITE;
/*!40000 ALTER TABLE `member` DISABLE KEYS */;
INSERT INTO `member` VALUES (86450,'9ZYRSVJ6','John','Murcott','crowdsterdemo@karma411.com','E8bQSE8OA2NOXUmTy7J67eulWVk=',NULL,'2013-03-01 08:12:30',NULL,NULL,1,NULL,NULL,'2013-03-01 08:12:30',NULL,'1,0,0,0',NULL),(86451,'UBABV6GY','Crowdster','','info@karma411.com','E8bQSE8OA2NOXUmTy7J67eulWVk=',NULL,NULL,NULL,NULL,1,NULL,NULL,'2013-03-01 08:12:30',NULL,'1,0,0,0',NULL),(86452,'L4NY9Z4C','Paul','Murcott','paul@karma411.com','E8bQSE8OA2NOXUmTy7J67eulWVk=',NULL,'2013-03-01 15:28:54',NULL,NULL,1,NULL,NULL,'2013-03-01 15:28:54',NULL,'1,0,2,0',NULL),(1362146005137,'LNELVNUQ','Alex','Test','gilyashov.alexandr@gmail.com','zbIy1CJQNDDtp9N6gg/ucLvwAU0=',NULL,'2015-08-31 06:56:38',NULL,NULL,1,NULL,NULL,'2015-08-31 06:56:38',NULL,'1,0,0,0',0),(1362146005194,'4KXAGF4G','Reg','Test','reg@test.qwe','E8bQSE8OA2NOXUmTy7J67eulWVk=',NULL,'2015-08-31 07:31:47',NULL,NULL,1,NULL,NULL,'2015-08-31 07:31:47',NULL,'1,0,0,0',0),(1362146005569,'UIVVVAM9','Crowdster','Crowdster','info@crowdster.com','password',NULL,NULL,NULL,NULL,1,NULL,NULL,'2015-08-31 18:25:21',NULL,'1,0,0,0',0),(1362146005759,'LBALY4AV','Crowdster','Crowdster','qwe3@qwe3.qwe','password',NULL,NULL,NULL,NULL,1,NULL,NULL,'2015-09-01 12:30:25',NULL,'1,0,0,0',0),(1362146005764,'85HZV3D2','Ron','Red','ron@red.qwe','xVmnEcgUI8tx6AI9eiKisDtkR8g=',NULL,'2015-09-01 12:46:26',NULL,NULL,1,NULL,NULL,NULL,NULL,'1,0,0,0',0),(1362146005778,'V6R7SBCK','Usert','First','qwe1@qwe1.qwe','zbIy1CJQNDDtp9N6gg/ucLvwAU0=',NULL,'2015-09-01 12:47:23',NULL,NULL,1,NULL,NULL,NULL,NULL,'1,0,0,0',0),(1362146005787,'IAJNXFF2','Team','Owner','team@test.qwe','zbIy1CJQNDDtp9N6gg/ucLvwAU0=',NULL,'2015-09-01 12:47:56',NULL,NULL,1,NULL,NULL,NULL,NULL,'1,0,0,0',0),(1362146005801,'9G5NZBEV','User','Second','qwe2@qwe2.qwe','zbIy1CJQNDDtp9N6gg/ucLvwAU0=',NULL,'2015-09-01 12:48:32',NULL,NULL,1,NULL,NULL,NULL,NULL,'1,0,0,0',0),(1362146005831,'PE73VBYN','qwe','qwe','qwe@qwe.qwea','xVmnEcgUI8tx6AI9eiKisDtkR8g=',NULL,'2015-09-01 12:55:06',NULL,NULL,1,NULL,NULL,NULL,NULL,'1,0,0,0',0),(1362146005839,'7NQ8LTC2','John','Green','john@qwe.qwe','xVmnEcgUI8tx6AI9eiKisDtkR8g=',NULL,'2015-09-01 12:56:13',NULL,NULL,1,NULL,NULL,NULL,NULL,'1,0,0,0',0);
/*!40000 ALTER TABLE `member` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `memberauthorization`
--

DROP TABLE IF EXISTS `memberauthorization`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `memberauthorization` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `memberId` bigint(20) DEFAULT NULL,
  `totalSites` int(11) DEFAULT NULL,
  `totalSolutions` int(11) DEFAULT NULL,
  `totalPages` int(11) DEFAULT NULL,
  `allowAuction` tinyint(4) NOT NULL DEFAULT '0',
  `allowWhiteLabel` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `memberauthorization`
--

LOCK TABLES `memberauthorization` WRITE;
/*!40000 ALTER TABLE `memberauthorization` DISABLE KEYS */;
INSERT INTO `memberauthorization` VALUES (1,'2015-08-31 06:56:38',1362146005137,100,100,0,0,0),(2,'2015-08-31 07:31:48',1362146005194,100,100,0,0,0);
/*!40000 ALTER TABLE `memberauthorization` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `memberblock`
--

DROP TABLE IF EXISTS `memberblock`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `memberblock` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `memberId` bigint(20) DEFAULT NULL,
  `blockedId` bigint(20) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `memberId` (`memberId`),
  KEY `blockedId` (`blockedId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `memberblock`
--

LOCK TABLES `memberblock` WRITE;
/*!40000 ALTER TABLE `memberblock` DISABLE KEYS */;
/*!40000 ALTER TABLE `memberblock` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `membercampaigncount`
--

DROP TABLE IF EXISTS `membercampaigncount`;
/*!50001 DROP VIEW IF EXISTS `membercampaigncount`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `membercampaigncount` (
  `id` tinyint NOT NULL,
  `count` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `memberdetails`
--

DROP TABLE IF EXISTS `memberdetails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `memberdetails` (
  `id` bigint(20) DEFAULT NULL,
  `isMinor` tinyint(4) DEFAULT NULL,
  `parentFirstName` varchar(40) DEFAULT NULL,
  `parentLastName` varchar(40) DEFAULT NULL,
  `languageId` int(11) DEFAULT NULL,
  `gender` tinyint(4) DEFAULT NULL,
  `maidenName` varchar(40) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `city` varchar(75) DEFAULT NULL,
  `state` varchar(20) DEFAULT NULL,
  `zip` varchar(10) DEFAULT NULL,
  `country` int(11) DEFAULT NULL,
  `aboutMe` varchar(2500) DEFAULT NULL,
  `bioURL` varchar(150) DEFAULT NULL,
  `maritalStatus` tinyint(2) DEFAULT NULL,
  `awards` varchar(100) DEFAULT NULL,
  `motto` varchar(80) DEFAULT NULL,
  `credit` float DEFAULT NULL,
  `mediaURL` varchar(1000) DEFAULT NULL,
  `spouseId` bigint(20) DEFAULT NULL,
  `phone` varchar(30) DEFAULT NULL,
  `leadsource` int(11) DEFAULT NULL,
  `orgName` varchar(200) DEFAULT NULL,
  KEY `memberdetails_index_id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `memberdetails`
--

LOCK TABLES `memberdetails` WRITE;
/*!40000 ALTER TABLE `memberdetails` DISABLE KEYS */;
INSERT INTO `memberdetails` VALUES (86450,0,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,0,NULL,NULL,0,NULL,NULL,NULL,0,NULL),(86451,0,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,0,NULL,NULL,0,NULL,NULL,NULL,0,NULL),(86452,0,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,0,NULL,NULL,0,NULL,NULL,NULL,0,NULL),(1362146005137,0,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,0,'',NULL,0,NULL,NULL,NULL,0,'Test'),(1362146005194,0,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,0,'',NULL,0,NULL,NULL,NULL,0,'Organization'),(1362146005569,0,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,0,NULL,NULL,0,NULL,NULL,NULL,0,NULL),(1362146005759,0,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,0,NULL,NULL,0,NULL,NULL,NULL,0,NULL),(1362146005764,0,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,0,NULL,NULL,0,NULL,NULL,NULL,0,NULL),(1362146005778,0,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,0,NULL,NULL,0,NULL,NULL,NULL,0,NULL),(1362146005787,0,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,0,NULL,NULL,0,NULL,NULL,NULL,0,NULL),(1362146005801,0,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,0,NULL,NULL,0,NULL,NULL,NULL,0,NULL),(1362146005831,0,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,0,NULL,NULL,0,NULL,NULL,NULL,0,NULL),(1362146005839,0,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,0,NULL,NULL,0,NULL,NULL,NULL,0,NULL);
/*!40000 ALTER TABLE `memberdetails` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `memberfriendcount`
--

DROP TABLE IF EXISTS `memberfriendcount`;
/*!50001 DROP VIEW IF EXISTS `memberfriendcount`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `memberfriendcount` (
  `id` tinyint NOT NULL,
  `count` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `membergroupcount`
--

DROP TABLE IF EXISTS `membergroupcount`;
/*!50001 DROP VIEW IF EXISTS `membergroupcount`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `membergroupcount` (
  `id` tinyint NOT NULL,
  `count` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `memberinvitationcount`
--

DROP TABLE IF EXISTS `memberinvitationcount`;
/*!50001 DROP VIEW IF EXISTS `memberinvitationcount`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `memberinvitationcount` (
  `id` tinyint NOT NULL,
  `count` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `memberklogcount`
--

DROP TABLE IF EXISTS `memberklogcount`;
/*!50001 DROP VIEW IF EXISTS `memberklogcount`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `memberklogcount` (
  `id` tinyint NOT NULL,
  `count` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `memberlanguage`
--

DROP TABLE IF EXISTS `memberlanguage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `memberlanguage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `memberId` bigint(20) DEFAULT NULL,
  `languageId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `memberId` (`memberId`),
  KEY `languageId` (`languageId`),
  CONSTRAINT `memberlanguage_ibfk_2` FOREIGN KEY (`languageId`) REFERENCES `language` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `memberlanguage`
--

LOCK TABLES `memberlanguage` WRITE;
/*!40000 ALTER TABLE `memberlanguage` DISABLE KEYS */;
/*!40000 ALTER TABLE `memberlanguage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `memberpollcount`
--

DROP TABLE IF EXISTS `memberpollcount`;
/*!50001 DROP VIEW IF EXISTS `memberpollcount`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `memberpollcount` (
  `id` tinyint NOT NULL,
  `count` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `memberpostcount`
--

DROP TABLE IF EXISTS `memberpostcount`;
/*!50001 DROP VIEW IF EXISTS `memberpostcount`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `memberpostcount` (
  `id` tinyint NOT NULL,
  `count` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `memberrole`
--

DROP TABLE IF EXISTS `memberrole`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `memberrole` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `memberId` bigint(20) DEFAULT NULL,
  `roleId` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `memberId` (`memberId`),
  KEY `roleId` (`roleId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `memberrole`
--

LOCK TABLES `memberrole` WRITE;
/*!40000 ALTER TABLE `memberrole` DISABLE KEYS */;
/*!40000 ALTER TABLE `memberrole` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `memberstats`
--

DROP TABLE IF EXISTS `memberstats`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `memberstats` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `memberId` bigint(20) DEFAULT NULL,
  `totalFreePosts` int(11) DEFAULT NULL,
  `totalPaidPosts` int(11) DEFAULT NULL,
  `totalPostValue` float DEFAULT NULL,
  `totalCOTValue` float DEFAULT NULL,
  `totalCOT` int(11) DEFAULT NULL,
  `totalInvitations` int(11) DEFAULT NULL,
  `totalPolls` int(11) DEFAULT NULL,
  `totalPollValue` float DEFAULT NULL,
  `recentVisits` int(11) DEFAULT NULL,
  `recentVotes` int(11) DEFAULT NULL,
  `recentFreePosts` int(11) DEFAULT NULL,
  `recentPaidPosts` int(11) DEFAULT NULL,
  `recentAcceptedInvitations` int(11) DEFAULT NULL,
  `recentPollResponses` int(11) DEFAULT NULL,
  `recentCoupons` float DEFAULT NULL,
  `karmaScore` int(11) DEFAULT NULL,
  `totalDonations` float DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `memberId` (`memberId`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `memberstats`
--

LOCK TABLES `memberstats` WRITE;
/*!40000 ALTER TABLE `memberstats` DISABLE KEYS */;
INSERT INTO `memberstats` VALUES (1,86450,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),(2,86451,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),(3,86452,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),(4,1362146005137,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),(5,1362146005194,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),(6,1362146005764,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),(7,1362146005778,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),(8,1362146005787,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),(9,1362146005801,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),(10,1362146005759,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
/*!40000 ALTER TABLE `memberstats` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `micrositetemplate`
--

DROP TABLE IF EXISTS `micrositetemplate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `micrositetemplate` (
  `id` bigint(20) NOT NULL,
  `created` datetime DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `siteId` bigint(20) DEFAULT NULL,
  `description` varchar(1000) DEFAULT NULL,
  `templateImageId` bigint(20) DEFAULT NULL,
  `categoryId` bigint(20) DEFAULT NULL,
  `memberId` bigint(20) DEFAULT NULL,
  `mediaId` bigint(20) DEFAULT '0',
  `subPageMediaId` bigint(20) DEFAULT NULL,
  `participateMediaId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `micrositetemplate`
--

LOCK TABLES `micrositetemplate` WRITE;
/*!40000 ALTER TABLE `micrositetemplate` DISABLE KEYS */;
INSERT INTO `micrositetemplate` VALUES (1362145328624,'2014-07-16 18:12:03','Basic Benefit Template',1362145328620,'',1362145332076,1362145328570,1362145318095,0,NULL,NULL),(1362145328660,'2014-07-16 18:45:34','Advanced Banefit Template',1362145328656,'',1362145345069,1362145328570,1362145318095,0,NULL,NULL),(1362145328765,'2014-07-16 19:51:48','Gala Basic Template',1362145328761,'',1362145328811,1362145328574,1362145318095,0,NULL,NULL),(1362145328783,'2014-07-16 20:02:55','Run/Walk Basic Template',1362145328779,'',1362145328838,1362145328578,1362145318095,0,NULL,NULL),(1362145328800,'2014-07-16 20:11:25','Gala Advanced Template',1362145328796,'',1362145328810,1362145328574,1362145318095,0,NULL,NULL),(1362145328848,'2014-07-16 20:59:56','Run/Walk Advanced Template',1362145328844,'',0,1362145328578,1362145318095,0,NULL,NULL),(1362145329518,'2014-07-17 13:54:01','Luncheon Basic Template',1362145329514,'<p>A basic template for a luncheon</p>\r\n',0,1362145328584,1362145318095,0,NULL,NULL),(1362145329526,'2014-07-17 13:54:22','Pancake Breakfast Basic ',1362145329522,'',0,1362145328596,1362145318095,0,NULL,NULL),(1362145329587,'2014-07-17 14:10:39','Pancake Breakfast Advanced ',1362145329583,'',0,1362145328596,1362145318095,0,NULL,NULL),(1362145329599,'2014-07-17 14:12:56','Pancake Breakfast ',1362145329595,'',0,1362145328596,1362145318095,0,NULL,NULL),(1362145329612,'2014-07-17 14:13:46','Luncheon Advanced Template',1362145329608,'<p>Luncheon Advanced Template</p>\r\n',0,1362145328584,1362145318095,0,NULL,NULL),(1362145330084,'2014-07-17 14:51:21','Concert Advanced Template',1362145330080,'<p>Template for a Concert</p>\r\n',0,1362145328572,1362145318095,0,NULL,NULL),(1362145330577,'2014-07-17 15:24:03','Golf Basic Template',1362145330573,'<p>Template for Golf</p>\r\n',0,1362145328576,1362145318095,0,NULL,NULL),(1362145330593,'2014-07-17 15:38:43','Appeal Basic Template',1362145330589,'<p>Template for appeal</p>\r\n',0,1362145328576,1362145318095,0,NULL,NULL),(1362145330600,'2014-07-17 15:38:55','Art Exhibition Basic',1362145330596,'',0,1362145328598,1362145318095,0,NULL,NULL),(1362145330614,'2014-07-17 15:42:18','Art Exhibition Advanced',1362145330610,'',0,1362145328598,1362145318095,0,NULL,NULL),(1362145330625,'2014-07-17 15:42:52','Appeal Advanced Template',1362145330621,'<p>Advanced template for appeal</p>\r\n',0,1362145328576,1362145318095,0,NULL,NULL),(1362145332130,'2014-07-17 17:38:36','New Benefit Template',1362145332126,'',1362145332131,1362145328570,1362145318095,0,NULL,NULL),(1362145345085,'2014-07-22 12:34:24','Test Template for Paul 7-22',1362145345081,'<p>Description</p>\r\n',0,1362145328592,1362145318095,0,NULL,NULL),(1362145346115,'2014-07-22 17:57:21','Benefit',1362145346111,'',0,1362145346097,1362145346085,0,NULL,NULL),(1362145346125,'2014-07-22 18:05:25','Ronald McDonald House Fundraiser',1362145346121,'<p>This is a fundraiser for the Ronald McDonald House foundation.&nbsp;</p>\r\n',1362145347078,1362145346099,1362145346085,0,NULL,NULL),(1362145346152,'2014-07-22 18:19:06','Luncheon - Draft',1362145346148,'<p>A template for a Luncheon</p>\r\n',0,1362145346101,1362145346085,0,NULL,NULL),(1362145346177,'2014-07-22 18:33:01','Luncheon - Ed Draft',1362145346173,'<p>Good template for Luncheons</p>\r\n',0,1362145346101,1362145346085,0,NULL,NULL),(1362145346640,'2014-07-22 19:48:55','Luncheon',1362145346636,'<p>Basic Luncheon</p>\r\n',1362145346669,1362145346101,1362145346085,0,NULL,NULL),(1362145353594,'2014-07-23 15:25:22','Benefit - Helping Abroad',1362145353590,'<p>This is a really nice Template for a Benefit</p>\r\n',1362145354569,1362145329072,1362145328864,0,NULL,NULL),(1362145355596,'2014-07-23 19:54:25','Local Parish Fundraiser',1362145355592,'<p>Local Parish Fundraiser</p>\r\n',1362145356569,1362145329076,1362145328864,0,NULL,NULL),(1362145357076,'2014-07-23 21:20:52','Luncheon',1362145357072,'<p>Basic Luncheon</p>\r\n',1362145357097,1362145346103,1362145328864,0,NULL,NULL),(1362145361217,'2014-07-24 05:16:18','mytemp1',1362145361213,'<p>dfdf</p>\r\n',0,1362145688111,86480,1362145731072,1362145706617,1362145691218),(1362145361223,'2014-07-24 05:17:44','test66',1362145361219,'<p>asdf</p>\r\n',0,1362145688111,86480,1362145489109,1362145490831,1362145485584),(1362145361240,'2014-07-24 05:54:04','destop123',1362145361236,'<p>test</p>\r\n',0,0,86480,0,NULL,NULL),(1362145372099,'2014-07-25 20:06:03','Virtual Lemonade Stand',1362145372095,'<p>Virtual Lemonade Stand</p>\r\n',1362145372105,1362145372082,1362145372077,0,NULL,NULL),(1362145372121,'2014-07-25 20:35:33','Virtual Piggy Bank',1362145372117,'<p>Virtual Piggy Bank</p>\r\n',1362145372123,1362145372080,1362145372077,0,NULL,NULL),(1362145374599,'2014-07-28 08:03:33','today34',1362145374595,'<p>test55</p>\r\n',0,1362145688111,86480,1362145417268,1362145485584,1362145444570),(1362145376093,'2014-07-28 14:26:59','TestTemplate',1362145376089,'',1362145376099,1362145376106,1362145376078,0,NULL,NULL),(1362145379301,'2014-07-29 04:56:20','Hankok temp',1362145379297,'<p>testoo6</p>\r\n',0,0,86480,0,NULL,NULL),(1362145379613,'2014-07-29 05:13:57','Nh370',1362145379609,'<p>Max12</p>\r\n',0,1362145556569,86480,1362145507599,1362145489109,1362145485584),(1362145380079,'2014-07-29 05:38:20','Template-One time',1362145380073,NULL,0,0,86480,0,NULL,NULL),(1362145380115,'2014-07-29 06:29:26','Vexx55',1362145380111,'<p>test</p>\r\n',1362145380116,1362145688647,86480,0,0,0),(1362145382076,'2014-07-29 09:09:06','MS 50',1362145382072,'<p>test</p>\r\n',0,0,86480,1362145507599,NULL,NULL),(1362145383077,'2014-07-29 15:46:46','Bake Sale',1362145383073,'',1362145383079,1362145372084,1362145372077,0,NULL,NULL),(1362145383116,'2014-07-29 18:06:50','In Memory Of ',1362145383112,'',1362145383156,1362145372088,1362145372077,0,NULL,NULL),(1362145383122,'2014-07-29 18:12:21','Birthday ',1362145383118,'',1362145383123,1362145372086,1362145372077,0,NULL,NULL),(1362145383162,'2014-07-29 19:45:42','Marathon',1362145383158,'',1362145383176,1362145372092,1362145372077,0,NULL,NULL),(1362145383164,'2014-07-29 19:46:25','Luncheon ',1362145383158,'',1362145383166,1362145372090,1362145372077,0,NULL,NULL),(1362145388627,'2014-07-30 20:39:40','Potluck Luncheon',1362145388623,'<p>luncheon</p>\r\n',1362145388628,1362145372090,1362145372077,0,NULL,NULL),(1362145389133,'2014-07-31 12:11:07','Tarrt888',1362145389129,'<p>test</p>\r\n',1362145389134,0,86480,0,NULL,NULL),(1362145389583,'2014-07-31 13:48:31','Fundraiser',1362145389579,'<p>Fundraising template</p>\r\n',1362145389588,1362145389586,1362145372077,0,NULL,NULL),(1362145404138,'2014-08-05 06:50:36','Template-Ban45',1362145404133,NULL,0,0,86480,0,NULL,NULL),(1362145404146,'2014-08-05 06:51:01','Template-One time-1',1362145404140,NULL,0,0,86480,0,NULL,NULL),(1362145404153,'2014-08-05 06:51:39','Template-Ban45-1',1362145404148,NULL,0,0,86480,0,NULL,NULL),(1362145404160,'2014-08-05 06:52:37','Template-gala-2',1362145404155,NULL,0,0,86480,0,NULL,NULL),(1362145404168,'2014-08-05 06:56:30','Template-TT11-3',1362145404162,NULL,0,0,86480,0,NULL,NULL),(1362145404176,'2014-08-05 07:00:24','Template-TT11-4',1362145404170,NULL,0,0,86480,0,NULL,NULL),(1362145404598,'2014-08-05 12:00:59','Template-Ganex 1-3',1362145404592,NULL,0,0,86480,0,NULL,NULL),(1362145404607,'2014-08-05 12:02:00','Template-Ganex 1-4',1362145404601,NULL,0,0,86480,0,NULL,NULL),(1362145404616,'2014-08-05 12:02:29','Template-test12',1362145404610,NULL,0,0,86480,0,NULL,NULL),(1362145405603,'2014-08-05 20:26:14','Marathonn',1362145405599,'',0,1362145405596,1362144796118,0,NULL,NULL),(1362145406075,'2014-08-05 20:33:51','Template-SINGLE PLAYER REGISTRATIONn',1362145406070,NULL,0,0,1362144796118,0,NULL,NULL),(1362145406082,'2014-08-05 20:34:21','Template-SINGLE PLAYER REGISTRATIONn-1',1362145406077,'',0,1362145405596,1362144796118,0,NULL,NULL),(1362145416087,'2014-08-08 01:01:43','ppt',1362145416083,'<p>test</p>\r\n',1362145416088,1362145380107,86480,0,NULL,NULL),(1362145418095,'2014-08-08 11:53:35','templatetest',1362145418091,'<p>this is a test template</p>\r\n',1362145418100,1362145418088,1362145372077,0,NULL,NULL),(1362145427576,'2014-08-12 03:20:03','Template-okhla',1362145427570,NULL,0,0,86480,0,NULL,NULL),(1362145432684,'2014-08-12 06:15:55','Template-okhla-1',1362145432678,NULL,0,0,86480,0,NULL,NULL),(1362145432691,'2014-08-12 06:16:34','Template-mytemp',1362145432686,NULL,0,0,86480,0,NULL,NULL),(1362145432706,'2014-08-12 06:17:53','Template-Owan test',1362145432701,NULL,0,0,86480,0,NULL,NULL),(1362145432718,'2014-08-12 06:28:40','Template-unin7',1362145432712,NULL,0,0,86480,0,NULL,NULL),(1362145432730,'2014-08-12 06:35:24','Template-admin',1362145432724,NULL,0,0,86480,0,NULL,NULL),(1362145432744,'2014-08-12 06:35:57','Template-sda',1362145432739,NULL,0,0,86480,0,NULL,NULL),(1362145432809,'2014-08-12 08:22:46','Template-Owan test-1',1362145432804,NULL,0,0,86480,0,NULL,NULL),(1362145452765,'2014-08-19 07:17:48','Template-Auction',1362145452759,NULL,0,0,86480,0,NULL,NULL),(1362145452776,'2014-08-19 07:19:19','Template-Kmk56',1362145452770,NULL,0,0,86480,0,NULL,NULL),(1362145452790,'2014-08-19 09:45:13','Template-asdf',1362145452784,NULL,0,0,86480,0,NULL,NULL),(1362145452797,'2014-08-19 09:45:35','Template-sda-1',1362145452792,NULL,0,0,86480,0,NULL,NULL),(1362145470490,'2014-08-26 06:35:03','nano',1362145470486,'<p>test</p>\r\n',1362145470491,1362145379093,86480,0,NULL,NULL),(1362145470499,'2014-08-26 06:46:04','Template-test12-1',1362145470493,NULL,0,0,86480,0,NULL,NULL),(1362145470507,'2014-08-26 06:58:02','Template-game',1362145470501,'',0,0,86480,0,NULL,NULL),(1362145470515,'2014-08-26 06:58:20','Template-test12-2',1362145470509,NULL,0,0,86480,0,NULL,NULL),(1362145472616,'2014-08-27 13:44:29','25 tempo',1362145472612,'<p>test</p>\r\n',1362145472617,1362145379093,86480,0,NULL,NULL),(1362145472623,'2014-08-27 13:51:14','Bot27',1362145472619,'<p>test</p>\r\n',0,1362145380109,86480,0,NULL,NULL),(1362145472629,'2014-08-27 14:01:31','MK2',1362145472625,'<p>test</p>\r\n',0,1362145379093,86480,0,NULL,NULL),(1362145472651,'2014-08-27 14:07:59','27today',1362145472647,'<p>test</p>\r\n',0,1362145379093,86480,0,NULL,NULL),(1362145479656,'2014-08-28 06:17:54','Template-Tarun5',1362145479653,'',0,1362145379093,86480,0,NULL,NULL),(1362145501820,'2014-09-12 07:50:54','job',1362145501816,'<p>test</p>\r\n',0,0,86480,0,NULL,NULL),(1362145522647,'2014-09-22 19:35:42','Test Test 3',1362145522643,'<p>Test</p>\r\n',0,1362145522661,1362145522588,1362145534582,NULL,NULL),(1362145524602,'2014-09-23 17:35:08','Gala',1362145524598,'<p>Gala</p>\r\n',0,1362145522661,1362145522588,0,NULL,NULL),(1362145524744,'2014-09-24 14:40:08','Template-Test Gala',1362145524740,NULL,0,0,1362145522588,0,NULL,NULL),(1362145525595,'2014-09-24 19:23:06','Winter Gala Test Again',1362145525591,'<p>Winter Gala Test Again</p>\r\n',0,1362145522661,1362145522588,0,NULL,NULL),(1362145525603,'2014-09-24 19:27:19','BBQ Test 2',1362145525599,'<p>BBQ Test 2</p>\r\n',0,1362145525584,1362145522588,0,NULL,NULL),(1362145525636,'2014-09-24 20:35:34','',1362145525632,'<p>Test Template 3b</p>\r\n',0,1362145522661,1362145522588,1362145522660,NULL,NULL),(1362145531489,'2014-09-26 07:07:00','TN78bxl',1362145531485,'<p>test</p>\r\n',0,0,86480,1362145515583,NULL,NULL),(1362145533139,'2014-09-26 20:44:04','Template-Winter Gala 2015',1362145533136,NULL,0,0,1362145522588,0,NULL,NULL),(1362145533153,'2014-09-26 20:47:02','Template-Winter Gala 2015-copy',1362145533150,NULL,0,0,1362145522588,0,NULL,NULL),(1362145535598,'2014-09-30 13:19:25','CFF Test',1362145535594,'<p>CFF Test</p>\r\n',0,1362145522661,1362145522588,1362145522649,NULL,NULL),(1362145535621,'2014-09-30 13:30:48','Template-CFF Test 1',1362145535617,NULL,0,0,1362145522588,0,NULL,NULL),(1362145536075,'2014-09-30 14:07:14','Testing QA Template',1362145536071,'<p>Testing QA Template</p>\r\n',0,1362145522661,1362145522588,0,NULL,NULL),(1362145536090,'2014-09-30 14:10:18','Testing QA Again',1362145536086,'<p>Testing QA Again</p>\r\n',0,1362145522661,1362145522588,0,NULL,NULL),(1362145536111,'2014-09-30 14:17:24','Template-Testing QA Again-copy',1362145536107,NULL,0,0,1362145522588,0,NULL,NULL),(1362145536117,'2014-09-30 14:18:39','Template-CFF Test 1-1',1362145536113,NULL,0,0,1362145522588,0,NULL,NULL),(1362145539575,'2014-10-02 02:57:09','TestVipinTemp',1362145539571,'<p>TestVipinTemp</p>\r\n',0,1362145528569,86480,0,NULL,NULL),(1362145550350,'2014-10-07 04:01:07','Template-test',1362145550347,NULL,0,0,1362145550157,0,NULL,NULL),(1362145556581,'2014-10-07 07:28:29','user',1362145556577,'<p>test</p>\r\n',0,1362145556569,86480,1362145489109,1362145471699,1362145456779),(1362145556651,'2014-10-07 15:15:02','10.7 MS Template',1362145556647,'<p>10.7 MS Template</p>\r\n',0,1362145556636,1362145522588,0,NULL,NULL),(1362145688117,'2014-11-27 12:53:10','londday',1362145688113,'',0,1362145688111,86480,1362145471699,1362145426072,1362145489109),(1362145689240,'2014-12-02 07:20:41','decembe',1362145689236,'',0,1362145689230,86480,1362145489109,1362145490831,1362145485584),(1362145691187,'2014-12-03 06:39:20','Template-After night parrty',1362145691184,NULL,0,0,86480,0,0,0),(1362145769694,'2015-03-23 14:31:20','test microsite',1362145769690,'<p>test</p>\r\n',0,1362145757667,1362145689298,0,0,0),(1362145769701,'2015-03-23 14:33:20','Template-Mother\'s Day',1362145769697,NULL,0,0,1362145689298,0,0,0),(1362145806082,'2015-05-04 05:27:48','Ebola Template',1362145806078,'<p>hi this is my template</p>\r\n',0,1362145786171,86480,1362145806097,0,0),(1362145811594,'2015-05-06 19:45:10','Test Golf Template',1362145811590,'',0,1362145811601,1362145689298,0,0,0),(1362145828075,'2015-06-01 17:31:35','Template-Summer show',1362145828070,'',0,1362145837669,1362145827226,1362145838144,0,0),(1362145828083,'2015-06-01 17:31:42','Template-Summer show-copy',1362145828078,NULL,0,0,1362145827226,0,0,0),(1362145837666,'2015-06-12 17:50:08','Template-Summer show-1',1362145837661,NULL,0,0,1362145827226,0,0,0),(1362145837705,'2015-06-12 18:03:08','Template-Summer show-2',1362145837700,'',0,1362145837669,1362145827226,0,0,0),(1362145838094,'2015-06-17 12:27:18','Template-offer later',1362145838089,NULL,0,0,86480,0,0,0),(1362145838100,'2015-06-17 12:28:29','17 templte',1362145838096,'<p>test</p>\r\n',0,1362145688111,86480,1362145807113,1362145806090,1362145806076),(1362145838112,'2015-06-17 12:33:57','Template-offer later-1',1362145838107,NULL,0,0,86480,0,0,0),(1362145838576,'2015-06-17 14:29:50','Template-Summer show-3',1362145838571,'',0,1362145837669,1362145827226,0,0,0),(1362145838593,'2015-06-17 14:31:07','Template-Summer show-4',1362145838588,NULL,0,0,1362145827226,0,0,0),(1362145838626,'2015-06-17 14:38:02','Template-Summer show-5',1362145838621,NULL,0,0,1362145827226,0,0,0),(1362145838642,'2015-06-17 14:48:42','Template-Summer show-6',1362145838637,NULL,0,0,1362145827226,0,0,0),(1362145838654,'2015-06-17 14:56:51','fg',1362145838650,'<p>wert</p>\r\n',0,0,1362145828143,0,0,0),(1362145838660,'2015-06-17 14:59:28','Template-Test Soc MS A',1362145838656,NULL,0,0,1362145828143,0,0,0),(1362145839178,'2015-06-18 20:11:07','Katie\'s Test Template',1362145839174,'<p>Template</p>\r\n',0,1362145757667,1362145689298,0,0,0),(1362145843628,'2015-06-26 14:07:25','Winter Gala',1362145843624,'<p><em>foo</em></p>\r\n',0,1362145843620,1362145835582,0,0,0);
/*!40000 ALTER TABLE `micrositetemplate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mobilepage`
--

DROP TABLE IF EXISTS `mobilepage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mobilepage` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `content` text,
  `name` varchar(200) DEFAULT NULL,
  `url` varchar(800) DEFAULT NULL,
  `isParent` tinyint(4) DEFAULT NULL,
  `parentId` int(11) DEFAULT NULL,
  `isDeleted` tinyint(4) DEFAULT NULL,
  `isHomePage` tinyint(4) DEFAULT NULL,
  `homePageImageCode` text,
  `pagePosition` tinyint(4) DEFAULT NULL,
  `isPrivate` tinyint(4) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `hideWhenLoggedIn` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mobilepage`
--

LOCK TABLES `mobilepage` WRITE;
/*!40000 ALTER TABLE `mobilepage` DISABLE KEYS */;
/*!40000 ALTER TABLE `mobilepage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mobilesitenav`
--

DROP TABLE IF EXISTS `mobilesitenav`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mobilesitenav` (
  `id` bigint(20) NOT NULL DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `siteId` bigint(20) DEFAULT NULL,
  `pageId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mobilesitenav`
--

LOCK TABLES `mobilesitenav` WRITE;
/*!40000 ALTER TABLE `mobilesitenav` DISABLE KEYS */;
/*!40000 ALTER TABLE `mobilesitenav` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mygroup`
--

DROP TABLE IF EXISTS `mygroup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mygroup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `memberId` bigint(20) DEFAULT NULL,
  `name` varchar(500) DEFAULT NULL,
  `type` tinyint(3) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `memberId` (`memberId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mygroup`
--

LOCK TABLES `mygroup` WRITE;
/*!40000 ALTER TABLE `mygroup` DISABLE KEYS */;
/*!40000 ALTER TABLE `mygroup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `newsletter`
--

DROP TABLE IF EXISTS `newsletter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `newsletter` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `memberId` bigint(20) DEFAULT NULL,
  `campaignId` bigint(20) DEFAULT NULL,
  `title` varchar(250) DEFAULT NULL,
  `subject` varchar(250) DEFAULT NULL,
  `body` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `newsletter`
--

LOCK TABLES `newsletter` WRITE;
/*!40000 ALTER TABLE `newsletter` DISABLE KEYS */;
/*!40000 ALTER TABLE `newsletter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `offlinedonation`
--

DROP TABLE IF EXISTS `offlinedonation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `offlinedonation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `keepAnonymous` tinyint(4) DEFAULT NULL,
  `campaignId` bigint(20) DEFAULT NULL,
  `memberId` bigint(20) DEFAULT NULL,
  `value` float DEFAULT NULL,
  `note` varchar(2500) DEFAULT NULL,
  `charityId` bigint(20) DEFAULT NULL,
  `donationOption` tinyint(4) DEFAULT NULL,
  `donationOptionText` varchar(250) DEFAULT NULL,
  `option1` tinyint(4) DEFAULT NULL,
  `option2` tinyint(4) DEFAULT NULL,
  `option3` tinyint(4) DEFAULT NULL,
  `option4` tinyint(4) DEFAULT NULL,
  `offlineName` varchar(55) DEFAULT NULL,
  `taxDeduct` float DEFAULT NULL,
  `masterCampaignId` bigint(20) DEFAULT NULL,
  `refundTime` datetime DEFAULT NULL,
  `zip` varchar(10) DEFAULT NULL,
  `siteId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `campaignId` (`campaignId`),
  KEY `memberId` (`memberId`),
  KEY `charityId` (`charityId`),
  KEY `masterCampaignId` (`masterCampaignId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `offlinedonation`
--

LOCK TABLES `offlinedonation` WRITE;
/*!40000 ALTER TABLE `offlinedonation` DISABLE KEYS */;
/*!40000 ALTER TABLE `offlinedonation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `page`
--

DROP TABLE IF EXISTS `page`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `page` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `content` text,
  `name` varchar(200) DEFAULT NULL,
  `url` varchar(800) DEFAULT NULL,
  `isParent` tinyint(4) DEFAULT NULL,
  `parentId` int(11) DEFAULT NULL,
  `isDeleted` tinyint(4) DEFAULT NULL,
  `isHomePage` tinyint(4) DEFAULT NULL,
  `homePageImageCode` text,
  `pagePosition` tinyint(4) DEFAULT NULL,
  `isPrivate` tinyint(4) NOT NULL DEFAULT '0',
  `type` int(11) NOT NULL DEFAULT '0',
  `hideWhenLoggedIn` tinyint(4) NOT NULL DEFAULT '0',
  `navigationlabel` varchar(200) DEFAULT NULL,
  `rank` int(11) NOT NULL DEFAULT '0',
  `rightSideContent` text,
  `showNavigation` tinyint(4) NOT NULL DEFAULT '0',
  `showInNavigation` tinyint(4) NOT NULL DEFAULT '0',
  `startTime` datetime DEFAULT NULL,
  `endTime` datetime DEFAULT NULL,
  `permalink` varchar(99) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `page`
--

LOCK TABLES `page` WRITE;
/*!40000 ALTER TABLE `page` DISABLE KEYS */;
/*!40000 ALTER TABLE `page` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pagetemplate`
--

DROP TABLE IF EXISTS `pagetemplate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pagetemplate` (
  `id` bigint(20) NOT NULL DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `memberId` bigint(20) DEFAULT NULL,
  `campaignName` varchar(95) DEFAULT NULL,
  `templateName` varchar(95) DEFAULT NULL,
  `description` varchar(2000) DEFAULT NULL,
  `deleteTime` datetime DEFAULT NULL,
  `pageType` tinyint(4) DEFAULT NULL,
  `pageAbstract` varchar(400) DEFAULT NULL,
  `page_image_media_id` bigint(20) DEFAULT NULL,
  `template_image_media_id` bigint(20) DEFAULT NULL,
  `categoryId` bigint(20) DEFAULT NULL,
  `mediaUrl` varchar(500) DEFAULT NULL,
  `mediaId` bigint(20) DEFAULT NULL,
  `tributeHeader` varchar(45) DEFAULT NULL,
  `tributeName` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pagetemplate`
--

LOCK TABLES `pagetemplate` WRITE;
/*!40000 ALTER TABLE `pagetemplate` DISABLE KEYS */;
/*!40000 ALTER TABLE `pagetemplate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `paidpost`
--

DROP TABLE IF EXISTS `paidpost`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `paidpost` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `aboutMemberId` bigint(20) DEFAULT NULL,
  `fromMemberId` bigint(20) DEFAULT NULL,
  `body` varchar(2500) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `value` float DEFAULT NULL,
  `keepAnonymous` tinyint(4) DEFAULT NULL,
  `accurateYes` int(11) DEFAULT NULL,
  `accurateNo` int(11) DEFAULT NULL,
  `accurateTotal` int(11) DEFAULT NULL,
  `reply` varchar(2500) DEFAULT NULL,
  `relationship` varchar(40) DEFAULT NULL,
  `profileTheme` varchar(40) DEFAULT NULL,
  `showDonation` tinyint(4) DEFAULT NULL,
  `charityName` varchar(85) DEFAULT NULL,
  `directedGiftId` int(11) DEFAULT NULL,
  `charityId` bigint(20) DEFAULT NULL,
  `deleteTime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `aboutMemberId` (`aboutMemberId`),
  KEY `fromMemberId` (`fromMemberId`),
  KEY `directedGiftId` (`directedGiftId`),
  CONSTRAINT `paidpost_ibfk_2` FOREIGN KEY (`directedGiftId`) REFERENCES `directedgift` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `paidpost`
--

LOCK TABLES `paidpost` WRITE;
/*!40000 ALTER TABLE `paidpost` DISABLE KEYS */;
/*!40000 ALTER TABLE `paidpost` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `paidpostaccuracy`
--

DROP TABLE IF EXISTS `paidpostaccuracy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `paidpostaccuracy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `postId` int(11) NOT NULL,
  `memberId` bigint(20) DEFAULT NULL,
  `yesVote` tinyint(4) DEFAULT NULL,
  `noVote` tinyint(4) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `postId` (`postId`),
  KEY `memberId` (`memberId`),
  CONSTRAINT `paidpostaccuracy_ibfk_1` FOREIGN KEY (`postId`) REFERENCES `paidpost` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `paidpostaccuracy`
--

LOCK TABLES `paidpostaccuracy` WRITE;
/*!40000 ALTER TABLE `paidpostaccuracy` DISABLE KEYS */;
/*!40000 ALTER TABLE `paidpostaccuracy` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `partner`
--

DROP TABLE IF EXISTS `partner`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `partner` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL,
  `footer` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `partner`
--

LOCK TABLES `partner` WRITE;
/*!40000 ALTER TABLE `partner` DISABLE KEYS */;
/*!40000 ALTER TABLE `partner` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `patchhistory`
--

DROP TABLE IF EXISTS `patchhistory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `patchhistory` (
  `number` int(11) DEFAULT NULL,
  `installed` datetime DEFAULT NULL,
  `notes` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `patchhistory`
--

LOCK TABLES `patchhistory` WRITE;
/*!40000 ALTER TABLE `patchhistory` DISABLE KEYS */;
/*!40000 ALTER TABLE `patchhistory` ENABLE KEYS */;

INSERT INTO `patchhistory` (`number`, `installed`, `notes`) VALUES (652, '2015-08-26 06:46:24', 'Add column in supersiteconfig to enable subscription');

UNLOCK TABLES;

--
-- Table structure for table `pendingtransaction`
--

DROP TABLE IF EXISTS `pendingtransaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pendingtransaction` (
  `id` bigint(20) NOT NULL,
  `memberId` bigint(20) DEFAULT NULL,
  `campaignId` bigint(20) DEFAULT NULL,
  `xmlobject` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pendingtransaction`
--

LOCK TABLES `pendingtransaction` WRITE;
/*!40000 ALTER TABLE `pendingtransaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `pendingtransaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pledgemakerresponse`
--

DROP TABLE IF EXISTS `pledgemakerresponse`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pledgemakerresponse` (
  `id` bigint(20) NOT NULL,
  `created` datetime DEFAULT NULL,
  `pledgeMakerQueueItemId` bigint(20) DEFAULT NULL,
  `pledgeMakerResponse` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pledgemakerresponse`
--

LOCK TABLES `pledgemakerresponse` WRITE;
/*!40000 ALTER TABLE `pledgemakerresponse` DISABLE KEYS */;
/*!40000 ALTER TABLE `pledgemakerresponse` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `poll`
--

DROP TABLE IF EXISTS `poll`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `poll` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `startTime` datetime DEFAULT NULL,
  `endTime` datetime DEFAULT NULL,
  `memberId` bigint(20) DEFAULT NULL,
  `name` varchar(120) DEFAULT NULL,
  `isAnonymous` tinyint(2) DEFAULT NULL,
  `isPrivate` tinyint(2) DEFAULT NULL,
  `audienceType` tinyint(3) DEFAULT NULL,
  `searchLogId` int(11) DEFAULT NULL,
  `groupId` int(11) DEFAULT NULL,
  `isSponsored` tinyint(2) DEFAULT NULL,
  `sponsoredValue` float DEFAULT NULL,
  `sponsoredLimit` int(11) DEFAULT NULL,
  `privateValue` float DEFAULT NULL,
  `charityId` int(11) DEFAULT NULL,
  `COTMemberId` bigint(20) DEFAULT NULL,
  `showDonation` tinyint(4) DEFAULT NULL,
  `subject` varchar(40) DEFAULT NULL,
  `societyId` int(11) DEFAULT NULL,
  `openInvite` tinyint(4) DEFAULT NULL,
  `numberInvites` int(11) DEFAULT NULL,
  `numberResponses` int(11) DEFAULT NULL,
  `charityName` varchar(85) DEFAULT NULL,
  `includeCharges` tinyint(4) NOT NULL DEFAULT '0',
  `description` varchar(2500) DEFAULT NULL,
  `deleteTime` datetime DEFAULT NULL,
  `pollType` tinyint(4) DEFAULT NULL,
  `responseMessage` varchar(1000) DEFAULT NULL,
  `campaignId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `memberId` (`memberId`),
  KEY `campaignId` (`campaignId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `poll`
--

LOCK TABLES `poll` WRITE;
/*!40000 ALTER TABLE `poll` DISABLE KEYS */;
/*!40000 ALTER TABLE `poll` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pollanswer`
--

DROP TABLE IF EXISTS `pollanswer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pollanswer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pollResponseId` int(11) DEFAULT NULL,
  `pollQuestionId` int(11) DEFAULT NULL,
  `multipleChoice` tinyint(2) DEFAULT NULL,
  `writeIn` varchar(2500) DEFAULT NULL,
  `trueFalse` tinyint(2) DEFAULT NULL,
  `pollRange` tinyint(2) DEFAULT NULL,
  `rank` int(11) DEFAULT NULL,
  `checkbox1` tinyint(4) DEFAULT NULL,
  `checkbox2` tinyint(4) DEFAULT NULL,
  `checkbox3` tinyint(4) DEFAULT NULL,
  `checkbox4` tinyint(4) DEFAULT NULL,
  `checkbox5` tinyint(4) DEFAULT NULL,
  `findADateAvailability` varchar(200) DEFAULT NULL,
  `mediaId` bigint(20) DEFAULT '0',
  `country` varchar(200) DEFAULT NULL,
  `state` varchar(200) DEFAULT NULL,
  `selectbox` varchar(200) DEFAULT NULL,
  `checkBox` varchar(2000) DEFAULT NULL,
  `confirmationCheckbox` tinyint(4) DEFAULT NULL,
  `past_date` varchar(11) DEFAULT NULL,
  `future_date` varchar(11) DEFAULT NULL,
  `imageId` bigint(20) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `pollResponseId` (`pollResponseId`),
  KEY `pollQuestionId` (`pollQuestionId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pollanswer`
--

LOCK TABLES `pollanswer` WRITE;
/*!40000 ALTER TABLE `pollanswer` DISABLE KEYS */;
/*!40000 ALTER TABLE `pollanswer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `pollinvitecount`
--

DROP TABLE IF EXISTS `pollinvitecount`;
/*!50001 DROP VIEW IF EXISTS `pollinvitecount`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `pollinvitecount` (
  `id` tinyint NOT NULL,
  `count` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `pollquestion`
--

DROP TABLE IF EXISTS `pollquestion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pollquestion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pollId` int(11) DEFAULT NULL,
  `question` varchar(1000) DEFAULT NULL,
  `type` tinyint(2) DEFAULT NULL,
  `rank` int(11) DEFAULT NULL,
  `otherField` tinyint(2) DEFAULT NULL,
  `answer1` varchar(1000) DEFAULT NULL,
  `answer2` varchar(1000) DEFAULT NULL,
  `answer3` varchar(1000) DEFAULT NULL,
  `answer4` varchar(1000) DEFAULT NULL,
  `answer5` varchar(1000) DEFAULT NULL,
  `findADateStart` date DEFAULT NULL,
  `findADateRangeDays` tinyint(4) DEFAULT NULL,
  `duration` int(11) DEFAULT NULL,
  `findADateInclusionType` tinyint(4) DEFAULT NULL,
  `hour` tinyint(4) DEFAULT NULL,
  `minute` tinyint(4) DEFAULT NULL,
  `ampm` varchar(2) DEFAULT NULL,
  `timeZone` tinyint(4) DEFAULT NULL,
  `findADateNote` varchar(250) DEFAULT NULL,
  `durationScale` tinyint(4) DEFAULT NULL,
  `keepAnonymous` tinyint(4) DEFAULT NULL,
  `answer1Val` int(11) DEFAULT NULL,
  `answer2Val` int(11) DEFAULT NULL,
  `answer3Val` int(11) DEFAULT NULL,
  `answer4Val` int(11) DEFAULT NULL,
  `answer5Val` int(11) DEFAULT NULL,
  `campaignId` bigint(20) DEFAULT NULL,
  `showOnStandardDonation` tinyint(4) DEFAULT '0',
  `isRequired` tinyint(4) DEFAULT '0',
  `countryAnswer` varchar(2000) DEFAULT NULL,
  `stateAnswer` varchar(2000) DEFAULT NULL,
  `selectbox` varchar(2000) DEFAULT NULL,
  `checkBox` varchar(2000) DEFAULT NULL,
  `textblock` varchar(1500) DEFAULT NULL,
  `sdquestion` tinyint(4) DEFAULT NULL,
  `confirmationCheckbox` varchar(5000) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `pollId` (`pollId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pollquestion`
--

LOCK TABLES `pollquestion` WRITE;
/*!40000 ALTER TABLE `pollquestion` DISABLE KEYS */;
/*!40000 ALTER TABLE `pollquestion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pollquestionitem`
--

DROP TABLE IF EXISTS `pollquestionitem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pollquestionitem` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `campaignItemId` bigint(20) DEFAULT NULL,
  `pollQuestionId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pollquestionitem`
--

LOCK TABLES `pollquestionitem` WRITE;
/*!40000 ALTER TABLE `pollquestionitem` DISABLE KEYS */;
/*!40000 ALTER TABLE `pollquestionitem` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pollresponse`
--

DROP TABLE IF EXISTS `pollresponse`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pollresponse` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pollId` int(11) DEFAULT NULL,
  `memberId` bigint(20) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `score` int(11) DEFAULT NULL,
  `value` float DEFAULT NULL,
  `objectType` tinyint(4) DEFAULT NULL,
  `objectId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `pollId` (`pollId`),
  KEY `memberId` (`memberId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pollresponse`
--

LOCK TABLES `pollresponse` WRITE;
/*!40000 ALTER TABLE `pollresponse` DISABLE KEYS */;
/*!40000 ALTER TABLE `pollresponse` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `pollresponsecount`
--

DROP TABLE IF EXISTS `pollresponsecount`;
/*!50001 DROP VIEW IF EXISTS `pollresponsecount`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `pollresponsecount` (
  `id` tinyint NOT NULL,
  `count` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `postnocount`
--

DROP TABLE IF EXISTS `postnocount`;
/*!50001 DROP VIEW IF EXISTS `postnocount`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `postnocount` (
  `id` tinyint NOT NULL,
  `count` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `postyescount`
--

DROP TABLE IF EXISTS `postyescount`;
/*!50001 DROP VIEW IF EXISTS `postyescount`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `postyescount` (
  `id` tinyint NOT NULL,
  `count` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `process_transaction`
--

DROP TABLE IF EXISTS `process_transaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `process_transaction` (
  `id` bigint(20) NOT NULL,
  `clientId` bigint(20) DEFAULT NULL,
  `transactionId` bigint(20) DEFAULT NULL,
  `authcode` varchar(40) DEFAULT NULL,
  `checknumber` varchar(11) DEFAULT NULL,
  `processed` tinyint(4) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `process_transaction`
--

LOCK TABLES `process_transaction` WRITE;
/*!40000 ALTER TABLE `process_transaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `process_transaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `profilelog`
--

DROP TABLE IF EXISTS `profilelog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `profilelog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `memberId` bigint(20) DEFAULT NULL,
  `viewingMemberId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `memberId` (`memberId`),
  KEY `viewingMemberId` (`viewingMemberId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `profilelog`
--

LOCK TABLES `profilelog` WRITE;
/*!40000 ALTER TABLE `profilelog` DISABLE KEYS */;
/*!40000 ALTER TABLE `profilelog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `promocode`
--

DROP TABLE IF EXISTS `promocode`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `promocode` (
  `id` bigint(20) NOT NULL,
  `created` datetime DEFAULT NULL,
  `siteId` bigint(20) DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL,
  `code` varchar(40) DEFAULT NULL,
  `discountValue` float DEFAULT NULL,
  `startDate` datetime DEFAULT NULL,
  `endDate` datetime DEFAULT NULL,
  `type` tinyint(4) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `promocode`
--

LOCK TABLES `promocode` WRITE;
/*!40000 ALTER TABLE `promocode` DISABLE KEYS */;
INSERT INTO `promocode` VALUES (1362146005827,'2015-09-01 12:51:58',1362146005199,'15$ discount','15',15,'2015-09-01 00:00:00','2017-02-09 00:00:00',0,0),(1362146005828,'2015-09-01 12:52:24',1362146005199,'Limited promocode 10','10',10,'2015-09-01 00:00:00','2017-10-17 00:00:00',1,1);
/*!40000 ALTER TABLE `promocode` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prospect`
--

DROP TABLE IF EXISTS `prospect`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prospect` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idCode` varchar(8) DEFAULT NULL,
  `firstName` varchar(40) NOT NULL,
  `lastName` varchar(40) NOT NULL,
  `email` varchar(50) NOT NULL,
  `created` datetime DEFAULT NULL,
  `emailVerified` datetime DEFAULT NULL,
  `inviterId` int(11) DEFAULT NULL,
  `message` varchar(700) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  KEY `inviterId` (`inviterId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prospect`
--

LOCK TABLES `prospect` WRITE;
/*!40000 ALTER TABLE `prospect` DISABLE KEYS */;
/*!40000 ALTER TABLE `prospect` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `purchaseoption`
--

DROP TABLE IF EXISTS `purchaseoption`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `purchaseoption` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `campaignItemId` bigint(20) DEFAULT NULL,
  `childItemId` bigint(20) DEFAULT NULL,
  `itemQuantity` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `purchaseoption`
--

LOCK TABLES `purchaseoption` WRITE;
/*!40000 ALTER TABLE `purchaseoption` DISABLE KEYS */;
INSERT INTO `purchaseoption` VALUES (1,'2015-09-01 12:51:14',0,1362146005821,1),(2,'2015-09-01 12:51:14',0,1362146005820,1),(3,'2015-09-01 12:51:14',0,1362146005819,1),(4,'2015-09-01 12:51:27',0,1362146005821,2),(5,'2015-09-01 12:51:27',0,1362146005820,2),(6,'2015-09-01 12:51:27',0,1362146005819,2);
/*!40000 ALTER TABLE `purchaseoption` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `queue`
--

DROP TABLE IF EXISTS `queue`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `queue` (
  `id` bigint(20) unsigned NOT NULL,
  `created` datetime DEFAULT NULL,
  `objectId` bigint(20) unsigned NOT NULL,
  `objectType` varchar(50) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `deleted` datetime DEFAULT NULL,
  `processed` datetime DEFAULT NULL,
  `status` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `queue`
--

LOCK TABLES `queue` WRITE;
/*!40000 ALTER TABLE `queue` DISABLE KEYS */;
/*!40000 ALTER TABLE `queue` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `queuestore`
--

DROP TABLE IF EXISTS `queuestore`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `queuestore` (
  `id` bigint(20) unsigned NOT NULL,
  `created` datetime DEFAULT NULL,
  `name` varchar(40) DEFAULT NULL,
  `value` varchar(2000) DEFAULT NULL,
  `queueId` bigint(20) unsigned NOT NULL,
  `deleted` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `queuestore`
--

LOCK TABLES `queuestore` WRITE;
/*!40000 ALTER TABLE `queuestore` DISABLE KEYS */;
/*!40000 ALTER TABLE `queuestore` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ranking`
--

DROP TABLE IF EXISTS `ranking`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ranking` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `rank` varchar(200) DEFAULT NULL,
  `points` int(11) DEFAULT NULL,
  `description` text,
  `objectId` int(11) DEFAULT NULL,
  `objectType` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ranking`
--

LOCK TABLES `ranking` WRITE;
/*!40000 ALTER TABLE `ranking` DISABLE KEYS */;
/*!40000 ALTER TABLE `ranking` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(40) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (1,'SelfServiceAdmin','This allow user to work with self service');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rssregistry`
--

DROP TABLE IF EXISTS `rssregistry`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rssregistry` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) DEFAULT NULL,
  `url` varchar(512) DEFAULT NULL,
  `groupid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rssregistry`
--

LOCK TABLES `rssregistry` WRITE;
/*!40000 ALTER TABLE `rssregistry` DISABLE KEYS */;
/*!40000 ALTER TABLE `rssregistry` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `searchlog`
--

DROP TABLE IF EXISTS `searchlog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `searchlog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `memberId` bigint(20) DEFAULT NULL,
  `firstName` varchar(40) DEFAULT NULL,
  `lastName` varchar(40) DEFAULT NULL,
  `middleName` varchar(40) DEFAULT NULL,
  `gender` tinyint(2) DEFAULT NULL,
  `nickname` varchar(40) DEFAULT NULL,
  `maidenName` varchar(40) DEFAULT NULL,
  `city` varchar(30) DEFAULT NULL,
  `state` varchar(20) DEFAULT NULL,
  `zip` varchar(10) DEFAULT NULL,
  `country` int(11) DEFAULT NULL,
  `age1` tinyint(4) DEFAULT NULL,
  `age2` tinyint(4) DEFAULT NULL,
  `language` tinyint(2) DEFAULT NULL,
  `maritalStatus` tinyint(2) DEFAULT NULL,
  `skills` varchar(100) DEFAULT NULL,
  `interests` varchar(100) DEFAULT NULL,
  `awards` varchar(100) DEFAULT NULL,
  `highSchool` varchar(100) DEFAULT NULL,
  `highSchoolYear1` year(4) DEFAULT NULL,
  `highSchoolYear2` year(4) DEFAULT NULL,
  `highSchoolCity` varchar(25) DEFAULT NULL,
  `highSchoolState` varchar(20) DEFAULT NULL,
  `highSchoolCountry` int(11) DEFAULT NULL,
  `college` varchar(100) DEFAULT NULL,
  `collegeMajor` varchar(40) DEFAULT NULL,
  `collegeDegree` tinyint(4) DEFAULT NULL,
  `collegeYear1` year(4) DEFAULT NULL,
  `collegeYear2` year(4) DEFAULT NULL,
  `collegeCity` varchar(25) DEFAULT NULL,
  `collegeState` varchar(20) DEFAULT NULL,
  `collegeCountry` int(11) DEFAULT NULL,
  `gradSchool` varchar(100) DEFAULT NULL,
  `gradSchoolMajor` varchar(40) DEFAULT NULL,
  `gradSchoolDegree` tinyint(4) DEFAULT NULL,
  `gradSchoolYear1` year(4) DEFAULT NULL,
  `gradSchoolYear2` year(4) DEFAULT NULL,
  `gradSchoolCity` varchar(25) DEFAULT NULL,
  `gradSchoolState` varchar(20) DEFAULT NULL,
  `gradSchoolCountry` int(11) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `companyRole` varchar(40) DEFAULT NULL,
  `companyYear1` year(4) DEFAULT NULL,
  `companyYear2` year(4) DEFAULT NULL,
  `companyCity` varchar(25) DEFAULT NULL,
  `companyState` varchar(20) DEFAULT NULL,
  `companyCountry` int(11) DEFAULT NULL,
  `results` int(11) DEFAULT NULL,
  `name` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `memberId` (`memberId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `searchlog`
--

LOCK TABLES `searchlog` WRITE;
/*!40000 ALTER TABLE `searchlog` DISABLE KEYS */;
/*!40000 ALTER TABLE `searchlog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `searchterm`
--

DROP TABLE IF EXISTS `searchterm`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `searchterm` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `memberId` bigint(20) DEFAULT NULL,
  `term` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `memberId` (`memberId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `searchterm`
--

LOCK TABLES `searchterm` WRITE;
/*!40000 ALTER TABLE `searchterm` DISABLE KEYS */;
/*!40000 ALTER TABLE `searchterm` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `selfdiscount`
--

DROP TABLE IF EXISTS `selfdiscount`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `selfdiscount` (
  `id` bigint(20) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` varchar(200) DEFAULT NULL,
  `internaldescription` varchar(200) DEFAULT NULL,
  `type` tinyint(4) DEFAULT NULL,
  `data` int(11) DEFAULT NULL,
  `extendeddata` int(11) DEFAULT NULL,
  `durationofbenefits` int(11) DEFAULT NULL,
  `systemredemption` int(11) DEFAULT NULL,
  `accountredemption` int(11) DEFAULT NULL,
  `durationtype` tinyint(4) DEFAULT NULL,
  `discounttype` tinyint(4) DEFAULT NULL,
  `systemRedtype` tinyint(4) DEFAULT NULL,
  `accountRedtype` tinyint(4) DEFAULT NULL,
  `active` tinyint(4) DEFAULT NULL,
  `code` varchar(200) DEFAULT NULL,
  `created` datetime NOT NULL,
  `updated` datetime DEFAULT NULL,
  `deleted` datetime DEFAULT NULL,
  `validfrom` datetime DEFAULT NULL,
  `expires` datetime DEFAULT NULL,
  `packageId` bigint(20) DEFAULT NULL,
  `bundleCode` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `selfdiscount`
--

LOCK TABLES `selfdiscount` WRITE;
/*!40000 ALTER TABLE `selfdiscount` DISABLE KEYS */;
/*!40000 ALTER TABLE `selfdiscount` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `selfinvoice`
--

DROP TABLE IF EXISTS `selfinvoice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `selfinvoice` (
  `id` bigint(20) NOT NULL,
  `stripeinvoiceid` varchar(50) NOT NULL,
  `livemode` tinyint(4) DEFAULT NULL,
  `subtotal` int(11) DEFAULT NULL,
  `total` int(11) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  `customerid` varchar(50) DEFAULT NULL,
  `memberid` bigint(20) DEFAULT NULL,
  `attempted` tinyint(4) DEFAULT NULL,
  `paid` tinyint(4) DEFAULT NULL,
  `amount_due` int(11) DEFAULT NULL,
  `discount` int(11) DEFAULT NULL,
  `application_fee` int(11) DEFAULT NULL,
  `startdate` datetime NOT NULL,
  `enddate` datetime DEFAULT NULL,
  `nextattempt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `selfinvoice`
--

LOCK TABLES `selfinvoice` WRITE;
/*!40000 ALTER TABLE `selfinvoice` DISABLE KEYS */;
/*!40000 ALTER TABLE `selfinvoice` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `selfinvoiceitem`
--

DROP TABLE IF EXISTS `selfinvoiceitem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `selfinvoiceitem` (
  `id` bigint(20) NOT NULL,
  `invoiceid` bigint(20) NOT NULL,
  `stripeinvoiceitemid` varchar(50) NOT NULL,
  `type` varchar(50) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `currency` varchar(50) DEFAULT NULL,
  `livemode` tinyint(4) DEFAULT NULL,
  `startdate` datetime NOT NULL,
  `enddate` datetime DEFAULT NULL,
  `quantity` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `selfinvoiceitem`
--

LOCK TABLES `selfinvoiceitem` WRITE;
/*!40000 ALTER TABLE `selfinvoiceitem` DISABLE KEYS */;
/*!40000 ALTER TABLE `selfinvoiceitem` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `selfmemberstripecustomer`
--

DROP TABLE IF EXISTS `selfmemberstripecustomer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `selfmemberstripecustomer` (
  `id` bigint(20) NOT NULL,
  `memberId` bigint(20) NOT NULL,
  `customerId` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `selfmemberstripecustomer`
--

LOCK TABLES `selfmemberstripecustomer` WRITE;
/*!40000 ALTER TABLE `selfmemberstripecustomer` DISABLE KEYS */;
/*!40000 ALTER TABLE `selfmemberstripecustomer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `selforder`
--

DROP TABLE IF EXISTS `selforder`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `selforder` (
  `id` bigint(20) NOT NULL,
  `memberId` bigint(20) NOT NULL,
  `customerId` varchar(50) DEFAULT NULL,
  `description` varchar(100) DEFAULT NULL,
  `type` tinyint(4) DEFAULT NULL,
  `amount` float DEFAULT NULL,
  `subcriptionId` bigint(20) DEFAULT NULL,
  `siteId` bigint(20) DEFAULT NULL,
  `authcode` varchar(30) DEFAULT NULL,
  `note` varchar(100) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `selforder`
--

LOCK TABLES `selforder` WRITE;
/*!40000 ALTER TABLE `selforder` DISABLE KEYS */;
/*!40000 ALTER TABLE `selforder` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `selforderitem`
--

DROP TABLE IF EXISTS `selforderitem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `selforderitem` (
  `id` bigint(20) NOT NULL,
  `transactionId` bigint(20) NOT NULL,
  `memberId` bigint(20) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` varchar(200) DEFAULT NULL,
  `itemid` bigint(20) DEFAULT NULL,
  `itemtype` tinyint(4) DEFAULT NULL,
  `discountId` bigint(20) DEFAULT NULL,
  `amount` float DEFAULT NULL,
  `recurringamount` float DEFAULT NULL,
  `discount` float DEFAULT NULL,
  `content` varchar(100) DEFAULT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `selforderitem`
--

LOCK TABLES `selforderitem` WRITE;
/*!40000 ALTER TABLE `selforderitem` DISABLE KEYS */;
/*!40000 ALTER TABLE `selforderitem` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `selfpackage`
--

DROP TABLE IF EXISTS `selfpackage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `selfpackage` (
  `id` bigint(20) NOT NULL,
  `name` varchar(200) NOT NULL,
  `description` varchar(200) DEFAULT NULL,
  `setupcost` float DEFAULT NULL,
  `recurringcost` float DEFAULT NULL,
  `active` tinyint(4) DEFAULT NULL,
  `enabledDefault` tinyint(4) DEFAULT NULL,
  `created` datetime NOT NULL,
  `updated` datetime DEFAULT NULL,
  `deleted` datetime DEFAULT NULL,
  `rankorder` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `selfpackage`
--

LOCK TABLES `selfpackage` WRITE;
/*!40000 ALTER TABLE `selfpackage` DISABLE KEYS */;
/*!40000 ALTER TABLE `selfpackage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `selfpackagediscount`
--

DROP TABLE IF EXISTS `selfpackagediscount`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `selfpackagediscount` (
  `id` bigint(20) NOT NULL,
  `packageId` bigint(20) NOT NULL,
  `discountId` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `selfpackagediscount`
--

LOCK TABLES `selfpackagediscount` WRITE;
/*!40000 ALTER TABLE `selfpackagediscount` DISABLE KEYS */;
/*!40000 ALTER TABLE `selfpackagediscount` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `selfsubscriptionsite`
--

DROP TABLE IF EXISTS `selfsubscriptionsite`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `selfsubscriptionsite` (
  `id` bigint(20) NOT NULL,
  `subscriptionId` bigint(20) NOT NULL,
  `siteId` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `selfsubscriptionsite`
--

LOCK TABLES `selfsubscriptionsite` WRITE;
/*!40000 ALTER TABLE `selfsubscriptionsite` DISABLE KEYS */;
/*!40000 ALTER TABLE `selfsubscriptionsite` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `selfuserdiscount`
--

DROP TABLE IF EXISTS `selfuserdiscount`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `selfuserdiscount` (
  `id` bigint(20) NOT NULL,
  `memberId` bigint(20) NOT NULL,
  `packageId` bigint(20) NOT NULL,
  `discountId` bigint(20) DEFAULT NULL,
  `name` varchar(100) NOT NULL,
  `description` varchar(200) DEFAULT NULL,
  `data` int(11) DEFAULT NULL,
  `extendeddata` int(11) DEFAULT NULL,
  `type` tinyint(4) DEFAULT NULL,
  `percent` int(11) DEFAULT NULL,
  `code` varchar(200) DEFAULT NULL,
  `durationofbenefits` int(11) DEFAULT NULL,
  `created` datetime NOT NULL,
  `updated` datetime DEFAULT NULL,
  `deleted` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `selfuserdiscount`
--

LOCK TABLES `selfuserdiscount` WRITE;
/*!40000 ALTER TABLE `selfuserdiscount` DISABLE KEYS */;
/*!40000 ALTER TABLE `selfuserdiscount` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `selfuserpackage`
--

DROP TABLE IF EXISTS `selfuserpackage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `selfuserpackage` (
  `id` bigint(20) NOT NULL,
  `memberId` bigint(20) NOT NULL,
  `packageId` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `selfuserpackage`
--

LOCK TABLES `selfuserpackage` WRITE;
/*!40000 ALTER TABLE `selfuserpackage` DISABLE KEYS */;
/*!40000 ALTER TABLE `selfuserpackage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `serializeobject`
--

DROP TABLE IF EXISTS `serializeobject`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `serializeobject` (
  `id` bigint(20) NOT NULL,
  `object` text,
  `sessionid` varchar(50) DEFAULT NULL,
  `datestored` datetime DEFAULT NULL,
  `memberId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `serializeobject`
--

LOCK TABLES `serializeobject` WRITE;
/*!40000 ALTER TABLE `serializeobject` DISABLE KEYS */;
/*!40000 ALTER TABLE `serializeobject` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shippingaddress`
--

DROP TABLE IF EXISTS `shippingaddress`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shippingaddress` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `address` varchar(250) DEFAULT NULL,
  `city` varchar(45) DEFAULT NULL,
  `state` varchar(45) DEFAULT NULL,
  `country` int(11) DEFAULT NULL,
  `zip` varchar(45) DEFAULT NULL,
  `countrycode` char(3) DEFAULT NULL,
  `auctionitembidid` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shippingaddress`
--

LOCK TABLES `shippingaddress` WRITE;
/*!40000 ALTER TABLE `shippingaddress` DISABLE KEYS */;
/*!40000 ALTER TABLE `shippingaddress` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shorturl`
--

DROP TABLE IF EXISTS `shorturl`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shorturl` (
  `id` bigint(20) unsigned NOT NULL,
  `url` text,
  `shorturl` text,
  `service` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shorturl`
--

LOCK TABLES `shorturl` WRITE;
/*!40000 ALTER TABLE `shorturl` DISABLE KEYS */;
INSERT INTO `shorturl` VALUES (1362146005760,'https://firstevent.karma.com','https://goo.gl/i3Rr2f',0),(1362146005761,'https://event.karma.com','https://goo.gl/C7Rfyq',0),(1362146005762,'https://anotheroneevenet.karma.com','https://goo.gl/KLSSY3',0),(1362146005829,'https://uniqueevent.karma.com','https://goo.gl/Im6cG0',0);
/*!40000 ALTER TABLE `shorturl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `signupnewsletter`
--

DROP TABLE IF EXISTS `signupnewsletter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `signupnewsletter` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `signupdate` datetime DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `signupnewsletter`
--

LOCK TABLES `signupnewsletter` WRITE;
/*!40000 ALTER TABLE `signupnewsletter` DISABLE KEYS */;
/*!40000 ALTER TABLE `signupnewsletter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `site`
--

DROP TABLE IF EXISTS `site`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `site` (
  `id` bigint(20) NOT NULL DEFAULT '0',
  `idCode` varchar(8) NOT NULL,
  `created` datetime DEFAULT NULL,
  `siteType` int(11) DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL,
  `url` varchar(800) DEFAULT NULL,
  `mastheadCode` varchar(700) DEFAULT NULL,
  `masterCampaignId` bigint(20) DEFAULT NULL,
  `adminId` bigint(20) DEFAULT NULL,
  `isDeleted` tinyint(4) DEFAULT NULL,
  `templateType` tinyint(4) DEFAULT NULL,
  `waiverText` varchar(4000) DEFAULT NULL,
  `deleteTime` datetime DEFAULT NULL,
  `mobileMastheadCode` varchar(700) DEFAULT NULL,
  `dateOfEvent` date DEFAULT NULL,
  `location` varchar(800) DEFAULT NULL,
  `sitebannerblobid` bigint(10) DEFAULT NULL,
  `address_1` varchar(200) DEFAULT NULL,
  `address_2` varchar(200) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `state` varchar(25) DEFAULT NULL,
  `zip` varchar(10) DEFAULT NULL,
  `contact_phone` varchar(50) DEFAULT NULL,
  `dashbaord_widgets_list` varchar(200) DEFAULT NULL,
  `subdomain` varchar(50) DEFAULT NULL,
  `thermometerstyle` varchar(500) DEFAULT NULL,
  `alertEmailAddresses` varchar(1000) DEFAULT NULL,
  `themeId` bigint(20) DEFAULT NULL,
  `isTemplate` tinyint(2) DEFAULT '0',
  `overrideOrder` int(10) DEFAULT '0',
  `event_time` datetime DEFAULT NULL,
  `settings` varchar(30) DEFAULT NULL,
  `mediaId` bigint(20) DEFAULT '0',
  `presetdonations` varchar(200) DEFAULT NULL,
  `recurringoptions` varchar(45) DEFAULT NULL,
  `folder` varchar(100) DEFAULT NULL,
  `includeJUSTDonationsOrALLTransactions` tinyint(4) DEFAULT NULL,
  `showDonorsListedAsAnonymous` tinyint(4) DEFAULT NULL,
  `showDollarAmountsNextToNames` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idCode` (`idCode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `site`
--

LOCK TABLES `site` WRITE;
/*!40000 ALTER TABLE `site` DISABLE KEYS */;
INSERT INTO `site` VALUES (1362145798075,'S505VHO1','2015-08-31 17:56:35',0,'One more test',NULL,'<img src=\'/images/crowdster/theme/supersite/supersite-default-banner.jpg\' style=\'width: 992px; height: 156px;\' />',1362145798074,1362146005137,0,0,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'One-more-test',NULL,NULL,0,3,0,NULL,'10000000',0,NULL,NULL,NULL,NULL,NULL,NULL),(1362146005143,'7KL3U8RB','2015-08-31 07:23:04',0,'FirstSupersite',NULL,'<img src=\'/images/crowdster/theme/supersite/supersite-default-banner.jpg\' style=\'width: 992px; height: 156px;\' />',1362146005142,1362146005137,0,0,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'FirstSupersite',NULL,NULL,0,3,0,NULL,'10000000',0,NULL,NULL,NULL,NULL,NULL,NULL),(1362146005152,'5NBTNIEB','2015-08-31 07:23:21',0,'SecondSupersite',NULL,'<img src=\'/images/crowdster/theme/supersite/supersite-default-banner.jpg\' style=\'width: 992px; height: 156px;\' />',1362146005151,1362146005137,0,0,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'SecondSupersite',NULL,NULL,0,3,0,NULL,'10000000',0,NULL,NULL,NULL,NULL,NULL,NULL),(1362146005161,'NHLJVRXF','2015-08-31 07:23:41',0,'ThirdSupersite',NULL,'<img src=\'/images/crowdster/theme/supersite/supersite-default-banner.jpg\' style=\'width: 992px; height: 156px;\' />',1362146005160,1362146005137,0,0,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'ThirdSupersite',NULL,NULL,0,3,0,NULL,'10000000',0,NULL,NULL,NULL,NULL,NULL,NULL),(1362146005170,'36TBK55M','2015-08-31 07:24:08',0,'TestSupersite',NULL,'<img src=\'/images/crowdster/theme/supersite/supersite-default-banner.jpg\' style=\'width: 992px; height: 156px;\' />',1362146005169,1362146005137,0,0,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'TestSupersite',NULL,NULL,0,3,0,NULL,'10000000',0,NULL,NULL,NULL,NULL,NULL,NULL),(1362146005179,'5FNVJPIV','2015-08-31 07:27:42',0,'SoftwareSS',NULL,'<img src=\'/images/crowdster/theme/supersite/supersite-default-banner.jpg\' style=\'width: 992px; height: 156px;\' />',1362146005178,1362146005137,0,0,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'SoftwareSS',NULL,NULL,0,3,0,NULL,'10000000',0,NULL,NULL,NULL,NULL,NULL,NULL),(1362146005188,'CJ254SU5','2015-08-31 07:29:55',0,'TestSsName',NULL,'<img src=\'/images/crowdster/theme/supersite/supersite-default-banner.jpg\' style=\'width: 992px; height: 156px;\' />',1362146005187,1362146005137,0,0,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'TestSsName',NULL,NULL,0,3,0,NULL,'10000000',0,NULL,NULL,NULL,NULL,NULL,NULL),(1362146005199,'7N03E4NI','2015-08-31 07:37:38',0,'FirstEvent',NULL,NULL,1362146005198,1362146005137,0,0,NULL,NULL,NULL,'2015-09-15','',0,'','','','','','',NULL,'firstevent',NULL,'gilyashov.alexandr@gmail.com',0,0,0,NULL,'10000101',0,NULL,NULL,NULL,NULL,NULL,NULL),(1362146005206,'BGNQ98RG','2015-08-31 07:38:23',0,'AnotherOneEvenet',NULL,NULL,1362146005205,1362146005137,0,0,NULL,NULL,NULL,'2015-08-31','',0,'','','','','','',NULL,'anotheroneevenet',NULL,'gilyashov.alexandr@gmail.com',0,0,0,NULL,'10000001',0,NULL,NULL,NULL,NULL,NULL,NULL),(1362146005213,'VHCA3ZJ7','2015-08-31 07:44:23',0,'Event',NULL,NULL,1362146005212,1362146005137,0,0,NULL,NULL,NULL,'2015-08-30','',0,'','','','','','',NULL,'event',NULL,'gilyashov.alexandr@gmail.com',0,0,0,NULL,'10000001',0,NULL,NULL,NULL,NULL,NULL,NULL),(1362146005220,'6O43Z9X0','2015-08-31 07:47:15',0,'Unique Event',NULL,NULL,1362146005219,1362146005137,0,0,NULL,NULL,NULL,'2015-08-31','',0,'','','','','','',NULL,'uniqueevent',NULL,'gilyashov.alexandr@gmail.com',0,0,0,NULL,'10000001',0,NULL,NULL,NULL,NULL,NULL,NULL),(1362146005227,'DKWQDVGQ','2015-08-31 07:53:07',0,'Anna Event',NULL,NULL,1362146005226,1362146005137,0,0,NULL,NULL,NULL,'2015-08-29',NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'annaevent',NULL,'gilyashov.alexandr@gmail.com',0,0,0,NULL,'10000000',0,NULL,NULL,NULL,NULL,NULL,NULL),(1362146005234,'TXELRE9P','2015-08-31 07:53:46',0,'Qwe Event',NULL,NULL,1362146005233,1362146005137,0,0,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'qweevent',NULL,'gilyashov.alexandr@gmail.com',0,0,0,NULL,'10000000',0,NULL,NULL,NULL,NULL,NULL,NULL),(1362146005241,'6A98ON90','2015-08-31 07:54:48',0,'EventMicro',NULL,NULL,1362146005240,1362146005137,0,0,NULL,NULL,NULL,'2015-08-22',NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'event',NULL,'gilyashov.alexandr@gmail.com',0,0,0,NULL,'10000000',0,NULL,NULL,NULL,NULL,NULL,NULL),(1362146005248,'X9A3RJZ9','2015-08-31 07:56:37',0,'FirstOne',NULL,NULL,1362146005247,1362146005137,0,0,NULL,NULL,NULL,'2015-08-27',NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'firstone',NULL,'gilyashov.alexandr@gmail.com',0,0,0,NULL,'10000000',0,NULL,NULL,NULL,NULL,NULL,NULL),(1362146005255,'0138Z2VF','2015-08-31 07:57:25',0,'SecondOne',NULL,NULL,1362146005254,1362146005137,0,0,NULL,NULL,NULL,'2015-08-22',NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'secondone',NULL,'gilyashov.alexandr@gmail.com',0,0,0,NULL,'10000000',0,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `site` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `siteadmin`
--

DROP TABLE IF EXISTS `siteadmin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `siteadmin` (
  `id` bigint(20) unsigned NOT NULL,
  `created` datetime DEFAULT NULL,
  `memberId` bigint(20) DEFAULT NULL,
  `siteId` bigint(20) DEFAULT NULL,
  `adminType` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `siteadmin`
--

LOCK TABLES `siteadmin` WRITE;
/*!40000 ALTER TABLE `siteadmin` DISABLE KEYS */;
/*!40000 ALTER TABLE `siteadmin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `siteblob`
--

DROP TABLE IF EXISTS `siteblob`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `siteblob` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `siteId` bigint(20) DEFAULT NULL,
  `siterotatorimages` varchar(100) DEFAULT NULL,
  `blobid` bigint(20) DEFAULT NULL,
  `thumbnailId` bigint(20) DEFAULT NULL,
  `blobMediaId` bigint(20) DEFAULT NULL,
  `thumbnailMediaId` bigint(20) DEFAULT NULL,
  `siterotatormedia` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `siteblob`
--

LOCK TABLES `siteblob` WRITE;
/*!40000 ALTER TABLE `siteblob` DISABLE KEYS */;
/*!40000 ALTER TABLE `siteblob` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sitecampaignitem`
--

DROP TABLE IF EXISTS `sitecampaignitem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sitecampaignitem` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `siteId` bigint(20) DEFAULT NULL,
  `value` float DEFAULT NULL,
  `taxDeductRate` float DEFAULT NULL,
  `name` varchar(500) DEFAULT NULL,
  `description` varchar(1000) DEFAULT NULL,
  `hasQuantity` tinyint(4) DEFAULT NULL,
  `deleteTime` datetime DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sitecampaignitem`
--

LOCK TABLES `sitecampaignitem` WRITE;
/*!40000 ALTER TABLE `sitecampaignitem` DISABLE KEYS */;
/*!40000 ALTER TABLE `sitecampaignitem` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `siteconfig`
--

DROP TABLE IF EXISTS `siteconfig`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `siteconfig` (
  `id` bigint(20) NOT NULL,
  `created` datetime DEFAULT NULL,
  `siteId` bigint(20) DEFAULT NULL,
  `donationUrl` varchar(500) DEFAULT NULL,
  `registrationText` varchar(150) DEFAULT NULL,
  `userHomePageText` varchar(1000) DEFAULT NULL,
  `joinText` varchar(50) DEFAULT NULL,
  `fromEmail` varchar(200) DEFAULT NULL,
  `partnerId` int(11) NOT NULL DEFAULT '0',
  `loginId` varchar(100) DEFAULT NULL,
  `transactionKey` varchar(100) DEFAULT NULL,
  `processorType` int(11) DEFAULT NULL,
  `registrationEmailText` varchar(1500) DEFAULT NULL,
  `participationText` varchar(200) DEFAULT NULL,
  `awarenessGoal` int(11) DEFAULT NULL,
  `financialGoal` float DEFAULT NULL,
  `teamLeaderText` varchar(200) DEFAULT NULL,
  `singleLeaderText` varchar(200) DEFAULT NULL,
  `teamJoinText` varchar(200) DEFAULT NULL,
  `teamContentCampaignId` int(11) DEFAULT NULL,
  `showTeamLink` tinyint(4) NOT NULL DEFAULT '1',
  `footer` varchar(1500) DEFAULT NULL,
  `showResponseOnTeamMembers` varchar(200) DEFAULT NULL,
  `hideViewTeamPage` int(11) DEFAULT NULL,
  `salesForceLogin` varchar(100) DEFAULT NULL,
  `salesForcePassword` varchar(100) DEFAULT NULL,
  `backgroundcolor` varchar(50) DEFAULT NULL,
  `imageproperty` varchar(15) DEFAULT NULL,
  `donationtext` varchar(150) DEFAULT NULL,
  `sponsortext` varchar(150) DEFAULT NULL,
  `homepage_description` text,
  `fundraising_widget_off_on` tinyint(4) DEFAULT NULL,
  `leaderboard_widget_off_on` tinyint(4) DEFAULT NULL,
  `map_widget_off_on` tinyint(4) DEFAULT NULL,
  `widget_ordered_list` varchar(200) DEFAULT NULL,
  `comments_widget_off_on` tinyint(4) DEFAULT NULL,
  `emailerframecode` varchar(250) DEFAULT NULL,
  `campaignpagestylesheet` varchar(1000) DEFAULT NULL,
  `receiptEmailText` varchar(2000) DEFAULT NULL,
  `inviteDefaultText` varchar(1500) DEFAULT NULL,
  `thankYouDefaultText` varchar(1500) DEFAULT NULL,
  `thankYouWebsiteMessage` varchar(1000) DEFAULT NULL,
  `createpagetext` varchar(150) DEFAULT NULL,
  `addcreatepagewidget` tinyint(4) DEFAULT NULL,
  `showStyle` tinyint(4) NOT NULL DEFAULT '0',
  `override` varchar(30) DEFAULT NULL,
  `freeformwidgetoffon` tinyint(4) DEFAULT '1',
  `freeformwidget2offon` tinyint(4) DEFAULT '1',
  `signupPageText` varchar(3000) DEFAULT NULL,
  `allowAuction` tinyint(4) NOT NULL DEFAULT '0',
  `auctiontext` varchar(150) DEFAULT NULL,
  `apiSignature` varchar(100) DEFAULT NULL,
  `openmovesConfig` varchar(150) DEFAULT NULL,
  `campaign_widget_on_off` tinyint(4) DEFAULT NULL,
  `applicationform_id` bigint(20) DEFAULT NULL,
  `heroContent` varchar(1500) DEFAULT NULL,
  `head_tags` varchar(2000) DEFAULT NULL,
  `supporter_wid_off_on` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `siteconfig_index_siteId` (`siteId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `siteconfig`
--

LOCK TABLES `siteconfig` WRITE;
/*!40000 ALTER TABLE `siteconfig` DISABLE KEYS */;
INSERT INTO `siteconfig` VALUES (1362145798079,'2015-08-31 17:56:35',1362145798075,NULL,'Registration','',NULL,NULL,0,'pk_test_PNEuDR4lODkImtjqCXotVXvJ','sk_test_LyB9Y7xZOhOzjKBLpGLaggs7',4,NULL,NULL,0,0,NULL,NULL,NULL,0,1,NULL,NULL,0,NULL,NULL,'#b6e8f9',NULL,NULL,NULL,NULL,1,1,1,NULL,1,NULL,NULL,'Thank you for your contribution',NULL,NULL,NULL,NULL,1,0,'1011100010011111111',1,1,NULL,0,NULL,NULL,NULL,1,0,NULL,NULL,NULL),(1362146005147,'2015-08-31 07:23:04',1362146005143,NULL,'Registration','',NULL,NULL,0,'pk_test_PNEuDR4lODkImtjqCXotVXvJ','sk_test_LyB9Y7xZOhOzjKBLpGLaggs7',4,NULL,NULL,0,0,NULL,NULL,NULL,0,1,NULL,NULL,0,NULL,NULL,'#b6e8f9',NULL,NULL,NULL,NULL,1,1,1,NULL,1,NULL,NULL,'Thank you for your contribution',NULL,NULL,NULL,NULL,1,0,'1011100010011111111',1,1,NULL,0,NULL,NULL,NULL,1,0,NULL,NULL,NULL),(1362146005156,'2015-08-31 07:23:22',1362146005152,NULL,'Registration','',NULL,NULL,0,'pk_test_PNEuDR4lODkImtjqCXotVXvJ','sk_test_LyB9Y7xZOhOzjKBLpGLaggs7',4,NULL,NULL,0,0,NULL,NULL,NULL,0,1,NULL,NULL,0,NULL,NULL,'#b6e8f9',NULL,NULL,NULL,NULL,1,1,1,NULL,1,NULL,NULL,'Thank you for your contribution',NULL,NULL,NULL,NULL,1,0,'1011100010011111111',1,1,NULL,0,NULL,NULL,NULL,1,0,NULL,NULL,NULL),(1362146005165,'2015-08-31 07:23:42',1362146005161,NULL,'Registration','',NULL,NULL,0,'pk_test_PNEuDR4lODkImtjqCXotVXvJ','sk_test_LyB9Y7xZOhOzjKBLpGLaggs7',4,NULL,NULL,0,0,NULL,NULL,NULL,0,1,NULL,NULL,0,NULL,NULL,'#b6e8f9',NULL,NULL,NULL,NULL,1,1,1,NULL,1,NULL,NULL,'Thank you for your contribution',NULL,NULL,NULL,NULL,1,0,'1011100010011111111',1,1,NULL,0,NULL,NULL,NULL,1,0,NULL,NULL,NULL),(1362146005174,'2015-08-31 07:24:08',1362146005170,NULL,'Registration','',NULL,NULL,0,'pk_test_PNEuDR4lODkImtjqCXotVXvJ','sk_test_LyB9Y7xZOhOzjKBLpGLaggs7',4,NULL,NULL,0,0,NULL,NULL,NULL,0,1,NULL,NULL,0,NULL,NULL,'#b6e8f9',NULL,NULL,NULL,NULL,1,1,1,NULL,1,NULL,NULL,'Thank you for your contribution',NULL,NULL,NULL,NULL,1,0,'1011100010011111111',1,1,NULL,0,NULL,NULL,NULL,1,0,NULL,NULL,NULL),(1362146005183,'2015-08-31 07:27:42',1362146005179,NULL,'Registration','',NULL,NULL,0,'pk_test_PNEuDR4lODkImtjqCXotVXvJ','sk_test_LyB9Y7xZOhOzjKBLpGLaggs7',4,NULL,NULL,0,0,NULL,NULL,NULL,0,1,NULL,NULL,0,NULL,NULL,'#b6e8f9',NULL,NULL,NULL,NULL,1,1,1,NULL,1,NULL,NULL,'Thank you for your contribution',NULL,NULL,NULL,NULL,1,0,'1011100010011111111',1,1,NULL,0,NULL,NULL,NULL,1,0,NULL,NULL,NULL),(1362146005192,'2015-08-31 07:29:55',1362146005188,NULL,'Registration','',NULL,NULL,0,'pk_test_PNEuDR4lODkImtjqCXotVXvJ','sk_test_LyB9Y7xZOhOzjKBLpGLaggs7',4,NULL,NULL,0,0,NULL,NULL,NULL,0,1,NULL,NULL,0,NULL,NULL,'#b6e8f9',NULL,NULL,NULL,NULL,1,1,1,NULL,1,NULL,NULL,'Thank you for your contribution',NULL,NULL,NULL,NULL,1,0,'1011100010011111111',1,1,NULL,0,NULL,NULL,NULL,1,0,NULL,NULL,NULL),(1362146005202,'2015-08-31 07:37:39',1362146005199,NULL,'Registration','',NULL,NULL,0,'pk_test_PNEuDR4lODkImtjqCXotVXvJ','sk_test_LyB9Y7xZOhOzjKBLpGLaggs7',4,NULL,NULL,0,15000,NULL,NULL,NULL,0,1,NULL,NULL,0,NULL,NULL,NULL,NULL,'Donation','Sponsor','<p>Event desc</p>\r\n',0,0,1,NULL,1,NULL,NULL,'<p><font face=\'Arial\'>Thank you for your contribution.</font></p>',NULL,NULL,NULL,NULL,0,0,'1011110010001111010',1,1,NULL,0,'Auction Registration',NULL,NULL,0,0,NULL,NULL,NULL),(1362146005209,'2015-08-31 07:38:23',1362146005206,NULL,'Registration','',NULL,NULL,0,'pk_test_PNEuDR4lODkImtjqCXotVXvJ','sk_test_LyB9Y7xZOhOzjKBLpGLaggs7',4,NULL,NULL,0,12567,NULL,NULL,NULL,0,1,NULL,NULL,0,NULL,NULL,NULL,NULL,'Donation','Sponsor','Enter Description of Event Here.',0,0,1,NULL,1,NULL,NULL,'<p><font face=\'Arial\'>Thank you for your contribution.</font></p>',NULL,NULL,NULL,NULL,0,0,'1011110010001111010',1,1,NULL,0,'Auction Registration',NULL,NULL,0,0,NULL,NULL,NULL),(1362146005216,'2015-08-31 07:44:23',1362146005213,NULL,'Registration','',NULL,NULL,0,'pk_test_PNEuDR4lODkImtjqCXotVXvJ','sk_test_LyB9Y7xZOhOzjKBLpGLaggs7',4,NULL,NULL,0,123,NULL,NULL,NULL,0,1,NULL,NULL,0,NULL,NULL,NULL,NULL,'Donation','Sponsor','',1,1,1,NULL,1,NULL,NULL,'<p><font face=\'Arial\'>Thank you for your contribution.</font></p>',NULL,NULL,NULL,NULL,0,0,'1011110010001111010',1,1,NULL,0,'Auction Registration',NULL,NULL,1,0,NULL,NULL,NULL),(1362146005223,'2015-08-31 07:47:15',1362146005220,NULL,'Registration','',NULL,NULL,0,'pk_test_PNEuDR4lODkImtjqCXotVXvJ','sk_test_LyB9Y7xZOhOzjKBLpGLaggs7',4,NULL,NULL,0,45321,NULL,NULL,NULL,0,1,NULL,NULL,0,NULL,NULL,NULL,NULL,'Donation','Sponsor','',0,1,1,NULL,1,NULL,NULL,'<p><font face=\'Arial\'>Thank you for your contribution.</font></p>',NULL,NULL,NULL,NULL,0,0,'1011110010001111010',1,1,NULL,0,'Auction Registration',NULL,NULL,1,0,NULL,NULL,NULL),(1362146005230,'2015-08-31 07:53:07',1362146005227,NULL,'Registration','',NULL,NULL,0,'pk_test_PNEuDR4lODkImtjqCXotVXvJ','sk_test_LyB9Y7xZOhOzjKBLpGLaggs7',4,NULL,NULL,0,321,NULL,NULL,NULL,0,1,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,'',1,1,1,NULL,1,NULL,NULL,'<p><font face=\'Arial\'>Thank you for your contribution.</font></p>',NULL,NULL,NULL,NULL,1,0,'1111110010001111110',1,1,NULL,0,NULL,NULL,NULL,1,0,NULL,NULL,NULL),(1362146005237,'2015-08-31 07:53:46',1362146005234,NULL,'Registration','',NULL,NULL,0,'pk_test_PNEuDR4lODkImtjqCXotVXvJ','sk_test_LyB9Y7xZOhOzjKBLpGLaggs7',4,NULL,NULL,0,0,NULL,NULL,NULL,0,1,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,'',1,1,1,NULL,1,NULL,NULL,'<p><font face=\'Arial\'>Thank you for your contribution.</font></p>',NULL,NULL,NULL,NULL,1,0,'1111110010001111110',1,1,NULL,0,NULL,NULL,NULL,1,0,NULL,NULL,NULL),(1362146005244,'2015-08-31 07:54:48',1362146005241,NULL,'Registration','',NULL,NULL,0,'pk_test_PNEuDR4lODkImtjqCXotVXvJ','sk_test_LyB9Y7xZOhOzjKBLpGLaggs7',4,NULL,NULL,0,5000,NULL,NULL,NULL,0,1,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,'',1,1,1,NULL,1,NULL,NULL,'<p><font face=\'Arial\'>Thank you for your contribution.</font></p>',NULL,NULL,NULL,NULL,1,0,'1011110010001111110',1,1,NULL,0,NULL,NULL,NULL,1,0,NULL,NULL,NULL),(1362146005251,'2015-08-31 07:56:37',1362146005248,NULL,'Registration','',NULL,NULL,0,'pk_test_PNEuDR4lODkImtjqCXotVXvJ','sk_test_LyB9Y7xZOhOzjKBLpGLaggs7',4,NULL,NULL,0,3000,NULL,NULL,NULL,0,1,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,'',1,1,1,NULL,1,NULL,NULL,'<p><font face=\'Arial\'>Thank you for your contribution.</font></p>',NULL,NULL,NULL,NULL,1,0,'1111110010001111110',1,1,NULL,0,NULL,NULL,NULL,1,0,NULL,NULL,NULL),(1362146005258,'2015-08-31 07:57:25',1362146005255,NULL,'Registration','',NULL,NULL,0,'pk_test_PNEuDR4lODkImtjqCXotVXvJ','sk_test_LyB9Y7xZOhOzjKBLpGLaggs7',4,NULL,NULL,0,3214,NULL,NULL,NULL,0,1,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,'<p>zxc</p>\r\n',1,1,1,NULL,1,NULL,NULL,'<p><font face=\'Arial\'>Thank you for your contribution.</font></p>',NULL,NULL,NULL,NULL,1,0,'1011110010001111110',1,1,NULL,0,NULL,NULL,NULL,1,0,NULL,NULL,NULL);
/*!40000 ALTER TABLE `siteconfig` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sitefamilymember`
--

DROP TABLE IF EXISTS `sitefamilymember`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sitefamilymember` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `siteMemberId` bigint(20) DEFAULT NULL,
  `firstName` varchar(40) NOT NULL,
  `lastName` varchar(40) NOT NULL,
  `email` varchar(100) DEFAULT NULL,
  `gender` tinyint(4) DEFAULT NULL,
  `dob` datetime DEFAULT NULL,
  `campaignItemId` int(11) DEFAULT NULL,
  `campaignItemName` varchar(100) DEFAULT NULL,
  `registrationFee` float NOT NULL DEFAULT '0',
  `signedWaiver` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sitefamilymember`
--

LOCK TABLES `sitefamilymember` WRITE;
/*!40000 ALTER TABLE `sitefamilymember` DISABLE KEYS */;
/*!40000 ALTER TABLE `sitefamilymember` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `siteformsettings`
--

DROP TABLE IF EXISTS `siteformsettings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `siteformsettings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `siteId` bigint(20) DEFAULT NULL,
  `showPassword` tinyint(4) DEFAULT NULL,
  `showConfirmPassword` tinyint(4) DEFAULT NULL,
  `showAddress` tinyint(4) DEFAULT NULL,
  `showCity` tinyint(4) DEFAULT NULL,
  `showState` tinyint(4) DEFAULT NULL,
  `showZip` tinyint(4) DEFAULT NULL,
  `showGender` tinyint(4) DEFAULT NULL,
  `showDob` tinyint(4) DEFAULT NULL,
  `sponsorText` varchar(4000) DEFAULT NULL,
  `sponsorText2` varchar(4000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `siteformsettings`
--

LOCK TABLES `siteformsettings` WRITE;
/*!40000 ALTER TABLE `siteformsettings` DISABLE KEYS */;
INSERT INTO `siteformsettings` VALUES (1,'2015-08-31 07:23:04',1362146005143,1,1,1,1,1,1,1,1,NULL,NULL),(2,'2015-08-31 07:23:22',1362146005152,1,1,1,1,1,1,1,1,NULL,NULL),(3,'2015-08-31 07:23:42',1362146005161,1,1,1,1,1,1,1,1,NULL,NULL),(4,'2015-08-31 07:24:08',1362146005170,1,1,1,1,1,1,1,1,NULL,NULL),(5,'2015-08-31 07:27:42',1362146005179,1,1,1,1,1,1,1,1,NULL,NULL),(6,'2015-08-31 07:29:55',1362146005188,1,1,1,1,1,1,1,1,NULL,NULL),(7,'2015-08-31 07:37:39',1362146005199,1,1,1,1,1,1,1,1,NULL,NULL),(8,'2015-08-31 07:38:23',1362146005206,1,1,1,1,1,1,1,1,NULL,NULL),(9,'2015-08-31 07:44:23',1362146005213,1,1,1,1,1,1,1,1,NULL,NULL),(10,'2015-08-31 07:47:15',1362146005220,1,1,1,1,1,1,1,1,NULL,NULL),(11,'2015-08-31 07:53:07',1362146005227,1,1,1,1,1,1,1,1,NULL,NULL),(12,'2015-08-31 07:53:46',1362146005234,1,1,1,1,1,1,1,1,NULL,NULL),(13,'2015-08-31 07:54:48',1362146005241,1,1,1,1,1,1,1,1,NULL,NULL),(14,'2015-08-31 07:56:37',1362146005248,1,1,1,1,1,1,1,1,NULL,NULL),(15,'2015-08-31 07:57:25',1362146005255,1,1,1,1,1,1,1,1,NULL,NULL),(16,'2015-08-31 17:56:35',1362145798075,1,1,1,1,1,1,1,1,NULL,NULL);
/*!40000 ALTER TABLE `siteformsettings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sitemember`
--

DROP TABLE IF EXISTS `sitemember`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sitemember` (
  `id` bigint(20) NOT NULL DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `siteId` bigint(20) DEFAULT NULL,
  `memberId` bigint(20) DEFAULT NULL,
  `firstName` varchar(40) NOT NULL,
  `lastName` varchar(40) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `password_salt` varchar(60) DEFAULT NULL,
  `campaignId` bigint(20) DEFAULT NULL,
  `signedWaiver` tinyint(4) DEFAULT NULL,
  `teamCampaignId` bigint(20) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `state` varchar(25) DEFAULT NULL,
  `zip` varchar(10) DEFAULT NULL,
  `country` tinyint(4) DEFAULT NULL,
  `gender` tinyint(4) DEFAULT NULL,
  `dob` datetime DEFAULT NULL,
  `campaignItemId` bigint(20) DEFAULT NULL,
  `campaignItemName` varchar(100) DEFAULT NULL,
  `registrationFee` float NOT NULL DEFAULT '0',
  `phone` varchar(50) DEFAULT NULL,
  `photoPath` int(11) DEFAULT NULL,
  `ccid` bigint(20) DEFAULT NULL,
  `imageBlobId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `campaignId` (`campaignId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sitemember`
--

LOCK TABLES `sitemember` WRITE;
/*!40000 ALTER TABLE `sitemember` DISABLE KEYS */;
INSERT INTO `sitemember` VALUES (1362146005765,'2015-09-01 12:46:26',1362146005206,1362146005764,'Ron','Red','ron@red.qwe','zbIy1CJQNDDtp9N6gg/ucLvwAU0=',NULL,1362146005771,0,0,NULL,NULL,NULL,NULL,0,0,'0002-11-30 12:46:00',0,NULL,0,NULL,0,0,0),(1362146005779,'2015-09-01 12:47:23',1362146005199,1362146005778,'Usert','First','qwe1@qwe1.qwe','zbIy1CJQNDDtp9N6gg/ucLvwAU0=',NULL,1362146005780,0,0,NULL,NULL,NULL,NULL,0,0,NULL,0,NULL,0,NULL,0,0,0),(1362146005788,'2015-09-01 12:47:56',1362146005199,1362146005787,'Team','Owner','team@test.qwe','zbIy1CJQNDDtp9N6gg/ucLvwAU0=',NULL,1362146005789,0,1362146005796,NULL,NULL,NULL,NULL,0,0,NULL,0,NULL,0,NULL,0,0,0),(1362146005802,'2015-09-01 12:48:32',1362146005199,1362146005801,'User','Second','qwe2@qwe2.qwe','zbIy1CJQNDDtp9N6gg/ucLvwAU0=',NULL,1362146005803,0,1362146005796,NULL,NULL,NULL,NULL,0,0,NULL,0,NULL,0,NULL,0,0,0),(1362146005811,'2015-09-01 12:48:59',1362146005199,1362146005759,'Test','Third','qwe3@qwe3.qwe','zbIy1CJQNDDtp9N6gg/ucLvwAU0=',NULL,1362146005812,0,0,NULL,NULL,NULL,NULL,0,0,NULL,0,NULL,0,NULL,0,0,0),(1362146005832,'2015-09-01 12:55:06',1362146005220,1362146005831,'qwe','qwe','qwe@qwe.qwea','xVmnEcgUI8tx6AI9eiKisDtkR8g=',NULL,0,0,0,NULL,NULL,NULL,NULL,0,0,'0002-11-30 12:54:05',0,NULL,0,NULL,0,0,0),(1362146005840,'2015-09-01 12:56:13',1362146005220,1362146005839,'John','Green','john@qwe.qwe','xVmnEcgUI8tx6AI9eiKisDtkR8g=',NULL,0,0,0,NULL,NULL,NULL,NULL,0,0,'0002-11-30 12:55:41',0,NULL,0,NULL,0,0,0);
/*!40000 ALTER TABLE `sitemember` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sitememberactivity`
--

DROP TABLE IF EXISTS `sitememberactivity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sitememberactivity` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `siteMemberId` bigint(20) DEFAULT NULL,
  `activityName` varchar(200) DEFAULT NULL,
  `activityPoints` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sitememberactivity`
--

LOCK TABLES `sitememberactivity` WRITE;
/*!40000 ALTER TABLE `sitememberactivity` DISABLE KEYS */;
/*!40000 ALTER TABLE `sitememberactivity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sitememberembedinfo`
--

DROP TABLE IF EXISTS `sitememberembedinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sitememberembedinfo` (
  `id` bigint(20) NOT NULL,
  `siteMemberId` bigint(20) NOT NULL,
  `personalCallOut` varchar(400) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sitememberembedinfo`
--

LOCK TABLES `sitememberembedinfo` WRITE;
/*!40000 ALTER TABLE `sitememberembedinfo` DISABLE KEYS */;
/*!40000 ALTER TABLE `sitememberembedinfo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sitemessage`
--

DROP TABLE IF EXISTS `sitemessage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sitemessage` (
  `id` bigint(20) NOT NULL DEFAULT '0',
  `siteid` bigint(20) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `fbtitle` varchar(150) DEFAULT NULL,
  `fbmessage` varchar(300) DEFAULT NULL,
  `mediaid` bigint(20) DEFAULT NULL,
  `twitterMessage` varchar(140) DEFAULT NULL,
  `thankyouwebsitemessage` varchar(1000) DEFAULT NULL,
  `donationdescription` varchar(2000) DEFAULT NULL,
  `registrationdescription` varchar(2000) DEFAULT NULL,
  `sponsordescription` varchar(2000) DEFAULT NULL,
  `create_page_body` varchar(500) DEFAULT NULL,
  `custom_trasaction_msg` varchar(1000) DEFAULT NULL,
  `email_message` varchar(1000) DEFAULT NULL,
  `page_thankyou_msg` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sitemessage`
--

LOCK TABLES `sitemessage` WRITE;
/*!40000 ALTER TABLE `sitemessage` DISABLE KEYS */;
/*!40000 ALTER TABLE `sitemessage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sitemobileconfig`
--

DROP TABLE IF EXISTS `sitemobileconfig`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sitemobileconfig` (
  `id` bigint(20) unsigned NOT NULL,
  `created` datetime DEFAULT NULL,
  `siteId` bigint(20) DEFAULT NULL,
  `mobileMastheadCode` text,
  `mobileNavBgColor` varchar(8) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sitemobileconfig`
--

LOCK TABLES `sitemobileconfig` WRITE;
/*!40000 ALTER TABLE `sitemobileconfig` DISABLE KEYS */;
/*!40000 ALTER TABLE `sitemobileconfig` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sitenav`
--

DROP TABLE IF EXISTS `sitenav`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sitenav` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `siteId` bigint(20) DEFAULT NULL,
  `pageId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sitenav`
--

LOCK TABLES `sitenav` WRITE;
/*!40000 ALTER TABLE `sitenav` DISABLE KEYS */;
/*!40000 ALTER TABLE `sitenav` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sitepageform`
--

DROP TABLE IF EXISTS `sitepageform`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sitepageform` (
  `id` bigint(20) NOT NULL,
  `site_id` bigint(20) DEFAULT NULL,
  `form_id` bigint(20) DEFAULT NULL,
  `thankyou_msg` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sitepageform`
--

LOCK TABLES `sitepageform` WRITE;
/*!40000 ALTER TABLE `sitepageform` DISABLE KEYS */;
/*!40000 ALTER TABLE `sitepageform` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sitepagerel`
--

DROP TABLE IF EXISTS `sitepagerel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sitepagerel` (
  `id` bigint(20) NOT NULL DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `siteId` bigint(20) DEFAULT NULL,
  `templateId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sitepagerel`
--

LOCK TABLES `sitepagerel` WRITE;
/*!40000 ALTER TABLE `sitepagerel` DISABLE KEYS */;
/*!40000 ALTER TABLE `sitepagerel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sitepoll`
--

DROP TABLE IF EXISTS `sitepoll`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sitepoll` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `siteId` bigint(20) DEFAULT NULL,
  `pollCampaignId` bigint(20) DEFAULT NULL,
  `pollType` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sitepoll`
--

LOCK TABLES `sitepoll` WRITE;
/*!40000 ALTER TABLE `sitepoll` DISABLE KEYS */;
/*!40000 ALTER TABLE `sitepoll` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sitesettings`
--

DROP TABLE IF EXISTS `sitesettings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sitesettings` (
  `id` bigint(20) NOT NULL,
  `site_id` bigint(20) NOT NULL,
  `supersite_id` bigint(20) NOT NULL,
  `key_code` varchar(100) DEFAULT NULL,
  `value` varchar(200) DEFAULT NULL,
  `locale` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sitesettings`
--

LOCK TABLES `sitesettings` WRITE;
/*!40000 ALTER TABLE `sitesettings` DISABLE KEYS */;
/*!40000 ALTER TABLE `sitesettings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sitestat`
--

DROP TABLE IF EXISTS `sitestat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sitestat` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `totalRaised` float DEFAULT NULL,
  `totalDonors` int(11) DEFAULT NULL,
  `totalEmailsSent` int(11) DEFAULT NULL,
  `totalPages` int(11) DEFAULT NULL,
  `memberId` bigint(20) DEFAULT NULL,
  `siteId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sitestat`
--

LOCK TABLES `sitestat` WRITE;
/*!40000 ALTER TABLE `sitestat` DISABLE KEYS */;
INSERT INTO `sitestat` VALUES (1,'2015-09-01 13:15:25',0,0,0,0,1362146005137,1362145798075),(2,'2015-09-01 13:15:25',0,0,0,0,1362146005137,1362146005143),(3,'2015-09-01 13:15:25',0,0,0,0,1362146005137,1362146005152),(4,'2015-09-01 13:15:25',0,0,0,0,1362146005137,1362146005161),(5,'2015-09-01 13:15:25',0,0,0,0,1362146005137,1362146005170),(6,'2015-09-01 13:15:25',0,0,0,0,1362146005137,1362146005179),(7,'2015-09-01 13:15:25',0,0,0,0,1362146005137,1362146005188),(8,'2015-09-01 13:15:25',0,0,0,5,1362146005137,1362146005199),(9,'2015-09-01 13:15:26',120,1,0,1,1362146005137,1362146005206),(10,'2015-09-01 13:15:26',0,0,0,0,1362146005137,1362146005213),(11,'2015-09-01 13:15:26',100,2,0,0,1362146005137,1362146005220),(12,'2015-09-01 13:15:26',0,0,0,0,1362146005137,1362146005227),(13,'2015-09-01 13:15:26',0,0,0,0,1362146005137,1362146005234),(14,'2015-09-01 13:15:26',0,0,0,0,1362146005137,1362146005241),(15,'2015-09-01 13:15:26',0,0,0,0,1362146005137,1362146005248),(16,'2015-09-01 13:15:26',0,0,0,0,1362146005137,1362146005255);
/*!40000 ALTER TABLE `sitestat` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `siteteam`
--

DROP TABLE IF EXISTS `siteteam`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `siteteam` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `siteId` bigint(20) DEFAULT NULL,
  `siteMemberId` bigint(20) DEFAULT NULL,
  `campaignId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `siteteam`
--

LOCK TABLES `siteteam` WRITE;
/*!40000 ALTER TABLE `siteteam` DISABLE KEYS */;
INSERT INTO `siteteam` VALUES (1,'2015-09-01 12:47:57',1362146005199,1362146005788,1362146005796);
/*!40000 ALTER TABLE `siteteam` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sitetemplate`
--

DROP TABLE IF EXISTS `sitetemplate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sitetemplate` (
  `id` bigint(20) unsigned NOT NULL,
  `created` datetime DEFAULT NULL,
  `blobId` bigint(20) DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL,
  `url` varchar(2000) DEFAULT NULL,
  `stylesheet` longtext,
  `pic1` bigint(20) DEFAULT NULL,
  `pic2` bigint(20) DEFAULT NULL,
  `pic3` bigint(20) DEFAULT NULL,
  `description` varchar(5000) DEFAULT NULL,
  `isDefault` tinyint(4) DEFAULT NULL,
  `masthead` text,
  `backgroundcolor` varchar(15) DEFAULT NULL,
  `backgroundimageid` bigint(20) DEFAULT NULL,
  `imageproperty` varchar(6) DEFAULT NULL,
  `memberId` bigint(20) DEFAULT NULL,
  `lastUpdated` datetime DEFAULT NULL,
  `categoryId` bigint(20) DEFAULT NULL,
  `mediaId` bigint(20) DEFAULT NULL,
  `bgImageMediaid` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sitetemplate`
--

LOCK TABLES `sitetemplate` WRITE;
/*!40000 ALTER TABLE `sitetemplate` DISABLE KEYS */;
/*!40000 ALTER TABLE `sitetemplate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sitetemplatecount`
--

DROP TABLE IF EXISTS `sitetemplatecount`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sitetemplatecount` (
  `id` bigint(20) NOT NULL DEFAULT '0',
  `sourceId` bigint(20) DEFAULT NULL,
  `childId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sitetemplatecount`
--

LOCK TABLES `sitetemplatecount` WRITE;
/*!40000 ALTER TABLE `sitetemplatecount` DISABLE KEYS */;
/*!40000 ALTER TABLE `sitetemplatecount` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `skill`
--

DROP TABLE IF EXISTS `skill`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `skill` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `memberId` bigint(20) DEFAULT NULL,
  `rank` tinyint(3) DEFAULT NULL,
  `name` varchar(40) DEFAULT NULL,
  `rankTotal` int(11) DEFAULT NULL,
  `voteTotal` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `name` (`name`),
  KEY `memberId` (`memberId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `skill`
--

LOCK TABLES `skill` WRITE;
/*!40000 ALTER TABLE `skill` DISABLE KEYS */;
/*!40000 ALTER TABLE `skill` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `skillvote`
--

DROP TABLE IF EXISTS `skillvote`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `skillvote` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tagId` int(11) DEFAULT NULL,
  `memberId` bigint(20) DEFAULT NULL,
  `rank` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tagId` (`tagId`),
  KEY `memberId` (`memberId`),
  CONSTRAINT `skillvote_ibfk_1` FOREIGN KEY (`tagId`) REFERENCES `skill` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `skillvote`
--

LOCK TABLES `skillvote` WRITE;
/*!40000 ALTER TABLE `skillvote` DISABLE KEYS */;
/*!40000 ALTER TABLE `skillvote` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `socialnetworkmember`
--

DROP TABLE IF EXISTS `socialnetworkmember`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `socialnetworkmember` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `socialNetworkId` varchar(100) DEFAULT NULL,
  `memberId` bigint(20) DEFAULT NULL,
  `socialNetworkType` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `socialnetworkmember`
--

LOCK TABLES `socialnetworkmember` WRITE;
/*!40000 ALTER TABLE `socialnetworkmember` DISABLE KEYS */;
/*!40000 ALTER TABLE `socialnetworkmember` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `socialnetworksitemember`
--

DROP TABLE IF EXISTS `socialnetworksitemember`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `socialnetworksitemember` (
  `id` bigint(20) NOT NULL DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `socialNetworkId` varchar(100) DEFAULT NULL,
  `siteMemberId` bigint(20) DEFAULT NULL,
  `socialNetworkType` int(11) DEFAULT NULL,
  `siteId` bigint(20) DEFAULT NULL,
  `superSiteId` bigint(20) DEFAULT NULL,
  `superSiteMemberId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `socialnetworksitemember`
--

LOCK TABLES `socialnetworksitemember` WRITE;
/*!40000 ALTER TABLE `socialnetworksitemember` DISABLE KEYS */;
/*!40000 ALTER TABLE `socialnetworksitemember` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `socialstat`
--

DROP TABLE IF EXISTS `socialstat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `socialstat` (
  `id` bigint(20) NOT NULL DEFAULT '0',
  `siteId` bigint(20) DEFAULT NULL,
  `campaignId` bigint(20) DEFAULT NULL,
  `type` tinyint(4) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `memberId` bigint(20) DEFAULT '0',
  `reach` int(5) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `socialstat`
--

LOCK TABLES `socialstat` WRITE;
/*!40000 ALTER TABLE `socialstat` DISABLE KEYS */;
/*!40000 ALTER TABLE `socialstat` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `society`
--

DROP TABLE IF EXISTS `society`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `society` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `mission` varchar(2500) DEFAULT NULL,
  `founderId` bigint(20) DEFAULT NULL,
  `moderatorId` bigint(20) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `privacy` tinyint(4) DEFAULT NULL,
  `joinMethod` tinyint(4) DEFAULT NULL,
  `inclusionMethod` tinyint(4) DEFAULT NULL,
  `exclusionMethod` tinyint(4) DEFAULT NULL,
  `moderatorPower` tinyint(4) DEFAULT NULL,
  `memberCount` int(11) DEFAULT NULL,
  `photoPath` int(11) DEFAULT NULL,
  `showIntro` tinyint(4) DEFAULT NULL,
  `campaignId` bigint(20) DEFAULT NULL,
  `charityId` bigint(20) DEFAULT NULL,
  `type` tinyint(4) DEFAULT NULL,
  `causeType` tinyint(4) DEFAULT NULL,
  `extLabel` varchar(350) DEFAULT NULL,
  `applicantCount` int(11) DEFAULT NULL,
  `pollId` int(11) DEFAULT NULL,
  `societyInfo` text,
  `masterSocietyId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `founderId` (`founderId`),
  KEY `moderatorId` (`moderatorId`),
  KEY `campaignId` (`campaignId`),
  KEY `charityId` (`charityId`),
  KEY `pollId` (`pollId`),
  CONSTRAINT `society_ibfk_5` FOREIGN KEY (`pollId`) REFERENCES `poll` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `society`
--

LOCK TABLES `society` WRITE;
/*!40000 ALTER TABLE `society` DISABLE KEYS */;
/*!40000 ALTER TABLE `society` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `societyevent`
--

DROP TABLE IF EXISTS `societyevent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `societyevent` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `societyId` int(11) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `description` varchar(2500) DEFAULT NULL,
  `location` varchar(100) DEFAULT NULL,
  `hour` tinyint(4) DEFAULT NULL,
  `minute` tinyint(4) DEFAULT NULL,
  `ampm` varchar(2) DEFAULT NULL,
  `timeZone` tinyint(4) DEFAULT NULL,
  `duration` tinyint(4) DEFAULT NULL,
  `durationScale` tinyint(4) DEFAULT NULL,
  `url` varchar(150) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `remindTime` datetime DEFAULT NULL,
  `modTime` datetime DEFAULT NULL,
  `memberId` bigint(20) DEFAULT NULL,
  `campaignId` bigint(20) DEFAULT NULL,
  `imageName` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `societyId` (`societyId`),
  KEY `memberId` (`memberId`),
  KEY `campaignId` (`campaignId`),
  CONSTRAINT `societyevent_ibfk_1` FOREIGN KEY (`societyId`) REFERENCES `society` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `societyevent`
--

LOCK TABLES `societyevent` WRITE;
/*!40000 ALTER TABLE `societyevent` DISABLE KEYS */;
/*!40000 ALTER TABLE `societyevent` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `societyexcludevote`
--

DROP TABLE IF EXISTS `societyexcludevote`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `societyexcludevote` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subjectId` bigint(20) DEFAULT NULL,
  `voterId` bigint(20) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `societyId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `subjectId` (`subjectId`),
  KEY `voterId` (`voterId`),
  KEY `societyId` (`societyId`),
  CONSTRAINT `societyexcludevote_ibfk_3` FOREIGN KEY (`societyId`) REFERENCES `society` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `societyexcludevote`
--

LOCK TABLES `societyexcludevote` WRITE;
/*!40000 ALTER TABLE `societyexcludevote` DISABLE KEYS */;
/*!40000 ALTER TABLE `societyexcludevote` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `societyincludevote`
--

DROP TABLE IF EXISTS `societyincludevote`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `societyincludevote` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subjectId` bigint(20) DEFAULT NULL,
  `voterId` bigint(20) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `societyId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `subjectId` (`subjectId`),
  KEY `voterId` (`voterId`),
  KEY `societyId` (`societyId`),
  CONSTRAINT `societyincludevote_ibfk_3` FOREIGN KEY (`societyId`) REFERENCES `society` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `societyincludevote`
--

LOCK TABLES `societyincludevote` WRITE;
/*!40000 ALTER TABLE `societyincludevote` DISABLE KEYS */;
/*!40000 ALTER TABLE `societyincludevote` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `societylog`
--

DROP TABLE IF EXISTS `societylog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `societylog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `societyId` int(11) DEFAULT NULL,
  `viewingMemberId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `societyId` (`societyId`),
  KEY `viewingMemberId` (`viewingMemberId`),
  CONSTRAINT `societylog_ibfk_1` FOREIGN KEY (`societyId`) REFERENCES `society` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `societylog`
--

LOCK TABLES `societylog` WRITE;
/*!40000 ALTER TABLE `societylog` DISABLE KEYS */;
/*!40000 ALTER TABLE `societylog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `societymedia`
--

DROP TABLE IF EXISTS `societymedia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `societymedia` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `anonymous` tinyint(4) DEFAULT NULL,
  `societyId` int(11) DEFAULT NULL,
  `memberId` bigint(20) DEFAULT NULL,
  `memberName` varchar(50) DEFAULT NULL,
  `caption` varchar(250) DEFAULT NULL,
  `filename` varchar(50) DEFAULT NULL,
  `mediaURL` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `societyId` (`societyId`),
  KEY `memberId` (`memberId`),
  CONSTRAINT `societymedia_ibfk_1` FOREIGN KEY (`societyId`) REFERENCES `society` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `societymedia`
--

LOCK TABLES `societymedia` WRITE;
/*!40000 ALTER TABLE `societymedia` DISABLE KEYS */;
/*!40000 ALTER TABLE `societymedia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `societymember`
--

DROP TABLE IF EXISTS `societymember`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `societymember` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `memberId` bigint(20) DEFAULT NULL,
  `societyId` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `extData` varchar(1000) DEFAULT NULL,
  `title` varchar(200) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `phone` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `memberId` (`memberId`),
  KEY `societyId` (`societyId`),
  CONSTRAINT `societymember_ibfk_2` FOREIGN KEY (`societyId`) REFERENCES `society` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `societymember`
--

LOCK TABLES `societymember` WRITE;
/*!40000 ALTER TABLE `societymember` DISABLE KEYS */;
/*!40000 ALTER TABLE `societymember` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `societymembercount`
--

DROP TABLE IF EXISTS `societymembercount`;
/*!50001 DROP VIEW IF EXISTS `societymembercount`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `societymembercount` (
  `id` tinyint NOT NULL,
  `count` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `societymessage`
--

DROP TABLE IF EXISTS `societymessage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `societymessage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `societyMessageId` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `anonymous` tinyint(4) DEFAULT NULL,
  `societyId` int(11) DEFAULT NULL,
  `memberId` bigint(20) DEFAULT NULL,
  `memberName` varchar(50) DEFAULT NULL,
  `headline` varchar(100) DEFAULT NULL,
  `body` varchar(5000) DEFAULT NULL,
  `filename` varchar(50) DEFAULT NULL,
  `archived` datetime DEFAULT NULL,
  `invitedId` int(11) DEFAULT NULL,
  `lastMessage` datetime DEFAULT NULL,
  `priority` tinyint(4) DEFAULT NULL,
  `endOfThread` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `societyMessageId` (`societyMessageId`),
  KEY `societyId` (`societyId`),
  KEY `memberId` (`memberId`),
  CONSTRAINT `societymessage_ibfk_1` FOREIGN KEY (`societyMessageId`) REFERENCES `societymessage` (`id`) ON DELETE CASCADE,
  CONSTRAINT `societymessage_ibfk_2` FOREIGN KEY (`societyId`) REFERENCES `society` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `societymessage`
--

LOCK TABLES `societymessage` WRITE;
/*!40000 ALTER TABLE `societymessage` DISABLE KEYS */;
/*!40000 ALTER TABLE `societymessage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `societyopportunity`
--

DROP TABLE IF EXISTS `societyopportunity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `societyopportunity` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `societyId` int(11) DEFAULT NULL,
  `title` varchar(200) DEFAULT NULL,
  `description` longtext,
  `address` varchar(200) DEFAULT NULL,
  `city` varchar(75) DEFAULT NULL,
  `state` varchar(20) DEFAULT NULL,
  `zip` varchar(10) DEFAULT NULL,
  `fileName` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `societyopportunity`
--

LOCK TABLES `societyopportunity` WRITE;
/*!40000 ALTER TABLE `societyopportunity` DISABLE KEYS */;
/*!40000 ALTER TABLE `societyopportunity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `solution`
--

DROP TABLE IF EXISTS `solution`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `solution` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `name` varchar(800) DEFAULT NULL,
  `adminId` bigint(20) DEFAULT NULL,
  `mastheadCode` text,
  `charityId` bigint(20) DEFAULT NULL,
  `styleSheet` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `solution`
--

LOCK TABLES `solution` WRITE;
/*!40000 ALTER TABLE `solution` DISABLE KEYS */;
INSERT INTO `solution` VALUES (1,'2015-08-31 07:20:20','TestSolution',1362146005137,NULL,131930701,NULL),(2,'2015-08-31 07:20:53','AnotherSolution',1362146005137,NULL,131930701,NULL),(3,'2015-08-31 07:21:32','UniqueSolution',1362146005137,NULL,131930701,NULL);
/*!40000 ALTER TABLE `solution` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `solutionnav`
--

DROP TABLE IF EXISTS `solutionnav`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `solutionnav` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `solutionId` int(11) DEFAULT NULL,
  `solutionPageId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `solutionnav`
--

LOCK TABLES `solutionnav` WRITE;
/*!40000 ALTER TABLE `solutionnav` DISABLE KEYS */;
/*!40000 ALTER TABLE `solutionnav` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `solutionpage`
--

DROP TABLE IF EXISTS `solutionpage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `solutionpage` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `content` text,
  `name` varchar(200) DEFAULT NULL,
  `url` varchar(800) DEFAULT NULL,
  `isParent` tinyint(4) DEFAULT NULL,
  `parentId` int(11) DEFAULT NULL,
  `isDeleted` tinyint(4) DEFAULT NULL,
  `isHomePage` tinyint(4) DEFAULT NULL,
  `homePageImageCode` text,
  `pagePosition` tinyint(4) DEFAULT NULL,
  `isPrivate` tinyint(4) NOT NULL DEFAULT '0',
  `type` int(11) NOT NULL DEFAULT '0',
  `hideWhenLoggedIn` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `solutionpage`
--

LOCK TABLES `solutionpage` WRITE;
/*!40000 ALTER TABLE `solutionpage` DISABLE KEYS */;
/*!40000 ALTER TABLE `solutionpage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `solutionrel`
--

DROP TABLE IF EXISTS `solutionrel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `solutionrel` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `solutionId` int(11) DEFAULT NULL,
  `superSiteId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `solutionrel`
--

LOCK TABLES `solutionrel` WRITE;
/*!40000 ALTER TABLE `solutionrel` DISABLE KEYS */;
INSERT INTO `solutionrel` VALUES (1,'2015-08-31 07:23:04',1,1362146005140),(2,'2015-08-31 07:23:22',1,1362146005149),(3,'2015-08-31 07:23:42',1,1362146005158),(4,'2015-08-31 07:24:08',2,1362146005167),(5,'2015-08-31 07:27:42',3,1362146005176),(6,'2015-08-31 07:29:55',3,1362146005185),(7,'2015-08-31 17:56:35',1,1362145798072);
/*!40000 ALTER TABLE `solutionrel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `solutionstat`
--

DROP TABLE IF EXISTS `solutionstat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `solutionstat` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `totalRaised` float DEFAULT NULL,
  `totalDonors` int(11) DEFAULT NULL,
  `totalEmailsSent` int(11) DEFAULT NULL,
  `totalPages` int(11) DEFAULT NULL,
  `totalSites` int(11) DEFAULT NULL,
  `totalSuperSites` int(11) DEFAULT NULL,
  `memberId` bigint(20) DEFAULT NULL,
  `solutionId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `solutionstat`
--

LOCK TABLES `solutionstat` WRITE;
/*!40000 ALTER TABLE `solutionstat` DISABLE KEYS */;
INSERT INTO `solutionstat` VALUES (1,'2015-09-01 13:15:25',220,3,0,6,6,4,1362146005137,1),(2,'2015-09-01 13:15:25',0,0,0,0,1,1,1362146005137,2),(3,'2015-09-01 13:15:25',0,0,0,0,2,2,1362146005137,3);
/*!40000 ALTER TABLE `solutionstat` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stripeaccount`
--

DROP TABLE IF EXISTS `stripeaccount`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stripeaccount` (
  `id` bigint(20) unsigned NOT NULL,
  `created` datetime DEFAULT NULL,
  `siteId` bigint(20) DEFAULT NULL,
  `scope` varchar(25) DEFAULT NULL,
  `stripePublishableKey` varchar(200) DEFAULT NULL,
  `tokenType` varchar(25) DEFAULT NULL,
  `refreshToken` varchar(200) DEFAULT NULL,
  `liveMode` varchar(25) DEFAULT NULL,
  `stripeUserId` varchar(200) DEFAULT NULL,
  `accessToken` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stripeaccount`
--

LOCK TABLES `stripeaccount` WRITE;
/*!40000 ALTER TABLE `stripeaccount` DISABLE KEYS */;
INSERT INTO `stripeaccount` VALUES (1362144944571,'2014-03-03 12:41:17',1362144748077,'read_write','pk_test_PNEuDR4lODkImtjqCXotVXvJ','bearer','rt_1nVYRlUOWINWmNy8ihFXyWfhfzVR07qydzBbS03A2I9Sjx0j','false','acct_1mULRT6kWoR9Xq78U5eQ','sk_test_LyB9Y7xZOhOzjKBLpGLaggs7'),(1362144957084,'2014-03-07 05:56:37',1362144957074,'read_write','pk_test_RtqSufp0QarqDe4Q0qU1M9Fk','bearer','rt_3cW06y32AhwqVFfThxp8E2ZhA6gMaCUZ4ngHAHUBWnvgiXMZ','false','acct_103cW02Bou5sRV9t','sk_test_AjY6BfNYiNBuhJL2KWDiYDkH'),(1362144963012,'2014-03-10 04:59:12',1362144962968,'read_write','pk_test_PCEeOy5sxVKRS6lD0cKUQsiC','bearer','rt_3dck8Rhk8aNk1RlGbPuJtmiXVOnCUxYwK30r4WJBOcC13yyK','false','acct_103dck2DxX2psMjr','sk_test_Z1RNVmN0AczsNG7EsmU4MSYh'),(1362144963130,'2014-03-10 09:31:58',1362144963120,'read_write','pk_test_PCEeOy5sxVKRS6lD0cKUQsiC','bearer','rt_3dck8Rhk8aNk1RlGbPuJtmiXVOnCUxYwK30r4WJBOcC13yyK','false','acct_103dck2DxX2psMjr','sk_test_Z1RNVmN0AczsNG7EsmU4MSYh'),(1362145040657,'2014-04-03 12:02:42',1362145040638,'read_write','pk_test_uonPCh0ip5tae9GU8eXEpvld','bearer','rt_3mj0JJQkl1yGnqlnxImGMB3rsLJlNdQe9Rl9Pd9NpDFz69Lj','false','acct_103miz2hs6DgC9Jh','sk_test_ikdrxELkAM8nZC7MBIXPAFgS'),(1362145086508,'2014-04-16 04:38:28',1362144757571,'read_write','pk_test_bfbv7l1tXnwloQDVYbefB6GD','bearer','rt_3rTlfJ2UkMCDQUI77lx7hP7j0lSTDZMRNDTsi2IsYAtRAtrp','false','acct_103rTl2LT7l2WoLr','sk_test_iZPuJKwbD8izbqcKXpA6EnVa'),(1362145087404,'2014-04-16 04:47:26',1362144750586,'read_write','pk_test_iivts5ucFumvU5fJESi9kVzU','bearer','rt_3rTukQLkJHbA7RRt5e9flOlecjdJ2giU3mEerR7HOMMmNJEb','false','acct_103rTu2vcY4T6xOy','sk_test_Y4j8Zjk7BaMjhOBx0ObbVYR3'),(1362145302815,'2014-05-27 09:21:38',1362145257178,'read_write','pk_test_5MnzRQTvZtxYj5EsN4DSupJn','bearer','rt_46uaRTpJrsvfDVdbkxKARkSDNdKKjA8zqmCtFDbaPHa1ZARd','false','acct_1046uY4FMriosE0k','sk_test_yc7g2JRMmWKavT32tPvn244x'),(1362145302816,'2014-05-27 11:06:27',1362144750571,'read_write','pk_test_5MnzRQTvZtxYj5EsN4DSupJn','bearer','rt_46uaRTpJrsvfDVdbkxKARkSDNdKKjA8zqmCtFDbaPHa1ZARd','false','acct_1046uY4FMriosE0k','sk_test_yc7g2JRMmWKavT32tPvn244x');
/*!40000 ALTER TABLE `stripeaccount` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stripecustomer`
--

DROP TABLE IF EXISTS `stripecustomer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stripecustomer` (
  `id` bigint(20) unsigned NOT NULL,
  `created` datetime DEFAULT NULL,
  `customerId` varchar(100) DEFAULT NULL,
  `livemode` tinyint(4) DEFAULT NULL,
  `accountBalance` int(11) DEFAULT NULL,
  `activeCardId` varchar(100) DEFAULT NULL,
  `delinquent` tinyint(4) DEFAULT NULL,
  `description` varchar(400) DEFAULT NULL,
  `discount` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `subscriptionId` varchar(100) DEFAULT NULL,
  `siteId` bigint(20) DEFAULT NULL,
  `type` tinyint(4) DEFAULT NULL,
  `pollResponseId` bigint(20) unsigned DEFAULT NULL,
  `campaignId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stripecustomer`
--

LOCK TABLES `stripecustomer` WRITE;
/*!40000 ALTER TABLE `stripecustomer` DISABLE KEYS */;
/*!40000 ALTER TABLE `stripecustomer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stripeplan`
--

DROP TABLE IF EXISTS `stripeplan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stripeplan` (
  `id` bigint(20) unsigned NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `stripeplanId` varchar(100) DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `monthlyfee` int(11) DEFAULT NULL,
  `setupfee` int(11) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `currency` varchar(10) DEFAULT NULL,
  `livemode` tinyint(4) DEFAULT NULL,
  `planinterval` varchar(20) DEFAULT NULL,
  `intervalcount` tinyint(4) DEFAULT NULL,
  `trialperioddays` int(11) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `deleted` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stripeplan`
--

LOCK TABLES `stripeplan` WRITE;
/*!40000 ALTER TABLE `stripeplan` DISABLE KEYS */;
INSERT INTO `stripeplan` VALUES (1362144789678,'Plan:1362144789677','1362144789677',29900,29900,24900,1,'usd',0,'month',1,0,0,'2014-06-16 16:39:04',NULL),(1362144796149,'Plan:1362144796148','1362144796148',0,0,0,0,'usd',0,'month',1,0,0,'2013-12-27 10:26:24',NULL),(1362144979081,'Plan:1362144979080','1362144979080',0,0,0,0,'usd',0,'month',1,0,0,'2014-03-13 13:33:15',NULL),(1362144979083,'Plan:1362144979082','1362144979082',0,0,0,0,'usd',0,'month',1,0,0,'2014-03-13 13:33:19',NULL),(1362144979101,'Plan:1362144979100','1362144979100',0,0,0,0,'usd',0,'month',1,0,0,'2014-03-13 14:28:09',NULL),(1362145001895,'Plan:1362145001894','1362145001894',0,0,0,0,'usd',0,'month',1,0,0,'2014-03-20 21:53:03',NULL),(1362145001897,'Plan:1362145001896','1362145001896',0,0,0,0,'usd',0,'month',1,0,0,'2014-03-20 21:53:28',NULL),(1362145046669,'Plan:1362145046668','1362145046668',0,0,0,0,'usd',0,'month',1,0,0,'2014-04-04 19:42:05',NULL),(1362145473096,'Plan:1362145473095','1362145473095',40000,40000,0,1,'usd',0,'month',1,0,0,'2014-09-03 13:06:09',NULL),(1362145473122,'Plan:1362145473121','1362145473121',50000,50000,0,1,'usd',0,'month',1,0,0,'2014-08-27 14:43:11',NULL),(1362145480618,'Plan:1362145480617','1362145480617',30000,30000,0,1,'usd',0,'month',1,0,0,'2014-08-28 12:38:38',NULL),(1362145480629,'Plan:1362145480628','1362145480628',20000,20000,0,1,'usd',0,'month',1,0,0,'2014-08-28 12:40:03',NULL),(1362145491592,'Plan:1362145491591','1362145491591',10000,10000,0,1,'usd',0,'month',1,0,0,'2014-09-05 06:52:48',NULL),(1362145494072,'Plan:1362145494071','1362145494071',40000,40000,0,1,'usd',0,'year',1,0,0,'2014-09-08 11:44:44',NULL),(1362145691226,'Plan:1362145691225','1362145691225',4400,4400,0,1,'usd',0,'month',1,0,0,'2014-12-03 10:09:18',NULL);
/*!40000 ALTER TABLE `stripeplan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stripeplanmember`
--

DROP TABLE IF EXISTS `stripeplanmember`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stripeplanmember` (
  `id` bigint(20) NOT NULL,
  `planId` bigint(20) NOT NULL,
  `memberId` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stripeplanmember`
--

LOCK TABLES `stripeplanmember` WRITE;
/*!40000 ALTER TABLE `stripeplanmember` DISABLE KEYS */;
/*!40000 ALTER TABLE `stripeplanmember` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stripesubscription`
--

DROP TABLE IF EXISTS `stripesubscription`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stripesubscription` (
  `id` bigint(20) unsigned NOT NULL,
  `start` datetime DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `planId` bigint(20) DEFAULT NULL,
  `customerId` bigint(20) DEFAULT NULL,
  `enddate` datetime DEFAULT NULL,
  `trialstart` datetime DEFAULT NULL,
  `trialend` datetime DEFAULT NULL,
  `canceldate` datetime DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `stripecustomerId` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stripesubscription`
--

LOCK TABLES `stripesubscription` WRITE;
/*!40000 ALTER TABLE `stripesubscription` DISABLE KEYS */;
/*!40000 ALTER TABLE `stripesubscription` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subscriberlist`
--

DROP TABLE IF EXISTS `subscriberlist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subscriberlist` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `campaignId` bigint(20) DEFAULT NULL,
  `mygroupId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subscriberlist`
--

LOCK TABLES `subscriberlist` WRITE;
/*!40000 ALTER TABLE `subscriberlist` DISABLE KEYS */;
/*!40000 ALTER TABLE `subscriberlist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subscription`
--

DROP TABLE IF EXISTS `subscription`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subscription` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `expires` datetime DEFAULT NULL,
  `charityAffiliateId` int(11) DEFAULT NULL,
  `type` tinyint(4) DEFAULT NULL,
  `memberId` bigint(20) DEFAULT NULL,
  `payOption` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `charityAffiliateId` (`charityAffiliateId`),
  KEY `memberId` (`memberId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subscription`
--

LOCK TABLES `subscription` WRITE;
/*!40000 ALTER TABLE `subscription` DISABLE KEYS */;
/*!40000 ALTER TABLE `subscription` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `supersite`
--

DROP TABLE IF EXISTS `supersite`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `supersite` (
  `id` bigint(20) NOT NULL DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `siteId` bigint(20) DEFAULT NULL,
  `partnerId` int(11) DEFAULT NULL,
  `name` varchar(800) DEFAULT NULL,
  `mastheadCode` varchar(1000) DEFAULT NULL,
  `adminId` bigint(20) DEFAULT NULL,
  `isDeleted` tinyint(4) NOT NULL DEFAULT '0',
  `charityId` bigint(20) DEFAULT NULL,
  `allowms` tinyint(1) DEFAULT NULL,
  `defaultTemplateId` bigint(20) DEFAULT '0',
  `selectCriteria` tinyint(2) DEFAULT '0',
  `allowPage` tinyint(1) DEFAULT NULL,
  `deleteTime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `supersite`
--

LOCK TABLES `supersite` WRITE;
/*!40000 ALTER TABLE `supersite` DISABLE KEYS */;
INSERT INTO `supersite` VALUES (1362145798072,'2015-08-31 17:56:35',0,0,'One more test','<img src=\'/images/crowdster/theme/supersite/supersite-default-banner.jpg\' style=\'width: 992px; height: 156px;\' />',1362146005137,0,0,0,0,0,0,NULL),(1362146005140,'2015-08-31 07:23:04',0,0,'FirstSupersite','<img src=\'/images/crowdster/theme/supersite/supersite-default-banner.jpg\' style=\'width: 992px; height: 156px;\' />',1362146005137,0,0,0,0,0,0,NULL),(1362146005149,'2015-08-31 07:23:21',0,0,'SecondSupersite','<img src=\'/images/crowdster/theme/supersite/supersite-default-banner.jpg\' style=\'width: 992px; height: 156px;\' />',1362146005137,0,0,0,0,0,0,NULL),(1362146005158,'2015-08-31 07:23:41',0,0,'ThirdSupersite','<img src=\'/images/crowdster/theme/supersite/supersite-default-banner.jpg\' style=\'width: 992px; height: 156px;\' />',1362146005137,0,0,0,0,0,0,NULL),(1362146005167,'2015-08-31 07:24:08',0,0,'TestSupersite','<img src=\'/images/crowdster/theme/supersite/supersite-default-banner.jpg\' style=\'width: 992px; height: 156px;\' />',1362146005137,0,0,0,0,0,0,NULL),(1362146005176,'2015-08-31 07:27:42',0,0,'SoftwareSS','<img src=\'/images/crowdster/theme/supersite/supersite-default-banner.jpg\' style=\'width: 992px; height: 156px;\' />',1362146005137,0,0,0,0,0,0,NULL),(1362146005185,'2015-08-31 07:29:55',0,0,'TestSsName','<img src=\'/images/crowdster/theme/supersite/supersite-default-banner.jpg\' style=\'width: 992px; height: 156px;\' />',1362146005137,0,0,0,0,0,0,NULL);
/*!40000 ALTER TABLE `supersite` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `supersiteconfig`
--

DROP TABLE IF EXISTS `supersiteconfig`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `supersiteconfig` (
  `id` bigint(20) NOT NULL,
  `created` datetime DEFAULT NULL,
  `superSiteId` bigint(20) DEFAULT NULL,
  `partnerId` int(11) NOT NULL DEFAULT '0',
  `loginId` varchar(100) DEFAULT NULL,
  `transactionKey` varchar(100) DEFAULT NULL,
  `processorType` tinyint(4) DEFAULT NULL,
  `enable_cc_live` tinyint(4) DEFAULT NULL,
  `enable_subscription` tinyint(4) DEFAULT NULL,
  `apiSignature` varchar(100) DEFAULT NULL,
  `openmovesConfig` varchar(150) DEFAULT NULL,
  `defaultSiteId` bigint(20) DEFAULT NULL,
  `head_tags` varchar(2000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `supersiteconfig`
--

LOCK TABLES `supersiteconfig` WRITE;
/*!40000 ALTER TABLE `supersiteconfig` DISABLE KEYS */;
INSERT INTO `supersiteconfig` VALUES (1362145798078,'2015-08-31 17:56:35',1362145798072,0,'pk_test_PNEuDR4lODkImtjqCXotVXvJ','sk_test_LyB9Y7xZOhOzjKBLpGLaggs7',4,0,0,NULL,NULL,1362145798075,NULL),(1362146005146,'2015-08-31 07:23:04',1362146005140,0,'pk_test_PNEuDR4lODkImtjqCXotVXvJ','sk_test_LyB9Y7xZOhOzjKBLpGLaggs7',4,0,0,NULL,NULL,1362146005143,NULL),(1362146005155,'2015-08-31 07:23:22',1362146005149,0,'pk_test_PNEuDR4lODkImtjqCXotVXvJ','sk_test_LyB9Y7xZOhOzjKBLpGLaggs7',4,0,0,NULL,NULL,1362146005152,NULL),(1362146005164,'2015-08-31 07:23:42',1362146005158,0,'pk_test_PNEuDR4lODkImtjqCXotVXvJ','sk_test_LyB9Y7xZOhOzjKBLpGLaggs7',4,0,0,NULL,NULL,1362146005161,NULL),(1362146005173,'2015-08-31 07:24:08',1362146005167,0,'pk_test_PNEuDR4lODkImtjqCXotVXvJ','sk_test_LyB9Y7xZOhOzjKBLpGLaggs7',4,0,0,NULL,NULL,1362146005170,NULL),(1362146005182,'2015-08-31 07:27:42',1362146005176,0,'pk_test_PNEuDR4lODkImtjqCXotVXvJ','sk_test_LyB9Y7xZOhOzjKBLpGLaggs7',4,0,0,NULL,NULL,1362146005179,NULL),(1362146005191,'2015-08-31 07:29:55',1362146005185,0,'pk_test_PNEuDR4lODkImtjqCXotVXvJ','sk_test_LyB9Y7xZOhOzjKBLpGLaggs7',4,0,0,NULL,NULL,1362146005188,NULL);
/*!40000 ALTER TABLE `supersiteconfig` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `supersitemember`
--

DROP TABLE IF EXISTS `supersitemember`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `supersitemember` (
  `id` bigint(20) NOT NULL,
  `created` datetime DEFAULT NULL,
  `memberId` bigint(20) DEFAULT NULL,
  `firstName` varchar(40) DEFAULT NULL,
  `lastName` varchar(40) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `phone` varchar(15) DEFAULT NULL,
  `organization` varchar(100) DEFAULT NULL,
  `superSiteId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `supersitemember`
--

LOCK TABLES `supersitemember` WRITE;
/*!40000 ALTER TABLE `supersitemember` DISABLE KEYS */;
/*!40000 ALTER TABLE `supersitemember` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `supersitenav`
--

DROP TABLE IF EXISTS `supersitenav`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `supersitenav` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `superSiteId` bigint(20) DEFAULT NULL,
  `pageId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `supersitenav`
--

LOCK TABLES `supersitenav` WRITE;
/*!40000 ALTER TABLE `supersitenav` DISABLE KEYS */;
INSERT INTO `supersitenav` VALUES (1,'2015-08-31 17:57:06',1362145798072,1);
/*!40000 ALTER TABLE `supersitenav` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `supersitepage`
--

DROP TABLE IF EXISTS `supersitepage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `supersitepage` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `content` text,
  `name` varchar(200) DEFAULT NULL,
  `url` varchar(800) DEFAULT NULL,
  `isParent` tinyint(4) DEFAULT NULL,
  `parentId` bigint(20) DEFAULT NULL,
  `isDeleted` tinyint(4) DEFAULT NULL,
  `isHomePage` tinyint(4) DEFAULT NULL,
  `homePageImageCode` text,
  `pagePosition` tinyint(4) DEFAULT NULL,
  `title` varchar(2000) DEFAULT NULL,
  `pageType` int(11) DEFAULT NULL,
  `rankOrder` bigint(10) DEFAULT '0',
  `show_navigation` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `supersitepage`
--

LOCK TABLES `supersitepage` WRITE;
/*!40000 ALTER TABLE `supersitepage` DISABLE KEYS */;
INSERT INTO `supersitepage` VALUES (1,'2015-08-31 17:57:06','<p>qew</p>\r\n','qwe','qew',1,0,0,0,NULL,0,'qew',0,0,0);
/*!40000 ALTER TABLE `supersitepage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `supersiterel`
--

DROP TABLE IF EXISTS `supersiterel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `supersiterel` (
  `id` bigint(20) NOT NULL DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `superSiteId` bigint(20) DEFAULT NULL,
  `siteId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `supersiterel`
--

LOCK TABLES `supersiterel` WRITE;
/*!40000 ALTER TABLE `supersiterel` DISABLE KEYS */;
INSERT INTO `supersiterel` VALUES (1362146005203,'2015-08-31 07:37:39',1362146005140,1362146005199),(1362146005210,'2015-08-31 07:38:23',1362146005140,1362146005206),(1362146005217,'2015-08-31 07:44:23',1362146005140,1362146005213),(1362146005224,'2015-08-31 07:47:15',1362146005149,1362146005220),(1362146005231,'2015-08-31 07:53:07',1362146005158,1362146005227),(1362146005238,'2015-08-31 07:53:46',1362146005158,1362146005234),(1362146005245,'2015-08-31 07:54:48',1362146005167,1362146005241),(1362146005252,'2015-08-31 07:56:38',1362146005176,1362146005248),(1362146005259,'2015-08-31 07:57:25',1362146005176,1362146005255);
/*!40000 ALTER TABLE `supersiterel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `supersitestat`
--

DROP TABLE IF EXISTS `supersitestat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `supersitestat` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `totalRaised` float DEFAULT NULL,
  `totalDonors` int(11) DEFAULT NULL,
  `totalEmailsSent` int(11) DEFAULT NULL,
  `totalPages` int(11) DEFAULT NULL,
  `totalSites` int(11) DEFAULT NULL,
  `memberId` bigint(20) DEFAULT NULL,
  `superSiteId` bigint(20) DEFAULT NULL,
  `topIndividuals` varchar(4000) DEFAULT NULL,
  `topTeams` varchar(4000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `supersitestat`
--

LOCK TABLES `supersitestat` WRITE;
/*!40000 ALTER TABLE `supersitestat` DISABLE KEYS */;
INSERT INTO `supersitestat` VALUES (1,'2015-09-01 13:15:25',0,0,0,0,0,1362146005137,1362145798072,'<null/>','<null/>'),(2,'2015-09-01 13:15:25',120,1,0,6,3,1362146005137,1362146005140,'<null/>','<null/>'),(3,'2015-09-01 13:15:25',100,2,0,0,1,1362146005137,1362146005149,'<null/>','<null/>'),(4,'2015-09-01 13:15:25',0,0,0,0,2,1362146005137,1362146005158,'<null/>','<null/>'),(5,'2015-09-01 13:15:25',0,0,0,0,1,1362146005137,1362146005167,'<null/>','<null/>'),(6,'2015-09-01 13:15:25',0,0,0,0,2,1362146005137,1362146005176,'<null/>','<null/>'),(7,'2015-09-01 13:15:25',0,0,0,0,0,1362146005137,1362146005185,'<null/>','<null/>');
/*!40000 ALTER TABLE `supersitestat` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `supersitetemplate`
--

DROP TABLE IF EXISTS `supersitetemplate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `supersitetemplate` (
  `id` bigint(20) unsigned NOT NULL,
  `created` datetime DEFAULT NULL,
  `superSiteId` bigint(20) DEFAULT NULL,
  `templateId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `supersitetemplate`
--

LOCK TABLES `supersitetemplate` WRITE;
/*!40000 ALTER TABLE `supersitetemplate` DISABLE KEYS */;
INSERT INTO `supersitetemplate` VALUES (1362145340569,'2014-07-21 17:27:53',1362145336569,1362145330577),(1362145341148,'2014-07-21 19:31:50',1362145337092,1362145328660),(1362145341149,'2014-07-21 19:31:50',1362145337092,1362145328765),(1362145341150,'2014-07-21 19:31:50',1362145337092,1362145328783),(1362145346694,'2014-07-22 20:36:02',1362145346088,1362145346115),(1362145346695,'2014-07-22 20:36:02',1362145346088,1362145346125),(1362145346696,'2014-07-22 20:36:02',1362145346088,1362145346640),(1362145357094,'2014-07-23 21:51:53',1362145329069,1362145353594),(1362145357095,'2014-07-23 21:51:53',1362145329069,1362145355596),(1362145357096,'2014-07-23 21:51:53',1362145329069,1362145357076),(1362145364071,'2014-07-24 18:34:16',1362145364069,1362145361217),(1362145364072,'2014-07-24 18:34:16',1362145364069,1362145361223),(1362145364073,'2014-07-24 18:34:16',1362145364069,1362145361240),(1362145376103,'2014-07-28 14:35:33',1362145376100,1362145376093),(1362145390165,'2014-07-31 16:32:06',1362145372135,1362145372099),(1362145390166,'2014-07-31 16:32:06',1362145372135,1362145372121),(1362145390167,'2014-07-31 16:32:06',1362145372135,1362145383077),(1362145390168,'2014-07-31 16:32:06',1362145372135,1362145383116),(1362145390169,'2014-07-31 16:32:06',1362145372135,1362145383122),(1362145390170,'2014-07-31 16:32:06',1362145372135,1362145383162),(1362145390171,'2014-07-31 16:32:06',1362145372135,1362145388627),(1362145390172,'2014-07-31 16:32:06',1362145372135,1362145389583),(1362145416073,'2014-08-08 00:21:41',1362145414590,1362145379301),(1362145500104,'2014-09-10 20:47:30',1362145337131,1362145405603),(1362145500105,'2014-09-10 20:47:30',1362145337131,1362145406075),(1362145500106,'2014-09-10 20:47:30',1362145337131,1362145406082),(1362145539576,'2014-10-02 03:00:19',1362145338095,1362145539575),(1362145638061,'2014-11-17 13:02:23',1362145336571,1362145361217),(1362145638585,'2014-11-17 14:57:38',1362145375077,1362145361217),(1362145638586,'2014-11-17 14:57:38',1362145375077,1362145361223),(1362145638587,'2014-11-17 14:57:38',1362145375077,1362145361240),(1362145638588,'2014-11-17 14:57:38',1362145375077,1362145380115),(1362145706596,'2015-01-05 05:54:04',1362145603945,1362145379613),(1362145755940,'2015-03-12 02:26:21',1362145747706,1362145361217),(1362145769695,'2015-03-23 14:31:56',1362145765612,1362145769694),(1362145771744,'2015-03-26 04:35:28',1362145765140,1362145404153),(1362145776167,'2015-04-02 05:19:37',1362145711069,1362145404616),(1362145776168,'2015-04-02 05:19:37',1362145711069,1362145416087),(1362145811602,'2015-05-06 19:48:02',1362145803616,1362145811594),(1362145837658,'2015-06-12 17:50:06',1362145827328,1362145828083),(1362145837834,'2015-06-17 10:44:51',1362145706618,1362145361217),(1362145838104,'2015-06-17 12:32:11',1362145830069,1362145361217),(1362145838105,'2015-06-17 12:32:11',1362145830069,1362145838100),(1362145838662,'2015-06-17 15:07:04',1362145827315,1362145828075),(1362145838663,'2015-06-17 15:07:04',1362145827315,1362145838576),(1362145839186,'2015-06-18 20:21:49',1362145824639,1362145839178),(1362145844661,'2015-06-30 22:44:19',1362145844641,1362145811594);
/*!40000 ALTER TABLE `supersitetemplate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `supersitetheme`
--

DROP TABLE IF EXISTS `supersitetheme`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `supersitetheme` (
  `id` bigint(20) unsigned NOT NULL,
  `created` datetime DEFAULT NULL,
  `superSiteId` bigint(20) DEFAULT NULL,
  `themeId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `supersitetheme`
--

LOCK TABLES `supersitetheme` WRITE;
/*!40000 ALTER TABLE `supersitetheme` DISABLE KEYS */;
INSERT INTO `supersitetheme` VALUES (1362145314085,'2014-06-03 13:24:35',1362145314077,1362145314055),(1362145315088,'2014-06-13 15:14:32',1362145314569,1362145315079),(1362145317104,'2014-06-17 13:05:53',1362145317102,1362145315079),(1362145318615,'2014-06-17 15:42:48',1362145318098,1362145318569),(1362145325828,'2014-07-07 18:46:15',1362145325820,1362145315079),(1362145325829,'2014-07-07 19:15:58',1362145325820,1362145315079),(1362145325830,'2014-07-07 19:15:58',1362145325820,1362145318569),(1362145325831,'2014-07-07 19:15:58',1362145325820,1362145322681),(1362145331082,'2014-07-17 16:16:38',1362145330569,1362145328610),(1362145338571,'2014-07-19 17:45:07',1362145315090,1362145315079),(1362145338572,'2014-07-19 17:45:07',1362145315090,1362145318569),(1362145340079,'2014-07-21 16:44:26',1362145323076,1362145315079),(1362145340080,'2014-07-21 16:44:26',1362145323076,1362145328699),(1362145375085,'2014-07-28 13:11:35',1362145375077,1362145315079),(1362145386622,'2014-07-30 11:03:08',1362145361571,1362145318569),(1362145386623,'2014-07-30 11:03:08',1362145361571,1362145322681),(1362145386624,'2014-07-30 11:03:08',1362145361571,1362145328730),(1362145416070,'2014-08-08 00:14:08',1362145414590,1362145315079),(1362145416071,'2014-08-08 00:14:08',1362145414590,1362145318569),(1362145416072,'2014-08-08 00:14:08',1362145414590,1362145322681),(1362145496650,'2014-09-09 19:24:39',1362145337131,1362145328724),(1362145496651,'2014-09-09 19:24:39',1362145337131,1362145328726),(1362145556602,'2014-10-07 12:25:33',1362145550199,1362145315079),(1362145556603,'2014-10-07 12:25:33',1362145550199,1362145318569),(1362145556604,'2014-10-07 12:25:33',1362145550199,1362145322681),(1362145556605,'2014-10-07 12:25:33',1362145550199,1362145328610),(1362145556606,'2014-10-07 12:25:33',1362145550199,1362145328699),(1362145556607,'2014-10-07 12:25:33',1362145550199,1362145328724),(1362145556608,'2014-10-07 12:25:33',1362145550199,1362145328726),(1362145556618,'2014-10-07 12:43:51',1362145388685,1362145318569),(1362145556619,'2014-10-07 12:43:51',1362145388685,1362145322681),(1362145691343,'2014-12-03 20:04:30',1362145691309,1362145318569),(1362145691344,'2014-12-03 20:04:30',1362145691309,1362145328699),(1362145691345,'2014-12-03 20:04:30',1362145691309,1362145328724),(1362145706594,'2015-01-05 05:29:37',1362145694130,1362145328732),(1362145706595,'2015-01-05 05:53:06',1362145603945,1362145318569),(1362145706637,'2015-01-05 09:40:02',1362145706618,1362145318569),(1362145706638,'2015-01-05 09:40:02',1362145706618,1362145328726),(1362145707679,'2015-01-06 07:06:07',1362145707639,1362145322681),(1362145707739,'2015-01-06 09:22:07',1362145707603,1362145322681),(1362145814569,'2015-05-14 06:41:52',1362145803577,1362145315079),(1362145814570,'2015-05-14 06:41:52',1362145803577,1362145525586),(1362145814571,'2015-05-14 06:41:52',1362145803577,1362145525588),(1362145817652,'2015-05-18 13:09:33',1362145816584,1362145328699),(1362145817653,'2015-05-18 13:09:33',1362145816584,1362145328724),(1362145817654,'2015-05-18 13:09:33',1362145816584,1362145328726),(1362145827324,'2015-06-01 14:13:58',1362145827306,1362145328699),(1362145827325,'2015-06-01 14:13:58',1362145827306,1362145328724),(1362145827326,'2015-06-01 14:13:58',1362145827306,1362145328726),(1362145827327,'2015-06-01 14:13:58',1362145827306,1362145328732),(1362145830647,'2015-06-03 15:11:47',1362145827315,1362145322681),(1362145830650,'2015-06-03 17:19:31',1362145830610,1362145328724),(1362145840583,'2015-06-24 06:41:11',1362145830069,1362145328734),(1362145843594,'2015-06-26 13:17:44',1362145835586,1362145322681),(1362145844660,'2015-06-30 22:41:55',1362145844641,1362145318569);
/*!40000 ALTER TABLE `supersitetheme` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sysid`
--

DROP TABLE IF EXISTS `sysid`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sysid` (
  `id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sysid`
--

LOCK TABLES `sysid` WRITE;
/*!40000 ALTER TABLE `sysid` DISABLE KEYS */;
INSERT INTO `sysid` VALUES (1362146006068);
/*!40000 ALTER TABLE `sysid` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tags`
--

DROP TABLE IF EXISTS `tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tags` (
  `id` bigint(20) unsigned NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tags`
--

LOCK TABLES `tags` WRITE;
/*!40000 ALTER TABLE `tags` DISABLE KEYS */;
/*!40000 ALTER TABLE `tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `team`
--

DROP TABLE IF EXISTS `team`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `team` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `memberId` bigint(20) DEFAULT NULL,
  `campaignId` bigint(20) DEFAULT NULL,
  `masterCampaignId` bigint(20) DEFAULT NULL,
  `name` varchar(85) DEFAULT NULL,
  `honoring` varchar(85) DEFAULT NULL,
  `inMemoriam` varchar(85) DEFAULT NULL,
  `rMotherProperty` text,
  `masterTeamId` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `memberId` (`memberId`),
  KEY `campaignId` (`campaignId`),
  KEY `masterCampaignId` (`masterCampaignId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `team`
--

LOCK TABLES `team` WRITE;
/*!40000 ALTER TABLE `team` DISABLE KEYS */;
/*!40000 ALTER TABLE `team` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teammember`
--

DROP TABLE IF EXISTS `teammember`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teammember` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `memberId` bigint(20) DEFAULT NULL,
  `teamId` int(11) DEFAULT NULL,
  `walkMemberId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `memberId` (`memberId`),
  KEY `teamId` (`teamId`),
  KEY `walkMemberId` (`walkMemberId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teammember`
--

LOCK TABLES `teammember` WRITE;
/*!40000 ALTER TABLE `teammember` DISABLE KEYS */;
/*!40000 ALTER TABLE `teammember` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tempmailblast`
--

DROP TABLE IF EXISTS `tempmailblast`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tempmailblast` (
  `id` int(10) unsigned NOT NULL DEFAULT '0',
  `memberId` bigint(20) DEFAULT NULL,
  `objectType` tinyint(3) unsigned NOT NULL,
  `objectId` bigint(20) DEFAULT NULL,
  `blastType` tinyint(3) unsigned NOT NULL,
  `created` datetime NOT NULL,
  `rImpactJobId` int(10) unsigned NOT NULL,
  `messageId` int(10) unsigned NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tempmailblast`
--

LOCK TABLES `tempmailblast` WRITE;
/*!40000 ALTER TABLE `tempmailblast` DISABLE KEYS */;
/*!40000 ALTER TABLE `tempmailblast` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tempmailblastmember`
--

DROP TABLE IF EXISTS `tempmailblastmember`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tempmailblastmember` (
  `id` int(10) unsigned NOT NULL DEFAULT '0',
  `blastId` int(10) unsigned NOT NULL,
  `memberId` bigint(20) DEFAULT NULL,
  `created` datetime NOT NULL,
  `lastNag` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tempmailblastmember`
--

LOCK TABLES `tempmailblastmember` WRITE;
/*!40000 ALTER TABLE `tempmailblastmember` DISABLE KEYS */;
/*!40000 ALTER TABLE `tempmailblastmember` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tempsiteblobs`
--

DROP TABLE IF EXISTS `tempsiteblobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tempsiteblobs` (
  `siteId` bigint(20) NOT NULL DEFAULT '0',
  `siterotatorimages` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tempsiteblobs`
--

LOCK TABLES `tempsiteblobs` WRITE;
/*!40000 ALTER TABLE `tempsiteblobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `tempsiteblobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `topcampaign`
--

DROP TABLE IF EXISTS `topcampaign`;
/*!50001 DROP VIEW IF EXISTS `topcampaign`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `topcampaign` (
  `firstname` tinyint NOT NULL,
  `imageBlobId` tinyint NOT NULL,
  `campaignId` tinyint NOT NULL,
  `type` tinyint NOT NULL,
  `totalRaised` tinyint NOT NULL,
  `sitememberId` tinyint NOT NULL,
  `siteId` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `transaction`
--

DROP TABLE IF EXISTS `transaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transaction` (
  `id` bigint(20) NOT NULL DEFAULT '0',
  `memberId` bigint(20) DEFAULT NULL,
  `charityId` bigint(20) DEFAULT NULL,
  `creditCardId` bigint(20) DEFAULT NULL,
  `pollId` int(11) DEFAULT NULL,
  `orderId` varchar(50) DEFAULT NULL,
  `campaignId` bigint(20) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `value` float DEFAULT NULL,
  `authCode` varchar(40) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `showDonation` tinyint(4) DEFAULT NULL,
  `referenceNum` varchar(50) DEFAULT NULL,
  `transactionType` tinyint(4) DEFAULT NULL,
  `memo` varchar(1000) DEFAULT NULL,
  `pollResponseId` int(11) DEFAULT NULL,
  `notifyEmail` varchar(100) DEFAULT NULL,
  `notifyMemo` varchar(1000) DEFAULT NULL,
  `taxDeduct` float DEFAULT NULL,
  `fees` float DEFAULT NULL,
  `flatfee` float DEFAULT NULL,
  `refundTime` datetime DEFAULT NULL,
  `errorMessage` varchar(250) DEFAULT NULL,
  `note` varchar(1000) DEFAULT NULL,
  `socialValue` float DEFAULT NULL,
  `invoiceTime` datetime DEFAULT NULL,
  `noPurchaseDate` datetime DEFAULT NULL,
  `siteMemberId` bigint(20) DEFAULT NULL,
  `siteId` bigint(20) unsigned DEFAULT NULL,
  `supersiteId` bigint(20) unsigned DEFAULT NULL,
  `txnNumber` varchar(20) DEFAULT NULL,
  `billingcycle` tinyint(4) DEFAULT NULL,
  `promoCodeId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `memberId` (`memberId`),
  KEY `charityId` (`charityId`),
  KEY `creditCardId` (`creditCardId`),
  KEY `campaignId` (`campaignId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transaction`
--

LOCK TABLES `transaction` WRITE;
/*!40000 ALTER TABLE `transaction` DISABLE KEYS */;
INSERT INTO `transaction` VALUES (1362146005767,1362146005764,1362146005204,1362146005766,NULL,NULL,1362146005771,'2015-09-01 12:46:00',120,'ch_6u2EknxAwkBsNJ',2,0,NULL,13,NULL,NULL,NULL,NULL,0,342,0,NULL,NULL,NULL,0,NULL,NULL,1362146005765,1362146005206,0,NULL,0,0),(1362146005834,1362146005831,1362146005218,1362146005833,NULL,NULL,1362146005219,'2015-09-01 12:54:05',40,'ch_6u2Meq0IFxrFfd',2,0,NULL,13,NULL,NULL,NULL,NULL,0,94,0,NULL,NULL,NULL,0,NULL,NULL,1362146005832,1362146005220,0,NULL,0,0),(1362146005842,1362146005839,1362146005218,1362146005841,NULL,NULL,1362146005219,'2015-09-01 12:55:41',60,'ch_6u2NVRkRaCT2T7',2,0,NULL,13,NULL,NULL,NULL,NULL,0,156,0,NULL,NULL,NULL,0,NULL,NULL,1362146005840,1362146005220,0,NULL,0,0);
/*!40000 ALTER TABLE `transaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transactionaudit`
--

DROP TABLE IF EXISTS `transactionaudit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transactionaudit` (
  `id` bigint(20) unsigned NOT NULL,
  `created` datetime DEFAULT NULL,
  `memberId` bigint(20) DEFAULT NULL,
  `transactionId` bigint(20) DEFAULT NULL,
  `status` varchar(2000) DEFAULT NULL,
  `auditStatus` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transactionaudit`
--

LOCK TABLES `transactionaudit` WRITE;
/*!40000 ALTER TABLE `transactionaudit` DISABLE KEYS */;
/*!40000 ALTER TABLE `transactionaudit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transactionitem`
--

DROP TABLE IF EXISTS `transactionitem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transactionitem` (
  `id` bigint(20) NOT NULL DEFAULT '0',
  `transactionId` bigint(20) DEFAULT NULL,
  `campaignId` bigint(20) DEFAULT NULL,
  `value` float DEFAULT NULL,
  `taxDeduct` float DEFAULT NULL,
  `name` varchar(500) DEFAULT NULL,
  `campaignItemId` bigint(20) DEFAULT NULL,
  `quantity` tinyint(4) DEFAULT NULL,
  `socialValue` float DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `transactionId` (`transactionId`),
  KEY `campaignItemId` (`campaignItemId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transactionitem`
--

LOCK TABLES `transactionitem` WRITE;
/*!40000 ALTER TABLE `transactionitem` DISABLE KEYS */;
INSERT INTO `transactionitem` VALUES (1362146005769,1362146005767,1362146005771,120,0,'Standard Donation',0,1,0),(1362146005836,1362146005834,1362146005219,40,0,'Standard Donation',0,1,0),(1362146005844,1362146005842,1362146005219,60,0,'Standard Donation',0,1,0);
/*!40000 ALTER TABLE `transactionitem` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transactionqueue`
--

DROP TABLE IF EXISTS `transactionqueue`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transactionqueue` (
  `id` bigint(20) NOT NULL,
  `created` datetime DEFAULT NULL,
  `site_id` bigint(20) DEFAULT NULL,
  `crm_type` tinyint(4) DEFAULT NULL,
  `transaction_id` bigint(20) DEFAULT NULL,
  `pushed` datetime DEFAULT NULL,
  `synced` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transactionqueue`
--

LOCK TABLES `transactionqueue` WRITE;
/*!40000 ALTER TABLE `transactionqueue` DISABLE KEYS */;
/*!40000 ALTER TABLE `transactionqueue` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transactionsnapshot`
--

DROP TABLE IF EXISTS `transactionsnapshot`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transactionsnapshot` (
  `id` bigint(20) NOT NULL,
  `objectId` bigint(20) DEFAULT NULL,
  `firstname` varchar(40) DEFAULT NULL,
  `lastname` varchar(40) DEFAULT NULL,
  `snapshot` varchar(1000) DEFAULT NULL,
  `type` tinyint(4) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `transactionId` (`objectId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transactionsnapshot`
--

LOCK TABLES `transactionsnapshot` WRITE;
/*!40000 ALTER TABLE `transactionsnapshot` DISABLE KEYS */;
INSERT INTO `transactionsnapshot` VALUES (1362146005768,1362146005767,'Ron','Red','<org.karma.common.Snapshot>\n  <email>ron@red.qwe</email>\n  <address>qwe</address>\n  <address2></address2>\n  <city>dsa</city>\n  <state>XX</state>\n  <countryCode>US</countryCode>\n  <postalCode>123</postalCode>\n  <billingCycle>0</billingCycle>\n</org.karma.common.Snapshot>',2,'2015-09-01 12:46:26'),(1362146005835,1362146005834,'qwe','qwe','<org.karma.common.Snapshot>\n  <email>qwe@qwe.qwea</email>\n  <address>qwe</address>\n  <address2></address2>\n  <city>qwe</city>\n  <state>XX</state>\n  <countryCode>US</countryCode>\n  <postalCode>321</postalCode>\n  <billingCycle>0</billingCycle>\n</org.karma.common.Snapshot>',2,'2015-09-01 12:55:06'),(1362146005843,1362146005842,'John','Green','<org.karma.common.Snapshot>\n  <email>john@qwe.qwe</email>\n  <address>asdsvb</address>\n  <address2></address2>\n  <city>qwecfdg</city>\n  <state>XX</state>\n  <countryCode>US</countryCode>\n  <postalCode>654</postalCode>\n  <billingCycle>0</billingCycle>\n</org.karma.common.Snapshot>',2,'2015-09-01 12:56:13');
/*!40000 ALTER TABLE `transactionsnapshot` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `urlregistry`
--

DROP TABLE IF EXISTS `urlregistry`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `urlregistry` (
  `id` bigint(20) unsigned NOT NULL,
  `created` datetime DEFAULT NULL,
  `alias` varchar(150) NOT NULL,
  `resource` varchar(150) DEFAULT NULL,
  `resourceId` bigint(20) NOT NULL,
  `resourcetype` tinyint(4) NOT NULL,
  `status` int(11) DEFAULT NULL,
  `multiple` tinyint(4) DEFAULT NULL,
  `deleted` datetime DEFAULT NULL,
  `external` tinyint(4) DEFAULT NULL,
  `subdomain` varchar(150) NOT NULL,
  `webrootid` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `urlregistry`
--

LOCK TABLES `urlregistry` WRITE;
/*!40000 ALTER TABLE `urlregistry` DISABLE KEYS */;
INSERT INTO `urlregistry` VALUES (1362145798076,'2015-08-31 17:56:35','/','/site/displaySite.do?siteIdCode=S505VHO1',1362145798075,1,200,0,NULL,0,'One-more-test',0),(1362145798077,'2015-08-31 17:56:35','/one-more-test','/campaign/display/profile.do?campaignId=1362145798074',1362145798074,0,200,0,NULL,0,'One-more-test',0),(1362146005144,'2015-08-31 07:23:04','/','/site/displaySite.do?siteIdCode=7KL3U8RB',1362146005143,1,200,0,NULL,0,'FirstSupersite',0),(1362146005145,'2015-08-31 07:23:04','/firstsupersite','/campaign/display/profile.do?campaignId=1362146005142',1362146005142,0,200,0,NULL,0,'FirstSupersite',0),(1362146005153,'2015-08-31 07:23:22','/','/site/displaySite.do?siteIdCode=5NBTNIEB',1362146005152,1,200,0,NULL,0,'SecondSupersite',0),(1362146005154,'2015-08-31 07:23:22','/secondsupersite','/campaign/display/profile.do?campaignId=1362146005151',1362146005151,0,200,0,NULL,0,'SecondSupersite',0),(1362146005162,'2015-08-31 07:23:41','/','/site/displaySite.do?siteIdCode=NHLJVRXF',1362146005161,1,200,0,NULL,0,'ThirdSupersite',0),(1362146005163,'2015-08-31 07:23:42','/thirdsupersite','/campaign/display/profile.do?campaignId=1362146005160',1362146005160,0,200,0,NULL,0,'ThirdSupersite',0),(1362146005171,'2015-08-31 07:24:08','/','/site/displaySite.do?siteIdCode=36TBK55M',1362146005170,1,200,0,NULL,0,'TestSupersite',0),(1362146005172,'2015-08-31 07:24:08','/testsupersite','/campaign/display/profile.do?campaignId=1362146005169',1362146005169,0,200,0,NULL,0,'TestSupersite',0),(1362146005180,'2015-08-31 07:27:42','/','/site/displaySite.do?siteIdCode=5FNVJPIV',1362146005179,1,200,0,NULL,0,'SoftwareSS',0),(1362146005181,'2015-08-31 07:27:42','/softwaress','/campaign/display/profile.do?campaignId=1362146005178',1362146005178,0,200,0,NULL,0,'SoftwareSS',0),(1362146005189,'2015-08-31 07:29:55','/','/site/displaySite.do?siteIdCode=CJ254SU5',1362146005188,1,200,0,NULL,0,'TestSsName',0),(1362146005190,'2015-08-31 07:29:55','/testssname','/campaign/display/profile.do?campaignId=1362146005187',1362146005187,0,200,0,NULL,0,'TestSsName',0),(1362146005200,'2015-08-31 07:37:39','/','/site/displaySite.do?siteIdCode=7N03E4NI',1362146005199,1,200,0,NULL,0,'firstevent',0),(1362146005201,'2015-08-31 07:37:39','/firstevent','/campaign/display/profile.do?campaignId=1362146005198',1362146005198,0,200,0,NULL,0,'firstevent',0),(1362146005207,'2015-08-31 07:38:23','/','/site/displaySite.do?siteIdCode=BGNQ98RG',1362146005206,1,200,0,NULL,0,'anotheroneevenet',0),(1362146005208,'2015-08-31 07:38:23','/anotheroneevenet','/campaign/display/profile.do?campaignId=1362146005205',1362146005205,0,200,0,NULL,0,'anotheroneevenet',0),(1362146005214,'2015-08-31 07:44:23','/','/site/displaySite.do?siteIdCode=VHCA3ZJ7',1362146005213,1,200,0,NULL,0,'event',0),(1362146005215,'2015-08-31 07:44:23','/event','/campaign/display/profile.do?campaignId=1362146005212',1362146005212,0,200,0,NULL,0,'event',0),(1362146005221,'2015-08-31 07:47:15','/','/site/displaySite.do?siteIdCode=6O43Z9X0',1362146005220,1,200,0,NULL,0,'uniqueevent',0),(1362146005222,'2015-08-31 07:47:15','/unique-event','/campaign/display/profile.do?campaignId=1362146005219',1362146005219,0,200,0,NULL,0,'uniqueevent',0),(1362146005228,'2015-08-31 07:53:07','/','/site/displaySite.do?siteIdCode=DKWQDVGQ',1362146005227,1,200,0,NULL,0,'annaevent',0),(1362146005229,'2015-08-31 07:53:07','/anna-event','/campaign/display/profile.do?campaignId=1362146005226',1362146005226,0,200,0,NULL,0,'annaevent',0),(1362146005235,'2015-08-31 07:53:46','/','/site/displaySite.do?siteIdCode=TXELRE9P',1362146005234,1,200,0,NULL,0,'qweevent',0),(1362146005236,'2015-08-31 07:53:46','/qwe-event','/campaign/display/profile.do?campaignId=1362146005233',1362146005233,0,200,0,NULL,0,'qweevent',0),(1362146005242,'2015-08-31 07:54:48','/','/site/displaySite.do?siteIdCode=6A98ON90',1362146005241,1,200,0,NULL,0,'event',0),(1362146005243,'2015-08-31 07:54:48','/eventmicro','/campaign/display/profile.do?campaignId=1362146005240',1362146005240,0,200,0,NULL,0,'event',0),(1362146005249,'2015-08-31 07:56:37','/','/site/displaySite.do?siteIdCode=X9A3RJZ9',1362146005248,1,200,0,NULL,0,'firstone',0),(1362146005250,'2015-08-31 07:56:37','/firstone','/campaign/display/profile.do?campaignId=1362146005247',1362146005247,0,200,0,NULL,0,'firstone',0),(1362146005256,'2015-08-31 07:57:25','/','/site/displaySite.do?siteIdCode=0138Z2VF',1362146005255,1,200,0,NULL,0,'secondone',0),(1362146005257,'2015-08-31 07:57:25','/secondone','/campaign/display/profile.do?campaignId=1362146005254',1362146005254,0,200,0,NULL,0,'secondone',0),(1362146005775,'2015-09-01 12:46:45','/ronred','/campaign/display/profile.do?campaignId=1362146005771',1362146005771,0,200,0,NULL,0,'anotheroneevenet',0),(1362146005784,'2015-09-01 12:47:23','/usertfirst','/campaign/display/profile.do?campaignId=1362146005780',1362146005780,0,200,0,NULL,0,'firstevent',0),(1362146005797,'2015-09-01 12:47:57','/teamowner1','/campaign/display/profile.do?campaignId=1362146005796',1362146005796,0,200,0,NULL,0,'firstevent',0),(1362146005800,'2015-09-01 12:47:57','/teamowner','/campaign/display/profile.do?campaignId=1362146005789',1362146005789,0,200,0,NULL,0,'firstevent',0),(1362146005810,'2015-09-01 12:48:37','/usersecond','/campaign/display/profile.do?campaignId=1362146005803',1362146005803,0,200,0,NULL,0,'firstevent',0),(1362146005816,'2015-09-01 12:48:59','/testthird','/campaign/display/profile.do?campaignId=1362146005812',1362146005812,0,200,0,NULL,0,'firstevent',0);
/*!40000 ALTER TABLE `urlregistry` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vanityurl`
--

DROP TABLE IF EXISTS `vanityurl`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vanityurl` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `objectType` int(11) DEFAULT NULL,
  `objectId` int(11) DEFAULT NULL,
  `keyCode` varchar(100) DEFAULT NULL,
  `url` varchar(800) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vanityurl`
--

LOCK TABLES `vanityurl` WRITE;
/*!40000 ALTER TABLE `vanityurl` DISABLE KEYS */;
/*!40000 ALTER TABLE `vanityurl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `walkmember`
--

DROP TABLE IF EXISTS `walkmember`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `walkmember` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `memberId` bigint(20) DEFAULT NULL,
  `campaignId` bigint(20) DEFAULT NULL,
  `optionNum` tinyint(4) DEFAULT NULL,
  `tShirtSize` varchar(5) DEFAULT NULL,
  `company` varchar(85) DEFAULT NULL,
  `paceMin` tinyint(4) DEFAULT NULL,
  `paceSec` tinyint(4) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `city` varchar(75) DEFAULT NULL,
  `state` varchar(25) DEFAULT NULL,
  `country` tinyint(4) DEFAULT NULL,
  `zip` varchar(10) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `rrNumber` varchar(15) DEFAULT NULL,
  `rrTeam` varchar(15) DEFAULT NULL,
  `masterCampaignId` bigint(20) DEFAULT NULL,
  `optionGroup` tinyint(4) DEFAULT NULL,
  `doesCompanyMatch` tinyint(4) DEFAULT NULL,
  `championChip` varchar(7) DEFAULT NULL,
  `donationId` int(11) DEFAULT NULL,
  `walkFamilyMembers` text,
  `volunteer` varchar(6) DEFAULT NULL,
  `password` varchar(100) NOT NULL,
  `runwalkType` tinyint(4) DEFAULT NULL,
  `firstName` varchar(40) NOT NULL,
  `lastName` varchar(40) NOT NULL,
  `dob` date DEFAULT NULL,
  `gender` int(11) DEFAULT NULL,
  `walkmemberSettings` text,
  PRIMARY KEY (`id`),
  KEY `masterCampaignId` (`masterCampaignId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `walkmember`
--

LOCK TABLES `walkmember` WRITE;
/*!40000 ALTER TABLE `walkmember` DISABLE KEYS */;
/*!40000 ALTER TABLE `walkmember` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wcm`
--

DROP TABLE IF EXISTS `wcm`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wcm` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `section` varchar(50) DEFAULT NULL,
  `value` text,
  `site` varchar(50) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `active` tinyint(4) DEFAULT NULL,
  `masterCampaignId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wcm`
--

LOCK TABLES `wcm` WRITE;
/*!40000 ALTER TABLE `wcm` DISABLE KEYS */;
/*!40000 ALTER TABLE `wcm` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `webroot`
--

DROP TABLE IF EXISTS `webroot`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `webroot` (
  `id` bigint(20) unsigned NOT NULL,
  `created` datetime DEFAULT NULL,
  `domain` varchar(150) NOT NULL,
  `prefix` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `webroot`
--

LOCK TABLES `webroot` WRITE;
/*!40000 ALTER TABLE `webroot` DISABLE KEYS */;
/*!40000 ALTER TABLE `webroot` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `webrootreference`
--

DROP TABLE IF EXISTS `webrootreference`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `webrootreference` (
  `id` bigint(20) NOT NULL,
  `web_root_id` bigint(20) NOT NULL,
  `object_id` bigint(20) NOT NULL,
  `object_type` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `webrootreference`
--

LOCK TABLES `webrootreference` WRITE;
/*!40000 ALTER TABLE `webrootreference` DISABLE KEYS */;
/*!40000 ALTER TABLE `webrootreference` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yscmember`
--

DROP TABLE IF EXISTS `yscmember`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yscmember` (
  `id` int(11) NOT NULL DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `memberId` bigint(20) DEFAULT NULL,
  `firstName` varchar(40) DEFAULT NULL,
  `lastName` varchar(40) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yscmember`
--

LOCK TABLES `yscmember` WRITE;
/*!40000 ALTER TABLE `yscmember` DISABLE KEYS */;
/*!40000 ALTER TABLE `yscmember` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `zipinfo`
--

DROP TABLE IF EXISTS `zipinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zipinfo` (
  `zip` varchar(6) NOT NULL DEFAULT '',
  `city` varchar(40) DEFAULT NULL,
  `state` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`zip`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zipinfo`
--

LOCK TABLES `zipinfo` WRITE;
/*!40000 ALTER TABLE `zipinfo` DISABLE KEYS */;
/*!40000 ALTER TABLE `zipinfo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'markslist'
--
/*!50003 DROP FUNCTION IF EXISTS `tagsbymedia` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `tagsbymedia`(mId bigint(5) unsigned) RETURNS varchar(300) CHARSET utf8
    READS SQL DATA
BEGIN
  DECLARE v_name varchar(100);
  DECLARE done INT DEFAULT FALSE;
  DECLARE tagnames varchar(300) ;
  DECLARE csr CURSOR FOR 
   select t.name from tags t, mediatag mt where t.id=mt.tagId and mt.mediaId=mId;
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

  SET tagnames = '';
  OPEN csr;
  read_loop: LOOP
    FETCH csr INTO v_name;
    IF done THEN
      LEAVE read_loop;
    END IF;

    SET tagnames = concat(v_name, ",",tagnames);
  END LOOP;
  CLOSE csr;

  SET tagnames=TRIM(TRAILING ',' FROM tagnames);

  RETURN tagnames;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Final view structure for view `campaignklogcount`
--

/*!50001 DROP TABLE IF EXISTS `campaignklogcount`*/;
/*!50001 DROP VIEW IF EXISTS `campaignklogcount`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `campaignklogcount` AS select `campaign`.`id` AS `id`,count(`klog`.`id`) AS `count` from (`campaign` join `klog`) where ((`campaign`.`id` = `klog`.`campaignId`) and isnull(`klog`.`deleteTime`)) group by `campaign`.`id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `campaignnumberdonations`
--

/*!50001 DROP TABLE IF EXISTS `campaignnumberdonations`*/;
/*!50001 DROP VIEW IF EXISTS `campaignnumberdonations`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `campaignnumberdonations` AS select `campaign`.`id` AS `id`,count(0) AS `count` from (`campaign` join `transaction`) where ((`transaction`.`campaignId` = `campaign`.`id`) and isnull(`transaction`.`refundTime`) and (`transaction`.`authCode` is not null)) group by `campaign`.`id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `campaignnumberofflinedonations`
--

/*!50001 DROP TABLE IF EXISTS `campaignnumberofflinedonations`*/;
/*!50001 DROP VIEW IF EXISTS `campaignnumberofflinedonations`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `campaignnumberofflinedonations` AS select `campaign`.`id` AS `id`,count(`offlinedonation`.`value`) AS `count` from (`campaign` join `offlinedonation`) where ((`offlinedonation`.`campaignId` = `campaign`.`id`) and (isnull(`offlinedonation`.`memberId`) or (`offlinedonation`.`value` = 0))) group by `campaign`.`id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `campaigntotaldonations`
--

/*!50001 DROP TABLE IF EXISTS `campaigntotaldonations`*/;
/*!50001 DROP VIEW IF EXISTS `campaigntotaldonations`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `campaigntotaldonations` AS select `campaign`.`id` AS `id`,sum(`transaction`.`value`) AS `value` from (`campaign` join `transaction`) where ((`transaction`.`campaignId` = `campaign`.`id`) and isnull(`transaction`.`refundTime`) and (`transaction`.`authCode` is not null)) group by `campaign`.`id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `campaigntotalofflinedonations`
--

/*!50001 DROP TABLE IF EXISTS `campaigntotalofflinedonations`*/;
/*!50001 DROP VIEW IF EXISTS `campaigntotalofflinedonations`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `campaigntotalofflinedonations` AS select `campaign`.`id` AS `id`,sum(`offlinedonation`.`value`) AS `value` from (`campaign` join `offlinedonation`) where ((`offlinedonation`.`campaignId` = `campaign`.`id`) and isnull(`offlinedonation`.`refundTime`) and isnull(`offlinedonation`.`memberId`)) group by `campaign`.`id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `expandedsite`
--

/*!50001 DROP TABLE IF EXISTS `expandedsite`*/;
/*!50001 DROP VIEW IF EXISTS `expandedsite`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `expandedsite` AS select `site`.`id` AS `id`,`site`.`idCode` AS `idCode`,`site`.`created` AS `created`,`site`.`siteType` AS `siteType`,`site`.`name` AS `name`,`site`.`url` AS `url`,`site`.`mastheadCode` AS `mastheadCode`,`site`.`masterCampaignId` AS `masterCampaignId`,`site`.`adminId` AS `adminId`,`site`.`isDeleted` AS `isDeleted`,`site`.`templateType` AS `templateType`,`site`.`waiverText` AS `waiverText`,`site`.`deleteTime` AS `deleteTime`,`site`.`mobileMastheadCode` AS `mobileMastheadCode`,`site`.`dateOfEvent` AS `dateOfEvent`,`site`.`location` AS `location`,`site`.`sitebannerblobid` AS `sitebannerblobid`,`site`.`address_1` AS `address_1`,`site`.`address_2` AS `address_2`,`site`.`city` AS `city`,`site`.`state` AS `state`,`site`.`zip` AS `zip`,`site`.`contact_phone` AS `contact_phone`,`site`.`dashbaord_widgets_list` AS `dashbaord_widgets_list`,`site`.`subdomain` AS `subdomain`,`site`.`thermometerstyle` AS `thermometerstyle`,`site`.`alertEmailAddresses` AS `alertEmailAddresses`,`site`.`themeId` AS `themeId`,`site`.`isTemplate` AS `isTemplate`,`site`.`overrideOrder` AS `overrideOrder`,`site`.`event_time` AS `event_time`,`site`.`settings` AS `settings`,`site`.`mediaId` AS `mediaId`,`site`.`presetdonations` AS `presetdonations`,`site`.`recurringoptions` AS `recurringoptions`,`site`.`folder` AS `folder`,`sitestat`.`totalRaised` AS `totalRaised`,`sitestat`.`totalDonors` AS `totalDonors` from (`site` left join `sitestat` on((`site`.`id` = `sitestat`.`siteId`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `mastercampaignnumberdonations`
--

/*!50001 DROP TABLE IF EXISTS `mastercampaignnumberdonations`*/;
/*!50001 DROP VIEW IF EXISTS `mastercampaignnumberdonations`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `mastercampaignnumberdonations` AS select `campaign`.`id` AS `id`,count(`transaction`.`value`) AS `value` from (`campaign` join `transaction`) where ((`campaign`.`masterCampaignId` = `transaction`.`campaignId`) and (`transaction`.`campaignId` = `transaction`.`campaignId`) and (`transaction`.`campaignId` = `campaign`.`id`) and isnull(`transaction`.`refundTime`) and (`transaction`.`authCode` is not null)) group by `campaign`.`id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `mastercampaignnumberofflinedonations`
--

/*!50001 DROP TABLE IF EXISTS `mastercampaignnumberofflinedonations`*/;
/*!50001 DROP VIEW IF EXISTS `mastercampaignnumberofflinedonations`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `mastercampaignnumberofflinedonations` AS select `campaign`.`id` AS `id`,count(`offlinedonation`.`value`) AS `value` from (`campaign` join `offlinedonation`) where ((`offlinedonation`.`masterCampaignId` = `campaign`.`id`) and isnull(`offlinedonation`.`refundTime`)) group by `campaign`.`id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `mastercampaigntotaldonations`
--

/*!50001 DROP TABLE IF EXISTS `mastercampaigntotaldonations`*/;
/*!50001 DROP VIEW IF EXISTS `mastercampaigntotaldonations`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `mastercampaigntotaldonations` AS select `campaign`.`id` AS `id`,sum(`transaction`.`value`) AS `value` from (`campaign` join `transaction`) where ((`campaign`.`masterCampaignId` = `transaction`.`campaignId`) and (`transaction`.`campaignId` = `transaction`.`campaignId`) and (`transaction`.`campaignId` = `campaign`.`id`) and isnull(`transaction`.`refundTime`) and (`transaction`.`authCode` is not null)) group by `campaign`.`id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `mastercampaigntotalofflinedonations`
--

/*!50001 DROP TABLE IF EXISTS `mastercampaigntotalofflinedonations`*/;
/*!50001 DROP VIEW IF EXISTS `mastercampaigntotalofflinedonations`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `mastercampaigntotalofflinedonations` AS select `campaign`.`id` AS `id`,sum(`offlinedonation`.`value`) AS `value` from (`campaign` join `offlinedonation`) where ((`offlinedonation`.`masterCampaignId` = `campaign`.`id`) and isnull(`offlinedonation`.`refundTime`)) group by `campaign`.`id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `mediadata`
--

/*!50001 DROP TABLE IF EXISTS `mediadata`*/;
/*!50001 DROP VIEW IF EXISTS `mediadata`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `mediadata` AS select `media`.`id` AS `id`,`media`.`blobId` AS `blobId`,`media`.`siteId` AS `siteId`,`media`.`superSiteId` AS `superSiteId`,`media`.`name` AS `name`,`media`.`description` AS `description`,`media`.`fileName` AS `fileName`,`media`.`uri` AS `uri`,`media`.`memberId` AS `memberId`,`media`.`source` AS `source`,`media`.`created` AS `created`,`media`.`updated` AS `updated`,`tagsbymedia`(`media`.`id`) AS `tagsname` from `media` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `membercampaigncount`
--

/*!50001 DROP TABLE IF EXISTS `membercampaigncount`*/;
/*!50001 DROP VIEW IF EXISTS `membercampaigncount`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `membercampaigncount` AS select `member`.`id` AS `id`,count(`campaign`.`id`) AS `count` from (`member` join `campaign`) where ((`member`.`id` = `campaign`.`memberId`) and isnull(`campaign`.`deleteTime`)) group by `member`.`id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `memberfriendcount`
--

/*!50001 DROP TABLE IF EXISTS `memberfriendcount`*/;
/*!50001 DROP VIEW IF EXISTS `memberfriendcount`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `memberfriendcount` AS select `member`.`id` AS `id`,count(`circleoftrust`.`id`) AS `count` from (`member` join `circleoftrust`) where ((`member`.`id` = `circleoftrust`.`inviterId`) and (`circleoftrust`.`acceptTime` is not null)) group by `member`.`id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `membergroupcount`
--

/*!50001 DROP TABLE IF EXISTS `membergroupcount`*/;
/*!50001 DROP VIEW IF EXISTS `membergroupcount`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `membergroupcount` AS select `member`.`id` AS `id`,count(`societymember`.`id`) AS `count` from (`member` join `societymember`) where (`member`.`id` = `societymember`.`memberId`) group by `member`.`id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `memberinvitationcount`
--

/*!50001 DROP TABLE IF EXISTS `memberinvitationcount`*/;
/*!50001 DROP VIEW IF EXISTS `memberinvitationcount`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `memberinvitationcount` AS select `member`.`id` AS `id`,count(`invitation`.`id`) AS `count` from (`member` join `invitation`) where ((`member`.`id` = `invitation`.`invitedId`) and isnull(`invitation`.`rejectTime`) and isnull(`invitation`.`acceptTime`) and isnull(`invitation`.`retireTime`)) group by `member`.`id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `memberklogcount`
--

/*!50001 DROP TABLE IF EXISTS `memberklogcount`*/;
/*!50001 DROP VIEW IF EXISTS `memberklogcount`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `memberklogcount` AS select `member`.`id` AS `id`,count(`klog`.`id`) AS `count` from (`member` join `klog`) where ((`member`.`id` = `klog`.`memberId`) and isnull(`klog`.`deleteTime`)) group by `member`.`id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `memberpollcount`
--

/*!50001 DROP TABLE IF EXISTS `memberpollcount`*/;
/*!50001 DROP VIEW IF EXISTS `memberpollcount`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `memberpollcount` AS select `member`.`id` AS `id`,count(`poll`.`id`) AS `count` from (`member` join `poll`) where ((`member`.`id` = `poll`.`memberId`) and isnull(`poll`.`societyId`)) group by `member`.`id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `memberpostcount`
--

/*!50001 DROP TABLE IF EXISTS `memberpostcount`*/;
/*!50001 DROP VIEW IF EXISTS `memberpostcount`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `memberpostcount` AS select `member`.`id` AS `id`,count(`paidpost`.`id`) AS `count` from (`member` join `paidpost`) where (`member`.`id` = `paidpost`.`aboutMemberId`) group by `member`.`id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `pollinvitecount`
--

/*!50001 DROP TABLE IF EXISTS `pollinvitecount`*/;
/*!50001 DROP VIEW IF EXISTS `pollinvitecount`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `pollinvitecount` AS select `poll`.`id` AS `id`,count(0) AS `count` from (`poll` join `invitation`) where (`poll`.`id` = `invitation`.`pollId`) group by `poll`.`id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `pollresponsecount`
--

/*!50001 DROP TABLE IF EXISTS `pollresponsecount`*/;
/*!50001 DROP VIEW IF EXISTS `pollresponsecount`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `pollresponsecount` AS select `poll`.`id` AS `id`,count(0) AS `count` from (`poll` join `pollresponse`) where (`poll`.`id` = `pollresponse`.`pollId`) group by `poll`.`id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `postnocount`
--

/*!50001 DROP TABLE IF EXISTS `postnocount`*/;
/*!50001 DROP VIEW IF EXISTS `postnocount`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `postnocount` AS select `paidpost`.`id` AS `id`,count(0) AS `count` from (`paidpost` join `paidpostaccuracy`) where ((`paidpost`.`id` = `paidpostaccuracy`.`postId`) and (`paidpostaccuracy`.`noVote` = 1)) group by `paidpost`.`id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `postyescount`
--

/*!50001 DROP TABLE IF EXISTS `postyescount`*/;
/*!50001 DROP VIEW IF EXISTS `postyescount`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `postyescount` AS select `paidpost`.`id` AS `id`,count(0) AS `count` from (`paidpost` join `paidpostaccuracy`) where ((`paidpost`.`id` = `paidpostaccuracy`.`postId`) and (`paidpostaccuracy`.`yesVote` = 1)) group by `paidpost`.`id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `societymembercount`
--

/*!50001 DROP TABLE IF EXISTS `societymembercount`*/;
/*!50001 DROP VIEW IF EXISTS `societymembercount`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `societymembercount` AS select `society`.`id` AS `id`,count(0) AS `count` from (`society` join `societymember`) where (`society`.`id` = `societymember`.`societyId`) group by `society`.`id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `topcampaign`
--

/*!50001 DROP TABLE IF EXISTS `topcampaign`*/;
/*!50001 DROP VIEW IF EXISTS `topcampaign`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `topcampaign` AS select `sitemember`.`firstName` AS `firstname`,`sitemember`.`imageBlobId` AS `imageBlobId`,`campaign`.`id` AS `campaignId`,`campaign`.`type` AS `type`,sum(`transaction`.`value`) AS `totalRaised`,`sitemember`.`id` AS `sitememberId`,`sitemember`.`siteId` AS `siteId` from ((`sitemember` join `campaign`) join `transaction`) where ((`transaction`.`campaignId` = `campaign`.`id`) and ((`sitemember`.`campaignId` = `campaign`.`id`) or (`sitemember`.`teamCampaignId` = `campaign`.`id`))) group by `transaction`.`campaignId` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-09-01 13:17:49
